<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$this->language = $doc->language;
$this->direction = $doc->direction;



//set session background color
$session = JFactory::getSession();
$user_session = $session->get('user');
$u = $user_session->id;
$b = $session->get('background_color');
if (!empty($u) && isset($b)) {
    $session->set('background_color', OpenShopHelper::getConfigValue('t_background_color'));
}

if (empty($u)) {
    $session->clear('background_color');
}

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');


require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/defines.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/inflector.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/autoload.php';
require_once JPATH_ADMINISTRATOR . '/components/com_openshop/libraries/rad/bootstrap.php';
// Load Bootstrap CSS and JS
if (OpenShopHelper::getConfigValue('load_bootstrap_css')) {
    OpenShopHelper::loadBootstrapCss();
}
if (OpenShopHelper::getConfigValue('load_bootstrap_js')) {
    OpenShopHelper::loadBootstrapJs();
}

// Output as HTML5
$doc->setHtml5(true);


$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet($this->baseurl . '/components/com_openshop/assets/css/toastr.min.css');
////$doc->addStyleSheet($this->baseurl . '/components/com_openshop/assets/bootstrap/css/font-awesome.css');
$doc->addStyleSheet($this->baseurl . '/components/com_openshop/assets/bootstrap/css/font-awesome.min.css');
//$doc->addStyleSheet($this->baseurl . '/components/com_openshop/assets/bootstrap/css/bootstrap.css.map');
$doc->addStyleSheet($this->baseurl . '/components/com_openshop/assets/css/swiper.min.css');
$doc->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/nelo.css');
$doc->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/menus.css');
$doc->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/breadcrumb.css');
$doc->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/main.css');
$doc->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/responsive.css');

$doc->addScript(JUri::root(true) . '/components/com_openshop/assets/bootstrap/js/jquery.min.js');
$doc->addScript($this->baseurl . '/components/com_openshop/assets/js/toastr.min.js');
$doc->addScript($this->baseurl . '/components/com_openshop/assets/js/swiper.min.js');
$doc->addScript(JUri::base(true) . '/components/com_openshop/assets/js/jquery.elevatzoom.min.js');
$doc->addScript(JUri::base(true) . '/templates/' . $this->template . '/js/jquery.lazyload.js');
$doc->addScript(JUri::base(true) . '/components/com_openshop/assets/js/nelo.js');
$doc->addScript(JUri::base(true) . '/components/com_openshop/assets/js/main.js');
$doc->addScript(JUri::base(true) . '/components/com_openshop/assets/js/responsive.js');

//visitor
// OpenShopHelper::saveInfoVisitor();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <jdoc:include type="head" />
        <link rel="stylesheet" type="text/css" href="<?php echo JUri::base() . 'components/com_openshop/assets/css/animate.css' ?>">
            <script src="<?php echo JUri::base() . 'components/com_openshop/assets/js/wow.min.js' ?>"></script>
            <?php // Use of Google Font      ?>
            <?php if ($this->params->get('googleFont')) : ?>
                <link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
                <style type="text/css">
                    h1,h2,h3,h4,h5,h6,.site-title{
                        font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
                    }
                </style>
            <?php endif; ?>

    </head>
    <style>
        .container_content{
            background-image: url('/images/patern/background-content1.png');
            box-shadow: 0px 0px 10px 0px #b3b3b3;
            border-radius: 5px 5px 5px 5px;
            margin-top: 5px;

        }
        .breadcrumb_new{
            border: solid 1px #CCC;
            border-radius: 5px;
            margin-top: 5px;
            padding: 2px;
            background: rgba(135, 141, 144, 0.22);
        }

        .wrapper-panel-left{
            background:rgba(245, 245, 245, 0.72);
            padding: 0 10px 20px 10px;
            margin-top: 6px;
            border: solid 1px #ccc;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #b3b3b3;
        }
        .wrapper-panel-left .sub_pro{
            margin-top:5px !important;
        }

        .wrapper-panel-left .page-header{
            margin: 0px;
        }

        .wrapper-content{

        }
        .wrapper-content-left{
            padding: 0 15px 10px 6px;

        }
        .wrapper-intro-right{
            padding-right: 3px;
        }
        .wrapper-intro-right .sub_pro{
            margin-top: 5px !important;
        }

    </style>

    <body   class="site <?php
    echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
    echo ($this->direction == 'rtl' ? ' rtl' : '');
    ?>">

        <?php
        $json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
        ?>

        <div id="wrapper_body">
            <!-- BEGIN HEAD -->
            <div id="wrapper_head">
                <div class="wapper-head-t2">
                    <div class="head-top">
                        <jdoc:include type="modules" name="nelo-top" style="none" />
                    </div>
                    <div class="head-logo-cart width-body">
                        <div class="head-logo col-sm-3 col-md-3">
                            <jdoc:include type="modules" name="nelo-logo" style="none" />
                        </div>
                        <div class="head-seach col-sm-6 col-md-6">
                            <jdoc:include type="modules" name="nelo-search" style="none" />
                        </div>
                        <div class="head-loginout col-sm-3 col-md-3">
                            <?php
                            $modules = JModuleHelper::getModules('nelo-loginout');
                            foreach ($modules as $module) {
                                echo JModuleHelper::renderModule($module);
                            }

                            $modules = JModuleHelper::getModules('nelo-cart');
                            foreach ($modules as $module) {
                                echo JModuleHelper::renderModule($module);
                            }
                            ?>
                        </div>
                    </div>

                    <div class="clr"></div>
                </div>
            </div>
            <!-- END HEAD -->
            <div class="clr"></div>

            <!-- BEGIN MAINMENU -->
            <div id="wrapper_menu">
                <div class="head-menu">
                    <div class="col-md-3">
                        <div>
                            <div class="menu-caterogy">
                                <?php
                                $modules = JModuleHelper::getModules('nelo-menu-categories');
                                foreach ($modules as $module) {
                                    echo JModuleHelper::renderModule($module);
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <?php
                        $modules = JModuleHelper::getModules('nelo-main-menu');
                        foreach ($modules as $module) {
                            echo JModuleHelper::renderModule($module);
                        }
                        ?>
                    </div>
                </div>
                <div class="clr"></div>
            </div>
            <!-- END MAINMENU -->
            <div class="clr"></div>

            <div id="wrapper-container"> 
                <!--BEGIN SLIDE-->
                <div id="wrapper_slide">
                    <div class="head-silde width-body">
                        <div class="col-xs-8 col-sm-6 col-md-3 menu-caterogy-left padding0">

                        </div>
                        <div class="menu-category-right">
                            <?php
                            if ($view == 'frontpage' && $option == 'com_openshop') {
                                $modules = JModuleHelper::getModules('nelo-slider');
                                foreach ($modules as $module) {
                                    echo JModuleHelper::renderModule($module);
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- END SLIDE -->
                <div class="clr"></div>

                <!-- BEGIN CONTENT-->
                <div id="wrapper_middle_content">

                    <?php
                    //Default HomePage
                    if ($option == 'com_openshop'):
                        ?>
                        <div class="container_products width-body" >
                            <div  class="productsBody">
                                <jdoc:include type="component" />
                            </div>
                        </div>
                        <?php
                    endif;

                    //Single Article
                    if ($option == 'com_content' && $view == 'article') :
                        ?>
                        <div class="container_content width-body" >

                            <div class="wrapper-content">
                                <div class="col-md-9 padding0">
                                    <div class="wrapper-content-left">
                                        <jdoc:include type="modules" name="breadcrumb" style="html5" />
                                        <div class="wrapper-panel-left">
                                            <jdoc:include type="component" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding0">
                                    <div class="wrapper-intro-right">
                                        <?php if ($this->countModules('intro-right')) : ?>
                                            <jdoc:include type="modules" name="intro-right" style="html5" />
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="clr10"></div>
                            </div>
                            <div class="clr"></div>
                            <div class="wrapper_intro_module">
                                <?php if ($this->countModules('intro-bot')) : ?>
                                    <jdoc:include type="modules" name="intro-bot" />
                                <?php endif; ?>
                            </div>
                            <div class="clr10"></div>
                        </div>

                        <?php
                    endif;

                    //Blog layout
                    if ($option == 'com_content' && $view == 'category' && $layout == 'blog') :
                        ?>
                        <div class="container_content width-body" >
                            <div class="wrapper-content">

                                <div class="col-md-9 padding0">
                                    <div class="wrapper-content-left">
                                        <jdoc:include type="modules" name="breadcrumb" style="html5" />
                                        <div class="wrapper-panel-left">
                                            <?php if ($this->countModules('news')) : ?>
                                                <jdoc:include type="modules" name="news" style="" />
                                            <?php endif; ?>
                                            <div class="clr"></div>
                                            <div class="list-article" style="overflow: hidden">
                                                <jdoc:include type="component" />
                                            </div>
                                            <div class="clr10"></div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-3 padding0">
                                    <div class="wrapper-intro-right">
                                        <?php if ($this->countModules('news-right')) : ?>
                                            <jdoc:include type="modules" name="news-right" style="html5" />
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="clr10"></div>

                            </div>
                        </div>
                        <?php
                    endif;
                    ?>

                </div>
                <!-- END CONTENT -->
                <div class="clr"></div>

                <!-- BEGIN BOTSLIDER-->
                <?php if ($this->countModules('main-news-bottom')) : ?>
                    <div class="wrapper_botslider">
                        <div class="containe-botslider width-body">

                            <jdoc:include type="modules" name="main-news-bottom" />

                        </div>
                    </div>
                <?php endif; ?>
                <!-- END BOTSIDER -->

                <div class="clr"></div>

                <!-- BEGIN  BOTTOMENU -->
                <div class="wrapper_bot_menu">
                    <div class="clr" style="height: 20px"></div>
                    <div class="footer_menu head-menu">
                        <?php if ($this->countModules('footer-menu')) : ?>
                            <div class="footer-menu width-body" style="align:center">
                                <jdoc:include type="modules" name="footer-menu" style="rounded" />
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
            <!-- END    BOTTOM MENU -->
            <div class="clr"></div>

            <!-- BEGIN FOOTER -->
            <div class="clr" style="height: 3px"></div>

            <div class="wrapper_footer">
                <div class="footer_container">
                    <div class="footer_top">
                        <?php if ($this->countModules('footer-info')) : ?>
                            <div>
                                <jdoc:include type="modules" name="footer-info" style="rounded" />
                            </div>
                        <?php endif; ?>
                        <div class="clr"></div>   
                    </div>
                    <div class="clr"></div>   
                    <div class="footer_bottom">

                    </div>
                </div>

            </div>
            <!-- END FOOTER -->
            <div class="clr"></div>
        </div>

        <!-------------------------------------------------------------------------------------------------->


        <div class="clr"></div>
        <!--loading-->
        <div class="loading">
            <div class="loading_access" >
            </div>
            <div class="content_access">
                <img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . 'Loading.gif' ?>" alt="Đang xử lý" title="Đang xử lý"/>
            </div>
        </div>

        <div class="clr"></div>

        <!-- Modal Loading -->
        <div class="modal" id="myLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="bg-loading"></div>
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="clr"></div>
        <!--question-->
        <div class="question">
            <div class="loading_access" >
            </div>
            <div class="content_access">
                <div class="titleAction">Bạn có chắc thực hiện?</div>
                <div class="buttonAction">
                    <span class="action" id="action" onclick="">Thực hiện</span>
                    <span class="closeAction" onclick="closeAction()">Thoát</span>
                </div>
            </div>
        </div>


        <div class="scrollTop">
            <div class="bg-scrollTop">
                <i class="fa fa-chevron-up" aria-hidden="true"></i>
            </div>
        </div>


        <div id="showDetail">
            <!--Show Detail-->
        </div>
        <input type="hidden" value="<?php echo OpenShopHelper::convertBase64Encode($_SERVER['REQUEST_URI']) ?>" id="urlHistory" />

        <!--//body script-->
        <script>
            jQuery(window).scroll(function () {
                var offset = 250;
                var duration = 500;
                var scrollTop = jQuery('.scrollTop');
                if (jQuery(this).scrollTop() > offset) {
                    scrollTop.fadeIn(500);
                } else {
                    scrollTop.fadeOut(500);
                }

            });

            jQuery('.scrollTop').click(function (event) {
                event.preventDefault();
                jQuery('html, body').animate({scrollTop: 0}, 500);
                return false;
            });
            //remove js and css
            function removecssjsfile(filename, type)
            {
                var ele = (type == 'js') ? 'script' : (type == 'css') ? 'link' : 'none';
                var attr = (type == 'js') ? 'src' : (type == 'css') ? 'href' : 'none';
                var t = document.getElementsByTagName(ele);
                for (i = 0; i < t.length; i++)
                {
                    if (t[i][attr].search(filename) != -1)
                    {
                        t[i].remove();
                    }
                }
            }
            removecssjsfile('/media/system/js/mootools-core.js', 'js');
            removecssjsfile('/media/system/js/core.js', 'js');
            removecssjsfile('/media/system/js/mootools-more.js', 'js');
            removecssjsfile('/media/system/js/modal.js', 'js');
            removecssjsfile('/media/jui/js/jquery.min.js', 'js');
            removecssjsfile('/media/jui/js/jquery-migrate.min.js', 'js');
            removecssjsfile('/media/jui/js/bootstrap.min.js', 'js');
            removecssjsfile('/media/jui/js/jquery-noconflict.js', 'js');
            removecssjsfile('/media/system/css/modal.css', 'css');

            jQuery(function () {
                jQuery("img.lazy").lazyload({
                    effect: "fadeIn"
                });
            });

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });

            new WOW().init();
        </script>
    </body>
</html>
