<?php
defined('_JEXEC') or die;
//no robot
$doc = JFactory::getDocument();
$doc->setMetaData('robot', 'no index,no follow');
?>

<style>
    .cart-success-title{
        font-size: 20px;
        border-bottom: 1px solid;
        color: #FF0000;
    }
    .btn-home-back{
        padding: 10px 20px;
        background: #498D0E;
        color: white;
    }
    .btn-home-back:hover{
        box-shadow: 0px 4px 15px -2px rgba(0,0,0,0.75);
        background: #5FB514;
        cursor: pointer;
    }
    .btn-icon{
        padding: 10px 15px;
        background: #4EA900;
        position: relative;
        right: -20px;
    }
</style>

<div>
    <div class="row-fluid">
        <div style="text-align: center;" class="span12">
            <hr>
            <span class="cart-success-title">
                CẢM ƠN ĐƠN ĐẶT HÀNG CỦA BẠN
            </span>
            <div style="margin: 5px 0 20px 0;">
                <i>Chúng tôi sẽ liên hệ với bạn trong vòng 48 giờ (kể từ lúc đặt hàng)</i>
            </div>
            <div style="margin: 0 0 30px 0;">
                <?php
                $t = JFactory::getApplication()->input->getString('t');
                if (isset($t)) {
                    $res = $this->checkNewUser($t);
                    if (!empty($res)) {
                        ?>
                        <div>Hệ thống tự động đăng ký tài khoản:</div>
                        <div><b>Tên: </b> <?php echo $res->name ?></div>
                        <div><b>Tên tài khoản: </b> <?php echo $res->username ?></div>
                        <div><b>Mật khẩu: </b> <i>(Số điện thoại bạn vừa đăng ký)</i></div>

                        <?php
                    }
                }
                ?>
            </div>
            <span class="btn-home-back" onclick="window.location.href = 'index.php'">
                <?php echo JText::_('OPENSHOP_HOME_BACK') ?>
                <span class="btn-icon"><i class="fa fa-angle-right"></i></span>
            </span>

            <hr>
        </div>

    </div>
</div>
