<?php
defined('_JEXEC') or die('Restricted access');
$app = JFactory::getApplication()->input;
?>

<div class="openshop-products-list">
    <?php
    $products = $this->products;
    $idCat = $app->getInt('catid');
    require_once OPENSHOP_COMPONENTS . 'common/products/viewProducts.php';
    ?>
</div>

<?php
//get Modal 
require_once OPENSHOP_COMPONENTS . 'common/product/showModalProduct.php';
?>

