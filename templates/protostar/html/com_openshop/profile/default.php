<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$session = JFactory::getSession();
?>

<div class="profile">
    <div class="col-md-3" style="margin-bottom: 40px;">
        <div class="titleGeneral">Chung</div>
        <div class="contentTitleProfile">
            <div class="ct">
                <a href="/thong-tin-ca-nhan.html">Thông tin cá nhân</a>
            </div>
            <div class="ct">
                <a href="/gio-hang.html">Giỏ hàng</a>
            </div>
            <div class="ct">
                <a href="/lich-su-mua-hang.html">Lịch sử mua hàng</a>
            </div>
        </div>
    </div>
    <div class="col-md-9" style="margin-bottom: 40px;">
        <div class="titleGeneral">Thông tin cá nhân</div>
        <div class="contentProfile">
            <?php
            $t = $session->get('resEdit');
            if (isset($t) && !empty($t)) {
                if ('success' == OpenShopHelper::convertBase64Decode($t)) {
                    ?>
                    <div class="col-md-12 resSuccess">
                        <i class="fa fa-check"></i> Cập nhật thông tin thành công <i class="fa fa-close resClose" onclick="removeAttr('resSuccess')"></i>
                    </div>
                    <?php
                } else if ('error' == OpenShopHelper::convertBase64Decode($t)) {
                    ?>
                    <div class="col-md-12 resError">
                        <i class="fa fa-close"></i> Cập nhật thông tin không thành công <i class="fa fa-close resClose" onclick="removeAttr('resError')"></i>
                    </div>
                    <?php
                } else if ('dontconnect' == OpenShopHelper::convertBase64Decode($t)) {
                    ?>
                    <div class="col-md-12 resError">
                        <i class="fa fa-close"></i> Kết nối với máy chủ thất bại. 
                        <a href="/thong-tin-ca-nhan.html" style="color:white;">Vui lòng nhấp vào đây để tải lại trang. </a>
                        <i class="fa fa-close resClose" onclick="removeAttr('resError')"></i>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="col-md-12 colPadding">
                <label class="col-md-3"> Họ & tên </label>
                <div class="col-md-9">
                    <?php
                    echo $this->profile->fullname;
                    ?>
                    <span class="fa fa-pencil-square-o editProfile" onclick="getModelShowEditProfile('editname')"> Chỉnh sửa</span>
                </div>
            </div>
            <div class="col-md-12 colPadding">
                <label class="col-md-3"> Tên tài khoản </label>
                <div class="col-md-9">
                    <?php
                    echo $this->profile->username;
                    ?>
                </div>
            </div>
            <div class="col-md-12 colPadding">
                <label class="col-md-3"> Mật khẩu </label>
                <div class="col-md-9">
                    *******************
                    <span class="fa fa-pencil-square-o editProfile" onclick="getModelShowEditProfile('editpassword')"> Chỉnh sửa</span>
                </div>
            </div>
            <div class="col-md-12 colPadding">
                <label class="col-md-3"> Số điện thoại </label>
                <div class="col-md-9">
                    <?php
                    echo isset($this->profile->telephone) ? $this->profile->telephone : 'Chưa có số điện thoại';
                    ?>
                    <span class="fa fa-pencil-square-o editProfile" onclick="getModelShowEditProfile('editphone')"> Chỉnh sửa</span>
                </div>
            </div>
            <div class="col-md-12 colPadding">
                <label class="col-md-3"> Email </label>
                <div class="col-md-9">
                    <?php
                    echo isset($this->profile->email) ? $this->profile->email : 'Chưa có email';
                    ?>
                    <span class="fa fa-pencil-square-o editProfile" onclick="getModelShowEditProfile('editemail')"> Chỉnh sửa</span>
                </div>
            </div>
            <div class="col-md-12 colPadding">
                <label class="col-md-3"> Địa chỉ </label>
                <div class="col-md-9">
                    <?php
                    echo isset($this->profile->address) ? $this->profile->address : 'Chưa có địa chỉ';
                    ?>
                    <span class="fa fa-pencil-square-o editProfile" onclick="getModelShowEditProfile('editaddress')"> Chỉnh sửa</span>
                    <br/>
                    [ <b><?php echo isset($this->profile->zone_name) ? $this->profile->zone_name : 'Chưa có Tỉnh'; ?></b> 
                        - 
                     <b><?php echo isset($this->profile->district_name) ? $this->profile->district_name : 'Chưa có Huyện' ?></b> ]
                </div>
            </div>
        </div>
    </div>
</div>

<form action="index.php" method="post" id="" class="form-horizontal">
    <div class="modal fade" id="myModalEditProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Nhập thông tin cần chỉnh sửa</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group editNoneProfile editname">
                        <label class="col-md-4">Nhập họ & tên</label>
                        <div class="col-md-8">
                            <input type="text" name="nameProfile" id="nameProfile" value="" class="form-control" placeholder="Nhập họ & tên mới"/>
                            <span class="error error-nameProfile"></span>
                        </div>
                    </div>
                    <div class="form-group editNoneProfile editpassword">
                        <label class="col-md-4">Nhập mật khẩu</label>
                        <div class="col-md-8">
                            <input type="password" name="passwordProfile" id="passwordProfile" value="" class="form-control" placeholder="Nhập mật khẩu"/>
                            <span class="error error-passwordProfile"></span>
                        </div>
                    </div>
                    <div class="form-group editNoneProfile editpassword">
                        <label class="col-md-4">Nhập lại mật khẩu</label>
                        <div class="col-md-8">
                            <input type="password" name="passwordagainProfile" id="passwordagainProfile" value="" class="form-control"  placeholder="Nhập lại mật khẩu"/>
                            <span class="error error-passwordagainProfile"></span>
                        </div>
                    </div>
                    <div class="form-group editNoneProfile editphone">
                        <label class="col-md-4">Nhập số điện thoại mới</label>
                        <div class="col-md-8">
                            <input type="text" name="phoneProfile" id="phoneProfile" value="" class="form-control" placeholder="Nhập số điện thoại mới"/>
                            <span class="error error-phoneProfile"></span>
                        </div>
                    </div>
                    <div class="form-group editNoneProfile editemail">
                        <label class="col-md-4">Nhập Email mới</label>
                        <div class="col-md-8">
                            <input type="text" name="emailProfile" id="emailProfile" value="" class="form-control"  placeholder="Nhập email mới" />
                            <span class="error error-emailProfile"></span>
                        </div>
                    </div>
                    <div class="form-group editNoneProfile editaddress">
                        <label class="col-md-4">Nhập địa chỉ mới</label>
                        <div class="col-md-8">
                            <input type="text" name="addressProfile" id="addressProfile" value="" class="form-control"  placeholder="Nhập địa chỉ của bạn bao gồm số nhà, đường,..." />
                            <span class="error error-addressProfile"></span>
                        </div>
                    </div>
                    <div class="form-group editNoneProfile editaddress">
                        <label class="col-md-4">Tỉnh/Thành Phố</label>
                        <div class="col-md-8">
                            <select name="townProfile" id="customer_town" class="form-control" onchange="changeZone(this.value)">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_ZONE') ?></option>
                                <?php
                                $db = JFactory::getDbo();
                                $query = $db->getQuery(TRUE);
                                try {
                                    $query->select('id,zone_name')
                                            ->from($db->quoteName('#__openshop_zones'))
                                            ->where('published = 1');
                                } catch (Exception $ex) {
                                    //error
                                }
                                $town = $db->setQuery($query)->loadObjectList();

                                foreach ($town as $v_z) {
                                    $cls = '';
                                    echo '<option value="' . $v_z->id . '" ' . $cls . '>' . $v_z->zone_name . '</option>';
                                }
                                ?>
                            </select>
                            <div class="error error-customer_town"></div>
                        </div>
                    </div>
                    <div class="form-group editNoneProfile editaddress">
                        <label class="col-md-4">Quận/Huyện </label>
                        <div class="col-md-8">
                            <select name="districtProfile" id="customer_district" class="form-control">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_DISTRICT') ?></option>
                                <?php
                                foreach ($district as $v_d) {
                                    $cls = '';
                                    echo '<option value="' . $v_d->id . '" ' . $cls . '>' . $v_d->district_name . '</option>';
                                }
                                ?>
                            </select>
                            <div class="error error-customer_district"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" onclick="return checkFormProfile()"><span class="fa fa-pencil-square-o"></span> Chỉnh sửa</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Thoát</button>
                    <input type="hidden" name="varedit" id="varedit" value="">
                    <input type="hidden" name="option" value="com_openshop">
                    <input type="hidden" name="task" value="profile.editprofile">
                    <input type="hidden" name="return" value="<?php echo OpenShopHelper::convertBase64Encode('/thong-tin-ca-nhan.html') ?>">
                    <?php echo JHtml::_('form.token') ?>
                </div>
            </div>
        </div>
    </div>
</form>