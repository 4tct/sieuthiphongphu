<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/js/cart.js" type = "text/javascript"></script>
<?php
$session = JFactory::getSession();
$user_session = $session->get('user');
$app = JFactory::getApplication()->input;

$sizeP = array();
foreach ($this->getAllSize() as $se) {
    $sizeP[$se->optionvalue_id] = $se->value;
}
?>

<form action="" class="form-horizontal" id="formCart" >
    <div class="row-fluid">
        <div class="col-md-8 cartP">
            <div class="cart-title">
                <span class="cart-title-1"><?php echo JText::_('OPENSHOP_CART_TITLE') ?></span>
            </div>
            <div class="contentCartP">
                <?php
                if (!OpenShopHelper::isMobile()) {
                    ?>
                    <div class="paddingContentCartP table-responsive">
                        <?php
                        $cart = $this->getCartProducts($_SERVER['REMOTE_ADDR'], $user_session->id);

                        if (!empty($cart)) {
                            ?>
                            <table width="100%" class="table table-bordered " id="table-cart">
                                <thead>
                                    <tr>
                                        <th width="5%"><?php echo JText::_('OPENSHOP_#') ?></th>
                                        <th width="55%"><?php echo JText::_('OPENSHOP_PRODUCT_NAME') ?></th>
                                        <th width="13%"><?php echo JText::_('OPENSHOP_QUANTITY') ?></th>
                                        <th width="12%">Đơn giá</th>
                                        <th width="20%"><?php echo JText::_('OPENSHOP_AMOUNT') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total_quantity = 0;
                                    $total_money = 0;
                                    $stt = 0;
                                    $t = 1;
                                    $id = '';
                                    foreach ($cart as $k_c => $v_c) {
                                        ++$stt;
                                        $id = OpenShopHelper::convertBase64Encode($v_c->id);
                                        ?>
                                        <tr>
                                            <td class="text-center"><?php echo $stt ?></td>
                                            <td style="position: relative;">
                                                <img class="lazy" data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $v_c->product_image ?>" width="50px"/>
                                                <div class="nameSizeP">
                                                    <?php echo $v_c->product_name ?>
                                                    <?php
                                                    if ($v_c->size_id) {
                                                        ?>
                                                        <br>
                                                        Size: <b><?php echo $sizeP[$v_c->size_id] ?></b>
                                                        <br>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <i class="deleteCartP" onclick="showRemoveAction('<?php echo $id ?>')">Xóa sản phẩm?</i>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                $readonly = 'readonly';
                                                if ($v_c->quantity < 5) {
                                                    $readonly = '';
                                                }
                                                $idCartEncode = OpenShopHelper::convertBase64Encode($v_c->id);
                                                ?>
                                                <div class="numricLR">
                                                    <div class="sub-quantity" onclick="updateQuantity(<?php echo $k_c ?>, '<?php echo $idCartEncode ?>', '-')">
                                                        <span>-</span>
                                                    </div>
                                                    <input <?php echo $readonly ?> name="slt_quantity" type="number" id="" class="inputNumberProduct inputQ_<?php echo $k_c ?>" value="<?php echo $v_c->quantity ?>" onchange="updateQuantity(<?php echo $k_c ?>, '<?php echo $idCartEncode ?>')"/>
                                                    <div class="add-quantity" onclick="updateQuantity(<?php echo $k_c ?>, '<?php echo $idCartEncode ?>', '+')">
                                                        <span>+</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                echo number_format((int) $v_c->product_price, 0, ',', '.') . '<sup>đ</sup>';
                                                if (!empty($v_c->product_price_r)) {
                                                    echo '<div class="price_r">' . number_format((int) $v_c->product_price_r, 0, ',', '.') . '<sup>đ</sup></div>';
                                                }
                                                ?>
                                                <input type="hidden" value="<?php echo (int) $v_c->product_price ?>" id="product_price_<?php echo $k_c ?>" />
                                            </td>
                                            <td class="text-center">
                                                <span class="totalPriceProduct_<?php echo $k_c ?>"><?php echo number_format($v_c->quantity * (int) $v_c->product_price, 0, ',', '.') ?></span>
                                                <sup>đ</sup>
                                                <input type="hidden" value="<?php echo (int) $v_c->quantity * (int) $v_c->product_price ?>" id="product_total_<?php echo $k_c ?>" />
                                            </td>
                                        </tr>
                                        <?php
                                        echo '</tr>';
                                        $total_quantity += $v_c->quantity;
                                        $total_money += (int) $v_c->quantity * (int) $v_c->product_price;
                                        if ($t == 1) {
                                            $id_cartpro = $v_c->id;
                                            $t = 0;
                                        } else {
                                            $id_cartpro .= ',' . $v_c->id;
                                        }
                                    }
                                    ?>
                                    <tr class="total_title">
                                        <td colspan="2" style="text-align: right"><?php echo JText::_('OPENSHOP_TOTAL_TITLE') ?></td>
                                        <td style="text-align:center">
                                            <span class="total-quantity"><?php echo $total_quantity ?></span>
                                        </td>
                                        <td colspan="2" style="text-align:center">
                                            <span class="total-money">
                                                <?php echo number_format($total_money, 0, ',', '.') ?>
                                            </span> 
                                            <sup>đ</sup>
                                        </td>
                                    </tr>
                                    <tr class="total_title">
                                        <td colspan="2" style="text-align: right"><?php echo JText::_('OPENSHOP_TRANSFER_MONEY') ?></td>
                                        <td style="text-align:center"></td>
                                        <td colspan="2" style="text-align:center">
                                            <span class="transfer-money">0</span> <sup>đ</sup>
                                        </td>
                                    </tr>
                                    <tr class="total_title">
                                        <td colspan="2" style="text-align: right"><?php echo JText::_('OPENSHOP_TOTAL') ?></td>
                                        <td style="text-align:center"></td>
                                        <td colspan="2" style="text-align:center">
                                            <span class="total"><?php echo number_format($total_money, 0, ',', '.') ?></span> <sup>đ</sup>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="total" id="total" value="<?php echo $total_money ?>"/>
                            <?php
                        } else {
                            ?>
                            <div style="    padding: 30px 0;">
                                <i>Hiện không có sản phẩm trong giỏ hàng </i>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    $carts = $this->getCartProducts($_SERVER['REMOTE_ADDR'], $user_session->id);
                    ?>
                    <div class="col-md-12 padding0">
                        <div>
                            <ul>
                                <?php
                                $total_quantity = 0;
                                $total_money = 0;
                                $stt = 0;
                                $t = 1;
                                $id = '';
                                foreach ($carts as $k_c => $cart) {
                                    ?>
                                    <li class="borderCartMobile">
                                        <div class="col-xs-12 col-sm-12 col-md-12 padding10">
                                            <div class="cartImg col-xs-4 col-sm-4 col-md-4 padding0">
                                                <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $cart->product_image ?>" alt="<?php echo $cart->product_name ?>" title="<?php echo $cart->product_name ?>" width="100px"/>
                                            </div>
                                            <div class="contentCartProduct col-xs-8 col-sm-8 col-md-8">
                                                <div class="productName">
                                                    <?php echo $cart->product_name ?>
                                                </div>
                                                <div>
                                                    <?php
                                                    if ($cart->size_id) {
                                                        ?>
                                                        Size: <b><?php echo $sizeP[$cart->size_id] ?></b>
                                                        <br>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <div class="priceProductMobile">
                                                    <?php
                                                    echo '<span class="pricePro">' . number_format((int) $cart->product_price, 0, ',', '.') . '<sup>đ</sup></span>';
                                                    if (!empty($cart->product_price_r)) {
                                                        echo '<div class="price_r">' . number_format((int) $cart->product_price_r, 0, ',', '.') . '<sup>đ</sup></div>';
                                                    }
                                                    ?>
                                                    <input type="hidden" value="<?php echo (int) $cart->product_price ?>" id="product_price_<?php echo $k_c ?>" />
                                                </div>
                                                <div>
                                                    <?php
                                                    $readonly = 'readonly';
                                                    if ($cart->quantity < 5) {
                                                        $readonly = '';
                                                    }
                                                    $idCartEncode = OpenShopHelper::convertBase64Encode($cart->id);
                                                    ?>
                                                    <div class="numricLR">
                                                        <div class="sub-quantity" onclick="updateQuantity(<?php echo $k_c ?>, '<?php echo $idCartEncode ?>', '-')">
                                                            <span>-</span>
                                                        </div>
                                                        <input <?php echo $readonly ?> name="slt_quantity" type="number" id="" class="inputNumberProduct inputQ_<?php echo $k_c ?>" value="<?php echo $cart->quantity ?>" onchange="updateQuantity(<?php echo $k_c ?>, '<?php echo $idCartEncode ?>')"/>
                                                        <div class="add-quantity" onclick="updateQuantity(<?php echo $k_c ?>, '<?php echo $idCartEncode ?>', '+')">
                                                            <span>+</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr"></div>
                                    </li>
                                    <?php
                                    $total_quantity += $cart->quantity;
                                    $total_money += (int) $cart->quantity * (int) $cart->product_price;
                                    if ($t == 1) {
                                        $id_cartpro = $cart->id;
                                        $t = 0;
                                    } else {
                                        $id_cartpro .= ',' . $cart->id;
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="clr"></div>
                    <?php
                }
                ?>
                <input type="hidden" name="total" id="total" value="<?php echo $total_money ?>"/>
                <input value="<?php echo OpenShopHelper::convertBase64Encode($id_cartpro) ?>" id="id_cartpro" name="id_cartpro" type="hidden"/>
                <div class="btnBuyOn">
                    <span class="btn-continue-buy" onclick="window.location.href = '<?php echo JUri::base(); ?>'">
                        <i class="fa fa-angle-left paddingR5" aria-hidden="true"></i> <?php echo JText::_('OPENSHOP_CONTINUE_BUY') ?>
                    </span>
                </div>
            </div>
        </div>
        <!--/LOGIN-->
        <div class="col-md-4 infoCusCart">
            <?php
            if (!empty($cart)) {
                if (empty($user_session->id)) {
                    ?>
                    <div class="logOrder">
                        <div class="login">
                            <span onclick="showlogin()">Đăng nhập</span>
                        </div>
                        <div>- hoặc -</div>
                        <div class="ordernotlogin">
                            <span onclick="showQuiclOrder()">Mua hàng nhanh</span>
                        </div>
                        <div><i>( Bạn chưa đăng nhập )</i></div>
                    </div>
                    <?php
                } else {
                    $u = OpenShopHelper::getUserInfo($user_session->id);
                    ?>
                    <div class="infoPayment">
                        <div class="form-group infoUser">
                            Thông tin mua hàng
                        </div>
                        <!--Địa chỉ của tài khoản-->
                        <div class="infoU">
                            <div class="form-group">
                                <label>Họ và tên <span class="error">*</span></label>
                                <input type="text" class="form-control infoUDisabled" name="user_name" id="user_name" value="<?php echo isset($u->fullname) ? $u->fullname : $user_session->name; ?>" />
                                <span class="error error-user_name"></span>
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại<span class="error">*</span></label>
                                <input type="text" class="form-control infoUDisabled" name="user_phone" id="user_phone" value="<?php echo isset($u->telephone) ? $u->telephone : ''; ?>" placeholder="Nhập số điện thoại"/>
                                <span class="error error-user_phone"></span>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control infoUDisabled" name="user_email" id="user_email" value="<?php echo isset($u->email) ? $u->email : ''; ?>" placeholder="Nhập email"/>
                                <span class="error error-user_email"></span>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ giao hàng</label>
                                <input type="text" class="form-control infoUDisabled" name="user_address" id="user_address" value="<?php echo isset($u->address) ? $u->address : ''; ?>" placeholder="Nhập địa chỉ giao hàng"/>
                                <span class="error error-user_address"></span>

                                <div class="row padding0 marginT10">
                                    <div class="col-md-6 user_town">
                                        <select class="form-control infoUDisabled" name="user_zone" id="user_zone" onchange="changeZoneUser(this.value)">
                                            <option value="">- Tỉnh/TP -</option>
                                            <?php
                                            $zones = OpenShopHelper::getZones();
                                            foreach ($zones as $zone) {
                                                $selected = '';
                                                if ($zone->id == $u->zone_id) {
                                                    $selected = 'selected="selected"';
                                                }
                                                ?>
                                                <option value="<?php echo $zone->id ?>" <?php echo $selected ?>><?php echo $zone->zone_name ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <span class="error error-user_zone"></span>
                                    </div>
                                    <div class="col-md-6 user_district">
                                        <select class="form-control infoUDisabled" name="user_district" id="user_district">
                                            <option value="">- Quận/Huyện -</option>
                                            <?php
                                            $districts = OpenShopHelper::getDistrictOfZone($u->zone_id);
                                            if (!empty($districts)) {
                                                foreach ($districts as $district) {
                                                    $selected = '';
                                                    if ($district->id == $u->district_id) {
                                                        $selected = 'selected="selected"';
                                                    }
                                                    ?>
                                                    <option value="<?php echo $district->id ?>" <?php echo $selected ?>><?php echo $district->district_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="error error-user_district"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <span class="error">* Yêu cầu bắt buộc nhập</span>
                            </div>
                            <div class="btnSubmitOrder" onclick="checkoutCart()">
                                <span class="btn-checkout"><i class=""></i> <?php echo JText::_('OPENSHOP_CHECKOUT') ?></span>
                            </div>
                            <div class="form-group">
                                <span>Bạn muốn giao hàng ở địa chỉ khác?</span>
                                <span class="shippingAddressDifferent">Địa chỉ khác</span>
                            </div>
                        </div>

                        <!--Giao hàng ở địa chỉ khác-->
                        <div class="shippingAddDiff">
                            <div class="form-group">
                                <label>Họ và tên <span class="error">*</span></label>
                                <input type="text" class="form-control shippingAddDiffDisabled" name="user_name" id="user_name_diff" value="" placeholder="Nhập họ và tên" disabled="disabled"/>
                                <span class="error error-user_name_diff"></span>
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại<span class="error">*</span></label>
                                <input type="text" class="form-control shippingAddDiffDisabled" name="user_phone" id="user_phone_diff" value="" placeholder="Nhập số điện thoại"  disabled="disabled"/>
                                <span class="error error-user_phone_diff"></span>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control shippingAddDiffDisabled" name="user_email" id="user_email_diff" value="" placeholder="Nhập email"  disabled="disabled"/>
                                <span class="error error-user_email_diff"></span>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ giao hàng</label>
                                <input type="text" class="form-control shippingAddDiffDisabled" name="user_address" id="user_address_diff" value="" placeholder="Nhập địa chỉ giao hàng"  disabled="disabled"/>
                                <span class="error error-user_address_diff"></span>

                                <div class="row padding0 marginT10">
                                    <div class="col-md-6 user_town">
                                        <select class="form-control shippingAddDiffDisabled" name="user_zone" id="user_zone_diff" onchange="changeZoneUserDiff(this.value)"  disabled="disabled">
                                            <option value="">- Chọn Tỉnh/TP -</option>
                                            <?php
                                            $zones = OpenShopHelper::getZones();
                                            foreach ($zones as $zone) {
                                                ?>
                                                <option value="<?php echo $zone->id ?>"><?php echo $zone->zone_name ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <span class="error error-user_zone_diff"></span>
                                    </div>
                                    <div class="col-md-6 user_district">
                                        <select class="form-control shippingAddDiffDisabled" name="user_district" id="user_district_diff"  disabled="disabled">
                                            <option value="">- Chọn Quận/Huyện -</option>
                                        </select>
                                        <span class="error error-user_district_diff"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <span class="error">* Yêu cầu bắt buộc nhập</span>
                            </div>
                            <div class="btnSubmitOrder" onclick="checkoutCartDiff()">
                                <span class="btn-checkout"><i class=""></i> <?php echo JText::_('OPENSHOP_CHECKOUT') ?></span>
                            </div>
                            <div class="form-group">
                                <span class="getMyaddress">Lấy địa chỉ của tôi</span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                if (empty($user_session->id)) {
                    ?>
                    <div class="btnSignin">
                        <span onclick="showSignIn()">Đăng ký</span>
                    </div>
                    <?php
                } else {
                    ?>
                    <div style="    padding: 30px 0;">
                        <i>Hiện không có sản phẩm trong giỏ hàng</i>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</form>

<!--//Quick Order-->
<form action="" method="post" id="formQuickOrder" class="form-horizontal">
    <div class="modal fade" id="myModalQuiclOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Đặt hàng nhanh</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3">Họ và tên <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập họ và tên của bạn" name="customer_name" id="customer_name_quick"/>
                            <span class="error error-customer_name_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Số điện thoại <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập số điện thoại của bạn" name="customer_phone" id="customer_phone_quick"/>
                            <span class="error error-customer_phone_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Email </label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập enail của bạn" name="customer_email" id="customer_email_quick"/>
                            <span class="error error-customer_email_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Địa chỉ <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập địa chỉ của bạn bao gồm số nhà, đường,..." name="customer_address" id="customer_address_quick"/>
                            <span class="error error-customer_address_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Tỉnh/Thành Phố <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <select name="zone" id="customer_town_quick" class="form-control" onchange="changeZone(this.value)">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_ZONE') ?></option>
                                <?php
                                $zone_id = isset($user) ? $user->zone_id : '-1';
                                foreach ($this->getZone() as $v_z) {
                                    $cls = '';
                                    if ($v_z->id == $zone_id) {
                                        $cls = 'selected';
                                    }
                                    echo '<option value="' . $v_z->id . '" ' . $cls . '>' . $v_z->zone_name . '</option>';
                                }
                                ?>
                            </select>
                            <span class="error error-customer_town_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Quận/Huyện <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <select name="district" id="customer_district_1" class="form-control">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_DISTRICT') ?></option>
                                <?php
                                $district_id = isset($user) ? $user->district_id : '-1';
                                foreach ($this->getDistrict() as $v_d) {
                                    $cls = '';
                                    if ($v_d->id == $district_id) {
                                        $cls = 'selected';
                                    }
                                    echo '<option value="' . $v_d->id . '" ' . $cls . '>' . $v_d->district_name . '</option>';
                                }
                                ?>
                            </select>
                            <span class="error error-customer_district_1"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3"></label>
                        <div class="col-md-9">
                            <i style="color:red;">(*)</i> Yêu cầu nhập
                            <input value="<?php echo $id ?>" id="id_cartpro" name="id_cartpro" type="hidden"/>
                            <input type="hidden" name="total" id="total" value="<?php echo $total_money ?>"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="checkoutCart('quick')">Đặt hàng</button>
                </div>
            </div>
        </div>
    </div>
</form>

<!--//LogIn-->
<form action="index.php?option=com_users&task=user.login" method="post" id="formLogin" class="form-horizontal">
    <div class="modal fade" id="modalFormLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Đăng nhập</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="username" id="username" value="" placeholder="Tài khoản" class="form-control"/>
                    <div class="error error-username"></div>
                    <br>
                    <input type="password" name="password" id="password" value="" placeholder="Mật khẩu" class="form-control"/>
                    <div class="error error-password"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="loginForm()">Đăng nhập</button>
                    <?php
                    $return = JRoute::_($_SERVER['REQUEST_URI']);
                    ?>
                    <input type="hidden" name="return" id="btl-return" value="<?php echo base64_encode($return) ?>">
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
?>




