<?php
defined('_JEXEC') or die('Restricted access');
$user = JFactory::getSession()->get('user');

$modules = JModuleHelper::getModules('nelo-show-product-home-site');
?>
<div class="show_product_home">
    <div class="show-pro-home-t4">
        <div class="content_show_product_home_t4">
            <?php
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
    </div>
</div>

<?php
//$params = json_decode($module->params);
//    if($params->showbanner && $params->chooseTypeShow == 'l1'){
//        echo '<div class="banner_home_site">';
//        $db = JFactory::getDbo();
//        $query = $db->getQuery(TRUE);
//        $query->select('a.media_name,b.media_image')
//                ->from($db->quoteName('#__openshop_medias','a'))
//                ->join('INNER', $db->quoteName('#__openshop_mediadetails','b') . ' ON a.id = b.media_id')
//                ->where('a.id = ' . $params->chooseBannerImg);
//        $banner_img = $db->setQuery($query)->loadObject();
//        echo '<img class="img-responsive img-banner-h" src="'. OPENSHOP_PATH_IMG_MEDIA_HTTP . $banner_img->media_image .'" title="'. $banner_img->media_name .'" alt="'. $banner_img->media_name .'" />';
//        echo '</div>';
//    }
?>