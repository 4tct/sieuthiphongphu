<?php
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript($this->baseurl . '/components/com_openshop/assets/js/jquery.countdown.min.js');

$app = JFactory::getApplication()->input;
$id = $app->getInt('id');
//print_r($this->banner);
date_default_timezone_set('Asia/Ho_Chi_Minh');

$doc = JFactory::getDocument();
$doc->setGenerator( 'Siêu Thị Phong Phú' );
$doc->setMetaData('keywords', empty($this->banner->meta_keywords) ? $this->banner->title : $this->banner->meta_keywords);

if (!empty($this->banner->meta_description)) {
    $doc->setDescription( $this->banner->meta_description );
}
$doc->setMetaData('robots', $this->banner->robot);
?>

<div class="row padding0">
    <div class="col-md-12 padding0 productBanner">
        <?php
        if (empty($this->products)) {
            ?>
            <div class="messageNotRun">
                <i>Hiện chương trình chưa mở.</i>
                <br>
                <b>Vui lòng quay lại sau</b>
            </div>
            <?php
        } else {
            $timefinish = $this->banner->time_finish . ' 23:59:59';
            $timestart = $this->banner->time_start . ' 00:00:00';

            $timecur = date('Y-m-d H:m:s');

//            echo $timecur;
            //ngày hiện tại < ngày bắt đầu chương trình
            if (strtotime($timestart) > strtotime($timecur)) {
                ?>
                <div class="messageNotRun">
                    <div class="countdown">
                        <i>Hiện chương trình sẽ mở sau <span id="clock" style="font-size: 30px;"></span>.</i>

                    </div>
                </div>

                <script>
                    //                    $('#clock').countdown('<?php echo date('Y/m/d H:m:s', strtotime($timestart)) ?>')
                    //                            .on('update.countdown', function (event) {
                    //                                var format = '%H:%M:%S';
                    //                                if (event.offset.totalDays > 0) {
                    //                                    format = '%-d ngày%!d ' + format;
                    //                                }
                    //                                if (event.offset.weeks > 0) {
                    //                                    format = '%-w tuần%!w ' + format;
                    //                                }
                    //                                $(this).html(event.strftime(format));
                    //                            })
                    //                            .on('finish.countdown', function (event) {
                    //                                window.location.reload();
                    //                            });
                </script>
                <?php
            }

            //ngày hiện tại > ngày kết thúc chương trình => chương trình kết thúc
            else if (strtotime($timefinish) < strtotime($timecur)) {
                ?>
                <div class="messageNotRun">
                    <i>Hiện chương trình đã kết thúc.</i>
                </div>
                <?php
            }

            //đang chạy chương trình
            else {
                ?>
                <div class="messageRun">
                    <div class="img_banner positionRelative">
                        <!--<img class="img-responsive" src="<?php echo OPENSHOP_PATH_BANNERS_HTTP . $this->banner->banner_image ?>" alt="<?php echo $this->banner->title ?>" title="<?php echo $this->banner->title ?>" />-->
                    </div>

                    <div class="col-md-12 titleBanner">
                        <h2>
                            <?php echo $this->banner->title ?>
                        </h2>
                        <div>
                            <?php echo $this->banner->description ?>
                        </div>
                    </div>

                    <div class="contentProductBanner col-md-12 padding0">
                        <?php
                        foreach ($this->products as $pro) {
                            $link = JRoute::_(OpenShopRoute::getProductRoute($pro->id, OpenShopHelper::getProductCategory($pro->id)));
                            ?>
                            <div class="col-md-3">
                                <div class="borderImgBanner">
                                    <div class="imgPro positionRelative">
                                        <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pro->id) ?>', '<?php echo $link ?>')">
                                            <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pro->product_image ?>" alt="<?php echo $pro->product_name ?>" title="<?php echo $pro->product_name ?>" data-toggle="tooltip" data-placement="top"/>
                                        </a>
                                        <?php
                                        if ($pro->product_price_r > 0) {
                                            $percent = 100 - (int) (((int) $pro->product_price * 100) / $pro->product_price_r)
                                            ?>
                                            <div class="percentSale top20"> 
                                                <?php echo $percent ?>%
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="infoPro">
                                        <div class="namePro">
                                            <a href="<?php echo $link ?>">
                                                <?php echo $pro->product_name ?>
                                            </a>
                                        </div>
                                        <div class="price">
                                            <span class="priceProBuy">
                                                <?php echo number_format($pro->product_price, 0, ',', '.') ?><sup>đ</sup>
                                            </span>
                                            <?php
                                            if ($pro->product_price_r > 0) {
                                                ?>
                                                <span class="priceProNotBuy">
                                                    <?php echo number_format($pro->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                </span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="buttonBuy">
                                            <div class="col-md-12" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pro->id) ?>', '<?php echo $link ?>')">
                                                <span>Mua ngay</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>


<!-- Detail -->
<div class="modal fade" id="myDetailProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 1030px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myDetailProductLabel">
                    <!--Tên sản phẩm-->
                </h4>
            </div>
            <div class="modal-body" id="myContentDetailProduct">
                <!--Detail-->





                <div class="clr"></div>


            </div>
        </div>
    </div>
</div>
