<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript(JURI::base() . '/modules/mod_openshop_advanced_search/assets/js/mod_openshop_advanced_search.js');

?>

<form action="<?php echo JRoute::_('index.php?option=com_openshop&task=search&Itemid='.$itemId); ?>" method="post" name="advancedSearchForm" id="advancedSearchForm">
	<div class="openshop_advanced_search<?php echo $params->get( 'classname' ) ?> row-fluid panel-group" id="accordion">
            <table border="1" cellpadding="0" cellspacing="0" style="width: 100%">
	<tbody>
            <?php
                if ($params->get('show_manufacturers', 1) && count($brands))
		{
            ?>
		<tr>
			<td>
                            <span class="openshop_name_category">
                                <div class="ttl-left"><?php echo JText::_('OPENSHOP_FILTER_MANUFACTURERS'); ?></div>
                            </span>
				
                        </td>
			<td>
                            <div class="menu-adv">
                            <?php
                            $j=0;
                            foreach ($brands as $brand)
			     {
                                
                            ?>
                            <label onclick="setimg('iconthuonghieu_<?php echo $j;?>','imgthuonghieu_<?php echo $j;?>')" class="for_brand brand_id-1">
<!--                                <a class="filter-cate" href="#">-->
                                    <i id="iconthuonghieu_<?php echo $j; ?>" class=""></i>
                                    <input id="iconthuonghieuinput_<?php echo $j; ?>" class="brand_ids" type="hidden" name="brand_ids[]" 
                                           value="<?php echo $brand->brand_id; ?>" 
                                           <?php //if (in_array($brand[$i]->id, $manufacturerIds)) echo 'checked="checked"'; ?> > 
                                    <img id="imgthuonghieu_<?php echo $j++;  ?>" class="img_attr_brand" src="<?php echo JPATH_MEDIA.$brand->brand_image; ?>" 
                                 width="40px" height="10px"/>
<!--                                </a>-->
                            </label>
                            
                            
                            <?php //if (in_array($brand->brand_id, $brandIds)) 
                                   // echo 'checked="checked"'; ?>
                            
                            &nbsp;&nbsp;&nbsp;
                            <?php
                            }
                            ?>
                            </div>
                        </td>
		</tr>
                <?php
                }
                ?>

                <?php
                if ($params->get('show_categories', 1) && count($categories))
		{
                ?>
		<tr>
			<td>
                            <div class="ttl-left"><?php echo JText::_('OPENSHOP_FILTER_CATEGORIES'); ?></div>
                        </td>
                        <td>
                            <div class="menu-adv">
                                    <?php
                                    for ($i = 0; $n = count($categories), $i < $n; $i++) {
                                        ?>
                                                <span class="adv_khoangcach" onclick="search('categories_<?php echo $i; ?>')">
                                                    <i id="categories_<?php echo $i; ?>" class="icon-checkbox-unchecked"></i>
                                                    <input id="categoriesinput_<?php echo $i; ?>" class="category_ids" type="hidden" name="category_ids[]" 
                                                            value="<?php echo $categories[$i]->id; ?>" <?php
                                                            if (in_array($categories[$i]->id, $categoryIds)) echo 'checked="checked"'; ?>>
                                                <?php
                                                echo $categories[$i]->category_name;
                                                ?>
                                                </span>
                                        <?php
                                    }
                                    ?>
                            </div>
                        </td>
		</tr>
                <?php
                }
                ?>
                
                <tr>
                    <td>
                        <div class="ttl-left">Giá(K=1000VNĐ)</div>
                    </td>
                    <td>
                        <div class="menu-adv">
                            <span onclick="search('gia_0')" class="adv_khoangcach">
                                <i id="gia_0" class="icon-checkbox-unchecked"></i>Dưới 100K
                                <input type="hidden" value="0_100000" id="giainput_0" class="min_price" name="min_price[]"/>
                            </span>
                            <span onclick="search('gia_1')" class="adv_khoangcach">
                                <i id="gia_1" class="icon-checkbox-unchecked"></i>100K-300K
                                <input type="hidden" value="100000_300000" id="giainput_1" class="min_price" name="min_price[]"/>
                            </span>
                            <span onclick="search('gia_2')" class="adv_khoangcach">
                                <i id="gia_2" class="icon-checkbox-unchecked"></i>300K-500K
                                <input type="hidden" value="300000_500000" id="giainput_2" class="min_price" name="min_price[]"/>
                            </span>
                            <span onclick="search('gia_3')" class="adv_khoangcach">
                                <i id="gia_3" class="icon-checkbox-unchecked"></i>500K-1000K
                                <input type="hidden" value="500000_1000000" id="giainput_3" class="min_price" name="min_price[]"/>
                            </span>
                            <span onclick="search('gia_4')" class="adv_khoangcach">
                                <i id="gia_4" class="icon-checkbox-unchecked"></i>Trên 1,000K
                                <input type="hidden" value="1000000_10000000" id="giainput_4" class="min_price" name="min_price[]"/>
                            </span>
                        </div>
                    </td>
                </tr>
                
                <?php
                if ($params->get('show_options', 1) && count($options))
		{
                ?>
		<tr>
                        
                                <?php
                                
                                foreach ($options as $option) {
                                    if (count($option->optionValues) && $option->option_name == "SIZE") {
                                        ?>
                                    <td>
                                                <div class="ttl-left">
                                                    <?php
                                                    echo "Kích thước"; 
                                                    ?>
                                                </div>
                                    </td>
                                    <td>
                                        <div class="menu-adv">
                                                <?php
                                                    $dem=0;
                                                    $i=0;
                                                    foreach ($option->optionValues as $optionValue) {
                                                        if($dem==10){
                                                            echo "<br/>";
                                                            $dem=0;
                                                        }
                                                ?>
                                        
                                            <span onclick="search('kichthuoc_<?php echo $i; ?>')" class="adv_khoangcach">
                                                                <i id="kichthuoc_<?php echo $i;?>" class="icon-checkbox-unchecked"></i>
                                                                <input id="kichthuocinput_<?php echo $i++;?>" class="openshop-options" type="hidden" name="optionvalue_ids[]" 
                                                                       value="<?php echo $optionValue->id; ?>" <?php if (in_array($optionValue->id, $optionValueIds)) echo 'checked="checked"'; ?>>
                                                                <?php echo $optionValue->value; ?>
                                            </span>
                                        
                                                            <?php
                                                            $dem++;
                                                            }
                                                            ?>
                                        </div>
                                         </div>
                                    </td>
                                <?php
                                
                                        }
                                    }
                                ?>
                            
		</tr>
                <?php
                }
                ?>
                <?php
                                if ($params->get('show_options', 1) && count($options))
		{
                ?>
		<tr>
                    <div class="">
                                <?php
                                
                                foreach ($options as $option) {
                                    if (count($option->optionValues) && $option->option_name == "COLOR") {
                                        ?>
                        <td>
                            <div class="ttl-left">
                                                    <?php 
                                                    
                                                    echo "Màu sắc"; 
                                                    ?>
                            </div>
                        </td>            
                                <td>
                                     <div class="menu-adv"> 
                                                <?php
                                                    $k=0;
                                                    $dem=0;
                                                    foreach ($option->optionValues as $optionValue) {
                                                        if($dem==10){
                                                            echo "<br/>";
                                                            $dem=0;
                                                        }
                                                ?>
                                                        <label onclick="setimg2('iconcolor_<?php echo $k; ?>','imgcolor_<?php echo $k; ?>')">
                                                                <i id="iconcolor_<?php echo $k; ?>" class=""></i>
                                                                <input id="iconcolorinput_<?php echo $k; ?>" class="openshop-options" type="hidden" name="optionvalue_ids[]" 
                                                                       value="<?php echo $optionValue->id; ?>" <?php if (in_array($optionValue->id, $optionValueIds)) echo 'checked="checked"'; ?>>
                                                                <img id="imgcolor_<?php echo $k++; ?>" class="img_attr_brand_img" src="<?php echo JPATH_OPENSHOP_SEARCH_ADV_IMG.OpenShopHelper::getNameImagesColor($optionValue->id) ?>"/>
                                                        </label>
                                                                <?php
                                                                    //echo $optionValue->value; 
                                                                ?>
                                                            <?php
                                                            $dem++;
                                                            }
                                                            ?>
                                     </div>
                                </td>
                                <?php
                                
                                        }
                                    }
                                ?>
                                </div>
                            
			
		</tr>
                <?php
                }
                ?>
                <tr>
                    <td colspan="3">
                        <div class="openshop-filter">
                            <div class="input-prepend">
                                <button class="btn btn-primary" name="Submit" tabindex="0" type="submit">
                                    <i class="fa fa-search"></i>
                                    <?php echo JText::_('OPENSHOP_SEARCH') ?>
                                </button>
                                &npsb;
                                <button class="btn btn-primary openshop-reset" name="Submit" tabindex="0" type="button">
                                    <i class="fa fa-refresh"></i>
                                    <?php echo JText::_('OPENSHOP_RESET_ALL') ?>
                                </button>
                                <?php
                                if ($params->get('show_length', 1) || $params->get('show_width', 1) || $params->get('show_height', 1)) {
                                    ?>
                                    <input type="hidden" value="<?php echo $params->get('same_length_unit', 1); ?>" name="same_length_unit" />
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </td>
                </tr>
	</tbody>
</table>

		
		
		
</form>



<script type="text/javascript">
	OpenShop.jQuery(function($){
		//reset button
		$('.openshop-reset').click(function(){
			<?php
			if ($params->get('show_price',1))
			{
				?>
				$("#price-behaviour").val([<?php echo $params->get( 'min_price', 0); ?>, <?php echo $params->get( 'max_price', 1000); ?>]);
				$('input[name^=min_price]').val('<?php echo  $params->get( 'min_price', 0).' '.$symbol ; ?>');
				$('input[name^=max_price]').val('<?php echo  $params->get( 'max_price', 1000).' '.$symbol; ?>');
				<?php
			}
			if ($params->get('show_weight',1))
			{
				?>
				$("#weight-behaviour").val([<?php echo $params->get( 'min_weight', 0); ?>, <?php echo $params->get( 'max_weight', 100); ?>]);
				$('input[name^=min_weight]').val('<?php echo $params->get( 'min_weight', 100) . $weightUnit; ?>');
				$('input[name^=max_weight]').val('<?php echo $params->get( 'max_weight', 100) . $weightUnit; ?>');
				<?php
			}
			if ($params->get('show_length',1))
			{
				?>
				$("#length-behaviour").val([<?php echo $params->get( 'min_length', 0); ?>, <?php echo $params->get( 'max_length', 100); ?>]);
				$('input[name^=min_length]').val('<?php echo $params->get( 'min_length', 100) . $lengthUnit; ?>');
				$('input[name^=max_length]').val('<?php echo $params->get( 'max_length', 100) . $lengthUnit; ?>');
				<?php
			}
			if ($params->get('show_width',1))
			{
				?>
				$("#width-behaviour").val([<?php echo $params->get( 'min_width', 0); ?>, <?php echo $params->get( 'max_width', 100); ?>]);
				$('input[name^=min_width]').val('<?php echo $params->get( 'min_width', 100) . $lengthUnit; ?>');
				$('input[name^=max_width]').val('<?php echo $params->get( 'max_width', 100) . $lengthUnit; ?>');
				<?php
			}
			if ($params->get('show_height',1))
			{
				?>
				$("#height-behaviour").val([<?php echo $params->get( 'min_height', 0); ?>, <?php echo $params->get( 'max_height', 100); ?>]);
				$('input[name^=min_height]').val('<?php echo $params->get( 'min_height', 100) . $lengthUnit; ?>');
				$('input[name^=max_height]').val('<?php echo $params->get( 'max_height', 100) . $lengthUnit; ?>');
				<?php
			}
			if ($params->get('show_stock',1))
			{
				?>
				$('#product_in_stock').val('2');
				<?php
			}
			if ($params->get('show_categories', 1) && count($categories))
			{
				?>
				$('input[name^=category_ids]').prop("checked", false);
				<?php
			}
			if ($params->get('show_manufacturers', 1) && count($brands))
			{
				?>
				$('input[name^=manufacturer_ids]').prop("checked", false);			
				<?php
			}
			if ($params->get('show_attributes', 1) && count($attributeGroups))
			{
				?>
				$('input[name^=attribute_ids]').prop("checked", false);		
				<?php
			}
			if ($params->get('show_options', 1) && count($options))
			{
				?>
				$('input[name^=optionvalue_ids]').prop("checked", false);	
                                $('input[name^=optionvalue_kichthuoc_ids]').prop("checked", false);
				<?php
			}
			?>
			$('input[name^=keyword]').val('');
		})
		
		
	})
	
	
	
	
</script>