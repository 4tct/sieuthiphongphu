<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<style>
    .openshop-category > ul > li > a{
        color: #55555A;
        font-size: 14px;
    }
    .openshop-category > ul > li > a:hover{
        font-size: 14px;
        color:#749C46;
    }
</style>

<div class="openshop-search<?php echo $params->get('moduleclass_sfx') ?>">
    <div class="category-title">
        <span class="menu-category">
            <?php echo JText::_('OPENSHOP_CATEGORY_TITLE') ?> <i class="icon-arrow-down-3"></i>

            <?php
            $v = JFactory::getApplication()->input->getString('view');
            if ($v != "categories" && $v != "remind") {
                ?>
                <div id="menu" class="openshop-category<?php echo $params->get('moduleclass_sfx') ?> menu-category-ul">
                    <i class="tg"><img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . 'icon_arrowup.png' ?>" /></i>
                    <ul class="menu-box">
                        <?php
                        $dem = 0;
                        foreach ($categories as $category) {
                            if ($showNumberProducts) {
                                $numberProducts = ' (' . OpenShopHelper::getNumCategoryProducts($category->id, true) . ')';
                            } else {
                                $numberProducts = '';
                            }
                            ?>
                            <li>

                                <?php
                                $active = $category->id == $parentCategoryId ? ' class="active"' : '';
                                ?>
                                <!-- female male heart briefcase-->
                                <i class="ic-vera-<?php echo $dem++; ?>"></i>
                                <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($category->id)); ?>"<?php echo $active; ?>><?php echo $category->category_name . $numberProducts; ?>

                                    <?php
                                    if ($category->childCategories) {
                                        ?>
                                        <span style="float: right;">
                                            <img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . 'icon_muiten.png' ?>" />
                                        </span>
                                    </a>
                                    <ul class="sub-menu" style="margin: -1px 0 0 15px;">
                                        <li style="z-index: 11;position: relative;margin-top: 10px;">
                                            <span class="category_sub"><?php echo $category->category_name . $numberProducts; ?></span>
                                        </li>
                                        <?php
                                        $t = 0;
                                        foreach ($category->childCategories as $childCategory) {
                                            if ($showNumberProducts) {
                                                $numberProducts = ' (' . OpenShopHelper::getNumCategoryProducts($childCategory->id, true) . ')';
                                            } else {
                                                $numberProducts = '';
                                            }
                                            ?>
                                            <li class="lihover_<?php echo (int) ( ++$t / 10) + 1 ?>">
                                                <?php
                                                $active = $childCategory->id == $childCategoryId ? 'class="active"' : '';
                                                ?>
                                                <a style="color:#5A555A" href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($childCategory->id)); ?>" <?php echo $active; ?>> <i class="icon-rightarrow" style="margin: 3px 0 0 0;font-size: 12px;"></i> <span style="margin: 0 0 0 0px;"><?php echo $childCategory->category_name . $numberProducts; ?></span></a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li class="img-category">
                                            <?php
                                            if (JFile::exists(JPATH_ROOT . '/images/com_openshop/categories/' . $category->category_image)) {
                                                $img = JUri::root() . '/images/com_openshop/categories/' . $category->category_image;
                                            } else if (JFile::exists(JPATH_ROOT . '/images/com_openshop/categories/resized/' . $category->category_image)) {
                                                $img = JUri::root() . '/images/com_openshop/categories/resized/' . $category->category_image;
                                            }
                                            ?>
                                            <img src="<?php echo $img ?>"
                                        </li>
                                    </ul>
                                    <?php
                                } else {
                                    echo '</a>';
                                }
                                ?>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <?php
            }
            ?>
        </span>
    </div>
    <div class="input-prepend">
        <input class="inputbox product_search" type="text" name="keyword" id="prependedInput" value="" placeholder="Tìm kiếm sản phẩm ...">
        <span class="btn" id="btn-search" onclick="this.form.submit();">Tìm kiếm</span>
    </div>
    <ul id="openshop_result"></ul>
    <input type="hidden" name="live_site" id="live_site" value="<?php echo JURI::root(); ?>">
    <input type="hidden" name="image_width" id="image_width" value="<?php echo $params->get('image_width') ?>">
    <input type="hidden" name="image_height" id="image_height" value="<?php echo $params->get('image_height') ?>">
    <input type="hidden" name="category_ids" id="category_ids" value="<?php echo $params->get('category_ids') ? implode(',', $params->get('category_ids')) : ''; ?>">
    <input type="hidden" name="description_max_chars" id="description_max_chars" value="<?php echo $params->get('description_max_chars', 50); ?>">
</div>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('#openshop_result').hide();
            $('input.product_search').val('');
            $(window).click(function () {
                $('#openshop_result').hide();
            });
            function search() {
                var query_value = $('input.product_search').val();
                $('b#search-string').html(query_value);
                if (query_value !== '') {
//                    $('.product_search').addClass('openshop-loadding');
                    $.ajax({
                        type: "POST",
                        url: $('#live_site').val() + "index.php?option=com_openshop&view=search&format=raw&layout=ajax<?php echo OpenShopHelper::getAttachedLangLink(); ?>",
                        data: '&keyword=' + query_value + '&image_width=' + $('#image_width').val() + '&image_height=' + $('#image_height').val() + '&category_ids=' + $('#category_ids').val() + '&description_max_chars=' + $('#description_max_chars').val(),
                        cache: false,
                        success: function (html) {
                            $("ul#openshop_result").html(html);
                            $('.product_search').removeClass('openshop-loadding');
                        }
                    });
                }
                return false;
            }

            $("input.product_search").live("keyup", function (e) {
                //Set Timeout
                clearTimeout($.data(this, 'timer'));
                // Set Search String
                var search_string = $(this).val();
                // Do Search
                if (search_string == '') {
                    $('.product_search').removeClass('openshop-loadding');
                    $("ul#openshop_result").slideUp();
                } else {
                    $("ul#openshop_result").slideDown('slow');
                    $(this).data('timer', setTimeout(search, 100));
                }
                ;
            });

        });
    })(jQuery);
</script>

<style>
    #openshop_result
    {
        background-color:#ffffff;
        width:<?php echo $params->get('width_result', 270) ?>px;
        position:absolute;
        z-index:9999;
        margin-left: 15%;
    }
</style>