<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<style>
    .openshop-category > ul > li > a{
        color: #55555A;
        font-size: 14px;
    }
    .openshop-category > ul > li > a:hover{
        font-size: 14px;
        color:#749C46;
    }
</style>
<div id="menu" class="openshop-category<?php echo $params->get('moduleclass_sfx') ?>">
    <ul id="menu-box">
        <?php
        $dem = 0;
        foreach ($categories as $category) {
            if ($showNumberProducts) {
                $numberProducts = ' (' . OpenShopHelper::getNumCategoryProducts($category->id, true) . ')';
            } else {
                $numberProducts = '';
            }
            ?>
            <li>

                <?php
                $active = $category->id == $parentCategoryId ? ' class="active"' : '';
                ?>
                <!-- female male heart briefcase-->
                <i class="ic-vera-<?php echo $dem++; ?>"></i>
                <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($category->id)); ?>"<?php echo $active; ?>><?php echo $category->category_name . $numberProducts; ?>
                </a>
                <i class="ic-vera-muiten"></i>
                <?php
                if ($showChildren && $category->childCategories) {
                    ?>
                    <ul class="sub-menu">
                        <li style="z-index: 11;position: relative;">
                            <span class="category_sub"><?php echo $category->category_name . $numberProducts; ?></span>
                        </li>
                        <?php
                        $t = 0;
//                        print_r($category->childCategories);
                        foreach ($category->childCategories as $childCategory) {
                            if ($showNumberProducts) {
                                $numberProducts = ' (' . OpenShopHelper::getNumCategoryProducts($childCategory->id, true) . ')';
                            } else {
                                $numberProducts = '';
                            }
                            ?>
                            <li class="lihover_<?php echo (int) ( ++$t / 10) + 1 ?>">
                                <?php
                                $active = $childCategory->id == $childCategoryId ? 'class="active"' : '';
                                ?>
                                <a style="color:#5A555A" href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($childCategory->id)); ?>" <?php echo $active; ?>> <i class="icon-rightarrow" style="margin: 3px 0 0 0;font-size: 12px;"></i> <span style="margin: 0 0 0 17px;"><?php echo $childCategory->category_name . $numberProducts; ?></span></a>
                            </li>
                            <?php
                        }
                        ?>
                        <li class="img-category">
                            <?php
                            if(JFile::exists(JPATH_ROOT.'/images/com_openshop/categories/' . $category->category_image)){
                                $img = JUri::root().'/images/com_openshop/categories/' . $category->category_image;
                            }
                            else if(JFile::exists(JPATH_ROOT.'/images/com_openshop/categories/resized/' . $category->category_image)){
                                $img = JUri::root().'/images/com_openshop/categories/resized/' . $category->category_image;
                            }
                            ?>
                            <img src="<?php echo $img ?>"
                        </li>
                    </ul>
                    <?php
                }
                ?>
            </li>
            <?php
        }
        ?>
    </ul>
</div>