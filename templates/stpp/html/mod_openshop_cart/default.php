<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

if (!OpenShopHelper::getConfigValue('catalog_mode')) {
    ?>
    <div id="openshop-cart" class="openshop-cart<?php echo $params->get('moduleclass_sfx'); ?>">
        <div class="openshop-items">
            <a href="index.php?option=com_openshop&view=cart">
                <span id="openshop-cart-total">
                    <?php echo $total['total_quantity'] ?>
                    <i class="icon-cart"></i>
                    <?php
                    if (OpenShopHelper::showPrice()) {
                        ?>
                        &nbsp;&nbsp;<?php echo number_format($total['total_price'], 0, ',', '.'); ?> <sup>đ</sup>
                        <?php
                    }
                    ?>
                </span>
            </a>
        </div>
    </div>
    <?php
}
