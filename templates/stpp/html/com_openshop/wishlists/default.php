<?php
defined('_JEXEC') or die;
$session = JFactory::getSession();
?>
<style>
    .dplay {
        display: inline-block;
    }
    .money-product {
        display: inline-block;
    }
    .money-product .content-money-product .percent-sale-off {
        display: inline-block;
    }
    .money-product .content-money-product .money-sell-saleoff {
        display: inline-block;
    }
    .money-product .content-money-product .percent-sale-off {
        font-size: 35px;
        font-weight: 700;
        color: #d46a0b;
    }
    .money-product .content-money-product .money-sell-saleoff .sell {
        font-size: 11px;
        color: #b1a9a9;
        text-decoration: line-through;
    }
    .money-product .content-money-product .money-sell-saleoff .saleoff {
        font-size: 17.5px;
        color: #739c42;
        line-height: 10px;
        font-weight: 700;
    }
    .table-responsive {
        text-align: center;
    }
    .table-responsive .th-table {
        font-weight: bold;
        color: white;
        background-color: #2E6DA4;
    }
    .table-bordered{
        margin:auto;
    }
    .table-responsive{
        padding: 15px 0;
    }
    table.tableWL tbody tr td{
        vertical-align: middle;
    }
    table.tableWL thead{
        background: #2E6DA4;
        color: white;
    }
</style>

<?php
if (!empty($this->wishlists)) {
    ?>
    <div class="table-responsive">
        <table class="table table-bordered tableWL" class="container-fluid" style="width: 100%;">
            <thead>
                <tr>
                    <th class="text-center col-lg-1">
                        #
                    </th>
                    <th class="text-center col-lg-3">
                        Tên sản phẩm
                    </th>
                    <th class="text-center col-lg-2">
                        Hình ảnh
                    </th>
                    <th class="text-center col-lg-2">
                        Size
                    </th>
                    <th class="text-center col-lg-2">
                        Giá
                    </th>
                    <th class="text-center col-lg-2">
                        Xem chi tiết
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($this->wishlists as $key => $value) {
                    $cartID = OpenShopHelper::getProductCategory($value->id);
                    $urlP = JRoute::_(OpenShopRoute::getProductRoute($value->id, $cartID));
                    ?>
                    <tr>
                        <td><?php echo $key + 1 ?></td>
                        <td>
                            <?php echo $value->product_name ?>
                        </td>
                        <td>
                            <?php
                            ?>
                            <img class="lazy" data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $value->product_image ?>" style="height: 100px;" title="<?php echo $value->product_name ?>" alt="<?php echo $value->product_name ?>">
                        </td>
                        <td>
                            <?php
                            foreach ($this->getSizeProduct($value->id) as $s) {
                                ?>
                                <button type="button" class="btn  btn-primary btn-md">
                                    <?php echo $s->value ?>
                                </button>
                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <div class="money-product">
                                <?php
                                if ($value->product_price_r != '0') {
                                    ?>
                                    <span class="content-money-product">
                                        <div class="percent-sale-off">
                                            <?php echo 100 - (int) ((int) ($value->product_price * 100) / (int) $value->product_price_r) . '%' ?>
                                        </div>
                                        <div class="money-sell-saleoff">
                                            <div class="saleoff">
                                                <?php echo (int) $value->product_price ?><sup>đ</sup>	
                                            </div>
                                            <div class="sell">
                                                <?php echo (int) $value->product_price_r ?><sup>đ</sup>	
                                            </div>
                                        </div>
                                    </span>
                                    <?php
                                }
                                ?>
                            </div>
                        </td>
                        <td>
                            <a href="<?php echo $urlP ?>">Xem chi tiết</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php
} else {
    ?>
    <div style="text-align: center; padding: 80px 0 100px 0; font-style: italic;">Hiện chưa có sản phẩm yêu thích</div>
    <?php
}
?>



