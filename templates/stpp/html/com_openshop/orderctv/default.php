<?php
defined('_JEXEC') or die;
?>
<style>
    .pop-up-C {
        height: 220px;
        width: 600px;
        background-color: rgba(101, 101, 101, 0.05);;
        padding: 0;
        margin: 0;
        display:block;
    }
    .logo4T .content-logo4T {
        margin: auto;
        width: 540px;
    }
    .logo4T .content-logo4T .text4T {
        color: red;
        font-size: 40px;
        font-weight: bold;
    }
    .pop-up-C .confirm .content-confirm {
        font-size: 25px;
        color: #5c93f0;
        margin: auto;
        width: 410px;
    }
    .pop-up-C .input {
        width: 45%;
        height: 35px;
        margin: 2%;
        float: left;
    }
    .pop-up-C .input:hover {
        color: red;
    }
    .pop-up-C .input .input-logo {
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 14%;
        background-color: #5391f9;
        border-radius: 4px 0 0 4px;
        font-size: 22px;
        text-align: center;
        vertical-align: middle;
        border-style: solid none solid solid;
        border-width: 1px;
        border-color: #cfcfcf;
    }
    .pop-up-C .input .input-logo .logo i {
        padding-top: 5px;
        color: white;
    }
    .pop-up-C .input .input-text {
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 80%;
        background-color: white;
        border-radius: 0 4px 4px 0;
        border-style: solid solid solid none;
        border-width: 1px;
        border-color: #cfcfcf;
        padding-left: 5%;
        color: #748096;
    }
    .pop-up-C .input .input-select {
        border-style: solid solid solid none;
        border-width: 1px;
        border-color: #cfcfcf;
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 80%;
        background-color: white;
        border-radius: 0 0px 0px 0;
        color: #748096;
    }
    .pop-up-C .input .input-select option {
        height: 35px;
    }
    .pop-up-C .input .clearfix {
        clear: both;
    }
    .pop-up-C .all-button {
        clear: left;
        margin: 0% 3%;
    }
    .pop-up-C .all-button .button {
        width: 130px;
        height: 35px;
        background-color: #5c93f0;
        margin: 5% 2%;
        font-size: 20px;
        float: right;
    }
    .pop-up-C .all-button .button .content-button {
        height: 20px;
        margin: auto;
        padding: 5px 0;
        color: white;
        text-align: center;
    }
    .pop-up-C .all-button .button .content-button {
        height: 20px;
        margin: auto;
        padding: 5px 0;
    }
    .pop-up-C .all-button .button:hover {
        background-color: #0f46a4;
        cursor: pointer;
    }

    /*# sourceMappingURL=form_input.css.map */

    .product-information {
        width: 90%;
        padding: 2% 5%;
    }
    .product-information .search-box {
        width: 100%;
        padding: 15px 25%;
    }
    .product-information .search-box .input-text {
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 80%;
        background-color: white;
        border-radius: 0 0px 0px 0;
        border-style: solid none solid solid;
        border-width: 1px;
        border-color: #cfcfcf;
        color: #748096;
    }
    .product-information .search-box .input-logo {
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 14%;
        background-color: #5391f9;
        border-radius: 0px 0 0 0px;
        font-size: 20px;
        text-align: center;
        vertical-align: middle;
        border-style: solid solid solid none;
        border-width: 1px;
        border-color: #cfcfcf;
    }
    .product-information .search-box .input-logo .logo i {
        padding-top: 5px;
        color: white;
    }
    .product-information .search-box .input-logo:hover {
        background-color: #0f46a4;
        cursor: pointer;
    }
    .product-information .search-box .clearfix {
        clear: both;
    }
    .product-information img {
        float: left;
        left: 12%;
        position: relative;
        margin: 30px;
    }
    .product-information .content-product-information {
        float: right;
        position: relative;
    }
    .product-information .content-product-information .name-of-prduct {
        background-color: #5391F9;
        color: white;
        width: 350px;
        margin: 30px;
        font-size: 20px;
        text-align: center;
    }
    .product-information .content-product-information .name-of-prduct .text-name-of-prduct {
        padding: 10px 0;
    }
    .product-information .content-product-information .price-of-prduct {
        background-color: #5391F9;
        color: white;
        width: 350px;
        margin: 30px;
        font-size: 20px;
        text-align: center;
        font-size: 35px;
    }
    .product-information .content-product-information .price-of-prduct .text-name-of-prduct {
        padding: 10px 0;
    }
    .product-information .content-product-information .price-of-prduct .text-price-of-prduct {

        padding: 10px 0;
    }
    .product-information .content-product-information .size-of-product {
        margin: 30px;
        height: 35px;
    }
    .product-information .content-product-information .size-of-product .input-logo {
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 14%;
        background-color: #5391f9;
        border-radius: 0px 0 0 0px;
        font-size: 22px;
        text-align: center;
        vertical-align: middle;
        border-style: solid none solid solid;
        border-width: 1px;
        border-color: #cfcfcf;
    }
    .product-information .content-product-information .size-of-product .input-logo .logo i {
        padding-top: 5px;
        color: white;
    }
    .product-information .content-product-information .size-of-product .input-select {
        border-style: solid solid solid none;
        border-width: 1px;
        border-color: #cfcfcf;
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 85%;
        background-color: white;
        border-radius: 0 0px 0px 0;
        color: #748096;
    }
    .product-information .content-product-information .size-of-product .input-select option {
        height: 35px;
    }
    .product-information .content-product-information .number-of-product {
        margin: 30px;
        height: 35px;
    }
    .product-information .content-product-information .number-of-product .input-logo {
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 14%;
        background-color: #5391f9;
        border-radius: 0px 0 0 0px;
        font-size: 22px;
        text-align: center;
        vertical-align: middle;
        border-style: solid none solid solid;
        border-width: 1px;
        border-color: #cfcfcf;
    }
    .product-information .content-product-information .number-of-product .input-logo .logo i {
        padding-top: 5px;
        color: white;
    }
    .product-information .content-product-information .number-of-product .input-select {
        border-style: solid solid solid none;
        border-width: 1px;
        border-color: #cfcfcf;
        display: inline;
        float: left;
        padding: 0px;
        margin: 0px;
        outline: 0px;
        height: 35px;
        width: 85%;
        background-color: white;
        border-radius: 0 0px 0px 0;
        color: #748096;
    }
    .product-information .content-product-information .number-of-product .input-select option {
        height: 35px;
    }
    .product-information .content-product-information .all-button {
        clear: left;
        margin: 30px;
        height: 50px;
    }
    .product-information .content-product-information .all-button .button {
        width: 157px;
        height: 50px;
        background-color: #5c93f0;
        font-size: 22px;
        margin: 25px 15px 0px 25px;
        font-size: 20px;
        float: right;
        margin: 10px;
    }
    .product-information .content-product-information .all-button .button .content-button {
        height: 20px;
        margin: auto;
        padding: 10px 0;
        color: white;
        text-align: center;
    }
    .product-information .content-product-information .all-button .button .content-button {
        height: 20px;
        margin: auto;
        padding: 10px 0;
    }
    .product-information .content-product-information .all-button .button:hover {
        background-color: #0f46a4;
        cursor: pointer;
    }

    /*# sourceMappingURL=product-information.css.map */
</style>

<div class="product-information">
    <div class="search-box">
        <input type="text" name="" class="input-text" placeholder="   Nhập mã sản phẩm">
        <div class="input-logo">
            <div class="logo">
                <i class="fa fa-search" aria-hidden="true"></i>
            </div>
        </div>
        <div class="clearfix">

        </div>
    </div>
    <img src="<?php echo JUri::base() . 'components/com_openshop/assets/images/image.jpg' ?>" />
    <div class="content-product-information">
        <div class="name-of-prduct">
            <div class="text-name-of-prduct">
                Đây là tên sản phẩm, nó phải dài như thế này này haha.
            </div>
        </div>
        <div class="price-of-prduct">
            <div class="text-price-of-prduct">
                100.000đ
            </div>	
        </div>
        <div class="size-of-product">
            <div class="input-logo">
                <div class="logo">
                    <i class="fa fa-child" aria-hidden="true"></i>
                </div>
            </div>
            <select class="input-select">
                <option>
                    Size
                </option>
                <option>
                    S
                </option>
                <option>
                    M
                </option>
                <option>
                    L
                </option>
            </select>
            <div class="clearfix">

            </div>
        </div>
        <div class="number-of-product">
            <div class="input-logo">
                <div class="logo">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </div>
            </div>
            <select class="input-select">
                <option>
                    Nhập Số Lượng
                </option>
                <option>
                    1
                </option>
                <option>
                    2
                </option>
                <option>
                    3
                </option>
            </select>
            <div class="clearfix">

            </div>
        </div>
        <div class="all-button">
            <div class="button" onclick="giaochokhach()">
                <div class="content-button">
                    Giao Cho Khách
                </div>
            </div>
            <div class="button">
                <div class="content-button">
                    Giao Cho Tôi
                </div>
            </div>
            <div class="clearfix">

            </div>
        </div>		
    </div>
    <div class="clearfix"></div>
</div>


<div class="modal fade" id="GiaoChoKhach" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="logo4T">
                    <div class="content-logo4T">
                        <img src="<?php echo JUri::base() . 'components/com_openshop/assets/images/logo4T.png' ?>" />
                        <span class="text4T"> Thời Trang Tích Tắc</span>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="pop-up-C">
                    <div class="confirm">
                        <div class="content-confirm">
                            Xác Nhận Thông Tin khách Hàng
                        </div>
                    </div>
                    <br>
                    <div class="input">
                        <div class="input-logo">
                            <div class="logo">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </div>
                        </div>
                        <input type="text" name="" class="input-text" placeholder="Tên Khách Hàng">
                        <div class="clearfix"></div>
                    </div>
                    <div class="input">
                        <div class="input-logo">
                            <div class="logo">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                        </div>
                        <input type="text" name="" class="input-text" placeholder="Số Điện Thoại">
                        <div class="clearfix"></div>
                    </div>
                    <div class="input">
                        <div class="input-logo">
                            <div class="logo">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                        </div>
                        <input type="text" name="" class="input-text" placeholder="Email">
                        <div class="clearfix"></div>
                    </div>
                    <div class="input">
                        <div class="input-logo">
                            <div class="logo">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </div>
                        </div>
                        <input type="text" name="" class="input-text" placeholder="Nhập địa chỉ gồm: số nhà, đường...">
                        <div class="clearfix"></div>
                    </div>
                    <div class="input">
                        <div class="input-logo">
                            <div class="logo">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                            </div>
                        </div>
                        <select class="input-select">
                            <option>
                                Tỉnh
                            </option>
                            <option>
                                a
                            </option>
                            <option>
                                a
                            </option>
                            <option>
                                a
                            </option>
                        </select>
                        <div class="clearfix"></div>
                    </div>
                    <div class="input">
                        <div class="input-logo">
                            <div class="logo">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                            </div>
                        </div>
                        <select class="input-select">
                            <option>
                                Huyện
                            </option>
                            <option>
                                a
                            </option>
                            <option>
                                a
                            </option>
                            <option>
                                a
                            </option>
                        </select>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary">Xác nhận</button>
            </div>
        </div>
    </div>
</div>

<script>
    function giaochokhach() {
        $('#GiaoChoKhach').modal();
    }
</script>