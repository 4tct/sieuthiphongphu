<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$span = intval(12 / $subCategoriesPerRow);
?>
<div class="row-fluid">
    <?php
    if (OpenShopHelper::getConfigValue('sub_categories_layout') == 'list_with_only_link') {
        ?>
        <h4><?php echo JText::_('OPENSHOP_REFINE_SEARCH'); ?></h4>
        <ul>
            <?php
            foreach ($subCategories as $subCategory) {
                ?>
                <li>
                    <h5>
                        <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($subCategory->id)); ?>">
        <?php echo $subCategory->category_name; ?>
                        </a>
                    </h5>
                </li>
        <?php
    }
    ?> 
        </ul>
            <?php
        } else {
            $count = 0;
            foreach ($subCategories as $subCategory) {
                $subCategoryUrl = JRoute::_(OpenShopRoute::getCategoryRoute($subCategory->id));
                ?>
            <div class="span<?php echo $span; ?>">
                <div class="openshop-category-wrap">
                    <div class="image">
                        <a href="<?php echo $subCategoryUrl; ?>" title="<?php echo $subCategory->category_page_title != '' ? $subCategory->category_page_title : $subCategory->category_name; ?>">
                            <img src="<?php echo $subCategory->image; ?>" alt="<?php echo $subCategory->category_page_title != '' ? $subCategory->category_page_title : $subCategory->category_name; ?>" />	            
                        </a>
                    </div>
                    <div class="openshop-info-block">
                        <h5>
                            <a href="<?php echo $subCategoryUrl; ?>" title="<?php echo $subCategory->category_page_title != '' ? $subCategory->category_page_title : $subCategory->category_name; ?>">
        <?php echo $subCategory->category_name; ?>
                            </a>
                        </h5>
                    </div>
                </div>	
            </div>
        <?php
        $count++;
        if ($count % $subCategoriesPerRow == 0 && $count < count($subCategories)) {
            ?>
            </div><div class="row-fluid">
                <?php
            }
        }
    }
    ?>
</div>
<hr />