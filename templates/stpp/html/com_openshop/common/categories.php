<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<style>
    .showCategories{
        padding: 20px 0 0 0;
    }
    .detailC .col-md-3{
        padding: 5px;
    }
    .nameC ul li{
        list-style: none;
        padding: 0 0 10px 0;
    }
    .nameC ul li a{
        color: #565656;
        cursor: pointer;
        text-decoration: none;
    }
    .upperC{
        font-weight: bold;
        text-transform: uppercase;
    }
    .cot{
        display: inline-table;
    }
    .nameC{
        padding: 10px 10px 0 10px;
    }
    .nameC ul li:hover a{
        color:#6CC550;
    }
</style>

<div class="clr"></div>

<div class="col-md-12" style="padding:0;">
    <div class="showCategories col-md-9">
        <div class="titleC titlePH">
            <a class="tPH">Loại sản phẩm</a>
        </div>
        <div class="detailC">
            <?php
            $br = 1;
            foreach ($categories as $category) {
                $categoryUrl = JRoute::_(OpenShopRoute::getCategoryRoute($category->id));
                if ($br == 1) {
                    echo '<div class="cot">';
                }
                ?>
                <div class="col-sm-4 col-md-3">
                    <img src="<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $category->category_image; ?>" alt="<?php echo $category->category_name ?>" width="100%" height="100px"/>
                    <div class="nameC">
                        <ul>
                            <li class="upperC">
                                <a href="<?php echo $categoryUrl ?>">
                                    <?php
                                    echo $category->category_name;
                                    ?>
                                </a>
                            </li>
                            <?php
                            $db = JFactory::getDbo();
                            $query = $db->getQuery(TRUE);
                            $query->select('*')
                                    ->from($db->quoteName('#__openshop_categories', 'a'))
                                    ->join('INNER', $db->quoteName('#__openshop_categorydetails', 'b') . 'ON a.id = b.category_id')
                                    ->where('a.category_parent_id = ' . $category->id);
                            $ccs = $db->setQuery($query)->loadObjectList();

                            foreach ($ccs as $c) {
                                $categoryUrlC = JRoute::_(OpenShopRoute::getCategoryRoute($category->id));
                                echo '<li><a href="' . $categoryUrlC . '">' . $c->category_name . '</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php
                if ($br > 3) {
                    echo '</div>';
                    $br = 1;
                } else {
                    ++$br;
                }
            }
            if ($br != 3) {
                echo '</div>';
            }
            ?>
        </div>
    </div>

    <div class="col-md-3" style="padding-top: 20px; padding-right: 0;">
        <?php
        $modules = JModuleHelper::getModules('nelo-tags');
        foreach ($modules as $module) {
            echo JModuleHelper::renderModule($module);
        }
        ?>
    </div>
</div>
