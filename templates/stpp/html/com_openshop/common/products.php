<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$user = JFactory::getSession()->get('user');
JFactory::getDocument(true)->addStyleSheet(JUri::base() . 'components/com_openshop/assets/css/list.css');
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
?>

<style>
    .openshop-products-list{
        background-image: url(<?php echo JUri::base() ?>images/patern/background-content1.png);
        box-shadow: 0px 0px 10px 0px #b3b3b3;
        border-radius: 0px 0px 5px 5px;
    }
    .search-categories{
        width: 100%;
        background-image: url(<?php echo JUri::base() ?>images/patern/background-left1.png);
        border: 1px solid #dedede;
        box-shadow: 1px 1px 5px 0px #dedede;
    }
</style>

<div id="products-list-container" class="products-list-container block list">
    <!--    <div class="sortPagiBar row-fluid">
            <div class="span3" style="text-align: right;">
                <div class="btn-group hidden-phone">
    <?php
    $active_grid = '';
    $active_list = '';
    if (OpenShopHelper::getConfigValue('default_products_layout') == 'grid') {
        $active_grid = "btn-chose-active";
    } else {
        $active_list = "btn-chose-active";
    }
    ?>
                    <a rel="grid" href="#" class="btn-chose <?php echo $active_grid ?>" style="padding: 10px;"><i class="fa fa-th-large"></i></a>
                    <a rel="list" href="#" class="btn-chose <?php echo $active_list ?>"><i class="fa fa-list"></i></a>
                </div>
            </div>
    <?php
    if ($showSortOptions) {
        ?>
                                                    <div class="span9">
                                                        <form method="post" name="adminForm" id="adminForm" action="<?php echo $actionUrl; ?>">
                                                            <div class="clearfix">
                                                                <div class="openshop-product-show">
                                                                    <b><?php echo JText::_('OPENSHOP_SHOW'); ?>: </b>
        <?php echo $pagination->getLimitBox(); ?>
                                                                </div>
        <?php
        if ($sort_options) {
            ?>
                                                                                                        <div class="openshop-product-sorting">
                                                                                                            <b><?php echo JText::_('OPENSHOP_SORTING_BY'); ?>: </b>
            <?php echo $sort_options; ?>
                                                                                                        </div>
            <?php
        }
        ?>
                                                            </div>
                                                        </form> 
                                                    </div>
        <?php
    }
    ?>
        </div>-->
    <div id="products-list" class="products-list">
        <!--//menu categories-->
<!--        <div class="col-md-2 colMenuC">
            <div class="search-categories">
                <div class="title-categories">Danh mục</div>
                <div class="allcategories">
                    <ul>
                        <?php
                        $db = JFactory::getDbo();
                        $query = $db->getQuery(TRUE);
                        $query->select('a.id, b.category_name AS title, a.category_parent_id AS parent_id')
                                ->from($db->quoteName('#__openshop_categories', 'a'))
                                ->join('INNER', $db->quoteName('#__openshop_categorydetails', 'b') . 'ON a.id = b.category_id')
                                ->where('a.published = 1');
                        $rowsC = $db->setQuery($query)->loadObjectList();
                        $children = array();
                        if ($rowsC) {
                            // first pass - collect children
                            foreach ($rowsC as $v) {
                                $pt = $v->parent_id;
                                $list = @$children[$pt] ? $children[$pt] : array();
                                array_push($list, $v);
                                $children[$pt] = $list;
                            }
                        }
                        $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
                        $options = array();
                        foreach ($list as $listItem) {
                            $options[] = JHtml::_('select.option', $listItem->id, $listItem->treename);
                        }

                        foreach ($options as $c) {
                            $categoryUrl = JRoute::_(OpenShopRoute::getCategoryRoute($c->value));
                            ?>
                            <li>
                                <a href="<?php echo $categoryUrl ?>"><?php echo $c->text ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>-->

        <!--//show products-->
        <div class="col-md-12 <?php echo OpenShopHelper::getConfigValue('default_products_layout') ?> colShowPro showTabContentT2" id="contentShowProduct" style="padding-left: 15px;">
            <div class="content_show_product_tabT2 tab1_T2">
                <div class="col-md-12 padding0">
                    <?php
                    if ($view != '' && $view != 'frontpage') {
                        ?>
                        <div class="clr"></div>
                        <div class="nelo-breadcrumb">
                            <?php
                            $modules = JModuleHelper::getModules('breadcrumb');
                            foreach ($modules as $module) {
                                echo JModuleHelper::renderModule($module);
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-12" id="contentLoadT2">
                    <?php
                    if (count($products)) {
                        foreach ($products as $ct2) {
                            $link = JRoute::_(OpenShopRoute::getProductRoute($ct2->id, OpenShopHelper::getProductCategory($ct2->id)));
                            ?>
                            <div class="col-sm-4 col-md-3 b_t_2 b_t_t_2">
                                <div class="borderContentProduct">
                                    <!--<div class="infoImgT2" onclick="location.href = '<?php echo $link ?>'">-->
                                    <div class="infoImgT2" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ct2->id) ?>', '<?php echo $link ?>')" >
                                        <img data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ct2->product_image ?>" class="img-responsive lazy" title="<?php echo $ct2->product_name ?>" alt="<?php echo $ct2->product_name ?>"/>
                                        <div class="infoChildProT2">
                                            <div class="pricePro1">
                                                <?php
                                                echo number_format($ct2->product_price, 0, ',', '.');
                                                echo '<sup>đ</sup>';
                                                if ($ct2->product_price_r != 0) {
                                                    echo '<span class="pricenotbuy">';
                                                    echo number_format($ct2->product_price_r, 0, ',', '.');
                                                    echo '<sup>đ</sup>';
                                                    echo '</span>';
                                                }
                                                ?>
                                            </div>
                                            <div>
                                                Kích thước: 
                                                <?php
                                                $sizes = OpenShopHelper::getProductOptionValueDetails($ct2->id, 'size');
                                                foreach ($sizes as $k => $s) {
                                                    echo $s->value;
                                                    if ($k != count($sizes) - 1) {
                                                        echo ' - ';
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <!--<div>Màu sắc: Xanh</div>-->
                                        </div>

                                        <?php
                                        if ($ct2->product_price_r > 0) {
                                            $percent = 100 - (int) (((int) $ct2->product_price * 100) / $ct2->product_price_r)
                                            ?>
                                            <div class="percentSale rightPercent2"> 
                                                <?php echo $percent ?>%
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="namePro">
                                        <!--<a href="<?php echo $link ?>">-->
                                        <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ct2->id) ?>', '<?php echo $link ?>')" >
                                            <?php echo $ct2->product_name ?>
                                        </a>
                                    </div> 
                                    <div class="codePro">
                                        Mã: <strong><?php echo $ct2->product_sku ?></strong>
                                    </div>
                                    <div class="pricePro">
                                        <?php
                                        echo number_format($ct2->product_price, 0, ',', '.');
                                        echo '<sup>đ</sup>';
                                        if ($ct2->product_price_r != 0) {
                                            echo '<span class="pricenotbuy">';
                                            echo number_format($ct2->product_price_r, 0, ',', '.');
                                            echo '<sup>đ</sup>';
                                            echo '</span>';
                                        }
                                        ?>
                                    </div>
                                    <div class="btn-buy">
                                        <a href="<?php echo $link ?>">
                                            <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ct2->id) ?>', '<?php echo $link ?>')" >
                                            Mua ngay
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="ct2_not_pro">
                            Hiện chưa có sản phẩm
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="clr"></div>

            <!--//pagination default-->
            <?php
            if (isset($pagination) && ($pagination->total > $pagination->limit)) {
                ?>
                <div class="row-fluid">
                    <div class="pagination">
                        <?php echo $pagination->getPagesLinks(); ?>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>

    </div>
</div>


<div class = "modal fade chooseSizeCP form-horizontal" tabindex = "-1" role = "dialog" aria-labelledby = "mySmallModalLabel">
    <div class = "modal-dialog modal-sm" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
                <h4 class = "modal-title" id = "myModalLabel">Chọn Size</h4>
            </div>
            <div class = "modal-body">
                <label>Chọn size</label>
                <div class="contentSizeCP">
                    <!--//size-->
                </div>
            </div>
            <div class = "modal-footer">
                <button type = "button" class = "btn btn-primary addCP" onclick="">Thêm vào giỏ hàng</button>
            </div>
        </div>
    </div>
</div>

<script>
    if (jQuery(document).width() <= 540) {
        jQuery('.productD').removeClass('col-xs-6');
    }

    if (jQuery(document).width() < 1024) {
        jQuery('.colMenuC').css('display', 'none');
        jQuery('.colShowPro').addClass('col-md-12');
        jQuery('.colShowPro').removeClass('col-md-10');
    } else
    {
        jQuery('.colMenuC').css('display', 'block');
        jQuery('.colShowPro').addClass('col-md-10');
        jQuery('.colShowPro').removeClass('col-md-12');
    }


    jQuery(window).resize(function () {
        if (jQuery(this).width() <= 450) {
            jQuery('.productD').removeClass('col-xs-6');
        } else
        {
            jQuery('.productD').addClass('col-xs-6');
        }
    });

</script>

<div class="clr"></div>