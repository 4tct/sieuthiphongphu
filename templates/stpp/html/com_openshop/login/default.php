<?php
defined('_JEXEC') or die;
$session = JFactory::getSession();
$user = $session->get('user');
?>
<div class="loginJ">
    <?php
    if (!empty($user->id)) {
        ?>
        <div class="col-md-12" style="padding: 100px 0;text-align: center;">
            <div class="">
                <i>Bạn đã đăng nhập</i>
            </div>
            <div>
                <a href="index.php">
                    <span>Quay lại trang chủ</span>
                </a>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="col-sm-8 col-md-8">
            <div class="col-md-6 col-md-offset-3">
                <div class="titleL">
                    Đăng nhập
                </div>
                <form action="index.php?option=com_users&task=user.login" method="post" class="form-horizontal formLoginJ">
                    <?php
                    if ($session->get('errorLogin')) {
                        ?>
                        <div class="errorLogin">
                            <i class="fa fa-remove"></i><span> Tên tài khoản hoặc mật khẩu không đúng</span>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="contentJ">
                        <div class="usernameJ form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="fa fa-user"></span></div>
                                <input type="text" class="form-control" name="username" id="exampleInputAmount" placeholder="Tên tài khoản">
                            </div>
                        </div>
                        <div class="passwordJ form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="fa fa-key"></span></div>
                                <input type="password" name="password" class="form-control" id="exampleInputAmount" placeholder="Mật khẩu">
                            </div>
                        </div>
                        <div class="btnForm">
                            <button type="submit" class="btn btn-primary">Đăng nhập</button>
                            <input type="hidden" name="return" id="btl-return" value="">
                            <?php echo JHtml::_('form.token'); ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-4 col-md-4">
            <div class="Jregistr" onclick="showSignIn()">
                Đăng ký
            </div>
        </div>
        <?php
    }
    ?>

</div>

<div class="clr"></div>



<style>
    .input_animation_deg{
        position: relative;
    }
    .input_animation_deg .label_animation_deg{
        display: inline-block;
        position: absolute;
        background: #0a8be6;
        padding: 6px 25px;
        border: 1px solid #0a8be6;
        color: white;
        transform: rotate3d(0,0,0,60deg);
        -webkit-transform: rotate3d(0,0,0,60deg);
        -moz-transform: rotate3d(0,0,0,60deg);
        transition: all .4s ease-in-out;
        -webkit-transition: all .4s ease-in-out;
        -moz-transition: all .4s ease-in-out;
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
        transform-origin: left bottom;
    }
    .input_animation_deg .input_act input[type=text], .input_animation_deg .input_act input[type=password]{
        padding: 5px 5px 5px 20px;
        width: 100%;
        transition: all .4s ease-in-out;
        -webkit-transition: all .4s ease-in-out;
        -moz-transition: all .4s ease-in-out;
    }

    /*//placeholder*/

    .input_animation_placeholder input[type=text], .input_animation_placeholder input[type=password]{
        border: none;
        border-bottom: 1px solid #0a8be6;
        outline: 0;
        width: 100%;
        font-size: 16px;
        padding: 5px 0 5px 0px;
        transform: translateY(0px);
        transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
    }
    .input_animation_placeholder input:focus, .input_animation_placeholder input:valid {
        box-shadow: none;
        outline: none;
        background-position: 0 0;
    }
    .input_animation_placeholder input:focus::-webkit-input-placeholder{
        transform: translateY(-20px);
        font-size: 10px;
        transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
    }
</style>

<div class="input_act_1">
    <label class="label_animation_deg" >
        Tên đăng nhập
    </label>
    <div class="input_act">
        <input type="text" placeholder="Nhập tên đăng nhập" class="animation_ac" ul-hover="username_hover"/>
    </div>
</div>

<br/>

<div class="input_act_2">
    <label class="label_animation_deg" >
        Mật khẩu
    </label>
    <div class="input_act">
        <input type="password" placeholder="Mật khẩu" class="animation_ac" ul-hover="password_hover"/>
    </div>
</div>

<br/>

<div class="input_act_3">
    <label class="label_animation_deg" >
        Email
    </label>
    <div class="input_act">
        <input type="text" placeholder="Nhập Email" class="animation_ac" ul-hover="password_hover"/>
    </div>
</div>

<script>
    /*
     * 
     * @param {type} c
     * @returns {undefined}
     * 
     */
    jQuery.fn.extend({
        animation_jd: function (c) {
            var _class = this.selector;

            switch (c) {
                case 'deg':
                {
                    jQuery(_class).addClass('input_animation_deg');
                    var t = 60;
                    var w = jQuery(_class + ' label').width();
                    jQuery(_class + ' input').css('padding-left', (parseInt(w) + t) + 'px');

                    jQuery(_class + ' input').focusin(function () {
                        jQuery(_class + ' label').css('transform', 'rotate3d(0,0,-1,60deg)');
                        jQuery(_class + ' input').removeAttr('style');
                    });

                    jQuery(_class + ' input').focusout(function () {
                        jQuery(_class + ' label').css('transform', 'rotate3d(0,0,0,60deg)');
                        jQuery(_class + ' input').css('padding-left', (parseInt(w) + t) + 'px');
                    });

                    break;
                }
                case 'hover_placeholder':
                {
                    jQuery(_class).addClass('input_animation_placeholder');
                    jQuery(_class + ' label').remove();
                    break;
                }
                default:
                    break;
            }
        }
    });


    jQuery('.input_act_1').animation_jd('deg');
    jQuery('.input_act_2').animation_jd('deg');
    jQuery('.input_act_3').animation_jd('deg');

</script>

<br/>
<br/>