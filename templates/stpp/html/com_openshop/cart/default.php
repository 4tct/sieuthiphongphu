<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/js/cart.js" type = "text/javascript"></script>
<?php
$session = JFactory::getSession();
$user_session = $session->get('user');
$app = JFactory::getApplication()->input;

$sizeP = array();
foreach ($this->getAllSize() as $se) {
    $sizeP[$se->optionvalue_id] = $se->value;
}
//if ($app->getString('quantityProduct') == '' || $app->getInt('IDProduct') == '') {
//    echo 'Hack';
//} else {
?>
<form action="" class="" id="formCart" >
    <div class="row-fluid">
        <div class="col-md-8 cartP" style="border-right: 1px solid #D2D2D2;">
            <div class="cart-title">
                <span class="cart-title-1"><?php echo JText::_('OPENSHOP_CART_TITLE') ?></span>
            </div>
            <?php
            $cart = $this->getCartProducts($_SERVER['REMOTE_ADDR'], $user_session->id);

            if (!empty($cart)) {
                ?>
                <table width="100%" class="table table-bordered table-responsive" id="table-cart" style="margin: 10px 0 0 0;">
                    <thead>
                        <tr>
                            <th width="5%"><?php echo JText::_('OPENSHOP_#') ?></th>
                            <th width="55%"><?php echo JText::_('OPENSHOP_PRODUCT_NAME') ?></th>
                            <th width="13%"><?php echo JText::_('OPENSHOP_QUANTITY') ?></th>
                            <th width="12%">Đơn giá</th>
                            <th width="20%"><?php echo JText::_('OPENSHOP_AMOUNT') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total_quantity = 0;
                        $total_money = 0;
                        $stt = 0;
                        $t = 1;
                        $id = '';
                        foreach ($cart as $k_c => $v_c) {
                            ++$stt;
                            $id = OpenShopHelper::convertBase64Encode($v_c->id);
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $stt ?></td>
                                <td style="position: relative;">
                                    <img class="lazy" data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $v_c->product_image ?>" width="50px"/>
                                    <div class="nameSizeP">
                                        <?php echo $v_c->product_name ?>
                                        <?php
                                        if ($v_c->size_id) {
                                            ?>
                                            <br>
                                            Size: <b><?php echo $sizeP[$v_c->size_id] ?></b>
                                            <br>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <i class="deleteCartP" onclick="showRemoveAction('<?php echo $id ?>')">Xóa sản phẩm?</i>
                                </td>
                                <td class="text-center">
                                    <select name="slt_quantity" id="slt_quantit" style="width: 100%;" onchange="changeQuantityCart('<?php echo $id ?>', this.value)">
                                        <?php
                                        for ($i = 1; $i <= 5; $i++) {
                                            $cls = '';
                                            if ($i == $v_c->quantity) {
                                                $cls = 'selected';
                                            }
                                            ?>
                                            <option value="<?php echo OpenShopHelper::convertBase64Encode($i) ?>" <?php echo $cls ?>><?php echo $i ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td class="text-center">
                                    <?php
                                    echo number_format((int) $v_c->product_price, 0, ',', '.') . '<sup>đ</sup>';
                                    if (!empty($v_c->product_price_r)) {
                                        echo '<div class="price_r">' . number_format((int) $v_c->product_price_r, 0, ',', '.') . '<sup>đ</sup></div>';
                                    }
                                    ?>
                                </td>
                                <td class="text-center"><?php echo number_format($v_c->quantity * (int) $v_c->product_price, 0, ',', '.') ?><sup>đ</sup></td>
                            </tr>
                            <?php
                            echo '</tr>';
                            $total_quantity += $v_c->quantity;
                            $total_money += (int) $v_c->quantity * (int) $v_c->product_price;
                            if ($t == 1) {
                                $id_cartpro = $v_c->id;
                                $t = 0;
                            } else {
                                $id_cartpro .= ',' . $v_c->id;
                            }
                        }
                        ?>
                        <tr class="total_title">
                            <td colspan="2" style="text-align: right"><?php echo JText::_('OPENSHOP_TOTAL_TITLE') ?></td>
                            <td style="text-align:center"><span class="total-quantity"><?php echo $total_quantity ?></span></td>
                            <td colspan="2" style="text-align:center"><span class="total-money"><?php echo number_format($total_money, 0, ',', '.') ?></span> <sup>đ</sup></td>
                        </tr>
                        <tr class="total_title">
                            <td colspan="2" style="text-align: right"><?php echo JText::_('OPENSHOP_TRANSFER_MONEY') ?></td>
                            <td style="text-align:center"></td>
                            <td colspan="2" style="text-align:center"><span class="transfer-money">0</span> <sup>đ</sup></td>
                        </tr>
                        <tr class="total_title">
                            <td colspan="2" style="text-align: right"><?php echo JText::_('OPENSHOP_TOTAL') ?></td>
                            <td style="text-align:center"></td>
                            <td colspan="2" style="text-align:center"><span class="total"><?php echo number_format($total_money, 0, ',', '.') ?></span> <sup>đ</sup></td>
                    <input type="hidden" name="total" id="total" value="<?php echo $total_money ?>"/>
                    </tr>
                    </tbody>
                </table>
                <?php
            } else {
                ?>
                <div style="    padding: 30px 0;">
                    <i>Hiện không có sản phẩm trong giỏ hàng </i>
                </div>
                <?php
            }
            ?>
            <input value="<?php echo OpenShopHelper::convertBase64Encode($id_cartpro) ?>" id="id_cartpro" name="id_cartpro" type="hidden"/>
            <div style="padding-top: 20px">
                <span class="btn-continue-buy" onclick="window.location.href = '<?php echo JUri::base(); ?>'">
                    <i class="icon-undo-2"></i> <?php echo JText::_('OPENSHOP_CONTINUE_BUY') ?>
                </span>
            </div>
        </div>
        <!--/LOGIN-->
        <div class="col-md-4 logCart">
            <?php
            if (!empty($cart)) {
                if (empty($user_session->id)) {
                    ?>
                    <div class="logOrder">
                        <div class="login">
                            <span onclick="showlogin()">Đăng nhập</span>
                        </div>
                        <div>- hoặc -</div>
                        <div class="ordernotlogin">
                            <span onclick="showQuiclOrder()">Mua hàng nhanh</span>
                        </div>
                        <div><i>( Bạn chưa đăng nhập )</i></div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="btnSubmitOrder">
                        <span class="btn-checkout" onclick="checkoutCart()"><i class=""></i> <?php echo JText::_('OPENSHOP_CHECKOUT') ?></span>
                    </div>
                    <?php
                }
            } else {
                if (empty($user_session->id)) {
                    ?>
                    <div class="btnSignin">
                        <span onclick="showSignIn()">Đăng ký</span>
                    </div>
                    <?php
                } else {
                    ?>
                    <div style="    padding: 30px 0;">
                        <i>Hiện không có sản phẩm trong giỏ hàng</i>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</form>

<!--//Quick Order-->
<form action="" method="post" id="formQuickOrder" class="form-horizontal">
    <div class="modal fade" id="myModalQuiclOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Đặt hàng nhanh</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3">Họ và tên <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập họ và tên của bạn" name="customer_name" id="customer_name_quick"/>
                            <span class="error error-customer_name_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Số điện thoại <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập số điện thoại của bạn" name="customer_phone" id="customer_phone_quick"/>
                            <span class="error error-customer_phone_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Email </label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập enail của bạn" name="customer_email" id="customer_email_quick"/>
                            <span class="error error-customer_email_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Địa chỉ <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập địa chỉ của bạn bao gồm số nhà, đường,..." name="customer_address" id="customer_address_quick"/>
                            <span class="error error-customer_address_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Tỉnh/Thành Phố <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <select name="zone" id="customer_town_quick" class="form-control" onchange="changeZone(this.value)">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_ZONE') ?></option>
                                <?php
                                $zone_id = isset($user) ? $user->zone_id : '-1';
                                foreach ($this->getZone() as $v_z) {
                                    $cls = '';
                                    if ($v_z->id == $zone_id) {
                                        $cls = 'selected';
                                    }
                                    echo '<option value="' . $v_z->id . '" ' . $cls . '>' . $v_z->zone_name . '</option>';
                                }
                                ?>
                            </select>
                            <span class="error error-customer_town_quick"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Quận/Huyện <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <select name="district" id="customer_district_1" class="form-control">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_DISTRICT') ?></option>
                                <?php
                                $district_id = isset($user) ? $user->district_id : '-1';
                                foreach ($this->getDistrict() as $v_d) {
                                    $cls = '';
                                    if ($v_d->id == $district_id) {
                                        $cls = 'selected';
                                    }
                                    echo '<option value="' . $v_d->id . '" ' . $cls . '>' . $v_d->district_name . '</option>';
                                }
                                ?>
                            </select>
                            <span class="error error-customer_district_1"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3"></label>
                        <div class="col-md-9">
                            <i style="color:red;">(*)</i> Yêu cầu nhập
                            <input value="<?php echo $id ?>" id="id_cartpro" name="id_cartpro" type="hidden"/>
                            <input type="hidden" name="total" id="total" value="<?php echo $total_money ?>"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="checkoutCart('quick')">Đặt hàng</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
//}
?>




