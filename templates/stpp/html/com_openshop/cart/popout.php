<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/colorbox/jquery.colorbox.js" type="text/javascript"></script>
<h1>
	<?php echo JText::_('OPENSHOP_SHOPPING_CART'); ?>
	<?php
	if ($this->weight)
	{
		echo '&nbsp;(' . $this->weight . ')';
	}
	?>
</h1>
<?php
if (isset($this->success))
{
	?>
	<div class="success"><?php echo $this->success; ?></div>
	<?php
}
if (isset($this->warning))
{
	?>
	<div class="warning"><?php echo $this->warning; ?></div>
	<?php
}
?>
<?php
if (!count($this->cartData))
{
	?>
	<div class="no-content"><?php echo JText::_('OPENSHOP_CART_EMPTY'); ?></div>
	<?php
}
else
{
	?>
	<div class="cart-info">
		<?php
		if(!OpenShopHelper::isMobile())
		{
			?>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style="text-align: center;"><?php echo JText::_('OPENSHOP_REMOVE'); ?></th>
						<th style="text-align: center;"><?php echo JText::_('OPENSHOP_IMAGE'); ?></th>
						<th><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
						<th><?php echo JText::_('OPENSHOP_QUANTITY'); ?></th>
						<th nowrap="nowrap"><?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?></th>
						<th><?php echo JText::_('OPENSHOP_TOTAL'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$countProducts = 0;
					foreach ($this->cartData as $key => $product)
					{
						$countProducts++;
						$optionData = $product['option_data'];
						$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
						?>
						<tr>
							<td class="openshop-center-text" style="vertical-align: middle;">
								<a class="openshop-remove-item-cart" id="<?php echo $key; ?>" style="cursor: pointer;">
									<img alt="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" title="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/images/remove.png" />
								</a>
							</td>
							<td class="muted openshop-center-text" style="vertical-align: middle;">
								<a href="<?php echo $viewProductUrl; ?>">
									<img class="img-polaroid" src="<?php echo $product['image']; ?>" />
								</a>
							</td>
							<td style="vertical-align: middle;">
								<a href="<?php echo $viewProductUrl; ?>">
									<?php echo $product['product_name']; ?>
								</a>
								<?php
								if (!$product['stock'] && !OpenShopHelper::getConfigValue('stock_checkout'))
								{
									?>
									<span class="stock">***</span>
									<?php
								}
								?>
								<br />	
								<?php
								for ($i = 0; $n = count($optionData), $i < $n; $i++)
								{
									echo '- ' . $optionData[$i]['option_name'] . ': ' . $optionData[$i]['option_value'] . (isset($optionData[$i]['sku']) && $optionData[$i]['sku'] != '' ? ' (' . $optionData[$i]['sku'] . ')' : '') . '<br />';
								}
								?>
							</td>
							<td style="vertical-align: middle;">
								<div class="input-append input-prepend">
									<span class="openshop-quantity">
										<input type="hidden" name="key[]" value="<?php echo $key; ?>" />
										<a onclick="quantityUpdate('+', <?php echo $countProducts; ?>)" class="btn btn-default button-plus" id="popout_<?php echo $countProducts; ?>">+</a>
											<input type="text" class="openshop-quantity-value" value="<?php echo htmlspecialchars($product['quantity'], ENT_COMPAT, 'UTF-8'); ?>" name="quantity[]" id="quantity_popout_<?php echo $countProducts; ?>" />
										<a onclick="quantityUpdate('-', <?php echo $countProducts; ?>)" class="btn btn-default button-minus" id="popout_<?php echo $countProducts; ?>">-</a>
									</span>
								</div>
							</td>
							<td style="vertical-align: middle;">
								<?php
								if (OpenShopHelper::showPrice())
									echo $this->currency->format($this->tax->calculate($product['price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax')));
								?>
							</td>
							<td style="vertical-align: middle;">
								<?php
								if (OpenShopHelper::showPrice())
									echo $this->currency->format($this->tax->calculate($product['total_price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax')));
								?>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
			<?php
			if (OpenShopHelper::showPrice())
			{ ?>
			<div class="totals text-center" style="text-align: center">
			<?php
				foreach ($this->totalData as $data)
				{
					?>
						<div>
							<?php echo $data['title']; ?>:
							<?php echo $data['text']; ?>
						</div>
					<?php	
				} ?>
			</div>
			<?php }
		}
		else
		{
			?>
			<div class="row-fluid">
				<?php
				foreach ($this->cartData as $key => $product)
				{
					$optionData = $product['option_data'];
					$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
					?>
					<div class="well clearfix">
						<div class="row-fluid">
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_REMOVE'); ?>: </strong>
								<a class="openshop-remove-item-cart" id="<?php echo $key; ?>" style="cursor: pointer;">
									<img alt="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" title="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/images/remove.png" />
								</a>
							</div>
							<div class="span2 openshop-center-text">
								<a href="<?php echo $viewProductUrl; ?>">
									<img class="img-polaroid" src="<?php echo $product['image']; ?>" />
								</a>
							</div>
							<div class="span2">
								<h5 class="openshop-center-text">
									<a href="<?php echo $viewProductUrl; ?>">
										<?php echo $product['product_name']; ?>
									</a>
								</h5>
								<?php
								if (!$product['stock'] && !OpenShopHelper::getConfigValue('stock_checkout'))
								{
									?>
									<span class="stock">***</span>
									<?php
								}
								?>
								<br />	
								<?php
								for ($i = 0; $n = count($optionData), $i < $n; $i++)
								{
									echo '- ' . $optionData[$i]['option_name'] . ': ' . $optionData[$i]['option_value'] . '<br />';
								}
								?>
							</div>
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_MODEL'); ?>: </strong>
								<?php echo $product['product_sku']; ?>
							</div>
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_QUANTITY'); ?></strong>
								<div class="input-append input-prepend">
									<span class="openshop-quantity">
										<input type="hidden" name="key[]" value="<?php echo $key; ?>" />
										<a onclick="quantityUpdate('+', <?php echo $countProducts; ?>)" class="btn btn-default button-plus" id="popout_<?php echo $countProducts; ?>">+</a>
											<input type="text" class="openshop-quantity-value" value="<?php echo htmlspecialchars($product['quantity'], ENT_COMPAT, 'UTF-8'); ?>" name="quantity[]" id="quantity_popout_<?php echo $countProducts; ?>" />
										<a onclick="quantityUpdate('-', <?php echo $countProducts; ?>)" class="btn btn-default button-minus" id="popout_<?php echo $countProducts; ?>">-</a>
											
									</span>
								</div>
							</div>
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?>: </strong>
								<?php
								if (OpenShopHelper::showPrice())
									echo $this->currency->format($this->tax->calculate($product['price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax')));
								?>
							</div>
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_TOTAL'); ?>: </strong>
								<?php
								if (OpenShopHelper::showPrice())
									echo $this->currency->format($this->tax->calculate($product['total_price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax')));
								?>
							</div>
						</div>
					</div>
					<?php
				}
				if (OpenShopHelper::showPrice())
				{
					?>
					<div class="well clearfix">
						<?php
						foreach ($this->totalData as $data)
						{
							echo $data['title']; ?>: <strong><?php echo $data['text']; ?></strong><br />
						<?php
						}
						?>
					</div>
					<?php
				}
				?>
			</div>
		<?php
	    }
	    ?>
    </div>
    <div class="bottom control-group" style="text-align: center;">
		<div class="controls">
			<a class="btn btn-danger" href="<?php echo JRoute::_(OpenShopHelper::getContinueShopingUrl()); ?>"><?php echo JText::_('OPENSHOP_CONTINUE_SHOPPING'); ?></a>
			<button type="button" class="btn btn-info" onclick="updateCart();" id="update-cart"><?php echo JText::_('OPENSHOP_UPDATE_CART'); ?></button>
			<a class="btn btn-success" href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('cart')); ?>"><?php echo JText::_('OPENSHOP_SHOPPING_CART'); ?></a>
			<?php
			if (OpenShopHelper::getConfigValue('active_https'))
			{
				$checkoutUrl = JRoute::_(OpenShopRoute::getViewRoute('checkout'), true, 1);
			}
			else
			{
				$checkoutUrl = JRoute::_(OpenShopRoute::getViewRoute('checkout'));
			}
			?>
			<a class="btn btn-primary" href="<?php echo $checkoutUrl; ?>"><?php echo JText::_('OPENSHOP_CHECKOUT'); ?></a>
		</div>
	</div>
	
	<script type="text/javascript">
		OpenShop.jQuery(function($){
			$(document).ready(function(){
				quantityUpdate = (function(sign, productId){
					var oldQuantity = $('#quantity_popout_' + productId).val();
					if(sign == '+')
					{
						oldQuantity++;
						$('#quantity_popout_' + productId).val(oldQuantity);
					}
					else if (sign == '-')
					{
						oldQuantity--;
						if (oldQuantity > 0)
						{
							$('#quantity_popout_' + productId).val(oldQuantity);
						}
					}
				})
			})
		})
		//Function to update cart
		function updateCart()
		{
			OpenShop.jQuery(function($){
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
				$.ajax({
					type: 'POST',
					url: siteUrl + 'index.php?option=com_openshop&task=cart.updates<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
					data: $('.cart-info input[type=\'text\'], .cart-info input[type=\'hidden\']'),
					beforeSend: function() {
						$('#update-cart').attr('disabled', true);
						$('#update-cart').after('<span class="wait">&nbsp;<img src="components/com_openshop/assets/images/loading.gif" alt="" /></span>');
					},
					complete: function() {
						$('#update-cart').attr('disabled', false);
						$('.wait').remove();
					},
					success: function() {
						$.ajax({
							url: siteUrl + 'index.php?option=com_openshop&view=cart&layout=popout&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
							dataType: 'html',
							success: function(html) {
								$.colorbox({
									overlayClose: true,
									opacity: 0.5,
									href: false,
									html: html
								});
								$.ajax({
									url: siteUrl + 'index.php?option=com_openshop&view=cart&layout=mini&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
									dataType: 'html',
									success: function(html) {
										$('#openshop-cart').html(html);
										$('.openshop-content').hide();
									},
									error: function(xhr, ajaxOptions, thrownError) {
										alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
									}
								});
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
				  	}
				});
			})
		}

		OpenShop.jQuery(function($) {
			//Ajax remove cart item
			$('.openshop-remove-item-cart').bind('click', function() {
				var aTag = $(this);
				var id = aTag.attr('id');
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
				$.ajax({
					type :'POST',
					url: siteUrl + 'index.php?option=com_openshop&task=cart.remove&key=' +  id + '&redirect=1<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
					beforeSend: function() {
						aTag.attr('disabled', true);
						aTag.after('<span class="wait">&nbsp;<img src="components/com_openshop/assets/images/loading.gif" alt="" /></span>');
					},
					complete: function() {
						aTag.attr('disabled', false);
						$('.wait').remove();
					},
					success : function() {
						$.ajax({
							url: siteUrl + 'index.php?option=com_openshop&view=cart&layout=popout&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
							dataType: 'html',
							success: function(html) {
								$.colorbox({
									overlayClose: true,
									opacity: 0.5,
									href: false,
									html: html
								});
								$.ajax({
									url: siteUrl + 'index.php?option=com_openshop&view=cart&layout=mini&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
									dataType: 'html',
									success: function(html) {
										$('#openshop-cart').html(html);
										$('.openshop-content').hide();
									},
									error: function(xhr, ajaxOptions, thrownError) {
										alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
									}
								});
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			});
		});
	</script>
	<?php
}