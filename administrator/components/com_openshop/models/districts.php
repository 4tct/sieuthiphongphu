<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelDistricts extends OpenShopModelList
{

	function __construct($config)
	{
		parent::__construct($config);
	}

        function _buildQuery()
	{
		$db = $this->getDbo();
		$state = $this->getState();
		$where = $this->_buildContentWhereArray();
		$query = $db->getQuery(true);
		$query->select('a.*, b.zone_name')
			->from('#__openshop_districts AS a')
			->join('LEFT', '#__openshop_zones AS b ON a.parent_id = b.id ');
		if (count($where))
			$query->where($where);
		$query->order($state->filter_order . ' ' . $state->filter_order_Dir);
		return $query;
	}

	function _buildContentWhereArray()
	{
		$state = $this->getState();
		$where = parent::_buildContentWhereArray();
		return $where;
	}
}