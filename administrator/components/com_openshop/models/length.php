<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * Eshop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelLength extends OpenShopModel
{
	public function __construct($config)
	{
		$config['translatable'] = true;
		$config['translatable_fields'] = array('length_name', 'length_unit');
		parent::__construct($config);
	}
	
	/**
	 * Method to remove lengths
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		if (count($cid))
		{
			$db = $this->getDbo();
			$cids = implode(',', $cid);
			$query = $db->getQuery(true);
			$query->delete('#__openshop_lengths')
				->where('id IN (' . $cids . ')')
				->where('id NOT IN (SELECT  DISTINCT(product_length_id) FROM #__openshop_products)');
			$db->setQuery($query);
			if (!$db->query())
				//Removed error
				return 0;
			$numItemsDeleted = $db->getAffectedRows();
			//Delete details records
			$query->clear();
			$query->delete('#__openshop_lengthdetails')
				->where('length_id IN (' . $cids . ')')
				->where('length_id NOT IN (SELECT  DISTINCT(product_length_id) FROM #__openshop_products)');
			$db->setQuery($query);
			if (!$db->query())
				//Removed error
				return 0;
			if ($numItemsDeleted < count($cid))
			{
				//Removed warning
				return 2;
			}
		}
		//Removed success
		return 1;
	}
}