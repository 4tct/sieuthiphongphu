<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Configuration Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelConfiguration extends JModelLegacy {

    /**
     * Containing all config data,  store in an object with key, value
     *
     * @var object
     */
    var $_data = null;

    function __construct() {
        parent::__construct();
    }

    /**
     * Get configuration data
     * @return object
     */
    function getData() {
        if (empty($this->_data)) {
            $config = new stdClass();
            $db = $this->getDbo();
            $query = $db->getQuery(true);
            $query->select('config_key, config_value')
                    ->from('#__openshop_configs');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            if (count($rows)) {
                for ($i = 0, $n = count($rows); $i < $n; $i++) {
                    $row = $rows[$i];
                    $key = $row->config_key;
                    $value = $row->config_value;
                    $config->$key = stripslashes($value);
                }
            }
            $this->_data = $config;
        }

        return $this->_data;
    }

    /**
     * Store the configuration data
     *
     * @param array $post
     * @return Boolean
     */
    function store(&$data) {

        $upload_img = array('', 'waiting_input_img', 'waiting_enouch_img', 'waiting_appointment_img', 'order_complete_img');
                    
        $db = $this->getDbo();
        $db->truncateTable('#__openshop_configs');
        $row = $this->getTable('OpenShop', 'Config');
        foreach ($data as $key => $value) {
            $row->id = '';
            if (is_array($value)) {
                $value = implode(',', $value);
            }
            
            if (array_search($key, $upload_img)) {
                
                $row->config_key = $key;
                $name = substr($key, 0, strlen($key) - 4);
                
                $imgFile = $_FILES[$name];
                $path = OPENSHOP_PATH_IMG_OTHER;
                if ($imgFile && !empty($imgFile['name'])) {
                    $imgName = rand(0, 1000000) . '_' . $imgFile['name'];

                    JFile::upload($imgFile['tmp_name'], $path . $imgName);

                    $row->config_value = $imgName;
                }
                else
                {
                    $row->config_value = $value; 
                }
            } else {
                $row->config_key = $key;
                $row->config_value = $value;
            }
          
            $row->store();
        }
        //Update currencies
        OpenShopHelper::updateCurrencies(true);
        return true;
    }

    public function upload_image($data, $key) {
        $imgFile = $_FILES[$key];
        $path = OPENSHOP_PATH_IMG_OTHER;
        $imgName = ran(0, 100000) . $imgFile['name'];
        if ($imgFile) {
            JFile::upload($imgFile['tmp_name'], $path . $imgName);
        }

        return $imgName;
    }

}
