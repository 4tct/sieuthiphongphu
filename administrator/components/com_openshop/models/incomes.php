<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelIncomes extends OpenShopModelList {

    function __construct($config) {
        $config['search_fields'] = array('');
        $config['translatable'] = false;
        $config['translatable_fields'] = array('');

        parent::__construct($config);
    }

    function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);

        $query->select('d.product_name,d.product_sku,d.price,d.quantity,d.price_input')
                ->from($this->mainTable . ' AS a ')
                ->join('INNER', $db->quoteName('#__openshop_carts','b') . ' ON a.cart_id = b.id')
                ->join('INNER', $db->quoteName('#__openshop_orders','c') .' ON b.order_id = c.id')
                ->join('INNER', $db->quoteName('#__openshop_orderproducts','d') . 'ON c.id = d.order_id')
                ->where('d.order_product_delete = 0');

        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

}
