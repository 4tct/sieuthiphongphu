<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelInventories extends OpenShopModelList {

    function __construct($config) {
        $config['search_fields'] = array('c.product_name');
        $config['translatable'] = false;
        parent::__construct($config);
    }

    function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);

        //get id warehouse in user
        $query->clear();
        $query->select('id,user_group_manage')
                ->from($db->quoteName('#__openshop_warehouses'));
        $rows_warehouse = $db->setQuery($query)->loadObjectList();

        $usergroup_id = OpenShopHelper::getUsergroupId(JFactory::getUser()->id);
        $res_warehouse = array();
        foreach ($rows_warehouse as $v_id) {
            $arr = explode(',', $v_id->user_group_manage);
            foreach ($arr as $v_arr) {
                if ($v_arr == $usergroup_id) {
                    $res_warehouse[] = $v_id->id;
                    break;
                }
            }
        }

        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_inventories', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . 'ON c.product_id = b.id')
                ->where('a.warehouse_id = ' . $res_warehouse[0])
                ->group('a.product_id')
                ->order('a.product_id ASC');
        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

    public function _buildContentWhereArray() {
        $db = $this->getDbo();
        $state = $this->getState();
        $where = array();
        if ($state->search) {
            $search = $db->quote('%' . $db->escape(strtolower($state->search), true) . '%', false);
            if (is_array($this->searchFields)) {
                $whereOr = array();
                foreach ($this->searchFields as $titleField) {
                    $whereOr[] = " LOWER($titleField) LIKE " . $search;
                }
                $where[] = ' (' . implode(' OR ', $whereOr) . ') ';
            } else {
                $where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape(strtolower($state->search), true) . '%', false);
            }
        }

        return $where;
    }

    public function getTotal() {
        // Lets load the content if it doesn't already exist
        if (empty($this->_total)) {
            $db = $this->getDbo();
            $state = $this->getState();
            $where = $this->_buildContentWhereArray();
            $query = $db->getQuery(true);
            $query->select('COUNT(*)');
            $query->from($this->mainTable . ' AS a ')
                    ->join('INNER', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . 'ON c.product_id = b.id');
            if (count($where))
                $query->where($where);

            $db->setQuery($query);
            $this->_total = $db->loadResult();
        }
        return $this->_total;
    }

}
