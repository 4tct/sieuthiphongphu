<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelLabel extends OpenShopModel
{

	public function __construct($config)
	{
		$config['translatable'] = true;
		$config['translatable_fields'] = array('label_name');
		
		parent::__construct($config);
	}

	function store(&$data)
	{
		$imagePath = JPATH_ROOT . '/media/com_openshop/labels/';
		$imageWidth = $data['label_image_width'] > 0 ? $data['label_image_width'] : OpenShopHelper::getConfigValue('label_image_width');
		if (!$imageWidth)
			$imageWidth = 50;
		$imageHeight = $data['label_image_height'] > 0 ? $data['label_image_height'] : OpenShopHelper::getConfigValue('label_image_height');
		if (!$imageHeight)
			$imageHeight = 50;
		
		if (JRequest::getInt('remove_image') && $data['id'])
		{
			//Remove image first
			$row = new OpenShopTable('#__openshop_labels', 'id', $this->getDbo());
			$row->load($data['id']);
			
			if (JFile::exists($imagePath . $row->label_image))
				JFile::delete($imagePath . $row->label_image);
			if (JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->label_image).'-'.$imageWidth.'x'.$imageHeight.'.'.JFile::getExt($row->label_image)))
				JFile::delete($imagePath . 'resized/' . JFile::stripExt($row->label_image).'-'.$imageWidth.'x'.$imageHeight.'.'.JFile::getExt($row->label_image));
			$data['label_image'] = '';
		}
		
		$labelImage = $_FILES['label_image'];
		if ($labelImage['name'])
		{
			$checkFileUpload = OpenShopFile::checkFileUpload($labelImage);
			if (is_array($checkFileUpload))
			{
				$mainframe = JFactory::getApplication();
				$mainframe->enqueueMessage(sprintf(JText::_('OPENSHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
				$mainframe->redirect('index.php?option=com_openshop&task=label.edit&cid[]=' . $data['id']);
			}
			else
			{
				if (is_uploaded_file($labelImage['tmp_name']) && file_exists($labelImage['tmp_name']))
				{
					if ($data['id'])
					{
						// Delete the old image
						$row = new OpenShopTable('#__openshop_labels', 'id', $this->getDbo());
						$row->load($data['id']);
							
						if (JFile::exists($imagePath . $row->label_image))
							JFile::delete($imagePath . $row->label_image);
						if (JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->label_image).'-'.$imageWidth.'x'.$imageHeight.'.'.JFile::getExt($row->label_image)))
							JFile::delete($imagePath . 'resized/' . JFile::stripExt($row->label_image).'-'.$imageWidth.'x'.$imageHeight.'.'.JFile::getExt($row->label_image));
					}
					if (JFile::exists($imagePath . $labelImage['name']))
					{
						$imageFileName = uniqid('image_') . '_' . $labelImage['name'];
					}
					else
					{
						$imageFileName = $labelImage['name'];
					}
					JFile::upload($labelImage['tmp_name'], $imagePath . $imageFileName);
					$data['label_image'] = $imageFileName;
				}	
			}
		}
		//Delete label elements first
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		if ($data['id'])
		{
			$query->delete('#__openshop_labelelements')
				->where('label_id = ' . intval($data['id']));
			$db->setQuery($query);
			$db->query();
		}
		parent::store($data);
		$row = new OpenShopTable('#__openshop_labels', 'id', $this->getDbo());
		$row->load($data['id']);
		if ($row->label_image && !JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->label_image).'-'.$imageWidth.'x'.$imageHeight.'.'.JFile::getExt($row->label_image)))
		{
			$imageSizeFunction = OpenShopHelper::getConfigValue('image_size_function', 'resizeImage');
			call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($row->label_image, JPATH_ROOT . '/media/com_openshop/labels/', $imageWidth, $imageHeight));
		}
		//Label for products
		if (isset($data['product_id']))
		{
			$productIds = $data['product_id'];
			if (count($productIds))
			{
				$query->clear();
				$query->insert('#__openshop_labelelements')
					->columns('label_id, element_id, element_type');
				$labelId = $data['id'];
				for ($i = 0; $i < count($productIds); $i++)
				{
					$productId = $productIds[$i];
					$query->values("$labelId, $productId, 'product'");
				}
				$db->setQuery($query);
				$db->query();
			}
		}
		//Label for brand
		if (isset($data['brand_id']))
		{
			$brandIds = $data['brand_id'];
			if (count($brandIds))
			{
				$query->clear();
				$query->insert('#__openshop_labelelements')
					->columns('label_id, element_id, element_type');
				$labelId = $data['id'];
				for ($i = 0; $i < count($brandIds); $i++)
				{
					$brandId = $brandIds[$i];
					$query->values("$labelId, $brandId, 'brand'");
				}
				$db->setQuery($query);
				$db->query();
			}
		}
		//Label for categories
		if (isset($data['category_id']))
		{
			$categoryIds = $data['category_id'];
			if (count($categoryIds))
			{
				$query->clear();
				$query->insert('#__openshop_labelelements')
					->columns('label_id, element_id, element_type');
				$labelId = $data['id'];
				for ($i = 0; $i < count($categoryIds); $i++)
				{
					$categoryId = $categoryIds[$i];
					$query->values("$labelId, $categoryId, 'category'");
				}
				$db->setQuery($query);
				$db->query();
			}
		}
		return true;
	}
	
	/**
	 * Method to remove labels
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		//Remove label elements
		if (count($cid))
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->delete('#__openshop_labelelements')
				->where('label_id IN (' . implode(',', $cid) . ')');
			$db->setQuery($query);
			$db->query();
		}
		parent::delete($cid);
	}
}