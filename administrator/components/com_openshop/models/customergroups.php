<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelCustomergroups extends OpenShopModelList
{
	function __construct($config)
	{
		$config['state_vars'] = array('filter_order' => array('b.customergroup_name', 'string', 1));
		$config['translatable'] = true;
		$config['search_fields'] = array('b.customergroup_name');
		$config['translatable_fields'] = array('customergroup_name');
		parent::__construct($config);
	}
}