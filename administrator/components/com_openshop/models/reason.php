<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelReason extends OpenShopModel {

    public function __construct($config) {
        parent::__construct($config);
    }

    public function store(&$data) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD'));
        if (isset($data['cid'][0]) && $data['cid'][0] != '') {
            $data_reason = new stdClass();

            $data_reason->id = $data['cid'][0];
            $data_reason->reason_name = $data['reason_name'];
            $data_reason->published = $data['published'];
            $data_reason->reason_description = $data['reason_description'];

            if ($db->updateObject('#__openshop_reasons', $data_reason, 'id')) {
                for ($i = 1; $i <= $data['max_value']; $i++) {
                    if ($data['id_value_reason_detail_' . $i] != '' && isset($data['id_value_reason_detail_' . $i])) { 
                        $data_reason_detail = new stdClass();
                        $data_reason_detail->id = $data['id_value_reason_detail_' . $i];
                        $data_reason_detail->reason_detail_description = $data['reason_name_desc_' . $i];
                        $db->updateObject('#__openshop_reasondetails', $data_reason_detail, 'id');
                    } else {
                        $data_reason_detail = new stdClass();
                        $query->clear();
                        $query->select('max(id)')->from($db->quoteName('#__openshop_reasons'));
                        $data_reason_detail->reason_id = $db->setQuery($query)->loadResult();
                        $data_reason_detail->reason_detail_description = $data['reason_name_desc_' . $i];
                        $db->insertObject('#__openshop_reasondetails', $data_reason_detail);
                    }
                }

                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
            }
        } else {
            $data_reason = new stdClass();
            $data_reason->reason_name = $data['reason_name'];
            $data_reason->published = $data['published'];
            $data_reason->reason_description = $data['reason_description'];
            if ($db->insertObject('#__openshop_reasons', $data_reason)) {
                $data_reason_detail = new stdClass();

                $query->select('max(id)')->from($db->quoteName('#__openshop_reasons'));

                $data_reason_detail->reason_id = $db->setQuery($query)->loadResult();

                for ($i = 1; $i <= $data['max_value']; $i++) {
                    $data_reason_detail->reason_detail_description = $data['reason_name_desc_' . $i];
                    $db->insertObject('#__openshop_reasondetails', $data_reason_detail);
                }

                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
            }
        }

        echo json_encode($res);
    }

}
