<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelProducts extends OpenShopModelList {

    /**
     *
     * Constructor
     * 
     * @param array $config        	
     */
    function __construct($config) {
        $config['search_fields'] = array('a.product_sku', 'b.product_name');
        $config['translatable'] = true;
        $config['state_vars'] = array(
            'argency_id' => array('', 'cmd', 1),
            'manufacturer_id' => array('', 'cmd', 1),
            'brand_id' => array('', 'cmd', 1),
            'category_id' => array('', 'cmd', 1),
            'stock_status' => array('', 'cmd', 1),
			'product_new' => array('', 'cmd', 1),
			'product_hot' => array('', 'cmd', 1),
            'show_all_product' => array('', 'cmd', 1));
        $config['translatable_fields'] = array(
            'product_name',
            'product_alias',
            'product_desc',
            'product_short_desc',
            'meta_key',
            'meta_desc');
        parent::__construct($config);
    }

    /**
     * Get total entities
     *
     * @return int
     */
    public function getTotal() {
        // Lets load the content if it doesn't already exist
        if (empty($this->_total)) {
            $db = $this->getDbo();
            $state = $this->getState();
            $where = $this->_buildContentWhereArray();
            $query = $db->getQuery(true);
            $query->select('COUNT(*)');
            $query->from($this->mainTable . ' AS a ')
                    ->innerJoin(OpenShopInflector::singularize($this->mainTable) . 'details AS b ON (a.id = b.' . OpenShopInflector::singularize($this->name) . '_id)');
            if ($state->category_id) {
                $query->innerJoin('#__openshop_productcategories AS c ON (a.id = c.product_id)');
            }
            if ($state->stock_status == '1') {
                $where[] = 'a.product_quantity > 0';
            } elseif ($state->stock_status == '2') {
                $where[] = 'a.product_quantity <= 0';
            }
            if (count($where))
                $query->where($where);

            $db->setQuery($query);
            $this->_total = $db->loadResult();
        }
        return $this->_total;
    }

    /**
     * Basic build Query function.
     * The child class must override it if it is necessary
     *
     * @return string
     */
    public function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);

        $query->select('a.*, ' . implode(', ', $this->translatableFields))
                ->from($this->mainTable . ' AS a ')
                ->innerJoin(OpenShopInflector::singularize($this->mainTable) . 'details AS b ON (a.id = b.' . OpenShopInflector::singularize($this->name) . '_id)');

        $where = $this->_buildContentWhereArray();
        if ($state->category_id) {
            $query->innerJoin('#__openshop_productcategories AS c ON (a.id = c.product_id)');
        }
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        $query->order('id DESC');
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

    /**
     * Build an where clause array
     *
     * @return array
     */
    public function _buildContentWhereArray() {
        $db = $this->getDbo();
        $state = $this->getState();
        $where = array();
        if ($state->filter_state == 'P')
            $where[] = ' a.published=1 ';
        elseif ($state->filter_state == 'U')
            $where[] = ' a.published = 0';

        if ($state->search) {
            $search = $db->quote('%' . $db->escape($state->search, true) . '%', false);
            if (is_array($this->searchFields)) {
                $whereOr = array();
                foreach ($this->searchFields as $titleField) {
                    $whereOr[] = " LOWER($titleField) LIKE " . $search;
                }
                $where[] = ' (' . implode(' OR ', $whereOr) . ') ';
            } else {
                $where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape($state->search, true) . '%', false);
            }
        }

        if ($this->translatable) {
//            $where[] = 'b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"';
        }

        if ($state->category_id) {
            $where[] = 'c.category_id = ' . $state->category_id;
        }

        if ($state->stock_status == '1') {
            $where[] = 'a.product_quantity > 0';
        } elseif ($state->stock_status == '2') {
            $where[] = 'a.product_quantity <= 0';
        }

        if ($state->brand_id) {
            $where[] = 'a.brand_id = ' . $state->brand_id;
        }

        if ($state->manufacturer_id) {
            $where[] = 'a.manufacturer_id = ' . $state->manufacturer_id;
        }

        if ($state->argency_id) {
            $where[] = 'a.argency_id = ' . $state->argency_id;
        }

        if (OpenShopPermissions::checkAdmin(JFactory::getUser()->groups)) {
            if ($state->show_all_product == '0' || $state->show_all_product == '1') {
                $where[] = 'a.delete_status = ' . $state->show_all_product;
            }
        } 
		else {
            $where[] = 'a.delete_status = 0';
        }
		
		if($state->product_new){
			$where[] = 'a.product_new = ' . $state->product_new; 
		}
		
		if($state->product_hot){
			$where[] = 'a.product_hot = ' . $state->product_hot; 
		}

        return $where;
    }

}
