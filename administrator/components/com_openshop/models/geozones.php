<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelGeozones extends OpenShopModelList {

    function __construct($config) {
        $config['search_fields'] = array('a.geozone_name');

        parent::__construct($config);
    }

    function _buildQuery() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*,b.transporter_name')
                ->from('#__openshop_geozones AS a')
                ->join('LEFT',$db->quoteName('#__openshop_transporters','b') . 'ON a.transporter_id = b.id');
        ;
        return $query;
    }

}
