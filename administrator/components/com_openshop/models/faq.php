<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelFAQ extends OpenShopModel {

    public function __construct($config) {
        $config['translatable'] = false;
        $config['translatable_fields'] = array('faq_name');

        parent::__construct($config);
    }

    function store(&$data) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        parent::store($data);

        if (!empty($data['cid'][0])) {    //update
            $query->clear();
            $query->delete('#__openshop_faqdetails')
                    ->where('faq_id = ' . $data['cid'][0]);
            if ($db->setQuery($query)->execute()) {
                for ($i = 1; $i <= $data['num_question_answer']; $i++) {
                    $d = new stdClass();
                    $d->faq_id = $data['cid'][0];
                    $d->faq_question = $data['faq_question_' . $i];
                    $d->faq_answer = $data['faq_answer_' . $i];
                    $db->insertObject('#__openshop_faqdetails', $d);
                }
            }
        } else {   //add new
            $query->clear();
            $query->select('max(id)')
                    ->from($db->quoteName('#__openshop_faqs'));
            $id = $db->setQuery($query)->loadResult();

            for ($i = 1; $i <= $data['num_question_answer']; $i++) {
                $d = new stdClass();
                $d->faq_id = $id;
                $d->faq_question = $data['faq_question_' . $i];
                $d->faq_answer = $data['faq_answer_' . $i];
                $db->insertObject('#__openshop_faqdetails', $d);
            }
        }


        return true;
    }

    /**
     * Method to remove brands
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $cids = explode(',', implode(',', $cid));
            foreach ($cids as $cid) {
                $query->clear();
                $query->delete('#__openshop_faqs')
                        ->where('id = ' . $cid);
                if ($db->setQuery($query)->execute()) {
                    $query->clear();
                    $query->delete('#__openshop_faqdetails')
                            ->where('faq_id = ' . $cid);
                    if ($db->setQuery($query)->execute()) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 2;
                }
            }
        }
        //Removed success
        return 1;
    }

}
