<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelTransporter extends OpenShopModel {

    public function __construct($config) {
//		$config['translatable'] = true;
//		$config['translatable_fields'] = array('manufacturer_name', 'manufacturer_alias', 'manufacturer_desc', 'manufacturer_phone', 'manufacturer_email', 'manufacturer_address');

        parent::__construct($config);
    }

    public function store(&$data) {
        parent::store($data);
//        print_r($data);
        $this->store_menu_transfer($data);
    }

    public function store_menu_transfer($data) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $app = JFactory::getApplication()->input;

        $query->select('id')->from($db->quoteName('#__openshop_transporters'));
        $db->setQuery($query);

        $c = $db->loadObjectList();
        $r = count($c) - 1;
        $id_transporter = (int) ($c[$r]->id);

        $query->clear();
        $query->select('id')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('menu_name = "OPENSHOP_TRANSFER_SERVICES"');
        $db->setQuery($query);
        $t_menu = $db->loadObjectList();

        //don't exists OPENSHOP_TRANSFER_SERVICES
        if (!count($t_menu)) {
            //get id component 
            $query->clear();
            $query->select('extension_id')
                    ->from($db->quoteName('#__extensions'))
                    ->where('element = "' . $app->getString('option') . '"');
            $db->setQuery($query);
            $id = $db->loadResult();

            $data_menu = new stdClass();
            $data_menu->menu_name = "OPENSHOP_TRANSFER_SERVICES";
            $data_menu->published = '1';
            $data_menu->menu_parent_id = '0';
            $data_menu->menu_view = '#';
            $data_menu->menu_com = $id;
            $data_menu->access = '1';

            $db->insertObject('#__openshop_menus', $data_menu);
        }

//        get id component
        $query->clear();
        $query->select('id')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('menu_name = "OPENSHOP_TRANSFER_SERVICES"');
        $db->setQuery($query);
        $t_menu = $db->loadObjectList();
        
        //convert 
        $name = OpenShopHelper::stringAlias($data['transporter_name']);
        $name_alias = 'OPENSHOP_TRANSPORTER_' . strtoupper( str_replace('-', '_',$name));
        
        $profile = new stdClass();
        $profile->menu_name = $name_alias;
        $profile->published = '1';
        $profile->menu_link = 'index.php?option=com_openshop&view=carts&id_transporter=' . $id_transporter;

        $profile->menu_parent_id = $t_menu[0]->id;

        if($db->insertObject('#__openshop_menus', $profile))
        {
            $fp = fopen(OPENSHOP_PATH_TRANSPLATE_ADMIN . 'en-GB' .DS. 'en-GB.com_openshop.ini', 'a');
            $content = "\r\n". $name_alias ."=\"". $data['transporter_name'] ."\"";
            fwrite($fp, $content);
            fclose($fp);
        }
    }

    

}
