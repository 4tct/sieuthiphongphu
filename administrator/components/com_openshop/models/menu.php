<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelMenu extends OpenShopModel {

    public function __construct($config) {
//		$config['translatable'] = true;
//		$config['translatable_fields'] = array('menu_name', 'menu_alias', 'menu_desc', 'menu_phone', 'menu_email', 'menu_address');

        parent::__construct($config);
    }

    function store(&$data) {
        if (JRequest::getInt('remove_image') && $data['id']) {
            //Remove image first
            $row = new OpenShopTable('#__openshop_menus', 'id', $this->getDbo());
            $row->load($data['id']);

            if (JFile::exists(OPENSHOP_ADMIN_PATH_ICON . $row->menu_image))
                JFile::delete(OPENSHOP_ADMIN_PATH_ICON . $row->menu_image);

            if (JFile::exists(OPENSHOP_ADMIN_PATH_ICON . JFile::stripExt($row->menu_image) . '.' . JFile::getExt($row->menu_image)))
                JFile::delete(OPENSHOP_ADMIN_PATH_ICON . JFile::stripExt($row->menu_image) . '.' . JFile::getExt($row->menu_image));
            $data['menu_image'] = '';
        }

        $menuImage = $_FILES['menu_image'];

        if ($menuImage['name']) {
            if (JFile::exists(OPENSHOP_ADMIN_PATH_ICON . $menuImage['name'])) {
                JFile::delete(OPENSHOP_ADMIN_PATH_ICON . $menuImage['name']);
            }
            $checkFileUpload = OpenShopFile::checkFileUpload($menuImage);
            if (is_array($checkFileUpload)) {
                $mainframe = JFactory::getApplication();
                $mainframe->enqueueMessage(sprintf(JText::_('OPENSHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
                $mainframe->redirect('index.php?option=com_openshop&task=menu.edit&cid[]=' . $data['id']);
            } else {
;
                if (is_uploaded_file($menuImage['tmp_name']) && file_exists($menuImage['tmp_name'])) {
                    $imageFileName = JFile::makeSafe($menuImage['name']);

                    //uploade file root
                    JFile::upload($menuImage['tmp_name'], OPENSHOP_ADMIN_PATH_ICON . $imageFileName);
                    // Resize images
                    $data['menu_image'] = $imageFileName;
                    OpenShopHelper::resizeImageURL($imageFileName, OPENSHOP_ADMIN_PATH_ICON, 48, 48);
                }
            }
        }

        parent::store($data);
        return true;
    }

    /**
     * Method to remove menus
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = $this->getDbo();
            $cids = implode(',', $cid);
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_menus')
                    ->where('id IN (' . $cids . ')');
            $db->setQuery($query);
            $menus = $db->loadColumn();
            if (count($menus)) {
                $query->clear();
                $query->delete('#__openshop_menus')
                        ->where('id IN (' . implode(',', $menus) . ')');
                $db->setQuery($query);
                if (!$db->query())
                //Removed error
                    return 0;
                $numItemsDeleted = $db->getAffectedRows();
            }
            else {
                return 2;
            }
        }
        //Removed success
        return 1;
    }

}
