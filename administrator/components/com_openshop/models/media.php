<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
ini_set('upload_max_filesize', '50M');
ini_set('post_max_size', '50M');
ini_set('max_execution_time', 1000);

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelMedia extends OpenShopModel {

    public function __construct($config) {
        $config['translatable'] = false;
//        $config['translatable_fields'] = array('brand_name', 'brand_alias', 'brand_desc', 'brand_page_title', 'brand_page_heading');

        parent::__construct($config);
    }

    function store(&$data) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        parent::store($data);
//        print_r($data);exit();
        $pathMedia = OPENSHOP_PATH_IMG_MEDIA;
        if (!empty($data['cid'][0])) {    //update
            $query->clear();
            $query->select('count(id)')
                    ->from($db->quoteName('#__openshop_mediadetails'))
                    ->where('media_id = ' . $data['cid'][0]);
            $start_row = $db->setQuery($query)->loadResult();   //get total image in mediadetails table
            
            if ($data['media_type'] == '1') {
                for ($i = ++$start_row; $i <= $data['num_row_media']; $i++) {
                    $fileMedia = $_FILES['media_file_' . $i];
                    if (!empty($fileMedia['name'])) {
                        $fileName = rand(10000, 1000000) . '_' . $fileMedia['name'];
                        JFile::upload($fileMedia['tmp_name'], $pathMedia . $fileName);

                        $d = new stdClass();
                        $d->media_id = $data['cid'][0];
                        $d->media_image = $fileName;
                        $db->insertObject('#__openshop_mediadetails', $d);
                    }
                }
            } else if ($data['media_type'] == '2') {
                for ($i = ++$start_row; $i <= $data['num_row_media']; $i++) {
                    $fileMedia = $_FILES['media_file_' . $i];
                    $fileName = '';
                    if (!empty($fileMedia['name'])) {
                        $fileName = rand(10000, 1000000) . '_' . $fileMedia['name'];
                        JFile::upload($fileMedia['tmp_name'], $pathMedia . $fileName);
                    }

                    $fileMedia1 = $_FILES['media_file_image_' . $i];
                    $fileImage = '';
                    if (!empty($fileMedia1['name'])) {
                        $fileImage = rand(10000, 1000000) . '_' . $fileMedia1['name'];
                        JFile::upload($fileMedia1['tmp_name'], $pathMedia . $fileImage);
                    }

                    $query->clear();
                    $query->select('max(id)')
                            ->from($db->quoteName('#__openshop_medias'));
                    $id = $db->setQuery($query)->loadResult();

                    $d = new stdClass();
                    $d->media_id = $data['cid'][0];
                    $d->media_image = $fileImage;
                    $d->media_file = $fileName;
                    $d->media_detail = $data['media_detail_' . $i];
                    if (!empty($fileName)) {
                        $db->insertObject('#__openshop_mediadetails', $d);
                    }
                }
            }
        } else {    //add new
            if ($data['media_type'] == '1') {
                for ($i = 1; $i <= $data['num_row_media']; $i++) {
                    $fileMedia = $_FILES['media_file_' . $i];
                    $fileName = rand(10000, 1000000) . '_' . $fileMedia['name'];
                    JFile::upload($fileMedia['tmp_name'], $pathMedia . $fileName);

                    $query->clear();
                    $query->select('max(id)')
                            ->from($db->quoteName('#__openshop_medias'));
                    $id = $db->setQuery($query)->loadResult();

                    $d = new stdClass();
                    $d->media_id = $id;
                    $d->media_image = $fileName;
                    $db->insertObject('#__openshop_mediadetails', $d);
                }
            } else if ($data['media_type'] == '2') {
                for ($i = 1; $i <= $data['num_row_media']; $i++) {

                    $fileMedia = $_FILES['media_file_' . $i];
                    $fileName = '';
                    if (!empty($fileMedia['name'])) {
                        $fileName = rand(10000, 1000000) . '_' . $fileMedia['name'];
                        JFile::upload($fileMedia['tmp_name'], $pathMedia . $fileName);
                    }

                    $fileMedia1 = $_FILES['media_file_image_' . $i];
                    $fileImage = '';
                    if (!empty($fileMedia1['name'])) {
                        $fileImage = rand(10000, 1000000) . '_' . $fileMedia1['name'];
                        JFile::upload($fileMedia1['tmp_name'], $pathMedia . $fileImage);
                    }

                    $query->clear();
                    $query->select('max(id)')
                            ->from($db->quoteName('#__openshop_medias'));
                    $id = $db->setQuery($query)->loadResult();

                    $d = new stdClass();
                    $d->media_id = $id;
                    $d->media_image = $fileImage;
                    $d->media_file = $fileName;
                    $d->media_detail = $data['media_detail_' . $i];
                    if (!empty($fileName)) {
                        $db->insertObject('#__openshop_mediadetails', $d);
                    }
                }
            }
        }

        return true;
    }

    /**
     * Method to remove brands
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $cids = explode(',', implode(',', $cid));

            foreach ($cids as $value) {
                $query->clear();
                $query->delete('#__openshop_medias')
                        ->where('id = ' . $value);
                if ($db->setQuery($query)->execute()) {
                    $query->clear();
                    $query->select('media_image')
                            ->from($db->quoteName('#__openshop_mediadetails'))
                            ->where('media_id = ' . $value);
                    $rows_img = $db->setQuery($query)->loadObjectList();
                    foreach ($rows_img as $img) {
                        JFile::delete(OPENSHOP_PATH_IMG_MEDIA . $img->media_image);
                    }

                    $query->clear();
                    $query->delete('#__openshop_mediadetails')
                            ->where('media_id = ' . $value);
                    if ($db->setQuery($query)->execute()) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 2;
                }
            }
        }
        //Removed success
        return 1;
    }

}
