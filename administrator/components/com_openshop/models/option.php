<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelOption extends OpenShopModel {

    public function __construct($config) {
        $config['translatable'] = true;
        $config['translatable_fields'] = array('option_name', 'option_desc');
        parent::__construct($config);
    }

    function store(&$data) {
        $imagePath = OPENSHOP_PATH_IMG_COLOR;
        if (JRequest::getInt('remove_image') && $data['id']) {
            //Remove image first
            $row = new OpenShopTable('#__openshop_options', 'id', $this->getDbo());
            $row->load($data['id']);

            if (JFile::exists($imagePath . $row->option_image))
                JFile::delete($imagePath . $row->option_image);

            if (JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->option_image) . '-100x100.' . JFile::getExt($row->option_image)))
                JFile::delete($imagePath . 'resized/' . JFile::stripExt($row->option_image) . '-100x100.' . JFile::getExt($row->option_image));
            $data['option_image'] = '';
        }

        $optionImage = $_FILES['option_image'];
        if ($optionImage['name']) {
            $checkFileUpload = OpenShopFile::checkFileUpload($optionImage);
            if (is_array($checkFileUpload)) {
                $mainframe = JFactory::getApplication();
                $mainframe->enqueueMessage(sprintf(JText::_('OPENSHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
                $mainframe->redirect('index.php?option=com_openshop&task=option.edit&cid[]=' . $data['id']);
            } else {
                if (is_uploaded_file($optionImage['tmp_name']) && file_exists($optionImage['tmp_name'])) {
                    if (JFile::exists($imagePath . $optionImage['name'])) {
                        $imageFileName = uniqid('image_') . '_' . JFile::makeSafe($optionImage['name']);
                    } else {
                        $imageFileName = JFile::makeSafe($optionImage['name']);
                    }
                    JFile::upload($optionImage['tmp_name'], $imagePath . $imageFileName);
                    // Resize images

                    $data['option_image'] = $imageFileName;
                    OpenShopHelper::resizeImage($imageFileName, OPENSHOP_PATH_IMG_COLOR, 100, 100);
                }
            }
        }
        $data['option_name'] = strtoupper($data['option_name']);
        parent::store($data);
        // Store option values
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $optionId = $data['id'];
        $optionValueId = JRequest::getVar('optionvalue_id');
        $published = JRequest::getVar('optionvalue_published');
        $ordering = JRequest::getVar('ordering');

        // Delete in option values table first
        $query->clear();
        $query->delete('#__openshop_optionvalues')
                ->where('option_id = ' . intval($optionId));
        if (count($optionValueId)) {
            $query->where('id NOT IN (' . implode(',', $optionValueId) . ')');
        }
        $db->setQuery($query);
        $db->query();
        // Delete in option values details
        $query->clear();
        $query->delete('#__openshop_optionvaluedetails')
                ->where('option_id = ' . intval($optionId));
        if (count($optionValueId)) {
            $query->where('optionvalue_id NOT IN (' . implode(',', $optionValueId) . ')');
        }
        $db->setQuery($query);
        $db->query();

        $languages = OpenShopHelper::getLanguages();
        if (JLanguageMultilang::isEnabled() && count($languages) > 1) {
            for ($i = 0; $n = count($ordering), $i < $n; $i++) {
                $row = new OpenShopTable('#__openshop_optionvalues', 'id', $db);
                $row->id = isset($optionValueId[$i]) ? $optionValueId[$i] : '';
                $row->option_id = $optionId;
                $row->published = $published[$i];
                $row->ordering = $ordering[$i];
                $row->store();
                foreach ($languages as $language) {
                    $langCode = $language->lang_code;
                    $optionValueDetailsId = JRequest::getVar('optionvaluedetails_id_' . $langCode);
                    $value = JRequest::getVar('value_' . $langCode);
                    $detailsRow = new OpenShopTable('#__openshop_optionvaluedetails', 'id', $db);
                    $detailsRow->id = isset($optionValueDetailsId[$i]) ? $optionValueDetailsId[$i] : '';
                    $detailsRow->optionvalue_id = $row->id;
                    $detailsRow->option_id = $optionId;
                    $detailsRow->value = $value[$i];
                    $detailsRow->language = $langCode;
                    $detailsRow->store();
                }
            }
        } else {
            $optionValueDetailsId = JRequest::getVar('optionvaluedetails_id');
            $value = JRequest::getVar('value');
            for ($i = 0; $n = count($ordering), $i < $n; $i++) {
                $row = new OpenShopTable('#__openshop_optionvalues', 'id', $db);
                $row->id = isset($optionValueId[$i]) ? $optionValueId[$i] : '';
                $row->option_id = $optionId;
                $row->published = $published[$i];
                $row->ordering = $ordering[$i];
                $row->store();
                $detailsRow = new OpenShopTable('#__openshop_optionvaluedetails', 'id', $db);
                $detailsRow->id = isset($optionValueDetailsId[$i]) ? $optionValueDetailsId[$i] : '';
                $detailsRow->optionvalue_id = $row->id;
                $detailsRow->option_id = $optionId;
                $detailsRow->value = $value[$i];

                $optionImage = $_FILES['img_color_' . $i];
                if($data['option_name'] == 'COLOR' && isset($optionImage['name']) && !empty($optionImage['name'])){
                    //upload color
                    $imagePath = OPENSHOP_PATH_IMG_COLOR;
                    $img_name = rand(0, 1000000) .'_'. $optionImage['name'];
                    JFile::upload($optionImage['tmp_name'], $imagePath . $img_name);
                    
                    $detailsRow->img_color = $img_name;
                }


                $detailsRow->language = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
                $detailsRow->store();
            }
        }
        return true;
    }

    /**
     * Method to remove options
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = $this->getDbo();
            $cids = implode(',', $cid);
            $query = $db->getQuery(true);
            $query->delete('#__openshop_options')
                    ->where('id IN (' . $cids . ')')
                    ->where('id NOT IN (SELECT  DISTINCT(option_id) FROM #__openshop_optionvalues)');
            $db->setQuery($query);
            if (!$db->query())
            //Removed error
                return 0;
            $numItemsDeleted = $db->getAffectedRows();
            //Delete option values records
            $query->clear();
            $query->delete('#__openshop_optiondetails')
                    ->where('option_id IN (' . $cids . ')')
                    ->where('option_id NOT IN (SELECT  DISTINCT(option_id) FROM #__openshop_optionvalues)');
            $db->setQuery($query);
            if (!$db->query())
            //Removed error
                return 0;
            if ($numItemsDeleted < count($cid)) {
                //Removed warning
                return 2;
            }
        }
        //Removed success
        return 1;
    }

    /**
     * Function to copy option and option values
     * @see OpenShopModel::copy()
     */
    function copy($id) {
        $copiedOptionId = parent::copy($id);
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_optionvalues')
                ->where('option_id = ' . intval($id));
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        for ($i = 0; $n = count($rows), $i < $n; $i++) {
            $row = $rows[$i];
            $optionValuesRow = new OpenShopTable('#__openshop_optionvalues', 'id', $db);
            $optionValuesRow->id = '';
            $optionValuesRow->option_id = $copiedOptionId;
            $optionValuesRow->published = $row->published;
            $optionValuesRow->ordering = $row->ordering;
            $optionValuesRow->store();
            $optionValuesId = $optionValuesRow->id;
            $sql = 'INSERT INTO #__openshop_optionvaluedetails'
                    . ' (optionvalue_id, option_id, value, language)'
                    . ' SELECT ' . $optionValuesId . ',' . $copiedOptionId . ', value, language'
                    . ' FROM #__openshop_optionvaluedetails'
                    . ' WHERE optionvalue_id = ' . intval($row->id);
            $db->setQuery($sql);
            $db->query();
        }
        return $copiedOptionId;
    }

}
