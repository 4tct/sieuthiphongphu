<?php
/**
 * @version		1.4.2
 * @package		Joomla
 * @subpackage	EShop
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die();

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelReview extends OpenShopModel
{

	function __construct($config)
	{
		$config['table_name'] = '#__openshop_reviews';
		parent::__construct($config);
	}
}