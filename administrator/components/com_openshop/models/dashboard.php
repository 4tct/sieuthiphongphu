<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Configuration Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelDashboard extends JModelLegacy {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 
     * Function to get shop statistics
     * @return array
     */
    public function getShopStatistics() {
        $data = array();
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        //Products
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_products')
                ->where('published = 1');
        $db->setQuery($query);
        $data['products'] = $db->loadResult();

        //Categories
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_categories')
                ->where('published = 1');
        $db->setQuery($query);
        $data['categories'] = $db->loadResult();

        //Manufacturers
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_manufacturers')
                ->where('published = 1');
        $db->setQuery($query);
        $data['manufacturers'] = $db->loadResult();

        //Customers
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_customers')
                ->where('published = 1');
        $db->setQuery($query);
        $data['customers'] = $db->loadResult();

        //Reviews
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_reviews')
                ->where('published = 1');
        $db->setQuery($query);
        $data['reviews'] = $db->loadResult();

        //Pending orders
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_orders')
                ->where('order_status_id = 8');
        $db->setQuery($query);
        $data['pending_orders'] = $db->loadResult();

        //Processed orders
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_orders')
                ->where('order_status_id = 9');
        $db->setQuery($query);
        $data['processed_orders'] = $db->loadResult();

        //Complete orders
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_orders')
                ->where('order_status_id = 4');
        $db->setQuery($query);
        $data['complete_orders'] = $db->loadResult();

        //Shipped orders
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_orders')
                ->where('order_status_id = 13');
        $db->setQuery($query);
        $data['shipped_orders'] = $db->loadResult();

        //Refunded orders
        $query->clear();
        $query->select('COUNT(*)')
                ->from('#__openshop_orders')
                ->where('order_status_id = 11');
        $db->setQuery($query);
        $data['refunded_orders'] = $db->loadResult();

        return $data;
    }

    /**
     * Function to get recent orders
     * @return orders object list
     */
    public function getRecentOrders() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.payment_fullname, a.total, a.created_date, b.orderstatus_name, a.currency_code, a.currency_exchanged_value')
                ->from('#__openshop_orders AS a')
                ->innerJoin('#__openshop_orderstatusdetails AS b ON (a.order_status_id = b.orderstatus_id AND b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '")')
                ->order('a.created_date DESC LIMIT 5');
        $db->setQuery($query);
        $data = $db->loadObjectList();
        return $data;
    }

    /**
     * 
     * Function to get recent reviews
     * @return reviews object list
     */
    public function getRecentReviews() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_reviews')
                ->order('created_date DESC LIMIT 5');
        $db->setQuery($query);
        $data = $db->loadObjectList();
        return $data;
    }

    public function getMonthlyReport($current_month_offset, $before, $after) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_orders')
                ->where('(order_status_id = ' . OpenShopHelper::getConfigValue('complete_status_id', 4) . ' OR order_status_id = 13)')
                ->where('created_date <= "' . $before . '"')
                ->where('created_date >= "' . $after . '"')
                ->order('created_date DESC');
        $db->setQuery($query);

        $data = $db->loadObjectList();

        return $data;
    }

    /**
     *
     * Function to get top sales products
     * @return products opject list
     */
    function getTopSales() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, b.product_name, SUM(quantity) AS sales')
                ->from('#__openshop_orderproducts AS a')
                ->innerJoin('#__openshop_productdetails AS b ON (a.product_id = b.product_id)')
                ->innerJoin('#__openshop_orders AS c ON (a.order_id = c.id)')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
                ->where('(c.order_status_id = ' . (int) OpenShopHelper::getConfigValue('complete_status_id', 4) . ' OR c.order_status_id = 13)')
                ->group('a.product_id')
                ->order('sales DESC LIMIT 0, 5');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     *
     * Function to get top hits products
     * @return products opject list
     */
    function getTopHits() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.hits, b.product_name')
                ->from('#__openshop_products AS a')
                ->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
                ->order('a.hits DESC LIMIT 0, 5');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /*
     * 
     * Function to get view website in today
     */

    function gettotleViewWeb() {
        try {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('*')
                    ->from($db->quoteName('#__openshop_develops'))
                    ->where('ip NOT IN ("66.249.79.12","66.249.79.13","66.249.79.11")')
                    ->where('date >= "' . date('Y-m-d') . ' 00:00:00' . '"')
                    ->where('date <= "' . date('Y-m-d') . ' 23:59:59' . '"');

            $rows = $db->setQuery($query)->loadObjectList();
            $cusView = 0;
            foreach ($rows as $k => $row) {
                if ($k == 0) {
                    $tempIp = $row->ip;
                    $tempUser_id = $row->user_id;
                    $arrIP = array($row->ip);
                    ++$cusView;
                } else {
                    if (!in_array($row->ip, $arrIP)) {
                        ++$cusView;
                        $tempIp = $row->ip;
                        $tempUser_id = $row->user_id;
                        array_push($arrIP, $row->ip);
                    }
                }
            }

            $res = array();
            $res['totalView'] = count($rows);
            $res['totalCusView'] = $cusView;
            return $res;
        } catch (Exception $ex) {
            return 'Error';
        }
    }

    /*
     * Function to get number order in to day
     */

    function gettotleOrder() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        try {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('count(id)')
                    ->from($db->quoteName('#__openshop_orders'))
                    ->where('created_date >= "' . date('Y-m-d') . ' 00:00:00' . '"')
                    ->where('created_date <= "' . date('Y-m-d') . ' 23:59:59' . '"');
            return $db->setQuery($query)->loadResult();
        } catch (Exception $ex) {
            return 'Error';
        }
    }

    /*
     * Function to get total user
     */

    function gettotalUser() {
        try {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('count(id)')
                    ->from($db->quoteName('#__users'));
            $rows = $db->setQuery($query)->loadResult();
            return $rows;
        } catch (Exception $ex) {
            return 0;
        }
    }

    /*
     * Function to get total products
     */

    function gettotalProduct() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_products'));
        return $db->setQuery($query)->loadResult();
    }

    /*
     * FUnction to get total categories
     */

    function gettotalCategory() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(b.id)')
                ->from($db->quoteName('#__openshop_categories', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_categorydetails', 'b') . ' ON a.id = b.category_id')
                ->where('a.published = 1');
        return $db->setQuery($query)->loadResult();
    }

}
