<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Coupon Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelCoupon extends OpenShopModel
{

	function store(&$data)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		if ($data['id'])
		{
			//Delete coupon products
			$query->delete('#__openshop_couponproducts')
				->where('coupon_id = ' . intval($data['id']));
			$db->setQuery($query);
			$db->query();
			//Delete coupon customer groups
			$query->clear();
			$query->delete('#__openshop_couponcustomergroups')
				->where('coupon_id = ' . intval($data['id']));
			$db->setQuery($query);
			$db->query();
		}
		// Check duplicated coupon
		$query->clear();
		$query->select('COUNT(*)')
			->from('#__openshop_coupons')
			->where('coupon_code = ' . $db->quote($data['coupon_code']));
		if ($data['id'])
		{
			$query->where('id != ' . intval($data['id']));
		}
		$db->setQuery($query);
		if ($db->loadResult())
		{
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage(JText::_('ESHOP_COUPON_EXISTED'), 'error');
			$mainframe->redirect('index.php?option=com_openshop&task=coupon.edit&cid[]=' . $data['id']);
		}
		parent::store($data);
		$couponId = $data['id'];
		//save new data
		if (isset($data['product_id']))
		{
			$productIds = $data['product_id'];
			if (count($productIds))
			{
				$query->clear();
				$query->insert('#__openshop_couponproducts')
					->columns('coupon_id, product_id');
				for ($i = 0; $i < count($productIds); $i++)
				{
					$productId = $productIds[$i];
					$query->values("$couponId, $productId");
				}
				$db->setQuery($query);
				$db->query();
			}
		}
		if (isset($data['customergroup_id']))
		{
			$customergroupIds = $data['customergroup_id'];
			if (count($customergroupIds))
			{
				$query->clear();
				$query->insert('#__openshop_couponcustomergroups')
					->columns('coupon_id, customergroup_id');
				for ($i = 0; $i < count($customergroupIds); $i++)
				{
					$customergroupId = $customergroupIds[$i];
					$query->values("$couponId, $customergroupId");
				}
				$db->setQuery($query);
				$db->query();
			}
		}
		return true;
	}
	
	/**
	 * Method to remove coupons
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		//Remove coupon products and history
		if (count($cid))
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->delete('#__openshop_couponproducts')
				->where('coupon_id IN (' . implode(',', $cid) . ')');
			$db->setQuery($query);
			$db->query();
			$query->clear();
			$query->delete('#__openshop_couponhistory')
				->where('coupon_id IN (' . implode(',', $cid) . ')');
			$db->setQuery($query);
			$db->query();
		}
		parent::delete($cid);
	}

}