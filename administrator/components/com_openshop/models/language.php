<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelLanguage extends JModelLegacy {

    protected $pagination;
    protected $total;

    /**
     * Constructor function
     *
     */
    function __construct() {
        parent::__construct();

        $app = JFactory::getApplication();
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', 100, 'int');
        $limitstart = $app->getUserStateFromRequest('com_openshop.language.limitstart', 'limitstart', 0, 'int');

        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
    }

    /**
     * Get pagination object
     *
     * @return JPagination
     */
    function getPagination() {
        // Lets load the content if it doesn't already exist
        if (empty($this->pagination)) {
            jimport('joomla.html.pagination');
            $this->pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }

        return $this->pagination;
    }

    public function getTotal() {
        return $this->total;
    }

    /**
     * Get language items and store them in an array
     *getTrans($language, $languageFile) 
     * getTrans($lang, $type , $object , $pos);
     */
    function getTrans($language, $type_name, $object_name, $postition) {
        if($type_name=="template")  $object_name="tpl_".$object_name;
        if($type_name=="library") $object_name="lib_".$object_name;
        $app = JFactory::getApplication();
        $search = $app->getUserStateFromRequest('com_openshop.language.search', 'search', '', 'string');
        $search = JString::strtolower($search);
        $registry = new JRegistry();
       
        if ($postition>0){
            $languageFolder = JPATH_ROOT . DS.'administrator'.DS.'language'.DS;
            $languageFile = $object_name;
        } else {
            $languageFolder = JPATH_ROOT . DS.'language'.DS;
            $languageFile = $object_name;
        }
        $path = $languageFolder .$language.DS.$language.'.'.$languageFile . '.ini';
        //echo $path;
        if(JFile::exists($path)){
            $registry->loadFile($path, 'INI');
        }else{
            $app->enqueueMessage(JText::_('FILE_NOT_FOUND'));
            //error_reporting(0);
        }
        $enGbItems = $registry->toArray();
        if ($search) {
            $search = strtolower($search);
            foreach ($enGbItems as $key => $value) {
                if (strpos(strtolower($key), $search) === false && strpos(strtolower($value), $search) === false) {
                    unset($enGbItems[$key]);
                }
            }
        }
        $this->total = count($enGbItems);
        $data['en-GB'][$languageFile] = array_slice($enGbItems, $this->getState('limitstart'), $this->getState('limit'));
        if ($language != 'en-GB') {
            $path = $languageFolder . $language . DS . $language . '.' . $languageFile . '.ini';
        //    print_r($path);
            if (JFile::exists($path)) {
                $registry->loadFile($path);
                $languageItems = $registry->toArray();
                if ($search) {
                    $search = strtolower($search);
                    foreach ($languageItems as $key => $value) {
                        if (strpos(strtolower($key), $search) === false && strpos(strtolower($value), $search) === false) {
                            unset($languageItems[$key]);
                        }
                    }
                }
                $data[$language][$languageFile] = array_slice($languageItems, $this->getState('limitstart'), $this->getState('limit'));
           } else {
                $data[$language][$languageFile] = array();
            }
        }
        return $data;
    }

    /**
     *  Get site languages
     *
     */
    function getSiteLanguages() {
        jimport('joomla.filesystem.folder');
        $path = JPATH_ROOT . '/language';
        $folders = JFolder::folders($path);
        $rets = array();
        foreach ($folders as $folder)
            if ($folder != 'pdf_fonts' && $folder != 'overrides')
                $rets[] = $folder;
        return $rets;
    }

    /**
     * Save translation data
     *
     * @param array $data
     */
    function save($data) {
        $app = JFactory::getApplication();
       
        $language           = $data['lang'];
        $languageFile       = $data['object_name'];
        $languageType       = $data['type_name'];
        $languageObjName    = $data['opject_name'];
        $langpos            = $data['pos'];
                
      //  print_r($data);exit();
        if ($langpos) {
            $languageFolder = JPATH_ROOT . DS.'administrator'.DS.'language'.DS;
            
        } else {
            $languageFolder = JPATH_ROOT . DS.'language'.DS;
        }
        $registry = new JRegistry();
        $filePath = $languageFolder . $language . DS . $language . '.' . $languageFile . '.ini';
        if (JFile::exists($filePath)) {
            $registry->loadFile($filePath, 'INI');
        } else {
            $registry->loadFile($languageFolder . $language.DS.$langpos.'.' . $languageFile . '.ini', 'INI');
        }
        //Get the current language file and store it to array
        $keys = $data['keys'];
        $content = "";
        foreach ($keys as $key) {
            $key = trim($key);
            $value = trim($data[$key]);
            $registry->set($key, $value);
        }
        if (isset($data['extra_keys'])) {
            $keys = $data['extra_keys'];
            $values = $data['extra_values'];
            for ($i = 0, $n = count($keys); $i < $n; $i++) {
                $key = trim($keys[$i]);
                $value = trim($values[$i]);
                $registry->set($key, $value);
            }
        }

        if ($language != 'en-GB') {
            //We need to add new language items which are not existing in the current language
            $enRegistry = new JRegistry();
            $enRegistry->loadFile($languageFolder . 'en-GB'.DS.'en-GB.' . $languageFile . '.ini', 'INI');
            $enLanguageItems = $enRegistry->toArray();
            $currentLanguageItems = $registry->toArray();
            foreach ($enLanguageItems as $key => $value) {
                $currentLanguageKeys = array_keys($currentLanguageItems);
                if (!in_array($key, $currentLanguageKeys)) {
                    $registry->set($key, $value);
                }
            }
        }
        JFile::write($filePath, $registry->toString('INI'));
        $app->redirect('index.php?option=com_openshop&view=language&lang='.$language.'&object_name='.JRequest::getVar('object_name'),true);
    }

    function getType() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('distinct type')
                ->from('#__extensions');
        $db->setQuery($query);

        $rows = $db->loadObjectList();
        return $rows;
    }

    function getObjectName($ob_name){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('element')
                ->from('#__extensions')
                ->where ('type ="'.$db->escape($ob_name).'"');
        $db->setQuery($query);

        $rows = $db->loadObjectList();
        return $rows;
    }

}
