<?php

defined('_JEXEC') or die('Restricted access');

class OpenShopModelCarts extends OpenShopModelList {

    function __construct($config) {
        $config['search_fields'] = array();
        $config['state_vars'] = array(
//            'id_status' => array('', 'cmd', 1),
        );
        parent::__construct($config);
    }

//    public function getData() {
//        if (empty($this->_data)) {
//            $query = $this->_buildQuery();
//            $this->_data = $this->_getList($query, 0, 20);
//        }
//
//        return $this->_data;
//    }

    public function _buildQuery() {
        $app = JFactory::getApplication()->input;
        $transporter_id = $app->getInt('id_transporter');
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);
        $query->select('a.*,z.zone_name,dt.district_name,d.transporter_name,e.telephone,f.appointment_type')
                ->from($db->quoteName('#__openshop_orders', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_zones', 'z') . ' ON a.payment_zone_id = z.id')
                ->join('LEFT', $db->quoteName('#__openshop_districts', 'dt') . ' ON a.payment_district_id = dt.id')
                ->join('LEFT', $db->quoteName('#__openshop_carts', 'c') . 'ON c.order_id = a.id')
                ->join('LEFT', $db->quoteName('#__openshop_transporters', 'd') . 'ON d.id = c.transporter_id')
                ->join('LEFT', $db->quoteName('#__openshop_customers', 'e') . 'ON e.customer_id = a.customer_id')
                ->join('LEFT', $db->quoteName('#__openshop_appointments','f') . 'ON f.cart_id = c.id')
                ->where('c.transporter_id = ' . $transporter_id)
                ->order('order_status_id DESC');
        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

    function _buildContentWhereArray() {
        $db = $this->getDbo();
        $state = $this->getState();
        $where = array();
        if ($state->filter_state == 'P')
            $where[] = ' a.published=1 ';
        elseif ($state->filter_state == 'U')
            $where[] = ' a.published = 0';
        if ($state->search) {
            $search = $db->quote('%' . $db->escape(strtolower($state->search), true) . '%', false);
            if (is_array($this->searchFields)) {
                $whereOr = array();
                foreach ($this->searchFields as $titleField) {
                    $whereOr[] = " LOWER($titleField) LIKE " . $search;
                }
                $where[] = ' (' . implode(' OR ', $whereOr) . ') ';
            } else {
                $where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape(strtolower($state->search), true) . '%', false);
            }
        }

        if ($this->translatable) {
            $where[] = 'b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"';
        }

        

        return $where;
    }

    function store(&$data) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $id_arr = array();
        $data_op = new stdClass();
        for ($i = 1; $i <= $data['numrow']; $i++) {
            if ($data['idpro_auto'] != "" &&
                    $data['sl_' . $i] != 0 && $data['price'] != "" && $data['id_warehouse'] != 0 && $data['namemanufac'] != 0) {
                $data_op->order_date = date('Y-m-d h:i:s');
                $data_op->id_product = $data['idpro_auto'];
                $data_op->manufacturer_sku = $data['namemanufac'];
                $data_op->id_optioncolor = $data['slcolor_' . $i];
                $data_op->id_optionsize = $data['slsize_' . $i];
                $data_op->quantity = $data['sl_' . $i];
                $data_op->price_input = $data['price'];
                $data_op->id_status = $data['statusios'];
                $data_op->date_input = date('Y-m-d H:i:s');
                $data_op->created_date = date('Y-m-d H:i:s');
                $data_op->modified_date = date('Y-m-d H:i:s');
                $data_op->created_by = JFactory::getUser()->id;
                $data_op->id_option = 1;
                $data_op->id_warehouse = $data['id_warehouse'];
                $data_op->invoice_sku = $data['invoice_sku'] . '-' . $data['lenofday'];

                JFactory::getDbo()->insertObject('#__openshop_ios', $data_op);

                array_push($id_arr, $this->getIdIoAdd());
            }
        }
        echo json_encode($id_arr);

        return FALSE;
    }

    function getIdIoAdd() {
        $db = $this->getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('max(id)')
                ->from($db->quoteName('#__openshop_ios'));
        $db->setQuery($query);
        return $db->loadResult();
    }

}
