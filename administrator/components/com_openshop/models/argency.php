<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelArgency extends OpenShopModel
{

	public function __construct($config)
	{
//		$config['translatable'] = true;
//		$config['translatable_fields'] = array('argency_name', 'argency_alias', 'argency_desc', 'argency_phone', 'argency_email', 'argency_address');
		
		parent::__construct($config);
	}

//	function store(&$data)
//	{
//		$imagePath = JPATH_ROOT . '/media/com_openshop/argencies/';
//		if (JRequest::getInt('remove_image') && $data['id'])
//		{
//			//Remove image first
//			$row = new OpenShopTable('#__openshop_argencies', 'id', $this->getDbo());
//			$row->load($data['id']);
//			
//			if (JFile::exists($imagePath . $row->argency_image))
//				JFile::delete($imagePath . $row->argency_image);
//			
//			if (JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->argency_image).'-100x100.'.JFile::getExt($row->argency_image)))
//				JFile::delete($imagePath . 'resized/' . JFile::stripExt($row->argency_image).'-100x100.'.JFile::getExt($row->argency_image));
//			$data['argency_image'] = '';
//		}
//		
//		$argencyImage = $_FILES['argency_image'];
//		if ($argencyImage['name'])
//		{
//			$checkFileUpload = OpenShopFile::checkFileUpload($argencyImage);
//			if (is_array($checkFileUpload))
//			{
//				$mainframe = JFactory::getApplication();
//				$mainframe->enqueueMessage(sprintf(JText::_('ESHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
//				$mainframe->redirect('index.php?option=com_openshop&task=argency.edit&cid[]=' . $data['id']);
//			}
//			else
//			{
//				if (is_uploaded_file($argencyImage['tmp_name']) && file_exists($argencyImage['tmp_name']))
//				{
//					if (JFile::exists($imagePath . $argencyImage['name']))
//					{
//						$imageFileName = uniqid('image_') . '_' . JFile::makeSafe($argencyImage['name']);
//					}
//					else
//					{
//						$imageFileName = JFile::makeSafe($argencyImage['name']);
//					}
//					JFile::upload($argencyImage['tmp_name'], $imagePath . $imageFileName);
//					// Resize images
//				
//					$data['argency_image'] = $imageFileName;
//					OpenShopHelper::resizeImage($imageFileName, JPATH_ROOT . '/media/com_openshop/argencies/', 100, 100);
//				}
//			}
//		}
//		if (count($data['argency_customergroups']))
//		{
//			$data['argency_customergroups'] = implode(',', $data['argency_customergroups']);
//		}
//		else
//		{
//			$data['argency_customergroups'] = '';
//		}
//		parent::store($data);
//		return true;
//	}
	
	/**
	 * Method to remove argencies
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		if (count($cid))
		{
			$db = $this->getDbo();
			$cids = implode(',', $cid);
			$query = $db->getQuery(true);
			$query->select('id')
				->from('#__openshop_argencies')
				->where('id IN (' . $cids . ')');
			$db->setQuery($query);
			$argencies = $db->loadColumn();
			if (count($argencies))
			{
				$query->clear();
				$query->delete('#__openshop_argencies')
					->where('id IN (' . implode(',', $argencies) . ')');
				$db->setQuery($query);
				if (!$db->query())
					//Removed error
					return 0;
				$numItemsDeleted = $db->getAffectedRows();
				//Delete details records
//				$query->clear();
//				$query->delete('#__openshop_argencydetails')
//					->where('argency_id IN (' . implode(',', $argencies) . ')');
//				$db->setQuery($query);
//				if (!$db->query())
//					//Removed error
//					return 0;
				//Remove SEF urls for categories
				for ($i = 0; $n = count($argencies), $i < $n; $i++)
				{
					$query->clear();
					$query->delete('#__openshop_urls')
						->where('query LIKE "view=argency&id=' . $argencies[$i] . '"');
					$db->setQuery($query);
					$db->query();
				}
				if ($numItemsDeleted < count($cid))
				{
					//Removed warning
					return 2;
				}
			}
			else 
			{
				return 2;
			}
		}
		//Removed success
		return 1;
	}
}