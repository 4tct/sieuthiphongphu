<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelStatistics extends OpenShopModelList {
    
    public function getData() {
        
    }
    
    public function getTotal() {
        
    }
    
    public function getdataView(){
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_develops'))
                ->where('ip NOT IN ("66.249.79.12","66.249.79.13","66.249.79.11")')
                ->where('date >= "'. date('Y-m-d').' 00:00:00' .'"')
                ->where('date <= "'. date('Y-m-d').' 23:59:59' .'"');
        return $db->setQuery($query)->loadObjectList();
    }
    
    public function getdataViewProduct(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.hits,b.product_name')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . ' ON a.id = b.product_id')
                ->order('hits DESC');
        return $db->setQuery($query,0,15)->loadObjectList();
    }
}
