<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelZones extends OpenShopModelList
{

	function __construct($config)
	{
		$config['state_vars'] = array('filter_order' => array('b.country_name', 'cmd', 1), 'country_id' => array(0, 'int', 1));
		$config['search_fields'] = array('a.zone_name');
		parent::__construct($config);
	}

	/**
	 * Build query to get list of records to display
	 *
	 * @see OpenShopModelList::_buildQuery()
	 */
	function _buildQuery()
	{
		$db = $this->getDbo();
		$state = $this->getState();
		$where = $this->_buildContentWhereArray();
		$query = $db->getQuery(true);
		$query->select('a.*, b.country_name')
			->from('#__openshop_zones AS a')
			->join('LEFT', '#__openshop_countries AS b ON a.country_id = b.id ');
		if (count($where))
			$query->where($where);
		$query->order($state->filter_order . ' ' . $state->filter_order_Dir);
		return $query;
	}

	function _buildContentWhereArray()
	{
		$state = $this->getState();
		$where = parent::_buildContentWhereArray();
		if ($state->country_id)
		{
			$where[] = ' a.country_id = ' . intval($state->country_id);
		}
		return $where;
	}
}