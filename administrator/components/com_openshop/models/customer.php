<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelCustomer extends OpenShopModel {

    function __construct($config) {
        parent::__construct($config);
    }

    /**
     * Load the data
     *
     */
    public function _loadData() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*, b.name, b.username,c.zone_id,c.district_id,c.address')
                ->from($this->_tableName . ' AS a')
                ->leftJoin('#__users AS b ON (a.customer_id = b.id)')
                ->innerJoin('#__openshop_addresses as c ON (c.customer_id = a.customer_id)')
                ->where('a.id = ' . intval($this->_id));
        $db->setQuery($query);
        $row = $db->loadObject();
        $this->_data = $row;
    }

    /**
     * Init Category data
     *
     */
    public function _initData() {
        $db = $this->getDbo();
        $row = new OpenShopTable($this->_tableName, 'id', $db);
        $this->_data = $row;
    }

    /**
     * Function to store product
     * @see OpenShopModel::store()
     */
    function store(&$data) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        if (!$data['id'] && $data['username'] && $data['password']) {
            // Store this account into the system
            jimport('joomla.user.helper');
            $params = JComponentHelper::getParams('com_users');
            $newUserType = $params->get('new_usertype', 2);

            $data['groups'] = array();
            $data['groups'][] = $newUserType;
            $data['block'] = 0;
            $data['name'] = $data['firstname'] . ' ' . $data['lastname'];
            $data['password1'] = $data['password2'] = $data['password'];
            $data['email1'] = $data['email2'] = $data['email'];
            $user = new JUser();
            $user->bind($data);
            if (!$user->save()) {
                JFactory::getApplication()->enqueueMessage($user->getError(), 'error');
                JFactory::getApplication()->redirect('index.php?option=com_openshop&view=customers');
            }
            $data['customer_id'] = $user->id;
        }
        // Check and store address

        if (isset($data['cid'][0])) {
            $query->select('customer_id')
                    ->from($db->quoteName('#__openshop_customers'))
                    ->where('id = ' . $data['cid'][0]);
            $id_cus = $db->setQuery($query)->loadResult();
            $d = new stdClass();
            $d->customer_id = $id_cus;
            $d->zone_id = $data['zone_id'];
            $d->district_id = $data['district_id'];
            $d->address = $data['address'];
            $d->modified_date = JFactory::getDate()->toSql();
            $db->updateObject('#__openshop_addresses', $d, 'customer_id');
        } else {
            $address = $data['address'];
            $row = JTable::getInstance('OpenShop', 'Address');
            $row->bind($address);
            $row->customer_id = $data['customer_id'];
            $row->created_date = JFactory::getDate()->toSql();
            $row->modified_date = JFactory::getDate()->toSql();
            $row->store();
            if ($i == 1) {
                $addressId = $row->id;
            }
        }

        $data['address_id'] = $addressId;
        parent::store($data);
        return true;
    }

}
