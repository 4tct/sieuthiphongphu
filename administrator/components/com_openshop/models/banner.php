<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelBanner extends OpenShopModel {

    public function __construct($config) {
        $config['translatable'] = FALSE;
        $config['translatable_fields'] = array('brand_name', 'brand_alias', 'brand_desc', 'brand_page_title', 'brand_page_heading');

        parent::__construct($config);
    }

    function store(&$data) {
        $user = JFactory::getSession()->get('user');
        $imagePath = JPATH_ROOT . '/images/com_openshop/banners/';
        if (JRequest::getInt('remove_image') && $data['id']) {
            //Remove image first
            $row = new OpenShopTable('#__openshop_banners', 'id', $this->getDbo());
            $row->load($data['id']);
            if (JFile::exists($imagePath . $row->banner_image))
                JFile::delete($imagePath . $row->banner_image);
            $data['banner_image'] = '';
        }
        
        $bannerImage = $_FILES['banner_img'];
        if ($bannerImage['name']) {
            $checkFileUpload = OpenShopFile::checkFileUpload($bannerImage);
            if (is_array($checkFileUpload)) {
                $mainframe = JFactory::getApplication();
                $mainframe->enqueueMessage(sprintf(JText::_('ESHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
                $mainframe->redirect('index.php?option=com_openshop&task=banner.edit&cid[]=' . $data['id']);
            } else {
                if (is_uploaded_file($bannerImage['tmp_name']) && file_exists($bannerImage['tmp_name'])) {
                    if (JFile::exists($imagePath . $bannerImage['name'])) {
                        $imageFileName = uniqid('image_') . '_' . JFile::makeSafe($bannerImage['name']);
                    } else {
                        $imageFileName = JFile::makeSafe($bannerImage['name']);
                    }
//                    JFile::upload($bannerImage['tmp_name'], $imagePath . $imageFileName);
                    // Resize images

                    $data['banner_image'] = $imageFileName;
                }
            }
        }

        $data['created_by'] = $user->id;
        $data['alias'] = JFilterOutput::stringURLSafe( $data['title'] );
        $data['time_start'] = date('Y-m-d', strtotime( $data['time_start'] ));
        $data['time_finish'] = date('Y-m-d', strtotime( $data['time_finish'] ));
        
        if(isset($data['arr_product'])){
            $data['arr_product'] = implode(',', $data['arr_product']);
        }
        else{
            $data['arr_product'] = '';
        }

        parent::store($data);
        return true;
    }

    /**
     * Method to remove brands
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = $this->getDbo();
            $cids = implode(',', $cid);
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_brands')
                    ->where('id IN (' . $cids . ')');
            $db->setQuery($query);
            $brands = $db->loadColumn();
            if (count($brands)) {
                $query->clear();
                $query->delete('#__openshop_brands')
                        ->where('id IN (' . implode(',', $brands) . ')');
                $db->setQuery($query);
                if (!$db->query())
                //Removed error
                    return 0;
                $numItemsDeleted = $db->getAffectedRows();
                //Delete details records
                $query->clear();
                $query->delete('#__openshop_branddetails')
                        ->where('brand_id IN (' . implode(',', $brands) . ')');
                $db->setQuery($query);
                if (!$db->query())
                //Removed error
                    return 0;
                //Remove SEF urls for categories
                for ($i = 0; $n = count($brands), $i < $n; $i++) {
                    $query->clear();
                    $query->delete('#__openshop_urls')
                            ->where('query LIKE "view=brand&id=' . $brands[$i] . '"');
                    $db->setQuery($query);
                    $db->query();
                }
                if ($numItemsDeleted < count($cid)) {
                    //Removed warning
                    return 2;
                }
            } else {
                return 2;
            }
        }
        //Removed success
        return 1;
    }

}
