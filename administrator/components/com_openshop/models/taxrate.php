<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Taxrate Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelTaxrate extends OpenShopModel
{

	function __construct($config)
	{		
		$config['table_name'] = '#__openshop_taxes';		
				
		parent::__construct($config);
	}

	function store(&$data)
	{
		parent::store($data);
		$taxRateId = $data['id'];
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->delete('#__openshop_taxcustomergroups')
			->where('tax_id = ' . intval($taxRateId));
		$db->setQuery($query);
		$db->query();
		if (isset($data['customergroup_id']))
		{
			$query->clear();
			$query->insert('#__openshop_taxcustomergroups')
				->columns('tax_id, customergroup_id');
			foreach ($data['customergroup_id'] as $groupId)
			{
				$query->values("$taxRateId, $groupId");
			}
			$db->setQuery($query);
			$db->query();			
		}
		
		return true;
	}
	
	/**
	 * 
	 * Function to copy a tax
	 * @see OpenShopModel::copy()
	 */
	public function copy($id)
	{
		//Copy from the main table
		$db = $this->getDbo();
		$row = new EShopTable('#__openshop_taxes', 'id', $db);
		$rowOld = new EShopTable('#__openshop_taxes', 'id', $db);
		$rowOld->load($id);
		$data = JArrayHelper::fromObject($rowOld);
		$data['id'] = 0;
		$data['tax_name'] = $data['tax_name'] . JText::_('ESHOP_COPY');
		$row->bind($data);
		$row->check();
		$row->store();
		$copiedTaxId = $row->id;
		$sql = 'INSERT INTO #__openshop_taxcustomergroups'
			. ' (tax_id, customergroup_id)'
			. ' SELECT ' . $copiedTaxId . ', customergroup_id'
			. ' FROM #__openshop_taxcustomergroups'
			. ' WHERE tax_id = ' . intval($id);
		$db->setQuery($sql);
		$db->query();
		return $copiedTaxId;
	}
}