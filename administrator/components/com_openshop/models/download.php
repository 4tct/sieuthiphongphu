<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelDownload extends OpenShopModel
{

	public function __construct($config)
	{
		$config['translatable'] = true;
		$config['translatable_fields'] = array('download_name');
		
		parent::__construct($config);
	}

	function store(&$data)
	{
		$file = $_FILES['file'];
		$message = '';
		if (!empty($file['name']))
		{
			$fileName = JFile::makeSafe($file['name']);
			if ((strlen($fileName) < 3) || (strlen($fileName) > 64))
			{
				$message = JText::_('OPENSHOP_UPLOAD_ERROR_FILENAME');
			}
			//Allowed file extension types
			$allowed = array();
			$fileTypes = explode("\n", OpenShopHelper::getConfigValue('file_extensions_allowed'));
			foreach ($fileTypes as $fileType)
			{
				$allowed[] = trim($fileType);
			}
			if (!in_array(substr(strrchr($fileName, '.'), 1), $allowed))
			{
				$message = JText::_('OPENSHOP_UPLOAD_ERROR_FILETYPE');
			}
			// Allowed file mime types
			$allowed = array();
			$fileTypes = explode("\n", OpenShopHelper::getConfigValue('file_mime_types_allowed'));
			foreach ($fileTypes as $fileType)
			{
				$allowed[] = trim($fileType);
			}
			if (!in_array($file['type'], $allowed))
			{
				$message = JText::_('OPENSHOP_UPLOAD_ERROR_FILE_MIME_TYPE');
			}
			if ($file['error'] != UPLOAD_ERR_OK)
			{
				$message = JText::_('OPENSHOP_ERROR_UPLOAD_' . $file['error']);
			}
			if (JFile::exists(JPATH_ROOT . '/media/com_openshop/downloads/' . $fileName))
			{
				if (!isset($_POST['overwrite']))
				{
					$message = JText::_('OPENSHOP_FILE_EXISTED');
				}
			}
			if ($message == '')
			{
				if (version_compare(JVERSION, '3.4.4', 'ge'))
				{
					JFile::upload($file['tmp_name'], JPATH_ROOT . '/media/com_openshop/downloads/' . $fileName, false, true);
				}
				else
				{
					JFile::upload($file['tmp_name'], JPATH_ROOT . '/media/com_openshop/downloads/' . $fileName);
				}
				$data['filename'] = $fileName;
			}
			else 
			{
				$mainframe = JFactory::getApplication();
				$mainframe->enqueueMessage($message, 'error');
				$mainframe->redirect('index.php?option=com_openshop&view=download&cid[]='.$data['id']);
			}
		}
		else 
		{
			if ($data['existed_file'] != '')
			{
				$data['filename'] = $data['existed_file'];
			}
		}
		parent::store($data);
		return true;
	}
	
	/**
	 * Method to remove downloads
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		//Remove download elements
		if (count($cid))
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->delete('#__openshop_productdownloads')
				->where('download_id IN (' . implode(',', $cid) . ')');
			$db->setQuery($query);
			$db->query();
		}
		parent::delete($cid);
	}
}