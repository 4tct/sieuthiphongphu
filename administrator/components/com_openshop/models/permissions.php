<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
JLoader::import('group', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'models');

class OpenShopModelPermissions extends JModelLegacy {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    function getUsersGroup() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $id_usergroup = array();
        foreach (JFactory::getUser()->groups as $value) {
            $id_usergroup[] = $value;
        }

        $query->select('count(id)')
                ->from($db->quoteName('#__usergroups'))
                ->where('id = ' . $id_usergroup[0])
                ->where('UPPER(title) = "SUPER USERS"');
        $check_user = $db->setQuery($query)->loadResult();

        $query->clear();
        $query->select('lft,rgt')
                ->from('#__usergroups')
                ->order('id');

        if (!$check_user) {
            $query->where('id = 6');
        }

        $row_mana = $db->setQuery($query)->loadObject();

        $query->clear();
        $query->select('*')
                ->from('#__usergroups')
                ->where('lft >= ' . $row_mana->lft)
                ->where('rgt <= ' . $row_mana->rgt)
                ->order('id');

        $db->setQuery($query);

        return $db->loadObjectList();
    }

    function getMenuView() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("id, menu_name")
                ->from('#__openshop_menus')
                ->where('menu_view <> ""');
        $db->setQuery($query);
        $rs = $db->loadObjectList();
        return $rs;
    }

    function addPermissions() {
        $menuView = $this->getMenuView();
        $permissions = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_openshop' . DS . 'permissions.xml';
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->startDocument("1.0", "utf-8");
        $writer->startElement("access");
        $writer->writeAttribute("component", "com_openshop");
        $writer->writeRaw("\n\t");
        $writer->startElement("section");
        $writer->writeAttribute("name", "component");
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.select" title="JACTION_SELECT" description="JACTION_SELECT_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.create" title="JACTION_CREATE" description="JACTION_CREATE_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.delete" title="JACTION_DELETE" description="JACTION_DELETE_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.edit" title="JACTION_EDIT" description="JACTION_EDIT_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.edit.state" title="JACTION_EDITSTATE" description="JACTION_EDITSTATE_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.edit.own" title="JACTION_EDITOWN" description="JACTION_EDITOWN_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.options" title="JACTION_OPTIONS" description="JACTION_OPTIONS_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.manage" title="JACTION_MANAGE" description="JACTION_MANAGE_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t\t");
        $writer->writeRaw('<action name="core.admin" title="JACTION_ADMIN" description="JACTION_ADMIN_COMPONENT_DESC" />');
        $writer->writeRaw("\n\t");
        $writer->endElement();
        $writer->writeRaw("\n\t");


        foreach ($menuView as $view) {
            $writer->startElement("section");
            $writer->writeAttribute("name", strtolower(substr($view->menu_name, strpos(".", $view->menu_name))));
            $writer->writeRaw("\n\t");
            $writer->writeRaw("\t");
            $writer->writeRaw('<action name="core.select" title="JACTION_SELECT" description="JACTION_SELECT_COMPONENT_DESC" />');
            $writer->writeRaw("\n\t\t");
            $writer->writeRaw('<action name="core.create" title="JACTION_CREATE" description="JACTION_CREATE_COMPONENT_DESC" />');
            $writer->writeRaw("\n\t\t");
            $writer->writeRaw('<action name="core.delete" title="JACTION_DELETE" description="JACTION_DELETE_COMPONENT_DESC" />');
            $writer->writeRaw("\n\t\t");
            $writer->writeRaw('<action name="core.edit" title="JACTION_EDIT" description="JACTION_EDIT_COMPONENT_DESC" />');
            $writer->writeRaw("\n\t");
            $writer->endElement();
            $writer->writeRaw("\n\t");
        }



        $writer->endElement();
        $writer->writeRaw("\n");
        $writer->endDocument();
        $file = fopen($permissions, "w");
        fwrite($file, $writer->outputMemory());
        fclose($file);
        return true;
    }

    function getAccessInMenu() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('menu_name,permission')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('published = 1')
                ->where('access = 1');

        $rows = $db->setQuery($query)->loadObjectList();

        $res = array();

        foreach ($rows as $key => $value) {
            $res[strtolower($value->menu_name)] = $value->permission;
        }

        return $res;
    }

    function checkPermission($name) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__usergroups'))
                ->where('LOWER(title) = "' . strtolower($name) . '"');
        $res = $db->setQuery($query)->loadResult();
        return $res;
    }

    function addPermissionOperactionAdministrator() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('id')->from($db->quoteName('#__usergroups'))->where('LOWER(title) = "administrator"');
        $id_admin = $db->setQuery($query)->loadResult();

        if ($id_admin) {
            $data = array();
            $data['title'] = 'Operation Administrator';
            $data['parent_id'] = $id_admin;
            $data['tags'] = '';
            $data['id'] = 0;

            //get other model in different component
            JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'models');

            $usergroup_model = JModelLegacy::getInstance('group', 'UsersModel');    //get model
            $usergroup_model->save($data);
        }
    }

}
