<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelTaxrates extends OpenShopModelList
{

	function __construct($config)
	{
		$config['main_table'] = '#__openshop_taxes';
		$config['state_vars'] = array('filter_order' => array('a.tax_name', 'cmd', 1), 'geozone_id' => array(0, 'int', 1));
		$config['search_fields'] = array('b.geozone_name');
		parent::__construct($config);
	}

	/**
	 * Build query to get list of records to display
	 *
	 * @see EShopModelList::_buildQuery()
	 */
	function _buildQuery()
	{
		$db = $this->getDbo();
		$state = $this->getState();
		$where = $this->_buildContentWhereArray();
		$query = $db->getQuery(true);
		$query->select('a.*, b.geozone_name')
			->from('#__openshop_taxes AS a')
			->join('LEFT', '#__openshop_geozones AS b ON a.geozone_id = b.id ');
		if (count($where))
			$query->where($where);		
		$query->order($state->filter_order . ' ' . $state->filter_order_Dir);
		
		return $query;
	
	}

	function _buildContentWhereArray()
	{
		$state = $this->getState();
		$where = parent::_buildContentWhereArray();
		if ($state->geozone_id)
		{
			$where[] = ' a.geozone_id = ' . intval($state->geozone_id);
		}
		
		return $where;
	}
}