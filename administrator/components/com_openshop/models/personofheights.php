<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelPersonOfHeights extends OpenShopModelList
{

	function __construct($config)
	{
//		$config['state_vars'] = array('filter_order' => array('b.country_name', 'cmd', 1), 'country_id' => array(0, 'int', 1));
//		$config['search_fields'] = array('a.zone_name');
		parent::__construct($config);
	}

}