<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelDiscountProducts extends OpenShopModelList {

    /**
     *
     * Constructor
     * 
     * @param array $config        	
     */
    function __construct($config) {
        $config['search_fields'] = array('c.product_sku', 'b.product_name');
//        $config['translatable'] = true;
        $config['state_vars'] = array(
            'argency_id' => array('', 'cmd', 1),
            'manufacturer_id' => array('', 'cmd', 1),
            'brand_id' => array('', 'cmd', 1),
            'category_id' => array('', 'cmd', 1),
            'stock_status' => array('', 'cmd', 1));
//        $config['translatable_fields'] = array(
//            'product_name',
//            'product_alias',
//            'product_desc',
//            'product_short_desc',
//            'meta_key',
//            'meta_desc');
        parent::__construct($config);
    }

    /**
     * Get total entities
     *
     * @return int
     */
    public function getTotal() {
        // Lets load the content if it doesn't already exist
        if (empty($this->_total)) {
            $db = $this->getDbo();
            $state = $this->getState();
            $where = $this->_buildContentWhereArray();
            $query = $db->getQuery(true);
            $query->select('COUNT(*)');
            $query->from($this->mainTable . ' AS a')
            ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.product_id = b.product_id')
            ->join('INNER', $db->quoteName('#__openshop_products', 'c') . 'ON c.id = b.product_id');

            if (count($where))
                $query->where($where);

            $db->setQuery($query);
            $this->_total = $db->loadResult();
        }
        return $this->_total;
    }

    /**
     * Basic build Query function.
     * The child class must override it if it is necessary
     *
     * @return string
     */
    public function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);

        $query->select('a.*,b.product_name,c.product_sku,c.product_image,c.product_price')
                ->from($this->mainTable . ' AS a')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.product_id = b.product_id')
                ->join('INNER', $db->quoteName('#__openshop_products', 'c') . 'ON c.id = b.product_id')
                ->where('a.delete_status = 0')
                ->order('published DESC');

        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

    /**
     * Build an where clause array
     *
     * @return array
     */
    public function _buildContentWhereArray() {
        $db = $this->getDbo();
        $state = $this->getState();
        $where = array();
        if ($state->filter_state == 'P')
            $where[] = ' a.published=1 ';
        elseif ($state->filter_state == 'U')
            $where[] = ' a.published = 0';

        if ($state->search) {
            $search = $db->quote('%' . $db->escape($state->search, true) . '%', false);
            if (is_array($this->searchFields)) {
                $whereOr = array();
                foreach ($this->searchFields as $titleField) {
                    $whereOr[] = " LOWER($titleField) LIKE " . $search;
                }
                $where[] = ' (' . implode(' OR ', $whereOr) . ') ';
            } else {
                $where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape($state->search, true) . '%', false);
            }
        }

        return $where;
    }

}
