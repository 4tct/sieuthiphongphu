<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Category Model
 *
 * @package Joomla
 * @subpackage OpenShop
 * @since 1.5
 */
class OpenShopModelCategory extends OpenShopModel {

    public function __construct($config) {
        $config['translatable'] = true;
        $config['translatable_fields'] = array('category_name', 'category_alias', 'category_desc', 'category_page_title', 'category_page_heading', 'meta_key', 'meta_desc');

        parent::__construct($config);
    }

    function store(&$data) {
        $imagePath = JPATH_ROOT . '/images/com_openshop/categories/';
        if (JRequest::getInt('remove_image') && $data['id']) {
            //Remove image first
            $row = new OpenShopTable('#__openshop_categories', 'id', $this->getDbo());
            $row->load($data['id']);
            if (JFile::exists($imagePath . $row->category_image))
                JFile::delete($imagePath . $row->category_image);

            if (JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->category_image) . '-100x100.' . JFile::getExt($row->category_image)))
                JFile::delete($imagePath . 'resized/' . JFile::stripExt($row->category_image) . '-100x100.' . JFile::getExt($row->category_image));
            $data['category_image'] = '';
        }
        
        if (JRequest::getInt('remove_banner_image') && $data['id']) {
            //Remove image first
            $row = new OpenShopTable('#__openshop_categories', 'id', $this->getDbo());
            $row->load($data['id']);
            if (JFile::exists($imagePath . $row->banner_image))
                JFile::delete($imagePath . $row->banner_image);

            $data['banner_image'] = '';
        }

        $categoryImage = $_FILES['category_image'];
        if ($categoryImage['name']) {
            $checkFileUpload = OpenShopFile::checkFileUpload($categoryImage);
            if (is_array($checkFileUpload)) {
                $mainframe = JFactory::getApplication();
                $mainframe->enqueueMessage(sprintf(JText::_('OPENSHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
                $mainframe->redirect('index.php?option=com_openshop&task=category.edit&cid[]=' . $data['id']);
            } else {
                if (is_uploaded_file($categoryImage['tmp_name']) && file_exists($categoryImage['tmp_name'])) {
                    if (JFile::exists($imagePath . $categoryImage['name'])) {
                        $imageFileName = uniqid('image_') . '_' . JFile::makeSafe($categoryImage['name']);
                    } else {
                        $imageFileName = JFile::makeSafe($categoryImage['name']);
                    }
                    JFile::upload($categoryImage['tmp_name'], $imagePath . $imageFileName);
                    // Resize image
                    OpenShopHelper::resizeImage($imageFileName, JPATH_ROOT . '/images/com_openshop/categories/', 100, 100);
                    $data['category_image'] = $imageFileName;
                }
            }
        }
        
        $categoryImage = $_FILES['banner_image'];
        if ($categoryImage['name']) {
            $checkFileUpload = OpenShopFile::checkFileUpload($categoryImage);
            if (is_array($checkFileUpload)) {
                $mainframe = JFactory::getApplication();
                $mainframe->enqueueMessage(sprintf(JText::_('OPENSHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
                $mainframe->redirect('index.php?option=com_openshop&task=category.edit&cid[]=' . $data['id']);
            } else {
                if (is_uploaded_file($categoryImage['tmp_name']) && file_exists($categoryImage['tmp_name'])) {
                    if (JFile::exists($imagePath . $categoryImage['name'])) {
                        $imageFileName = uniqid('image_') . '_' . JFile::makeSafe($categoryImage['name']);
                    } else {
                        $imageFileName = JFile::makeSafe($categoryImage['name']);
                    }
                    JFile::upload($categoryImage['tmp_name'], $imagePath . $imageFileName);
                    // Resize image
//                    OpenShopHelper::resizeImage($imageFileName, JPATH_ROOT . '/images/com_openshop/categories/', 100, 100);
                    $data['banner_image'] = $imageFileName;
                }
            }
        }
        
        if (count($data['category_customergroups'])) {
            $data['category_customergroups'] = implode(',', $data['category_customergroups']);
        } else {
            $data['category_customergroups'] = '';
        }
        parent::store($data);
        return true;
    }

    /**
     * Method to remove categories
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = $this->getDbo();
            $cids = implode(',', $cid);
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_categories')
                    ->where('id IN (' . $cids . ')')
                    ->where('id NOT IN (SELECT  DISTINCT(category_id) FROM #__openshop_productcategories)')
                    ->where('id NOT IN (SELECT DISTINCT(category_parent_id) FROM #__openshop_categories WHERE category_parent_id > 0)');
            $db->setQuery($query);
            $categories = $db->loadColumn();
            if (count($categories)) {
                $query->clear();
                $query->delete('#__openshop_categories')
                        ->where('id IN (' . implode(',', $categories) . ')');
                $db->setQuery($query);
                if (!$db->query())
                //Removed error
                    return 0;
                $numItemsDeleted = $db->getAffectedRows();
                //Delete details records
                $query->clear();
                $query->delete('#__openshop_categorydetails')
                        ->where('category_id IN (' . implode(',', $categories) . ')');
                $db->setQuery($query);
                if (!$db->query())
                //Removed error
                    return 0;
                //Remove SEF urls for categories
                for ($i = 0; $n = count($categories), $i < $n; $i++) {
                    $query->clear();
                    $query->delete('#__openshop_urls')
                            ->where('query LIKE "view=category&id=' . $categories[$i] . '"');
                    $db->setQuery($query);
                    $db->query();
                }
                if ($numItemsDeleted < count($cid)) {
                    //Removed warning
                    return 2;
                }
            } else {
                return 2;
            }
        }
        //Removed success
        return 1;
    }

}
