<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelOrder extends OpenShopModel {

    protected $_customer_id = null;

    function __construct($config) {
        $customer_id = JFactory::getApplication()->input->getInt('customer_id');
        $this->_customer_id = $customer_id;
        parent::__construct($config);
    }

    function &getData() {
        if (empty($this->_data)) {
            $temp = 1;
            if (isset($this->_id)) {
                $this->_loadData();
                $temp = 0;
            }

            if (isset($this->_customer_id)) {
                $this->_loadDataNewOrder();
                $temp = 0;
            }

            if ($temp == 1) {
                $this->_initData();
            }
        }


        return $this->_data;
    }

    function _loadDataNewOrder() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.fullname as payment_fullname, a.telephone as payment_telephone, a.email as payment_email, b.address as payment_address,zone_id as payment_zone_id ,district_id as payment_district_id')
                ->from($db->quoteName('#__openshop_customers', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_addresses', 'b') . 'ON a.customer_id = b.customer_id')
                ->join('LEFT', $db->quoteName('#__openshop_zones', 'c') . 'ON b.zone_id = c.id')
                ->where('a.customer_id = ' . $this->_customer_id);
        $this->_data = $db->setQuery($query)->loadObject();
    }

    function store(&$data) {
//        print_r($data);exit();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $cid = $data['cid'][0];

        if ($cid == '') {
            //check exists customer
            //if not exists ->into customers table
            if ($data['change_info'] == 0) {   //no change information customer
                if ($this->check_customer($data)) {
                    $data['customer_id'] = $this->get_customer();
                }
            } else if ($data['change_info'] == 1) {  //change information customer
                $this->change_info_customer($data);
            }

            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $data[$key] = json_encode($value);
                }
            }

            /*
             * ORDER_NUMBER
             */
//            $query->clear();
//            $query->select('max(invoice_number)')->from($db->quoteName('#__openshop_orders'));
//            $db->setQuery($query);
//
//            $data['invoice_number'] = (int) $db->loadResult() + 1;
//            $data['order_number'] = 'DH' . ($db->loadResult() + 1);
//            $data['order_status_id'] = '8';  //pending
        }

//        parent::store($data);
        // --------------------
//        if ($cid == '') {
//            $query->clear();
//            $query->select('max(id)')
//                    ->from($db->quoteName('#__openshop_orders'));
//            $db->setQuery($query);
//            $order_id = $db->loadResult();
//        } else {
//            $order_id = $cid;
//
//            //delete record old
//            $query->clear();
//            $query->delete($db->quoteName('#__openshop_orderproducts'))
//                    ->where('order_id = ' . $order_id);
//            $db->setQuery($query)->execute();
//        }
        //add orderproducts table
        $data_op = new stdClass();
//        $data_op->order_id = $order_id;

        for ($i = 1; $i <= $data['max_value']; $i++) {
            if ($data['product_sku_' . $i] != '' && $data['product_price_' . $i] != '') {

                if ($cid == '') {
                    $query->clear();
                    $query->select('max(invoice_number)')->from($db->quoteName('#__openshop_orders'));
                    $db->setQuery($query);

                    $data['invoice_number'] = (int) $db->loadResult() + 1;
                    $data['order_number'] = 'DH' . ($db->loadResult() + 1);
                    $data['order_status_id'] = '8';  //pending
                    $data['id'] = 0;    //them nhieu
                }
                
                //insert into orders table
                parent::store($data);

                if ($cid == '') {
                    $query->clear();
                    $query->select('max(id)')
                            ->from($db->quoteName('#__openshop_orders'));
                    $db->setQuery($query);
                    $order_id = $db->loadResult();
                } else {
                    $order_id = $cid;

                    //delete record old
                    $query->clear();
                    $query->delete($db->quoteName('#__openshop_orderproducts'))
                            ->where('order_id = ' . $order_id);
                    $db->setQuery($query)->execute();
                }
                $data_op->order_id = $order_id;
                $data_op->product_id = $data['product_id_' . $i];
                $data_op->product_name = $data['product_name_' . $i];
                $data_op->product_sku = $data['product_sku_' . $i];
                $data_op->quantity = $data['product_quantity_' . $i];
                $data_op->price = $data['product_price_' . $i];
                $data_op->price_input = $data['product_price_input_' . $i];
                $data_op->total_price = $data['product_total_' . $i];
                $data_op->product_size = $data['product_size_' . $i];
                $data_op->product_color = $data['product_color_' . $i];
                $data_op->optionheight_id = $data['optionheight_id_' . $i];
                $data_op->optionweight_id = $data['optionweight_id_' . $i];
                $data_op->order_product_desc = $data['order_product_desc_' . $i];

                //insert into OrderProducts table
                JFactory::getDbo()->insertObject('#__openshop_orderproducts', $data_op);

                // --------------------
                $query->clear();
                $query->select('c.id')
                        ->from($db->quoteName('#__openshop_geozones', 'a'))
                        ->join('LEFT', $db->quoteName('#__openshop_geozonezones', 'b') . 'ON a.id = b.geozone_id')
                        ->join('LEFT', $db->quoteName('#__openshop_transporters', 'c') . 'ON a.transporter_id = c.id')
                        ->where('zone_id = ' . $data['payment_zone_id'])
                        ->where('district_id = ' . $data['payment_district_id']);
                $db->setQuery($query);

                $transporter_id = "";
                if ($db->loadResult()) {
                    $transporter_id = $db->loadResult();
                }

                if ($cid == '') {
                    $query->clear();
                    $query->select('max(id)')
                            ->from($db->quoteName('#__openshop_orders'));
                    $db->setQuery($query);
                    $order_id = $db->loadResult();

                    $data_cart = new stdClass();
                    $data_cart->order_id = $order_id;
                    $data_cart->transporter_id = $transporter_id;
                    JFactory::getDbo()->insertObject('#__openshop_carts', $data_cart);
                } else {
                    $order_id = $cid;

                    //delete record old
                    $query->clear();

                    $data_cart = new stdClass();
                    $data_cart->order_id = $order_id;
                    $data_cart->transporter_id = $transporter_id;

                    JFactory::getDbo()->updateObject('#__openshop_carts', $data_cart, 'order_id');
                }
            }
        }

//        ----------------------------SEND MAIL------------------------------
//        
//		$row->load($data['id']);
//		$language = JLanguage::getInstance($row->language, 0);
//		$language->load('com_openshop', JPATH_ADMINISTRATOR, $row->language);
//		if ($orderStatusChanged)
//		{
//			if ($data['order_status_id'] == OpenShopHelper::getConfigValue('complete_status_id'))
//			{
//				JPluginHelper::importPlugin('openshop');
//				$dispatcher = JDispatcher::getInstance();
//				$dispatcher->trigger('onAfterCompleteOrder', array($row));
//			}
//			if (JRequest::getVar('send_notification_email'))
//			{
//				$jconfig = new JConfig();
//				$mailer = JFactory::getMailer();
//				$fromName = $jconfig->fromname;
//				$fromEmail =  $jconfig->mailfrom;
//				$subject = sprintf($language->_('OPENSHOP_NOTIFICATION_EMAIL_SUBJECT'), OpenShopHelper::getConfigValue('store_name'));
//				$body = OpenShopHelper::getNotificationEmailBody($row, $orderStatusFrom, $orderStatusTo);
//				$mailer->ClearAllRecipients();
//				$mailer->sendMail($fromEmail, $fromName, $row->email, $subject, $body, 1);
//			}
//		}
//		if (JRequest::getVar('send_shipping_notification_email') && JRequest::getVar('shipping_tracking_number') != '' && JRequest::getVar('shipping_tracking_url') != '')
//		{
//			$jconfig = new JConfig();
//			$mailer = JFactory::getMailer();
//			$fromName = $jconfig->fromname;
//			$fromEmail =  $jconfig->mailfrom;
//			$subject = $language->_('OPENSHOP_SHIPPING_NOTIFICATION_EMAIL_SUBJECT');
//			$body = OpenShopHelper::getShippingNotificationEmailBody($row);
//			$mailer->ClearAllRecipients();
//			$mailer->sendMail($fromEmail, $fromName, $row->email, $subject, $body, 1);
//		}
        return true;
    }

    /**
     * Method to remove orders
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = $this->getDbo();
            $cids = implode(',', $cid);
            $query = $db->getQuery(true);
            $query->delete('#__openshop_orders')
                    ->where('id IN (' . $cids . ')');
            $db->setQuery($query);
            if (!$db->query())
            //Removed error
                return 0;
            $numItemsDeleted = $db->getAffectedRows();
            //Delete order products
            $query->clear();
            $query->delete('#__openshop_orderproducts')
                    ->where('order_id IN (' . $cids . ')');
            $db->setQuery($query);
            if (!$db->query())
            //Removed error
                return 0;
            //Delete order totals
            $query->clear();
            $query->delete('#__openshop_ordertotals')
                    ->where('order_id IN (' . $cids . ')');
            $db->setQuery($query);
            if (!$db->query())
            //Removed error
                return 0;
            //Delete order options
            $query->clear();
            $query->delete('#__openshop_orderoptions')
                    ->where('order_id IN (' . $cids . ')');
            $db->setQuery($query);
            if (!$db->query())
            //Removed error
                return 0;
            //Delete order downloads
            $query->clear();
            $query->delete('#__openshop_orderdownloads')
                    ->where('order_id IN (' . $cids . ')');
            $db->setQuery($query);
            if (!$db->query())
            //Removed error
                return 0;

            if ($numItemsDeleted < count($cid)) {
                //Removed warning
                return 2;
            }
        }
        //Removed success
        return 1;
    }

    /**
     * 
     * Function to download file
     * @param int $id
     */
    function downloadFile($id, $download = true) {
        $app = JFactory::getApplication();
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('option_value')
                ->from('#__openshop_orderoptions')
                ->where('id = ' . intval($id));
        $db->setQuery($query);
        $filename = $db->loadResult();
        while (@ob_end_clean());
        OpenShopHelper::processDownload(JPATH_ROOT . '/media/com_openshop/files/' . $filename, $filename, $download);
        $app->close(0);
    }

    public function check_customer($data) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_customers'))
                ->where('telephone = "' . $data['payment_telephone'] . '"');
        $db->setQuery($query);
        $check = $db->loadResult();

        $query->clear();
        $query->select('max(customer_id)')
                ->from($db->quoteName('#__openshop_customers'));
        $db->setQuery($query);
        $customer_id = $db->loadResult();

        //customer not exists ==> add customer
        if (!$check) {

            //insert user
            $data['password'] = $data['payment_telephone'];
            $user = new JUser();
            $user->bind($data); //convert pass

            $data_user = new stdClass();
            $data_user->name = $data['payment_fullname'];
            $data_user->username = $data['payment_telephone'];
            $data_user->email = $data['payment_email'];
            $data_user->password = $data['password'];
            $data_user->block = '0';
            $data_user->sendEmail = '0';
            $data_user->registerDate = date('d-m-Y H:i:s');

            if ($db->insertObject('#__users', $data_user)) {
                $query->clear();
                $query->select('max(id)')
                        ->from($db->quoteName('#__users'));
                $user_id = $db->setQuery($query)->loadResult();
            }

            //add user_usergroup_map table
            $data_usergroup_map = new stdClass();
            $data_usergroup_map->group_id = '2';
            $data_usergroup_map->user_id = $user_id;

            $db->insertObject('#__user_usergroup_map', $data_usergroup_map);

            $d_cus = new stdClass();
            $d_cus->customer_id = $user_id;
            $d_cus->customergroup_id = '1';
            $d_cus->fullname = $data['payment_fullname'];
            $d_cus->email = $data['payment_email'];
            $d_cus->telephone = $data['payment_telephone'];
            $d_cus->published = '1';
            $d_cus->created_date = date('d-m-Y H:i:s');
            $d_cus->modified_date = date('d-m-Y H:i:s');


            $db->insertObject('#__openshop_customers', $d_cus);

            $d_address = new stdClass();
            $d_address->customer_id = $user_id;
            $d_address->address = $data['payment_address'];
            $d_address->zone_id = $data['payment_zone_id'];
            $d_address->district_id = $data['payment_district_id'];
            $d_address->created_date = date('d-m-Y H:i:s');
            $d_address->modified_date = date('d-m-Y H:i:s');

            $db->insertObject('#__openshop_addresses', $d_address);

            $query->clear();
            $query->select('max(id)')->from($db->quoteName('#__openshop_addresses'));
            $db->setQuery($query);
            $fields = array('address_id' . ' = ' . $db->loadResult());
            $query->update($db->quoteName('#__openshop_customers'))
                    ->set($fields)->where('customer_id = ' . $user_id);
            $db->setQuery($query)->execute();



            return TRUE;
        }

        return FALSE;
    }

    /*
     *  ----------- change information customer 
     */

    public function change_info_customer($data) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $fields = array(
            'cus.fullname' . ' = "' . $data['payment_fullname'] . '"',
            'cus.telephone' . ' = "' . $data['payment_telephone'] . '"',
            'cus.email' . ' = "' . $data['payment_email'] . '"',
            'add.address' . ' = "' . $data['payment_address'] . '"',
            'add.zone_id' . ' = "' . $data['payment_zone_id'] . '"',
            'add.district_id' . ' = "' . $data['payment_district_id'] . '"',
            'cus.modified_date' . ' = "' . date('Y-m-d H:m:s') . '"'
        );

        $query->update($db->quoteName('#__openshop_customers', 'cus'))
                ->set($fields)
                ->join('LEFT', $db->quoteName('#__openshop_addresses', 'add') . ' ON cus.customer_id = add.customer_id')
                ->where('cus.customer_id = ' . $data['customer_id']);
        $db->setQuery($query)->execute();
    }

    public function store_transporter($data) {
        $d = new stdClass();
        $d->transporter_id = $data['transporter_id'];
        $d->product_id = $data['id_product_transporter'];
        $d->size_id = $data['product_size'];
        $d->color_id = $data['product_color'];
        JFactory::getDbo()->insertObject('#__openshop_carts', $d);
    }

    public function get_customer() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('max(customer_id)')
                ->from($db->quoteName('#__openshop_customers'));

        return $db->setQuery($query)->loadResult();
    }

}
