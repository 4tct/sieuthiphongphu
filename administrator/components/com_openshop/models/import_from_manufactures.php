<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelimport_from_manufactures extends OpenShopModelList {

    function __construct($config) {
        parent::__construct($config);
    }

    function getItems() {

        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $id_warehouse = OpenShopHelper::getIdWarehouseUserF();
        
        $row_appointments = $db->setQuery('CALL getQuantityProductImport('. $id_warehouse .')')->loadObjectList();

        return $row_appointments;
    }

}
