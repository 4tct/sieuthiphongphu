<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelWarehouse extends OpenShopModel {

    public function __construct($config) {
//		$config['translatable'] = true;
//		$config['translatable_fields'] = array('warehouse_name', 'warehouse_alias', 'warehouse_desc', 'warehouse_phone', 'warehouse_email', 'warehouse_address');

        parent::__construct($config);
    }

    function store(&$data) {
        $userList = '';
        foreach ($data['user_group_m'] as $value) {
            if ($userList == '') {
                $userList .= $value;
            } else {
                $userList .= ',' . $value;
            }
        }

        $data['user_group_manage'] = $userList;

        parent::store($data);
    }

    /**
     * Method to remove warehouses
     *
     * @access	public
     * @return boolean True on success
     * @since	1.5
     */
    public function delete($cid = array()) {
        if (count($cid)) {
            $db = $this->getDbo();
            $cids = implode(',', $cid);
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_warehouses')
                    ->where('id IN (' . $cids . ')');
            $db->setQuery($query);
            $warehouses = $db->loadColumn();
            if (count($warehouses)) {
                $query->clear();
                $query->delete('#__openshop_warehouses')
                        ->where('id IN (' . implode(',', $warehouses) . ')');
                $db->setQuery($query);
                if (!$db->query())
                //Removed error
                    return 0;
                $numItemsDeleted = $db->getAffectedRows();
            }
            else {
                return 2;
            }
        }
        //Removed success
        return 1;
    }

}
