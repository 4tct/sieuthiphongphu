<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelScans extends OpenShopModelList {

    function __construct($config) {
        parent::__construct($config);
    }

    function getItems() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select(''
                        . '(SELECT value FROM #__openshop_optionvaluedetails as b WHERE b.optionvalue_id = a.size_id) as size,'
                        . '(SELECT value FROM #__openshop_optionvaluedetails as b WHERE b.optionvalue_id = a.color_id) as color')
                ->from($db->quoteName('#__openshop_kiemkhos', 'a'));
        if (JFactory::getApplication()->input->getString('layout') == 'add_mode') {
            if (OpenShopPermissions::checkAdmin(JFactory::getUser()->groups)) {
                $query->select('count(quantity_import) as quantity_import,id,product_name,product_sku,size_id,color_id,import_by')
                        ->group('product_sku,size_id,color_id');
            } else {
                $query->select('a.*');
            }
        } else {
            $query->select('a.*');
        }
        
        if(JFactory::getApplication()->input->getInt('user_filter')){
            $query->where('a.import_by = ' . JFactory::getApplication()->input->getInt('user_filter'));
        }
        return $db->setQuery($query)->loadObjectList();
    }

}
