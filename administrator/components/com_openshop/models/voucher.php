<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Voucher Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelVoucher extends OpenShopModel
{
	
	function store(&$data)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		// Check duplicated voucher
		$query->select('COUNT(*)')
			->from('#__openshop_vouchers')
			->where('voucher_code = ' . $db->quote($data['voucher_code']));
		if ($data['id'])
		{
			$query->where('id != ' . intval($data['id']));
		}
		$db->setQuery($query);
		if ($db->loadResult())
		{
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage(JText::_('OPENSHOP_VOUCHER_EXISTED'), 'error');
			$mainframe->redirect('index.php?option=com_openshop&task=voucher.edit&cid[]=' . $data['id']);
		}
		parent::store($data);
		return true;
	}
	
	/**
	 * Method to remove vouchers
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		//Remove voucher history
		if (count($cid))
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->delete('#__openshop_voucherhistory')
				->where('voucher_id IN (' . implode(',', $cid) . ')');
			$db->setQuery($query);
			$db->query();
		}
		parent::delete($cid);
	}
}