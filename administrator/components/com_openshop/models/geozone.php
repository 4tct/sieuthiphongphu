<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelGeozone extends OpenShopModel
{

	function store(&$data)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		if ($data['id'])
		{
			$query->delete('#__openshop_geozonezones')
				->where('geozone_id = ' . (int) $data['id']);
			$db->setQuery($query);
			$db->query();
		}
		parent::store($data);
		//save new data
		if (isset($data['country_id']))
		{
			$geozoneId = $data['id'];
			$countryIds = $data['country_id'];
			$zoneIds = $data['zone'];
                        $districtIds = $data['district'];
			$query->clear();
			$query->insert('#__openshop_geozonezones')->columns('geozone_id, zone_id, country_id, district_id');
			foreach ($countryIds as $key => $countryId)
			{
				$zoneId = $db->quote($zoneIds[$key]);
                                $districtId = $db->quote($districtIds[$key]);
				$query->values("$geozoneId, $zoneId, $countryId, $districtId");
			}
			$db->setQuery($query);
			$db->query();
		}
		
		return true;
	}
	
	/**
	 * Method to remove geozones
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		if (count($cid))
		{
			$db = $this->getDbo();
			$cids = implode(',', $cid);
			$query = $db->getQuery(true);
			$query->delete('#__openshop_geozones')
				->where('id IN (' . $cids . ')')
				->where('id NOT IN (SELECT  DISTINCT(geozone_id) FROM #__openshop_geozonezones)');
			$db->setQuery($query);
			if (!$db->query())
				//Removed error
				return 0;
			$numItemsDeleted = $db->getAffectedRows();
			if ($numItemsDeleted < count($cid))
			{
				//Removed warning
				return 2;
			}
		}
		//Removed success
		return 1;
	}
	
	/**
	 * Function to copy geozone and zones for it
	 * @see OpenShopModel::copy()
	 */
	function copy($id)
	{
		$copiedGeozoneId = parent::copy($id);
		$db = $this->getDbo();
		$sql = 'INSERT INTO #__openshop_geozonezones'
			. ' (geozone_id, zone_id, country_id)'
			. ' SELECT ' . $copiedGeozoneId . ', zone_id, country_id'
			. ' FROM #__openshop_geozonezones'
			. ' WHERE geozone_id = ' . intval($id);
		$db->setQuery($sql);
		$db->query();
		return $copiedGeozoneId;
	}

}