<?php

defined('_JEXEC') or die('Restricted access');

class OpenShopModelIos extends OpenShopModelList {

    function __construct($config) {
        $config['search_fields'] = array('b.product_sku');
        $config['state_vars'] = array(
            'warehouse_id' => array('', 'cmd', 1),
            'manufacturer_id' => array('', 'cmd', 1),
            'invoice_id' => array('', 'cmd', 1),
            'date_from' => array('', 'cmd', 1),
            'date_to' => array('', 'cmd', 1),
        );
        parent::__construct($config);
    }

    public function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);
        $query->select('c.product_name, d.manufacturer_name,e.warehouse_name,a.*,b.product_sku')
                ->from($db->quoteName('#__openshop_ios', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_products', 'b') . 'ON a.id_product = b.id')
                ->join('LEFT', $db->quoteName('#__openshop_productdetails', 'c') . 'ON c.product_id = b.id')
                ->join('LEFT', $db->quoteName('#__openshop_manufacturers', 'd') . 'ON d.id = b.manufacturer_id')
                ->join('LEFT', $db->quoteName('#__openshop_warehouses', 'e') . 'ON e.id = a.id_warehouse')
                ->order('xacnhan ASC, created_date DESC');
        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

    function _buildContentWhereArray() {
        $db = $this->getDbo();
        $state = $this->getState();
        $where = array();
        if ($state->filter_state == 'P')
            $where[] = ' a.published=1 ';
        elseif ($state->filter_state == 'U')
            $where[] = ' a.published = 0';
        if ($state->search) {
            $search = $db->quote('%' . $db->escape(strtolower($state->search), true) . '%', false);
            if (is_array($this->searchFields)) {
                $whereOr = array();
                foreach ($this->searchFields as $titleField) {
                    $whereOr[] = " LOWER($titleField) LIKE " . $search;
                }
                $where[] = ' (' . implode(' OR ', $whereOr) . ') ';
            } else {
                $where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape(strtolower($state->search), true) . '%', false);
            }
        }

        if ($this->translatable) {
            $where[] = 'b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"';
        }

        if ($state->warehouse_id) {
            $where[] = 'a.id_warehouse = ' . (int) $state->warehouse_id;
        }

        if ($state->manufacturer_id) {
            $where[] = 'a.manufacturer_sku = ' . (int) $state->manufacturer_id;
        }

        if ($state->invoice_id) {
            $where[] = 'a.invoice_sku = ' . (int) $state->invoice_id;
        }

        if ($state->date_from) {
            $where[] = 'a.created_date >= "' . date('Y-m-d 00:00:00', strtotime($state->date_from)) . '"';
        }

        if ($state->date_to) {
            $where[] = 'a.created_date <= "' . date('Y-m-d 23:59:59', strtotime($state->date_to)) . '"';
        }

        return $where;
    }

    function store(&$data) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $thongbao = array("status" => "", "messages" => "");
//Success, Info,Warning, Error
        $message = array();
        $db = JFactory::getDbo();
        $id_arr = array();
        $dem = 0;
        $soluong = 0;
        $timthay = 0;
        $array_sanpham_tontai = array();
        $array_sanpham_khongtontai = array();
        $array_optionval = array();
        $data_op = new stdClass();

        if ($data['statusios'] == 1) { // nhap kho
            for ($i = 1; $i <= $data['numrow']; $i++) {
                if ($data['productnew'] == 1 && $data['statusios'] == 1) { // nhap san pham da ton tai
                    if (empty($array_sanpham_tontai)) {// kiem tra mang rong
                        $array_sanpham_tontai[$dem] = $data['slcolor_' . $i] . "_" . $data['slsize_' . $i] . "_" . $data['sl_' . $i];
                    } else {// mang khong rong->kiem tra
                        for ($j = 0; $j < count($array_sanpham_tontai); $j++) {// lap tung mang kiem tra so sánh
                            $mang = $array_sanpham_tontai[$j];
                            $str = explode("_", $mang);
                            $soluong = $str[2];
                            if ($str[0] == $data['slcolor_' . $i] && $str[1] == $data['slsize_' . $i]) {// phan tu giong nhau
                                $soluong = $soluong + $data['sl_' . $i];
                                $timthay++;
                                $array_sanpham_tontai[$j] = $data['slcolor_' . $i] . "_" . $data['slsize_' . $i] . "_" . $soluong;
                                break;
                            }
                        }
                        if ($timthay == 0) {
                            $dem = $dem + 1;
                            $array_sanpham_tontai[$dem] = $data['slcolor_' . $i] . "_" . $data['slsize_' . $i] . "_" . $data['sl_' . $i];
                        }
                        $timthay = 0;
                    }
                } else if ($data['productnew'] == 0 && $data['statusios'] == 1) {// nhap san pham moi hoan toan vao kho
                    if (empty($array_sanpham_khongtontai)) {// kiem tra mang rong
                        $array_sanpham_khongtontai[$dem] = $data['slcolor_' . $i] . "_" . $data['slsize_' . $i] . "_" . $data['sl_' . $i];
                    } else {// mang khong rong->kiem tra
                        for ($j = 0; $j < count($array_sanpham_khongtontai); $j++) {// lap tung mang kiem tra so sánh
                            $mang = $array_sanpham_khongtontai[$j];
                            $str = explode("_", $mang);
                            $soluong = $str[2];
                            if ($str[0] == $data['slcolor_' . $i] && $str[1] == $data['slsize_' . $i]) {// phan tu giong nhau
                                $soluong = $soluong + $data['sl_' . $i];
                                $timthay++;
                                $array_sanpham_khongtontai[$j] = $data['slcolor_' . $i] . "_" . $data['slsize_' . $i] . "_" . $soluong;
                                break;
                            }
                        }
                        if ($timthay == 0) {
                            $dem = $dem + 1;
                            $array_sanpham_khongtontai[$dem] = $data['slcolor_' . $i] . "_" . $data['slsize_' . $i] . "_" . $data['sl_' . $i];
                        }
                        $timthay = 0;
                    }
                }
            } // end for()
        }
        $array_output = array();
        if ($data['statusios'] != 1) {
            switch ($data['statusios']) {
                case 2:
                    $data_export = new stdClass();
                    $data_export->id_product = $data['idpro_auto'];
                    $data_export->date_input = date('Y-m-d H:m:s');
                    $data_export->created_date = date('Y-m-d H:m:s');
                    $data_export->id_status = $data['statusios'];
                    $data_export->price_input = $data['price'];
                    $data_export->created_by = JFactory::getUser()->id;
                    $data_export->id_warehouse = $data['id_warehouse_from'];
                    $data_export->id_warehouse_to = $data['id_warehouse_to'];
                    $data_export->invoice_sku = 'XUATKHO_' . date('Y-m-d') .'_'. $data['lenofday'];
                    $data_export->xacnhan = 0;
                    $data_export->id_option = 1;
                    for ($i = 1; $i <= $data['sodongmax']; $i++) {
                        $data_export->id_optionsize = $data['slsize_' . $i];
                        $data_export->id_optioncolor = $data['slcolor_' . $i];
                        $data_export->quantity = $data['sl_' . $i];

                        try {
                            if ($db->insertObject('#__openshop_ios', $data_export)) {
                                $total_warehouse = $db->setQuery('CALL TotalInventory("import",' . $data['idpro_auto'] . ',' . $data['slsize_' . $i] . ',' . $data['slcolor_' . $i] . ',' . $data['id_warehouse_from'] . ')')->loadResult();
                                $total_warehouse_updated = (int) $total_warehouse - (int) $data['sl_' . $i];
                                if ($db->setQuery('CALL UpdateInInventory("import",' . $data['idpro_auto'] . ',' . $data['slsize_' . $i] . ',' . $data['slcolor_' . $i] . ',' . $data['id_warehouse_from'] . ',' . $total_warehouse_updated . ')')->execute()) {
                                    $thongbao["status"] = JText::_('OPENSHOP_SUCCESS');
                                    $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_SAVE_SUCCESS');
                                }
                            } else {
                                $thongbao["status"] = JText::_('OPENSHOP_WARNING');
                                $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_NOT_EXITS_QUANTITY');
                            }
                        } catch (Exception $ex) {
                            $thongbao["status"] = JText::_('OPENSHOP_WARNING');
                            $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_NOT_EXITS_QUANTITY');
                        }
                    }

                    break;

                case 3:// tra nha cung cap
                    {
// Xuly: 1.lay so luong sp nay ở bảng tồn kho
// 2. insert vao ios (id_warehouse='makho', id_warehouse_to=null,xacnhan=1,idsize,color)//lưu ý
//3. update lai bảng tồn kho
                        for ($m = 0; $m < $data['sodongmax']; $m++) {
                            $soluongmax = 0;
                            $str_soluongout = $data['sl_' . $m];
                            $str_chitiet = explode("_", $data['outputs_' . $m]);
//namecolor[0]_namesize[1]_soluong[2]_idcolor[3]_idsize[4]_ngay[5]_idauto(6)
//vd: Đỏ[0]_S[1]_10[2]_90[3]_22[4]_1[5]_6[6]__rách[7]//Đỏ_S_100_1_6_22
                            $description = $data['descript_' . $m];
                            $soluongmax = $str_chitiet[2];
                            if ($str_soluongout > 0) {
                                if ($str_soluongout >= 0 && $str_soluongout <= $soluongmax) {

                                    $array_output[$dem++] = $str_chitiet[0] . '_' . $str_chitiet[1] . '_' . $str_soluongout . '_' .
                                            $str_chitiet[3] . '_' . $str_chitiet[4] . '_' . $str_chitiet[5] . '_' . $description;
//namecolor(0)_namesize(1)_sloutput(2)_idcolor(3)_idsize(4)_idproauto(5)_description(8)
//vd:Đỏ[0]_S[1]_1[2]_1[3]_6[4]_22[5]_aaaaaaaa[6]
                                }
                            }
                        }// end for

                        if (!empty($array_output)) {
                            $soluong_tonnhapkho = 0;
                            for ($g = 0; $g < count($array_output); $g++) {

                                $str = explode("_", $array_output[$g]);
                                $query = $db->getQuery(true); // lay soluong sp nay ở bang ton kho
                                $query->select($db->quoteName(array('total_import_product')));
                                $query->from($db->quoteName('#__openshop_inventories'));
                                $query->where('product_id' . ' = ' . $str[5]);
                                $query->where('idsize' . ' = ' . $str[4]);
                                $query->where('idcolor' . ' = ' . $str[3]);
                                $query->where('warehouse_id' . ' = ' . $data['id_warehouse']);
                                $db->setQuery($query);
                                $resul_soluong_tonnhapkho = $db->loadObjectList();
                                foreach ($resul_soluong_tonnhapkho as $key => $val) {
                                    $soluong_tonnhapkho = $resul_soluong_tonnhapkho[$key]->total_import_product;
// đã lấy số lượng
                                }

                                $data_outputpro = new stdClass(); // lưu du lieu tranhacungcap vao ios
                                $data_outputpro->order_date = date('Y-m-d H:i:s');
                                $data_outputpro->id_product = $str[5];
                                $data_outputpro->manufacturer_sku = $data['namemanufac'];
                                $data_outputpro->id_optioncolor = $str[3];
                                $data_outputpro->id_optionsize = $str[4];
                                $data_outputpro->quantity = $str[2];
                                $data_outputpro->id_status = $data['statusios'];
                                $data_outputpro->date_input = date('Y-m-d H:i:s');
                                $data_outputpro->description = $str[6];
                                $data_outputpro->created_date = date('Y-m-d H:i:s');
                                $data_outputpro->modified_date = date('Y-m-d H:i:s');
                                $data_outputpro->created_by = JFactory::getUser()->id;
                                $data_outputpro->id_warehouse = $data['id_warehouse'];
                                $data_outputpro->id_warehouse_to = $data[''];
                                switch ($data['statusios']) {
                                    case 1:
                                        $data_outputpro->invoice_sku = 'NHAPKHO_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                    case 2:
                                        $data_outputpro->invoice_sku = 'CHUYENKHO_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                    case 3:
                                        $data_outputpro->invoice_sku = 'TRANHACUNGCAP_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                    case 4:
                                        $data_outputpro->invoice_sku = 'THUHOI_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                }
                                $data_outputpro->xacnhan = 1;
                                JFactory::getDbo()->insertObject('#__openshop_ios', $data_outputpro);
// lưu vào ios
//update vào ton kho
                                $query = $db->getQuery(true);
                                $fields = array(
                                    $db->quoteName('total_import_product') . ' = ' . ($soluong_tonnhapkho - $str[2])
                                );
                                $conditions = array(
                                    $db->quoteName('product_id') . ' = ' . $str[5],
                                    $db->quoteName('idsize') . ' = ' . $str[4],
                                    $db->quoteName('idcolor') . ' = ' . $str[3],
                                    $db->quoteName('warehouse_id') . ' = ' . $data['id_warehouse']
                                );
                                $query->update($db->quoteName('#__openshop_inventories'))->set($fields)->where($conditions);
                                $db->setQuery($query);
                                $db->execute();
                                $thongbao["status"] = JText::_('OPENSHOP_SUCCESS');
                                $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_SAVE_SUCCESS');
                            }// end for
                        } else {// end if
                            $thongbao["status"] = JText::_('OPENSHOP_WARNING');
                            $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_NOT_EXITS_QUANTITY');
                        }
                        break;
                    }
                case 4:// Thu hoi, hàng bị lỗi
                    {
//Xử lý: 1. lấy dữ liệu từ form
//2. lấy so luong nhập kho và xuất kho,bán: của sản phẩm đó trong bảng tồn kho
//3.insert vao ios
//4. update tổng tồn kho và tổng xuất kho và tổng bán trong bảng tồn kho

                        for ($m = 0; $m < $data['sodongmax']; $m++) {
                            $soluongmax = 0;
                            $str_soluongout = $data['sl_' . $m];
                            $str_chitiet = explode("_", $data['outputs_' . $m]);
//namecolor[0]_namesize[1]_soluong[2]_idcolor[3]_idsize[4]_ngay[5]_idauto(6)
//vd: Đỏ[0]_S[1]_10[2]_90[3]_22[4]_1[5]_6[6]__rách[7]//Đỏ_S_100_1_6_22
                            $description = $data['descript_' . $m];
                            $soluongmax = $str_chitiet[2];
                            if ($str_soluongout > 0) {
                                if ($str_soluongout >= 0 && $str_soluongout <= $soluongmax) {

                                    $array_output[$dem++] = $str_chitiet[0] . '_' . $str_chitiet[1] . '_' . $str_soluongout . '_' .
                                            $str_chitiet[3] . '_' . $str_chitiet[4] . '_' . $str_chitiet[5] . '_' . $description;
//namecolor(0)_namesize(1)_sloutput(2)_idcolor(3)_idsize(4)_idproauto(5)_description(8)
//vd:Đỏ[0]_S[1]_1[2]_1[3]_6[4]_22[5]_aaaaaaaa[6]
                                }
                            }
                        }// end for


                        if (!empty($array_output)) {
                            for ($g = 0; $g < count($array_output); $g++) {
                                $str = explode("_", $array_output[$g]);
                                $query = $db->getQuery(true); // lay soluong sp nay ở bang ton kho
                                $query->select($db->quoteName(array('total_import_product', 'total_export_product', 'total_number_sale')));
                                $query->from($db->quoteName('#__openshop_inventories'));
                                $query->where('product_id' . ' = ' . $str[5]);
                                $query->where('idsize' . ' = ' . $str[4]);
                                $query->where('idcolor' . ' = ' . $str[3]);
                                $query->where('warehouse_id' . ' = ' . $data['id_warehouse']);
                                $db->setQuery($query);
                                $resul_soluong_tonnhapkho = $db->loadObjectList();
                                foreach ($resul_soluong_tonnhapkho as $key => $val) {
                                    $soluong_tonnhapkho = $resul_soluong_tonnhapkho[$key]->total_import_product;
                                    $soluong_xuatkho = $resul_soluong_tonnhapkho[$key]->total_export_product;
                                    $soluong_tongban = $resul_soluong_tonnhapkho[$key]->total_number_sale;
// đã lấy số lượngnhập, tồn, bán
                                }

                                if ($soluong_xuatkho == 0 || $soluong_tongban == 0 || $soluong_tongban < $str[2]) {
                                    $thongbao["status"] = JText::_('OPENSHOP_ERROR');
                                    $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_NOT_SALES');

                                    break;
                                }



                                $data_outputpro = new stdClass(); // lưu du lieu tranhacungcap vao ios
                                $data_outputpro->order_date = date('Y-m-d H:i:s');
                                $data_outputpro->id_product = $str[5];
                                $data_outputpro->manufacturer_sku = $data['namemanufac'];
                                $data_outputpro->id_optioncolor = $str[3];
                                $data_outputpro->id_optionsize = $str[4];
                                $data_outputpro->quantity = $str[2];
                                $data_outputpro->id_status = $data['statusios'];
                                $data_outputpro->date_input = date('Y-m-d H:i:s');
                                $data_outputpro->description = $str[6];
                                $data_outputpro->created_date = date('Y-m-d H:i:s');
                                $data_outputpro->modified_date = date('Y-m-d H:i:s');
                                $data_outputpro->created_by = JFactory::getUser()->id;
                                $data_outputpro->id_warehouse = $data['id_warehouse'];
                                $data_outputpro->id_warehouse_to = $data[''];
                                switch ($data['statusios']) {
                                    case 1:
                                        $data_outputpro->invoice_sku = 'NHAPKHO_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                    case 2:
                                        $data_outputpro->invoice_sku = 'CHUYENKHO_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                    case 3:
                                        $data_outputpro->invoice_sku = 'TRANHACUNGCAP_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                    case 4:
                                        $data_outputpro->invoice_sku = 'THUHOI_' . date('Y-m-d') . '_' . $data['lenofday'];
                                        break;
                                }
                                $data_outputpro->xacnhan = 1;
                                JFactory::getDbo()->insertObject('#__openshop_ios', $data_outputpro);
// lưu vào ios
//update vào ton kho
                                $query = $db->getQuery(true);
                                $fields = array(
                                    $db->quoteName('total_import_product') . ' = ' . ($soluong_tonnhapkho - $str[2]),
                                    $db->quoteName('total_export_product' . ' = ' . ($soluong_xuatkho - $str[2])),
                                    $db->quoteName('total_number_sale' . ' = ' . ($soluong_tongban - $str[2]))
                                );
                                $conditions = array(
                                    $db->quoteName('product_id') . ' = ' . $str[5],
                                    $db->quoteName('idsize') . ' = ' . $str[4],
                                    $db->quoteName('idcolor') . ' = ' . $str[3],
                                    $db->quoteName('warehouse_id') . ' = ' . $data['id_warehouse']
                                );
                                $query->update($db->quoteName('#__openshop_inventories'))->set($fields)->where($conditions);
                                $db->setQuery($query);
                                $db->execute();
                                $thongbao["status"] = JText::_('OPENSHOP_SUCCESS');
                                $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_SAVE_SUCCESS');
                            }// end for
                        } else {// end if
                            $thongbao["status"] = JText::_('OPENSHOP_WARNING');
                            $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_NOT_EXITS_QUANTITY');
                        }
                        break;
                    }
                default :
                    break;
            }
        }

//        if ($data['statusios'] == 2) { // chuyen kho
//        }

//exit();
        if (!empty($array_sanpham_tontai)) { // save array san pham da ton tai, nhap kho
            $data_op_pro_exit = new stdClass();
            for ($k = 0; $k < count($array_sanpham_tontai); $k++) {
                $str_exit = explode("_", $array_sanpham_tontai[$k]);
                if ($str_exit[2] > 0) {
                    $data_op_pro_exit->order_date = date('Y-m-d H:i:s');
                    $data_op_pro_exit->id_product = $data['idpro_auto'];
                    $data_op_pro_exit->manufacturer_sku = $data['idproductmanufac'];
                    $data_op_pro_exit->id_optioncolor = $str_exit[0];
                    $data_op_pro_exit->id_optionsize = $str_exit[1];
                    $data_op_pro_exit->quantity = $str_exit[2];
                    $data_op_pro_exit->price_input = $data['price'];
                    $data_op_pro_exit->id_status = $data['statusios'];
                    $data_op_pro_exit->date_input = date('Y-m-d H:i:s');
                    $data_op_pro_exit->created_date = date('Y-m-d H:i:s');
                    $data_op_pro_exit->modified_date = date('Y-m-d H:i:s');
                    $data_op_pro_exit->created_by = JFactory::getUser()->id;
                    $data_op_pro_exit->id_option = 1;
                    $data_op_pro_exit->id_warehouse = $data['id_warehouse'];
                    $data_op_pro_exit->id_warehouse_to = $data['id_warehouse'];
                    $data_op_pro_exit->xacnhan = 1;
                    switch ($data['statusios']) {
                        case 1:
                            $data_op_pro_exit->invoice_sku = 'NHAPKHO_' . date('Y-m-d') . '_' . $data['lenofday'];
                            break;
                        case 2:
                            $data_op_pro_exit->invoice_sku = 'CHUYENKHO_' . date('Y-m-d') . '_' . $data['lenofday'];
                            break;
                        case 3:
                            $data_op_pro_exit->invoice_sku = 'TRANHACUNGCAP_' . date('Y-m-d') . '_' . $data['lenofday'];
                            break;
                        case 4:
                            $data_op_pro_exit->invoice_sku = 'THUHOI_' . date('Y-m-d') . '_' . $data['lenofday'];
                            break;
                    }
//$data_op_pro_exit->invoice_sku = $data['invoice_sku'] . '-' . $data['lenofday'];
                    JFactory::getDbo()->insertObject('#__openshop_ios', $data_op_pro_exit);

                    $thongbao["status"] = JText::_('OPENSHOP_SUCCESS');
                    $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_SAVE_SUCCESS');
                }
                array_push($id_arr, $this->getIdIoAdd());
            }

            /*
             * ADD RECORD INSERT INTO INVENTORIES TABLE
             */
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('id')
                    ->from($db->quoteName('#__openshop_products'))
                    ->where($db->quoteName('product_sku') . ' = "' . $data['id_product'] . '"');
            $id_product = $db->setQuery($query)->loadResult();

            for ($n = 0; $n < count($array_sanpham_tontai); $n++) { // insert bang ton kho TH:
                $str = explode("_", $array_sanpham_tontai[$n]);
                $query->clear();
                $query->select('id,total_import_product')
                        ->from($db->quoteName('#__openshop_inventories'))
                        ->where('product_id = ' . $id_product)
                        ->where('idsize = ' . $str[1])
                        ->where('idcolor = ' . $str[0])
                        ->where('warehouse_id = ' . $data['id_warehouse']);
                $id_inventory = $db->setQuery($query)->loadObject();

                if (count($id_inventory)) {      //condition exists => 1
                    $query->clear();
                    $query->update($db->quoteName('#__openshop_inventories'))
                            ->set('total_import_product = ' . ( (int) $str[2] + (int) $id_inventory->total_import_product ))
                            ->where('id = ' . $id_inventory->id);
                    $db->setQuery($query)->execute();
                } else {        //condition don't exists => 0
                    $profile = new stdClass();
                    $profile->product_id = $id_product;
                    $profile->idsize = $str[1];
                    $profile->idcolor = $str[0];
                    $profile->warehouse_id = $data['id_warehouse'];
                    $profile->total_import_product = $str[2];
                    $profile->total_export_product = 0;
                    $profile->total_number_sale = 0;
                    JFactory::getDbo()->insertObject('#__openshop_inventories', $profile);
                }
            }

            unset($array_sanpham_tontai);
            echo json_encode($thongbao);
        }

//        ------------------------------------------------

        if (!empty($array_sanpham_khongtontai)) { // nhap kho
            $data_op_pro_notexit = new stdClass();
            $data_op_pro_notexit->manufacproduct_sku = $data['idproductmanufac'];
            $data_op_pro_notexit->manufacturer_id = $data['namemanufac'];
            $data_op_pro_notexit->product_price = $data['price'];
            $data_op_pro_notexit->product_sku = $data['id_product'];
            $data_op_pro_notexit->published = 1;
            $data_op_pro_notexit->created_date = date('Y-m-d h:i:s');
            JFactory::getDbo()->insertObject('#__openshop_products', $data_op_pro_notexit);


            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('id');
            $query->from($db->quoteName('#__openshop_products'));
            $query->where($db->quoteName('product_sku') . ' = "' . $data['id_product'] . '"');
            $db->setQuery($query);
            $objects2 = $db->loadObjectList();

            foreach ($objects2 as $key => $val) {
                $lastid = $objects2[$key]->id;
            }

//            $query = $db->getQuery(true); // insert product vao bang __openshop_productdetails
//            $db->setQuery('insert into #__openshop_productdetails(product_id, product_name) VALUES ("' .
//                    $lastid . '","' .
//                    $data['nameproduct'] . '")');
//            $db->loadObjectList();

            $profile = new stdClass();
            $profile->product_id = $lastid;
            $profile->product_name = $data['nameproduct'];
            JFactory::getDbo()->insertObject('#__openshop_productdetails', $profile);

            for ($p = 0; $p < count($array_sanpham_khongtontai); $p++) {

                $str_noexit = explode("_", $array_sanpham_khongtontai[$p]);
                $data_op = new stdClass();
                $data_op->order_date = date('Y-m-d H:i:s');
                $data_op->id_product = $lastid;
                $data_op->manufacturer_sku = $data['namemanufac'];
                $data_op->id_optioncolor = $str_noexit[0];
                $data_op->id_optionsize = $str_noexit[1];
                $data_op->quantity = $str_noexit[2];
                $data_op->price_input = $data['price'];
                $data_op->id_status = $data['statusios'];
                $data_op->date_input = date('Y-m-d H:i:s');
                $data_op->created_date = date('Y-m-d H:i:s');
                $data_op->modified_date = date('Y-m-d H:i:s');
                $data_op->created_by = JFactory::getUser()->id;
                $data_op->id_option = 1;
                $data_op->id_warehouse = $data['id_warehouse'];
                $data_op->id_warehouse_to = $data['id_warehouse'];
                $data_op->invoice_sku = 'NHAPKHO_' . date('Y-m-d') . '_' . $data['lenofday'];
                $data_op->xacnhan = 1;

                JFactory::getDbo()->insertObject('#__openshop_ios', $data_op);
                array_push($id_arr, $this->getIdIoAdd());
            }// end for()

            $tt = 0;
            $array_soluong = array();
            for ($t = 0; $t < count($array_sanpham_khongtontai); $t++) {// lay all gia tri optionvalue id
                $str_option = explode("_", $array_sanpham_khongtontai[$t]);
                $array_soluong[$tt] = $str_option[2];
                $array_optionval[$tt++] = $str_option[0];
                $array_optionval[$tt++] = $str_option[1];
            }

            $resul2 = array_unique($array_optionval); // loai bo cac phan tu trung nhau
            $resul = array_keys($resul2);

            for ($c = 0; $c < count($resul); $c++) {

//$query = $db->getQuery(true);
                $query->clear();
                $query = $db->getQuery(true);
                $query->select('option_id,optionvalue_id');
                $query->from($db->quoteName('#__openshop_optionvaluedetails'));
                $query->where($db->quoteName('optionvalue_id') . ' = "' . $resul[$c] . '"');
                $db->setQuery($query);
                $obj1 = $db->loadObjectList();
                if (!empty($obj1)) {
                    foreach ($obj1 as $key => $val) {
                        $option_id = $obj1[$key]->option_id;
                        $optionvalue_id = $obj1[$key]->optionvalue_id;
                    }

                    $data_op = new stdClass();
                    $data_op->product_id = $lastid;
                    $data_op->option_id = $option_id;
                    $data_op->option_value_id = $optionvalue_id;
                    $data_op->price = $data['price'];
                    JFactory::getDbo()->insertObject('#__openshop_productoptionvalues', $data_op);
                }
            }


// insert or update bang ton kho xet 2 truong hop of nhap kho
            for ($n = 0; $n < count($array_sanpham_khongtontai); $n++) { // insert bang ton kho TH: sanpham moi hoan toan
                $str = explode("_", $array_sanpham_khongtontai[$n]);
                $profile = new stdClass();
                $profile->product_id = $lastid;
                $profile->idsize = $str[1];
                $profile->idcolor = $str[0];
                $profile->warehouse_id = $data['id_warehouse'];
                $profile->total_import_product = $str[2];
                $profile->total_export_product = 0;
                $profile->total_number_sale = 0;
                JFactory::getDbo()->insertObject('#__openshop_inventories', $profile);
//                }
            }
            $thongbao["status"] = JText::_('OPENSHOP_SUCCESS');
            $thongbao["messages"] = JText::_('OPENSHOP_MESSAGES_SAVE_SUCCESS');
            unset($array_sanpham_khongtontai);
            echo json_encode(array("masanpham" => $lastid));
        }


        if ($data['idpro_auto'] != "") {
            $id = $data['idpro_auto'];
        } else if ($lastid != "" && isset($lastid)) {
            $id = $lastid;
        }
        if ($data['statusios'] != 1) {
            echo json_encode($thongbao);
        }


//        echo json_encode($id_arr);
    }

    function getIdIoAdd() {
        $db = $this->getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('max(id)')
                ->from($db->quoteName('#__openshop_ios'));
        $db->setQuery($query);
        return $db->loadResult();
    }

    public function getTotal() {
// Lets load the content if it doesn't already exist
        if (empty($this->_total)) {
            $db = $this->getDbo();
            $where = $this->_buildContentWhereArray();
            $query = $db->getQuery(true);
            $query->select('COUNT(*)');
            if ($this->translatable) {
                $query->from($this->mainTable . ' AS a ')
                        ->innerJoin(OpenShopInflector::singularize($this->mainTable) . 'details AS b ON (a.id = b.' . OpenShopInflector::singularize($this->name) . '_id)');
            } else {
                $query->from($this->mainTable . ' AS a ');
            }
            $query->join('LEFT', $db->quoteName('#__openshop_products', 'b') . 'ON a.id_product = b.id')
                    ->join('LEFT', $db->quoteName('#__openshop_productdetails', 'c') . 'ON c.product_id = b.id')
                    ->join('LEFT', $db->quoteName('#__openshop_manufacturers', 'd') . 'ON d.id = b.manufacturer_id')
                    ->join('LEFT', $db->quoteName('#__openshop_warehouses', 'e') . 'ON e.id = a.id_warehouse');
            if (count($where))
                $query->where($where);

            $db->setQuery($query);
            $this->_total = $db->loadResult();
        }
        return $this->_total;
    }

}
