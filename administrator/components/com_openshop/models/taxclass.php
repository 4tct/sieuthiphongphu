<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopModelTaxclass extends OpenShopModel
{

	function store(&$data)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		if ($data['id'])
		{
			$query->delete('#__eshop_taxrules')
				->where('taxclass_id = ' . (int) $data['id']);
			$db->setQuery($query);
			$db->query();
		}
		parent::store($data);
		//save new data
		if (isset($data['tax_id']))
		{
			$taxGroupId = $data['id'];
			$taxIds = $data['tax_id'];
			$baseonIds = $data['based_on'];
			$priorityIds = $data['priority'];
			$query->clear();
			$query->insert('#__openshop_taxrules')->columns('taxclass_id, tax_id, based_on, priority');
			foreach ($taxIds as $key => $taxId)
			{
				$baseonId = $db->quote($baseonIds[$key]);
				$priorityId = $db->quote($priorityIds[$key]);
				$query->values("$taxGroupId, $taxId, $baseonId, $priorityId");
			}
			$db->setQuery($query);
			$db->query();
		}
		
		return true;
	}
	
	/**
	 * Function to copy taxclass
	 * @see EShopModel::copy()
	 */
	function copy($id)
	{
		$copiedTaxclassId = parent::copy($id);
		$db = $this->getDbo();
		$sql = 'INSERT INTO #__openshop_taxrules'
				. ' (taxclass_id, tax_id, based_on, priority)'
				. ' SELECT ' . $copiedTaxclassId . ', tax_id, based_on, priority'
				. ' FROM #__openshop_taxrules'
				. ' WHERE taxclass_id = ' . intval($id);
		$db->setQuery($sql);
		$db->query();
		return $copiedTaxclassId;
	}

}