<?php

defined('_JEXEC') or die('Restricted access');

class OpenShopModelIosconfirms extends OpenShopModelList {

    function __construct($config) {
        $config['state_vars'] = array(
            'export_warehouse_id' => array('', 'cmd', 1),
            'import_warehouse_id' => array('', 'cmd', 1)
        );
        parent::__construct($config);
    }

    public function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);

//        $uqery->'SELECT b.product_name, c.product_sku,
//        (select value from op_openshop_optionvaluedetails as b1 where a.id_optioncolor = b1.optionvalue_id) as color,
//        (select value from op_openshop_optionvaluedetails as b2 where a.id_optionsize = b2.optionvalue_id) as size,
//        a.quantity, a.invoice_sku, a.description,
//        (SELECT name FROM op_users as b3 WHERE a.created_by = b3.id ) as nguoixuat
//        FROM op_openshop_ios as a, op_openshop_productdetails as b, op_openshop_products as c
//        WHERE a.id_product = b.product_id
//        and a.id_product = c.id
//        and a.xacnhan = 0';

        $query->select('a.*,b.product_name, c.product_sku,
        (select value from op_openshop_optionvaluedetails as b1 where a.id_optioncolor = b1.optionvalue_id) as color,
        (select value from op_openshop_optionvaluedetails as b2 where a.id_optionsize = b2.optionvalue_id) as size,
        (SELECT e.warehouse_name FROM op_openshop_warehouses as e WHERE a.id_warehouse = e.id ) as khoxuat,
        (SELECT e2.warehouse_name FROM op_openshop_warehouses as e2 WHERE a.id_warehouse_to = e2.id ) as khonhap,
        (SELECT name FROM op_users as b3 WHERE a.created_by = b3.id ) as nguoixuat')
                ->from($db->quoteName('#__openshop_ios', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id_product = b.product_id')
                ->join('LEFT', $db->quoteName('#__openshop_products', 'c') . 'ON a.id_product = c.id');

        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

    public function _buildContentWhereArray() {
        $db = $this->getDbo();
        $state = $this->getState();
        $where = array();
        $where[] = ' a.id_product = b.product_id ';
        $where[] = ' a.id_product = c.id';
        $where[] = ' a.xacnhan = 0 ';
        
        if($state->export_warehouse_id){
            $where[] = ' a.id_warehouse = ' . $state->export_warehouse_id;
        }
        
        if($state->import_warehouse_id){
            $where[] = ' a.id_warehouse_to = ' . $state->import_warehouse_id;
        }
        
        return $where;
    }
    

    public function getPagination() {
        return 0;
    }

    function store(&$data) {

        //Success, Info,Warning, Error
        $array_chuyenkho = array();
        $dem_chuyenkho = 0;

        $thongbao = array("status" => "", "messages" => "");

        $idcheck = explode("_", $data['idAll']); // loai bo id phan tu dau tien
        for ($i = 1; $i <= $data['tongsoluong']; $i++) {// tong so cac radio button
            for ($j = 1; $j <= (count($idcheck) - 1); $j++) {// list cac radio check
                if ($idcheck[$j] == $i) {
                    //$idcheck[$j] id can xac nhan, xac nhan ios, cong kho nhập
                    // update ios
                    $str = explode("_", $data['txtxacnhan_' . $idcheck[$j]]);
                    // vd:CHUYENKHO[0]_2016-04-25 06:13:58[1]_0[2]_21[3_idproduct]_1[4_idcolor]_7[5_idsize]_1[6_makhoxuat]_2[7_makhonhap]_5[8_soluong]
                    
                    // cong dồn kho nhập
                    ////// Xet truong hop luu vao bang ton kho
                    switch ($str[0]) {
                        case "CHUYENKHO":
                            $array_chuyenkho[$dem_chuyenkho++] = $data['txtxacnhan_' . $idcheck[$j]];
                            break;
                        default:
                            $thongbao["status"] = "error";
                            $thongbao["messages"] = "Không xác định được mã hóa đơn";
                            break;
                    }
                    
                }
            }// end for j
        }// end for i
    
        // Kiem tra luu du lieu vao ton kho
        
        if (!empty($array_chuyenkho)) {
            // chuyen kho
            // update san pham nay trong bang ton kho TH: chuyen kho, san pham da ton tai
            //vd HCM->CT, kho HCM update công don soluong total_export, kho CT 
            for ($xx = 0; $xx < count($array_chuyenkho); $xx++) {
                $str_output2 = explode('_', $array_chuyenkho[$xx]);
                //vd:CHUYENKHO[0]_2016-05-03 12:01:44[1]_S[2]_27[3_id]_1[4_idcolor]_6[5_idsize]_
                //1[6_idwearhouse]_2[7_idwearhouseform]_5[8]
                
                
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $fields = array(
                    $db->quoteName('xacnhan') . ' = 1'
                );
                $conditions = array(
                    $db->quoteName('invoice_sku') . ' = "' . $str_output2[0] . '_' . $str_output2[1] . '_' . $str_output2[2] . '"',
                    $db->quoteName('id_product') . ' = ' . $str_output2[3],
                    $db->quoteName('id_optioncolor') . ' = ' . $str_output2[4],
                    $db->quoteName('id_optionsize') . ' = ' . $str_output2[5]
                );
                $query->update($db->quoteName('#__openshop_ios'))->set($fields)->where($conditions);
                $db->setQuery($query);
                $result = $db->execute(); // update xac nhan ios
                
                
                
                // lay so luong cua kho 1
                $query = $db->getQuery(true);
                $query->select($db->quoteName(array('product_id', 'idsize', 'idcolor', 'warehouse_id', 'total_import_product', 'total_export_product')))
                        ->from($db->quoteName('#__openshop_inventories'))
                        ->where($db->quoteName('product_id') . ' = ' . $str_output2[3])
                        ->where($db->quoteName('idcolor') . ' = ' . $str_output2[4])
                        ->where($db->quoteName('idsize') . ' = ' . $str_output2[5])
                        ->where($db->quoteName('warehouse_id') . ' = ' . $str_output2[6]);
                $db->setQuery($query);
                $results = $db->loadObjectList();

                // lay soluong kho 2
                $query->clear();
                $query = $db->getQuery(true);
                $query->select($db->quoteName(array('product_id', 'idsize', 'idcolor', 'warehouse_id', 'total_import_product', 'total_export_product')))
                        ->from($db->quoteName('#__openshop_inventories'))
                        ->where($db->quoteName('product_id') . ' = ' . $str_output2[3])
                        ->where($db->quoteName('idcolor') . ' = ' . $str_output2[4])
                        ->where($db->quoteName('idsize') . ' = ' . $str_output2[5])
                        ->where($db->quoteName('warehouse_id') . ' = ' . $str_output2[7]);
                $db->setQuery($query);
                $results_to = $db->loadObjectList();

                // Xet truong hop kho 2 da co san pham nay chua, chua co insert vao bang ton kho,
                // co roi thi update
                
                if (!empty($results_to)) { // san pham co trong kho 2 cap nhat công them total_import kho2
                    // kho 1 công them total_export và trừ bớt tổng nhập của kho 1
                    
                    $sl_temp = 0;
                    foreach ($results_to as $val) {
                        $sl_temp = $val->total_import_product; // lay tong so luong nhap kho 2 hien tai trong bang ton kho
                    }
                    
                    $query->clear();
                    $query = $db->getQuery(TRUE);
                    $sl_temp+=$str_output2[8]; // tong so luong can update vao bang ton kho

                    $fields = array(
                        $db->quoteName('total_import_product') . ' = ' . $sl_temp
                    );
                    $conditions = array(
                        $db->quoteName('product_id') . ' = ' . $str_output2[3],
                        $db->quoteName('idsize') . ' = ' . $str_output2[5],
                        $db->quoteName('idcolor') . ' = ' . $str_output2[4],
                        $db->quoteName('warehouse_id') . ' = ' . $str_output2[7]
                    );
                    $query->update($db->quoteName('#__openshop_inventories'))->set($fields)->where($conditions);
                    $db->setQuery($query);
                    $db->execute(); //update kho 2 xong (total_import)
                    // update kho 1
                    $sl_temp = 0;
                    $tongnhap_kho1 = 0;
                    foreach ($results as $val) {
                        $sl_temp = $val->total_export_product; // lay tong so luong xuat kho 1 hien tai trong bang ton kho
                        $tongnhap_kho1 = $val->total_import_product; // lay tong nhap kho 1
                    }


                    $query->clear();
                    $query = $db->getQuery(TRUE);
                    $sl_temp+=$str_output2[8]; // tong so luong can update vao bang ton kho 1


                    $fields = array(
                        $db->quoteName('total_export_product') . ' = ' . $sl_temp,
                        $db->quoteName('total_import_product') . ' = ' . ($tongnhap_kho1 - $str_output2[8])
                    );
                    $conditions = array(
                        $db->quoteName('product_id') . ' = ' . $str_output2[3],
                        $db->quoteName('idsize') . ' = ' . $str_output2[5],
                        $db->quoteName('idcolor') . ' = ' . $str_output2[4],
                        $db->quoteName('warehouse_id') . ' = ' . $str_output2[6]//$data['id_warehouse_from']
                    );
                    $query->update($db->quoteName('#__openshop_inventories'))->set($fields)->where($conditions);
                    $db->setQuery($query);
                    $db->execute(); //update kho 1 xong (total_import)
                } else { // trong kho 2 chua co san pham, insert vao bang ton kho ,kho 2
                    $profile = new stdClass();
                    $profile->product_id = $str_output2[3];
                    $profile->idsize = $str_output2[5];
                    $profile->idcolor = $str_output2[4];
                    $profile->warehouse_id = $str_output2[7];//$data['id_warehouse_to'];
                    $profile->total_import_product = $str_output2[8];
                    $profile->total_export_product = 0;
                    $profile->total_number_sale = 0;
                    JFactory::getDbo()->insertObject('#__openshop_inventories', $profile);
                    // them sanpham vao kho 2 xong
                    //capnhat kho 1
                    // update kho 1
                    $sl_temp = 0;
                    foreach ($results as $val) {
                        $sl_temp = $val->total_export_product; // lay tong so luong xuat kho 1 hien tai trong bang ton kho
                        $tongnhap_kho1 = $val->total_import_product; // lay tong nhap kho 1
                    }

                    $query->clear();
                    $query = $db->getQuery(TRUE);
                    $sl_temp+=$str_output2[8]; // tong so luong can update vao bang ton kho 1
                    $fields = array(
                        $db->quoteName('total_export_product') . ' = ' . $sl_temp,
                        $db->quoteName('total_import_product') . ' = ' . ($tongnhap_kho1 - $str_output2[8])
                    );
                    $conditions = array(
                        $db->quoteName('product_id') . ' = ' . $str_output2[3],
                        $db->quoteName('idsize') . ' = ' . $str_output2[5],
                        $db->quoteName('idcolor') . ' = ' . $str_output2[4],
                        $db->quoteName('warehouse_id') . ' = ' . $str_output2[6]//$data['id_warehouse_from']
                    );
                    $query->update($db->quoteName('#__openshop_inventories'))->set($fields)->where($conditions);
                    $db->setQuery($query);
                    $db->execute(); //update kho 1 xong (total_import)
                }
            }// end for
            
            $thongbao["status"] = JText::_('OPENSHOP_SUCCESS');
            $thongbao["messages"] = JText::_('OPENSHOP_CONFIRMS_SUCCESS');
        }else{
            $thongbao["status"] = JText::_('OPENSHOP_ERROR');
            $thongbao["messages"] = JText::_('OPENSHOP_CONFIRMS_NOT_SUCCESS');
        }



        echo json_encode($thongbao);
    }

}
