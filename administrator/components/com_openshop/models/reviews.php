<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelReviews extends OpenShopModelList
{

	function __construct($config)
	{
		$config['search_fields'] = array('a.review');
		parent::__construct($config);
	}

	public function _buildQuery()
	{
		$db = $this->getDbo();
		$state = $this->getState();
		$query = $db->getQuery(true);
		$query->select('a.*, b.product_name')
			->from($this->mainTable . ' AS a ')
			->innerJoin('#__openshop_productdetails AS b ON (a.product_id = b.product_id)')
			->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
		$where = $this->_buildContentWhereArray();
		if (count($where))
			$query->where($where);
		return $query;
	}
}