<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelAppointments extends OpenShopModelList {

    function __construct($config) {
        parent::__construct($config);
    }

    public function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);
        $query->select('a.*,z.zone_name,dt.district_name,d.transporter_name,e.telephone,f.appointment_type,'
                . 'f.appointment_date,f.appointment_description,c.sale')
                ->from($db->quoteName('#__openshop_orders', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_zones', 'z') . ' ON a.payment_zone_id = z.id')
                ->join('LEFT', $db->quoteName('#__openshop_districts', 'dt') . ' ON a.payment_district_id = dt.id')
                ->join('LEFT', $db->quoteName('#__openshop_carts', 'c') . 'ON c.order_id = a.id')
                ->join('LEFT', $db->quoteName('#__openshop_transporters', 'd') . 'ON d.id = c.transporter_id')
                ->join('LEFT', $db->quoteName('#__openshop_customers', 'e') . 'ON e.customer_id = a.customer_id')
                ->join('LEFT', $db->quoteName('#__openshop_appointments','f') . 'ON f.cart_id = c.id')
                ->where('order_status_id NOT IN (1)')
                ->where('f.appointment_status = "waiting"');
        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

}
