<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 * 
 * @package Joomla
 * @subpackage OpenShop
 */
class OpenShopModelMenus extends OpenShopModelList{
    function __construct($config) {
        $config['search_fields']    = array('a.menu_name');
        $config['state_vars'] = array(
			'menu_parent_id' => array('', 'cmd', 1));
        parent::__construct($config);
    }
    
    function getData() {
        if (empty($this->_data))
        {
            $db = JFactory::getDbo();
            $query = $this->_buildQuery();
            $query->select('a.menu_parent_id AS parent_id')->select('a.menu_name AS title');
//           print_r($query);exit();
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            $state = $this->getState();
            
            //children
            $children = array();
            if(count($rows))
            {
                foreach ($rows as $v) {
                    $pt = $v->parent_id;
                    if(!empty($children[$pt])&&  is_array($children[$pt])) {
                        $list = $children[$pt];
                    }
                    else {
                        $list = array();
                    }
                    //$list = $children[$pt] ? $children[$pt] : array();
                    array_push($list, $v);
                    $children[$pt] = $list;
                }
            }
            
            if(isset($state) && $state->menu_parent_id == '1')
            {
                $list = $rows;
            }
            else
            {
                $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999);
            }
            
            $total = count($list);
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($total, $this->getState('limitstart'), $this->getState('limit'));
            // slice out elements based on limits
            $list = array_slice($list, $this->_pagination->limitstart, $this->_pagination->limit);
            $this->_data = $list;
        }
        return $this->_data;
    }
    
    
    /*
     * Use filter
     */
    public function _buildContentWhereArray()
	{
		$db = $this->getDbo();
		$state = $this->getState();
		$where = array();
		if ($state->filter_state == 'P')
			$where[] = ' a.published = 1 ';
		elseif ($state->filter_state == 'U')
		$where[] = ' a.published = 0';
	
		if ($state->search)
		{
			$search = $db->quote('%' . $db->escape($state->search, true) . '%', false);
			if (is_array($this->searchFields))
			{
				$whereOr = array();
				foreach ($this->searchFields as $titleField)
				{
					$whereOr[] = " LOWER($titleField) LIKE " . $search;
				}
				$where[] = ' (' . implode(' OR ', $whereOr) . ') ';
			}
			else
			{
				$where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape($state->search, true) . '%', false);
			}
		}
	
		if ($this->translatable)
		{
			$where[] = 'b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"';
		}
	
		if ($state->menu_parent_id == '0')
		{
			$where[] = 'a.menu_parent_id = ' . $state->menu_parent_id;
		}
                
                if ($state->menu_parent_id == '1')
		{
			$where[] = 'a.menu_parent_id != 0';
		}
                
		return $where;
	}
}
?>