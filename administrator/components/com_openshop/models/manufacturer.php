<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelManufacturer extends OpenShopModel
{

	public function __construct($config)
	{
//		$config['translatable'] = true;
//		$config['translatable_fields'] = array('manufacturer_name', 'manufacturer_alias', 'manufacturer_desc', 'manufacturer_phone', 'manufacturer_email', 'manufacturer_address');
		
		parent::__construct($config);
	}

	/**
	 * Method to remove brand
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		if (count($cid))
		{
			$db = $this->getDbo();
			$cids = implode(',', $cid);
			$query = $db->getQuery(true);
			$query->select('id')
				->from('#__openshop_manufacturers')
				->where('id IN (' . $cids . ')');
			$db->setQuery($query);
			$manufacturers = $db->loadColumn();
			if (count($manufacturers))
			{
				$query->clear();
				$query->delete('#__openshop_manufacturers')
					->where('id IN (' . implode(',', $manufacturers) . ')');
				$db->setQuery($query);
				if (!$db->query())
					//Removed error
					return 0;
				$numItemsDeleted = $db->getAffectedRows();
			}
			else 
			{
				return 2;
			}
		}
		//Removed success
		return 1;
	}
}