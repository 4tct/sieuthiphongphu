<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelBrand extends OpenShopModel
{

	public function __construct($config)
	{
		$config['translatable'] = true;
		$config['translatable_fields'] = array('brand_name', 'brand_alias', 'brand_desc', 'brand_page_title', 'brand_page_heading');
		
		parent::__construct($config);
	}

	function store(&$data)
	{
		$imagePath = JPATH_ROOT . '/media/com_openshop/brands/';
		if (JRequest::getInt('remove_image') && $data['id'])
		{
			//Remove image first
			$row = new OpenShopTable('#__openshop_brands', 'id', $this->getDbo());
			$row->load($data['id']);
			
			if (JFile::exists($imagePath . $row->brand_image))
				JFile::delete($imagePath . $row->brand_image);
			
			if (JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->brand_image).'-100x100.'.JFile::getExt($row->brand_image)))
				JFile::delete($imagePath . 'resized/' . JFile::stripExt($row->brand_image).'-100x100.'.JFile::getExt($row->brand_image));
			$data['brand_image'] = '';
		}
		
		$brandImage = $_FILES['brand_image'];
		if ($brandImage['name'])
		{
			$checkFileUpload = OpenShopFile::checkFileUpload($brandImage);
			if (is_array($checkFileUpload))
			{
				$mainframe = JFactory::getApplication();
				$mainframe->enqueueMessage(sprintf(JText::_('ESHOP_UPLOAD_IMAGE_ERROR'), implode(' / ', $checkFileUpload)), 'error');
				$mainframe->redirect('index.php?option=com_openshop&task=brand.edit&cid[]=' . $data['id']);
			}
			else
			{
				if (is_uploaded_file($brandImage['tmp_name']) && file_exists($brandImage['tmp_name']))
				{
					if (JFile::exists($imagePath . $brandImage['name']))
					{
						$imageFileName = uniqid('image_') . '_' . JFile::makeSafe($brandImage['name']);
					}
					else
					{
						$imageFileName = JFile::makeSafe($brandImage['name']);
					}
					JFile::upload($brandImage['tmp_name'], $imagePath . $imageFileName);
					// Resize images
				
					$data['brand_image'] = $imageFileName;
					OpenShopHelper::resizeImage($imageFileName, JPATH_ROOT . '/media/com_openshop/brands/', 100, 100);
				}
			}
		}
		if (count($data['brand_customergroups']))
		{
			$data['brand_customergroups'] = implode(',', $data['brand_customergroups']);
		}
		else
		{
			$data['brand_customergroups'] = '';
		}
		parent::store($data);
		return true;
	}
	
	/**
	 * Method to remove brands
	 *
	 * @access	public
	 * @return boolean True on success
	 * @since	1.5
	 */
	public function delete($cid = array())
	{
		if (count($cid))
		{
			$db = $this->getDbo();
			$cids = implode(',', $cid);
			$query = $db->getQuery(true);
			$query->select('id')
				->from('#__openshop_brands')
				->where('id IN (' . $cids . ')');
			$db->setQuery($query);
			$brands = $db->loadColumn();
			if (count($brands))
			{
				$query->clear();
				$query->delete('#__openshop_brands')
					->where('id IN (' . implode(',', $brands) . ')');
				$db->setQuery($query);
				if (!$db->query())
					//Removed error
					return 0;
				$numItemsDeleted = $db->getAffectedRows();
				//Delete details records
				$query->clear();
				$query->delete('#__openshop_branddetails')
					->where('brand_id IN (' . implode(',', $brands) . ')');
				$db->setQuery($query);
				if (!$db->query())
					//Removed error
					return 0;
				//Remove SEF urls for categories
				for ($i = 0; $n = count($brands), $i < $n; $i++)
				{
					$query->clear();
					$query->delete('#__openshop_urls')
						->where('query LIKE "view=brand&id=' . $brands[$i] . '"');
					$db->setQuery($query);
					$db->query();
				}
				if ($numItemsDeleted < count($cid))
				{
					//Removed warning
					return 2;
				}
			}
			else 
			{
				return 2;
			}
		}
		//Removed success
		return 1;
	}
}