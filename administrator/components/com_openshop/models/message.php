<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage      OpenShop
 * @since 1.5
 */
class OpenShopModelMessage extends OpenShopModel
{
	function __construct($config)
	{
		$config['translatable'] = true;
		$config['translatable_fields'] = array('message_value');
		parent::__construct($config);
	}
}