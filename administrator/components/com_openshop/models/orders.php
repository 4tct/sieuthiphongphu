<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop Component Model
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopModelOrders extends OpenShopModelList {

    function __construct($config) {
        $config['search_fields'] = array('order_number', 'payment_telephone', 'payment_fullname', 'payment_email');
        $config['state_vars'] = array(
//            'filter_order_Dir' => array('DESC', 'cmd', 1),
            'transporter_filter' => array('', 'cmd', 1),
            'appointment_filter' => array('', 'cmd', 1),
            'date_from' => array('', 'cmd', 1),
            'date_to' => array('', 'cmd', 1)
        );
        parent::__construct($config);
    }

    /**
     * Basic build Query function.
     * The child class must override it if it is necessary
     *
     * @return string
     */
    public function _buildQuery() {
        $db = $this->getDbo();
        $state = $this->getState();
        $query = $db->getQuery(true);
        $query->select('a.*,z.zone_name,dt.district_name,d.transporter_name,e.telephone,f.appointment_type')
                ->from($db->quoteName('#__openshop_orders', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_zones', 'z') . ' ON a.payment_zone_id = z.id')
                ->join('LEFT', $db->quoteName('#__openshop_districts', 'dt') . ' ON a.payment_district_id = dt.id')
                ->join('LEFT', $db->quoteName('#__openshop_carts', 'c') . 'ON c.order_id = a.id')
                ->join('LEFT', $db->quoteName('#__openshop_transporters', 'd') . 'ON d.id = c.transporter_id')
                ->join('LEFT', $db->quoteName('#__openshop_customers', 'e') . 'ON e.customer_id = a.customer_id')
                ->join('LEFT', $db->quoteName('#__openshop_appointments', 'f') . 'ON f.cart_id = c.id')
                ->order('a.created_date DESC')
                ->order('order_status_id DESC')
				->group('a.id');
        $where = $this->_buildContentWhereArray();
        if (count($where))
            $query->where($where);
        $orderby = $this->_buildContentOrderBy();
        if ($orderby != '')
            $query->order($orderby);
        return $query;
    }

    /**
     * Build an where clause array
     *
     * @return array
     */
    public function _buildContentWhereArray() {
        $db = $this->getDbo();
        $state = $this->getState();
        $where = array();
        $orderStatusId = JRequest::getInt('order_status_id');
        if ($orderStatusId)
        {
            $where[] = 'a.order_status_id = ' . intval($orderStatusId);
        }
        else
        {
            $where[] = 'a.order_status_id NOT IN (1) ';
        }
        
        if ($state->search) {
            $search = $db->quote('%' . $db->escape($state->search, true) . '%', false);
            if (is_array($this->searchFields)) {
                $whereOr = array();
                foreach ($this->searchFields as $titleField) {
                    $whereOr[] = " LOWER($titleField) LIKE " . $search;
                }
                $where[] = ' (' . implode(' OR ', $whereOr) . ') ';
            } else {
                $where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape($state->search, true) . '%', false);
            }
        }

        if ($state->transporter_filter) {
            $where[] = 'd.id = ' . (int) $state->transporter_filter;
        }

        if ($state->appointment_filter) {
            $where[] = 'f.appointment_type = ' . (int) $state->appointment_filter;
        }
        
        if($state->date_from){
            $where[] = 'a.created_date >= "' . date('Y-m-d 00:00:00', strtotime($state->date_from)) .'"';
        }
        
        if($state->date_to){
            $where[] = 'a.created_date <= "' . date('Y-m-d 23:59:59', strtotime($state->date_to)) .'"';
        }

        return $where;
    }

    /**
     * Get total entities
     *
     * @return int
     */
    public function getTotal() {
        // Lets load the content if it doesn't already exist
        if (empty($this->_total)) {
            $db = $this->getDbo();
            $query = $db->getQuery(true);
            $query->select('COUNT(*)');

            $query->from($this->mainTable . ' AS a ')
                ->join('LEFT', $db->quoteName('#__openshop_zones', 'z') . ' ON a.payment_zone_id = z.id')
                ->join('LEFT', $db->quoteName('#__openshop_districts', 'dt') . ' ON a.payment_district_id = dt.id')
                ->join('LEFT', $db->quoteName('#__openshop_carts', 'c') . 'ON c.order_id = a.id')
                ->join('LEFT', $db->quoteName('#__openshop_transporters', 'd') . 'ON d.id = c.transporter_id')
                ->join('LEFT', $db->quoteName('#__openshop_customers', 'e') . 'ON e.customer_id = a.customer_id')
                ->join('LEFT', $db->quoteName('#__openshop_appointments', 'f') . 'ON f.cart_id = c.id');
            
            $where = $this->_buildContentWhereArray();
            if (count($where))
                $query->where($where);

            $db->setQuery($query);
            $this->_total = $db->loadResult();
        }
        return $this->_total;
    }

}
