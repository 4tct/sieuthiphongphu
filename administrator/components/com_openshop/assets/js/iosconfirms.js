/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$ = jQuery.noConflict();

Joomla.submitbutton = function (task) {
    switch (task) {
        case 'confirm':
            confirmImportWarehouse();
            break;
        case 'export_excel':
            Export_Excel_IO();
            break;
        default:
            break;
    }
};

function checkitem(item)
{
    if ($('#cbxacnhan_' + item).attr('class') == "icon-radio-unchecked")
    {
        $('#cbxacnhan_' + item).attr('class', 'icon-radio-checked');
        $('#cbxacnhan_' + item).attr("checked", true);
        var temp = $('#idAll').val();// lấy giá trị về
        temp = temp + '_' + item;// nối chuỗi thêm id vào
        $('#idAll').val(temp);
    } else if ($('#cbxacnhan_' + item).attr('class') == "icon-radio-checked")
    {
        $('#cbxacnhan_' + item).attr('class', 'icon-radio-unchecked');
        $('#cbxacnhan_' + item).attr("checked", false);
        var temp = $('#idAll').val().split("_");// tao chuoi thanh mảng
        var temp2 = '';
        for (var i = 1; i <= temp.length; i++) {// loại bỏ phần tử uncheck
            if (temp[i] != item && temp[i] != undefined) {
                temp2 = temp2 + "_" + temp[i];
            }
        }// end for
        $('#idAll').val(temp2);

    }//end elseif
}// end function

function checkIdIOAll() {
    var id = '';
    var idIO = $('#idALLIO').val().split('_');
    if ($('#cbxacnhan').is(':checked') == true) {
        for (var i = 1, l = idIO.length; i < l; i++) {
            $('#cbxacnhan_' + idIO[i]).attr("checked", true);
            $('#cbxacnhan_' + idIO[i]).attr("class", "icon-radio-checked");
            $('#idAll').val('');// xoa toan bo gia tri
            id = id + '_' + idIO[i];

        }
        $('#idAll').attr('value', id);
    }

    if ($('#cbxacnhan').is(':checked') == false) {
        for (var i = 1, l = idIO.length; i < l; i++) {
            $('#cbxacnhan_' + idIO[i]).attr("checked", false);
            $('#cbxacnhan_' + idIO[i]).attr("class", "icon-radio-unchecked");
            $('#idAll').val('');// xoa toan bo gia tri
        }
    }
}

//function old
function btn_save_ios_ajax() {

    $.ajax({
        url: 'index.php?option=com_openshop&task=iosconfirms.getFormAjax&format=ajax',
        method: 'post',
        data: {
            data: $('#adminForm').serializeArray()
        },
        success: function (dt)
        {


            var obj = $.parseJSON(dt);
            toastr[obj.status](obj.messages);
            if (obj.status != "") {
                var interval_obj = setInterval(
                        function ()
                        {
                            $('#adminForm').load('../administrator/index.php?option=com_openshop&view=iosconfirms').fadeIn("slow");
                            clearInterval(interval_obj);
                        }, 100);
            }

        }
    });
}


function confirmImportWarehouse() {
    if ($('#idAll').val() === '') {
        toastr['warning']('Vui lòng chọn sản phẩm xác nhận!');
    } else
    {
        $.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=iosconfirms.confirmImportWarehouse',
            method: 'post',
            data: {
                idIO: $('#idAll').val()
            },
            beforeSend: function (xhr) {
                $('.loading').css('display', 'block');
            },
            success: function (dt) {
                $('.loading').css('display', 'none');
                var d = $.parseJSON(dt);
                toastr[d['status']](d['message']);
                window.location.replace('index.php?option=com_openshop&view=iosconfirms');
            }
        });
    }
}
function show_search_tools()
{
    $('.search-tools').toggle(400);
}

function Export_Excel_IO(){
    jQuery('#myModalExport').modal();
}


/*
 * Lựa chọn nhà cung cấp
 * @returns {undefined}
 */
function changeExportManu(i){
    jQuery('.onchangeManuReset').removeClass('btn-primary');
    jQuery('.onchangeManuReset').removeClass('active');
    jQuery('.onchangeManu_' + i).addClass('btn-primary');
    jQuery('.onchangeManu_' + i).addClass('active');
    jQuery('#typeExport').val(i);
}

/*
 * xuất file cho nhà cung cấp
 */
function exportExcel(){
    var exportPlace = jQuery('#export_warehouse').val();
    var importPlace = jQuery('#import_warehouse').val();
    var check = 1;
    
    if(exportPlace === '0'){
        toastr['warning']('Vui lòng chọn nơi xuất');
        check = 0;
    }
    else if(importPlace === '0'){
        toastr['warning']('Vui lòng chọn nơi nhập');
        check = 0;
    }
    
    if(check){
        jQuery('#exportExcelModal').submit();
    }
}