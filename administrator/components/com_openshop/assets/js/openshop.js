function btn_close_ajax(task)
{
    window.location.replace('index.php?option=com_openshop&task=' + task);
}

function btn_save_ajax()
{
    $.ajax({
        xhr: function ()
        {
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    //Do something with upload progress
                    progressAjaxForm(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: 'index.php?option=com_openshop&task=order.getFormAjax&format=ajax',
        method: 'post',
        data: {
            data: $('#adminForm').serializeArray()
        },
        success: function (dt)
        {
            if ($('#cid').val() == '')
            {
                $('#adminForm')[0].reset();

                //delete product row
                for (var i = i_temp; i >= 1; i--) {
                    remove_tr('a_' + i);
                }

                $('.product-searched').html('');
            }
        }
    });
}


function validator(rules)
{
    var temp = 1;
    var res_validator = 1;
    $.each(rules, function (index, value)
    {
        var input = $('.' + value['class_input']).val();
        switch (value['cond']) {
            case 'required':
                if (input == '')
                {
                    $('.' + value['class_input']).css('border', '1px solid red');
                    if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                        $('.' + value['label']).css('color', 'red');
                    $('.error_' + value['class_input']).html('* required');
                    if (temp == 1)
                    {
                        $('.' + value['class_input']).focus();
                        toastr['warning']('Don\'t enouch information, Plaese!');
                        temp = 0;
                    }
                    res_validator = 0;
                } else
                {
                    $('.' + value['class_input']).css('border', '1px solid #DDDDDD');
                    if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                        $('.' + value['label']).css('color', '#333333');
                    $('.error_' + value['class_input']).html('');
                }
                break;
            case 'number':
                var digits = '0123456789';
                if (input == '')
                {
                    $('.' + value['class_input']).css('border', '1px solid red');
                    if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                        $('.' + value['label']).css('color', 'red');
                    $('.error_' + value['class_input']).html('* Only number');
                    if (temp == 1)
                    {
                        $('.' + value['class_input']).focus();
                        toastr['warning']('Don\'t enouch information, Plaese!');
                        temp = 0;
                    }
                    res_validator = 0;
                } else
                {
                    var res = 1;
                    for (var i = 0; i < input.length; i++)
                    {
                        var num = input.substr(i, 1);
                        if (digits.indexOf(num) === -1)
                        {
                            res = 0;
                        }
                    }

                    if (res === 1)
                    {
                        $('.' + value['class_input']).css('border', '1px solid #DDDDDD');
                        if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                            $('.' + value['label']).css('color', '#333333');
                        $('.error_' + value['class_input']).html('');
                    } else if (res === 0)
                    {
                        $('.' + value['class_input']).css('border', '1px solid red');
                        if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                            $('.' + value['label']).css('color', 'red');
                        $('.error_' + value['class_input']).html('* Only number');
                        res_validator = 0;
                    }
                }
                break;
            case 'email':
                if (input === '')
                {
                    $('.' + value['class_input']).css('border', '1px solid red');
                    if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                        $('.' + value['label']).css('color', 'red');
                    $('.error_' + value['class_input']).html('* required');
                    if (temp == 1)
                    {
                        $('.' + value['class_input']).focus();
                        toastr['warning']('Don\'t enouch information, Plaese!');
                        temp = 0;
                    }
                    res_validator = 0;
                } else
                {
                    var res_email = 1;
                    if (input.indexOf('@') === -1)
                    {
                        res_email = 0;
                    } else
                    {
                        var s = input.substr((input.indexOf('@') + 1), input.length);
                        if (s.indexOf('.') === -1)
                        {
                            res_email = 0;
                        }
                    }

                    if (res_email === 1)
                    {
                        $('.' + value['class_input']).css('border', '1px solid #DDDDDD');
                        if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                            $('.' + value['label']).css('color', '#333333');
                        $('.error_' + value['class_input']).html('');
                    } else if (res_email === 0)
                    {
                        $('.' + value['class_input']).css('border', '1px solid red');
                        if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                            $('.' + value['label']).css('color', 'red');
                        $('.error_' + value['class_input']).html('* Format email errror');
                        res_validator = 0;
                    }
                }
                break;
            case 'select':
                if (input == '' || input === 0)
                {
                    $('.' + value['class_input']).css('border', '1px solid red');
                    if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                        $('.' + value['label']).css('color', 'red');
                    $('.error_' + value['class_input']).html('* required');
                    if (temp == 1)
                    {
                        $('.' + value['class_input']).focus();
                        toastr['warning']('Don\'t enouch information, Plaese!');
                        temp = 0;
                    }
                    res_validator = 0;
                } else
                {
                    $('.' + value['class_input']).css('border', '1px solid #DDDDDD');
                    if (value['label'] !== '' && value['label'] !== null && typeof (value['label']) !== 'undefined')
                        $('.' + value['label']).css('color', '#333333');
                    $('.error_' + value['class_input']).html('');
                }
                break;
            default:
                break;
        }
    });

    if ((validator_custom() === 1 || validator_custom() === 'h9xz0') && res_validator === 1)
        return 1;
    else
        return 0;
}

function validator_custom() {
    return 'h9xz0';
}

function convert_money(num)
{
    var str_num = num.toString();
    var arr_num = new Array();
    var temp = 1;
    for (var i = (str_num.length - 1); i >= 0; i--)
    {
        if (temp === 3 && i !== 0 && str_num.slice(i - 1, i) !== '-')
        {
            arr_num[i] = '.' + str_num.slice(i, i + 1);
            temp = 1;
        } else
        {
            arr_num[i] = str_num.slice(i, i + 1);
            temp += 1;
        }
    }
    return arr_num.join('');
}

function convert_money_num(str) {
    var arr = str.split('.');
    var num = '';
    for (var i = 0, l = arr.length; i < l; i++) {
        num += arr[i];
    }

    return num;
}

function reset_filter(arr)
{
    if (arr !== null) {
        $.each(arr, function (index, value) {
            var temp = value.split(':');
            if (temp[1] !== null) {
                $('#' + temp[0]).val(temp[1]);
            } else
            {
                $('#' + temp[0]).val('');
            }
        });
    }

    this.form.submit();
}

function changeZone(v) {
    jQuery('#district_id').html('<option>Đang tải...</option>');
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=customer.changeZone',
        method: 'post',
        data: {
            id: v
        },
        success:function(dt){
            jQuery('#district_id').html(dt);
        }
    });
}

$(document).ready(function(){
//   $('#menu_toggle').on('click',function(){
//       $.ajax({
//           url: 'index.php?option=com_openshop&format=ajax&task=functions.setSession',
//           method: 'post',
//           success:function(){
//               //Code here
//           }
//       })
//   });
});