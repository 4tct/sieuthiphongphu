
/** Toggle form list data   **/
jQuery(document).ready(function () {
    jQuery(".adminfs").hide();
    if (jQuery(".alert-success").length) {
        //jQuery('#system-message-container').fadeOut();
        jQuery('#content').css('background', 'none');
        //toastr['success']("Tin Nhan");

    } else {
        jQuery('#content').css('background', 'url("components/com_openshop/assets/images/loading_1.gif") no-repeat center top');
    }
    //
    jQuery(".adminfs").animate({
        height: ["toggle", "swing"],
        opacity: "toggle"
    }, 1000, "linear", function () {
        jQuery('#content').css('background', 'none');
    });
});

/*
 * id_from  : id current of from
 * t        : table update
 * c_u      : column update
 * a_i      : access_id (status 0 or 1)
 * c_w      : column update
 * i        : use where in query sql
 * tsk      : task (controller)
 * u        :url of admin 
 */
function update_status_toggle(id_from, t, c_u, a_i, c_w, i, tsk, u)
{
    var url = 'index.php?option=com_openshop&task=' + tsk + '&format=ajax';
    jQuery.ajax({
        url: url,
        method: 'post',
        data: {
            table: t,
            col_u: c_u,
            access: a_i,
            col_w: c_w,
            id: i
        },
        beforeSend: function () {
            jQuery('#' + id_from).removeClass('icon-ok');
            jQuery('#' + id_from).removeClass('icon-cancel');
            jQuery('#' + id_from).css('background', 'url("' + u + 'components/com_openshop/assets/images/loading_1.gif") no-repeat');
            jQuery('#' + id_from).css('background-size', '24px');
            jQuery('#' + id_from).css('background-position-x', '7px');
        },
        success: function (dt)
        {
            //hide img loading
            jQuery('#' + id_from).css('background', 'none');
            if (dt)
            {
                if (a_i == 1)
                {
                    jQuery('#' + id_from).attr('onclick', 'update_status_toggle("' + c_u + '_' + i + '_0","' + t + '","' + c_u + '","0","' + c_w + '","' + i + '","' + tsk + '","' + u + '");');
                    jQuery('#' + id_from).addClass('icon-ok');
                    jQuery('#' + id_from).attr('id', c_u + '_' + i + '_0');
                } else if (a_i == 0)
                {
                    jQuery('#' + id_from).attr('onclick', 'update_status_toggle("' + c_u + '_' + i + '_1","' + t + '","' + c_u + '","1","' + c_w + '","' + i + '","' + tsk + '","' + u + '");');
                    jQuery('#' + id_from).addClass('icon-cancel');
                    jQuery('#' + id_from).attr('id', c_u + '_' + i + '_1');
                }
            } else
            {
                alert('ERROR');
            }
        }
    });
}

/*
 * id_class     : id of HTML
 * id           : variable
 */
function user_permissions(jQueryid_class, jQueryid)
{
    jQuery('#' + jQuery('#id_active').val()).removeClass('active');
    jQuery('#' + jQueryid_class).addClass('active');

    jQuery('#id_active').val(jQueryid_class);
}

/*
 * Check Alias
 * @param id        : id of from to get value
 * @param id_show   : id use to show result
 * @param task      : task (controller) -> get alias
 * @param u         : url of admin 
 */

function check_alias(id, id_show, table_check, task, u)
{
    var url = 'index.php?option=com_openshop&format=ajax&task=' + task;
    jQuery.ajax({
        url: url,
        method: 'post',
        data: {
            d: jQuery('#' + id).val().trim(),
            t: table_check
        },
        beforeSend: function ()
        {
            jQuery('#' + id_show).css('background', 'url("' + u + 'components/com_openshop/assets/images/loading_1.gif") no-repeat');
            jQuery('#' + id_show).css('background-size', '24px');
            jQuery('#' + id_show).css('background-position', 'right');
        },
        success: function (dt)
        {
            jQuery('#' + id_show).css('background', 'none');
//            jQuery('#' + id_show).val(dt);
            console.log(dt);
        }
    });
}

jQuery(function () {
    jQuery("body").append(jQuery("<div></div>").attr("id", "progress_loading"));
    progressAjaxForm(101);
});

function progressAjaxForm(percentComplete)
{
    jQuery('#progress_loading').attr('style', '');
    jQuery("#progress_loading").width(percentComplete + "%").delay(300).fadeOut(400);
}

function continueShowCompareInventory() {
    jQuery('.continue_show').css('display', 'none');
    var limit = jQuery('#limit').val();
    var num_row_cur = jQuery('#num_row').val();
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=compare.continueShowCompareInventory',
        method: 'post',
        data: {
            start: num_row_cur,
            limit: limit
        },
        beforeSend: function (xhr) {
            jQuery('.loading_data_compare').css('display', 'block');
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            var h = '';
            var class_compare = '';
            var class_icon = '';
            var q = '';
            var c = 0;

            jQuery.each(d, function (index, value) {
                c = 0;
                if (value['quantity'] === value['quantity_scan']) {
                    class_compare = '';
                } else if (value['quantity'] > value['quantity_scan']) {
                    class_compare = 'right';
                    c = 1;
                } else if (value['quantity'] < value['quantity_scan']) {
                    class_compare = 'left';
                }

                q = value['quantity_scan'];
                if (value['quantity_scan'] === null) {
                    q = 0;
                }

                h += '<tr>';
                h += '  <td class="text_center">';
                h += '      ' + (parseInt(num_row_cur) + index + 1);
                h += '  </td>';
                h += '  <td>';
                h += '      ' + value['product_name'];
                h += '  </td>';
                h += '  <td class="text_center">';
                h += '      ' + value['product_sku'] + '-' + value['product_size'];
                h += '  </td>';
                h += '  <td class="text_center" id="' + class_compare + '">';
                h += '      ' + value['quantity'];
                h += '  </td>';
                h += '  <td class="text_center" id="' + class_compare + '">';
                h += '      ' + q;
                if (c === 1) {
                    jQuery.ajax({
                        url: 'index.php?option=com_openshop&format=ajax&task=compare.compareProooo',
                        method: 'post',
                        data: {
                            code: value['product_sku']
                        },
                        success: function (dtt) {
                            var dP = jQuery.parseJSON(dtt);
                            h += '<br>';
                            jQuery.each(dP, function (i, v) {
                                h += '' + dP['product_sku'];
                                h += '<br>';
                            });
                        }
                    });
                }

                h += '  </td>';
                h += '</tr>';
            });

            jQuery('.compare_row').append(h);
            jQuery('#num_row').val(parseInt(limit) + parseInt(num_row_cur));
            jQuery('.continue_show').css('display', 'block');
            jQuery('.loading_data_compare').css('display', 'none');
        }
    });
}

function product_not_import() {
    jQuery('#product_not_import').modal('show');
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=compare.productNotImport',
        method: 'post',
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            var h = '';
            jQuery.each(d, function (index, value) {
                h += '<tr>'
                h += '  <td>';
                h += '      ' + (index + 1);
                h += '  </td>';
                h += '  <td>';
                h += '      ' + value['product_sku'] + '-' + value['size'];
                h += '  </td>';
                h += '  <td>';
                h += '      ' + value['quantity_import'];
                h += '  </td>';
                h += '</tr>';
            });

            jQuery('.content_product_not_import').html(h);
        }
    });
}

/*
 * tsk : controller
 * valup: value update
 */
function update_status_product(idP, tsk, valUp, idCur, clsAddOrRemove) {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=' + tsk + '',
        data: {
            idP: idP,
            valUp: valUp
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            toastr[d['status']](d['message']);
            if(d['status'] === 'success'){
                if(parseInt(valUp) === 1){
                    jQuery('#' + idCur).addClass(clsAddOrRemove);
                    jQuery('#' + idCur).attr("onclick","update_status_product("+ idP +",'"+ tsk +"',0,'"+ idCur +"','"+ clsAddOrRemove +"')");
                }
                else
                {
                    jQuery('#' + idCur).removeClass(clsAddOrRemove);
                    jQuery('#' + idCur).attr("onclick","update_status_product("+ idP +",'"+ tsk +"',1,'"+ idCur +"','"+ clsAddOrRemove +"')");
                }
            }
        }
    });
}