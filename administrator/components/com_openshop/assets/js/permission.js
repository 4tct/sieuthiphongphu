$ = jQuery.noConflict();

//function save_per()
//{
//    console.log($('#value_1_openshop_dashboard_core__select').val());
//}

function save_per(data) {
    jQuery('#' + data).toggleClass('icon-delete');
    jQuery('#' + data).toggleClass('icon-ok');

    var data_split = data.split('_');
    var act = data_split[data_split.length - 1];
    if (act === 'all')
    {
        var id_str = '';
        for (var i = 0, l = data_split.length; i < l - 1; i++) {
            if (i === 0)
            {
                id_str += data_split[i];
            } else
            {
                id_str += '_' + data_split[i];
            }
        }

        var arr_act = new Array('view', 'create', 'delete', 'edit', 'editstate');
        var d = new Array();
        if (jQuery('#value_' + data).val() === '0')
        {
            $.each(arr_act, function (index, value) {
                var id_str_temp = id_str;
                id_str_temp += '_' + value;
                jQuery('#' + id_str_temp).addClass('icon-ok');
                jQuery('#' + id_str_temp).removeClass('icon-delete');
                d.push(id_str_temp);
            });
            jQuery('#value_' + data).val('1');
            action_save_per(d, '1');
        } else
        {
            $.each(arr_act, function (index, value) {
                var id_str_temp = id_str;
                id_str_temp += '_' + value;
                jQuery('#' + id_str_temp).addClass('icon-delete');
                jQuery('#' + id_str_temp).removeClass('icon-ok');
                d.push(id_str_temp);
            });
            jQuery('#value_' + data).val('0');
            action_save_per(d, '0');
        }
    } else
    {
        if (jQuery('#value_' + data).val() == '0')
        {
            jQuery('#value_' + data).val('1');
        } else
        {
            jQuery('#value_' + data).val('0');
        }
        
        var d = new Array(data);
        
        action_save_per(d, jQuery('#value_' + data).val());
    }
}

function action_save_per(data,value) {
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=permissions.save_per',
        method: 'post',
        data: {
            data: data,
            value: value
        },
        beforeSend: function (xhr) {
            $('.loading').css('display','block');
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            $('.loading').css('display','none');
        }
    });
}

function apply_access()
{
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=permissions.apply_access',
        method: 'post',
        data: {
        },
        beforeSend: function (xhr) {
            $('.loading').css('display', 'block');
            $('.btn-success').attr('disabled', 'disabled');
        },
        success: function (dt, textStatus, jqXHR) {
            $('.btn-success').removeAttr('disabled');
            $('.loading').css('display', 'none');
            toastr['success']('Quyền đã được áp dụng');
        }
    });
}

function show_add_per()
{
    $('.modal_add_per').modal('show');
}

function save_per_new()
{
    if ($('#permission_name').val() === '')
    {
        toastr['warning']('Nhập tên quyền!');
    } else if ($('#permission_id').val() === '0')
    {
        toastr['warning']('Chọn nhóm quyền cha!');
    } else
    {
        $.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=permissions.checkPermissionName',
            method: 'post',
            data: {
                per_name: $('#permission_name').val()
            },
            success: function (dt) {
                if (dt === 'dexists') {
                    savePermission();
                } else
                {
                    toastr['warning']('Tên quyền đã tồn tại!');
                }
            }
        });
    }

}

function savePermission() {
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=permissions.save_per_new',
        method: 'post',
        data: {
            per_name: $('#permission_name').val(),
            id_per: $('#permission_id').val()
        },
        success: function (dt, textStatus, jqXHR) {
            $('.modal_add_per').modal('hide');
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            window.location.replace('index.php?option=com_openshop&view=permissions');
        }
    });
}

function getIDDelete(id) {
    $('#id_delete_per').val(id);
}

function show_modal_delete_per()
{
    $('.modal_delete_per').modal('show');
}

function delete_per()
{
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=permissions.delete_per',
        method: 'post',
        data: {
            id: $('#id_delete_per').val()
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            window.location.replace('index.php?option=com_openshop&view=permissions');
        }
    });
}