$ = jQuery.noConflict();

if ("undefined" === typeof OpenShop) {
    var OpenShop = {};
}
OpenShop.updateStateList = function (countryId, stateInputId) {
    // First of all, we need to empty the state dropdown	
    var list = document.getElementById(stateInputId);
    // empty the list
    for (i = 1; i < list.options.length; i++) {
        list.options[i] = null;
    }
    list.length = 1;
    var stateNames = stateList[countryId];
    if (stateNames) {
        var arrStates = stateNames.split(',');
        i = 0;
        var state = '';
        var stateName = '';
        for (var j = 0; j < arrStates.length; j++) {
            state = arrStates[j];
            stateName = state.split(':');
            opt = new Option();
            opt.value = stateName[0];
            opt.text = stateName[1];
            list.options[i++] = opt;
        }
        list.length = i;
    }
}

OpenShop.updateDistrictList = function(zone_Id, inputId)
{
    // First of all, we need to empty the state dropdown	
    var list = document.getElementById(inputId);
    // empty the list
    for (i = 1; i < list.options.length; i++) {
        list.options[i] = null;
    }
    list.length = 1;
    
    var stateNames = stateDistrictList[zone_Id];
    if (stateNames) {
        var arrStates = stateNames.split(',');
        i = 0;
        var state = '';
        var stateName = '';
        for (var j = 0; j < arrStates.length; j++) {
            state = arrStates[j];
            stateName = state.split(':');
            opt = new Option();
            opt.value = stateName[0];
            opt.text = stateName[1];
            list.options[i++] = opt;
        }
        list.length = i;
    }
}

function check_geozone(i)
{
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=functions.check_geozone',
        method: 'post',
        data: {
            zone_id         : $('#zone' + i).val(),
            district_id     : $('#district' + i).val()
        },
        success: function(dt){
            var d = $.parseJSON(dt);
            if(d['status'] == 'error')
            {
                toastr[d['status']](d['message'] + ' - Transporter: ' + d['transporter']);
                $('#district' + i).css('border','2px solid red');
                $('#zone' + i).css('border','2px solid red');
                $('#check_error').val('1');
            }
            else
            {
                toastr[d['status']](d['message']);
                $('#district' + i).css('border','1px solid #cccccc');
                $('#zone' + i).css('border','1px solid #cccccc');
                $('#check_error').val('0');
            }
            
        }
    })
}



