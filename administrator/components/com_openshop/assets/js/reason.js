$ = jQuery.noConflict();
function add_reason_detail()
{
    var i = parseInt($('#max_value').val());
    i = i + 1;
    var h = '';
    h += '<tr id="tr_' + i + '">';
    h += '  <td>';
    h += '      <input class="form-control" type="text" name="reason_name_desc_' + i + '" id="reason_name_desc_' + i + '" size="" maxlength="250" value="" />';
    h += '  </td>';
    h += '  <td style="text-align: center;">';
    h += '      <div class="btn btn-danger" id="btn_delete_reson_detail_' + i + '" onclick="delete_reason_detail(' + i + ');"><i class="icon-delete"></i></div>';
    h += '      <input type="hidden" name="id_value_reason_detail_'+ i +'" id="id_value_reason_detail_'+ i +'" value="" />';
    h += '  </td>';
    h += '</tr>';
    $('#content_reason_detail').append(h);
    $('#max_value').val(i);
}

function delete_reason_detail(j)
{
    if ($('#cid').val() != '')
    {
        $.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=reason.delete_reason_detail',
            method: 'post',
            data: {
                id_reason_detail: $('#id_value_reason_detail_' + j).val()
            },
            success: function (dt)
            {
                var d = $.parseJSON(dt);
                if (d['status'] == 'success')
                {
                    toastr[d['status']](d['message']);
                    $('#tr_' + j).remove();
                    var i_max = parseInt($('#max_value').val());
                    for (var i = j; i <= i_max; i++)
                    {
                        $('#tr_' + (i + 1)).attr('id', 'tr_' + i);
                        
                        $('#reason_name_desc_' + (i + 1)).attr('name', 'reason_name_desc_' + i);
                        $('#reason_name_desc_' + (i + 1)).attr('id', 'reason_name_desc_' + i);
                        
                        $('#btn_delete_reson_detail_' + (i + 1)).attr('onclick', 'delete_reason_detail(' + i + ')');
                        $('#btn_delete_reson_detail_' + (i + 1)).attr('id', 'btn_delete_reson_detail_' + i);
                        
                        $('#id_value_reason_detail_' + (i + 1)).attr('name', 'id_value_reason_detail_' + i);
                        $('#id_value_reason_detail_' + (i + 1)).attr('id', 'id_value_reason_detail_' + i);
                    }
                    $('#max_value').val((i_max - 1));
                }
            }
        });
    }
}

function btn_submit_ajax(task, id_form)
{
    if ($('#reason_name').val() == '')
    {
        toastr['warning']('Error!');
    } else
    {
        $.ajax({
            url: 'index.php?option=com_openshop&task=' + task + '&format=ajax',
            method: 'post',
            data: {
                data: $('#' + id_form).serializeArray()
            },
            success: function (dt)
            {
                console.log(dt);
                var d = $.parseJSON(dt);
                var i_temp = $('#max_value').val();
                if ($('#cid').val() == '')
                {
                    $('#adminForm')[0].reset();

                    //delete product row
                    for (i = i_temp; i >= 1; i--) {
                        $('#tr_' + i).remove();
                    }
                    $('#max_value').val(0);
                }
                toastr[d['status']](d['message']);
            }
        });
    }
}