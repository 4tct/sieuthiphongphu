/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$ = jQuery.noConflict();
function add_media_file() {
    var num = $('#mun_row_media').val();
    num = parseInt(num) + 1;
    //Photo & Video
    var h = '';
    h += '<tr class="row_' + num + '">';
    h += '  <td style="vertical-align: middle;">';
    h += '      <input type="file" name="media_file_' + num + '" id="media_file_' + num + '" style="margin: 0 0 10px 0;"/>';
    h += '  </td>';
    h += '  <td style="vertical-align: middle;text-align: center;">';
    h += '      <span class="icon-delete delete_media_' + num + '" style="color:red;" onclick="deleteRowMedia(' + num + ')"></span>';
    h += '  </td>';
    h += '</tr>';

    //File
    var hf = '';
    hf += '<tr class="row_' + num + '">'
    hf += '  <td style="vertical-align: middle;">';
    hf += '      <div>';
    hf += '         <div style="display: inline-block;width: 70px;">File:</div><input type="file" name="media_file_' + num + '" id="media_file_' + num + '" style="margin: 0 0 10px 0;    display: inline-block;"/>';
    hf += '     <div>';
    hf += '     <div>';
    hf += '         <div style="display: inline-block;width: 70px;">Image:</div><input type="file" name="media_file_image_' + num + '" id="media_file_image_' + num + '" style="margin: 0 0 10px 0;    display: inline-block;"/>';
    hf += '     </div>';
    hf += '     <div>Detail:</div>';
    hf += '     <div><textarea name="media_detail_'+ num +'" class="form-control"></textarea></div>';
    hf += '  </td>';
    hf += '  <td style="vertical-align: middle;text-align: center;">';
    hf += '      <span class="icon-delete delete_media_' + num + '" style="color:red;" onclick="deleteRowMedia(' + num + ')"></span>';
    hf += '  </td>';
    hf += '</tr>';

    if ($('#media_type').val() === '1') {
        $('.rows_media').append(h);
    } else if ($('#media_type').val() === '2') {
        $('.rows_media').append(hf);
    }

    $('#mun_row_media').val(num);
}

function deleteRowMedia(num) {
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=media.deleteRowMedia',
        method: 'post',
        data: {
            id: $('#mediadetail_id_' + num).val()
        },
        success: function (dt) {
            if (dt === 'success') {
                toastr['success']('Success');

                $('.row_' + num).remove();
                var numM = $('#mun_row_media').val();
                for (var i = (num + 1); i <= numM; i++) {
                    $('.row_' + i).attr('class', 'row_' + (i - 1));
                    $('.media_file' + i).attr('name', 'media_file_' + (i - 1));
                    $('.media_file' + i).attr('id', 'media_file_' + (i - 1));
                    $('.delete_media_' + i).attr('onclick', 'deleteRowMedia(' + (i - 1) + ')');
                    $('.delete_media_' + i).attr('class', 'icon-delete delete_media_' + (i - 1));
                    $('#mediadetail_id_' + i).attr('id', 'mediadetail_id_' + (i - 1));
                }
                $('#mun_row_media').val(--numM);
            } else
            {
                toastr['error']('Faild');
            }
        }
    });
}

function choose_media_type(value) {
    $('.rows_media').html('');
    $('#mun_row_media').val('0');
    if (value === '1') {
        $('.media_type_title').text('Photos & Videos');
    } else if (value === '2') {
        $('.media_type_title').text('Files');
    }
}
