$(document).ready(function () {
    $('.chosen').chosen();
    $('.btn-export').on('click',function(){
        var rules = [
            {label: 'export_name_label', class_input: 'export_name', cond: 'required'},
            {label:'status_export_label', class_input: 'status_export', cond: 'select'},
            {label:'column-choose-label', class_input: 'content-export-column', cond: 'required'}
        ]
        if(!validator(rules))
        {
            return false;
        }
        
       if($('#status_export').val() === null ){
           toastr['warning']('Chọn trạng thái cần xuất dữ liệu');
           $('.status_export_label').css('color','red');
           return false;
       }
    });
});


function show_modal_export()
{
    $('#modal-cart-export').modal('show');
    $.each($('.content-export-column').val().split(','), function (index, value)
    {
        $('.column-export-' + value).removeClass('active_column_export');
    });
    
    $('.export_name').val('');
    $('.order_column').html('');
    $('#status_export').val();
    $('.content-export-column').val('');
}

function choose_column_export(id)
{
    if ($('.content-export-column').val() == '')
    {
        var arr = new Array();
    } else
    {
        var arr = new Array();
        $.each($('.content-export-column').val().split(','), function (index, value)
        {
            arr.push(value);
        });
    }

    if ($('.column-export-' + id).hasClass('active_column_export'))
    {
        $('.column-export-' + id).removeClass('active_column_export');
        var index = arr.indexOf(id);
        arr.splice(index, 1);
        $('.content-export-column').val(arr);
        var h = '';
        if ($('.content-export-column').val() == '')
        {
            $('.order_column').html(h);
        } else
        {
            $.each($('.content-export-column').val().split(','), function (index, value)
            {
                h += '<span class="column-export-active">' + getNameColumnExxport(value) + '</span>';
            });
        }

        $('.order_column').html(h);
    } else
    {
        $('.column-export-' + id).addClass('active_column_export');
        arr.push(id);
        $('.content-export-column').val(arr);

        var h = '<span class="column-export-active">' + getNameColumnExxport(id) + '</span>';

        $('.order_column').append(h);
    }

}

function getNameColumnExxport(id)
{
    var res = '';
    switch (id) {
        case '1':
            res = 'Mã sản phẩm';
            break;
        case '2':
            res = 'Tên sản phẩm';
            break;
        case '3':
            res = 'Thành tiền';
            break;
        case '4':
            res = 'Tên khách hàng';
            break;
        case '5':
            res = 'Số điện thoại';
            break;
        case '6':
            res = 'Địa chỉ';
            break;
        default:
            break;
    }

    return res;
}