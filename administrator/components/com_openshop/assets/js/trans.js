$ = jQuery.noConflict();

function btn_save_transpoter_ajax()
{
    $.ajax({
        url: 'index.php?option=com_openshop&task=transporter.getFormTransporterAjax&format=ajax',
        method: 'post',
        data: {
            data: $('#adminForm').serializeArray()
        },
        success: function (dt)
        {
            $('#adminForm')[0].reset();
            toastr['success']('Lưu thành công rùi chế ơi! Mừng wa');
        }
    });
}

function delete_trasporter(id)
{
    $('#reason_delete_transporter').val(0);
    $('#reason_delete_transporter_desc').val('');
    $('#modal_reason_delete_trasnsporter').modal('show');
    $('.delete_transporter').attr('onclick', 'delete_ajax(' + id + ')');
}

function delete_ajax(id)
{
    var rules = [
        {label: 'reason_delete_transporter_label', class_input: 'reason_delete_transporter', cond: 'select'}
    ];

    if (validator(rules))
    {
        $.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=transporter.delete_ajax',
            method: 'post',
            data: {
                id: id,
                reason: $('#reason_delete_transporter').val(),
                desc: $('#reason_delete_transporter_desc').val()
            },
            success: function (dt) {
                var d = $.parseJSON(dt);
                toastr[d['status']](d['message']);
                $('#modal_reason_delete_trasnsporter').modal('hide');
                window.location.href = 'index.php?option=com_openshop&view=transporters';
            }
        });
    }
}


