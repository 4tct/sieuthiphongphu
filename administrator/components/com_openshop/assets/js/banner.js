function getProductSearch(v) {
    if (v.length >= 4) {
        $.ajax({
            url: 'index.php?option=com_openshop&task=product.getProductSearch&format=ajax',
            method: 'post',
            data: {
                v: v
            },
            success: function (dt) {
                var d = $.parseJSON(dt);
                if (d !== '') {
                    $('.infoSearch').css('display', 'block');
                    $('.infoSearch ul').html(d.html);
                }
            }
        });
    }
    else{
        $('.infoSearch').css('display', 'none');
    }
}

function setChooseProduct(id, name, img) {
    var html = '';

    if ($('.checkClass').hasClass('tr_' + id)) {
        toastr['warning']('Chọn rùi. Bộ bị lé không thấy hả?');
    } else {
        html += '<tr class="tr_' + id + '">';
        html += '   <td>1</td>';
        html += '   <td>' + name + ' <input type="hidden" value="' + id + '" name="arr_product[]" /></td>';
        html += '   <td><img src="' + img + '" width="50px"/></td>';
        html += '   <td><i class="icon-delete iconDeletePro" onclick="setDeletePro(' + id + ')"></i></td>';
        html += '</tr>';

        $('#loadProduct').append(html);
        $('.li_' + id).remove();
    }
}

function setDeletePro(i) {
    $('.tr_' + i).remove();
}