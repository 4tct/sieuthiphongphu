//jQuery = jQuery.noConflict();
/*
 * 
 * Default ready key down
 */
KeyDownCtrl();
var i_temp;

function initialize()
{
    i_temp = parseInt(jQuery('#max_value').val());
}

/*
 * 
 * @param {type} param1
 * @param {type} param2
 */

function add_new_product() {
    jQuery('#add_product').append(function () {
        var h = "";
        i_temp = parseInt(jQuery('#max_value').val());
        i_temp = i_temp + 1;
        getWeightHeight(i_temp);
        h += "<tr id='a_" + (i_temp) + "'>";
        h += "<td style='width: 35px;' class='center'><span name='stt_" + i_temp + "' id='stt_" + i_temp + "'>" + i_temp + "</span></td>";
        h += "<td style='width: 100px;'><input type='text' name='product_id_" + i_temp + "' id='product_id_" + i_temp + "' class='input-small' onchange='search_product(" + i_temp + ");' /></td>";
        h += "<td style='width: 200px;'>";
        h += "  <span name='product_name_s" + i_temp + "' id='product_name_s" + i_temp + "'></span>";
        h += "  <input type='hidden' value='' name='product_name_" + i_temp + "' id='product_name_" + i_temp + "' ";
        h += "</td>";
        h += "<td style='width: 100px;'><img src='/administrator/components/com_openshop/assets/images/no-image.png' alt='image' name='img_product_" + i_temp + "' id='img_product_" + i_temp + "'/></td>";
        h += "<td style='width: 14%;' class=''>" +
                "<select name='product_size_" + i_temp + "' id='product_size_" + i_temp + "' style='width: 70px'><option>Size</option></select>" +
                "<select name='product_color_" + i_temp + "' id='product_color_" + i_temp + "' style='width: 70px; margin-left: 10px;'><option>Color</option></select>" +
                "<select name='optionweight_id_" + i_temp + "' id='optionweight_id_" + i_temp + "' style='width: 150px'></select>" +
                "<select name='optionheight_id_" + i_temp + "' id='optionheight_id_" + i_temp + "' style='width: 150px'><option>Height</option></select>" +
                "</td>";
        h += "<td style='width: 100px;'><input type='text' name='product_price_" + i_temp + "' id='product_price_" + i_temp + "' class='input-small' readonly='readonly' value='' /></td>";
        h += "<td style='width: 100px;'><input type='text' name='product_quantity_" + i_temp + "' id='product_quantity_" + i_temp + "' class='input-small' onchange=\"total_price(" + i_temp + ");\" value='0' /></td>";
        h += "<td style='width: 100px;'><input type='text' name='product_total_" + i_temp + "' id='product_total_" + i_temp + "' class='input-small' readonly='readonly' value='' /></td>";
        h += "<td style='width: 250px;'><textarea rows='3' cols='50' name='order_product_desc_" + i_temp + "' id='order_product_desc_" + i_temp + "'></textarea></td>";
        h += '<td><div id="remove_tr_' + i_temp + '" class="btn-edit btn-primary-edit" onclick="remove_tr(\'a_' + i_temp + '\');">Xóa</div></td>';
        h += "<td style='display: none;'>";
        h += "  <input type='text' name='product_sku_" + i_temp + "' id='product_sku_" + i_temp + "' class='input-small' value='' />'";
        h += "  <input type='text' name='product_price_input_" + i_temp + "' id='product_price_input_" + i_temp + "' class='input-small' value='' />'";
        h += "</td>";
        h += "</tr>";

        jQuery('#max_value').val(i_temp);
        return h;
    });
    jQuery('#product_id_' + i_temp).focus();
}

/*
 * get Weight , get Height
 * @param {type} i_temp
 * @returns {undefined}
 */
function getWeightHeight(i_temp)
{
    var weight = '';
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.getWeightHeight',
        method: 'post',
        data: {i: i_temp},
        success: function (dt)
        {
            jQuery('#optionweight_id_' + i_temp).html(jQuery.parseJSON(dt)['weight']);
            jQuery('#optionheight_id_' + i_temp).html(jQuery.parseJSON(dt)['height']);
        }
    });
}

/*
 * 
 * @param {type} pos
 * @returns {undefined}
 */
function active_buyfrom(id)
{

    jQuery('#list' + jQuery('#curent_active_list').val()).removeClass('active');
    jQuery('#list' + id).addClass('active');
    jQuery('#curent_active_list').val(id);
}

/*
 *  Remove tag tr in table add new product
 */

function remove_tr(pos)
{
    var i_temp = jQuery('#max_value').val();
    document.getElementById(pos).remove();
    jQuery('#max_value').val(--i_temp);
    var arr_pos = pos.split('_');

    for (var i = parseInt(arr_pos[1]); i <= i_temp; i++)
    {
        document.getElementById('a_' + (i + 1)).setAttribute('id', 'a_' + i);
//            stt
        document.getElementById('stt_' + (i + 1)).setAttribute('name', 'stt_' + i);
        document.getElementById('stt_' + (i + 1)).setAttribute('id', 'stt_' + i);
//            id
        document.getElementById('product_id_' + (i + 1)).setAttribute('name', 'product_id_' + i);
        document.getElementById('product_id_' + (i + 1)).setAttribute('id', 'product_id_' + i);
//            name
        document.getElementById('product_name_' + (i + 1)).setAttribute('name', 'product_name_' + i);
        document.getElementById('product_name_' + (i + 1)).setAttribute('id', 'product_name_' + i);
//            img
        document.getElementById('img_product_' + (i + 1)).setAttribute('name', 'img_product_' + i);
        document.getElementById('img_product_' + (i + 1)).setAttribute('id', 'img_product_' + i);
//            size
        document.getElementById('product_size_' + (i + 1)).setAttribute('name', 'product_size_' + i);
        document.getElementById('product_size_' + (i + 1)).setAttribute('id', 'product_size_' + i);
//            Quantity
        document.getElementById('product_quantity_' + (i + 1)).setAttribute('onchange', 'total_price(' + i + ');');
        document.getElementById('product_quantity_' + (i + 1)).setAttribute('name', 'product_quantity_' + i);
        document.getElementById('product_quantity_' + (i + 1)).setAttribute('id', 'product_quantity_' + i);
//            price
        document.getElementById('product_price_' + (i + 1)).setAttribute('name', 'product_price_' + i);
        document.getElementById('product_price_' + (i + 1)).setAttribute('id', 'product_price_' + i);
//        print input
        document.getElementById('product_price_input_' + (i + 1)).setAttribute('name', 'product_price_input_' + i);
        document.getElementById('product_price_input_' + (i + 1)).setAttribute('id', 'product_price_input_' + i);
//            total
        document.getElementById('product_total_' + (i + 1)).setAttribute('name', 'product_total_' + i);
        document.getElementById('product_total_' + (i + 1)).setAttribute('id', 'product_total_' + i);
//            desc
        document.getElementById('order_product_desc_' + (i + 1)).setAttribute('name', 'order_product_desc_' + i);
        document.getElementById('order_product_desc_' + (i + 1)).setAttribute('id', 'order_product_desc_' + i);
        //product_sku
        document.getElementById('product_sku_' + (i + 1)).setAttribute('name', 'product_sku_' + i);
        document.getElementById('product_sku_' + (i + 1)).setAttribute('id', 'product_sku_' + i);
        //btn
        document.getElementById('remove_tr_' + (i + 1)).setAttribute('onclick', 'remove_tr(\'a_' + i + '\')');
        document.getElementById('remove_tr_' + (i + 1)).setAttribute('id', 'remove_tr_' + i);
        jQuery('#stt_' + i).text(i);
    }

    total_price_not_discount();
}


/*
 * CHECK TOWN -> SHOW DISTRICT
 */

function check_town()
{
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.check_town',
        method: 'post',
        data: {
            id: jQuery('#payment_zone_id option:selected').val()
        },
        beforeSend: function ()
        {
            jQuery('#loading_d').css('display', 'block');
        },
        success: function (dt)
        {
            jQuery('#loading_d').css('display', 'none');
            jQuery('#show_districts').html(jQuery.parseJSON(dt)['district']);
        }
    });
}

/*
 *  -------------- CHECK USER 
 * @returns {undefined}
 */

function check_user()
{
    var input_search = jQuery('#search_cus_info').val();
    if (input_search == '')
    {
//        alert('Input Key Search, please!');
    } else
    {
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=order.check_user',
            method: 'post',
            data: {
                i: input_search
            },
            beforeSend: function ()
            {
//                jQuery('#loading_modal').modal('show');
            },
            success: function (dt)
            {
//                jQuery('#loading_modal').modal('hide');
                var h = '';
                d = jQuery.parseJSON(dt);

                if (d['data_empty'] != "")
                {
                    h = '<td colspan="3" style="padding: 10px;">' + d['data_empty'] + '</td>';
                } else
                {
                    if ((input_search.length == 10 || input_search.length == 11) && d['data'].length == 1) {
                        //auto get user
                        get_user(d['data'][0]['customer_id'], d['data'][0]['fullname'], d['data'][0]['telephone'],
                                d['data'][0]['email'], d['data'][0]['address'],
                                d['data'][0]['zone_id'], d['data'][0]['district_id']);
                    } else
                    {
                        jQuery('.info_users_search').css('display', 'block');
                        jQuery('.info_orther').css('display', 'none');
                        jQuery.each(d['data'], function (key, value) {
                            var variable = value['customer_id'] + ' , \'' + value['fullname'] + '\' , \'' +
                                    value['telephone'] + '\' , \'' + value['email'] + '\' , \'' + value['address'] + '\' , ' +
                                    value['zone_id'] + ' , ' + value['district_id'] + '';

                            h += '<tr>';
                            h += '<td class="center"><span class="btn_choose_user icon-checkmark" onclick="get_user(' + variable + ');"></span></td>';
                            h += '<td>' + value['fullname'] + '</td>';
                            h += '<td>' + value['address'] + '</td>';
                            h += '</tr>';
                        });
                    }
                }

                jQuery('.info_cus_body').html(h);
            }
        });
    }
}

/*
 *  --------------- GET USER
 * @param {type} id
 * @param {type} name
 * @param {type} phone
 * @param {type} email
 * @param {type} address
 * @param {type} zone_id
 * @param {type} district_id
 * @returns {undefined}
 */

function get_user(id, name, phone, email, address, zone_id, district_id)
{
    jQuery('#customer_id').val(id);
    jQuery('#cus_name').val(name);
    jQuery('#cus_phone').val(phone);
    jQuery('#cus_email').val(email);
    jQuery('#cus_address').val(address);
    jQuery('#payment_zone_id').val(zone_id);
    jQuery('#payment_district_id').val(district_id);

    jQuery('.info_users_search').css('display', 'none');
    jQuery('.info_orther').css('display', 'block');
}

/*
 * ---------- CHECK CODE
 * @returns {undefined}
 */
function check_code()
{
    var code = jQuery('#code_input').val();
    if (code == "")
    {
        jQuery('#info_code').css('display', 'block');
        jQuery('#info_code').removeClass('code_ok');
        jQuery('#info_code').addClass('code_cancel');
        jQuery('#info_code').html('<span class="res_check_code code_cancel">Mã không hợp lệ<i class="icon-cancel" style="float: right;"></i></span>');
    } else
    {
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=order.check_code',
            method: 'post',
            data: {
                code: code
            },
            beforeSend: function ()
            {
                jQuery('.icon_code').html('<img style="width: 24px;" src="components/com_openshop/assets/images/loading_1.gif" no-repeat); />');
            },
            success: function (dt)
            {
                jQuery('.icon_code').html('<span class=""></span>');      //icon loading
                var d = jQuery.parseJSON(dt)[0];
                if (d)
                {
                    jQuery('#info_code').css('display', 'block');
                    jQuery('#info_code').addClass('code_ok');
                    jQuery('#info_code').removeClass('code_cancel');
                    jQuery('#info_code').html('<span class="res_check_code code_ok">Mã hợp lệ<i class="icon-ok" style="float: right;"></i></span>');
                    jQuery('#code_id').val(d['id']);
                } else
                {
                    jQuery('#info_code').css('display', 'block');
                    jQuery('#info_code').removeClass('code_ok');
                    jQuery('#info_code').addClass('code_cancel');
                    jQuery('#info_code').html('<span class="res_check_code code_cancel">Mã không hợp lệ hoặc đã sử dụng<i class="icon-cancel" style="float: right;"></i></span>');
                    jQuery('#code_id').val('');
                }
            }
        });
    }
}

/*
 * --------------------- KEY DOWM CONTROLLER 
 * @returns {undefined}
 */

function KeyDownCtrl()
{
    var list_key;
    var key = function () {
        checkAlt = false;
        checkCtrl = false;
        jQuery(window).keydown(function (e) {
            switch (e.keyCode) {
                case 13:    //enter

                    check_user();
                    break;
                case 18: //alt
                    checkAlt = true;
                    break;
                case 17: //ctrl
                    checkCtrl = true;
                    break;
                default:
                    break;
            }
        }).keyup(function (ev) {
            if (ev.keyCode == '18') {
                checkAlt = false;
            } else if (ev.keyCode == '17') {
                checkCtrl = false;
            }
        }).keydown(function (event) {
            var keyC = event.keyCode;
            /*
             * Check Alt
             */
            if (checkAlt) {
                jQuery.each(list_key, function (index, value) {
                    if (keyC == value['shortcut_key'].toUpperCase().charCodeAt(0))
                    {
                        active_buyfrom(value['id']);
                        checkAlt = false;
                        checkCtrl = false;
                    }
                });
            }

            /*
             * Check Ctrl
             */
            if (checkCtrl) {
                if (keyC == '32') {
                    add_new_product();
                    jQuery('#product_id_' + i_temp).focus();
                    checkCtrl = false;
                    checkAlt = false;
                }
            }
        })
    }

    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.shortcut_key_buyfrom',
        method: 'post',
        success: function (dt)
        {
            list_key = jQuery.parseJSON(dt);
//            action key press
            key();
        }
    });
}

/*
 *  ------------- SEARCH PRODUCT 
 * @param {type} id
 * @returns {undefined}
 */

function search_product(id)
{
    var search_key = jQuery('#product_id_' + id).val();

    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.search_product',
        method: 'post',
        data: {
            id_product: search_key
        },
        beforeSend: function () {
            toastr['warning']('Đang tìm sản phẩm...');
        },
        success: function (dt)
        {
            toastr['success']('Success!');
            var d = jQuery.parseJSON(dt);
            var h = '';

            jQuery.each(d['productList'], function (index, value) {

                if (value['product_image'] === '' || value['product_image'] === null)
                {
                    var src_img = '/administrator/components/com_openshop/assets/images/no-image.png';
                } else
                {
                    var src_img = '../images/com_openshop/products/' + value['product_image'];
                }

                var variable = value['id'] + ',"' + value['product_sku'] + '" ,"' + value['product_name'] + '","' + src_img + '","' + value['product_price'] + '","' + value['product_price_input'] + '"';

                h += '<tr>';
                h += '    <td width="5%>' + index + '</td>';
                h += '    <td width="5%>' + value["id"] + '</td>';
                h += '    <td width="8%">' + value['product_sku'] + '</td>';
                h += '    <td width="30%">' + value['product_name'] + '</td>';
                h += '    <td><img src="' + src_img + '" width="65px" /></td>';
                h += '    <td>' + parseFloat(value["product_price"]) + '</td>';
                h += '    <td class="quantity_product_search_' + value["id"] + '" width="20%"></td>';
                h += '    <td><span class="btn-edit btn-primary-edit" onclick=\'getProduct(' + variable + ');\'>Choose</span></td>';
                h += '</tr>';

            });
            //show info product 
            jQuery('.product-searched').html(h);

            var tr_q = 1;
            var h_q = '<table class="tbl-quantity-search-product">';
            h_q += '     <tr class="tr-size">';
            h_q += '         <td></td>';
            h_q += '     </tr>';
            h_q += '     <tr class="tr-quantity-' + tr_q + '">';
            h_q += '         <td>SL</td>';
            h_q += '     </tr>';
            h_q += '</table>';

            var id_pr = '';
            var temp = 1;
            jQuery.each(d['quantityProduct'], function (index, value) {
                if (id_pr === '' || id_pr === value['product_id']) {
                    id_pr = value['product_id'];
                    if (temp == 1) {
                        jQuery('.quantity_product_search_' + value['product_id']).html(h_q);
                        temp = 0;
                    }
                    if (jQuery('#manageSizeColor').val() === '2') {
                        var h_sc = '<td>' + value['size'] + ' <br/> ' + value['color'] + '</td>';
                    } else
                    {
                        var h_sc = '<td>' + value['size'] + '</td>';
                    }
                    jQuery('.quantity_product_search_' + value['product_id'] + ' .tr-size').append(h_sc);
                    if (typeof (value['total_import_product']) === 'undefined') {
                        var qP = 0;
                    } else {
                        var qP = value['total_import_product'];
                    }
                    jQuery('.quantity_product_search_' + value['product_id'] + ' .tr-quantity-' + tr_q).append('<td>' + qP + '</td>');

                } else //show a new product
                {
                    id_pr = value['product_id'];
                    temp = 1;
                    if (temp == 1) {
                        jQuery('.quantity_product_search_' + value['product_id']).html(h_q);
                        temp = 0;
                    }
                    if (jQuery('#manageSizeColor').val() === '2') {
                        var h_sc = '<td>' + value['size'] + ' <br/> ' + value['color'] + '</td>';
                    } else
                    {
                        var h_sc = '<td>' + value['size'] + '</td>';
                    }
                    jQuery('.quantity_product_search_' + value['product_id'] + ' .tr-size').append(h_sc);
                    jQuery('.quantity_product_search_' + value['product_id'] + ' .tr-quantity-' + tr_q).append('<td>' + value['total_import_product'] + '</td>');
                }
            });
        }
    });
}

/*
 *  -------------- GET PRODUCT
 * @param {type} id
 * @param {type} p_sku
 * @param {type} name
 * @param {type} img
 * @param {type} price
 * @returns {undefined}
 */

function getProduct(id, p_sku, name, img, price, price_input)
{
    for (var i = 1; i <= i_temp; i++)
    {
        if (jQuery('#product_id_' + i).val() == id)       //exists product 
        {
            jQuery('#product_quantity_' + i).val(parseInt(jQuery('#product_quantity_' + i).val() == '' ? 0 : jQuery('#product_quantity_' + i).val()) + 1);
            //Total Price
            total_price(i);
            total_price_not_discount();
            break;
        } else if (jQuery('#product_price_' + i).val() == '')    //not exsts product
        {
            jQuery('#product_id_' + i).val(id);
            jQuery('#product_name_s' + i).html(name + '<br/><b>' + p_sku + '</b>');
            jQuery('#product_name_' + i).val(name);
            jQuery('#img_product_' + i).attr('src', img);
            jQuery('#img_product_' + i).attr('width', '65px');
            jQuery('#product_quantity_' + i).val(1);
            jQuery('#product_price_' + i).val(parseFloat(price));
            jQuery('#product_price_input_' + i).val(parseFloat(price_input));
            jQuery('#product_sku_' + i).val(p_sku);
//            get Size Color
            jQuery.ajax({
                url: 'index.php?option=com_openshop&format=ajax&task=order.getSizeColor',
                method: 'post',
                data: {
                    id_pro: id
                },
                success: function (dt)
                {
                    jQuery('#product_size_' + i).html(jQuery.parseJSON(dt)['size']);
                    if (jQuery('#manageSizeColor').val() === '2') {
                        jQuery('#product_color_' + i).html(jQuery.parseJSON(dt)['color']);
                    }
                }
            });

            jQuery('#product_id_' + i).focus();
            /*
             *  price
             */
            total_price(i);

            total_price_not_discount();

            break;
        }
    }
}

/*
 * -------------- MONEY
 */

function total_price(id)
{
    var quantity = jQuery('#product_quantity_' + id).val();
    var price = jQuery('#product_price_' + id).val();
    var t = parseInt(quantity) * parseInt(price);

    jQuery('#product_total_' + id).val(t);

    total_price_not_discount();
}

/*
 * ---------------- TOTAL PRICE NOT DISCOUNT
 */

function total_price_not_discount()
{

    var t_quantity = 0, t_total = 0, t_discount = 0;
    for (j = 1; j <= parseInt(jQuery('#max_value').val()); j++)
    {
        t_quantity += parseInt(jQuery('#product_quantity_' + j).val() == '' ? 0 : jQuery('#product_quantity_' + j).val());
        t_total += parseFloat(jQuery('#product_total_' + j).val() == '' ? 0 : jQuery('#product_total_' + j).val());
    }

    t_discount = parseInt(jQuery('#discount').val() == '' ? 0 : jQuery('#discount').val());

    jQuery('.total_quantity').text(t_quantity);
    jQuery('#total_payable').val(t_total)
    jQuery('.total_total').text(t_total);
    jQuery('.money_payable').text(t_total - t_discount);
}

/*
 * -------------------- CHANGE INFO PAYMENT
 */
function change_info_payment(v)
{
    if (v == 0)
    {
        jQuery('#change_info_shipping').removeAttr('class');
        jQuery('#change_info_customer').removeAttr('class');
        jQuery('#change_info_shipping').addClass('active_info_cus');
        jQuery('#change_info_customer').addClass('not_active_info_cus');
    } else if (v == 1)
    {
        if (jQuery('#customer_id').val() != '')
        {
            jQuery('#change_info_shipping').removeAttr('class');
            jQuery('#change_info_customer').removeAttr('class');
            jQuery('#change_info_shipping').addClass('not_active_info_cus');
            jQuery('#change_info_customer').addClass('active_info_cus');
        }
    }

    jQuery('#change_info').val(v);
}

function check_input(table, col, val, id_input)
{
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.check_input',
        method: 'post',
        data: {
            table: table,
            col: col,
            val: val
        },
        success: function (dt)
        {
            if (dt != 0)
            {
                alert('This data is exists!');
                jQuery('#' + id_input).addClass('input_error');
                jQuery('#' + id_input).focus();
            } else
            {
                jQuery('#' + id_input).removeClass('input_error');
            }
        }
    })
}

function transporter_order(id_pro, id_order)
{
    jQuery('#id_product_transporter').val(id_pro);
    jQuery('#id_order_transporter').val(id_order);
    jQuery('.modal_transporter_order').modal('show');
}

function btn_trans_order_ajax()
{
    jQuery.ajax({
        xhr: function ()
        {
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    //Do something with upload progress
                    progressAjaxForm(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: 'index.php?option=com_openshop&task=order.getFormTransAjax&format=ajax',
        method: 'post',
        data: {
            data: jQuery('#adminForm').serializeArray()
        },
        success: function (dt)
        {
//            console.log(dt);
        }
    });
}

/*
 * Show Modal APPOINTMENT
 * @param {type} row
 * @param {type} row_pro
 * @returns {undefined}
 */
function show_modal_appointment(row)
{
    jQuery('#customer_name').text(row['payment_fullname']);
    jQuery('#customer_phone').text(row['payment_telephone']);
    jQuery('#appointment_transporter').val(row['transporter_name']);
    var address = row['payment_address'] + "<br> [ <b>" + row['zone_name'] + "</b> - <b>" + row['district_name'] + "</b> ]";
    jQuery('#customer_address').html(address);

    var h = '';
    var total = '';
    var temp_total_product = 0;
    h += '<tr><td colspan="5" class="havePro">Sản phẩm có hàng</td></tr>';

    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.getOrderByPhone',
        method: 'post',
        data: {
            phone: row['payment_telephone']
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            //có sản phẩm
            var h = '';
            var totalP = 0, totalPBuy = 0;

            h += '<tr><td colspan="5" class="havePro">Sản phẩm có hàng</td></tr>';
            var totalAll = 0;
            var order_id_arr = new Array();
            var order_id_havePro = new Array();
            jQuery.each(d[1], function (i, v) {
                order_id_arr.push(v['id']);
                order_id_havePro.push(v['id']);
                totalP += 1;
                totalPBuy += 1;
                total = parseInt(v['quantity']) * parseInt(v['price']);
                totalAll += total;

                h += '<tr>';
                h += '	<td>' + (i + 1) + '</td>';
                h += '	<td>' + v['product_name'] + '</td>';
                h += '	<td align="center" width="15%"><img src="/images/com_openshop/products/' + v['image'] + '" width="90px" /></td>';
                h += '	<td align="center">' + parseInt(v['quantity']) + '<br> x ' + convert_money(parseInt(v['price'])) + '<hr> <b id="money_' + v['id'] + '">' + convert_money(total) + '</b> </td>';
                h += '	<td align="center" width="28%"><textarea rows="4" style="width:220px;">' + v['order_product_desc'] + '</textarea></td>';
                h += '</tr>';
            });

            //chưa có hàng
            h += '<tr><td colspan="5" class="havePro">Sản phẩm chưa có hàng</td></tr>';
            jQuery.each(d[0], function (i, v) {
                order_id_arr.push(v['id']);
                total = parseInt(v['quantity']) * parseInt(v['price']);
                totalP += 1;

                h += '<tr>';
                h += '	<td>' + (i + 1) + '</td>';
                h += '	<td>' + v['product_name'] + '</td>';
                h += '	<td align="center" width="15%"><img src="/images/com_openshop/products/' + v['image'] + '" width="90px" /></td>';
                h += '	<td align="center">' + parseInt(v['quantity']) + '<br> x ' + convert_money(parseInt(v['price'])) + '<hr> <b id="money_' + v['id'] + '">' + convert_money(total) + '</b> </td>';
                h += '	<td align="center" width="28%"><textarea rows="4" style="width:220px;">' + v['order_product_desc'] + '</textarea></td>';
                h += '</tr>';
            });


            h += '<tr style="font-size: 25px">';
            h += '	<td colspan="3">Tổng (Chỉ tính các sản phẩm có hàng)</td>';
            h += '	<td colspan="2" id="total_money"><b>' + convert_money(parseInt(totalAll)) + ' <sup>đ</sup></b></td>';
            h += '</tr>';

            jQuery('.total_paid').text(convert_money(parseInt(totalAll)));
            jQuery('.total_money').text(convert_money(parseInt(totalAll)));

            //tổng sản phẩm
            jQuery('.total_buy_product').html('Có hàng: ' + totalPBuy + ' - Tổng số lượng: ' + totalP);
            jQuery('.show-pro-appointment').html(h);

            //order id
            jQuery('#order_id').val(order_id_havePro);
        }
    })

    jQuery('.btn-more-product').attr('onclick', 'derict_more_product("' + row["link_more_product"] + '")');
    //RESRT STATUS APPOINTMENT
    jQuery('#status_appointment').val(0);
    jQuery('.status-none').addClass('active');
    jQuery('.status-office').removeClass('active');
    jQuery('.status-shipping').removeClass('active');
    jQuery('.status-sales-shop').removeClass('active');

    convert_money(parseInt(row['total']));

    //Appointment
    if (row['appointment_type'] != null)
    {
        var h_a = '';
        jQuery('.transporter_hideshow').css('display', 'none');
        switch (row['appointment_type']) {
            case '1':   //office
                h_a += '<button type="button" class="btn btn-default status-office order-appointment active">' + row['message_appointment_status'] + '</button>';
                break;
            case '2':
                h_a += '<button type="button" class="btn btn-default status-shipping order-appointment active">' + row['message_appointment_status'] + '</button>';
                jQuery('.transporter_hideshow').css('display', 'block');
                break;
            default:
                break;
        }

        jQuery('.appointment_check').html(h_a);
        jQuery('#status_appointment').val(row['appointment_type']);
    }
    //SHOW MODAL
    jQuery('#modal-appointment').modal('show');
}


/*
 * Show Modal APPOINTMENT CART
 * @param {type} row
 * @param {type} row_pro
 * @returns {undefined}
 */
function show_modal_appointment_cart(row)
{
    jQuery('#customer_name').text(row['payment_fullname']);
    jQuery('#customer_phone').text(row['payment_telephone']);
    jQuery('#appointment_transporter').val(row['transporter_name']);
    var address = row['payment_address'] + "<br> [ <b>" + row['zone_name'] + "</b> - <b>" + row['district_name'] + "</b> ]";
    jQuery('#customer_address').html(address);
    var h = '';
    var total = '';
    var temp_total_product = 0;
    h += '<tr><td colspan="5" class="havePro">Sản phẩm đang đặt hàng</td></tr>';

    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.getAppointmentByPhone',
        method: 'post',
        data: {
            phone: row['payment_telephone']
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            //có sản phẩm
            var h = '';
            var totalP = 0, totalPBuy = 0;

            h += '<tr><td colspan="5" class="havePro">Sản phẩm có hàng</td></tr>';
            var totalAll = 0;
            var order_id_arr = new Array();
            var order_id_havePro = new Array();
            jQuery.each(d[1], function (i, v) {
                order_id_arr.push(v['id']);
                order_id_havePro.push(v['id']);
                totalP += 1;
                totalPBuy += 1;
                total = parseInt(v['quantity']) * parseInt(v['price']);
                totalAll += total;

                h += '<tr>';
                h += '	<td>' + (i + 1) + '</td>';
                h += '	<td>' + v['product_name'] + '</td>';
                h += '	<td align="center" width="15%"><img src="/images/com_openshop/products/' + v['image'] + '" width="90px" /></td>';
                h += '	<td align="center">' + parseInt(v['quantity']) + '<br> x ' + convert_money(parseInt(v['price'])) + '<hr> <b id="money_' + v['id'] + '">' + convert_money(total) + '</b> </td>';
                h += '	<td align="center" width="28%"><textarea rows="4" style="width:220px;">' + v['order_product_desc'] + '</textarea></td>';
                h += '</tr>';
            });

            h += '<tr style="font-size: 25px">';
            h += '	<td colspan="3">Tổng (Chỉ tính các sản phẩm có hàng)</td>';
            h += '	<td colspan="2" id="total_money"><b>' + convert_money(parseInt(totalAll)) + ' <sup>đ</sup></b></td>';
            h += '</tr>';

            jQuery('.total_paid').text(convert_money(parseInt(totalAll)));
            jQuery('.total_money').text(convert_money(parseInt(totalAll)));

            //tổng sản phẩm
            jQuery('.total_buy_product').html('Có hàng: ' + totalPBuy + ' - Tổng số lượng: ' + totalP);
            jQuery('.show-pro-appointment').html(h);

            //order id
            jQuery('#order_id').val(order_id_havePro);
        }
    })

    jQuery('.btn-more-product').attr('onclick', 'derict_more_product("' + row["link_more_product"] + '")');
    //RESRT STATUS APPOINTMENT
    jQuery('#status_appointment').val(0);
    jQuery('.status-none').addClass('active');
    jQuery('.status-office').removeClass('active');
    jQuery('.status-shipping').removeClass('active');
    jQuery('.status-sales-shop').removeClass('active');

    convert_money(parseInt(row['total']));

    //Appointment
    if (row['appointment_type'] != null)
    {
        var h_a = '';
        jQuery('.transporter_hideshow').css('display', 'none');
        switch (row['appointment_type']) {
            case '1':   //office
                h_a += '<button type="button" class="btn btn-default status-office order-appointment active">' + row['message_appointment_status'] + '</button>';
                break;
            case '2':
                h_a += '<button type="button" class="btn btn-default status-shipping order-appointment active">' + row['message_appointment_status'] + '</button>';
                jQuery('.transporter_hideshow').css('display', 'block');
                break;
            default:
                break;
        }

        jQuery('.appointment_check').html(h_a);
        jQuery('#status_appointment').val(row['appointment_type']);
    }


    //SHOW MODAL
    jQuery('#modal-appointment').modal('show');
}

function derict_more_product(url)
{
    window.location.replace(url);
}

function order_ok()
{
    if (jQuery('#status_appointment').val() == '0')
    {
        toastr['warning']('Chưa chọn trạng thái hẹn!');
    } else
    {
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=order.order_ok',
            method: 'post',
            data: {
                view: jQuery('#view').val(),
                stauts_appointment: jQuery('#status_appointment').val(),
                order_id: jQuery('#order_id').val(),
                appointment_description: jQuery('#appointment_description').val(),
                total: convert_money_num(jQuery('.total_money').text()),
                sale: convert_money_num(jQuery('.sale_money').text())
            },
            success: function (dt) {
                var d = jQuery.parseJSON(dt);
                toastr[d['status']](d['message']);
                jQuery('#modal-appointment').modal('hide');
                window.location.href = jQuery(location).attr('href');
            }
        });
    }
}

function order_appointment_ok()
{

}

/*
 *  0   : NONE
 *  1   : OFFICE
 *  2   : SHIPING
 *  3   : SALES SHOP
 */
function getStatusAppointmentAjax(status) {
    var active_curr = jQuery('#status_appointment').val();
    var id_hide = '';
    var id_show = '';
    var test = 1;
    switch (active_curr) {
        case '0':
            id_hide = 'status-none';
            break;
        case '1':
            id_hide = 'status-office';
            break;
        case '2':
            id_hide = 'status-shipping';
            jQuery('.transporter_hideshow').css('display', 'none');
            break;
        case '3':
            id_hide = 'status-sales-shop';
            jQuery('.btn-appointment').html('<i class="icon-apply"></i> Hẹn');
            break;
        default:
            toastr['warning']('Error!');
            test = 0;
            break;
    }

    switch (status) {
        case '0':
            id_show = 'status-none';
            break;
        case '1':
            id_show = 'status-office';
            break;
        case '2':
            id_show = 'status-shipping';
            jQuery('.transporter_hideshow').css('display', 'block');
            break;
        case '3':
            id_show = 'status-sales-shop';
            jQuery('.btn-appointment').html('<i class="icon-apply"></i> Thanh toán');
            break;
        default:
            toastr['warning']('Error!');
            test = 0;
            break;
    }

    if (test == 1)
    {
        jQuery('.' + id_hide).removeClass('active');
        jQuery('.' + id_show).addClass('active');
        jQuery('#status_appointment').val(status);
    }
}



function order_delete_product(order_id_product, order_id)
{
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.order_delete_product',
        method: 'post',
        data: {
            order_id_product: order_id_product,
            order_id: order_id
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            if (d['status'] == 'success')
            {
                toastr[d['status']](d['mesage']);
                jQuery('#appointment_' + order_id_product).remove();
                jQuery('#total_money').html('<b>' + convert_money(d['total_money']) + '</b>');
                jQuery('.check_delete_order').attr('onclick', 'check_delete_order()');
                jQuery('.total_paid').text(convert_money(d['total_money']));
                jQuery('.total_money').text(convert_money(d['total_money']));
                jQuery('.total_buy_product').text(parseInt(jQuery('.total_buy_product').text()) - 1);
            }
        }
    });
}

function check_delete_order()
{
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.check_delete_order',
        method: 'post',
        data: {
            order_id: jQuery('#order_id').val()
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            toastr[d['status']](d['mesage']);
            window.location.href = "index.php?option=com_openshop&view=" + jQuery('#view').val();
        }
    });
}

function show_delete_order(order_id, act)
{
    jQuery('.delete_order').attr('onclick', 'delete_order(' + order_id + ',"' + act + '")');
    jQuery('.reason_delete_order').attr('name', 'reason_delete_order_' + order_id + '');
    jQuery('.reason_delete_order').attr('id', 'reason_delete_order_' + order_id + '');
    jQuery('.reason_delete_order_desc').attr('name', 'reason_delete_order_desc_' + order_id + '');
    jQuery('.reason_delete_order_desc').attr('id', 'reason_delete_order_desc_' + order_id + '');
    jQuery('.reason_delete_order').val(0);
    jQuery('#modal-reason-delete-order').modal('show');
}

function delete_order(order_id, act)
{
    if (jQuery('#reason_delete_order_' + order_id).val() == 0)
    {
        toastr['warning']('Chưa chọn lý do xóa');
    } else if (jQuery('#deletePId').val() == '') {
        toastr['warning']('Chưa chọn sản phẩm xóa');
    } else
    {
        jQuery('.delete_order').attr('disabled', 'disable');
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=order.delete_order',
            method: 'post',
            data: {
                order_id: order_id,
                reason: jQuery('#reason_delete_order_' + order_id).val(),
                desc: jQuery('#reason_delete_order_desc_' + order_id).val(),
                act: act
            },
            success: function (dt) {
                jQuery('.delete_order').removeAttr('disable');
                var d = jQuery.parseJSON(dt);
                toastr[d['status']](d['message']);
                window.location.href = "index.php?option=com_openshop&view=" + jQuery('#view').val();
            }
        });
    }
}

function show_search_tools()
{
    jQuery('.search-tools').toggle(400);
}


function order_appointment_ok()
{

}

function validator_custom()
{
    var res = 1;
    if (typeof (jQuery('#product_id_1').val()) === 'undefined') {
        toastr['warning']('Chưa chọn sản phẩm!');
        add_new_product();
        res = 0;
    }

    return res;
}

function updateDescOrder(val, id) {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.updateDescOrder',
        method: 'post',
        data: {
            desc: val,
            id_orderpro: id
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            toastr[d['status']](d['message']);
            jQuery('.desc_' + id).css('background', '#5CB85C');
        }
    });
}

function quantityPriority(quantity, id) {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.quantityPriority',
        method: 'post',
        data: {
            quan: quantity,
            id_orderpro: id
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            toastr[d['status']](d['message']);
            jQuery('.qp_' + id).attr('onclick', '');
            jQuery('.qp_' + id).text('Đã ưu tiên');
            jQuery('.qp_' + id).removeClass('quantity-priority');
            jQuery('.qp_' + id).addClass('quantity-priorited');
            window.location.reload('index.php?option=com_openshop&view=orders');
        }
    });
}

function reversalOrder(id) {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=order.reversalOrder',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            toastr[d['status']](d['message']);
            if (d['status'] === "success") {
                window.location.href = d['link'];
            }
        }
    });
}

function chooseProDelete(id) {
    //Effect
    jQuery('.chooseP_' + id).addClass('activePId');
    jQuery('.chooseP_' + id).attr('onclick', 'notChooseProDelete(' + id + ')');
    toastr['warning']('Đã chọn sản phẩm: ' + jQuery('.chooseP_' + id).text());

    //get id
    var ids = new Array();
    if (jQuery('#deletePId').val() != '') {
        ids.push(jQuery('#deletePId').val());
    }
    ids.push(id);
    jQuery('#deletePId').val(ids);
}

function notChooseProDelete(id) {
    jQuery('.chooseP_' + id).removeClass('activePId');
    jQuery('.chooseP_' + id).attr('onclick', 'chooseProDelete(' + id + ')');
    toastr['success']('Đã bỏ chọn sản phẩm: ' + jQuery('.chooseP_' + id).text());

    var arr = jQuery('#deletePId').val().split(',');
    var k = jQuery.inArray(id.toString(), arr);
    arr.splice(k, 1);
    jQuery('#deletePId').val(arr);
}


jQuery(document).ready(function () {

    jQuery('#appointment_sale').on('keyup', function () {
        if (jQuery('#appointment_sale').val() === '') {
            jQuery('#appointment_sale').val(0);
        }
        var count_pro = jQuery('#order_id').val().split(',').length;
        jQuery('.sale_money').text(convert_money(jQuery('#appointment_sale').val() * count_pro));
        var t = convert_money_num(jQuery('.total_money').text()) - convert_money_num(jQuery('.sale_money').text());
        jQuery('.total_paid').text(convert_money(t));
    });

    jQuery('.hidden_user_found').on('click', function () {
        jQuery('.info_users_search').css('display', 'none');
        jQuery('.info_orther').css('display', 'block');
    });


});





