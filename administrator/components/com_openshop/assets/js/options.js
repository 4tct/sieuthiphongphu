$ = jQuery.noConflict();

function delete_image_option(i,id,img)
{
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=functions.delete_image_option_color',
        method: 'post',
        data: {
            id  : id,
            img : img
        },
        success:function(dt){
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            if(d['status'] === 'success'){
                $('.show_image_color_' + i).html('<input type="file" name="img_color_' + i + '" />');
            }
        }
    });
    
}