$ = jQuery.noConflict();
Joomla.submitbutton = function (task) {
    if (task == 'add_io')
    {
        jQuery('#slickbox').toggle(400);
    }
};
jQuery.fn.upload = function (remote, data, successFn, progressFn) {// upload images
    // if we dont have post data, move it along
    if (typeof data != "object") {
        progressFn = successFn;
        successFn = data;
    }

    var formData = new FormData();
    var numFiles = 0;
    this.each(function () {
        var i, length = this.files.length;
        numFiles += length;
        for (i = 0; i < length; i++) {
            formData.append(this.name, this.files[i]);
        }
    });
    // if we have post data too
    if (typeof data == "object") {
        for (var i in data) {
            formData.append(i, data[i]);
        }
    }

    var def = new jQuery.Deferred();
    if (numFiles > 0) {
        // do the ajax request
        jQuery.ajax({
            url: remote,
            type: "POST",
            xhr: function () {
                myXhr = jQuery.ajaxSettings.xhr();
                if (myXhr.upload && progressFn) {
                    myXhr.upload.addEventListener("progress", function (prog) {
                        var value = ~~((prog.loaded / prog.total) * 100);
                        // if we passed a progress function
                        if (typeof progressFn === "function") {
                            progressFn(prog, value);
                            // if we passed a progress element
                        } else if (progressFn) {
                            jQuery(progressFn).val(value);
                        }
                    }, false);
                }
                return myXhr;
            },
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            complete: function (res) {
                var json;
                try {
                    json = JSON.parse(res.responseText);
                } catch (e) {
                    json = res.responseText;
                }
                if (typeof successFn === "function")
                    successFn(json);
                def.resolve(json);
            }
        });
    } else {
        def.reject();
    }

    return def.promise();
};
Joomla.submitbutton = function (task) {
    switch (task) {
        case 'show_io':
            jQuery('#slickbox').toggle(400);
            break;
        case 'save_io':
            btn_save_ios_ajax();
            break;
        case 'reset_io':
            reset_form();
            break;
        default:
            break;
    }
};
function check_idproduct() {

    if (jQuery('#id_product').val() != "")
    {
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=ios.check_idproduct',
            method: 'post',
            data: {
                id: jQuery('#id_product').val()
            },
            success: function (dt)
            {
                jQuery('.arrow-up').css('display', 'block');
                var h = '';
                jQuery('#load tr').remove();
                var jsonobj = jQuery.parseJSON(dt);
                if (jsonobj == undefined || jsonobj == null || jsonobj.length == 0) {
                    jQuery('#warning_product').text('');
                    jQuery('#warning_product').append('Sản phẩm mới');
                    toastr['warning']('Sản phẩm mới');
                    jQuery('#productnew').val(0);
                    jQuery('#product_image').attr('type', 'file');
                } else {
                    jQuery('#product_image').attr('type', 'hidden');
                    jQuery('#productnew').val(1);
                    jQuery('#warning_product').text('');
                    var url = jQuery('#ul_img').val();
                    jQuery.each(jsonobj, function (index, value) {
                        var urls = '';
                        if (value['product_image'] == "" || value['product_image'] == null) {
                            urls = url + "no-image_1.png";
                        } else {
                            urls = url + value['product_image'];
                        }

                        h += '<tr>';
                        h += '  <td>';
                        h += '      <span onclick="load_product(\'' + value['product_sku'] + '\')" id="' + value['product_sku'] + '" >' + value['product_sku'] + '</span>';
                        h += '      <span onclick="load_product(\'' + value['product_sku'] + '\')">' + '<img height="100" width="100" src="' + urls + '"></span>';
                        h += '  </td>';
                        h += '</tr>';

                        jQuery('#load').append('<tr><td><span onclick="load_product(\'' + value['product_sku'] + '\')" id="' + value['product_sku'] + '" >' + value['product_sku'] +
                                '</span></td><td><span onclick="load_product(\'' + value['product_sku'] + '\')">' +
                                '<img height="100" width="100" src="' + urls + '"></span> </td></tr>');

//                        jQuery('#load').append(h);
                    });
                    if (jQuery('#invoice_sku').val() == "")
                    {
                        //jQuery('#invoice_sku').val(jQuery.format.date(new Date(), 'dd M yy'));
                        jQuery('#invoice_sku').attr('placeholder', 'dd-mm-YYYY');
                    }
                }


            }
        });
    } else if (jQuery('#id_product').val() == "") {
        jQuery('#adminForm').find('input:text').val('');
        jQuery('#warning_product').text('');
        var d = new Date();
        var day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
        var mon = (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
        var ngay = day + '-' + mon + '-' + d.getFullYear();
        jQuery('#invoice_sku').val(ngay);
        jQuery('#table_price tr').remove();
        jQuery('#namemanufac').removeAttr('disabled');
        jQuery('#namemanufac').val(0);
        jQuery('#statusios').val(0);
    }
    jQuery('#load tr').remove();
}

function load_product(id)
{
    id_pro = id;
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=ios.load_product',
        method: 'post',
        data: {
            id: id_pro
        },
        beforeSend: function ()
        {
        },
        success: function (dt)
        {
            jQuery('.arrow-up').css('display', 'none');
            jQuery('#load tr').remove();
            var jsonobj = jQuery.parseJSON(dt);
//            console.log(jsonobj);
            if (Object.getOwnPropertyNames(jsonobj).length == 1)
            {
                //code
            } else {
                jQuery.each(jsonobj, function (index, value) {
                    jQuery('input[name=id_product]').val('');
                    jQuery('input[name=id_product]').val(value['MASP']);
                    jQuery('#price').val(value['price_input']);
                    jQuery('input[name=idproductmanufac]').val(value['MASP_NCC']);
                    jQuery('input[name=idinvoice]').val(value['MAHD']);
                    jQuery('input[name=nameproduct]').val(value['product_name']);
                    jQuery('input[name=namemanufac]').val(value['TEN_NCC']);
                    jQuery('img[name=image]').attr('height', '70px');
                    jQuery('img[name=image]').attr('widht', '50px');
                    jQuery('input[name=idpro_auto]').val(value['id']);
                    jQuery('#namemanufac').attr('disabled', 'disabled');
//                    
                    jQuery('#namemanufac option')
                            .filter('[value=' + value['MANCC'] + ']')
                            .attr('selected', true);
                    jQuery('input[name=idmanufac]').val(value['MANCC']);
//                    jQuery('#namemanufac').selectedIndex = value['MANCC'];
                    jQuery('#table_price tr').remove();
                    jQuery('#table_price tr').remove();
                    load_price_input_ios(value['id']);
                });
            }
        }
    });
}

jQuery(document).ready(function () {
    jQuery('#loadimages').html('');
    jQuery('#loadimages').html('');
    jQuery('#slickbox').hide();
    jQuery('#slick-show').click(function () {
        jQuery('#slickbox').show('slow');
        return false;
    });
    jQuery('#slick-hide').click(function () {
        jQuery('#slickbox').hide('fast');
        return false;
    });
    jQuery('#slick-toggle').click(function () {
        jQuery('#slickbox').toggle(400);
        return false;
    });
    jQuery('#delete').click(function () {
        jQuery(this).remove();
    });
    jQuery('#statusios').change(function () {

        if (jQuery('#statusios').val() == 2) {
//            jQuery('#output').attr('style', 'visibility:visible');
//            jQuery('#input').attr('style', 'visibility:hidden');
//jQuery('#input th').html('');
            jQuery('#output').empty();
            jQuery('#output_text').append('Chọn kho');
            jQuery.ajax({// hiển thi select output from
                url: 'index.php?option=com_openshop&format=ajax&task=ios.select_input',
                method: 'post',
                success: function (dt)
                {
                    //console.log(jQuery.parseJSON(dt));
                    jsonobj = jQuery.parseJSON(dt);
                    var h = '';
                    h += '<select id="id_warehouse_from" name="id_warehouse_from" class="form-control inputbox" style="width:205px">';
                    h += '<option selected="selected" value="0">Chọn kho xuất</option>';
                    jQuery.each(jsonobj, function (index, value)
                    {
                        h += '<option value="' + value["id"] + '">';
                        h += '       ' + value["warehouse_name"];
                        h += '</option>';
                    });
                    h += '</select>';
                    h += '<select id="id_warehouse_to" name="id_warehouse_to" class="form-control inputbox" style="width:205px">';
                    h += '<option selected="selected" value="0">Chọn kho nhập</option>';
                    jQuery.each(jsonobj, function (index, value)
                    {
                        h += '<option value="' + value["id"] + '">';
                        h += '       ' + value["warehouse_name"];
                        h += '</option>';
                    });
                    h += '</select>';
                    jQuery('#output').append(h);
                }
            }); // end load select output
            jQuery('#input').empty();
            jQuery('#input_text').empty();
        }
        if (jQuery('#statusios').val() == 1 || jQuery('#statusios').val() == 3 || jQuery('#statusios').val() == 4) {
//            jQuery('#output').attr('style', 'visibility:hidden');
//            jQuery('#input').attr('style', 'visibility:visible');
            jQuery('#input_text').empty();
            jQuery('#input_text').append('Chọn kho');
            jQuery('#input').empty();
            jQuery.ajax({// hiển thi select input
                url: 'index.php?option=com_openshop&format=ajax&task=ios.select_input',
                method: 'post',
                success: function (dt)
                {
                    //console.log(jQuery.parseJSON(dt));
                    jsonobj = jQuery.parseJSON(dt);
                    var h = '';
                    h += '<select id="id_warehouse" name="id_warehouse" class="form-control inputbox" style="width:205px">';
                    h += '<option selected="selected" value="0">Chọn kho</option>';
                    jQuery.each(jsonobj, function (index, value)
                    {
                        h += '<option value="' + value["id"] + '">';
                        h += '       ' + value["warehouse_name"];
                        h += '</option>';
                    });
                    h += '</select>';
                    jQuery('#input').append(h);
                }
            }); // end load select
            jQuery('#output').empty();
            jQuery('#output_text').empty();
            jQuery('#price').attr('disabled', false);
        }
        if (jQuery('#statusios').val() == 1) {
            jQuery('#loadimages').append('<input name="myfile" id="myfile" type="file" /><br />\n\
                                    <progress id="prog" value="0" max="100" min="0"></progress>');
        } else {
            jQuery('#loadimages').html('');
            jQuery('#loadimages').html('');
        }

    });
});
function load_product_output(id)
{
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=ios.load_product_output',
        method: 'post',
        data: {
            id: id,
            id_warehouse_from: jQuery('#id_warehouse_from').val()
        },
        success: function (dt)
        {
            //console.log(jQuery.parseJSON(dt));
            jsonobj = jQuery.parseJSON(dt);
            var h = '';
            var dem = 0;
            if (jsonobj == "")
            {
                toastr['warning']('Sản phầm đã hết hàng');
            } else {
                jQuery.each(jsonobj, function (index, value)
                {
                    dem++;
                    h += '<tr>';
                    h += '   <td>';
                    h += '       ' + value["warehouse_name"];
                    h += '   </td>';
                    h += '   <td>';
                    h += '      <b>' + value["color"] + '_' + value["size"] + '</b>';
                    h += '   </td>';
                    h += '   <td>';
                    h += '      ' + value["soluong"];
                    h += '   </td>';
                    h += '    <td>';
                    h += '      <input type="text" id="sl_' + index + '" name="sl_' + index + '" value="0" />';
                    h += '      <input type="hidden" id="outputs_' + index + '" name="outputs_' + index + '" value="' + value["color"] + '_' + value["size"] + '_' + value["soluong"] + '_' + value["colorid"] + '_' + value["sizeid"] + '_' + value["NGAY"] + '_' + value["id"] + '" />';
                    h += '      <input disabled="disabled" type="text" id="ngay_' + index + '" name="ngay_' + index + '" value="' + value["NGAY"] + '" />';
                    h += '    </td>';
                    h += '    <td>';
                    h += '       <textarea rows="3" id="descript_' + index + '" name="descript_' + index + '"></textarea>';
                    h += '    </td>';
                    h += '</tr>';
                });
                h += '<input type="hidden" id="sodongmax" name="sodongmax" value="' + dem + '" />';
                jQuery('#attr').append(h);
            }
        }
    });
}
function reset_form() {
    jQuery('#adminForm').find('input:text').val('');
    jQuery('#attr tr').remove();
    jQuery('#table_price tr').remove();
    jQuery('#sodongmax').remove();
    jQuery('#numrow').val('');
    jQuery('#idpro_auto').val('');
    jQuery('.id_100 option')
            .filter('[value=0]')
            .attr('selected', true);
    if (jQuery('#invoice_sku').val() == "")
    {
        //jQuery('#invoice_sku').val(jQuery.format.date(new Date(), 'dd M yy'));
        jQuery('#invoice_sku').attr('placeholder', 'yyyy-mm-dd');
    }
    jQuery('#adminForm')[0].reset();
    jQuery('#namemanufac').selectedIndex = '0';
}

function add_row(d) { // hiển thị table thông tin sản phẩm (bảng phía tay phải)
    var id = jQuery('input[name=idpro_auto]').val();
    var check_product = jQuery('#productnew').val();
    var masp = jQuery('#id_product').val();
    var io = jQuery('#statusios').val();
    var pattern = /^[0-9]jQuery/;
    if (check_product == 0 && io == 1)                // truong hop 1.a them san pham moi, xuat ra all size&color, //nhap kho
    {
        if (jQuery('#invoice_sku').val() == "") {
            toastr['error']('Vui lòng nhập mã hóa đơn');
        } else if (jQuery('#id_product').val() == "") {
            toastr['error']('Vui lòng nhập mã sản phẩm');
        } else if (jQuery('#lenofday').val() == 0 || jQuery('#lenofday').val() == "") {
            toastr['error']('Vui lòng chọn mã buổi nhập hóa đơn');
        } else if (jQuery('#id_warehouse').val() == 0)
        {
            toastr['error']('Vui lòng chọn kho nhập');
        } else if (jQuery('#idproductmanufac').val() == "")
        {
            toastr['error']('Vui lòng nhập mã sản phẩm của nhà cung cấp');
        } else if (jQuery('#nameproduct').val() == "") {
            toastr['error']('Vui lòng nhập tên sản phẩm');
        } else if (jQuery('#namemanufac').val() == 0)
        {
            toastr['error']('Vui lòng chọn nhà cung cấp sản phẩm');
        } else if (jQuery('#price').val() == "") {
            toastr['error']('Vui lòng nhập giá sản phẩm');
        } else if (pattern.test(jQuery('#price').val()))
        {
            toastr['error']('Vui lòng nhập giá sản phẩm là số');
        } else if (jQuery('#statusios').val() == 1 && check_product == 0)
                // Gui du lieu them moi vao bang san pham & nhap xuat
                {
                    jQuery.ajax({
                        url: 'index.php?option=com_openshop&format=ajax&task=ios.add_row_new',
                        method: 'post',
                        data: {
                            id: id
                        },
                        success: function (dt)
                        {
                            //console.log(jQuery.parseJSON(dt));
                            var jsonobj = jQuery.parseJSON(dt);
                            add_tr(jsonobj);
                        }
                    });
                }
    } else if (id == "" && check_product == 1 && (io == 1 || io == 3 || io == 4)) {// truong hop 1.b san pham da co, kiem tra size&color xuat ra, //nhap kho
        toastr['error']('Chưa chọn sản phẩm');
    } else if (id != "" && check_product == 1 && (io == 1)) {/*===---kiem tra id product---===*/
        //// select du lieu nhập kho
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=ios.add_row',
            method: 'post',
            data: {
                id: id,
                status: io
            },
            success: function (dt)
            {
                var jsonobj = jQuery.parseJSON(dt);
                add_tr_old(jsonobj);
            }
        });
    } else if (id != "" && check_product == 1 && (io == 3 || io == 4)) {
        // select du lieu tra nha cung cap or thu hồi
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=ios.add_row_TH_TNCC',
            method: 'post',
            data: {
                id: id,
                id_warehouse: jQuery('#id_warehouse').val(),
                status: io
            },
            success: function (dt)
            {
                //console.log(jQuery.parseJSON(dt));
                var jsonobj = jQuery.parseJSON(dt);
                var kiemtra = check_integrity_constraints();
                if (kiemtra == 0) {
                    var h = '';
                    var dem = 0;
                    if (jsonobj == "")
                    {
                        toastr['warning']('Sản phầm đã hết trong kho');
                    } else {
                        jQuery.each(jsonobj, function (index, value)
                        {
                            dem++;
                            h += '<tr>';
                            h += '   <td>';
                            h += '       ' + value["warehouse_name"];
                            h += '   </td>';
                            h += '   <td>';
                            h += '      <b>' + value["color"] + '_' + value["size"] + '</b>';
                            h += '   </td>';
                            h += '   <td>';
                            h += '      ' + value["soluong"];
                            h += '   </td>';
                            h += '    <td>';
                            h += '      <input type="text" id="sl_' + index + '" name="sl_' + index + '" value="0" />';
                            h += '      <input type="hidden" id="outputs_' + index + '" name="outputs_' + index + '" value="' + value["color"] + '_' + value["size"] + '_' + value["soluong"] + '_' + value["idcolor"] + '_' + value["idsize"] + '_' + id + '" />';
                            h += '    </td>';
                            h += '    <td>';
                            h += '       <textarea rows="3" id="descript_' + index + '" name="descript_' + index + '"></textarea>';
                            h += '    </td>';
                            h += '</tr>';
                        });
                        h += '<input type="hidden" id="sodongmax" name="sodongmax" value="' + dem + '" />';
                        jQuery('#attr').append(h);
                    }
                }

            }
        });
    } else if (io != 1)
    {
        if (id == "")
        {
            toastr['error']('Vui lòng nhập mã sản phẩm');
        } else if (jQuery('#id_warehouse_from').val() == 0) {
            toastr['error']('Vui lòng chọn kho xuất sản phẩm');
        } else if (jQuery('#id_warehouse_to').val() == 0) {
            toastr['error']('Vui lòng chọn kho nhập sản phẩm');
        } else if (masp == "")
        {
            toastr['error']('Vui lòng nhập mã sản phẩm');
        } else {
            var kiemtra = check_integrity_constraints();
            if (kiemtra == 0)
            {
                if (jQuery('#sodongmax').val() > 0)
                {
                    toastr['error']('Đã tải hết sản phẩm trong kho');
                } else {
                    load_product_output(id);
                }
            }

        }
    }
}

function delete_row(id)
{
    //alert(id);
    jQuery('#addtr_' + id).remove();
    var numrow = parseInt(jQuery('#numrow').val()) - 1;
    jQuery('#numrow').val(numrow);
    for (i = id; i <= numrow; i++)
    {
        jQuery('#addtr_' + (i + 1)).attr('id', 'addtr_' + i);
        jQuery('#del_' + (i + 1)).attr('id', 'del_' + i);
        jQuery('#del_' + (i)).attr('onclick', 'delete_row(' + i + ')');
        jQuery('#' + (i + 1)).attr('id', i);
        jQuery('#' + (i + 1)).attr('name', i);
        jQuery('#sl_' + (i + 1)).attr('name', 'sl_' + i);
        jQuery('#sl_' + (i + 1)).attr('id', 'sl_' + i);
        jQuery('#slcolor_' + (i + 1)).attr('name', 'slcolor_' + i);
        jQuery('#slcolor_' + (i + 1)).attr('id', 'slcolor_' + i);
        jQuery('#slsize_' + (i + 1)).attr('name', 'slsize_' + i);
        jQuery('#slsize_' + (i + 1)).attr('id', 'slsize_' + i);
        jQuery('#slcolor_' + (i)).attr('onchange', 'setvalue(' + i + ')');
        jQuery('#slsize_' + (i)).attr('onchange', 'setvalue(' + i + ')');
    }
    for (i = id; i <= numrow; i++)
    {
        jQuery('#' + (i)).attr('name', jQuery('#slcolor_' + (i + 1)).val() + '_' + jQuery('#slsize_' + (i + 1)).val() + '_' + i);
    }


}



function add_tr_old(jsonobj) {
//    var numrow = jQuery('#numrow').val();
    var numrow = 1;
    var h = '';
    if (jQuery('#numrow').val() > 0) {
        toastr['warning']('Đã load tất cả mẫu của sản phẩm');
    } else {
        jQuery.each(jsonobj, function (index, value)
        {
            h += '<tr id="addtr_' + numrow + '">';
            h += '   <td>';
            h += '       <select onchange="setvalue(' + numrow + ')" name="slcolor_' + numrow + '" id="slcolor_' + numrow + '">';
            h += '<option value=' + value['id_optioncolor'] + '>';
            h += value['color'];
            h += '</option>';
            h += '       </select>';
            h += '<input type="hidden" name="option_id_color" id="option_id_color" value="1" />';
            h += '   </td>';
            h += '   <td>';
            h += '       <select onchange="setvalue(' + numrow + ')" name="slsize_' + numrow + '" id="slsize_' + numrow + '">';
            h += '<option value=' + value['id_optionsize'] + '>';
            h += value['size'];
            h += '</option>';
            h += '       </select>';
            h += '   </td>';
            h += '<input type="hidden" name="option_id_size" id="option_id_size" value="2" />';
            h += '    <td>';
            h += '        <input onmouseout="setvalue(' + numrow + ')" id="sl_' + numrow + '" name="sl_' + numrow + '" value="0" type="text"  /><input value="" type="text" id="' + numrow + '" name="' + numrow + '"  style="display:none;"/>';
            h += '    </td>';
            h += '</tr>';
            numrow++;
        });
        jQuery('#numrow').val(numrow - 1);
        jQuery('#attr').append(h);
    }



}



function add_tr(jsonobj) {
    var numrow = jQuery('#numrow').val();
    numrow++;
    var h = '';
    h += '<tr id="addtr_' + numrow + '">';
    h += '   <td>';
    h += '       <select onchange="setvalue(' + numrow + ')" name="slcolor_' + numrow + '" id="slcolor_' + numrow + '">';
    jQuery.each(jsonobj, function (index, value)
    {
        if (value['option_id'] == '1') {
            h += '<option value=' + value['option_value_id'] + '>';
            h += value['value'];
            h += '</option>';
        }
    });
    h += '       </select>';
    h += '<input type="hidden" name="option_id_color" id="option_id_color" value="1" />';
    h += '   </td>';
    h += '   <td>';
    h += '       <select onchange="setvalue(' + numrow + ')" name="slsize_' + numrow + '" id="slsize_' + numrow + '">';
    jQuery.each(jsonobj, function (index, value) {
        if (value['option_id'] == '2') {
            h += '<option value=' + value['option_value_id'] + '>';
            h += value['value'];
            h += '</option>';
        }
    });
    h += '       </select>';
    h += '   </td>';
    h += '<input type="hidden" name="option_id_size" id="option_id_size" value="2" />';
    h += '    <td>';
    h += '        <input onmouseout="setvalue(' + numrow + ')" id="sl_' + numrow + '" name="sl_' + numrow + '" value="0" type="text"  /><input value="" type="text" id="' + numrow + '" name="' + numrow + '"  style="display:none;"/>';
    h += '    </td>';
    h += '    <td>';
    h += '        <div class="icon-trash" id="del_' + numrow + '" onclick="delete_row(' + numrow + ')" ></div>';
    h += '    </td>';
    h += '</tr>';
    jQuery('#numrow').val(numrow);
    jQuery('#attr').append(h);
    for (var i = 0; i < numrow; i++)
    {
        //console.log(numrow);
        jQuery('#' + (i + 1)).attr('value', jQuery('#slcolor_' + (i + 1)).val() + '_' + jQuery('#slsize_' + (i + 1)).val() + '_' + jQuery('#sl_' + (i + 1)).val());
        jQuery('#' + (i + 1)).attr('name', jQuery('#slcolor_' + (i + 1)).val() + '_' + jQuery('#slsize_' + (i + 1)).val() + '_' + (i + 1));
    }

}

function update_sl(num) {
    //alert(num);

}
function setvalue(num) {
    valsize = jQuery('#slsize_' + num).val();
    valcolor = jQuery('#slcolor_' + num).val();
    soluong = jQuery('#sl_' + num).val();
    //alert(valsize);
    jQuery('#' + num).attr('value', valcolor + '_' + valsize + '_' + soluong);
}


function load_price_input_ios(product_id)
{

    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=ios.load_price_input_ios',
        method: 'post',
        data: {
            id: product_id
        },
        success: function (dt)
        {
            jsonobj = jQuery.parseJSON(dt);
            var h = '';
            jQuery.each(jsonobj, function (index, value) {
                if (value["GIANHAP"] == null) {
                    load_price_input_product(id);
                    //console.log('load_price_input_product()');
                } else {
                    h += '<tr>';
                    h += '   <td>';
                    h += '   ' + value["NGAY"];
                    h += '   </td>';
                    h += '     <td>';
                    h += '   <b>' + value["color"] + '_' + value["size"] + '</b>';
                    h += '     </td>';
                    h += '   <td>';
                    h += '   ' + value["GIANHAP"];
                    h += '   </td>';
                    h += '</tr>';
                }
            });
            jQuery('#table_price').append(h);
        }
    });
}

function load_price_input_product(id) {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=ios.load_price_input_product',
        method: 'post',
        data: {
            id: id
        },
        beforeSend: function ()
        {
        },
        success: function (dt)
        {

            jsonobj = jQuery.parseJSON(dt);
            //console.log(jsonobj);
            var h = '';
            jQuery.each(jsonobj, function (index, value) {
                h += '<tr>';
                h += '   <td>';
                h += '   ' + value["NGAY"];
                h += '   </td>';
                h += '   <td>';
                h += '   ' + value["GIANHAP"];
                h += '   </td>';
                h += '</tr>';
            });
            jQuery('#table_price').append(h);
        }
    });
}

function show_search_tools()
{
    jQuery('.search-tools').toggle(400);
}

function check_integrity_constraints()
{
    var check_product = jQuery('#productnew').val();
    var pattern = /^[0-9]jQuery/;
    var check = 0;
    if (jQuery('#invoice_sku').val() === "") {
        toastr['error']('Vui lòng nhập mã hóa đơn');
        check++;
    } else if (jQuery('#lenofday').val() === "") {
        toastr['error']('Vui lòng chọn mã buổi hóa đơn');
        check++;
    } else if (jQuery('#id_product').val() === "") {
        toastr['error']('Vui lòng nhập mã sản phẩm');
        check++;
    } else if (jQuery('#statusios').val() == 1)
    {
        if (jQuery('#id_warehouse').val() == 0)
        {
            toastr['error']('Vui lòng chọn kho nhập');
            check++;
        }

    } else if (jQuery('#idproductmanufac').val() === "")
    {
        toastr['error']('Vui lòng nhập mã sản phẩm của nhà cung cấp');
        check++;
    } else if (jQuery('#nameproduct').val() === "") {
        toastr['error']('Vui lòng nhập tên sản phẩm');
        check++;
    } else if (jQuery('#namemanufac').val() == 0)
    {
        toastr['error']('Vui lòng chọn nhà cung cấp sản phẩm');
        check++;
    } else if (jQuery('#price').val() === "" && jQuery('#statusios').val() == 1) {
        toastr['error']('Vui lòng nhập giá sản phẩm');
        check++;
    } else if (pattern.test(jQuery('#price').val()) && jQuery('#statusios').val() == 1)
    {
        toastr['error']('Vui lòng nhập giá sản phẩm là số');
        check++;
    } else if (jQuery('#statusios').val() != 1 && jQuery('#id_warehouse_from').val() == 0 && jQuery('#statusios').val() != 3 && jQuery('#statusios').val() != 4) {
        toastr['error']('Vui lòng chọn kho để xuất');
        check++;
    } else if (jQuery('#statusios').val() != 1 && (jQuery('#id_warehouse_from').val() == jQuery('#id_warehouse_to').val()) &&
            jQuery('#statusios').val() != 3 && jQuery('#statusios').val() != 4)
    {
        toastr['error']('Kho xuất và kho nhập phải khác nhau');
        check++;
    } else if ((jQuery('#id_warehouse').val() == 3 || jQuery('#id_warehouse').val() == 4) && jQuery('#id_warehouse').val() == 0) {
        toastr['error']('Vui lòng chọn kho');
        check++;
    }
    return check;
}

function btn_save_ios_ajax()
{
    var check_product = jQuery('#productnew').val();
    var pattern = /^[0-9]jQuery/;
    if (check_product == 0)                // truong hop them san pham moi, xuat ra all size&color
    {
        var check = check_integrity_constraints();
        if (jQuery('#statusios').val() == 1 && check == 0)
        {
            var loi = 0;
            var numrow = jQuery('#numrow').val();
            for (i = 1; i <= numrow; i++)
            {
                if (jQuery('#sl_' + i) == 0) {// kiem tra so luong
                    loi++;
                }
            }
            if (loi > 0) {
                toastr['error']('Vui lòng nhập số lượng sản phẩm hoặc xóa mẫu tin đã thêm');
            } else {


                jQuery.ajax({
                    url: 'index.php?option=com_openshop&task=ios.getFormAjax&format=ajax',
                    method: 'post',
                    data: {
                        data: jQuery('#adminForm').serializeArray(),
                        product_img: jQuery('#product_image').val()
                    },
                    beforeSend: function ()
                    {
                        jQuery('#btnidok').attr('disabled', 'disabled');
                    },
                    success: function (dt)
                    {
                        var obj = jQuery.parseJSON(dt);
                        var idmasp = obj.masanpham; // id sanpham trong ios khi da insert
                        jQuery('#myfile').upload('index.php?option=com_openshop&format=ajax&task=ios.saveimages', {
                            masanpham: idmasp
                        },
                                function (success) {
                                },
                                function (prog, value) {
                                    jQuery('#prog').val(value);
                                });
                        jQuery('#btnidok').removeAttr('disabled');
                        jQuery('#warning_product').text('');
                        jQuery('#adminForm')[0].reset();
                        var num = jQuery('#numrow').val();
                        for (i = 1; i <= num; i++)
                        {
                            jQuery('#addtr_' + i).remove();
                        }
                        jQuery('#table_price').html('');
                        reset_form();
//                        toastr['success'](obj.masanpham);
                        toastr['success']('Thêm sản phẩm thành công');
                    }
                });
            }// end if
        }
        //console.log('vao 5');
    } else if (jQuery('#id_product').val() == '')
    {
        toastr['error']('Vui lòng chọn sản phẩm');
    } else if (jQuery('#statusios').val() == 1 || jQuery('#statusios').val() == 3 || jQuery('#statusios').val() == 4)
    {
        var loi = 0;
        var check = check_integrity_constraints();
        if (check == 0) {
            var numrow = jQuery('#numrow').val();
            for (i = 1; i <= numrow; i++)
            {
                if (jQuery('#sl_' + i) == 0) { // kiem tra so luong
                    loi++;
                }
            }
        }
        if (loi > 0) {
            toastr['error']('Vui lòng nhập số lượng sản phẩm hoặc xóa mẫu tin đã thêm');
        } else {

            //load images ajax
            //"components/com_openshop/controllers/xhr2.php"
            //index.php?option=com_openshop&format=ajax&task=ios.saveimages

//            jQuery('#myfile').upload('index.php?option=com_openshop&format=ajax&task=ios.saveimages',
//                    function (success) {
//                    },
//                    function (prog, value) {
//                        jQuery('#prog').val(value);
            // insert
            jQuery.ajax({
                url: 'index.php?option=com_openshop&task=ios.getFormAjax&format=ajax',
                method: 'post',
                data: {
                    data: jQuery('#adminForm').serializeArray()
                },
                beforeSend: function ()
                {
                    jQuery('#btnidok').attr('disabled', 'disabled');
                },
                success: function (dt)
                {
                    var obj = jQuery.parseJSON(dt);
                    toastr[obj.status](obj.messages);
                    jQuery('#btnidok').removeAttr('disabled');
                    jQuery('#adminForm')[0].reset();
                    var num = jQuery('#numrow').val();
                    for (i = 1; i <= num; i++)
                    {
                        jQuery('#addtr_' + i).remove();
                    }
                    jQuery('#table_price').html('');
                    reset_form();
                    jQuery('#warning_product').text('');
                    toastr['success']('Thêm sản phẩm thành công');

                }
            });
//                    });
            // end load


        }// end if

    } else if (jQuery('#statusios').val() == 2)
    {
        jQuery.ajax({
            url: 'index.php?option=com_openshop&task=ios.getFormAjax&format=ajax',
            method: 'post',
            data: {
                data: jQuery('#adminForm').serializeArray()
            },
            beforeSend: function ()
            {
                jQuery('#btnidok').attr('disabled', 'disabled');
            },
            success: function (dt)
            {

                jQuery('#btnidok').removeAttr('disabled');
                jQuery('#adminForm')[0].reset();
                jQuery('#table_price').html('');
                reset_form();
                jQuery('#warning_product').text('');
                toastr['success']('Thêm sản phẩm thành công');
            }
        });
    }

}

function add_row_sizecolor(i) {
    if (jQuery('#id_product').val() == '')
    {
        toastr['warning']('Vui lòng nhập mã Sản phẩm');
    } else
    {
        var numrow = jQuery('#numrow').val();
        numrow++;
        var h = '';
        h += '<tr id="addtr_' + numrow + '">';
        h += '   <td>';
        h += '       <select onchange="setvalue(' + numrow + ')" name="slcolor_' + numrow + '" id="slcolor_' + numrow + '">';
        h += '       </select>';
        h += '<input type="hidden" name="option_id_color" id="option_id_color" value="1" />';
        h += '   </td>';
        h += '   <td>';
        h += '       <select onchange="setvalue(' + numrow + ')" name="slsize_' + numrow + '" id="slsize_' + numrow + '">';
        h += '       </select>';
        h += '   </td>';
        h += '<input type="hidden" name="option_id_size" id="option_id_size" value="2" />';
        h += '    <td>';
        h += '        <input onmouseout="setvalue(' + numrow + ')" id="sl_' + numrow + '" name="sl_' + numrow + '" value="0" type="text"  /><input value="" type="text" id="' + numrow + '" name="' + numrow + '"  style="display:none;"/>';
        h += '    </td>';
        h += '    <td>';
        h += '        <div class="icon-trash" id="del_' + numrow + '" onclick="delete_row(' + numrow + ')" ></div>';
        h += '    </td>';
        h += '</tr>';
        jQuery('#numrow').val(numrow);
        jQuery('#sodongmax').val(numrow);
        jQuery('#attr').append(h);

        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=ios.getColorSizeProduct',
            method: 'post',
            data: {id: jQuery('#id_product').val()},
            success: function (dt) {
                var d = jQuery.parseJSON(dt);
                var h_color = '';
                h_color += '<option value="0">- Chọn màu sắc -</option>';
                jQuery.each(d['color'], function (index, value) {
                    h_color += '<option value=' + value['id'] + '>';
                    h_color += value['value'];
                    h_color += '</option>';
                });

                var h_size = '';
                h_size += '<option value="0">- Chọn kích thước -</option>';
                jQuery.each(d['size'], function (index, value) {
                    h_size += '<option value=' + value['id'] + '>';
                    h_size += value['value'];
                    h_size += '</option>';
                });

                jQuery('#slcolor_' + numrow).append(h_color);
                jQuery('#slsize_' + numrow).append(h_size);
            }
        });

        for (var i = 0; i < numrow; i++)
        {
            //console.log(numrow);
            jQuery('#' + (i + 1)).attr('value', jQuery('#slcolor_' + (i + 1)).val() + '_' + jQuery('#slsize_' + (i + 1)).val() + '_' + jQuery('#sl_' + (i + 1)).val());
            jQuery('#' + (i + 1)).attr('name', jQuery('#slcolor_' + (i + 1)).val() + '_' + jQuery('#slsize_' + (i + 1)).val() + '_' + (i + 1));
        }
    }
}
