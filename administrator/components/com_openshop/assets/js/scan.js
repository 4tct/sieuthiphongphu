$ = jQuery.noConflict();

$(function () {
    $('#scan').keyup(function (e) {
        $('#scanMode').val(e.keyCode);
    });
});

function scanValue(val) {

    var arr = val.split('-');
    if ($('#zonescan').val() === "") {
        toastr['warning']('Chưa chọn khu vực!');
    } else
    {
        if (typeof (arr[1]) === 'undefined' || arr[1] === '' || arr[2] === '' || isNaN(arr[1]) || arr.length >= 3) {
            toastr['warning']('Mã sản phẩm không đúng!');
            $('.product_cur').text(val);
            var path = '/administrator/components/com_openshop/assets/audio/error.mp3';
            if($('#fileSound').val() !== ''){
                path = '/images/com_openshop/medias/' + $('#fileSound').val();
            }
            $('#error').html('<audio style="display:none;" src="'+ path +'"  controls autoplay/>');
            $.ajax({
                url: 'index.php?option=com_openshop&format=ajax&task=scans.writeError',
                method: 'post',
                data: {
                    error: val,
                    zone: $('#zonescan').val(),
                    mode: $('#scanMode').val()
                },
                success: function () {
                    $('#scan').val('');
                    $('#scan').focus();
                }
            });
        } else
        {
            $.ajax({
                url: 'index.php?option=com_openshop&format=ajax&task=scans.scanValue',
                method: 'post',
                data: {
                    val: val,
                    zone: $('#zonescan').val(),
                    mode: $('#scanMode').val()
                },
                success: function (dt) {
                    $('#scan').val('');
                    $('#scan').focus();
                    var d = $.parseJSON(dt);
                    toastr[d['status']](d['message']);
                    if (d['add'] === '0') {
                        $('.quantity_import_' + d['product_name']).text(d['quantity']);
                    }

                    var h = '';
                    var num = (parseInt($('#max_value').val()) + 1);
                    h += '<tr id="' + d['product_name'] + '">';
                    h += '  <td>' + num + '</td>';
                    h += '  <td>' + d['product_sku'] + '</td>';
                    h += '  <td>' + d['size'] + '</td>';
                    h += '  <td>' + d['color'] + '</td>';
                    h += '  <td class="quantity_import_' + d['product_name'] + '">' + d['quantity'] + '</td>';
                    h += '  <td>' + d['scan_by'] + '</td>';
                    h += '  <td>' + d['zone'] + '</td>';
                    h += '  <td>' + d['mode'] + '</td>';
                    h += '  <td>' + d['date_scan'] + '</td>';
                    h += '</tr>';

                    $('#tbl_scan').append(h);
                    $('#max_value').val(num);

                    $('.product_cur').text(d['product_name']);
                    $('.total_quantity_scaned').text(num);

                }
            });
        }
    }

    $('#scanAdmin').submit(function () {
        return false;
    });
}

function delete_all_scan() {
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=scans.delete_all_scaned',
        method: 'post',
        success: function (dt) {
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            window.location.href = 'index.php?option=com_openshop&view=scans';
        }
    });
}

function admin_mode() {
    window.location.href = 'index.php?option=com_openshop&view=scans&layout=add_mode';
}

function standard_mode() {
    window.location.href = 'index.php?option=com_openshop&view=scans';
}

function download_error(){
    $('#downloadErrorScan').submit();
}

function delete_error(){
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=scans.deleteError',
        method: 'post',
        success:function(dt){
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
        }
    });
}

function showDeleteScanPro(id){
    $('.deleteP').css('display','block');
    $('.btn-dlt').attr('onclick','deleteScanPro('+ id +')');
}

function closeScanPro(){
    $('.deleteP').css('display','none');
}

function deleteScanPro(id){
    $.ajax({
       url: 'index.php?option=com_openshop&task=scans.deleteScanPro&format=ajax',
       method: 'post',
       data:{
           id:id
       },
       success:function(dt){
           window.location.href = dt;
       }
    });
}