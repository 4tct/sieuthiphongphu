/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Author: Huy Huynh
 */

$ = jQuery.noConflict();

function delete_img_inOrder(key) {
    var h = '<input type="file" name="waiting_input" id="waiting_input"/>';
    h += '<input type="hidden" name="'+ key +'" />';
    
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=configuration.delete_img_inOrder',
        method: 'post',
        data: {
            key : key
        },
        success:function(dt){
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            if(d['status'] = 'success'){
                $('.show_img_inOrder_' + key).html(h);
            }
        }
    });
    
}

function addNumberPhone() {
    var h = '';
    var i = $('#max_phone').val();
    ++i;
    h += '<div class="input-group phone_' + i + '" style="width: 300px;">';
    h += '  <span class="input-group-addon"><i class="icon-phone-2"></i></span>';
    h += '  <input type="text" class="form-control input_phone_' + i + '" placeholder="Số điện thoại" aria-describedby="phone" onchange="getNumberPhone()">';
    h += '  <span class="input-group-addon deleteNumP_' + i + '" style="background-color:white;color: red;" onclick="deleteRowPhone(' + i + ')"><i class="icon-delete"></i></span>';
    h += '</div>';

    $('#max_phone').val(i);
    $('.numberphone').append(h);
}

function deleteRowPhone(i) {
    $('.phone_' + i).remove();
    var n = $('#max_phone').val();
    for (var j = i; j <= n; j++) {
        $('.phone_' + (j+1)).attr('class', 'input-group phone_' + j);
        $('.input_phone_' + (j+1)).attr('class', 'form-control input_phone_' + j);
        $('.deleteNumP_' + (j+1)).attr('onclick', 'deleteRowPhone(' + j + ')');
        $('.deleteNumP_' + (j+1)).attr('class', 'input-group-addon deleteNumP_' + j);
    }
    $('#max_phone').val(--n);
    
    getNumberPhone();
}

function getNumberPhone() {
    var n = $('#max_phone').val();
    var numberP = '';
    var t = 1;
    for (var i = 1; i <= n; i++) {
        if ($('.input_phone_' + i).val() !== '') {
            if (t === 1) {
                numberP += $('.input_phone_' + i).val();
                t = 0;
            } else {
                numberP += ',' + $('.input_phone_' + i).val();
            }
        }
    }

    $('#contact_phone').val(numberP);
}


//email
function addEmail() {
    var h = '';
    var i = $('#max_email').val();
    ++i;
    h += '<div class="input-group email_' + i + '" style="width: 300px;">';
    h += '  <span class="input-group-addon"><i class="icon-mail"></i></span>';
    h += '  <input type="text" class="form-control input_email_' + i + '" placeholder="Email" aria-describedby="email" onchange="getEmail()">';
    h += '  <span class="input-group-addon deleteEmail_' + i + '" style="background-color:white;color: red;" onclick="deleteRowEmail(' + i + ')"><i class="icon-delete"></i></span>';
    h += '</div>';

    $('#max_email').val(i);
    $('.email').append(h);
}

function deleteRowEmail(i) {
    $('.email_' + i).remove();
    var n = $('#max_email').val();
    for (var j = i; j <= n; j++) {
        $('.email_' + (j+1)).attr('class', 'input-group email_' + j);
        $('.input_email_' + (j+1)).attr('class', 'form-control input_email_' + j);
        $('.deleteEmail_' + (j+1)).attr('onclick', 'deleteRowEmail(' + j + ')');
        $('.deleteEmail_' + (j+1)).attr('class', 'input-group-addon deleteEmail_' + j);
    }
    $('#max_email').val(--n);
    
    getEmail();
}

function getEmail() {
    var n = $('#max_email').val();
    var strEmail = '';
    var t = 1;
    for (var i = 1; i <= n; i++) {
        if ($('.input_email_' + i).val() !== '') {
            if (t === 1) {
                strEmail += $('.input_email_' + i).val();
                t = 0;
            } else {
                strEmail += ',' + $('.input_email_' + i).val();
            }
        }
    }

    $('#contact_email').val(strEmail);
}

//address ------------------------------------------
function addAddress() {
    var h = '';
    var i = $('#max_address').val();
    ++i;
    h += '<div class="input-group address_' + i + '" style="width: 300px;">';
    h += '  <span class="input-group-addon"><i class="icon-pin"></i></span>';
    h += '  <input type="text" class="form-control input_address_' + i + '" placeholder="Địa chỉ" aria-describedby="address" onchange="getAddress()">';
    h += '  <input type="text" class="form-control input_map_' + i + '" placeholder="Bản đồ" aria-describedby="map" onchange="getAddress()">';
    h += '  <span class="input-group-addon deleteAddress_' + i + '" style="background-color:white;color: red;" onclick="deleteRowAddress(' + i + ')"><i class="icon-delete"></i></span>';
    h += '</div>';

    $('#max_address').val(i);
    $('.address').append(h);
}

function deleteRowAddress(i) {
    $('.address_' + i).remove();
    var n = $('#max_address').val();
    for (var j = i; j <= n; j++) {
        $('.address_' + (j+1)).attr('class', 'input-group address_' + j);
        $('.input_address_' + (j+1)).attr('class', 'form-control input_address_' + j);
        $('.deleteAddress_' + (j+1)).attr('onclick', 'deleteRowAddress(' + j + ')');
        $('.deleteAddress_' + (j+1)).attr('class', 'input-group-addon deleteAddress_' + j);
    }
    $('#max_address').val(--n);
    
    getAddress();
}

function getAddress() {
    var n = $('#max_address').val();
    var strAddress = '';
    var t = 1;
    for (var i = 1; i <= n; i++) {
        if ($('.input_address_' + i).val() !== '') {
            if (t === 1) {
                strAddress += $('.input_address_' + i).val();
                strAddress += '@@' + $('.input_map_' + i).val();
                t = 0;
            } else {
                strAddress += ';' + $('.input_address_' + i).val();
                strAddress += '@@' + $('.input_map_' + i).val();
            }
        }
    }

    $('#contact_address').val(strAddress);
}

//timework ------------------------------------------
function addTimework() {
    var h = '';
    var i = $('#max_timework').val();
    ++i;
    h += '<div class="input-group timework_' + i + '" style="width: 300px;">';
    h += '  <span class="input-group-addon"><i class="icon-clock"></i></span>';
    h += '  <input type="text" class="form-control input_timework_' + i + '" placeholder="Thời gian làm việc" aria-describedby="timework" onchange="getTimework()">';
    h += '  <span class="input-group-addon deleteTimework_' + i + '" style="background-color:white;color: red;" onclick="deleteRowTimework(' + i + ')"><i class="icon-delete"></i></span>';
    h += '</div>';

    $('#max_timework').val(i);
    $('.timework').append(h);
}

function deleteRowTimework(i) {
    $('.timework_' + i).remove();
    var n = $('#max_timework').val();
    for (var j = i; j <= n; j++) {
        $('.timework_' + (j+1)).attr('class', 'input-group timework_' + j);
        $('.input_timework_' + (j+1)).attr('class', 'form-control input_timework_' + j);
        $('.deleteTimework_' + (j+1)).attr('onclick', 'deleteRowTimework(' + j + ')');
        $('.deleteTimework_' + (j+1)).attr('class', 'input-group-addon deleteTimework_' + j);
    }
    $('#max_timework').val(--n);
    
    getTimework();
}

function getTimework() {
    var n = $('#max_timework').val();
    var strEmail = '';
    var t = 1;
    for (var i = 1; i <= n; i++) {
        if ($('.input_timework_' + i).val() !== '') {
            if (t === 1) {
                strEmail += $('.input_timework_' + i).val();
                t = 0;
            } else {
                strEmail += ',' + $('.input_timework_' + i).val();
            }
        }
    }

    $('#start_end_work').val(strEmail);
}
