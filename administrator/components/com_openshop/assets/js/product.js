jQuery = jQuery.noConflict();

jQuery(document).ready(function () {
    jQuery('#product_image').on('change', function () {
        var t = jQuery('#product_image')[0].files[0].name;
        jQuery('#name_image_show').text(t);
    });

    jQuery('#addTagBtn').click(function () {
        jQuery('#tags option:selected').each(function () {
            jQuery(this).appendTo(jQuery('#selectedTags'));
        });
    });
    jQuery('#removeTagBtn').click(function () {
        jQuery('#selectedTags option:selected').each(function (el) {
            jQuery(this).appendTo(jQuery('#tags'));
        });
    });
    jQuery('.tagRemove').click(function (event) {
        event.preventDefault();
        jQuery(this).parent().remove();
    });
    jQuery('ul.tags').click(function () {
        jQuery('#searchProSKUColorDiff').focus();
    });
    jQuery('#searchProSKUColorDiff').keypress(function (event) {
        if (event.which == '13') {
            var sku = jQuery('.productSKUDiff_0').val();
            var id = jQuery('.productIDDiff_0').val();
            if (jQuery(this).val() != '' && typeof (sku) !== 'undefined') {
                jQuery('<li class="addedTagProduct">' + sku + '<span onclick="jQuery(this).parent().remove();" class="tagRemove icon-delete "></span><input type="hidden" value="' + id + '" name="productSKUColorDifferent[]"></li>').insertBefore('.tags .tagAdd');
                jQuery(this).val('');
            }
        }
    });

});

function show_modal_productInOrder(id) {
    jQuery('#modal_productInOrder').modal('show');
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=product.getProductInOrder',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            var h = '';
            jQuery('.productName').html('<b>' + d[0]['product_name'] + ' - ' + d[0]['product_sku'] + ' </b>');
            jQuery.each(d, function (index, value) {
                h += '<tr>';
                h += '  <td>' + (index + 1) + '</td>';
                h += '  <td>' + value['fullname'] + '</td>';
                h += '  <td><a href="index.php?option=com_openshop&task=order.edit&customer_id=' + value['customer_id'] + '" target="_blank">' + value['telephone'] + '</a></td>';
                h += '  <td>' + value['email'] + '</td>';
                h += '  <td>';
                h += '      ' + value['address'] + '<br>' + '[ <b>' + value['district_name'] + '</b> - <b>' + value['zone_name'] + '</b> ]';
                h += '  </td>';
                h += '  <td style="text-align:center;">' + value['quantity'] + '</td>';
                h += '</tr>';
            });


            jQuery('#info_productInOrder').html(h);
        }
    });
}

function restore_product(id) {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=product.restore_product',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            if (dt === 'success')
                window.location.href = 'index.php?option=com_openshop&view=products';
            else
                toastr['error']('Không kết nối được với máy chủ');
        }
    });
}


function checkProductSKU(v) {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&task=product.checkProductSKU&format=ajax',
        method: 'post',
        data: {
            v: v
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            if (d.status === 'warning') {
                toastr[d.status](d.message);
                jQuery('.error_message_product_sku').html(d.message);
                jQuery('#product_sku').css('border', '2px solid red');
                jQuery('#checkFormSubmit').val('0');
            } else {
                jQuery('.error_message_product_sku').html('');
                jQuery('#product_sku').css('border', '1px solid #cccccc');
                jQuery('#checkFormSubmit').val('1');
            }
        }
    })
}

/*
 * get product
 */
function getProductDiff(i, v) {

    if (v.length > 3) {
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=product.getProductDiff',
            method: 'post',
            data: {
                i: i,
                v: v
            },
            success: function (dt) {
                var d = jQuery.parseJSON(dt);
                if (d.status === 'success') {
                    jQuery('.showSearchProductDiff ul').html(d.html);
                    jQuery('.showSearchProductDiff').css('display', 'block');
                } else {
                    toastr[d.status](d.message);
                }
            }
        })
    } else {
        jQuery('.showSearchProductDiff').css('display', 'none');
    }
}


function getSKUProDiff(i) {
    var sku = jQuery('.productSKUDiff_' + i).val();
    var id = jQuery('.productIDDiff_' + i).val();
    if (typeof (sku) !== 'undefined') {
        jQuery('<li class="addedTagProduct">' + sku + '<span onclick="jQuery(this).parent().remove();" class="tagRemove icon-delete "></span><input type="hidden" value="' + id + '" name="productSKUColorDifferent[]"></li>').insertBefore('.tags .tagAdd');
        jQuery('#searchProSKUColorDiff').val('');
        jQuery('.showSearchProductDiff').css('display', 'none');
    }
}

function exportPro() {
    var chooseFile = jQuery('#slt_chooseFile').val();
    var manu = jQuery('#slt_manu').val();
    var status = jQuery('#slt_typeP').val();
    $check = 1;

    if (chooseFile === '') {
        toastr['warning']('Vui lòng chọn File');
        $check = 0;
    } else if (manu === '') {
        toastr['warning']('Vui lòng chọn NCC');
        $check = 0;
    } else if (status === '') {
        toastr['warning']('Vui lòng chọn loại SP');
        $check = 0;
    }

    if ($check) {
        jQuery('#myFormExportPro').submit();
    }
}

function LoadFileDownLoad() {
    var chooseFile = jQuery('#slt_chooseFile').val();
    var manu = jQuery('#slt_manu').val();
    var status = jQuery('#slt_typeP').val();
    $check = 1;

    if (chooseFile === '') {
        toastr['warning']('Vui lòng chọn File');
        $check = 0;
    } else if (manu === '') {
        toastr['warning']('Vui lòng chọn NCC');
        $check = 0;
    } else if (status === '') {
        toastr['warning']('Vui lòng chọn loại SP');
        $check = 0;
    }

    if ($check) {
        
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=product.getFileDownLoad',
            method: 'post',
            data: {
                chooseFile: chooseFile,
                manu: manu,
                status: status
            },
            success:function(dt){
                var d = jQuery.parseJSON(dt);
                jQuery('.fileDownLoad').html( d.data );
            }
        })
        
    }
}


function downloadPatern() {
    jQuery('#downloadImportProduct').submit();
}

var options = {
    url: 'index.php?option=com_openshop&task=product.uploadFile&format=ajax',
    beforeSubmit: function (formData, jqForm, options) {
        jQuery('.barPercent').css('width', '0%');
        jQuery('.barPercent').html('0%');
        var type_arr = ['xls', 'xlsx'];
        var nameF = formData[0]['value']['name'];
        var sizeF = formData[0]['value']['size'];

        var arr = nameF.split('.');
        var typeF = arr[ parseInt(arr.length) - 1 ];

        if (type_arr.indexOf(typeF) != -1) {
            return true;
        } else {
            toastr['warning']('Vui lòng chọn file có định dạng XLS, XLSX');
            return false;
        }
//        console.log(jqForm);
//        console.log(options);
    },
    uploadProgress: function (event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        jQuery('.barPercent').css('width', percentVal);
        jQuery('.barPercent').html(percentVal);
//        console.log(percentVal);
    },
    success: function (responseText, statusText) {
        if (statusText === 'success') {
            var d = jQuery.parseJSON(responseText);
            if (d.status === 'success') {

//                jQuery('.checkDataForm').css('display','block');
//                jQuery('.uploadFileForm').css('display','none');

                console.log(d);
            }

            toastr[d.status](d.message);

//            window.location.reload();
        }
    },
    error: function () {
        console.log('error');
    }
};


jQuery('#myUpoadFile').ajaxForm(options);