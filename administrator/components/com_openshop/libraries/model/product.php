<?php
defined('_JEXEC') or die('Restricted access');

class Product {

    //tên bảng
    public $table = '#__openshop_products';
    //tên các bảng liên kết
    public $tablejoin = array(
        '#__openshop_productdetailes'
    );
    public $consfig_export = array(
        'join' => '1',
        'start' => '5',
        'column' => array(
            'B' => 'product_sku',
            'C' => 'product_price'
        ),
        'tablejoin' => array(
            '#__openshop_productdetails' => array(
                'id' => 'product_id',
                'constant' => '0',      //k cho phép sử dụng 1 ip cho liện tục insert
                'columnjoin' => array(
                    'E' => 'product_name'
                )
            ),
            '#__openshop_productcategories' => array(
                'id' => 'product_id',
                'constant' => '1',  //sử dụng 1 id cho insert nhiều dòng
                'explode' => ',',
                'function' => 'getCategoryPro',
                'columnjoin' => array(
                    'F' => 'category_id'
                )
            )
        )
    );

}
