<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')){
    define('DS', DIRECTORY_SEPARATOR);
}
define('OPENSHOP_LANG_PREFIX', 'OPENSHOP');
define('OPENSHOP_TABLE_PREFIX', 'openshop');
define('OPENSHOP_COMPONENTS', JPATH_ROOT.DS.'components'.DS.'com_openshop'.DS);
define('OPENSHOP_ADMIN_COMPONENTS', JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_openshop');
define('OPENSHOP_SITE_COMPONENTS', JPATH_ROOT .DS.'com_openshop'.DS.'com_openshop');
define('OPENSHOP_ADMIN_PATH_ICON',OPENSHOP_ADMIN_COMPONENTS.DS.'assets'.DS.'icons'.DS);
define('OPENSHOP_ADMIN_PATH_ICON_HTTP',  JUri::base().'components'.DS.'com_openshop'.DS.'assets'.DS.'icons'.DS);
define('OPENSHOP_ADMIN_JS', JUri::base(). 'components'.DS.'com_openshop' . DS . 'assets' . DS . 'js');
define('OPENSHOP_ADMIN_ICON', JUri::base(). 'components'.DS.'com_openshop' . DS . 'assets' . DS . 'icons');
define('OPENSHOP_ADMIN_PATH_IMG_HTTP',  JUri::base().'components'.DS.'com_openshop'.DS.'assets'.DS.'images');
define('OPENSHOP_PATH_IMG_PRODUCT',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'products'.DS);
define('OPENSHOP_PATH_IMG_PRODUCT_RESIZED',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'products'.DS.'resized'.DS);
define('OPENSHOP_PATH_IMG_PRODUCT_HTTP',  JUri::root() .'images'.DS.'com_openshop'.DS.'products'.DS);
define('OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'products'.DS.'resized'.DS);
define('OPENSHOP_PATH_TRANSPLATE_ADMIN',  JPATH_ROOT.DS.'administrator'.DS.'language'.DS);
define('OPENSHOP_PATH_IMG_COLOR',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'color'.DS);
define('OPENSHOP_PATH_IMG_COLOR_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'color'.DS);
define('OPENSHOP_PATH_IMG_OTHER',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'other'.DS);
define('OPENSHOP_PATH_IMG_OTHER_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'other'.DS);
define('OPENSHOP_PATH_IMG_CATEGORIES',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'categories'.DS);
define('OPENSHOP_PATH_IMG_CATEGORIES_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'categories'.DS);
define('OPENSHOP_PATH_IMG_OPTION',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'options'.DS);
define('OPENSHOP_PATH_IMG_OPTION_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'options'.DS);
define('OPENSHOP_PATH_IMG_MEDIA',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'medias'.DS);
define('OPENSHOP_PATH_IMG_MEDIA_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'medias'.DS);
define('OPENSHOP_PATH_INCLUDES',  JPATH_ROOT.DS.'includes'.DS);
define('OPENSHOP_PATH_FILES',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'files'.DS);
define('OPENSHOP_PATH_FILES_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'files'.DS);
define('OPENSHOP_PATH_SCANS',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'scans'.DS);
define('OPENSHOP_PATH_SCANS_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'scans'.DS);
define('OPENSHOP_PATH_BANNERS',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'banners'.DS);
define('OPENSHOP_PATH_BANNERS_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'banners'.DS);
define('OPENSHOP_PATH_TEMP',  JPATH_ROOT.DS.'images'.DS.'com_openshop'.DS.'temp'.DS);
define('OPENSHOP_PATH_TEMP_HTTP',  JUri::root().'images'.DS.'com_openshop'.DS.'temp'.DS);

