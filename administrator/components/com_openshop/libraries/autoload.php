<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$librariesPath = dirname(__FILE__);
$classes = array(
	'OpenShopViewList'		=> $librariesPath.'/mvc/viewlist.php',
	'OpenShopViewForm'		=> $librariesPath.'/mvc/viewform.php',
	'OpenShopModelList'             => $librariesPath.'/mvc/modellist.php',				
	'OpenShopModel'                 => $librariesPath.'/mvc/model.php',
	'OpenShopController'            => $librariesPath.'/mvc/controller.php',
	'OpenShopTable'                 => $librariesPath.'/mvc/table.php',
	'OpenShopView'			=> $librariesPath.'/mvc/view.php',
	'OpenShopCart'			=> JPATH_ROOT.'/components/com_openshop/helpers/cart.php',
	'OpenShopQuote'                 => JPATH_ROOT.'/components/com_openshop/helpers/quote.php',
	'OpenShopCoupon'		=> JPATH_ROOT.'/components/com_openshop/helpers/coupon.php',
	'OpenShopVoucher'		=> JPATH_ROOT.'/components/com_openshop/helpers/voucher.php',
	'OpenShopCurrency'		=> JPATH_ROOT.'/components/com_openshop/helpers/currency.php',
	'OpenShopCustomer'		=> JPATH_ROOT.'/components/com_openshop/helpers/customer.php',
	'OpenShopHelper'		=> JPATH_ROOT.'/components/com_openshop/helpers/helper.php',
        'OpenShopAPI'			=> JPATH_ROOT.'/components/com_openshop/helpers/api.php',
	'OpenShopHtmlHelper'            => JPATH_ROOT.'/components/com_openshop/helpers/html.php',
	'OpenShopImage'                 => JPATH_ROOT.'/components/com_openshop/helpers/image.php',
	'OpenShopLength'		=> JPATH_ROOT.'/components/com_openshop/helpers/length.php',
	'OpenShopOption'		=> JPATH_ROOT.'/components/com_openshop/helpers/option.php',
	'OpenShopShipping'		=> JPATH_ROOT.'/components/com_openshop/helpers/shipping.php',
	'OpenShopPayment'		=> JPATH_ROOT.'/components/com_openshop/helpers/payment.php',
	'OpenShopDonate'		=> JPATH_ROOT.'/components/com_openshop/helpers/donate.php',
	'OpenShopFile'			=> JPATH_ROOT.'/components/com_openshop/helpers/file.php',
	'OpenShopTax'			=> JPATH_ROOT.'/components/com_openshop/helpers/tax.php',
	'OpenShopWeight'		=> JPATH_ROOT.'/components/com_openshop/helpers/weight.php',
	'os_payments'                   => JPATH_ROOT . '/components/com_openshop/plugins/payment/os_payments.php',
	'os_payment'                    => JPATH_ROOT . '/components/com_openshop/plugins/payment/os_payment.php',
	'openshop_shipping'             => JPATH_ROOT . '/components/com_openshop/plugins/shipping/openshop_shipping.php'
);

foreach ($classes as $className => $path)
{
	JLoader::register($className, $path);
}
JLoader::register('OpenShopRoute', JPATH_ROOT.'/components/com_openshop/helpers/' . ((version_compare(JVERSION, '3.0', 'ge') && JLanguageMultilang::isEnabled() && count(OpenShopHelper::getLanguages()) > 1) ? 'routev3.php' : 'route.php'));