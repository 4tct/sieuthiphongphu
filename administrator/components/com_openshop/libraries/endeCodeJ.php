<?php

defined('_JEXEC') or die('Restricted access');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class endeCodeJ {
    
    //Decode By HuyHuynh (Jandar)
    private static function changeEncode($value) {
        $v = array('a', 'A', '/', 'b', 'B', '*', 'c', 'C', '1', '-', 'd', 'D', '+', 'e', 'E', '|', '2', 'f', 'F', '@', 'g', 'G', '#', 'h', 'H', '$', 'i', 'I', '%', 'j', 'J', '^', '3', 'k', 'K', '&', 'l', 'L', '(', 'm', 'M', ')', 'n', 'N', '_', 'o', 'O', '~', '4', 'p', 'P', '`', 'q', 'Q', '[', 'r', 'R', ']', 's', 'S', '{', 't', 'T', '}', '5', 'u', 'U', ':', 'v', 'V', ';', 'w', 'W', '"', 'x', 'X', '\'', 'y', 'Y', '?', '6', 'z', 'Z', '>', '<', '.', ',', '7', '8', '9', '0', ' ');
        return $v[$value];
    }

    private static function changeDecode($value) {
        $v = array('a', 'A', '/', 'b', 'B', '*', 'c', 'C', '1', '-', 'd', 'D', '+', 'e', 'E', '|', '2', 'f', 'F', '@', 'g', 'G', '#', 'h', 'H', '$', 'i', 'I', '%', 'j', 'J', '^', '3', 'k', 'K', '&', 'l', 'L', '(', 'm', 'M', ')', 'n', 'N', '_', 'o', 'O', '~', '4', 'p', 'P', '`', 'q', 'Q', '[', 'r', 'R', ']', 's', 'S', '{', 't', 'T', '}', '5', 'u', 'U', ':', 'v', 'V', ';', 'w', 'W', '"', 'x', 'X', '\'', 'y', 'Y', '?', '6', 'z', 'Z', '>', '<', '.', ',', '7', '8', '9', '0', ' ');
        $res = array_keys($v, $value);
        return $res[0];
    }

    private static function changeValueDecode($value, $checkJ = 0) {
        $v = array('a', 'A', '/', 'b', 'B', '*', 'c', 'C', '1', '-', 'd', 'D', '+', 'e', 'E', '|', '2', 'f', 'F', '@', 'g', 'G', '#', 'h', 'H', '$', 'i', 'I', '%', 'j', 'J', '^', '3', 'k', 'K', '&', 'l', 'L', '(', 'm', 'M', ')', 'n', 'N', '_', 'o', 'O', '~', '4', 'p', 'P', '`', 'q', 'Q', '[', 'r', 'R', ']', 's', 'S', '{', 't', 'T', '}', '5', 'u', 'U', ':', 'v', 'V', ';', 'w', 'W', '"', 'x', 'X', '\'', 'y', 'Y', '?', '6', 'z', 'Z', '>', '<', '.', ',', '7', '8', '9', '0', ' ');
        $randomString = 'abcdefghijhklmnopqrstuvwxyz';
        $res = '';
        for ($i = 0; $i < strlen($value); $i++) {
            $t = array_keys($v, substr($value, $i, 1));
            if (!$checkJ) {
                $res .= $t[0];
            } else {
                $ran = rand(1, strlen($randomString) - 1);

                $res .= $t[0] . substr($randomString, $ran, 1);
            }
        }
        return $res;
    }

    private static function mergeKeyValue($k, $p) {
        $res = '';
        $t = 0;

        if (count($k) > count($p)) {
            $c = strlen($k);
        } else {
            $c = strlen($p);
        }

        for ($i = 0; $i < $c; $i++) {
            if (substr($k, $i, 1) != '') {
                $res .= substr($k, $i, 1);
            }

            if (substr($p, $i, 1) != '') {
                $res .= substr($p, $i, 1);
                ++$t;
            }
        }
        return $res . '-' . self::changeEncode($t);
//        return $p .'-'. strlen($p);
    }

    private static function divideKeyValue($value) {
        $rvalue = '';
        $rkey = '';
        $v = explode('-', $value);
        $n = self::changeDecode($v[1]);  //số lượng
        $vv = $v[0];        //mã đã mà hóa
        //lấy res1 theo số lượng
        $checkt = 0;
        $tKey = 0;
        $countK = strlen(self::changeValueDecode(OpenShopHelper::getConfigValue('keyDecode')));
        for ($i = 0; $i < strlen($vv); $i++) {
            if ($checkt < $n) {
                if ($i % 2 == 0 && $tKey < $countK) {
                    $rkey .= substr($vv, $i, 1);
                    ++$tKey;
                } else {
                    $rvalue .= substr($vv, $i, 1);
                    ++$checkt;
                }
            } else {
                $rkey .= substr($vv, $i, 1);
            }
        }

        return $rkey . '-' . $rvalue;
    }

    private static function checkKeyEncode($key) {
        $key_sub = '';
        for ($i = (strlen($key) - 1); $i >= 0; $i--) {
            $key_sub .= substr($key, $i, 1);              //Đảo ngược + giải mã
        }
        //mã hóa key hệ thống để so sánh
        $keyE = self::changeValueDecode(OpenShopHelper::getConfigValue('keyDecode'));

        if ($keyE == $key_sub) {
            return '1';
        } else {
            return '0';
        }
    }

    private static function encodeValue($v) {
        $find = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'h', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        $res = '';
        $valueE = explode(' ', str_replace($find, ' ', $v));
        foreach ($valueE as $ve) {
            if ($ve != '') {
                $res .= self::changeEncode($ve);
//                $res .= '-' . $ve;
            }
        }
        return $res;
    }

    public static function encodeJ($value) {
        //mã hóa key hệ thống (chỉ số)
        $key = self::changeValueDecode(OpenShopHelper::getConfigValue('keyDecode'));
        //mã hóa giá trị (có chữ và số)
        $param = self::changeValueDecode($value, '1');

        $key_sub = '';
        for ($i = (strlen($key) - 1); $i >= 0; $i--) {
            $key_sub .= substr($key, $i, 1);        //Đảo ngược
        }
        //hợp + xem các giá trị
        $res = self::mergeKeyValue($key_sub, $param);

        return $res;
    }

    public static function decodeJ($value) {
        $r = explode('-', self::divideKeyValue($value)); //phân ra 2 giá trị: 0->key; 1->value

        $checkKey = self::checkKeyEncode($r[0]);
        if ($checkKey) {
            return self::encodeValue($r[1]);
        } else {
            return 'Error';
        }
    }

}
