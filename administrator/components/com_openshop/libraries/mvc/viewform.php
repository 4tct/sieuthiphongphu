<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopViewForm extends JViewLegacy {

    /**
     * Language prefix
     * @var string
     */
    public $lang_prefix = OPENSHOP_LANG_PREFIX;

    public function display($tpl = null) {
        // Check access first
        $mainframe = JFactory::getApplication();
        if(OpenShopPermissions::checkPerOA())
        {
            if(!(OpenShopHelper::getPermissionAction(OpenShopInflector::pluralize($this->getName()), 'edit') || OpenShopHelper::getPermissionAction(OpenShopInflector::pluralize($this->getName()), 'create'))){                
                $mainframe->redirect('index.php?option=com_openshop&view=' . OpenShopInflector::pluralize($this->getName()));
            }
            else
            {
                if(!OpenShopHelper::getPermissionAction(OpenShopInflector::pluralize($this->getName()), 'edit') && JFactory::getApplication()->input->getString('task') == 'edit'){
                    $mainframe->redirect('index.php?option=com_openshop&view=' . OpenShopInflector::pluralize($this->getName()));
                }
            }
        }
        else
        {
            $mainframe->redirect('index.php?option=com_openshop&view=dashboard');
        }
        
        $db = JFactory::getDbo();
        $item = $this->get('Data');
        $lists = array();
        //publish
        if (property_exists($item, 'published'))
            $lists['published'] = JHtml::_('Openselect.booleanlist', 'published', ' class="inputbox" ', isset($item->published) ? $item->published : 1);
        //access
        if (property_exists($item, 'access'))
            $lists['access_menu'] = JHtml::_('Openselect.booleanlist', 'access', ' class="inputbox" ', isset($item->access) ? $item->access : 1);


        if (isset($item->access)) {
            if (version_compare(JVERSION, '1.6.0', 'ge')) {
                $lists['access'] = JHtml::_('access.level', 'access', $item->access, 'class="inputbox"', false);
            } else {
                $sql = 'SELECT id AS value, name AS text' . ' FROM #__groups' . ' ORDER BY id';
                $db->setQuery($sql);
                $groups = $db->loadObjectList();
                $lists['access'] = JHtml::_('select.genericlist', $groups, 'access', 'class="inputbox" ', 'value', 'text', $item->access);
            }
        }
        if ($this->get('translatable')) {
            if (JLanguageMultilang::isEnabled()) {
                $this->languages = OpenShopHelper::getLanguages();
                $this->languageData = OpenShopHelper::getLanguageData();
            }
            $this->translatable = true;
        } else {
            $this->translatable = false;
        }
        $this->_buildListArray($lists, $item);
        $this->assignRef('item', $item);
        $this->assignRef('lists', $lists);

        $this->_buildToolbar();

        parent::display($tpl);
    }

    /**
     * Build all the lists items used in the form and store it into the array
     * @param  $lists
     * @return boolean
     */
    public function _buildListArray(&$lists, $item) {

        return true;
    }

    /**
     * Build the toolbar for view list
     */
    public function _buildToolbar() {
        $viewName = $this->getName();
        $edit = JRequest::getVar('edit');
        $text = $edit ? JText::_($this->lang_prefix . '_EDIT') : JText::_($this->lang_prefix . '_NEW');
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . $viewName) . ': <small><small class="title-product">[ ' . $text . ' ]</small></small>');
        if (OpenShopHelper::getPermissionAction(OpenShopInflector::pluralize($this->getName()), 'create') || OpenShopHelper::getPermissionAction(OpenShopInflector::pluralize($this->getName()), 'edit')) {
            JToolBarHelper::apply($viewName . '.apply');
            JToolBarHelper::save($viewName . '.save');
        }
        if (OpenShopHelper::getPermissionAction(OpenShopInflector::pluralize($this->getName()), 'create') && OpenShopHelper::getPermissionAction(OpenShopInflector::pluralize($this->getName()), 'edit')) {
            JToolBarHelper::save2new($viewName . '.save2new');
        }
        if ($edit)
            JToolBarHelper::cancel($viewName . '.cancel', 'JTOOLBAR_CLOSE');
        else
            JToolBarHelper::cancel($viewName . '.cancel');
    }

}
