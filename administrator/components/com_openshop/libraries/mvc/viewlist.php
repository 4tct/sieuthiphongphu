<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopViewList extends JViewLegacy {

    public $lang_prefix = OPENSHOP_LANG_PREFIX;
    protected static $dropDownList = array();

    public function display($tpl = null) {
        // Check access first
        $mainframe = JFactory::getApplication();
        OpenShopPermissions::CheckPermission();
      
        $state = $this->get('State');
        $items = $this->get('Data');
        $pagination = $this->get('Pagination');

        $this->state = $state;

        $lists = array();
        $lists['order_Dir'] = $state->filter_order_Dir;
        $lists['order'] = $state->filter_order;
        $lists['filter_state'] = JHtml::_('grid.state', $state->filter_state, JText::_('OPENSHOP_PUBLISHED'), JText::_('OPENSHOP_UNPUBLISHED'));
        $lists['filter_parent'] = JHtml::_('grid.state', 's', JText::_('OPENSHOP_PUBLISHED_1'), JText::_('OPENSHOP_UNPUBLISHED'));
        $this->_buildListArray($lists, $state);
        $this->assignRef('lists', $lists);
        $this->assignRef('items', $items);
        $this->assignRef('pagination', $pagination);
        
        $this->_buildToolbar();

        parent::display($tpl);
    }

    /**
     * Build all the lists items used in the form and store it into the array
     * @param  $lists
     * @return boolean
     */
    public function _buildListArray(&$lists, $state) {
        return true;
    }

    /*     * 2
     * Build the toolbar for view list
     */

    public function _buildToolbar() {
        $viewName = $this->getName();
        $controller = OpenShopInflector::singularize($this->getName());
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
        
        if (OpenShopHelper::getPermissionAction($viewName, 'create')) {
            JToolBarHelper::addNew($controller . '.add');
        }
        if (OpenShopHelper::getPermissionAction($viewName, 'edit')){
            JToolBarHelper::editList($controller . '.edit');
        }
        if (OpenShopHelper::getPermissionAction($viewName, 'create')) {
            JToolBarHelper::custom($controller . '.copy', 'copy.png', 'copy_f2.png', JText::_('OPENSHOP_COPY'), true);
        }
        if (OpenShopHelper::getPermissionAction($viewName, 'delete')){
            JToolBarHelper::deleteList(JText::_($this->lang_prefix . '_DELETE_' . strtoupper($this->getName()) . '_CONFIRM'), $controller . '.remove');
        }
        if (OpenShopHelper::getPermissionAction($viewName, 'editstate')) {
            JToolBarHelper::publishList($controller . '.publish');
            JToolBarHelper::unpublishList($controller . '.unpublish');
        }
    }

    public static function renderDropdownList($item = '') {
        $html = array();
        $html[] = '<button data-toggle="dropdown" class="dropdown-toggle btn btn-micro">';
        $html[] = '<span class="caret"></span>';
        if ($item) {
            $html[] = '<span class="element-invisible">' . JText::sprintf('JACTIONS', $item) . '</span>';
        }
        $html[] = '</button>';
        $html[] = '<ul class="dropdown-menu">';
        $html[] = implode('', self::$dropDownList);
        $html[] = '</ul>';
        self::$dropDownList = null;
        return implode('', $html);
    }

    public static function addDropdownList($label, $icon = '', $i = '', $task = '') {
        self::$dropDownList[] = '<li>'
                . '<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\'' . $task . '\')">'
                . ($icon ? '<span class="icon-' . $icon . '"></span> ' : '')
                . $label
                . '</a>'
                . '</li>';
    }

}
