<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopView extends JViewLegacy
{
    public function __construct($config)
    {
        $paths =  array();
        $paths[] =  JPATH_COMPONENT.DS.'themes'.DS.'default'.DS.'views'.DS.$this->getName();
        $theme = OpenShopHelper::getConfigValue('theme');
        if ($theme != 'default')
        {
        	$paths[] =  JPATH_COMPONENT.DS.'themes'.DS.$theme.DS.'views'.DS.$this->getName();
        }
        $config['template_path'] = $paths;
        parent::__construct($config);
    }
}