<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JToolBarHelper::title(JText::_('OPENSHOP_DASHBOARD'), 'generic.png');
?>

<?php
$user = JFactory::getUser();
if (OpenShopPermissions::checkSuperUser($user->groups) == 'a') {
    ?>
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-clock-o"></i> <?php echo JText::_('OPENSHOP_TOTAL_VIEW_WEBSITE_TODAY'); ?></span>
            <div onclick="location.href = 'index.php?option=com_openshop&view=statistics&layout=viewweb'" class="count total_pr" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('OPENSHOP_TOTAL_VIEW_WEBSITE_TODAY'); ?>">
                <?php echo $this->totleViewWeb['totalView'] ?>
            </div>
            <span class="count_bottom">Đang thống kê...</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> <?php echo JText::_('OPENSHOP_TOTAL_CUS_TODAY'); ?></span>
            <div class="count" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('OPENSHOP_TOTAL_CUS_TODAY'); ?>"><?php echo $this->totleViewWeb['totalCusView']; ?></div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-shopping-cart"></i> <?php echo JText::_('OPENSHOP_TOTAL_ORDER_WEBSITE_TODAY'); ?></span>
            <div class="count green" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('OPENSHOP_TOTAL_ORDER_WEBSITE_TODAY'); ?>"><?php echo $this->totleOrder ?></div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> <?php echo JText::_('OPENSHOP_TOTAL_PRODUCTS'); ?></span>
            <div class="count" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('OPENSHOP_TOTAL_PRODUCTS'); ?>"><?php echo $this->totalProduct ?></div>
            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> <?php echo JText::_('OPENSHOP_TOTAL_CATEGORIES'); ?></span>
            <div class="count" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('OPENSHOP_TOTAL_CATEGORIES'); ?>"><?php echo $this->totalCategory ?></div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> <?php echo JText::_('OPENSHOP_TOTAL_USERS'); ?></span>
            <div class="count" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('OPENSHOP_TOTAL_USERS'); ?>"><?php echo $this->totalUser ?></div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
        </div>
    </div>

    <?php
}
?>

<fieldset class="adminfs">
    <legend></legend>
    <table>
        <tr>
            <td>
                <div id="cpanel" class="span12">
                    <?php
                    $menus = $this->getMenuNames();
                    foreach ($menus as $menu) {
                        if (OpenShopHelper::getPermissionAction($menu->menu_view, 'view')) {
                            if (!empty($menu->menu_link)) {
                                $link = $menu->menu_link;
                            } else {
                                $link = 'index.php?option=' . $menu->com_name . '&view=' . $menu->menu_view;
                            }
                            $this->quickiconButton($link, $menu->menu_image, JText::_($menu->menu_name));
                        }
                    }
                    ?>
                </div>
            </td>
        </tr>
    </table>
</fieldset>

<!--
<div class="clearfix" style="margin-top:20px ">
    <div class="span6">
        <div class="bs-example bs-shop-statistics">
            <table class="table dashboard-table">
                <thead>
                    <tr>
                        <th style="width: 35%"><?php echo JText::_('OPENSHOP_SHOP_INFORMATION'); ?></th>
                        <th style="width: 35%"><?php echo JText::_('OPENSHOP_ORDERS'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=products'); ?>"><?php echo $this->shopStatistics['products'] . ' ' . JText::_('OPENSHOP_PRODUCTS'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=categories'); ?>"><?php echo $this->shopStatistics['categories'] . ' ' . JText::_('OPENSHOP_CATEGORIES'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=manufacturers'); ?>"><?php echo $this->shopStatistics['manufacturers'] . ' ' . JText::_('OPENSHOP_MANUFACTURERS'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=customers'); ?>"><?php echo $this->shopStatistics['customers'] . ' ' . JText::_('OPENSHOP_CUSTOMERS'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=reviews'); ?>"><?php echo $this->shopStatistics['reviews'] . ' ' . JText::_('OPENSHOP_REVIEWS'); ?></a>
                        </td>
                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=orders&order_status_id=8'); ?>"><?php echo $this->shopStatistics['pending_orders'] . ' ' . JText::_('OPENSHOP_PENDING'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=orders&order_status_id=9'); ?>"><?php echo $this->shopStatistics['processed_orders'] . ' ' . JText::_('OPENSHOP_PROCESSED'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=orders&order_status_id=4'); ?>"><?php echo $this->shopStatistics['complete_orders'] . ' ' . JText::_('OPENSHOP_COMPLETED'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=orders&order_status_id=13'); ?>"><?php echo $this->shopStatistics['shipped_orders'] . ' ' . JText::_('OPENSHOP_SHIPPED'); ?></a><br/>
                            <a href="<?php echo JRoute::_('index.php?option=com_openshop&view=orders&order_status_id=11'); ?>"><?php echo $this->shopStatistics['refunded_orders'] . ' ' . JText::_('OPENSHOP_REFUNDED'); ?></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="bs-example bs-recentorders">
            <table class="table dashboard-table">
                <thead>
                    <tr>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_ORDER_ID'); ?></th>
                        <th style="width: 20%"><?php echo JText::_('OPENSHOP_CUSTOMER'); ?></th>
                        <th style="width: 15%;"><?php echo JText::_('OPENSHOP_ORDER_STATUS'); ?></th>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_TOTAL'); ?></th>
                        <th style="width: 20%;"><?php echo JText::_('OPENSHOP_DATE_ADDED'); ?></th>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_ACTION'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (empty($this->recentOrders)) {
                        ?>
                        <tr>
                            <td colspan="6"><?php echo JText::_('OPENSHOP_NO_ORDERS'); ?></td>
                        </tr>
                        <?php
                    } else {
                        foreach ($this->recentOrders as $order) {
                            ?>
                            <tr>
                                <td><?php echo $order->id; ?></td>
                                <td><?php echo $order->firstname . ' ' . $order->lastname; ?></td>
                                <td><?php echo $order->orderstatus_name; ?></td>
                                <td><?php echo $this->currency->format($order->total, $order->currency_code, $order->currency_exchanged_value); ?></td>
                                <td>
                                    <?php
                                    if ($order->created_date != $this->nullDate)
                                        echo JHtml::_('date', $order->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y'));
                                    ?>
                                </td>
                                <td><a href="<?php echo JRoute::_('index.php?option=com_openshop&task=order.edit&cid[]=' . $order->id); ?>"><?php echo JText::_('OPENSHOP_VIEW'); ?></a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="bs-example bs-top-sales" style="width: 45%; float: left;">
            <table class="table dashboard-table">
                <thead>
                    <tr>
                        <th style="width: 70%;"><?php echo JText::_('OPENSHOP_PRODUCT'); ?></th>
                        <th style="width: 30%;"><?php echo JText::_('OPENSHOP_NUMBER_SALES'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (empty($this->topSales)) {
                        ?>
                        <tr>
                            <td colspan="3"><?php echo JText::_('OPENSHOP_NO_ORDERS'); ?></td>
                        </tr>
                        <?php
                    } else {
                        foreach ($this->topSales as $product) {
                            ?>
                            <tr>
                                <td><?php echo $product->product_name; ?></td>
                                <td><?php echo $product->sales; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="bs-example bs-top-hits" style="width: 45%; float: right;">
            <table class="table dashboard-table">
                <thead>
                    <tr>
                        <th style="width: 70%;"><?php echo JText::_('OPENSHOP_PRODUCT'); ?></th>
                        <th style="width: 30%;"><?php echo JText::_('OPENSHOP_NUMBER_HITS'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (empty($this->topHits)) {
                        ?>
                        <tr>
                            <td colspan="3"><?php echo JText::_('OPENSHOP_NO_PRODUCTS'); ?></td>
                        </tr>
                        <?php
                    } else {
                        foreach ($this->topHits as $product) {
                            ?>
                            <tr>
                                <td><?php echo $product->product_name; ?></td>
                                <td><?php echo $product->hits; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="span6" >
        <div class="bs-example bs-monthlyreport">
            <table class="table dashboard-table">
                <tbody>
                    <tr>
                        <td>
                            <?php
                            global $currentMonthOffset;
                            $currentMonthOffset = (int) date('m');
                            if (JRequest::getInt('month') != 0)
                                $currentMonthOffset = JRequest::getInt('month');
                            ?>
                            <script type="text/javascript" src="<?php echo JUri::root(); ?>administrator/components/com_openshop/assets/js/jquery.flot.min.js"></script>
                            <script type="text/javascript" src="<?php echo JUri::root(); ?>administrator/components/com_openshop/assets/js/jquery.flot.pie.min.js"></script>
                            <div class="monthly-stats">
                                <p>
                                    <?php
                                    if ($currentMonthOffset != date('m')) {
                                        ?><a href="index.php?option=com_openshop&amp;view=dashboard&amp;month=<?php echo $currentMonthOffset + 1; ?>" class="next"><?php echo JText::_('OPENSHOP_NEXT_MONTH'); ?></a>
                                        <?php
                                    }
                                    ?>
                                    <a href="index.php?option=com_openshop&amp;view=dashboard&amp;month=<?php echo $currentMonthOffset - 1; ?>" class="previous"><?php echo JText::_('OPENSHOP_PREVIOUS_MONTH'); ?></a>
                                </p>
                                <div class="inside">
                                    <div id="placeholder" style="width:100%; height:300px; position:relative;"></div>
                                    <script type="text/javascript">
                /* <![CDATA[ */
                jQuery(function () {
                    function weekendAreas(axes)
                    {
                        var markings = [];
                        var d = new Date(axes.xaxis.min);
                        // go to the first Saturday
                        d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7));
                        d.setUTCSeconds(0);
                        d.setUTCMinutes(0);
                        d.setUTCHours(0);
                        var i = d.getTime();
                        do
                        {
                            // when we don't set yaxis, the rectangle automatically
                            // extends to infinity upwards and downwards
                            markings.push({xaxis: {from: i, to: i + 2 * 24 * 60 * 60 * 1000}});
                            i += 7 * 24 * 60 * 60 * 1000;
                        } while (i < axes.xaxis.max);
                        return markings;
                    }
<?php
global $currentMonthOffset;
$month = $currentMonthOffset;
$year = (int) date('Y');
$firstDay = strtotime("{$year}-{$month}-01");
$lastDay = strtotime('-1 second', strtotime('+1 month', $firstDay));
$after = date('Y-m-d H:i:s', $firstDay);
$before = date('Y-m-d H:i:s', $lastDay);
$orders = $this->model->getMonthlyReport($currentMonthOffset, $before, $after);
$orderCounts = array();
$orderAmounts = array();
// Blank date ranges to begin
$month = $currentMonthOffset;
$year = (int) date('Y');
$firstDay = strtotime("{$year}-{$month}-01");
$lastDay = strtotime('-1 second', strtotime('+1 month', $firstDay));
if ((date('m') - $currentMonthOffset) == 0) :
    $upTo = date('d', strtotime('NOW'));
else :
    $upTo = date('d', $lastDay);
endif;
$count = 0;
while ($count < $upTo) {
    $time = strtotime(date('Ymd', strtotime('+ ' . $count . ' DAY', $firstDay))) . '000';
    $orderCounts[$time] = 0;
    $orderAmounts[$time] = 0;
    $count++;
}
if ($orders) {
    foreach ($orders as $order) {
        $time = strtotime(date('Ymd', strtotime($order->created_date))) . '000';
        if (isset($orderCounts[$time])) {
            $orderCounts[$time] ++;
        } else {
            $orderCounts[$time] = 1;
        }
        if (isset($orderAmounts[$time])) {
            $orderAmounts[$time] = $orderAmounts[$time] + $order->total;
        } else {
            $orderAmounts[$time] = (float) $order->total;
        }
    }
}
?>
                    var d = [
<?php
$values = array();
foreach ($orderCounts as $key => $value) {
    $values[] = "[$key, $value]";
}
echo implode(',', $values);
?>
                    ];
                    for (var i = 0; i < d.length; ++i)
                        d[i][0] += 60 * 60 * 1000;
                    var d2 = [
<?php
$values = array();
foreach ($orderAmounts as $key => $value) {
    $values[] = "[$key, $value]";
}
echo implode(',', $values);
?>
                    ];
                    for (var i = 0; i < d2.length; ++i)
                        d2[i][0] += 60 * 60 * 1000;
                    var plot = jQuery.plot(jQuery("#placeholder"), [
                        {label: "<?php echo JText::_('OPENSHOP_NUMBER_OF_SALES'); ?>", data: d},
                        {label: "<?php echo JText::_('OPENSHOP_SALES_AMOUNT'); ?>", data: d2, yaxis: 2}
                    ], {
                        series: {
                            lines: {show: true},
                            points: {show: true}
                        },
                        grid: {
                            show: true,
                            aboveData: false,
                            color: '#ccc',
                            backgroundColor: '#fff',
                            borderWidth: 2,
                            borderColor: '#ccc',
                            clickable: false,
                            hoverable: true,
                            markings: weekendAreas
                        },
                        xaxis: {
                            mode: "time",
                            timeformat: "%d %b",
                            tickLength: 1,
                            minTickSize: [1, "day"]
                        },
                        yaxes: [
                            {min: 0, tickSize: 1, tickDecimals: 0},
                            {position: "right", min: 0, tickDecimals: 2}
                        ],
                        colors: ["#21759B", "#ed8432"]
                    });
                    function showTooltip(x, y, contents) {
                        jQuery('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 5,
                            border: '1px solid #fdd',
                            padding: '2px',
                            'background-color': '#fee',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                    }
                    var previousPoint = null;
                    jQuery("#placeholder").bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;
                                jQuery("#tooltip").remove();
                                if (item.series.label == "<?php echo JText::_('OPENSHOP_NUMBER_OF_SALES', 'jigoshop'); ?>") {
                                    var y = item.datapoint[1];
                                    showTooltip(item.pageX, item.pageY, item.series.label + " - " + y);
                                } else {
                                    var y = item.datapoint[1].toFixed(2);
                                    showTooltip(item.pageX, item.pageY, item.series.label + " - <?php echo $this->defaultCurrency->left_symbol; ?>" + y + "<?php echo $this->defaultCurrency->right_symbol; ?>");
                                }
                            }
                        } else {
                            jQuery("#tooltip").remove();
                            previousPoint = null;
                        }
                    });
                });
                /* ]]> */
                                    </script>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="bs-example bs-recentreviews">
            <table class="table dashboard-table">
                <thead>
                    <tr>
                        <th style="width: 10%; "><?php echo JText::_('OPENSHOP_ID'); ?></th>
                        <th style="width: 20%;"><?php echo JText::_('OPENSHOP_AUTHOR'); ?></th>
                        <th style="width: 15%;"><?php echo JText::_('OPENSHOP_RATING'); ?></th>
                        <th style="width: 10%"><?php echo JText::_('OPENSHOP_REVIEW_STATUS'); ?></th>
                        <th style="width: 20%;"><?php echo JText::_('OPENSHOP_DATE_ADDED'); ?></th>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_ACTION'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (empty($this->recentReviews)) {
                        ?>
                        <tr>
                            <td colspan="6"><?php echo JText::_('OPENSHOP_NO_REVIEWS'); ?></td>
                        </tr>
                        <?php
                    } else {
                        foreach ($this->recentReviews as $review) {
                            ?>
                            <tr>
                                <td><?php echo $review->id; ?></td>
                                <td><?php echo $review->author; ?></td>
                                <td><?php echo $review->rating; ?></td>
                                <td><span class="icon-<?php echo ($review->published) ? 'publish' : 'unpublish'; ?>"></span></td>
                                <td>
                                    <?php
                                    if ($review->created_date != $this->nullDate)
                                        echo JHtml::_('date', $review->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y'));
                                    ?>
                                </td>
                                <td><a href="<?php echo JRoute::_('index.php?option=com_openshop&task=review.edit&cid[]=' . $review->id); ?>"><?php echo JText::_('OPENSHOP_VIEW'); ?></a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

-->
