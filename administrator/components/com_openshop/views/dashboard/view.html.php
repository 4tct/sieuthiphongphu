<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewDashboard extends JViewLegacy {

    public function display($tpl = null) {
        
        OpenShopPermissions::CheckPermission();
        
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $currency = new OpenShopCurrency();
        $defaultCurrencyCode = OpenShopHelper::getConfigValue('default_currency_code');
        $query->select('*')
                ->from('#__openshop_currencies')
                ->where('currency_code = ' . $db->quote($defaultCurrencyCode));
        $db->setQuery($query);
        $defaultCurrency = $db->loadObject();
        $this->shopStatistics = $this->get('ShopStatistics');
        $this->recentOrders = $this->get('RecentOrders');
        $this->recentReviews = $this->get('RecentReviews');
        $this->topSales = $this->get('TopSales');
        $this->topHits = $this->get('TopHits');
        $this->currency = $currency;
        $this->defaultCurrency = $defaultCurrency;
        $nullDate = $db->getNullDate();
        $this->nullDate = $nullDate;
        $this->model = $this->getModel();
        $this->totleViewWeb = $this->get('totleViewWeb');
        $this->totleOrder = $this->get('totleOrder');
        $this->totalUser = $this->get('totalUser');
        $this->totalProduct = $this->get('totalProduct');
        $this->totalCategory = $this->get('totalCategory');
        parent::display($tpl);
    }

    /**
     * 
     * Function to create the buttons view.
     * @param string $link targeturl
     * @param string $image path to image
     * @param string $text image description
     */
    function quickiconButton($link, $image, $text) {
        $language = JFactory::getLanguage();
        ?>
        <div style="float:left">
            <div class="icon">
                <a href="<?php echo $link; ?>" title="<?php echo $text; ?>">
                    <img src="<?php echo OPENSHOP_ADMIN_ICON.DS.$image?>" alt="<?php echo $text;?>"/>
                    <span><?php echo $text; ?></span>
                </a>
            </div>
        </div>
        <?php
    }
    function getMenuNames(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*,b.element as com_name')
                ->from('#__openshop_menus as a, #__extensions as b')
                ->where('a.menu_com = b.extension_id')
                ->where('b.element="com_openshop"')
                ->where('a.menu_parent_id >0')
                ->where('a.published =1')
                ->order('ordering');
           
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

}
