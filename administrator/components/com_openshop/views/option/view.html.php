<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewOption extends OpenShopViewForm
{

	function _buildListArray(&$lists, $item)
	{
		$options = array();
		$options[] = JHtml::_('select.option', 'Select', 'Select');
		$options[] = JHtml::_('select.option', 'Radio', 'Radio');
		$options[] = JHtml::_('select.option', 'Checkbox', 'Checkbox');
		$options[] = JHtml::_('select.option', 'Text', 'Text');
		$options[] = JHtml::_('select.option', 'Textarea', 'Textarea');
		$options[] = JHtml::_('select.option', 'File', 'File');
		$options[] = JHtml::_('select.option', 'Date', 'Date');
		$options[] = JHtml::_('select.option', 'Datetime', 'Datetime');
		$lists['option_type'] = JHtml::_('select.genericlist', $options, 'option_type', 
			array(
				'option.text.toHtml' => false, 
				'option.text' => 'text', 
				'option.value' => 'value', 
				'list.attr' => ' class="inputbox" ', 
				'list.select' => $item->option_type));
		if ($item->id)
		{
			$lists['option_values'] = OpenShopHelper::getOptionValues($item->id);
		}
		else 
		{
			$lists['option_values'] = array();
		}
		parent::_buildListArray($lists, $item);
	}
}