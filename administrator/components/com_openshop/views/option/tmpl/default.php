<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'options.js');
?>
<style>
    .delete_image{
        color: red;
        margin: 0 0 0 4px;
        vertical-align: middle;
        cursor: pointer;
    }
    .input-option{
        margin-bottom: 0px !important;
    }
</style>


<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'option.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
<?php
if ($translatable) {
    foreach ($this->languages as $language) {
        $langId = $language->lang_id;
        ?>
                    if (document.getElementById('option_name_<?php echo $langId; ?>').value == '') {
                        alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                        document.getElementById('option_name_<?php echo $langId; ?>').focus();
                        return;
                    }
        <?php
    }
} else {
    ?>
                if (form.option_name.value == '') {
                    alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                    form.option_name.focus();
                    return;
                }
    <?php
}
?>
            Joomla.submitform(pressbutton, form);
        }
    }
    var countOptionValues = '<?php echo count($this->lists['option_values']); ?>';
    function addOptionValue() {
        var i_temp = jQuery('#max_value').val();
        var html = '<tr id="option_value_' + countOptionValues + '">'
        // Option Value column
        html += '<td style="text-align: center; vertical-align: middle;">';
<?php
if ($translatable) {
    foreach ($this->languages as $language) {
        $langCode = $language->lang_code;
        ?>
                html += '<input class="input-large" type="text" name="value_<?php echo $langCode; ?>[]" maxlength="255" value="" />';
                html += '<img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" /><br />';
        <?php
    }
} else {
    ?>
            html += '<input class="input-large" type="text" name="value[]" maxlength="255" value="" />';
    <?php
    if ($this->item->option_name == 'COLOR') {
        ?>
                html += '<input type="file" name="img_color_' + i_temp + '" />';
        <?php
    }
}
?>
        html += '</td>';
        // Ordering column
        html += '<td style="text-align: center; vertical-align: middle;"><input class="input-small" type="text" name="ordering[]" maxlength="10" value="" /></td>';
        // Published column
        html += '<td style="text-align: center; vertical-align: middle;"><select class="inputbox" name="optionvalue_published[]" id="published">';
        html += '<option selected="selected" value="1"><?php echo JText::_('OPENSHOP_YES'); ?></option>';
        html += '<option value="0"><?php echo JText::_('OPENSHOP_NO'); ?></option>';
        html += '</select></td>';
        // Remove button column
        html += '<td style="text-align: center; vertical-align: middle;"><input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" onclick="removeOptionValue(' + countOptionValues + ');" /></td>';
        html += '</tr>';
        jQuery('#option_values_area').append(html);
        countOptionValues++;
        jQuery('#max_value').val(++i_temp);
    }
    function removeOptionValue(rowIndex) {
        jQuery('#option_value_' + rowIndex).remove();
    }
</script>
<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <div class="row-fluid">
            <div class="span6">
                <fieldset class="admintable">
                    <legend><?php echo JText::_('OPENSHOP_OPTION_DETAILS'); ?></legend>
                    <table class="admintable adminform" style="width: 100%;">
                        <tr>
                            <td class="key">
                                <span class="required">*</span>
                                <?php echo JText::_('OPENSHOP_NAME'); ?>
                            </td>
                            <td>
                                <?php
                                if ($translatable) {
                                    foreach ($this->languages as $language) {
                                        $langId = $language->lang_id;
                                        $langCode = $language->lang_code;
                                        ?>
                                        <input class="input-xlarge" type="text" name="option_name_<?php echo $langCode; ?>" id="option_name_<?php echo $langId; ?>" size="" maxlength="255" value="<?php echo isset($this->item->{'option_name_' . $langCode}) ? $this->item->{'option_name_' . $langCode} : ''; ?>" />
                                        <img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" />
                                        <br />
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <input class="input-xlarge" type="text" name="option_name" id="option_name" maxlength="255" value="<?php echo $this->item->option_name; ?>" />
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="key">
                                <?php echo JText::_('OPENSHOP_OPTION_TYPE'); ?>
                            </td>
                            <td>
                                <?php echo $this->lists['option_type']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td  class="key">
                                <?php echo JText::_('OPENSHOP_OPTION_IMAGE'); ?>
                            </td>
                            <td>
                                <input type="file" class="input-large" accept="image/*" name="option_image" /><br />
                                <?php
                                if (JFile::exists(JPATH_ROOT . '/media/com_openshop/options/' . $this->item->option_image)) {
                                    $viewImage = JFile::stripExt($this->item->option_image) . '-100x100.' . JFile::getExt($this->item->option_image);
                                    if (Jfile::exists(JPATH_ROOT . '/media/com_openshop/options/resized/' . $viewImage)) {
                                        ?>
                                        <img src="<?php echo JURI::root() . 'media/com_openshop/options/resized/' . $viewImage; ?>" />
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo JURI::root() . 'media/com_openshop/options/' . $this->item->option_image; ?>" height="100" />
                                        <?php
                                    }
                                    ?>
                                    <label class="checkbox">
                                        <input type="checkbox" name="remove_image" value="1" />
                                        <?php echo JText::_('OPENSHOP_REMOVE_IMAGE'); ?>
                                    </label>
                                    <?php
                                }
                                ?>
                            </td>				
                        </tr>
                        <tr>
                            <td class="key" valign="top">
                                <?php echo JText::_('OPENSHOP_OPTION_DESCRIPTION'); ?>
                            </td>
                            <td>
                                <?php
                                if ($translatable) {
                                    ?>
                                    <div class="row-fluid">
                                        <?php
                                        foreach ($this->languages as $language) {
                                            $langId = $language->lang_id;
                                            $langCode = $language->lang_code;
                                            ?>
                                            <div class="span12">
                                                <img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" />
                                                <?php
                                                echo $editor->display('option_desc_' . $langCode, isset($this->item->{'option_desc_' . $langCode}) ? $this->item->{'option_desc_' . $langCode} : '', '80%', '250', '75', '10');
                                                ?>
                                            </div>
                                            <br />
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                } else {
                                    echo $editor->display('option_desc', $this->item->option_desc, '80%', '250', '75', '10');
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="key">
                                <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                            </td>
                            <td>
                                <?php echo $this->lists['published']; ?>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div class="span6">
                <fieldset class="admintable">
                    <legend><?php echo JText::_('OPENSHOP_OPTION_VALUES'); ?></legend>
                    <table class="adminlist table table-bordered" style="text-align: center;">
                        <thead>
                            <tr>
                                <th class="title" width="50%"><?php echo JText::_('OPENSHOP_OPTION_VALUE'); ?></th>
                                <th class="title" width="20%"><?php echo JText::_('OPENSHOP_ORDERING'); ?></th>
                                <th class="title" width="15%"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></th>
                                <th class="title" width="15%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="option_values_area">
                            <?php
                            $options = array();
                            $options[] = JHtml::_('select.option', '1', Jtext::_('OPENSHOP_YES'));
                            $options[] = JHtml::_('select.option', '0', Jtext::_('OPENSHOP_NO'));
                            $optionValues = $this->lists['option_values'];
                            for ($i = 0; $n = count($optionValues), $i < $n; $i++) {
                                $optionValue = $optionValues[$i];
                                ?>
                                <tr id="option_value_<?php echo $i; ?>">
                                    <td style="text-align: center; vertical-align: middle;">
                                        <?php
                                        if ($translatable) {
                                            foreach ($this->languages as $language) {
                                                $langCode = $language->lang_code;
                                                ?>
                                                <input class="input-large" type="text" name="value_<?php echo $langCode; ?>[]" maxlength="255" value="<?php echo isset($optionValue->{'value_' . $langCode}) ? $optionValue->{'value_' . $langCode} : ''; ?>" />
                                                <img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" />
                                                <input type="hidden" class="inputbox" name="optionvaluedetails_id_<?php echo $langCode; ?>[]" value="<?php echo isset($optionValue->{'optionvaluedetails_id_' . $langCode}) ? $optionValue->{'optionvaluedetails_id_' . $langCode} : ''; ?>" />
                                                <br />
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <input class="input-large input-option" type="text" name="value[]" maxlength="255" value="<?php echo $optionValue->value; ?>" />
                                            <input type="hidden" class="inputbox" name="optionvaluedetails_id[]" value="<?php echo $optionValue->optionvaluedetails_id; ?>" />
                                            <?php
                                        }
                                        ?>
                                        <input type="hidden" class="inputbox" name="optionvalue_id[]" value="<?php echo $optionValue->id; ?>" />


                                        <?php
                                        if ($this->item->option_name == 'COLOR' && !empty($optionValue->img_color) && JFile::exists(OPENSHOP_PATH_IMG_COLOR . $optionValue->img_color)) {
                                            ?>
                                            <span class="show_image_color_<?php echo $i; ?>">
                                                <img src="<?php echo OPENSHOP_PATH_IMG_COLOR_HTTP . $optionValue->img_color; ?>" alt="<?php $optionValue->img_color ?>" width="35px" />
                                                <span class="icon-delete delete_image" id="delete_image" onclick="delete_image_option(<?php echo $i ?>, <?php echo $optionValue->optionvaluedetails_id ?>, '<?php echo $optionValue->img_color; ?>');" title="<?php echo JText::_('OPENSHOP_DELETE_IMAGE_OPTION_TITLE') ?>"></span>
                                            </span>
                                            <?php
                                        } else if ($this->item->option_name == 'COLOR') {
                                            ?>
                                            <input type="file" name="img_color_<?php echo $i; ?>" />
                                            <?php
                                        }
                                        ?>

                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <input class="input-small" type="text" name="ordering[]" maxlength="10" value="<?php echo $optionValue->ordering; ?>" />
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <?php echo JHtml::_('select.genericlist', $options, 'optionvalue_published[]', ' class="inputbox"', 'value', 'text', $optionValue->published); ?>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" onclick="removeOptionValue(<?php echo $i; ?>);" />
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">
                                    <input type="button" class="btn btn-small btn-primary" name="btnAdd" value="<?php echo JText::_('OPENSHOP_BTN_ADD'); ?>" onclick="addOptionValue();" />
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
        <?php
        if ($translatable) {
            foreach ($this->languages as $language) {
                $langCode = $language->lang_code;
                ?>
                <input type="hidden" name="details_id_<?php echo $langCode; ?>" value="<?php echo isset($this->item->{'details_id_' . $langCode}) ? $this->item->{'details_id_' . $langCode} : ''; ?>" />
                <?php
            }
        } elseif ($this->translatable) {
            ?>
            <input type="hidden" name="details_id" value="<?php echo isset($this->item->{'details_id'}) ? $this->item->{'details_id'} : ''; ?>" />
            <?php
        }
        ?>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="max_value" id="max_value" value="<?php echo count($optionValues); ?>"
    </form>
</fieldset>