<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'media.js');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'media.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
            Joomla.submitform(pressbutton, form);
        }
    }
</script>

<style>
    .btn-media{
        text-align: center;
        margin: 20px 0px 0 0;
    }
    .btn-media span{
        padding: 10px 50px;
        background: #146514;
        color: white;
        cursor: pointer;
    }
    .btn-media span:hover{
        background: #5CB85C;
        box-shadow: 0 2px 10px 0px black;
    }
    .rows_media .icon-delete{
        cursor: pointer;
    }
</style>


<form action="index.php" method="post" name="adminForm" class="adminfs" id="adminForm" enctype="multipart/form-data">
    <div class="row-fluild">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo JText::_('OPENSHOP_MEDIA_TITLE') ?></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3"><?php echo JText::_('OPENSHOP_MEDIA_NAME') ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="media_name" value="<?php echo $this->item->media_name ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3"><?php echo JText::_('OPENSHOP_MEDIA_TYPE') ?></label>
                        <div class="col-md-9">
                            <select class="form-control" name="media_type" id="media_type" onchange="choose_media_type(this.value)">
                                <?php
                                $selected1 = '';
                                $selected2 = '';
                                if ($this->item->media_type == 1) {
                                    $selected1 = 'selected';
                                } elseif ($this->item->media_type == 2) {
                                    $selected2 = 'selected';
                                }
                                ?>
                                <option value="1" <?php echo $selected1 ?>><?php echo JText::_('OPENSHOP_MEDIA_TYPE_PHOTO_VIDEO') ?></option>
                                <option value="2" <?php echo $selected2 ?>><?php echo JText::_('OPENSHOP_MEDIA_TYPE_FILE') ?></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--//PHOTOS & VIDEOS & FILES-->
        <div class="form-group col-md-7">
            <div class="panel panel-default">
                <?php
                if ($this->item->media_type == 1) {
                    $title = JText::_('OPENSHOP_PHOTO_VIDEO_TITLE');
                } else if ($this->item->media_type == 2) {
                    $title = JText::_('OPENSHOP_FILES_TITLE');
                } else {
                    $title = JText::_('OPENSHOP_PHOTO_VIDEO_TITLE');
                }
                ?>
                <div class="panel-heading media_type_title"><?php echo $title ?></div>
                <div class="panel-body">
                    <table class="table">
                        <tbody class="rows_media">
                            <?php
                            if (isset($this->item->id)) {
                                $mediaFiles = $this->getMediaFiles($this->item->id);
                                $num_media = count($mediaFiles);
                                foreach ($mediaFiles as $key => $value) {
                                    $num = ($key + 1);
                                    echo '<tr class="row_' . $num . '">';
                                    echo '  <td style="vertical-align: middle;text-align:center;" width="90%">';
                                    if ($this->item->media_type == '1') {
                                        echo '      <img src="' . OPENSHOP_PATH_IMG_MEDIA_HTTP . $value->media_image . '" style="width: 500px;margin: 0 0 10px 0;"/>';
                                        echo '      <input type="hidden" id="mediadetail_id_' . $num . '" value="' . $value->id . '"';
                                    } else if ($this->item->media_type == '2') {
                                        echo '      <img src="' . OPENSHOP_PATH_IMG_MEDIA_HTTP . $value->media_image . '" style="width: 200px;margin: 0 0 10px 0;"/>';
                                        echo '      <a href="' . OPENSHOP_PATH_IMG_MEDIA_HTTP . $value->media_file . '" style="margin-left: 20px;"><i class="icon-download"></i>Download</a>';
                                        echo '      <br><span>' . $value->media_detail . '</span>';
                                        echo '      <input type="hidden" id="mediadetail_id_' . $num . '" value="' . $value->id . '"';
                                    }
                                    echo '  </td>';
                                    echo '  <td style="vertical-align: middle;text-align:center;">';
                                    echo '      <span class="icon-delete delete_media_' . $num . '" style="color:red;" onclick="deleteRowMedia(' . $num . ')"></span>';
                                    echo '  </td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                    <div class="btn-media">
                        <span onclick="add_media_file()"><?php echo JText::_('OPENSHOP_ADD') ?></span>
                        <input type="hidden" name="num_row_media" id="mun_row_media" value="<?php echo isset($num_media) ? $num_media : '0' ?>" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php echo JHtml::_('form.token'); ?>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
    <input type="hidden" name="task" value="" />
</form>