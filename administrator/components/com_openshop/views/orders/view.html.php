<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewOrders extends OpenShopViewList {

    function _buildListArray(&$lists, $state) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, b.orderstatus_name AS text')
                ->from('#__openshop_orderstatuses AS a')
                ->innerJoin('#__openshop_orderstatusdetails AS b ON (a.id = b.orderstatus_id)')
                ->where('a.published = 1');
//                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
        $db->setQuery($query);
        $options = array();
        $rows = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDERSSTATUS_ALL'));
        foreach ($db->loadObjectList() as $value) {
            $rows[] = JHtml::_('select.option', $value->value, JText::_('OPENSHOP_STATUS_ORDER_' . strtoupper($value->text)));
        }
        $options = array_merge($options, $rows);
        $lists['order_status_id'] = JHtml::_('select.genericlist', $options, 'order_status_id', ' class="inputbox" style="width: 150px;" onchange="this.form.submit();"', 'value', 'text', JRequest::getInt('order_status_id'));

        //get reason delete order
        $query->clear();
        $query->select('config_value')->from($db->quoteName('#__openshop_configs'))->where('config_key = "reason_delete_order"');
        $reason_id = $db->setQuery($query)->loadResult();
        $query->clear();
        $query->select('b.id as value, b.reason_detail_description as text')
                ->from($db->quoteName('#__openshop_reasons', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_reasondetails', 'b') . 'ON a.id = b.reason_id')
                ->where('a.id = "' . $reason_id . '"');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_REASON_DELETE_ORDER_SELECT'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['reason_delete_order'] = JHtml::_('select.genericlist', $options, 'reason_delete_order', 'class="form-control reason_delete_order"', 'value', 'text', '0');

        //filter appointment
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_APPOINTMENT_FILTER'));
        $options[] = JHtml::_('select.option', '1', JText::_('OPENSHOP_STATUS_OFFICE'));
        $options[] = JHtml::_('select.option', '2', JText::_('OPENSHOP_STATUS_SHIP'));
        $options[] = JHtml::_('select.option', '3', JText::_('OPENSHOP_STATUS_SALES_SHOP'));
        $lists['appointments_filter'] = JHtml::_('select.genericlist', $options, 'appointment_filter', 'class="form-control" onchange="this.form.submit();"', 'value', 'text', isset($state->appointment_filter) ? $state->appointment_filter : '' );

        //filter transporter
        $query->clear();
        $query->select('id as value, transporter_name as text')
                ->from($db->quoteName('#__openshop_transporters'));
        $rows = $db->setQuery($query)->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_TRANSPORTER_FILTER'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['transporters_filter'] = JHtml::_('select.genericlist', $options, 'transporter_filter', 'class="form-control" onchange="this.form.submit();"', 'value', 'text', isset($state->transporter_filter) ? $state->transporter_filter : '' );

        $db = JFactory::getDbo();
        $nullDate = $db->getNullDate();
        $this->nullDate = $nullDate;
        $currency = new OpenShopCurrency();
        $this->currency = $currency;
    }

    /**
     * Override Build Toolbar function, only need Delete, Edit and Download Invoice
     */
    function _buildToolbar() {
        $app = JFactory::getApplication()->input;
        $layout = $app->getString('layout');
        switch ($layout) {
            case 'cartphone':
                JToolbarHelper::title(JText::_('OPENSHOP_APPOINMENT_CART_PHONE'));
                break;
            default :
                $viewName = $this->getName();
                $controller = OpenShopInflector::singularize($this->getName());
                JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
                if (OpenShopHelper::getPermissionAction($viewName, 'create')) {
                    JToolBarHelper::addNew($controller . '.add');
                }
                if (OpenShopHelper::getPermissionAction($viewName, 'delete')) {
                    JToolBarHelper::deleteList(JText::_($this->lang_prefix . '_DELETE_' . strtoupper($this->getName()) . '_CONFIRM'), $controller . '.remove');
                }
                if (OpenShopHelper::getPermissionAction($viewName, 'edit')) {
                    JToolBarHelper::editList($controller . '.edit');
                }
                if (OpenShopHelper::getConfigValue('invoice_enable')) {
                    JToolBarHelper::custom($controller . '.downloadInvoice', 'print', 'print', JText::_('OPENSHOP_DOWNLOAD_INVOICE'), true);
                }
                break;
        }
    }

    public function productListItem($i) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('ord.*, pro.product_image')
                ->from($db->quoteName('#__openshop_orderproducts', 'ord'))
                ->join('LEFT', $db->quoteName('#__openshop_products', 'pro') . 'ON ord.product_id = pro.id')
                ->where('ord.order_id = ' . $i);
        if (JFactory::getApplication()->input->getInt('order_status_id') == '1') {
            $query->where('order_product_delete = 1');
        } else {
            $query->where('order_product_delete = 0');
        }
        $db->setQuery($query);

        return $db->loadObjectList();
    }

    public function getSizeColor($i_pro, $id_cond, $cond, $manaSizeColor = null) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->clear();
        $query->select('distinct a.optionvalue_id as value, a.value as text')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'));
        $options = array();
        switch ($cond) {
            case 'size':
//                $query->join('LEFT', $db->quoteName('#__openshop_inventories', 'b') . 'ON a.optionvalue_id = b.idsize')
//                        ->where('b.product_id = ' . $i_pro);
                $query->where('a.optionvalue_id = ' . $id_cond);
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $t_id = 'product_size';
                $t_witdh = '50px';
                $disabled = '';
                break;
            case 'color':
//                $query->join('LEFT', $db->quoteName('#__openshop_inventories', 'b') . 'ON a.optionvalue_id = b.idcolor')
//                        ->where('b.product_id = ' . $i_pro);
                $query->where('a.optionvalue_id = ' . $id_cond);
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $t_id = 'product_color';
                $t_witdh = '70px';
                if (!empty($manaSizeColor) || isset($manaSizeColor)) {
                    $disabled = $manaSizeColor;
                } else {
                    $disabled = '';
                }
                break;
            default :
                break;
        }

        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" ' . $disabled . ' style="width: ' . $t_witdh . '; ', 'value', 'text', $id_cond);
        return $lists;
    }

    public function getWeight($id_cond, $cond) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('opd.value AS text, opd.optionvalue_id AS value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'optd') . 'ON opd.option_id = optd.option_id');

        $options = array();
        switch ($cond) {
            case 'weight':
                $query->where('optd.option_name = "WEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $t_id = 'optionweight_id';
                break;
            case 'height':
                $query->where('optd.option_name = "HEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $t_id = 'optionheight_id';
                break;
            default:
                break;
        }
        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" style="width: 120px; ', 'value', 'text', $id_cond);
        return $lists;
    }

    function getRowProductInOrder($id_order) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(True);
        $app = JFactory::getApplication()->input;
        $status_order = $app->getInt('order_status_id');
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('order_id = ' . $id_order);

        if ($status_order == 1) {
            $query->where('order_product_delete = 1');
        } else {
            $query->where('order_product_delete = 0');
        }
        $db->setQuery($query);
        return (int) ($db->loadResult());
    }

    function getDataCancel($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.reason_detail_description,a.reason_description,a.created_date')
                ->from($db->quoteName('#__openshop_orderreasons', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_reasondetails', 'b') . 'ON a.reasondetail_id = b.id')
                ->where('a.order_id = ' . $id);
        return $db->setQuery($query)->loadObject();
    }

    function getQuantityProductInWarehouse($id_product, $id_size, $id_color) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->clear();
        $query->select('id,user_group_manage')
                ->from($db->quoteName('#__openshop_warehouses'));
        $rows_warehouse = $db->setQuery($query)->loadObjectList();

        $usergroup_id = OpenShopHelper::getUsergroupId(JFactory::getUser()->id);
        $res_warehouse = array();
        foreach ($rows_warehouse as $v_id) {
            $arr = explode(',', $v_id->user_group_manage);
            foreach ($arr as $v_arr) {
                if ($v_arr == $usergroup_id) {
                    $res_warehouse[] = $v_id->id;
                    break;
                }
            }
        }

        $query->clear();
        $query->select('b.total_import_product')
                ->from($db->quoteName('#__openshop_inventories', 'b'))
                ->where('b.idsize = ' . $id_size)
                ->where('b.idcolor = ' . $id_color)
                ->where('b.warehouse_id = ' . $res_warehouse[0])
                ->where('b.product_id = ' . $id_product);
        $res = $db->setQuery($query)->loadResult();
        return $res == '' ? 0 : $res;
    }

    function checkOrderEnouch($id_order) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('product_id,product_size,product_color,quantity,quantity_priority')
                ->from($db->quoteName('#__openshop_orderproducts', 'a'))
                ->where('order_id = ' . $id_order);

        $rows_product = $db->setQuery($query)->loadObjectList();
        $res = 1;
        foreach ($rows_product as $value) {
            $id_p = $value->product_id;
            $size = $value->product_size;
            $color = $value->product_color;
            $quantity_order = $value->quantity;
            $query->clear();
            $query->select('total_import_product')
                    ->from($db->quoteName('#__openshop_inventories'))
                    ->where('idsize = ' . $size)
                    ->where('idcolor = ' . $color)
                    ->where('product_id = ' . $id_p);
            $quantity = $db->setQuery($query)->loadResult();    //quantity product in warehouse

            if (($quantity == 0 || $quantity_order > $quantity) && $quantity_order > ($quantity + $value->quantity_priority)) {
                $res = 0;
            }
        }

        return $res;
    }

    function checkStatusProduct($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('order_id = ' . $id)
                ->where('status_order != 1');       //1: canceled
        return $db->setQuery($query)->loadResult();
    }

    function getAllOrderThisPhone($phone) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.*,c.product_image,a.created_date')
                ->from($db->quoteName('#__openshop_orders', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_orderproducts', 'b') . 'ON a.id = b.order_id')
                ->join('INNER', $db->quoteName('#__openshop_products', 'c') . 'ON c.id = b.product_id')
                ->where('a.order_status_id = 8')
                ->where('a.payment_telephone = ' . $phone);
        $res = array();
        $res['noprocess'] = $db->setQuery($query)->loadObjectList();

        $query->clear();
        $query->select('b.*,c.product_image,a.created_date')
                ->from($db->quoteName('#__openshop_orders', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_orderproducts', 'b') . 'ON a.id = b.order_id')
                ->join('INNER', $db->quoteName('#__openshop_products', 'c') . 'ON c.id = b.product_id')
                ->where('a.order_status_id != 8')
                ->where('a.payment_telephone = ' . $phone);
        $res['process'] = $db->setQuery($query)->loadObjectList();
        return $res;
    }
    
    function getCustomerCartPhone($phone){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.address,b.zone_id,b.district_id')
                ->from($db->quoteName('#__openshop_customers','a'))
                ->join('INNER', $db->quoteName('#__openshop_addresses','b') . ' ON a.customer_id = b.customer_id')
                ->where('telephone = "'. $phone .'"' );
        return $db->setQuery($query)->loadObject();
    }
    
    function getTown($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('zone_name')
                ->from($db->quoteName('#__openshop_zones'))
                ->where('id = ' . $id);
        return $db->setQuery($query)->loadResult();
    }
    
    function getDistrict($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('district_name')
                ->from($db->quoteName('#__openshop_districts'))
                ->where('id = ' . $id);
        return $db->setQuery($query)->loadResult();
    }

}
