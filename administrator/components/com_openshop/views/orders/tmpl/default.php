<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addStyleSheet(JURI::base() . 'components/com_openshop/assets/css/order.css');
JFactory::getDocument()->addStyleSheet(JURI::base() . 'components/com_openshop/assets/css/io.css');
JFactory::getDocument()->addScript(JURI::base() . 'components/com_openshop/assets/js/order.js');
JFactory::getDocument()->addScript(JURI::base() . 'components/com_openshop/assets/js/openshop.js');
//print_r($this->items);
?>
<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=orders" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <?php
                    $filter = array(
                        'search', 'transporter_filter:0', 'appointment_filter:0', 'date_from', 'date_to'
                    );
                    ?>
                    <button onclick='reset_filter(<?php echo json_encode($filter); ?>);' class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>	
                    <!--search-->
                    <span class="btn btn-default hidden-phone" onclick="show_search_tools();"><?php echo JText::_('OPENSHOP_SEARCHTOOLS') ?><span class="icon-arrow-down-3 edit-arrow-icon"></span></span>
                </td>
                <td align="right">
                    <?php echo $this->lists['order_status_id']; ?>
                </td>	
            </tr>
        </table>

        <!--search tools-->
        <?php
        $display = "none";
        if ($this->state->transporter_filter != 0 || $this->state->appointment_filter != 0 || $this->state->date_from != '' || $this->state->date_to != '') {
            $display = "block";
        }
        ?>
        <div class="search-tools" style="display: <?php echo $display ?>;">
            <div class="search-tool-edit">
                <?php
                echo $this->lists['transporters_filter'];
                ?>
            </div>
            <div class="search-tool-edit">
                <?php
                echo $this->lists['appointments_filter'];
                ?>
            </div>
            <div class="search-tool-edit">
                <?php echo JHtml::_('calendar', $this->state->date_from, 'date_from', 'date_from', '%d-%m-%Y', array('style' => 'width: 100px;', 'placeholder' => JText::_('OPENSHOP_DATE_FROM'), 'onchange' => 'this.form.submit();')); ?>
            </div>
            <div class="search-tool-edit">
                <?php echo JHtml::_('calendar', $this->state->date_to, 'date_to', 'date_to', '%d-%m-%Y', array('style' => 'width: 100px;', 'placeholder' => JText::_('OPENSHOP_DATE_TO'), 'onchange' => 'this.form.submit();')); ?>
            </div>
        </div>

        <div id="editcell">
            <table class="adminlist table table-striped" style="vertical-align: middle;">
                <thead>
                    <tr>
                        <th width="2%" class="text_center">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_center" width="17%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_CUSTOMER'), 'a.payment_fullname', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_center"  width="15%">
                            <?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
                        </th>
                        <th class="text_center"  width="10%">
                            <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                        </th>
                        <th class="text_center" width="11%">
                            <?php echo JText::_('OPENSHOP_ORDER_SIZE') ?>
                        </th>
                        <th class="text_center" width="8%" title="<?php echo JText::_('OPENSHOP_QUANTITY_X_PRICE') ?>">
                            <?php echo JText::_('OPENSHOP_PRICE'); ?>
                        </th>
                        <th class="text_center" width="8%" title="<?php echo JText::_('OPENSHOP_QUANTITY_WAREHOUSE') ?>">
                            <?php echo JText::_('OPENSHOP_QUANLITY_WAREHOUSE'); ?>
                        </th>
                        <th class="text_center" width="8%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_CREATED_DATE'), 'a.created_date', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_DESCRIPTION') ?>
                        </th>
                        <th class="text_center" width="11%">
                            <?php echo JText::_('OPENSHOP_ORDER_STATUS'); ?>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="11">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    $stt = 0;
                    $hide_show = 0;
                    $order_status = JRequest::getInt('order_status_id');
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $row->link_more_product = JRoute::_('index.php?option=com_openshop&task=order.edit&cid[]=' . $row->id);
                        $link_new_order = JRoute::_('index.php?option=com_openshop&task=order.edit&cid[]=' . $row->id);
                        $link = JRoute::_('index.php?option=com_openshop&task=order.edit&customer_id=' . $row->customer_id);
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        $rowspan = $this->getRowProductInOrder($row->id);
//                            $rowspan = $this->checkStatusProduct($row->id);
                        $phone = substr($row->payment_telephone, 0, 4) . ' ' . substr($row->payment_telephone, 4, 3) . ' ' . substr($row->payment_telephone, 7, (int) (strlen($row->payment_telephone) - 7));
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                <?php echo $this->pagination->getRowOffset($i); ?> 
                            </td>
                            <td class="text_left" rowspan="<?php echo $rowspan ?>">
                                <?php
                                if (strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'PENDING') {
                                    echo '<a href="' . $link . '">' . $row->payment_fullname . '</a><br>';
                                } else {
                                    echo '<b>' . $row->payment_fullname . '</b><br>';
                                }
                                ?>
                                ( <b><?php echo $phone; ?></b> )
                                <br>
                                <span><?php echo $row->payment_address ?></span>
                                <br>
                                <span><b>[ <?php echo $row->zone_name; ?> - <?php echo $row->district_name; ?> ]</b></span>
                            </td>
                            <?php
                            $row_pro = array();
                            foreach ($this->productListItem($row->id) as $row_product) {
                                array_push($row_pro, $row_product);
                            }
                            foreach ($this->productListItem($row->id) as $row_product) {
                                //separating the phone number
                                ?>
                                <td class="text_center">
                                    <span>
                                        <?php echo $row_product->product_name; ?>
                                    </span>
                                </td>
                                <td class="text_center">
                                    <?php
                                    if (empty($row_product->product_image)) {
                                        $img_product = 'no-image_1.png';
                                    } else {
                                        $img_product = $row_product->product_image;
                                    }
                                    ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $img_product; ?>" alt="Product Image" width="90px" title="<?php echo $img_product ?>"/>
                                </td>
                                <td class="text_left">
                                    <?php echo $this->getSizeColor($row_product->product_id, $row_product->product_size, 'size'); ?>
                                    <?php
                                    $disabled = '';
                                    if (OpenShopHelper::getConfigValue('managesizecolor') != '2') {
                                        $disabled = 'disabled';
                                    }
                                    echo $this->getSizeColor($row_product->product_id, $row_product->product_color, 'color', $disabled);
                                    ?>
                                    <?php echo $this->getWeight($row_product->optionweight_id, 'weight'); ?>
                                    <?php echo $this->getWeight($row_product->optionheight_id, 'height'); ?>
                                </td>
                                <td class="text_center">
                                    <?php echo $row_product->quantity; ?> x
                                    <br/>
                                    <?php echo (float) $row_product->price; ?>
                                    <hr/>
                                    <b><?php echo (float) $row_product->price * (int) $row_product->quantity; ?></b>
                                </td>
                                <td class="text_center">
                                    <?php
                                    $t = OpenShopHelper::getOrderStatusName($row->order_status_id, JComponentHelper::getParams('com_languages')->get('site', 'en-GB'));
                                    if (strtoupper($t) == 'COMPLETE') {
                                        echo '<img class="img-padding-bottom-10" src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('order_complete_img') . '" width="64px" title="' . JText::_('OPENSHOP_WARTING_SHIPPING') . '" />';
                                    } else if (strtoupper($t) == 'PROCESSING') {
                                        echo '<img class="img-padding-bottom-10" src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_appointment_img') . '" width="64px" title="' . JText::_('OPENSHOP_WARTING_SHIPPING') . '" />';
                                    } else {
                                        //get quantity of product in warehouse
                                        $quantity = $this->getQuantityProductInWarehouse($row_product->product_id, $row_product->product_size, $row_product->product_color);

                                        if ($quantity >= $row_product->quantity || (!empty($row_product->quantity_priority) || $row_product->quantity_priority != '0')) {
                                            if ($this->checkOrderEnouch($row->id)) {
                                                echo $row_product->quantity . ' / <b>' . $quantity . '</b>';
                                                if ($row_product->quantity_priority > 0) {
                                                    echo ' <span style="font-weight: bold; color: #FF00E9;" title="' . JText::_('OPENSHOP_QUANTITY_PRIORITIED') . '">(+' . $row_product->quantity_priority . ')<span>';
                                                }
                                                echo '<br/><br/>';
                                                echo '<img class="img-padding-bottom-10" src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_appointment_img') . '" width="64px" title="' . JText::_('OPENSHOP_WARTING_SHIPPING') . '" />';
                                            } else {
                                                if ($row_product->quantity == $row_product->quantity_priority) { //quantity priority == quantity in order
                                                    echo $row_product->quantity . ' / <b>' . $quantity . '</b>';
                                                    echo ' <span style="font-weight: bold; color: #FF00E9;" title="' . JText::_('OPENSHOP_QUANTITY_PRIORITIED') . '">(+' . $row_product->quantity_priority . ')<span><br/><br/>';
                                                    echo '<img class="img-padding-bottom-10" src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_enouch_img') . '" width="64px" title="' . JText::_('OPENSHOP_WARTING_ENOUCH') . '" />';
                                                } else {
                                                    echo $row_product->quantity . ' / <b>' . $quantity . '</b> ';
                                                    echo ' <span style="font-weight: bold; color: #FF00E9;" title="' . JText::_('OPENSHOP_QUANTITY_PRIORITIED') . '">(+' . $row_product->quantity_priority . ')<span><br/><br/>';
                                                    echo '<img class="img-padding-bottom-10" src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_input_img') . '" width="64px" title="' . JText::_('OPENSHOP_WARTING_IMPORT') . '" />';
                                                }
                                            }
                                            //check priority
                                            if ($order_status != 1) {
                                                if (empty($row_product->quantity_priority) || $row_product->quantity_priority == '0') { //don't priority
                                                    echo '<div class="quantity-priority qp_' . $row_product->id . '" onclick="quantityPriority(' . $row_product->quantity . ',' . $row_product->id . ')">' . JText::_('OPENSHOP_QUANTITY_PRIORITY') . '</div>';
                                                } else if ($row_product->quantity != $row_product->quantity_priority) {             //quantity in order different quantity priority
                                                    if ($quantity > 0) {
                                                        echo '<div class="quantity-priority qp_' . $row_product->id . '" onclick="quantityPriority(' . $row_product->quantity . ',' . $row_product->id . ')">' . JText::_('OPENSHOP_QUANTITY_PRIORITY') . '</div>';
                                                    }
                                                } else {
                                                    echo '<div class="quantity-priorited" >' . JText::_('OPENSHOP_QUANTITY_PRIORITED') . '</div>';
                                                }
                                            }
                                        } else {
                                            echo $row_product->quantity . ' / <b>' . $quantity . '</b><br/><br/>';
                                            echo '<img class="img-padding-bottom-10" src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_input_img') . '" width="64px" title="' . JText::_('OPENSHOP_WARTING_IMPORT') . '" />';

                                            if ($quantity > 0) {
                                                echo '<div class="quantity-priority qp_' . $row_product->id . '" onclick="quantityPriority(' . $row_product->quantity . ',' . $row_product->id . ')">' . JText::_('OPENSHOP_QUANTITY_PRIORITY') . '</div>';
                                            }
                                        }
                                    }
                                    ?>
                                </td>
                                <?php
                                if ($order_status == 1) {
                                    $data_cancel = $this->getDataCancel($row->id);
                                    ?>
                                    <td class="text_center">
                                        <?php
                                        if ($row->created_date != $this->nullDate) {
                                            echo date('d-m-Y', strtotime($row->created_date));
                                        }
                                        echo '<br /> <hr />';
                                        echo '<span style="color:red;"><b>' . (isset($data_cancel->created_date) ? date('H:m:s', strtotime($row->created_date)) . ' <br/> ' . date('d-m-Y', strtotime($data_cancel->created_date)) : '') . '</b></span>';
                                        ?>
                                    </td>
                                    <td class="text_center">
                                        <textarea rows="4" style="width:120px;">
                                            <?php
                                            if (!empty($data_cancel)) {
                                                echo $data_cancel->reason_detail_description;

                                                if (!empty($data_cancel->reason_description) || isset($data_cancel->reason_description)) {
                                                    echo ' [ ' . $data_cancel->reason_description . ' ] ';
                                                }
                                            }
                                            ?>
                                        </textarea>
                                    </td>
                                    <?php
                                } else {
                                    ?>
                                    <td class="text_center">
                                        <?php
                                        if ($row->created_date != $this->nullDate) {
                                            echo date('H:m:s', strtotime($row->created_date));
                                            echo '<br/>';
                                            echo date('d-m-Y', strtotime($row->created_date));
                                        }
                                        ?>
                                    </td>
                                    <td class="text_center">
                                        <textarea class="desc_<?php echo $row_product->id ?>" rows="4" style="width:120px;" onchange="updateDescOrder(this.value,<?php echo $row_product->id ?>)"><?php echo $row_product->order_product_desc; ?></textarea>
                                    </td>
                                    <?php
                                }
                                if ($hide_show == 0) {
                                    ?>
                                    <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                        <?php
                                        $t = OpenShopHelper::getOrderStatusName($row->order_status_id, JComponentHelper::getParams('com_languages')->get('site', 'en-GB'));
                                        ?>
                                        <span class="border_background_<?php echo strtolower($t); ?>">
                                            <?php echo JText::_('OPENSHOP_STATUS_ORDER_' . strtoupper($t)); ?>
                                        </span>
                                        <?php
                                        if ($row->appointment_type == '1') {   //office order
                                            echo '<div class="">[ ' . JText::_('OPENSHOP_ORDER_OFFICE') . ' ]</div>';
                                        } else {
                                            echo '<div class="">[ ' . $row->transporter_name . ' ]</div>';
                                        }
                                        ?>
                                        <?php
                                        $view = JFactory::getApplication()->input->getString('view');
                                        if (strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'PENDING' || strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'CANCELED REVERSAL') {
                                            if (($this->checkOrderEnouch($row->id) == 1 && OpenShopHelper::getPermissionAction($view, 'create') && OpenShopHelper::getPermissionAction($view, 'edit'))) {
                                                if (OpenShopHelper::getConfigValue('hideshow_appointment') == '1') { //modal mode
                                                    ?>
                                                    <div class="btn btn-primary margin-btn" title="<?php echo JText::_('OPENSHOP_ORDER_APPOINTMENT_DESC') ?>" onclick='show_modal_appointment(<?php echo json_encode($row) ?>);'><i class="icon-tags-2"></i></div>
                                                    <?php
                                                }
                                                else if (OpenShopHelper::getConfigValue('hideshow_appointment') == '2') { //blank mode
                                                    $urlBlankAppoinment = 'index.php?option=com_openshop&view=orders&layout=cartphone&phone=' . $row->payment_telephone;
                                                    ?>
                                                    <a href="<?php echo $urlBlankAppoinment ?>" target="_blank" class="btn btn-primary margin-btn" title="<?php echo JText::_('OPENSHOP_ORDER_APPOINTMENT_DESC') ?>"><i class="icon-tags-2"></i></a>
                                                    <?php
                                                }
                                            } else if (OpenShopHelper::getPermissionAction($view, 'create') && OpenShopHelper::getPermissionAction($view, 'edit')) {
                                                ?>
                                                <div class="btn btn-primary margin-btn btn-appointment-disable" title="<?php echo JText::_('OPENSHOP_ORDER_APPOINTMENT_DESC') ?>"><i class="icon-tags-2"></i></div>
                                                <?php
                                            }

                                            if (OpenShopHelper::getPermissionAction($view, 'delete')) {
                                                ?>
                                                <div class="btn btn-danger margin-btn" onclick='show_delete_order(<?php echo $row->id ?>, "delete")' title="<?php echo JText::_('OPENSHOP_ORDER_DELETE_DESC') ?>"><i class="icon-delete"></i></div>
                                                <?php
                                            }

                                            if (OpenShopHelper::getPermissionAction($view, 'create') && OpenShopHelper::getPermissionAction($view, 'edit')) {
                                                ?>
                                                <div class="add-order-product" onclick="window.open('<?php echo $link_new_order ?>')"><i class="icon-plus-2"></i><i class="icon-cart"></i></div>
                                                <?php
                                            }
                                        } else if (strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'COMPLETE' && OpenShopHelper::getPermissionAction($view, 'edit')) {
                                            ?>
                                            <div class="btn btn-default" onclick='show_delete_order(<?php echo $row->id ?>, "return")' title="<?php echo JText::_('OPENSHOP_ORDER_RETURN_DESC') ?>"><i class="icon-enter"></i></div>
                                            <?php
                                        }
                                        //if order had deleted
                                        if ($order_status == 1) {
                                            ?>
                                            <div class="reversal-order">
                                                <span onclick="reversalOrder(<?php echo $row->id ?>)">
                                                    <i class="icon-backward-2"></i>
                                                </span>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <?php
                                    $hide_show = 1;
                                }
                                ?>
                            </tr>		
                            <?php
                        }
                        $hide_show = 0;
                        $k = 1 - $k;
                        $stt += 1;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <!--APPOINTMENT-->
        <div class="modal fade" id="modal-appointment" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px;">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_APPOINTMENT') ?></h3>
                        <div class="btn-right" style="float: right;">
                            <button type="button" class="btn btn-primary btn-appointment" onclick="order_ok()"><i class="icon-apply"></i> <?php echo JText::_('OPENSHOP_OK') ?></button>
                            <button type="button" class="btn btn-default btn-more-product" onclick=""><i class="icon-plus-2"></i> <?php echo JText::_('OPENSHOP_MORE_PRODUCTS') ?></button>
                            <button type="button" class="btn btn-default check_delete_order" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!--INFO CUSTOMER + APPOINTMENT-->
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_ORDER_CUSTOMER_INFORMATION') ?>
                                        </div>
                                        <div class="panel-body responsive-edit row">
                                            <div class="form-group col-md-12">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_NAME') ?></label>
                                                <span id="customer_name" class="col-md-9"></span>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_PHONE') ?></label>
                                                <span id="customer_phone" class="col-md-9"></span>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_ADDRESS') ?></label>
                                                <span id="customer_address" class="col-md-9"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_ORDER_TOTAL_PAID') ?>
                                        </div>
                                        <div class="panel-body responsive-edit">
                                            <div class="total_paid">
                                                <!--//paid-->
                                            </div>
                                            <div class="unit_money">( VNĐ )</div>
                                            <div>
                                                Số lượng sản phẩm: <b><span class="total_buy_product label label-default"></span></b>
                                                <br/>
                                                Tổng tiền: <b><span class="total_money"></span></b>
                                                <br/>
                                                Giảm giá: <b><span class="sale_money">0</span></b>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_STATUS_APPOINTMENT') ?>
                                        </div>
                                        <div class="panel-body cus responsive-edit">
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_STATUS_1') ?></label>
                                                <span class="col-md-9">
                                                    <div class="btn-group" role="group" aria-label="..." style="margin-bottom: 10px;">
                                                        <button type="button" class="btn btn-default status-none active order-appointment" onclick="getStatusAppointmentAjax('0')"><?php echo JText::_('OPENSHOP_STATUS_NONE'); ?></button>
                                                        <button type="button" class="btn btn-default status-office order-appointment" onclick="getStatusAppointmentAjax('1')"><?php echo JText::_('OPENSHOP_STATUS_OFFICE'); ?></button>
                                                        <button type="button" class="btn btn-default status-shipping order-appointment" onclick="getStatusAppointmentAjax('2')"><?php echo JText::_('OPENSHOP_STATUS_SHIP'); ?></button>
                                                        <button type="button" class="btn btn-default status-sales-shop order-appointment" onclick="getStatusAppointmentAjax('3')"><?php echo JText::_('OPENSHOP_STATUS_SALES_SHOP'); ?></button>
                                                    </div>
                                                </span>
                                                <input type="hidden" value="0" id="status_appointment" name="status_appointment" />
                                            </div>
                                            <div class="form-group transporter_hideshow" style="display:none;">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_TRANSPORTER') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" disabled="disable" class="form-control" name="appointment_transporter" id="appointment_transporter" value="" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DATE') ?></label>
                                                <span class="col-md-9">
                                                    <?php
                                                    $date = getdate();
                                                    $y = $date['year'];
                                                    $m = $date['mon'] < 10 ? '0' . $date['mon'] : $date['mon'];
                                                    $d = $date['mday'] < 10 ? '0' . $date['mday'] : $date['mday'];
                                                    echo JHtml::_('calendar', $y . '-' . $m . '-' . $d, 'appointment_date', 'appointment_date', '%Y-%m-%d', array('style' => 'width: 208px;'));
                                                    ?>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_SALE') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" class="form-control" name="appointment_sale" id="appointment_sale" value="0" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DISCOUNT') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" class="form-control" name="appointment_discount" id="appointment_discount" value="0" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DESCRIPTION') ?></label>
                                                <span class="col-md-9">
                                                    <textarea rows="3" class="form-control" width="" name="appointment_description" id="appointment_description"></textarea>
                                                </span>
                                            </div>

                                            <!--ID ORDER-->
                                            <input type="hidden" name="order_id" id="order_id" value=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--PRODUCTS IN ORDER-->
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_NUM'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_IMAGE'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_PRICE_TOTAL'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody class="show-pro-appointment">
                                        <!--show product information-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--DELETE APPOINTMENT-->
        <div class="modal fade" id="modal-reason-delete-order" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_REASON_DELETE_ORDER') ?></h3>
                        <div style="float: right;">
                            <button type="button" class="btn btn-danger delete_order" onclick="" ><i class="icon-delete"></i> <?php echo JText::_('OPENSHOP_DELETE') ?></button>
                            <button type="button" class="btn btn-default delete_order_cancel" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3"><?php echo JText::_('OPENSHOP_SELECT_REASON') ?></label>
                                    <span class="col-md-9">
                                        <?php echo $this->lists['reason_delete_order'] ?>
                                        <div class="error_reason"></div>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3"><?php echo JText::_('OPENSHOP_SELECT_REASON_DESC') ?></label>
                                    <span class="col-md-9">
                                        <textarea class="form-control reason_delete_order_desc" rows="5" name="reason_delete_order_desc" id="reason_delete_order_desc" style="width:625px"></textarea>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
        <input type="hidden" id="view" value="<?php echo JFactory::getApplication()->input->getString('view') ?>" />
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>