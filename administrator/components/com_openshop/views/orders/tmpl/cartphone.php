<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
$app = JFactory::getApplication()->input;
$phone = $app->getString('phone');

$sizes = array();
foreach (OpenShopHelper::getValueSize() as $s) {
    $sizes[$s->optionvalue_id] = $s->value;
}
?>
<div class="adminfs">
    <div class="panel panel-default">
        <div class="panel-heading">Thông tin khách hàng</div>
        <div class="panel-body">
            <div class="span6">
                <?php
                $cus = $this->getCustomerCartPhone($phone);
                ?>
                <div class="span12" style="margin: 0;">
                    <label class="span3">Tên khách hàng</label>
                    <div class="span9"><?php echo $cus->fullname ?></div>
                </div> 
                <div class="span12" style="margin: 0;">
                    <label class="span3">Số điện thoại</label>
                    <div class="span9"><?php echo $cus->telephone ?></div>

                </div>
                <div class="span12" style="margin: 0;">
                    <label class="span3">Địa chỉ</label>
                    <div class="span9">
                        <?php echo $cus->address ?>
                        <br/>
                        [ <?php echo '<b>' . $this->getTown($cus->zone_id) . '</b>' ?> - <?php echo '<b>' . $this->getDistrict($cus->district_id) . '</b>' ?> ]
                    </div>
                </div>
            </div>
            <div class="span6">

            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Thông tin hẹn</div>
        <div class="panel-body">
            <div class="span8">
                a
            </div>
            <div class="span4">
                b
            </div>
        </div>
    </div>

    <table class="table cartphonetbl">
        <thead style="background: #D4D4D4;text-align: center;">
            <tr>
                <td>#</td>
                <td>
                    <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                </td>
                <td><?php echo JText::_('OPENSHOP_PRODUCT_NAME') ?></td>
                <td><?php echo JText::_('OPENSHOP_IMAGE') ?></td>
                <td><?php echo JText::_('OPENSHOP_ORDER_SIZE') ?></td>
                <td><?php echo JText::_('OPENSHOP_PRICE') ?></td>
                <td><?php echo JText::_('OPENSHOP_CREATED_DATE') ?></td>
                <td><?php echo JText::_('OPENSHOP_DESCRIPTION') ?></td>
            </tr>
        </thead>
        <tbody>
            <tr style="background: #1F5286; color: white; text-align: center;">
                <td colspan="8">
                    Đơn hàng đang chờ xử lý
                </td> 
            </tr>
            <?php
            $rows = $this->getAllOrderThisPhone($phone);
            if (count($rows['noprocess'])) {
                foreach ($rows['noprocess'] as $k => $row) {
                    ?>
                    <tr>
                        <td class="center"><?php echo $k + 1 ?></td>
                        <td class="center">
                            <?php echo JHtml::_('grid.id', $k, $row->id); ?>
                        </td>
                        <td><?php echo $row->product_name ?></td>
                        <td class="center">
                            <?php
                            if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $row->product_image)) {
                                $viewImage = JFile::stripExt($row->product_image) . '-100x100.' . JFile::getExt($row->product_image);
                                if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                    ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" width="50" />
                                    <?php
                                } else {
                                    ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image; ?>" width="50" />
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <td class="center"><?php echo $sizes[$row->product_size] ?></td>
                        <td class="center"><?php echo number_format($row->price, 0, ',', '.') ?></td>
                        <td class="center">
                            <?php
                            $date = explode(' ', $row->created_date);
                            echo $date[1];
                            echo '<br/>' . date('d-m-Y', strtotime($date[0]));
                            ?>
                        </td>
                        <td class="center">
                            <textarea name="desc_cartphone" id="desc_cartphone"></textarea>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td class="center" colspan="8">
                        Hiện không có đơn hàng
                    </td>
                </tr>
                <?php
            }
            ?>

            <!--//Processed-->
            <?php
            if (count($rows['process'])) {
                ?>
                <tr style="background: #BD4F4F; color: white; text-align: center;">
                    <td colspan="8">
                        Đơn hàng đã xử lý
                    </td> 
                </tr>
                <?php
                foreach ($rows['process'] as $k => $row) {
                    ?>
                    <tr>
                        <td></td>
                        <td>

                        </td>
                        <td><?php echo $row->product_name ?></td>
                        <td class="center">
                            <?php
                            if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $row->product_image)) {
                                $viewImage = JFile::stripExt($row->product_image) . '-100x100.' . JFile::getExt($row->product_image);
                                if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                    ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" width="50" />
                                    <?php
                                } else {
                                    ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image; ?>" width="50" />
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <td class="center"><?php echo $sizes[$row->product_size] ?></td>
                        <td class="center"><?php echo number_format($row->price, 0, ',', '.') ?></td>
                        <td class="center">
                            <?php
                            $date = explode(' ', $row->created_date);
                            echo $date[1];
                            echo '<br/>' . date('d-m-Y', strtotime($date[0]));
                            ?>
                        </td>
                        <td class="center">
                            <textarea name="desc_cartphone" id="desc_cartphone"></textarea>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

    <input type="text" name="option" value="com_openshop" />
    <input type="text" name="task" value="" />
    <input type="text" name="boxchecked" value="0" />
    <input type="text" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
    <input type="text" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
    <?php echo JHtml::_('form.token'); ?>
</div>

