<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
JFactory::getDocument()->addScript(JUri::base() . DS . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'trans.js');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'transporter.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
            Joomla.submitform(pressbutton, form);
        }
    }
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
    <div class="panel panel-default box-show-huy" style="background-color: white;">
        <div class="panel-heading"><?php echo JText::_('OPENSHOP_TRANSPORTER') ?></div>
        <div class="panel-body">
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_NAME'); ?></label>
                <span class="col-sm-5">
                    <input class="input-xlarge" type="text" name="transporter_name" id="transporter_name" size="" maxlength="250" value="<?php echo $this->item->transporter_name; ?>" />
                </span>
            </div>
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_PHONE'); ?></label>
                <span class="col-sm-5">
                    <input class="input-xlarge" type="text" name="transporter_phone" id="transporter_phone" size="" maxlength="250" value="<?php echo $this->item->transporter_phone; ?>" />
                </span>
            </div>
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_EMAIL'); ?></label>
                <span class="col-sm-5">
                    <input class="input-xlarge" type="text" name="transporter_email" id="transporter_email" size="" maxlength="250" value="<?php echo $this->item->transporter_email; ?>" />
                </span>
            </div>
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_ADDRESS'); ?></label>
                <span class="col-sm-5">
                    <input class="input-xlarge" type="text" name="transporter_address" id="transporter_address" size="" maxlength="250" value="<?php echo $this->item->transporter_address; ?>" />
                </span>
            </div>
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_WEBSITE'); ?></label>
                <span class="col-sm-5">
                    <input class="input-xlarge" type="text" name="transporter_website" id="transporter_website" size="" maxlength="250" value="<?php echo $this->item->transporter_website; ?>" />
                </span>
            </div>
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TRANSPOTER_MANAGER'); ?></label>
                <span class="col-sm-5">
                    <input class="input-xlarge" type="text" name="transporter_manager" id="transporter_manager" size="" maxlength="250" value="<?php echo $this->item->transporter_manager; ?>" />
                </span>
            </div>
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></label>
                <span class="col-sm-5">
                    <?php echo $this->lists['published']; ?>
                </span>
            </div>
            <div class="form-group col-md-12">
                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></label>
                <span class="col-sm-10">
                    <?php echo $editor->display('transporter_description', $this->item->transporter_description, '100%', '250', '75', '10'); ?>
                </span>
            </div>
        </div>
    </div>

    <?php echo JHtml::_('form.token'); ?>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
    <?php
    if ($translatable) {
        foreach ($this->languages as $language) {
            $langCode = $language->lang_code;
            ?>
            <input type="hidden" name="details_id_<?php echo $langCode; ?>" value="<?php echo isset($this->item->{'details_id_' . $langCode}) ? $this->item->{'details_id_' . $langCode} : ''; ?>" />
            <?php
        }
    } elseif ($this->translatable) {
        ?>
        <input type="hidden" name="details_id" value="<?php echo isset($this->item->{'details_id'}) ? $this->item->{'details_id'} : ''; ?>" />
        <?php
    }
    ?>
    <input type="hidden" name="task" value="" />
</form>