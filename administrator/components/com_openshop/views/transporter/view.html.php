<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewTransporter extends OpenShopViewForm {

    public function _buildToolbar() {
        $viewName = $this->getName();
        $view = OpenShopInflector::pluralize($viewName);
        $edit = JRequest::getVar('edit');
        $text = $edit ? JText::_($this->lang_prefix . '_EDIT') : JText::_($this->lang_prefix . '_NEW');
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . $viewName) . ': <small><small class="title-product">[ ' . $text . ' ]</small></small>');
        if (OpenShopHelper::getPermissionAction($view, 'create')) {
//            JToolbarHelper::custom('save_transpoter','apply','apply',  JText::_('OPENSHOP_SAVE'), false);
            JToolBarHelper::apply($viewName . '.apply');
        }
        JToolbarHelper::custom('reset_transporter','refresh','refresh',  JText::_('OPENSHOP_RESET'), false);
        if ($edit)
            JToolbarHelper::custom('close_transporter','delete','delete',  JText::_('OPENSHOP_CLOSE'), false);
        else
            JToolbarHelper::custom('close_transporter','delete','delete',  JText::_('OPENSHOP_CLOSE'), false);
    }

}
