<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewMenu extends OpenShopViewForm {
    function _buildListArray(&$lists, $item) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        
        $query->select('menu_name AS title, id AS id, menu_parent_id as parent_id')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('published = 1')
                ->order('parent_id');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $children = array();
        if ($rows) {
            // first pass - collect children
            foreach ($rows as $v) {
                $pt = $v->parent_id;
                $list = @$children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
        }
        $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
        
        $lists['menu_option'] = $list;
        
        $query->clear();
        $query->select('extension_id AS value, name AS text')
                ->from($db->quoteName('#__extensions'))
                ->where('type = "component"');
        $db->setQuery($query);
        $lists['components'] = $db->loadObjectList();
    }
}
