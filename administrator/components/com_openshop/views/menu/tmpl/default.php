<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'menu.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
<?php
if ($translatable) {
    foreach ($this->languages as $language) {
        $langId = $language->lang_id;
        ?>
                    if (document.getElementById('menu_name_<?php echo $langId; ?>').value == '') {
                        alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                        document.getElementById('menu_name_<?php echo $langId; ?>').focus();
                        return;
                    }
        <?php
    }
} else {
    ?>
                if (form.menu_name.value == '') {
                    alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                    form.menu_name.focus();
                    return;
                }
    <?php
}
?>
            Joomla.submitform(pressbutton, form);
        }
    }
</script>
<fieldset class="adminfs">
    <form action="index.php" method="post" class="from-validation" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <div class="row-fluid">
            <div class="tab-content">
                <div class="tab-pane active" id="general-page">
                    <div class="span10">
                        <table class="admintable adminform" style="width: 100%;">
                            <tr>
                                <td class="key">
                                    <span class="required">*</span>
                                    <?php echo JText::_('OPENSHOP_NAME'); ?>
                                </td>
                                <td>
                                    <input class="input-xlarge" type="text" name="menu_name" id="menu_name" size="" maxlength="250" value="<?php echo $this->item->menu_name; ?>" />
                                </td>								
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_CHOOSE_COMPONENTS'); ?>
                                </td>
                                <td>
                                    <select name="menu_com">
                                        <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE'); ?></option>
                                        <?php
                                        foreach ($this->lists['components'] AS $c) {
                                            $s = '';
                                            if ($c->value === JFactory::getApplication()->input->get('com_id')) {
                                                $s = 'selected="selected"';
                                            }
                                            echo '<option value="' . $c->value . '" ' . $s . '>' . $c->text . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>							
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_MENU_LINK') ?>
                                </td>
                                <td>
                                    <input class="input-xlarge" type="text" name="menu_link" id="menu_link" maxlength="250" value="<?php echo $this->item->menu_link ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_MENU_VIEW') ?>
                                </td>
                                <td>
                                    <input class="input-xlarge" type="text" name="menu_view" id="menu_view" maxlength="250" value="<?php echo $this->item->menu_view ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_MENU_LAYOUT') ?>
                                </td>
                                <td>
                                    <input class="input-xlarge" type="text" name="menu_layout" id="menu_layout" maxlength="250" value="<?php echo $this->item->menu_layout ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_MENU_CLASS') ?>
                                </td>
                                <td>
                                    <input class="input-xlarge required" type="text" name="menu_class" id="menu_class" maxlength="250" value="<?php echo $this->item->menu_class ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_MENU_PARANT') ?>
                                </td>
                                <td>
                                    <select name="menu_parent_id">
                                        <option value='0'>----<?php echo JText::_('OPENSHOP_CHOOSE'); ?>-----</option>
                                        <?php
                                        $selected = 'selected="selected"';

                                        foreach ($this->lists['menu_option'] as $menu) {
                                            if ($menu->id == JRequest::getInt('parent_id', 0))
                                                $selected = 'selected="selected"';
                                            else
                                                $selected = '';

                                            $name = $menu->treename;
                                            $name = explode(' ', $name);
                                            if (count($name) == 2) {
                                                $menu_name = $name[0] .' '. JText::_($name[1]);
                                            }
                                            else
                                            {
                                                $menu_name = JText::_($menu->treename);
                                            }
                                            echo '<option ' . $selected . ' value=' . $menu->id . '>' . $menu_name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_ACCESS'); ?>
                                </td>
                                <td>
                                    <?php echo $this->lists['access_menu']; ?>
                                </td>							
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                                </td>
                                <td>
                                    <?php echo $this->lists['published']; ?>
                                </td>							
                            </tr>
                            <tr>
                                <td  class="key">
                                    <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                                </td>
                                <td>
                                    <input type="file" class="input-large" accept="image/*" name="menu_image" />
                                    <?php
                                    $viewImage = JFile::stripExt($this->item->menu_image) . '.' . JFile::getExt($this->item->menu_image);

                                    if (JFile::exists(OPENSHOP_ADMIN_PATH_ICON . $viewImage)) {
                                        ?>
                                        <img src="<?php echo OPENSHOP_ADMIN_PATH_ICON_HTTP . $viewImage; ?>" />
                                    <?php } ?>

                                    <label class="checkbox">
                                        <input type="checkbox" name="remove_image" value="1" />
                                        <?php echo JText::_('OPENSHOP_REMOVE_IMAGE'); ?>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div><!-- End General page -->
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
        <?php
        if ($translatable) {
            foreach ($this->languages as $language) {
                $langCode = $language->lang_code;
                ?>
                <input type="hidden" name="details_id_<?php echo $langCode; ?>" value="<?php echo isset($this->item->{'details_id_' . $langCode}) ? $this->item->{'details_id_' . $langCode} : ''; ?>" />
                <?php
            }
        } elseif ($this->translatable) {
            ?>
            <input type="hidden" name="details_id" value="<?php echo isset($this->item->{'details_id'}) ? $this->item->{'details_id'} : ''; ?>" />
            <?php
        }
        ?>
        <input type="hidden" name="task" value="" />
    </form>
</fieldset>