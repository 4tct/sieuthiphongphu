<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewPayments extends OpenShopViewList {

    /**
     * Override Build Toolbar function, only need Publish, Unpublish and Delete button
     */
    function _buildToolbar() {
        $viewName = $this->getName();
        $controller = OpenShopInflector::singularize($this->getName());
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
        if (OpenShopHelper::getPermissionAction($viewName, 'delete')) {
            JToolBarHelper::deleteList(JText::_($this->lang_prefix . '_DELETE_' . strtoupper($this->getName()) . '_CONFIRM'), $controller . '.remove');
        }
        if (OpenShopHelper::getPermissionAction($viewName, 'editstate')) {
            JToolBarHelper::publishList($controller . '.publish');
            JToolBarHelper::unpublishList($controller . '.unpublish');
        }
    }

}
