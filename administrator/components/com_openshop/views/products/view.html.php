<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewProducts extends OpenShopViewList {

    public function display($tpl = null) {
        $this->currency = new OpenShopCurrency();
        parent::display($tpl);
    }
    
    /*
     * 
     */
    public function _buildToolbar() {
        parent::_buildToolbar();
        JToolbarHelper::custom('importProduct','upload','upload', 'Import sản phẩm', FALSE);
        JToolbarHelper::custom('exportProduct','download','download', 'Export sản phẩm', FALSE);
    }


    /**
     * Build all the lists items used in the form and store it into the array
     * @param  $lists
     * @return boolean
     */
    public function _buildListArray(&$lists, $state) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, b.category_name AS title, a.category_parent_id AS parent_id')
                ->from('#__openshop_categories AS a')
                ->innerJoin('#__openshop_categorydetails AS b ON (a.id = b.category_id)')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $children = array();
        if ($rows) {
            // first pass - collect children
            foreach ($rows as $v) {
                $pt = $v->parent_id;
                $list = @$children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
        }
        $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_SELECT_A_CATEGORY'));
        foreach ($list as $listItem) {
            $options[] = JHtml::_('select.option', $listItem->id, $listItem->treename);
        }
        $lists['category_id'] = JHtml::_('select.genericlist', $options, 'category_id', array(
                    'option.text.toHtml' => false,
                    'option.text' => 'text',
                    'option.value' => 'value',
                    'list.attr' => ' class="inputbox select-width-180 form-control" onchange="this.form.submit();"',
                    'list.select' => $state->category_id));

        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__usergroups'));
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $children = array();
        if ($rows) {
            // first pass - collect children
            foreach ($rows as $v) {
                $pt = $v->parent_id;
                $list = @$children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
        }
        $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
        $lists['usergroup'] = $list;

        // Stock status list
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_SELECT_STOCK_STATUS'));
        $options[] = JHtml::_('select.option', '1', JText::_('OPENSHOP_IN_STOCK'));
        $options[] = JHtml::_('select.option', '2', JText::_('OPENSHOP_OUT_OF_STOCK'));
        $lists['stock_status'] = JHtml::_('select.genericlist', $options, 'stock_status', array(
                    'option.text.toHtml' => false,
                    'option.text' => 'text',
                    'option.value' => 'value',
                    'list.attr' => ' class="inputbox select-width-180 form-control" onchange="this.form.submit();" ',
                    'list.select' => $state->stock_status));

        //brand filter
        $query->clear();
        $query->select('a.id AS value, b.brand_name AS text')
                ->from($db->quoteName('#__openshop_brands', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_branddetails', 'b') . 'ON a.id = b.brand_id')
                ->where('a.published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_BRAND_FILTER'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['brand_filter'] = JHtml::_('select.genericlist', $options, 'brand_id', ' class="inputbox select-width-180 form-control" onchange="this.form.submit();" ', 'value', 'text', $state->brand_id);

        //manufacturer filter
        $query->clear();
        $query->select('a.id AS value, a.manufacturer_name AS text')
                ->from($db->quoteName('#__openshop_manufacturers', 'a'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_MANUFACTURER_FILTER'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['manufacturer_filter'] = JHtml::_('select.genericlist', $options, 'manufacturer_id', ' class="inputbox select-width-180 form-control" onchange="this.form.submit();" ', 'value', 'text', $state->manufacturer_id);

        //argency filter
        $query->clear();
        $query->select('a.id AS value, a.argency_name AS text')
                ->from($db->quoteName('#__openshop_argencies', 'a'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_ARGENCY_FILTER'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['argency_filter'] = JHtml::_('select.genericlist', $options, 'argency_id', ' class="inputbox select-width-180 form-control" onchange="this.form.submit();" ', 'value', 'text', $state->argency_id);
        
		//product new
		$options = array();
		$options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_PRODUCT_NEW_CHOOSE'));
		$options[] = JHtml::_('select.option', '1', JText::_('OPENSHOP_PRODUCT_NEW'));
		$lists['product_new'] = JHtml::_('select.genericlist', $options, 'product_new', ' class="inputbox select-width-180 form-control" onchange="this.form.submit();" ', 'value', 'text', $state->product_new);
		
		//product hot
		$options = array();
		$options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_PRODUCT_HOT_CHOOSE'));
		$options[] = JHtml::_('select.option', '1', JText::_('OPENSHOP_PRODUCT_HOT'));
		$lists['product_hot'] = JHtml::_('select.genericlist', $options, 'product_hot', ' class="inputbox select-width-180 form-control" onchange="this.form.submit();" ', 'value', 'text', $state->product_hot);
		
		
        $options = array();
        $options[] = JHtml::_('select.option', '' , JText::_('OPENSHOP_SHOW_ALL_PRO_FILTER'));
        $options[] = JHtml::_('select.option', '0' , JText::_('OPENSHOP_PRODUCTS_DONT_DELETE'));
        $options[] = JHtml::_('select.option', '1' , JText::_('OPENSHOP_PRODUCTS_DELETE'));
        $lists['show_all_product'] = JHtml::_('select.genericlist', $options, 'show_all_product',' class="inputbox select-width-180 form-control" onchange="this.form.submit();" ', 'value', 'text', $state->show_all_product );
    }

    /**
     * Additional grid function for featured toggles
     *
     * @return string HTML code to write the toggle button
     */
    function toggle($field, $i, $imgY = 'tick.png', $imgX = 'publish_x.png', $prefix = '') {
        $img = $field ? $imgY : $imgX;
        $task = $field ? 'product.unfeatured' : 'product.featured';
        $alt = $field ? JText::_('OPENSHOP_PRODUCT_FEATURED') : JText::_('OPENSHOP_PRODUCT_UNFEATURED');
        $action = $field ? JText::_('OPENSHOP_PRODUCT_UNFEATURED') : JText::_('OPENSHOP_PRODUCT_FEATURED');
        return ('<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\'' . $task . '\')" title="' . $action . '">' .
                JHtml::_('image', 'admin/' . $img, $alt, null, true) . '</a>');
    }

    public function featured($value = 0, $i, $canChange = true) {
        JHtml::_('bootstrap.tooltip');
        // Array of image, task, title, action
        $states = array(
            0 => array('unfeatured', 'product.featured', 'OPENSHOP_PRODUCT_UNFEATURED', 'OPENSHOP_PRODUCT_FEATURED'),
            1 => array('featured', 'product.unfeatured', 'OPENSHOP_PRODUCT_FEATURED', 'OPENSHOP_PRODUCT_UNFEATURED'),
        );
        $state = JArrayHelper::getValue($states, (int) $value, $states[1]);
        $icon = $state[0];
        if ($canChange) {
            $html = '<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\'' . $state[1] . '\')" class="btn btn-micro hasTooltip' . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[3]) . '"><i class="icon-' . $icon . '"></i></a>';
        } else {
            $html = '<a class="btn btn-micro hasTooltip disabled' . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[2]) . '"><i class="icon-' . $icon . '"></i></a>';
        }
        return $html;
    }
    
    function getNumberProductOfOrder($id_order_pro){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.quantity')
                ->from($db->quoteName('#__openshop_orderproducts' , 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_orders','b') . 'ON a.order_id = b.id')
                ->where('a.product_id = ' . $id_order_pro);
        $rows = $db->setQuery($query)->loadObjectList();
        
        $num = 0;
        foreach ($rows as $value) {
            $num += $value->quantity;
        }
        
        return $num;
    }

}
