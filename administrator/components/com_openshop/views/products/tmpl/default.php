<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addStyleSheet(JUri::base() . DS . 'components/com_openshop/assets/css/product.css');
JFactory::getDocument()->addScript(JUri::base() . DS . 'components/com_openshop/assets/js/jquery.form.min.js');
JFactory::getDocument()->addScript(JUri::base() . DS . 'components/com_openshop/assets/js/product.js');
?>
<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=products" method="post" name="adminForm" id="adminForm">
        <table width="100%" style="    margin: 0 0 5px 0;">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>
                    <!--search-->
                    <span class="btn btn-default hidden-phone" onclick="show_search_tools();"><?php echo JText::_('OPENSHOP_SEARCHTOOLS') ?><span class="icon-arrow-down-3 edit-arrow-icon"></span></span>					
                </td>
                <td  class="text-right">
                    <?php echo JText::_("Số SP / Trang ") . $this->pagination->getLimitBox(); ?>

                </td>
            </tr>
        </table>
        <!--search tools-->
        <?php
        $display = "none";
        if ($this->state->product_hot != 0 || $this->state->product_new != 0 || $this->state->argency_id != 0 || $this->state->manufacturer_id != 0 || $this->state->brand_id != 0 || $this->state->category_id != 0 || $this->state->filter_state != 0 || $this->state->show_all_product != 0) {
            $display = "block";
        }
        ?>
        <div class="search-tools" style="display: <?php echo $display ?>;">
            <div class="search-tool-edit">
                <?php echo $this->lists['product_hot']; ?>
            </div>
            <div class="search-tool-edit">
                <?php echo $this->lists['product_new']; ?>
            </div>
            <div class="search-tool-edit">
                <?php echo $this->lists['argency_filter']; ?>
            </div>
            <div class="search-tool-edit">
                <?php echo $this->lists['manufacturer_filter']; ?>
            </div>
            <div class="search-tool-edit">
                <?php echo $this->lists['brand_filter']; ?>
            </div>
            <div class="search-tool-edit">
                <?php echo $this->lists['category_id']; ?>
            </div>
            <div class="search-tool-edit">
                <?php echo $this->lists['filter_state']; ?>
            </div>
            <div class="search-tool-edit">
                <?php
                if (OpenShopPermissions::checkAdmin(JFactory::getUser()->groups)) {
                    echo $this->lists['show_all_product'];
                }
                ?>
            </div>
        </div>


        <div id="editcell">
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th class="text_center" width="2%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_center" width="2%">
                            <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                        </th>
                        <th width="1%" class="text_center" style="min-width:55px">
                            <?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_left" width="15%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_NAME'), 'b.product_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th class="text_center" width="5%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_PRODUCT_SKU'), 'a.product_sku', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_center" width="7%">
                            <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_PRODUCT_PRICE'), 'a.product_price', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
<!--                        <th class="text_center" width="5%">
                        <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_PRODUCT_QUANTITY'), 'a.product_quantity', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>-->
                        <th class="text_center" width="18%">
                            <?php echo JText::_('OPENSHOP_CATEGORY'); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_NUMBER_BUY_PRODUCT'); ?>
                        </th>
                        <th width="10%" class="text_right">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ORDER'), 'a.ordering', $this->lists['order_Dir'], $this->lists['order']); ?>
                            <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'product.save_order'); ?>
                        </th>
                        <th class="text_center" width="4%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>													
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="13">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $link = JRoute::_('index.php?option=com_openshop&task=product.edit&cid[]=' . $row->id);
                        $class_pro = "row$k";
                        if ($row->delete_status) { //Deleted
                            $class_pro = 'delete_pro';
                        }
                        ?>
                        <tr class="<?php echo $class_pro ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <td class="text_center">
                                <?php echo JHtml::_('grid.id', $i, $row->id); ?>
                            </td>
                            <td class="text_center">
                                <div class="btn-group">
                                    <?php
                                    echo JHtml::_('jgrid.published', $row->published, $i, 'product.');
                                    echo $this->featured($row->product_featured, $i);
                                    ?>
                                </div>
                                <div class="btn-group">
                                    <?php
                                    if ($row->product_new == 1) {
                                        $clsNew = 'active';
                                        $upN = '0';
                                    } else {
                                        $clsNew = '';
                                        $upN = '1';
                                    }
                                    ?>
                                    <a class="btn btn-micro hasTooltip padding5 <?php echo $clsNew ?>"  title="" id="pN_<?php echo $row->id ?>" onclick="update_status_product(<?php echo $row->id ?>, 'product.updateNew',<?php echo $upN ?>, 'pN_<?php echo $row->id ?>', 'active')">
                                        <span>NEW</span>
                                    </a>
                                    <?php
                                    if ($row->product_hot == 1) {
                                        $clsHOT = 'active';
                                        $upH = '0';
                                    } else {
                                        $clsHOT = '';
                                        $upH = '1';
                                    }
                                    ?>
                                    <a class="btn btn-micro hasTooltip padding5 <?php echo $clsHOT ?>"  title="" id="pH_<?php echo $row->id ?>" onclick="update_status_product(<?php echo $row->id ?>, 'product.updateHOT',<?php echo $upH ?>, 'pH_<?php echo $row->id ?>', 'active')">
                                        <span>HOT</span>
                                    </a>
                                </div>
                            </td>
                            <td class="text_left">																			
                                <a href="<?php echo $link; ?>"><?php echo $row->product_name; ?></a>				
                            </td>																			
                            <td class="text_center">
                                <?php echo $row->product_sku; ?>
                            </td>
                            <td class="text_center">
                                <?php
                                if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $row->product_image)) {
                                    $viewImage = JFile::stripExt($row->product_image) . '-100x100.' . JFile::getExt($row->product_image);
                                    if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                        ?>
                                        <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" width="50" />
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image; ?>" width="50" />
                                        <?php
                                    }
                                }
                                ?>
                            </td>
                            <td class="text_center">																			
                                <?php
                                $productPriceArray = OpenShopHelper::getProductPriceArray($row->id, $row->product_price);
                                if ($productPriceArray['salePrice']) {
                                    ?>
                                                                    <!--<span class="base-price"><?php echo $this->currency->format($productPriceArray['basePrice'], OpenShopHelper::getConfigValue('default_currency_code')); ?></span>&nbsp;-->
                                                                    <!--<span class="sale-price"><?php echo $this->currency->format($productPriceArray['salePrice'], OpenShopHelper::getConfigValue('default_currency_code')); ?></span>-->
                                    <?php
                                } else {
                                    ?>
                                                                    <!--<span class="price"><?php echo $this->currency->format($productPriceArray['basePrice'], OpenShopHelper::getConfigValue('default_currency_code')); ?></span>-->
                                    <?php
                                }

                                //gia san pham
                                echo number_format($row->product_price, 0, ',', '.');
                                ?>
                            </td>
    <!--                            <td class="text_center">
                            <?php echo $row->product_quantity; ?>
                            </td>-->
                            <td class="text_center">
                                <?php
                                $categories = OpenShopHelper::getProductCategories($row->id);
                                for ($j = 0; $m = count($categories), $j < $m; $j++) {
                                    $category = $categories[$j];
                                    $editCategoryLink = JRoute::_('index.php?option=com_openshop&task=category.edit&cid[]=' . $category->id);
                                    $dividedChar = ($j < ($m - 1)) ? ' | ' : '';
                                    ?>
                                    <a href='<?php echo $editCategoryLink; ?>'><?php echo $category->category_name; ?></a><?php echo $dividedChar; ?>
                                    <?php
                                }
                                ?>
                            </td>

                            <td class="text_center">
                                <?php
                                if ($this->getNumberProductOfOrder($row->id) == 0) {
                                    echo '<span class="num_sale_0" >' . $this->getNumberProductOfOrder($row->id) . '</span>';
                                } else {
                                    echo '<span class="num_sale" onclick="show_modal_productInOrder(' . $row->id . ')">' . $this->getNumberProductOfOrder($row->id) . '</span>';
                                }
                                ?>
                            </td>
                            <td class="order text_right">
                                <span><?php echo $this->pagination->orderUpIcon($i, true, 'product.orderup', 'Move Up', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'product.orderdown', 'Move Down', $ordering); ?></span>
                                <?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?>				
                                <input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="input-mini" style="text-align: center" <?php echo $disabled; ?> />
                            </td>
                            <td class="text_center">
                                <?php
                                if ($row->delete_status) {
                                    echo '<div style="padding: 5px 5px; background: #5CB85C; cursor: pointer;">';
                                    echo '  <span class="icon-undo-2" onclick="restore_product(' . $row->id . ')"></span>';
                                    echo '</div>';
                                } else {
                                    echo $row->id;
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
            </table>
        </div>


        <!--PRODUCT IN ORDER-->
        <div class="modal fade" id="modal_productInOrder" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_PRODUCT_IN_ORDER') ?>: <i class="productName"></i></h3>
                        <div style="float: right;">
                            <button type="button" class="btn btn-default delete_order_cancel" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered" id="editcell">  
                            <thead>
                            <th><?php echo JText::_('OPENSHOP_STT') ?></th>
                            <th><?php echo JText::_('OPENSHOP_NAME') ?></th>
                            <th><?php echo JText::_('OPENSHOP_TELEPHONE') ?></th>
                            <th><?php echo JText::_('OPENSHOP_EMAIL') ?></th>
                            <th><?php echo JText::_('OPENSHOP_ADDRESS') ?></th>
                            <th><?php echo JText::_('OPENSHOP_QUANTITY') ?></th>
                            </thead>
                            <tbody id="info_productInOrder">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>
    </form>
</fieldset>



<!-- Import Sản phẩm bằng Excel -->
<form action="" method="post" enctype="multipart/form-data" class="form-horizontal" id="myUpoadFile">
    <div class="modal fade" id="myUploadFile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Import Sản phẩm bằng Excel</h4>
                </div>
                <div class="modal-body" style="display: block;">
                    <div class="uploadFileForm" style="opacity: 1;">
                        <div class="form-group">
                            <label class="col-md-3">Chọn file</label>
                            <div class="col-md-9">
                                <input type="file" name="fileUp" id="fileUp" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="progress  progress-striped progress-info active" data-max="1000" data-min="1" data-step="10" data-value="240">		
                                    <div class="bar barPercent" style="width: 0%;">0%</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="checkDataForm" style="display: none;">
                        <div class="form-group">
                            <div class="col-md-12">
                                Đang kiểm tra file...
                            </div>
                            <div class="col-md-12">
                                <div class="progress  progress-striped progress-info active" data-max="1000" data-min="1" data-step="10" data-value="240">		
                                    <div class="bar barPercent" style="width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="downloadPatern()">Tải file mẫu</button>
                    <button type="submit" class="btn btn-primary submitButtomUpload">
                        Upload
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Export Sản phẩm bằng Excel -->
<!--<form action="index.php?option=com_openshop&format=ajax&task=product.exportPro" method="post" enctype="multipart/form-data" class="form-horizontal" id="myFormExportPro">-->
<div class="modal fade" id="myExportPro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Export Sản phẩm bằng Excel</h4>
            </div>
            <div class="modal-body form-horizontal" style="display: block;">
                <div class="uploadFileForm" style="opacity: 1;">
                    <div class="form-group">
                        <label class="col-md-3">Chọn file</label>
                        <div class="col-md-9">
                            <select class="form-conthol" name="slt_chooseFile" id="slt_chooseFile">
                                <!--<option value="">- Chọn -</option>-->
                                <option value="1">File cập nhật kho NCC</option>
                            </select>
                        </div>
                    </div>

                    <!--File cập nhật NCC-->
                    <div class="file_1">
                        <div class="form-group">
                            <label class="col-md-3">Chọn NCC</label>
                            <div class="col-md-9">
                                <?php
                                $manus = OpenShopHelper::getManufactures();
                                ?>
                                <select class="form-control" name="slt_manu" id="slt_manu">
                                    <option value="">- Chọn -</option>
                                    <?php
                                    foreach ($manus as $manu) {
                                        ?>
                                        <option value="<?php echo $manu->id ?>"><?php echo $manu->manufacturer_name ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Chọn Loại SP</label>
                            <div class="col-md-9">
                                <select class="form-control" id="slt_typeP" name="slt_typeP">
                                    <option value="">- Chọn -</option>
                                    <option value="1">Tất cả sản phẩm đang ON, Kiểm tra để OFF</option>
                                    <option value="2">Tất cả sản phẩm đang OFF, Kiểm tra để ON</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-md-12 fileDownLoad">
                                <i>Hiện chưa có</i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <!--                    <button type="button" class="btn btn-primary" onclick="exportPro()">
                                        Tải xuống
                                    </button>-->
                <button type="button" class="btn btn-primary" onclick="LoadFileDownLoad()">Load tất cả file sẽ tải xuống</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
            </div>
        </div>
    </div>
</div>
<!--</form>-->


<form action="index.php?option=com_openshop&format=ajax&task=product.paternimportProduct" method="post" id="downloadImportProduct">
</form>

<script>
    function show_search_tools()
    {
        jQuery('.search-tools').toggle(400);
    }

    Joomla.submitbutton = function (task) {
        switch (task) {
            case 'importProduct':
                jQuery('#myUploadFile').modal();
                break;
            case 'exportProduct':
                jQuery('#myExportPro').modal();
                break;
            default:
                Joomla.submitform(task);
                break;
        }
    }
</script>



