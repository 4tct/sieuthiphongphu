<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
//print_r($this->items); exit();
?>

<style>
    #editcell tr td, #editcell tr th{
        vertical-align: middle;
        padding: 5px;
    }

    #tbl-quantity{
        margin: 0 auto;
    }

    #tbl-quantity tr td, #tbl-quantity tr th{
        vertical-align: middle;
        padding: 5px;
    }
</style>

<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=inventories" method="post" name="adminForm" id="adminForm">
        <table width="100%" style="margin-bottom: 5px;">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>
                </td>
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th width="2%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_left" width="30%">
                            <?php echo JText::_('OPENSHOP_NAME'); ?>				
                        </th>
                        <th class="text_center" width="5%">
                            <?php echo JText::_('OPENSHOP_IMAGE'); ?>				
                        </th>
                        <th width="20%" class="text_center" style="min-width:55px">
                            <?php echo JText::_('OPENSHOP_QUANTITY'); ?>
                        </th>
                        <th width="5%" class="text_center">
                            <?php echo JText::_('OPENSHOP_ID'); ?>
                        </th>													
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="7">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $pro = $this->getProduct($row->product_id);
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <td>
                                <?php
                                echo $pro->product_name . ' - <b>' . $pro->product_sku . '</b>';
                                ?>
                            </td>
                            <td class="text_center">
                                <?php
                                if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $pro->product_image)) {
                                    $viewImage = JFile::stripExt($pro->product_image) . '-100x100.' . JFile::getExt($pro->product_image);
                                    if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                        ?>
                                        <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" width="100" />
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pro->product_image; ?>" width="100" />
                                        <?php
                                    }
                                }
                                ?>
                            </td>
                            <td class="text_center">
                                <?php
                                $size = $this->getSizeColor($row->warehouse_id, $row->product_id, 'size');
                                $color = $this->getSizeColor($row->warehouse_id, $row->product_id, 'color');
                                ?>
                                <table id="tbl-quantity">
                                    <tr>
                                        <?php
                                        if (OpenShopHelper::getConfigValue('managesizecolor') == '2') {
                                            echo '<td></td>';
                                        }
                                        foreach ($size as $v_size) {
                                            echo '<td>' . $v_size->value . '</td>';
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                    if (empty($color)) {
                                        echo '<tr>';
                                        foreach ($size as $v_size) {
                                            echo '<td>' . $this->getQauntityInWarehouse($row->product_id, $v_size->idsize, 0, $row->warehouse_id) . '</td>';
                                        }
                                        echo '</tr>';
                                    } else {
                                        foreach ($color as $v_color) {
                                            echo '<tr>';
                                            if (OpenShopHelper::getConfigValue('managesizecolor') == '2') {
                                                echo '  <td>' . $v_color->value . '</td>';
                                            }
                                            foreach ($size as $v_size) {
                                                echo '<td>' . $this->getQauntityInWarehouse($row->product_id, $v_size->idsize, $v_color->idcolor, $row->warehouse_id) . '</td>';
                                            }
                                            echo '</tr>';
                                        }
                                    }
                                    ?>

                                </table>
                            </td>
                            <td class="text_center">
                                <?php echo $row->product_id; ?>
                            </td>
                        </tr>		
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
                </tbody>
            </table>
        </div>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>