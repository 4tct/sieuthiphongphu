<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class openshopViewInventories extends OpenShopViewList {
    function _buildToolbar() {
        JToolbarHelper::title(JText::_('OPENSHOP_INVENTORIES'));
    }
    
    function getProduct($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.product_name,a.product_sku,a.product_image')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . 'ON a.id = b.product_id')
                ->where('a.id = ' . $id);
        
        return $db->setQuery($query)->loadObject(); 
    }
    
    function getSizeColor($id_warehouse,$id_pro,$act){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->from($db->quoteName('#__openshop_inventories','a'))
                ->where('a.product_id = ' . $id_pro)
                ->where('a.warehouse_id = ' . $id_warehouse);
        
        switch ($act){
            case 'size':
                $query->select('a.idsize,b.value')
                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails','b'))
                    ->where('a.idsize = b.optionvalue_id')
                    ->group('a.idsize');
                break;
            case 'color':
                $query->select('a.idcolor,b.value')
                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails','b'))
                    ->where('a.idcolor = b.optionvalue_id')
                    ->group('a.idcolor');
                break;
            default :
                break;
        }
        
        return $db->setQuery($query)->loadObjectList();
    }
    
    function getQauntityInWarehouse($id_pro,$idsize,$idcolor,$warehou_id){
        $db = JFactory::getDbo();
        
        $quantity = $db->setQuery('CALL TotalInventory("import",'. $id_pro .','. $idsize .','. $idcolor .','. $warehou_id .')')->loadResult();      
        
        return isset($quantity) ? $quantity : '0';
    }
}
