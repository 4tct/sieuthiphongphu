<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewManufacturers extends OpenShopViewList {

    public function sumProductsInMan($man_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("COUNT(*) as SL")
                ->from('#__openshop_products')
                ->where('manufacturer_id=' . $man_id)
                ->where('published =1');
        $db->setQuery($query);
        $proOn = $db->loadResult();
        $query->clear();
        $query->select("COUNT(*) as SL")
                ->from('#__openshop_products')
                ->where('manufacturer_id=' . $man_id)
                ->where('published =0');
        $proOff = $db->setQuery($query)->loadResult();
        return $proOn."_-_".$proOff;
    }

}
