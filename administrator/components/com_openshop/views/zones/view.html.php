<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewZones extends OpenShopViewList
{

	function _buildListArray(&$lists, $state)
	{
		$db = JFactory::getDbo();
		//Build countries list
		$query = $db->getQuery(true);
		$query->select('id AS value, country_name AS text')
			->from('#__openshop_countries')
			->where('published = 1')
			->order('country_name');
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$options = array();
		$options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_SELECT_A_COUNTRY'), 'value', 'text');
		if (count($rows))
		{
			$options = array_merge($options, $rows);
		}
		$lists['countries'] = JHtml::_('select.genericlist', $options, 'country_id', 
			array(
				'option.text.toHtml' => false, 
				'option.value' => 'value', 
				'option.text' => 'text', 
				'list.attr' => ' class="inputbox" onchange = "this.form.submit();" ', 
				'list.select' => $state->country_id));
		return true;
	}
}