<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/ios.js');
JFactory::getDocument()->addStyleSheet(JUri::base() . DS . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'css' . DS . 'io.css');
JFactory::getDocument()->addScript(JUri::base() . '/components/com_openshop/assets/js/jquery.form.js');
?>

<style>
    #uploadForm {border-top:#F0F0F0 2px solid;background:#FAF8F8;padding:10px;}
    #uploadForm label {margin:2px; font-size:1em; font-weight:bold;}
    .demoInputBox{padding:5px; border:#F0F0F0 1px solid; border-radius:4px; background-color:#FFF;}
    #progress-bar {background-color: #12CC1A;height:20px;color: #FFFFFF;width:0%;-webkit-transition: width .3s;-moz-transition: width .3s;transition: width .3s;}
    .btnSubmit{background-color:#09f;border:0;padding:10px 40px;color:#FFF;border:#F0F0F0 1px solid; border-radius:4px;}
    #progress-div {border:#0FA015 1px solid;padding: 5px 0px;margin:30px 0px;border-radius:4px;text-align:center;}
    #targetLayer{width:100%;text-align:center;}

    .file-upload {
        position: relative;
        overflow: hidden;
    }

    .file-upload input.file-input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0); 
    }

    .btn-file-input{
        background-color: rgb(116, 225, 121) !important;
        border: none !important;
    }

    .btn-upload{
        padding: 5px 20px;
        background: rgb(109, 154, 255);
        border: none;
        margin-top: 10px;
        color: white;
    }

    .btn-upload:hover
    {
        background-color: rgb(22, 94, 255);
    }

</style>

<script type="text/javascript">
    $(document).ready(function () {

        $('#uploadForm').ajaxForm({
            beforeSubmit: function () {
                $("#progress-bar").width('0%');
            },
            uploadProgress: function (event, position, total, percentComplete) {
                $("#progress-bar").width(percentComplete + '%');
                $("#progress-bar").html('<div id="progress-status">' + percentComplete + ' %</div>')
            },
            success: function () {
                $('#loader-icon').hide();
                return false;
            }
        });
    });
</script>



<div class="col-ms-12">
    <div id="slickbox">
        <form action="index.php?option=com_openshop&view=ios" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" >
            <div class="col-md-4 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo JText::_('OPENSHOP_IO_INFORMATION'); ?></div>
                    <div class="panel-body">
                        <table class="table-hover" width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th><?php echo JText::_('OPENSHOP_IO_ID_PRODUCT'); ?></th>
                                <td><input id="id_product" name="id_product" type="text" onkeyup="check_idproduct()" />
                                    <input type="hidden" id="ul_img" name="ul_img" value="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP; ?>"/>
                                    <input type="hidden" id="idpro_auto" name="idpro_auto" value=""/>
                                    <div style="background-color:#f0ad4e;border-radius: 5px;" id="warning_product" name="warning_product"></div>
                                    <input type="hidden" name="productnew" id="productnew" value="" />
                                    <!--search product-->
                                    <div class="arrow-up"  style="display:none;"></div>
                                    <table id="load" name="load" class="search-product" border="1">
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo JText::_('OPENSHOP_IO_NAME_PRODUCT'); ?></th>
                                <td><input type="text" id="nameproduct" name="nameproduct" /></td>
                            </tr>
                            <tr>
                                <th><?php echo JText::_('OPENSHOP_IO_PRICE_INPUT'); ?></th>
                                <td>
                                    <input type="text" name="price" id="price" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo JText::_('OPENSHOP_IO_ID_INVOICE'); ?>:</th>
                                <td>
                                    <input type="text" class="form-control" id="invoice_sku" name="invoice_sku" value="<?php echo date('d-m-Y') ?>" aria-describedby="basic-addon1" style="width: 120px"/>
                                    <select class="buoi" style="width: 70px" id="lenofday" name="lenofday">
                                        <option value="0">-</option>
                                        <option value="S">S</option>
                                        <option value="C">C</option>
                                        <option value="T">T</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo JText::_('OPENSHOP_IO_ID_PRODUCT_MANUFACTURER_NAME'); ?></th>
                                <td><input type="text" id="idproductmanufac" name="idproductmanufac"/></td>
                            </tr>
                            <tr>
                                <th><?php echo JText::_('OPENSHOP_IO_MANUFACTURER_NAME'); ?></th>
                                <td><!--<input type="text" id="namemanufac" name="namemanufac" />-->
                                    <div class="id_100">
                                        <?php echo $this->selectmanufac(""); ?>
                                    </div>
                                    <input type="hidden" id="idmanufac" name="idmanufac" />
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo JText::_('OPENSHOP_IO_STATUS'); ?></th>
                                <td>
                                    <?php echo $this->lists['statusios']; ?>
                                </td>
                            </tr>
                            <tr>

                                <th><div id="input_text"><?php //echo JText::_('OPENSHOP_WAREHOUSE');      ?></div></th>
                                <td><div id="input"><?php //echo $this->lists['id_warehouse'];      ?></div></td>

                            </tr>
                            <tr>
                                <th><div id="output_text"><?php //echo JText::_('OPENSHOP_WAREHOUSE_OUTPUT');      ?></div></th>
                                <td><div id="output"><?php //echo $this->lists['id_warehouse_form'];      ?>
                                        <?php //echo $this->lists['id_warehouse_to']; ?></div>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table id="table_price" class="table table-hover">

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="loadimages">
                                        <input name="myfile" id="myfile" type="file" /><br />
                                        <progress id="prog" value="0" max="100" min="0"></progress>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-8 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo JText::_('OPENSHOP_IO_INFORMATION_PRODUCT'); ?></div>
                    <div class="panel-body">

                        <table class="table table-hover table-striped" id="attr" name="attr">

                        </table>
                        <div class="center">
                            <?php $d = 0; ?>
<!--                            <input type="button" id="add_rowattr" onclick="add_row(<?php $d++; ?>)" class="btn btn-primary"
                                   value="<?php echo JText::_('OPENSHOP_IO_ADD'); ?>"/>-->

                            <input type="button" id="add_rowattr" onclick="add_row_sizecolor(<?php $d++; ?>)" class="btn btn-primary"
                                   value="<?php echo JText::_('OPENSHOP_IO_ADD'); ?>"/>
                        </div>
                        <input  type="hidden" id="numrow" name="numrow" value="0"/>
                        <table class="table table-hover table-striped" id="attr_product_input" name="attr_product_input">

                        </table>
                    </div>
                </div>
            </div>
            <input type="hidden" id="sodongmax" name="sodongmax" value="0" />
        </form>
    </div>
</div>


<fieldset class="adminfs col-lg-12 col-md-12">
    <form action="index.php?option=com_openshop&view=ios" method="post" name="adminForm1" id="adminForm1">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>

                    <?php
                    $filter = array(
                        'search', 'invoice_id:0', 'warehouse_id:0', 'manufacturer_id:0', 'date_from', 'date_to'
                    );
                    ?>
                    <button onclick='reset_filter(<?php echo json_encode($filter); ?>);' class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>	


                    <!--search-->
                    <span class="btn btn-default hidden-phone" onclick="show_search_tools();"><?php echo JText::_('OPENSHOP_SEARCHTOOLS') ?><span class="icon-arrow-down-3 edit-arrow-icon"></span></span>
                </td>
                <td>
                    <div class="form-inline" align="right">
                        <?php
                        $total = '0';
                        foreach ($this->items as $value) {
                            $total += $value->price_input;
                        }
                        ?>
                        <span>Tổng số lượng: <?php echo count($this->items); ?></span>
                        <br/>
                        <span>Tổng tiền: <?php echo number_format($total, 0, ',', '.'); ?></span>
                    </div>
                </td>
            </tr>
        </table>

        <?php
        $display = "none";
        if ($this->state->warehouse_id != 0 || $this->state->manufacturer_id != 0 || $this->state->invoice_id != 0 || $this->state->date_from != '' || $this->state->date_to != '') {
            $display = "block";
        }
        ?>
        <!--search tools-->
        <div class="search-tools" style="display: <?php echo $display ?>;">
            <div class="search-tool-edit">
                <?php
                echo $this->lists['filter_invoice'];
                ?>
            </div>
            <div class="search-tool-edit">
                <?php
                echo $this->lists['filter_warehouse'];
                ?>
            </div>
            <div class="search-tool-edit">
                <?php
                echo $this->lists['filter_manufacturer'];
                ?>
            </div>
            <div class="search-tool-edit calendar-edit">
                <?php echo JHtml::_('calendar', $this->state->date_from, 'date_from', 'date_from', '%d-%m-%Y', array('style' => 'width: 100px;', 'placeholder' => JText::_('OPENSHOP_DATE_FROM'), 'onchange' => 'this.form.submit()')); ?>
            </div>
            <div class="search-tool-edit calendar-edit">
                <?php echo JHtml::_('calendar', $this->state->date_to, 'date_to', 'date_to', '%d-%m-%Y', array('style' => 'width: 100px;', 'placeholder' => JText::_('OPENSHOP_DATE_TO'), 'onchange' => 'this.form.submit()')); ?>
            </div>
        </div>
        <br>
        <div id="editcell">
            <table class="adminlist table table-striped" id="show_info_data">
                <thead>
                    <tr>
                        <th width="3%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_left" width="10%">
                            <?php echo JText::_('OPENSHOP_INVOICE'); ?>				
                        </th>
                        <th class="text_left" width="20%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_PRODUCT_NAME'), 'product_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th class="text_left" width="10%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_IO_MANUFACTURER_NAME'), 'manufacturer_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th class="text_left" width="20%">
                            <?php echo JText::_('OPENSHOP_WAREHOUSE'); ?>				
                        </th>
                        <th class="text_left" width="10%">
                            <?php echo JText::_('OPENSHOP_IO_DATE_INPUT'); ?>				
                        </th>
                        <th class="text_left" width="8%">
                            <?php echo JText::_('OPENSHOP_PRICE'); ?>				
                        </th>
                        <th class="text_left" width="10%">
                            <?php echo JText::_('OPENSHOP_QUANTITY'); ?>				
                        </th>
                        <th width="8%" class="text_center">
                            <?php echo JText::_('OPENSHOP_USER'); ?>
                        </th>	
                        <th width="4%" class="text_center">
                            <?php echo JText::_('OPENSHOP_CONFIRMED'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody id="insert">
                    <?php
                    foreach ($this->items as $key => $row) {
                        $sizecolor = $this->getSizeColor($row->id_optionsize, $row->id_optioncolor);
                        ?>
                        <tr>
                            <td><?php echo $key + 1 ?></td>
                            <td><?php echo $row->invoice_sku ?></td>
                            <td>
                                <?php echo $row->product_name ?>
                                <br/>
                                <b><?php echo $row->product_sku ?></b>
                            </td>
                            <td><?php echo $row->manufacturer_name ?></td>
                            <td>
                                <?php
                                echo $row->warehouse_name;
                                if ($row->id_status == 2) {
                                    $warehouse_to = OpenShopHelper::getWareHouse($row->id_warehouse_to);
                                    echo ' => ' . $warehouse_to->warehouse_name;
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $date = explode(' ', $row->created_date);
                                echo $date[1] . ' <br> ' . date('d-m-Y', strtotime($date[0]));
                                ?>
                            </td>
                            <td><?php echo number_format($row->price_input, 0, ',', '.') ?></td>
                            <td>
                                <table class="tab-pad" style="margin: 0 auto;">
                                    <tr>
                                        <?php
                                        if (OpenShopHelper::getConfigValue('managesizecolor') == 2)
                                            echo '<td></td>';
                                        ?>
                                        <td><?php echo $sizecolor['size'] ?></td>
                                    </tr>
                                    <tr>
                                        <?php
                                        if (OpenShopHelper::getConfigValue('managesizecolor') == 2)
                                            echo '<td>' . $sizecolor['color'] . '</td>';
                                        ?>
                                        <td><?php echo $row->quantity ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <?php echo JFactory::getUser($row->modified_by)->name ?>
                            </td>
                            <td>
                                <?php
                                if ($row->xacnhan == '1') {
                                    echo '<i class="icon-ok"></i>';
                                } else {
                                    echo '<i class="icon-cancel" style="color:red; cursor:pointer;" title=' . JText::_('OPENSHOP_RECOVER_TITLE') . ' onclick="showRecover(' . $row->id . ',\'' . $row->product_name . '\',\'' . $row->product_sku . '\')"></i>';
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <div class="loading_img" style="display:none; text-align: center;">
                <img style="width: 60px; background: no-repeat" src="components/com_openshop/assets/images/loading_1.gif" />
            </div>
        </div>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="cid[]" id="cid" value="" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
        <input type="hidden" id="config_manage_sizecolor" value="<?php echo OpenShopHelper::getConfigValue('managesizecolor'); ?>" />
        <input type="hidden" id="limit_ajax" value="0" />
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>

<div class="modal fade" id="myModalRecover" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="    margin-top: 230px;">
        <div class="modal-content">
            <div class="modal-body">
                <span style="font-size:16px">
                    <?php echo JText::_('OPENSHOP_RECOVER_CONFIRM_IO_DESC') ?>
                    <b><span class="nameProduct"></span></b>
                    <input type="hidden" id="idIOrecover" value=""/>
                </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="recoverIO()"><i class="icon-apply"></i> <?php echo JText::_('OPENSHOP_RECOVER') ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-cancel" style="color:red;"></i>  <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
            </div>
        </div>
    </div>
</div>