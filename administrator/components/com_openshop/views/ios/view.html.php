<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewIos extends OpenShopViewList {

    public function _buildListArray(&$lists, $state) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        //warehouse
        $query->select('id AS value, warehouse_name AS text')
                ->from($db->quoteName('#__openshop_warehouses'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_IO_WAREHOUSE_FILTER'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['filter_warehouse'] = JHtml::_('select.genericlist', $options, 'warehouse_id', ' class="form-control select-width-180 inputbox" onchange="this.form.submit();" ', 'value', 'text', $state->warehouse_id);
        $lists['id_warehouse'] = JHtml::_('select.genericlist', $options, 'id_warehouse', ' class="form-control inputbox" style="width: 205px" ', 'value', 'text');

        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_IO_WAREHOUSE_FROM'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['filter_warehouse_form'] = JHtml::_('select.genericlist', $options, 'warehouse_id', ' class="form-control select-width-180 inputbox" onchange="this.form.submit();" ', 'value', 'text', $state->warehouse_id);
        $lists['id_warehouse_form'] = JHtml::_('select.genericlist', $options, 'id_warehouse_from', ' class="form-control inputbox" style="width: 205px" ', 'value', 'text');
        
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_IO_WAREHOUSE_TO'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['filter_warehouse_to'] = JHtml::_('select.genericlist', $options, 'warehouse_id', ' class="form-control select-width-180 inputbox" onchange="this.form.submit();" ', 'value', 'text', $state->warehouse_id);
        $lists['id_warehouse_to'] = JHtml::_('select.genericlist', $options, 'id_warehouse_to', ' class="form-control inputbox" style="width: 205px" ', 'value', 'text');
        
        
        //manufacturer
        $query->clear();
        $query->select('id AS value, manufacturer_name AS text')
                ->from($db->quoteName('#__openshop_manufacturers'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_IO_MANUFACTURER_FILTER'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['filter_manufacturer'] = JHtml::_('select.genericlist', $options, 'manufacturer_id', ' class=" form-control select-width-180 inputbox" onchange="this.form.submit();" ', 'value', 'text', $state->manufacturer_id);
        
        //status ios
        $query->clear();
        $query->select('id AS value, status_name AS text')
                ->from($db->quoteName('#__openshop_status_ios'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_STATUS'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['statusios']= JHtml::_('select.genericlist', $options, 'statusios', ' class=" form-control " style="width: 205px" ', 'value', 'text');

        //invoice
        $query->clear();
        $query->select('distinct invoice_sku as value, invoice_sku as text')
                ->from($db->quoteName('#__openshop_ios'));
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_IO_INVOICE_FILTER'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['filter_invoice'] = JHtml::_('select.genericlist', $options, 'invoice_id', ' class="form-control select-width-180 inputbox" onchange="this.form.submit();" ', 'value', 'text', $state->invoice_id);
    }

    function _buildToolbar() {
        $viewName = $this->getName();
        $controller = OpenShopInflector::singularize($this->getName());
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
        //JToolBarHelper::preferences('com_openshop','550','850');
        JToolBarHelper::custom('show_io', 'menu-3', 'menu-3', JText::_('OPENSHOP_SHOW_ADD_IO'), false);
        JToolBarHelper::custom('save_io', 'new', 'new', JText::_('OPENSHOP_SAVE'), false);
        JToolBarHelper::custom('reset_io', 'refresh ', 'refresh ', JText::_('OPENSHOP_RESET_FORM'), false);
    }

    function getSizeColor($id_size, $id_color) {
        $sizecolor = array();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        //size
        $query->select('value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('optionvalue_id = ' . $id_size);
        $db->setQuery($query);
        $sizecolor['size'] = $db->loadResult();

        //color
        $query->clear();
        $query->select('value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('optionvalue_id = ' . $id_color);
        $db->setQuery($query);
        $sizecolor['color'] = $db->loadResult();

        return $sizecolor;
    }

    function selectmanufac($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id AS value, manufacturer_name AS text')
                ->from($db->quoteName('#__openshop_manufacturers'));
        $db->setQuery($query);
        $options = array();
        if ($id == "") {
            $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_IO_SELECT_MANUFACTURERS'));
            $options = array_merge($options, $db->loadObjectList());
            $lists = JHtml::_('select.genericlist', $options, 'namemanufac', ' class="form-control" style="width: 205px"', 'value', 'text');
            return $lists;
        } else {
            $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_IO_SELECT_MANUFACTURERS'));
            $options = array_merge($options, $db->loadObjectList());
            $lists = JHtml::_('select.genericlist', $options, 'namemanufac', ' class="form-control" style="width: 205px" disabled', 'value', 'text', isset($id) ? $id : "");
            return $lists;
        }
    }
    
//    function getW

}
