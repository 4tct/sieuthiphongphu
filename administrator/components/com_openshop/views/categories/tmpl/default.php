<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

$ordering = ($this->lists['order'] == 'a.ordering');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');

$session = JFactory::getSession();
print_r($session->get('menu_toggle'));
?>

<style>
    #editcell tr td, #editcell tr th{
        vertical-align: middle;
    }
    .border-number-product{
        padding: 5px 20px;
        background: #67F8FF;
        border-radius: 18px;
        cursor: pointer;
    }
    .border-number-product-0{
        padding: 5px 20px;
        background: #C7CFD0;
        border-radius: 18px;
    }
</style>

<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=categories" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>		
                    <?php echo $this->lists['filter_state']; ?>
                </td>
                <td  class="text-right">
                    <?php echo JText::_("Số SP / Trang ") . $this->pagination->getLimitBox(); ?>

                </td>
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th width="2%" class="text_center">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th width="2%" class="text_center">
                            <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                        </th>
                        <th width="1%" class="text_center" style="min-width:55px">
                            <?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_left" width="40%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_NAME'), 'b.category_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>	 
                        <th width="10%">
                            <?php echo JText::_('OPENSHOP_IMAGES') ?>
                        </th>
                        <th width="10%">
                            <?php echo JText::_('OPENSHOP_NUMBER_PRODUCT') ?>
                        </th>
                        <th width="10%" class="text_right">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ORDER'), 'a.ordering', $this->lists['order_Dir'], $this->lists['order']); ?>
                            <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'category.save_order'); ?>
                        </th>
                        <th width="5%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>													
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="6" class="text_center">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $link = JRoute::_('index.php?option=com_openshop&task=category.edit&cid[]=' . $row->id);
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        $published = JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', 'category.');
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <td class="text_center">
                                <?php echo $checked; ?>
                            </td>
                            <td class="text_center">
                                <div class="btn-group">
                                    <?php
                                    echo JHtml::_('jgrid.published', $row->published, $i, 'category.');
                                    echo $this->addDropdownList(JText::_('OPENSHOP_COPY'), 'copy', $i, 'category.copy');
                                    echo $this->addDropdownList(JText::_('OPENSHOP_DELETE'), 'trash', $i, 'category.remove');
                                    echo $this->renderDropdownList($this->escape($row->category_name));
                                    ?>
                                </div>
                            </td>
                            <td>																			
                                <a href="<?php echo $link; ?>"><?php echo $row->treename; ?></a>				
                            </td>
                            <td style="text-align: center;">
                                <?php
                                if (empty($row->category_image)) {
                                    $img_categogy = 'no-image.png';
                                } else {
                                    $img_categogy = $row->category_image;
                                }

                                echo '<img src="' . OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $img_categogy . '" width="60px"/>';
                                ?>
                            </td>
                            <td style="text-align: center;" >
                                <?php
                                if ($this->getNumberProductCategory($row->id) == '0') {
                                    $class = 'border-number-product-0';
                                } else {
                                    $class = 'border-number-product';
                                }
                                ?>
                                <span class="<?php echo $class ?>" onclick="return window.open('index.php?option=com_openshop&view=products&category_id=<?php echo $row->id; ?>')" ><?php echo $this->getNumberProductCategory($row->id) ?></span>
                            </td>
                            <td class="order text_right">
                                <span><?php echo $this->pagination->orderUpIcon($i, true, 'category.orderup', 'Move Up', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'category.orderdown', 'Move Down', $ordering); ?></span>
                                <?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?>				
                                <input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="input-mini" style="text-align: center" <?php echo $disabled; ?> />
                            </td>
                            <td class="text_center" width="5%">
                                <?php echo $row->id; ?>
                            </td>
                        </tr>		
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>