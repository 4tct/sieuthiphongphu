<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(OPENSHOP_ADMIN_JS . DS .'script_admin.js');

?>
<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=menus" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>
                </td>
                <td align="right">
                    <?php echo $this->lists['menu_parent_id']; ?>
                    <?php echo $this->lists['filter_state']; ?>
                </td>
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th width="2%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th width="2%" class="text_center">
                            <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                        </th>
                        <th class="text_left" width="35%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_NAME'), 'a.menu_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th class="text_center" width="5%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_MENU_ACCESS'), 'a.access', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th width="5%" class="text_center" style="min-width:55px">
                            <?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th width="10%" class="text_right">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ORDER'), 'a.ordering', $this->lists['order_Dir'], $this->lists['order']); ?>
                            <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'menu.save_order'); ?>
                        </th>
                        <th width="5%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>													
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="7">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $lv_menu = substr($row->treename, 0, (strlen($row->treename) - strlen($row->menu_name)));
                        $menu_name_jtext = substr($row->treename, (strlen($row->treename) - strlen($row->menu_name)), strlen($row->menu_name));
                        $link = JRoute::_('index.php?option=com_openshop&task=menu.edit&cid[]=' . $row->id .'&parent_id='. $row->parent_id.'&com_id='.$row->menu_com);
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        $published = JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', 'menu.');
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <td class="text_center">
                                <?php echo $checked; ?>
                            </td>
                            <td>
                                <?php
                                if($menu_name_jtext != '')
                                    echo '<a href="'. $link .'">'. $lv_menu .' '. JText::_($menu_name_jtext) .'</a>';
                                else
                                    echo '<a href="'. $link .'">'. JText::_($row->menu_name) .'</a>';
                                ?>
                            </td>
                            <td class="text_center">
                                <?php
                                $sts = 1 - $row->access;
                                $icon = $sts === 0 ? "icon-ok" : "icon-cancel";
                                echo '<div class="btn btn-toggle '. $icon .'"  onclick=\'update_status_toggle("access_'. $row->id .'_'. $sts .'","menus","access","'. $sts .'","id","'. $row->id .'","menus.update_access","'. JUri::base() .'");\' id="access_'. $row->id .'_'. $sts .'">';
                                echo '</div>';
                                ?>
                            </td>
                            <td class="text_center">
                                <?php
                                $s_p = 1 - $row->published;
                                $i_p = $s_p === 0 ? "icon-ok" : "icon-cancel";
                                echo '<div class="btn btn-toggle '. $i_p .'"  onclick=\'update_status_toggle("published_'. $row->id .'_'. $s_p .'","menus","published","'. $s_p .'","id","'. $row->id .'","menus.update_access","'. JUri::base() .'");\' id="published_'. $row->id .'_'. $s_p .'">';
                                echo '</div>';
                                ?>
                            </td>
                            <td class="order text_right">
                                <span><?php echo $this->pagination->orderUpIcon($i, true, 'menu.orderup', 'Move Up', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'menu.orderdown', 'Move Down', $ordering); ?></span>
                                <?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?>				
                                <input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="input-mini" style="text-align: center" <?php echo $disabled; ?> />
                            </td>
                            <td class="text_center">
                                <?php echo $row->id; ?>
                            </td>
                        </tr>		
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
                </tbody>
            </table>
        </div>
        <input type="hidden" name="option" value="com_openshop" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
	<?php echo JHtml::_( 'form.token' ); ?>	
        <input type="hidden" id="url_admin" value="<?php echo JUri::base(); ?>" />
    </form>
</fieldset>