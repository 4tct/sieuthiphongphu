<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewMenus extends OpenShopViewList {
   
    function _buildToolbar() {
        $viewName = $this->getName();
        $controller = OpenShopInflector::singularize($this->getName());
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
        
        if (OpenShopHelper::getPermissionAction($viewName, 'create')) {
            JToolBarHelper::addNew($controller . '.add');
        }
        if (OpenShopHelper::getPermissionAction($viewName, 'edit'))
            JToolBarHelper::editList($controller . '.edit');
        if (OpenShopHelper::getPermissionAction($viewName, 'create')) {
            JToolBarHelper::custom($controller . '.copy', 'copy.png', 'copy_f2.png', JText::_('OPENSHOP_COPY'), true);
        }
        if (OpenShopHelper::getPermissionAction($viewName, 'delete')){
            JToolBarHelper::deleteList(JText::_($this->lang_prefix . '_DELETE_' . strtoupper($this->getName()) . '_CONFIRM'), $controller . '.remove');
        }
        JToolBarHelper::preferences('com_openshop','550','850');
        //JToolBarHelper::custom('userdetails.sendMail', 'mail.png', 'mail_f2.png', 'Send Mail', false);
        
    }

    public function _buildListArray(&$lists, $state) {
        $options = array();
        $options[] = JHtml::_('select.option', '', JText::_('OPENSHOP_SELECT_LEVELS'));
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_SELECT_PARENTS'));
        $options[] = JHtml::_('select.option', 1, JText::_('OPENSHOP_SELECT_CHILDREN'));
        $lists['menu_parent_id'] = JHtml::_('select.genericlist', $options, 'menu_parent_id', array(
                    'option.text.toHtml' => false,
                    'option.text' => 'text',
                    'option.value' => 'value',
                    'list.attr' => ' class="inputbox" onchange="this.form.submit();"',
                    'list.select' => $state->menu_parent_id));
    }
    

}
