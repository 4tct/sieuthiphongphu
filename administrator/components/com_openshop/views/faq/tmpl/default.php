<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Resthicted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$thanslatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'brand.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
            Joomla.submitform(pressbutton, form);
        }
    }
</script>

<style>
    .btn_question_answer{
        text-align: center;
        margin: 20px 0 0 0;
    }
    .btn_question_answer span{
        padding: 10px 100px;
        background: #148414;
        color: white;
    }
    .btn_question_answer span:hover{
        background: #1FAD1F;
        box-shadow: 0 2px 10px 0 black;
        cursor: pointer;
    }
    #table-faq tr td, #table-faq tr th{
        vertical-align: middle;
    }
    #table-faq tr td{
        text-align: center;
    }
</style>

<form action="index.php" method="post" class="adminfs" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row-fluild">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo JText::_('OPENSHOP_FAQS_TITLE') ?></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3"><?php echo JText::_('OPENSHOP_FAQS_NAME') ?></label>
                        <div class="col-md-9">
                            <input type="text" name="faq_name" class="form-conthol" value="<?php echo $this->item->faq_name ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo JText::_('OPENSHOP_QUESTIONS_ANSWERS') ?></div>
                <div class="panel-body">
                    <table class="table" id="table-faq">
                        <thead>
                            <tr>
                                <th width="5%"><?php echo JText::_('OPENSHOP_NUM') ?></th>
                                <th><?php echo JText::_('OPENSHOP_QUESTION') ?></th>
                                <th class="widthFAQanswer"><?php echo JText::_('OPENSHOP_ANSWER') ?></th>
                                <th width="5%"></th>
                            </tr>
                        </thead>
                        <tbody class="question_answer">
                            <?php
                            if (isset($this->item->id)) {
                                $dt = $this->getQuestionAnswer($this->item->id);
                                foreach ($dt as $key => $value) {
                                    $num = $key + 1;
                                    echo '<tr class="tr_' . $num . '">';
                                    echo '  <td><span class="stt_' . $num . '">' . $num . '</span></td>';
                                    echo '  <td>';
                                    echo '      <textarea class="form-control faq_question_' . $num . '" name="faq_question_' . $num . '" rows="10">' . $value->faq_question . '</textarea>';
                                    echo '  </td>';
                                    echo '  <td>';
//                                    echo '      <textarea class="form-control faq_answer_' . $num . '" name="faq_answer_' . $num . '">' . $value->faq_answer . '</textarea>';
                                    echo $editor->display('faq_answer_' . $num, $value->faq_answer , '100%', '200', '20', '20', FALSE);
                                    echo '  </td>';
                                    echo '  <td>';
                                    echo '      <span class="icon-delete" id="deleteFAQ_' . $num . '" style="color: red; cursor: pointer;" onclick="deteleFAQ(' . $num . ')"></span>';
                                    echo '  </td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                    <div class="btn_question_answer">
                        <span onclick="addQuestionAnswer()"><?php echo JText::_('OPENSHOP_ADD') ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php echo JHtml::_('form.token'); ?>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="num_question_answer" id="num_question_answer" value="<?php echo isset($dt) ? count($dt) : '0' ?>" />
</form>

<script>
    $ = jQuery.noConflict();

    function addQuestionAnswer() {
        var num = $('#num_question_answer').val();
        var h = '';
        h += '<tr class="tr_' + ++num + '">';
        h += '  <td>';
        h += '      <span class="stt_' + num + '">' + num + '</span>';
        h += '  </td>';
        h += '  <td>';
        h += '      <textarea class="form-control faq_question_' + num + '" name="faq_question_' + num + '"></textarea>';
        h += '  </td>';
        h += '  <td>';
        h += '      <textarea class="form-control faq_answer_' + num + '" name="faq_answer_' + num + '"></textarea>';
        h += '  </td>';
        h += '  <td>';
        h += '      <span class="icon-delete delete_FAQ_' + num + '" style="color: red; cursor: pointer;" onclick="deteleFAQ(' + num + ')"></span>';
        h += '  </td>';
        h += '</tr>';

        $('.question_answer').append(h);
        $('#num_question_answer').val(num);
    }

    function deteleFAQ(num) {
        $('.tr_' + num).remove();
        var numFAQ = $('#num_question_answer').val();
        for (var i = (num + 1); i <= numFAQ; i++) {
            $('.tr_' + i).attr('class', 'tr_' + (i - 1));
            $('.stt_' + i).text((i - 1));
            $('.stt_' + i).attr('class', 'stt_' + (i - 1));
            $('.faq_question_' + i).attr('name', 'faq_question_' + (i - 1));
            $('.faq_question_' + i).attr('class', 'form-control faq_question_' + (i - 1));
            $('.faq_answer_' + i).attr('name', 'faq_answer_' + (i - 1));
            $('.faq_answer_' + i).attr('class', 'form-control faq_answer_' + (i - 1));
            $('.delete_FAQ_' + i).attr('onclick', 'deteleFAQ(' + (i - 1) + ')');
            $('.delete_FAQ_' + i).attr('class', 'icon-delete delete_FAQ_' + (i - 1));
        }

        $('#num_question_answer').val(--numFAQ);
    }
</script>