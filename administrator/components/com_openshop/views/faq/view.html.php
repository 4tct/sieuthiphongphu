<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewFAQ extends OpenShopViewForm {

    public function getQuestionAnswer($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_faqdetails'))
                ->where('faq_id = ' . $id);
        return $db->setQuery($query)->loadObjectList();
    }

}
