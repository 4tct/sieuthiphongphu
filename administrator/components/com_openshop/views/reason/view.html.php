<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewReason extends OpenShopViewForm {

    public function _buildToolbar() {
        $viewName = $this->getName();
        $view = OpenShopInflector::pluralize($viewName);
        $edit = JRequest::getVar('edit');
        $text = $edit ? JText::_($this->lang_prefix . '_EDIT') : JText::_($this->lang_prefix . '_NEW');
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . $viewName) . ': <small><small class="title-product">[ ' . $text . ' ]</small></small>');
        if (OpenShopHelper::getPermissionAction($view, 'create') || OpenShopHelper::getPermissionAction($view, 'edit')) {
            JToolBarHelper::custom('reason_save', 'apply', 'apply', JText::_('OPENSHOP_SAVE'), FALSE);
        }
        
        JToolBarHelper::cancel($viewName . '.cancel');
    }
    
    public function getReasonDetail($id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        
        $query->select('*')
                ->from($db->quoteName('#__openshop_reasondetails'))
                ->where('reason_id = ' . $id);
        return $db->setQuery($query)->loadObjectList();
    }

}
