<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'jquery.min.js');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'reason.js');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;

        switch (pressbutton) {
            case 'reason.cancel':
                Joomla.submitform(pressbutton, form);
                return;
                break;
            case 'reason_save':
                var validate = [
                    {label: 'reason_name_label', class_input: 'reason_name', cond: 'required'}
                ];
                if (validator(validate))
                    btn_submit_ajax('reason.saveReason', 'adminForm');
                break;
            default:
                break;
        }

    }
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="adminfs">
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading ">
                <?php echo JText::_('OPENSHOP_ORDER_REASON_INFORMATION') ?>
            </div>
            <div class="panel-body cus responsive-edit row">
                <div class="form-group col-sm-12">
                    <label for="nameProduct" class="col-sm-3 control-label reason_name_label"><?php echo JText::_('OPENSHOP_REASON_NAME'); ?></label>
                    <span class="col-sm-9">
                        <input class="form-control reason_name" type="text" name="reason_name" id="reason_name" size="" maxlength="250" value="<?php echo $this->item->reason_name ?>" placeholder="<?php echo JText::_('OPENSHOP_REASON_NAME') ?>"/>
                        <span class="error_reason_name error-validate"></span>
                    </span>
                </div>
                <div class="form-group col-sm-12">
                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></label>
                    <span class="col-sm-9">
                        <?php echo $this->lists['published']; ?>
                    </span>
                </div>
                <div class="form-group col-sm-12">
                    <label for="nameProduct" class="col-sm-3 control-label reason_description_label"><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></label>
                    <span class="col-sm-9">
                        <textarea name="reason_description" id="reason_description" rows="8" class="form-control reason_description"><?php echo $this->item->reason_description ?></textarea>
                        <span class="error_reason_description error-validate"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-7">
        <div class="panel panel-default">
            <div class="panel-heading ">
                <?php echo JText::_('OPENSHOP_ORDER_REASON_INFORMATION') ?>
            </div>
            <div class="panel-body cus responsive-edit">
                <table class="table" border="0">
                    <thead>
                        <tr>
                            <th><?php echo JText::_('OPENSHOP_REASON_NAME_DETAIL') ?></th>
                            <th><?php echo JText::_('OPENSHOP_ACTION') ?></th>
                        </tr>
                    </thead>
                    <tbody id="content_reason_detail">
                        <?php
                        if (isset($this->item->id) && $this->item->id != '') {
                            $rows = $this->getReasonDetail($this->item->id);
                            foreach ($rows as $key => $row) {
                                $i = $key + 1;
                                echo '<tr id="tr_' . $i . '">';
                                echo '  <td>';
                                echo '      <input class="form-control" type="text" name="reason_name_desc_' . $i . '" id="reason_name_desc_' . $i . '" size="" maxlength="250" value="' . $row->reason_detail_description . '" />';
                                echo '  </td>';
                                echo '  <td style="text-align: center;">';
                                echo '      <div class="btn btn-danger" id="btn_delete_reson_detail_' . $i . '" onclick="delete_reason_detail(' . $i . ');"><i class="icon-delete"></i></div>';
                                echo '      <input type="hidden" name="id_value_reason_detail_' . $i . '" id="id_value_reason_detail_' . $i . '" value="' . $row->id . '" />';
                                echo '  </td>';
                                echo '</tr>';
                            }

                            $count_reasondetail = count($row);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div width="100%" style="text-align: center;"> 
                <div class="btn" onclick="add_reason_detail()" style="margin: 10px;">Add</div>
            </div>
        </div>
    </div>
    <?php echo JHtml::_('form.token'); ?>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" id="cid"/>
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="max_value" id="max_value" value="<?php echo isset($count_reasondetail) ? $count_reasondetail : '0'; ?>" />
</form>