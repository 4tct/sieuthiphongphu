<?php
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'permission.js');

$permissions = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_openshop' . DS . 'permissions.xml';
$lists = $this->lists;
$menuView = $this->menuView;
$num_row = count($menuView);

if (file_exists($permissions)) {//file exists and must be have contents
    $fp = fopen($permissions, 'r+');

    $content = readfile($permissions); // read content
    if ($content != 0) {//data not null
        $xml = @simplexml_load_file($permissions);
        if ($xml) {
            $num_element = count($xml->section) - 1;
            if ($num_row != $num_element) {
                $fp = fopen($permissions, 'r+');
                ftruncate($fp, 0);
                echo 'LMX';
            }
        } else {
            echo 'XML not Valid';
        }
    } else {//data  null
        $this->addPermissions;
    }
    fclose($fp);
} else {//file khong ton tai
    echo "File does not exists";
}
?>


<script>
    Joomla.submitbutton = function (task) {
        switch (task) {
            case 'per_save':
                save_per();
                break;
            default:
                toastr['error']('Không kết nối được với máy chủ!');
                break;
        }
    };
</script>

<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm">
        <ul class="nav nav-pills nav-stacked col-md-2">

            <?php
            for ($i = 0; $i < count($lists); $i++) {
                $list = $lists[$i];
                ?>
                <li class="<?php if ($i == 0) echo 'active' ?>"><a href="#tab_<?php echo $list->id ?>" data-toggle="pill"><?php echo $list->title ?></a></li>
                <?php
            }
            ?>
        </ul>
        <div class="tab-content col-md-10">
            <?php
            for ($i = 0; $i < count($lists); $i++) {
                $rs = $lists[$i];
                ?>
                <div class="tab-pane <?php if ($i == 0) echo 'active' ?>" id="tab_<?php echo $rs->id ?>">
                    <!--<h4><?php echo $rs->title ?></h4-->
                    <table width="100%" border="1" class="table table-striped">
                        <thead>
                        <th><?php echo JText::_('OPENSHOP_PER_NAME') ?></th>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_PER_SELECT') ?></th>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_PER_CREATE') ?></th>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_PER_DELETE') ?></th>
                        <th style="width: 10%;"><?php echo JText::_('OPENSHOP_PER_EDIT') ?></th>
                        <th><?php echo JText::_('OPENSHOP_PER_EDIT_STATE') ?></th>
                        <th><?php echo JText::_('OPENSHOP_PER_EDIT_OWN') ?></th>
                        <th><?php echo JText::_('OPENSHOP_PER_OPTIONS') ?></th>
                        <th><?php echo JText::_('OPENSHOP_PER_MANAGE') ?></th>
                        <th><?php echo JText::_('OPENSHOP_PER_ADMIN') ?></th>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($xml)) {
                                foreach ($xml->section as $section):
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo JText::_(strtoupper($section['name'])); ?>
                                        </td>
                                        <?php
                                        foreach ($section->action as $action):
                                            //if($count_td)
                                            ?>
                                            <td align="center">
                                                <?php
                                                $name = substr($action['name'], strpos(".", $action['name']));
                                                $name1 = str_replace('.', '_', $name);
                                                ?>
                                                <span id="<?php echo $rs->id . '_' . $section['name'] . '_' . $name1; ?>" class="icon-generic btn_cursor_pointer" style="color: red; font-size: 14pt" onclick="show('<?php echo $rs->id . '_' . $section['name'] . '_' . $name1; ?>')" ></span>
                                                <input type="hidden" name="<?php echo 'value_' . $rs->id . '_' . $section['name'] . '_' . $name1; ?>" id="<?php echo 'value_' . $rs->id . '_' . $section['name'] . '_' . $name1; ?>" value="1" />
                                            </td>
                                            <?php
                                        endforeach;
                                        ?>
                                    </tr>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
                <?php
            }
            ?>

        </div><!-- tab content -->
        <div class="clearfix"></div>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</fieldset>
