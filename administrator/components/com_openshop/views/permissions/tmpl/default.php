<?php
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'permission.js');

$lists = $this->lists;
$menuView = $this->menuView;
$num_row = count($menuView);

?>

<style>
    .per_title_parent{
        font-size: 18px;
        font-weight: bold;
        text-align: center;
    }
</style>

<script>
    Joomla.submitbutton = function (task) {
        switch (task) {
            case 'apply_access':
                apply_access();
                break;
            case 'add_per':
                show_add_per();
                break;
            case 'delete_per':
                show_modal_delete_per();
                break;
            default:
                toastr['error']('Không kết nối được với máy chủ!');
                break;
        }
    };
</script>

<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm">
        <ul class="nav nav-pills nav-stacked col-md-2" style="padding: 0 0 0 10px;">

            <?php
            $children = array();
            if ($lists) {
                // first pass - collect children
                foreach ($lists as $v) {
                    $pt = $v->parent_id;
                    $list = @$children[$pt] ? $children[$pt] : array();
                    array_push($list, $v);
                    $children[$pt] = $list;
                }
            }
            $list = JHtml::_('menu.treerecurse', 1, '', array(), $children, 9999, 0, 0);

            $class = 'active';
            foreach ($list as $key => $value) {
                ?>
                <li class="<?php echo $class; ?>"><a href="#tab_<?php echo $value->id ?>" data-toggle="pill" onclick="getIDDelete(<?php echo $value->id ?>);"><?php echo $value->treename ?></a></li>
                <?php
                if ($class == 'active') {
                    $id_per = $value->id;
                }

                $class = '';
            }
            ?>
        </ul>
        <div class="tab-content col-md-10">
            <?php
            for ($i = 0; $i < count($lists); $i++) {
                $rs = $lists[$i];
                ?>
                <div class="tab-pane <?php if ($i == 0) echo 'active' ?>" id="tab_<?php echo $rs->id ?>">
                    <!--<h4><?php echo $rs->title ?></h4-->
                    <table width="100%" border="1" class="table table-striped">
                        <thead>
                        <th><?php echo JText::_('OPENSHOP_PER_NAME') ?></th>
                        <th style="width: 10%; text-align: center;"><?php echo JText::_('OPENSHOP_PER_VIEW') ?></th>
                        <th style="width: 10%; text-align: center;"><?php echo JText::_('OPENSHOP_PER_CREATE') ?></th>
                        <th style="width: 10%; text-align: center;"><?php echo JText::_('OPENSHOP_PER_DELETE') ?></th>
                        <th style="width: 10%; text-align: center;"><?php echo JText::_('OPENSHOP_PER_EDIT') ?></th>
                        <th style="width: 10%; text-align: center;"><?php echo JText::_('OPENSHOP_PER_EDIT_STATE') ?></th>
                        <th style="width: 10%; text-align: center;"><?php echo JText::_('OPENSHOP_PER_ALL') ?></th>
    <!--                        <th><?php echo JText::_('OPENSHOP_PER_OPTIONS') ?></th>
                        <th><?php echo JText::_('OPENSHOP_PER_MANAGE') ?></th>
                        <th><?php echo JText::_('OPENSHOP_PER_ADMIN') ?></th>-->
                        </thead>
                        <tbody>
                            <?php
                            foreach ($this->getMenuParent() as $parent) {
                                echo '<tr>';

                                $ChildrenInMenu = $this->numChildrenInMenu($parent->id);
                                if ($ChildrenInMenu['number']) {
                                    echo '<td colspan="7" class="per_title_parent">' . JText::_($parent->menu_name) . '</td>';
                                } else {
                                    echo '<td>' . JText::_($parent->menu_name) . '</td>';
                                    $menu_name = strtolower($parent->menu_name);
                                    $per = $this->getPerMenuParent($parent->id);
                                    if (isset($per[$rs->id]) || !empty($per[$rs->id])) {
                                        $per_acc = $per[$rs->id];
                                        $class_icon = 'icon-ok';
                                        foreach ($per_acc as $k_a => $v_a) {
                                            if ($v_a == '1') {
                                                ?>
                                            <td align="center">
                                                <span id="<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>" class="btn_cursor_pointer icon-ok" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>')" ></span>
                                                <input type="hidden" name="value_<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>" id="value_<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>" value="1" />
                                            </td>
                                            <?php
                                        } else {
                                            ?>
                                            <td align="center">
                                                <span id="<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>" class="btn_cursor_pointer icon-delete" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>')" ></span>
                                                <input type="hidden" name="value_<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>" id="value_<?php echo $rs->id . '_' . $menu_name . '_core_' . $k_a ?>" value="0" />
                                            </td>
                                            <?php
                                            $class_icon = 'icon-delete';
                                        }
                                    }
                                    ?>
                                    <td align="center">
                                        <span id="<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>" class="btn_cursor_pointer <?php echo $class_icon ?>" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>')" ></span>
                                        <input type="hidden" name="value_<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>" id="value_<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>" value="<?php echo $class_icon === 'icon-ok' ? '1' : '0' ?>" />
                                    </td>
                                    <?php
                                } else {
                                    $arr = array(
                                        'view', 'create', 'delete', 'edit', 'editstate'
                                    );
                                    foreach ($arr as $v_empty) {
                                        ?>
                                        <td align="center">
                                            <span id="<?php echo $rs->id . '_' . $menu_name . '_core_' . $v_empty ?>" class="btn_cursor_pointer icon-delete" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $menu_name . '_core_' . $v_empty ?>')" ></span>
                                            <input type="hidden" name="value_<?php echo $rs->id . '_' . $menu_name . '_core_' . $v_empty ?>" id="value_<?php echo $rs->id . '_' . $menu_name . '_core_' . $v_empty ?>" value="0" />
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td align="center">
                                        <span id="<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>" class="btn_cursor_pointer icon-delete" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>')" ></span>
                                        <input type="hidden" name="value_<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>" id="value_<?php echo $rs->id . '_' . $menu_name . '_core_all' ?>" value="0" />
                                    </td>
                                    <?php
                                }
                            }
                            echo '</tr>';

                            //children menus
                            if ($ChildrenInMenu['number']) {
                                $data_child = $ChildrenInMenu['data'];       //menu_name , permission
                                foreach ($data_child as $v_child) {
                                    echo '<tr>';
                                    echo '  <td>' . JText::_($v_child->menu_name) . '</td>';
                                    
                                    $per_acc_child = json_decode($v_child->permission, TRUE);
                                    $per_child_name = strtolower($v_child->menu_name);
                                    if (isset($per_acc_child[$rs->id]) || !empty($per_acc_child[$rs->id])) {
                                        $per_acc_child_arr = $per_acc_child[$rs->id];
                                        $class_icon = 'icon-ok';
                                        foreach ($per_acc_child_arr as $k_a => $v_a) {
                                            if ($v_a == '1') {
                                                ?>
                                                <td align="center">
                                                    <span id="<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>" class="btn_cursor_pointer icon-ok" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>')" ></span>
                                                    <input type="hidden" name="value_<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>" id="value_<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>" value="1" />
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td align="center">
                                                    <span id="<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>" class="btn_cursor_pointer icon-delete" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>')" ></span>
                                                    <input type="hidden" name="value_<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>" id="value_<?php echo $rs->id . '_' . $per_child_name . '_core_' . $k_a ?>" value="0" />
                                                </td>
                                                <?php
                                                $class_icon = 'icon-delete';
                                            }
                                        }
                                        ?>
                                        <td align="center">
                                            <span id="<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>" class="btn_cursor_pointer <?php echo $class_icon ?>" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>')" ></span>
                                            <input type="hidden" name="value_<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>" id="value_<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>" value="<?php echo $class_icon === 'icon-ok' ? '1' : '0' ?>" />
                                        </td>
                                        <?php
                                    } else {
                                        $arr = array(
                                            'view', 'create', 'delete', 'edit', 'editstate'
                                        );
                                        foreach ($arr as $v_empty) {
                                            ?>
                                            <td align="center">
                                                <span id="<?php echo $rs->id . '_' . $per_child_name . '_core_' . $v_empty ?>" class="btn_cursor_pointer icon-delete" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $per_child_name . '_core_' . $v_empty ?>')" ></span>
                                                <input type="hidden" name="value_<?php echo $rs->id . '_' . $per_child_name . '_core_' . $v_empty ?>" id="value_<?php echo $rs->id . '_' . $per_child_name . '_core_' . $v_empty ?>" value="0" />
                                            </td>
                                            <?php
                                        }
                                        ?>
                                        <td align="center">
                                            <span id="<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>" class="btn_cursor_pointer icon-delete" style="color: red; font-size: 14pt" onclick="save_per('<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>')" ></span>
                                            <input type="hidden" name="value_<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>" id="value_<?php echo $rs->id . '_' . $per_child_name . '_core_all' ?>" value="0" />
                                        </td>
                                        <?php
                                    }

                                    echo '</tr>';
                                }
                            }
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
                <?php
            }
            ?>

        </div><!-- tab content -->
        <div class="clearfix"></div>
        <input type="hidden" name="id_delete_per" id="id_delete_per" value="<?php echo $id_per ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </form>
</fieldset>

<!--show add permission-->
<div class="modal fade modal_add_per" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 150px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="myModalLabel">
                    <span class="title_per"><?php echo JText::_('OPENSHOP_ADD_PERMISSION_TITLE') ?></span>
                    <button type="button" class="btn btn-default pos_btn" data-dismiss="modal"><i class="icon-cancel-circle" style="color:red"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                    <button type="button" class="btn btn-primary pos_btn" onclick="save_per_new()"><i class="icon-apply"></i> <?php echo JText::_('OPENSHOP_SAVE') ?></button>
                </div>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label><?php echo JText::_('OPENSHOP_PERMISSION_NAME') ?></label>
                    <input type="text" class="form-control" name="permission_name" id="permission_name" value="" placeholder="<?php echo JText::_('OPENSHOP_PERMISSION_NAME_DESC') ?>" />
                </div>
                <div class="form-group">
                    <label><?php echo JText::_('OPENSHOP_PERMISSION_GROUP_SELECT') ?></label>
                    <?php // echo $this->getUserGroupAdmin();       ?>

                    <?php
                    $idOA = $this->getIdOA();
                    $rows = $this->getUserGroupAdmin( $idOA->lft, $idOA->rgt );
                    $children = array();
                    if ($rows['lists']) {
                        // first pass - collect children
                        foreach ($rows['lists'] as $v) {
                            $pt = $v->parent_id;
                            $list = @$children[$pt] ? $children[$pt] : array();
                            array_push($list, $v);
                            $children[$pt] = $list;
                        }
                    }

                    $list = JHtml::_('menu.treerecurse', $idOA->parent_id , '', array(), $children, 9999, 0, 0);

                    $class = 'active';
                    ?>
                    <select class="form-control" id="permission_id" name="permission_id">
                        <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_PERMISSION') ?></option>
                        <?php
                        foreach ($list as $key => $value) {
                            ?>
                            <option value="<?php echo $value->id ?>"><?php echo $value->treename ?></option>
                            <?php
                            $class = '';
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>


<!--show delete permission-->
<div class="modal fade modal_delete_per" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 150px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="myModalLabel">
                    <span class="title_per"><?php echo JText::_('OPENSHOP_DELETE_PERMISSION_TITLE') ?></span>
                    <button type="button" class="btn btn-default pos_btn" data-dismiss="modal"><i class="icon-cancel-circle" style="color:red"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                    <button type="button" class="btn btn-danger pos_btn" onclick="delete_per()"><i class="icon-delete"></i> <?php echo JText::_('OPENSHOP_DELETE') ?></button>
                </div>
            </div>
            <div class="modal-body">
                <?php echo JText::_('OPENSHOP_DELETE_PERMISSION_DESC') ?>
            </div>
        </div>
    </div>
</div>

<!--loading-->
<div class="loading" style="display: none;">
    <div class="loading_access" >
    </div>
    <div class="content_access">
        <img src="<?php echo OPENSHOP_ADMIN_PATH_IMG_HTTP . DS . 'loading_setup.gif' ?>" />
    </div>
</div>


