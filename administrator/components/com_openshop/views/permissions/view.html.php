<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.component.model');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewPermissions extends JViewLegacy {

    function display($tpl = null) {
        
        OpenShopPermissions::CheckPermission();
        
        //check permission name is Operation Administrator
        $this->checkPermissionOperactionAdministrator();
        
        $model = $this->getModel('permissions');
        $lists = $model->getUsersGroup();
        $menuView = $model->getMenuView();
        $addPermissions = $model->addPermissions();
        $this->access = $model->getAccessInMenu();
        $this->addPermissions = $addPermissions;
        $this->menuView = $menuView;
        $this->lists = $lists;
        
        $this->addToolBar();
        parent::display($tpl);
    }

    function addToolbar() {
        JToolbarHelper::title(JText::_('OPENSHOP_PERMISSIONS'));
        JToolbarHelper::custom('apply_access', 'apply', 'apply', JText::_('OPENSHOP_ACCESS_APPLY'), false);
        JToolbarHelper::custom('add_per', 'plus-2', 'plus-2', JText::_('OPENSHOP_ADD_PERMISSION'), false);
        JToolbarHelper::custom('delete_per', 'delete', 'delete', JText::_('OPENSHOP_DELETE_PERMISSION'), false);
    }

    function getNameUserGroup($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,title')
                ->from($db->quoteName('#__usergroups'))
                ->where('id = ' . $id);
        $res = $db->setQuery($query)->loadObject();

        return $res->id;
    }
    
    function getIdOA(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__usergroups'))
                ->where('LOWER(title) = "operation administrator"');
        return $db->setQuery($query)->loadObject();
    }

    function getUserGroupAdmin($l, $r) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->clear();
        $query->select('id, title, parent_id')
                ->from($db->quoteName('#__usergroups'))
                ->where('lft >= ' . $l)
                ->where('rgt <= ' .$r);

        $rows['lists'] = $db->setQuery($query)->loadObjectList();
        return $rows;
    }
    
    function checkPermissionOperactionAdministrator(){
        $model = $this->getModel('permissions');
        $check = $model->checkPermission('Operation Administrator');
        
        if(!$check){
            $model->addPermissionOperactionAdministrator();
        }
    }
    
    //new
    function getMenuParent(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id, menu_name')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('menu_parent_id = 0')
                ->where('published = 1')
                ->where('access = 1');
        return $db->setQuery($query)->loadObjectList();
    }
    
    function numChildrenInMenu($id_parent){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('menu_name, permission')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('menu_parent_id = ' . $id_parent);
        
        $res = array();
        $res['number'] = count($db->setQuery($query)->loadObjectList());
        $res['data'] = $db->setQuery($query)->loadObjectList();
        
        return $res;
    }
    
    function getPerMenuParent($id_parent){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('permission')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('id = ' . $id_parent);
        return json_decode($db->setQuery($query)->loadResult(),TRUE);
    }

}
