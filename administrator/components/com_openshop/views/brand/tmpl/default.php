<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'brand.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
<?php
if ($translatable) {
    foreach ($this->languages as $language) {
        $langId = $language->lang_id;
        ?>
                    if (document.getElementById('brand_name_<?php echo $langId; ?>').value == '') {
                        alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                        document.getElementById('brand_name_<?php echo $langId; ?>').focus();
                        return;
                    }
        <?php
    }
} else {
    ?>
                if (form.brand_name.value == '') {
                    alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                    form.brand_name.focus();
                    return;
                }
    <?php
}
?>
            Joomla.submitform(pressbutton, form);
        }
    }
</script>
<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <div class="row-fluid">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#general-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_GENERAL'); ?></a></li>
                <li><a href="#data-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_DATA'); ?></a></li>									
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="general-page">
                    <div class="span10">
                        <?php
                        if ($translatable) {
                            ?>
                            <ul class="nav nav-tabs">
                                <?php
                                $i = 0;
                                foreach ($this->languages as $language) {
                                    $langCode = $language->lang_code;
                                    ?>
                                    <li <?php echo $i == 0 ? 'class="active"' : ''; ?>><a href="#general-page-<?php echo $langCode; ?>" data-toggle="tab"><?php echo $this->languageData['title'][$langCode]; ?>
                                            <img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" /></a></li>
                                    <?php
                                    $i++;
                                }
                                ?>																					
                            </ul>
                            <div class="tab-content">
                                <?php
                                $i = 0;
                                foreach ($this->languages as $language) {
                                    $langId = $language->lang_id;
                                    $langCode = $language->lang_code;
                                    ?>
                                    <div class="tab-pane<?php echo $i == 0 ? ' active' : ''; ?>" id="general-page-<?php echo $langCode; ?>">													
                                        <table class="admintable adminform" style="width: 100%;">
                                            <tr>
                                                <td class="key">
                                                    <span class="required">*</span>
                                                    <?php echo JText::_('OPENSHOP_NAME'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="brand_name_<?php echo $langCode; ?>" id="brand_name_<?php echo $langId; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'brand_name_' . $langCode}) ? $this->item->{'brand_name_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>																		
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_ALIAS'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="brand_alias_<?php echo $langCode; ?>" id="brand_alias_<?php echo $langId; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'brand_alias_' . $langCode}) ? $this->item->{'brand_alias_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_PAGE_TITLE'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="brand_page_title_<?php echo $langCode; ?>" id="brand_page_title_<?php echo $langId; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'brand_page_title_' . $langCode}) ? $this->item->{'brand_page_title_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_PAGE_HEADING'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="brand_page_heading_<?php echo $langCode; ?>" id="brand_page_heading_<?php echo $langId; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'brand_page_heading_' . $langCode}) ? $this->item->{'brand_page_heading_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_DESCRIPTION'); ?>
                                                </td>
                                                <td>
                                                    <?php echo $editor->display('brand_desc_' . $langCode, isset($this->item->{'brand_desc_' . $langCode}) ? $this->item->{'brand_desc_' . $langCode} : '', '100%', '250', '75', '10'); ?>
                                                </td>								
                                            </tr>
                                        </table>
                                    </div>							
                                    <?php
                                    $i++;
                                }
                                ?>
                            </div>
                            <?php
                        } else {
                            ?>
                            <table class="admintable adminform" style="width: 100%;">
                                <tr>
                                    <td class="key">
                                        <span class="required">*</span>
                                        <?php echo JText::_('OPENSHOP_NAME'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="brand_name" id="brand_name" size="" maxlength="250" value="<?php echo $this->item->brand_name; ?>" />
                                    </td>								
                                </tr>																		
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_ALIAS'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="brand_alias" id="brand_alias" size="" maxlength="250" value="<?php echo $this->item->brand_alias; ?>" />
                                    </td>								
                                </tr>
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_PAGE_TITLE'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="brand_page_title" id="brand_page_title" size="" maxlength="250" value="<?php echo $this->item->brand_page_title; ?>" />
                                    </td>								
                                </tr>
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_PAGE_HEADING'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="brand_page_heading" id="brand_page_heading" size="" maxlength="250" value="<?php echo $this->item->brand_page_heading; ?>" />
                                    </td>								
                                </tr>
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_DESCRIPTION'); ?>
                                    </td>
                                    <td>
                                        <?php echo $editor->display('brand_desc', $this->item->brand_desc, '100%', '250', '75', '10'); ?>
                                    </td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </div>
                </div><!-- End General page -->
                <div class="tab-pane" id="data-page">
                    <div class="span8">
                        <table class="admintable adminform" style="width: 100%;">
                            <tr>
                                <td  class="key">
                                    <?php echo JText::_('OPENSHOP_EMAIL'); ?>
                                </td>
                                <td>
                                    <input class="text_area" type="text" name="brand_email" id="brand_email" size="40" maxlength="250" value="<?php echo $this->item->brand_email; ?>" />
                                </td>							
                            </tr>
                            <tr>
                                <td  class="key">
                                    <?php echo JText::_('OPENSHOP_URL'); ?>
                                </td>
                                <td>
                                    <input class="text_area" type="text" name="brand_url" id="brand_url" size="60" value="<?php echo $this->item->brand_url; ?>" />
                                </td>							
                            </tr>
                            <tr>
                                <td  class="key">
                                    <?php echo JText::_('OPENSHOP_BRAND_IMAGE'); ?>
                                </td>
                                <td>
                                    <input type="file" class="input-large" accept="image/*" name="brand_image" />
                                    <?php
                                    if (JFile::exists(JPATH_ROOT . '/media/com_openshop/brands/' . $this->item->brand_image)) {
                                        $viewImage = JFile::stripExt($this->item->brand_image) . '-100x100.' . JFile::getExt($this->item->brand_image);
                                        if (Jfile::exists(JPATH_ROOT . '/media/com_openshop/brands/resized/' . $viewImage)) {
                                            ?>
                                            <img src="<?php echo JURI::root() . 'media/com_openshop/brands/resized/' . $viewImage; ?>" />
                                            <?php
                                        } else {
                                            ?>
                                            <img src="<?php echo JURI::root() . 'media/com_openshop/brands/' . $this->item->brand_image; ?>" height="100" />
                                            <?php
                                        }
                                        ?>
                                        <label class="checkbox">
                                            <input type="checkbox" name="remove_image" value="1" />
                                            <?php echo JText::_('OPENSHOP_REMOVE_IMAGE'); ?>
                                        </label>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_CUSTOMERGROUPS'); ?>
                                </td>
                                <td>
                                    <?php echo $this->lists['brand_customergroups']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                                </td>
                                <td>
                                    <?php echo $this->lists['published']; ?>
                                </td>							
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
        <?php
        if ($translatable) {
            foreach ($this->languages as $language) {
                $langCode = $language->lang_code;
                ?>
                <input type="hidden" name="details_id_<?php echo $langCode; ?>" value="<?php echo isset($this->item->{'details_id_' . $langCode}) ? $this->item->{'details_id_' . $langCode} : ''; ?>" />
                <?php
            }
        } elseif ($this->translatable) {
            ?>
            <input type="hidden" name="details_id" value="<?php echo isset($this->item->{'details_id'}) ? $this->item->{'details_id'} : ''; ?>" />
            <?php
        }
        ?>
        <input type="hidden" name="task" value="" />
    </form>
</fieldset>