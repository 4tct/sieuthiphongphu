<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewBrand extends OpenShopViewForm {

    function _buildListArray(&$lists, $item) {
        //Build customer groups list
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, b.customergroup_name AS text')
                ->from('#__openshop_customergroups AS a')
                ->innerJoin('#__openshop_customergroupdetails AS b ON (a.id = b.customergroup_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
                ->order('b.customergroup_name');
        $db->setQuery($query);
        $options = $db->loadObjectList();
        if ($item->brand_customergroups != '') {
            $selectedItems = explode(',', $item->brand_customergroups);
        } else {
            $selectedItems = array();
        }
        $lists['brand_customergroups'] = JHtml::_('select.genericlist', $options, 'brand_customergroups[]', array(
                    'option.text.toHtml' => false,
                    'option.text' => 'text',
                    'option.value' => 'value',
                    'list.attr' => ' class="inputbox chosen" multiple ',
                    'list.select' => $selectedItems));
    }

}
