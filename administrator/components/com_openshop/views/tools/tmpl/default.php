<?php
/**
 * @version		2.0.1.6
 * @package		Joomla
 * @subpackage	OpenShop
 * @author  	LMNX
 * @copyright	LMNX
 * @license	GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die();
JToolBarHelper::title(JText::_('OPENSHOP_TOOLS'), 'generic.png');
?>
<script type="text/javascript">
	function confirmation(message, destnUrl) {
		var answer = confirm(message);
		if (answer) {
			window.location = destnUrl;
		}
	}
</script>