<?php
/**
 * @version	2.0.1.6
 * @package	Joomla
 * @subpackage	OpenShop
 * @author  	LMNX
 * @copyright	LMNX
 * @license     GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die();

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewTools extends JViewLegacy
{

	function display($tpl = null)
	{
		// Check access first
		$mainframe = JFactory::getApplication();
		if (!JFactory::getUser()->authorise('openshop.tools', 'com_openshop'))
		{
			$mainframe->enqueueMessage(JText::_('OPENSHOP_ACCESS_NOT_ALLOW'), 'error');
			$mainframe->redirect('index.php?option=com_openshop&view=dashboard');
		}
		else 
		{
			parent::display($tpl);
		}
	}
	/**
	 * 
	 * Function to create the buttons view.
	 * @param string $link targeturl
	 * @param string $image path to image
	 * @param string $text image description
	 */
	function quickiconButton($link, $image, $text, $textConfirm)
	{
		$language = JFactory::getLanguage();
		?>
		<div style="float:<?php echo ($language->isRTL()) ? 'right' : 'left'; ?>;">
			<div class="icon">
				<a onclick="javascript:confirmation('<?php echo $textConfirm; ?>', '<?php echo $link; ?>');" title="<?php echo $text; ?>" href="#">
					<?php echo JHtml::_('image', 'administrator/components/com_openshop/assets/icons/' . $image, $text); ?>
					<span><?php echo $text; ?></span>
				</a>
			</div>
		</div>
		<?php
	}
}