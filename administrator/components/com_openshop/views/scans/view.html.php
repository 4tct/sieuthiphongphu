<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewScans extends JViewLegacy {

    function display($tpl = null) {

        $this->items = $this->get('Items');
        $this->buildToolbar();
        parent::display($tpl);
    }

    function totalQuantityProduct() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('sum(quantity_import)')
                ->from($db->quoteName('#__openshop_kiemkhos'));
        return $db->setQuery($query)->loadResult();
    }

    public function buildToolbar() {
        JToolbarHelper::title(JText::_('OPENSHOP_SCAN'));
        if (JFactory::getApplication()->input->getString('layout') == '') {
            JToolbarHelper::custom('download_error', 'download', 'download', JText::_('OPENSHOP_DOWNLOAD_ERROR'), false);
            if (OpenShopPermissions::checkAdmin(JFactory::getUser()->groups)) {
                JToolbarHelper::custom('delete_error', 'delete', 'delete', JText::_('OPENSHOP_DELETE_ERROR'), false);
            }
            JToolbarHelper::custom('choose_audio', 'file-2', 'file-2', JText::_('OPENSHOP_CHOOSE_AUDIO'), false);
            JToolbarHelper::custom('scan_phone', 'file-2', 'file-2', JText::_('OPENSHOP_SCAN_PHONE'), false);
        }
        
        if (JFactory::getApplication()->input->getString('layout') == 'scan_phone') {
            JToolbarHelper::custom('uploadFilePhoneNumber','upload','upload', JText::_('OPENSHOP_UPLOAD_FILE'),false);
            JToolbarHelper::custom('ChooseFile','generic','generic', JText::_('OPENSHOP_UPLOAD_CHOOSE_FILE'),false);
        }
    }

    function getImportBy($product_name) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('import_by')
                ->from($db->quoteName('#__openshop_kiemkhos'))
                ->where('product_name = "' . $product_name . '"')
                ->group('import_by');
        $rows = $db->setQuery($query)->loadObjectList();

        $username = array();
        foreach ($rows as $row) {
            $query->clear();
            $query->select('name')
                    ->from($db->quoteName('#__users'))
                    ->where('id = ' . $row->import_by);
            $username = array_merge($username, $db->setQuery($query)->loadObjectList());
        }

        return $username;
    }

    function getAllUserOA() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $users = array();
        $query->select('*')
                ->from($db->quoteName('#__usergroups'))
                ->where('UPPER(title) = "ADMINISTRATOR"');
        $row_ad = $db->setQuery($query)->loadObject();

        $query->clear();
        $query->select('id,title')
                ->from($db->quoteName('#__usergroups'))
                ->where('lft > ' . $row_ad->lft)
                ->where('rgt < ' . $row_ad->rgt);
        $rows_ug = $db->setQuery($query)->loadObjectList();

        $query->clear();
        $query->select('a.id,a.name')
                ->from($db->quoteName('#__users', 'a'))
                ->join('INNER', $db->quoteName('#__user_usergroup_map', 'b') . 'ON a.id = b.user_id');
        foreach ($rows_ug as $v) {
            $query->where('b.group_id = ' . $v->id, 'OR');
        }
        $query->group('id');

        return $db->setQuery($query)->loadObjectList();
    }

    function getZoneScan($id = null) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('optionvalue_id,value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'b') . 'ON b.option_id = a.option_id')
                ->where('UPPER(b.option_name) = "ZONE SCAN"');
        if (isset($id)) {
            $query->where('a.optionvalue_id = ' . $id);
        }

        return $db->setQuery($query)->loadObjectList();
    }

    function getSoundError() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('media_file,media_detail')
                ->from($db->quoteName('#__openshop_mediadetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_medias', 'b') . 'ON a.media_id = b.id')
                ->where('UPPER(b.media_name) = "ERROR SOUND"');
        return $db->setQuery($query)->loadObjectList();
    }

}
