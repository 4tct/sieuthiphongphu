<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(JUri::base() . DS . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'scan.js');
//print_r($this->items);exit();
?>

<style>
    .delele_all_scaned{
        display: inline-block;
        float: right;
        padding: 5px 21px;
        background: red;
        margin-right: 10px;
        color: white;
        cursor: pointer;
    }
    .delele_all_scaned:hover{
        box-shadow: 0 2px 10px 0 black;
        background: #B91414;
    }
    .admin_mode{
        display: inline-block;
        float: right;
        padding: 5px 21px;
        background: #198226;
        margin-right: 10px;
        color: white;
        cursor: pointer;
    }
    .admin_mode:hover{
        box-shadow: 0 2px 10px 0 black;
        background: #25B337;
    }
    .deleteP{
        display: none;
    }
    .bg-deleteP{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: black;
        z-index: 1000;
        opacity: 0.5;
    }
    .content-deleteP{
        position: fixed;
        top: 30%;
        left: 40%;
        width: 300px;
        z-index: 10000;
        background: white;
        border-radius: 5px;
    }
    .deleteP-title{
        color: #3C3C3C;
        border-bottom: 1px solid #737373;
        padding: 10px 10px 10px 20px;
        font-size: 20px;
    }
    .btn-deleteP{
        padding: 20px 10px;
        text-align: right;
    }
    .btn-dlt{
        padding: 10px;
        border: 1px solid red;
        color: red;
        margin-right: 10px;
        cursor: pointer;
    }
    .btn-close{
        padding: 10px;
        border: 1px solid #696969;
        color: dimgrey;
        margin-right: 10px;
        cursor: pointer;
    }
    .btn-dlt:hover{
        background: red;
        color: white;
    }
    .btn-close:hover{
        background: #696969;
        color: white;
    }
</style>

<script>
    Joomla.submitbutton = function (task) {
        switch (task) {
            case 'download_error':
                download_error();
                break;
            case 'delete_error':
                delete_error();
                break;
            case 'choose_audio':
                $('.chooseSound').modal('show');
                break;
            case 'scan_phone':
                window.open('index.php?option=com_openshop&view=scans&layout=scan_phone');
                break;
            default:
                break;
        }
    };
</script>

<form action="index.php?option=com_openshop&view=scans" method="post" id="scanAdmin">
    <fieldset class="adminfs">
        <br>
        Scan : <input  type="text" class="scan" id="scan" value="" autofocus onchange="scanValue(this.value)" style="width:150px;"/>
        <input type="hidden" name="scanMode" id="scanMode" value="" />

        <span style="margin-left: 40px;">
            Tổng số SP: <b><span class="total_quantity_scaned"><?php echo isset($this->items) ? count($this->items) : '0' ?></span></b>
        </span>
        <span style="margin-left: 40px;">
            Mã SP vừa nhập: <b><span class="product_cur"></span></b>
        </span>
        <span style="margin-left: 40px;">
            Tổng số lượng: <b><span class="totalQuantityProduct"><?php echo $this->totalQuantityProduct(); ?></span></b>
        </span>
        <span style="margin-left: 40px;">
            <select name="zonescan" id="zonescan" onchange="this.form.submit()" style="width: 100px;">
                <option value=""><?php echo JText::_('OPENSHOP_SELECT_ZONE_SCAN') ?></option>
                <?php
                $zone_act = JFactory::getApplication()->input->getInt('zonescan');
                foreach ($this->getZoneScan() as $value) {
                    $selected = '';
                    if ($value->optionvalue_id == $zone_act) {
                        $selected = 'selected';
                    }
                    ?>
                    <option value="<?php echo $value->optionvalue_id ?>" <?php echo $selected ?>><?php echo $value->value ?></option>
                    <?php
                }
                ?>
            </select>
        </span>
        <?php
        if (OpenShopPermissions::checkAdmin(JFactory::getUser()->groups)) {
            ?>

            <span>
                <div class="delele_all_scaned" onclick="delete_all_scan()">
                    <span>Xóa tất cả</span>
                </div>
            </span>
            <span>
                <div class="admin_mode" onclick="admin_mode()">
                    <span>Cộng dồn số lượng</span>
                </div>
            </span>
            <!--//filte user-->
            <span style="margin-left: 40px;">
                <select name="user_filter" onchange="this.form.submit()" style="width:96px;">
                    <option value="">- Chọn -</option>
                    <?php
                    $state_user_filter = JFactory::getApplication()->input->getInt('user_filter');
                    foreach ($this->getAllUserOA() as $value) {
                        $selected = '';
                        if ($value->id == $state_user_filter) {
                            $selected = 'selected';
                        }
                        ?>
                        <option value="<?php echo $value->id ?>" <?php echo $selected; ?>><?php echo $value->name ?></option>
                        <?php
                    }
                    ?>
                </select>
            </span>
            <?php
        }
        ?>
        <br>

        <table class="adminlist table table-striped">
            <thead>
                <tr>
                    <td>STT</td>
                    <td>Mã sản phẩm</td>
                    <td>Kích thước</td>
                    <td>Màu sắc</td>
                    <td>Số lượng</td>
                    <td>Quét mã bởi</td>
                    <td>Khu vực</td>
                    <td>Chế độ</td>
                    <td>Thời gian</td>
                    <td>Chức năng</td>
                </tr>
            </thead>
            <tbody id="tbl_scan">
                <?php
                if(OpenShopPermissions::checkAdmin(JFactory::getUser()->groups)){
                        $onclick = 'onclick="showDeleteScanPro(' . $value->id . ')"';
                    $sD = 'color:red;cursor:pointer;';
                } else {
                    $onclick = '';
                    $sD = 'color:#CCCCCC;cursor:pointer;';
                }
                
                foreach ($this->items as $key => $value) {
                    $zone = $this->getZoneScan($value->zone_import);
                    $z = '-';
                    if (isset($zone) && !empty($zone)) {
                        $z = $zone[0]->value;
                    }
                    $date = explode(' ', $value->date_import);
                    echo '<tr id="' . $value->product_name . '">';
                    echo '  <td>' . ($key + 1) . '</td>';
                    echo '  <td>' . $value->product_sku . '</td>';
                    echo '  <td>' . ($value->size == '' ? 'No Item' : $value->size) . '</td>';
                    echo '  <td>' . ($value->color == '' ? 'No Item' : $value->color) . '</td>';
                    echo '  <td class="quantity_import_' . $value->product_name . '">' . $value->quantity_import . '</td>';
                    echo '  <td>' . JFactory::getUser($value->import_by)->name . '</td>';
                    echo '  <td>' . $z . '</td>';
                    echo '  <td>' . $value->scan_mode . '</td>';
                    echo '  <td>' .
                    $date[1] . ' ' . date('d-m-Y', strtotime($date[0]))
                    . '</td>';
                    echo '  <td align="center">';
                    echo '      <span class="icon-delete" style="'. $sD .'" '. $onclick .'></span>';
                    echo ' </td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>

        <div id="error">
            <!--code-->
        </div>

        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="max_value" id="max_value" value="<?php echo isset($this->items) ? count($this->items) : '0' ?>" />
        <input type="hidden" name="boxchecked" value="0" />	
        <?php echo JHtml::_('form.token'); ?>		

        <!--//Choose sound-->
        <div class="modal fade chooseSound" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="gridSystemModalLabel"><?php echo JText::_('OPENSHOP_CHOOSE_AUDIO'); ?></h2>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-4"><?php echo JText::_('OPENSHOP_CHOOSE_SOUND'); ?></label>
                            <div class="col-md-8">
                                <select class="form-control" name="fileSound" id="fileSound">
                                    <option value=""><?php echo JText::_('OPENSHOP_CHOOSE'); ?></option>
                                    <?php
                                    $fileSound = JFactory::getApplication()->input->getString('fileSound');
                                    foreach ($this->getSoundError() as $value) {
                                        $selected = '';
                                        if ($value->media_file == $fileSound) {
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option value="<?php echo $value->media_file ?>" <?php echo $selected; ?>><?php echo $value->media_detail ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo JText::_('OPENSHOP_CLOSE'); ?></button>
                        <button type="button" class="btn btn-primary" onclick="this.form.submit()"><?php echo JText::_('OPENSHOP_CHOOSE_SOUND'); ?></button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</form>
</fieldset>

<!--//download file error-->
<form action="index.php?option=com_openshop&format=ajax&task=scans.getUrlErrorScan" method="post" id="downloadErrorScan">

</form>

<div class="deleteP">
    <div class="bg-deleteP"></div>
    <div class="content-deleteP">
        <div class="deleteP-title">Bạn thực sự muốn xóa?</div>
        <div class="btn-deleteP">
            <span class="btn-dlt" onclick=""><i class="icon-delete"></i> Xóa</span>
            <span class="btn-close" onclick="closeScanPro()"><i class="icon-cancel"></i> Thoát</span>
        </div>
    </div>
</div>