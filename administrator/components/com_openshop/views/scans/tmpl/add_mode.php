<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(JUri::base() . DS . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'scan.js');
//print_r($this->items);exit();
?>

<style>
    .delele_all_scaned{
        display: inline-block;
        float: right;
        padding: 5px 21px;
        background: red;
        margin-right: 10px;
        color: white;
        cursor: pointer;
    }
    .delele_all_scaned:hover{
        box-shadow: 0 2px 10px 0 black;
        background: #B91414;
    }
    .admin_mode{
        display: inline-block;
        float: right;
        padding: 5px 21px;
        background: #198226;
        margin-right: 10px;
        color: white;
        cursor: pointer;
    }
    .admin_mode:hover{
        box-shadow: 0 2px 10px 0 black;
        background: #25B337;
    }
</style>

<fieldset class="adminfs">
    <br>
    Scan : <input  type="text" class="scan" id="scan" value="" autofocus onchange="scanValue(this.value)"/>

    <span style="margin-left: 40px;">
        Tổng số sản phẩm: <b><span class="total_quantity_scaned"><?php echo isset($this->items) ? count($this->items) : '0' ?></span></b>
    </span>
    <span style="margin-left: 40px;">
        Mã sản phẩm vừa nhập: <b><span class="product_cur"></span></b>
    </span>
    <span style="margin-left: 40px;">
        Tổng số lượng: <b><span class="totalQuantityProduct"><?php echo $this->totalQuantityProduct(); ?></span></b>
    </span>
    <?php
    if (OpenShopPermissions::checkAdmin(JFactory::getUser()->groups)) {
        ?>
        <span style="margin-left: 40px;">
            <div class="delele_all_scaned" onclick="delete_all_scan()">
                <span>Xóa tất cả</span>
            </div>
        </span>
        <span style="margin-left: 40px;">
            <div class="admin_mode" onclick="standard_mode()">
                <span>Xem chế độ bình thường</span>
            </div>
        </span>
        <?php
    }
    ?>
    <br>

    <table class="adminlist table table-striped">
        <thead>
            <tr>
                <td>STT</td>
                <td>Mã sản phẩm</td>
                <td>Kích thước</td>
                <td>Màu sắc</td>
                <td>Số lượng</td>
                <td>Quét mã bởi</td>
            </tr>
        </thead>
        <tbody id="tbl_scan">
            <?php
            foreach ($this->items as $key => $value) {
                echo '<tr id="' . $value->product_name . '">';
                echo '  <td>' . ($key + 1) . '</td>';
                echo '  <td>' . $value->product_sku . '</td>';
                echo '  <td>' . ($value->size == '' ? 'No Item' : $value->size) . '</td>';
                echo '  <td>' . ($value->color == '' ? 'No Item' : $value->color) . '</td>';
                echo '  <td class="quantity_import_' . $value->product_name . '">' . $value->quantity_import . '</td>';
                echo '  <td>';
                $rows_importby = $this->getImportBy($value->product_name);
                foreach ($rows_importby as $key => $row) {
                    echo '  <span>'. ($key+1) .'. '. $row->name .'</span><br>';
                }
                echo '  </td>';
                echo '</tr>';
            }
            ?>
        </tbody>
    </table>

    <div id="error">
        <!--code-->
    </div>

    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="max_value" id="max_value" value="<?php echo isset($this->items) ? count($this->items) : '0' ?>" />
    <input type="hidden" name="boxchecked" value="0" />	
    <?php echo JHtml::_('form.token'); ?>			
</form>
</fieldset>