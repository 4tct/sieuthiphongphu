<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//include OPENSHOP_PATH_INCLUDES . 'QRCode/qrlib.php';
//JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'qrcode.min.js');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'jquery.qrcode.js');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'qrcode.js');
require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';
?>

<style>
    #id_qrcode img{
        display: inline-block !important;
        padding: 0 80px 80px 0;
    }
    #id_qrcode canvas{
        padding: 0 65px 60px 0;
    }
    .show_qrcode{
        display: inline-block;
        padding: 10px;
        background: #2B9618;
        color: white;
        cursor: pointer;
    }
    .show_qrcode:hover{
        box-shadow: 0 3px 15px 0 #656565;
        background: #2CAF30;
    }
    .show_error{
        display: inline-block;
        padding: 10px 20px;
        background: #AD1818;
        color: white;
        cursor: pointer;
    }
    .show_error:hover{
        box-shadow: 0 3px 15px 0 #656565;
        background: red;
    }
    .modal.fade.in{
        position: absolute;
    }
</style>

<script>
    Joomla.submitbutton = function (task) {
        switch (task) {
            case 'uploadFilePhoneNumber':
                $('#myModalUpload').modal('show');
                break;
            case 'ChooseFile':
                $('.chooseFile').toggle(200);
                break;
            default:
                break;
        }
    };
</script>

<?php
//get file name
$filelist = glob(OPENSHOP_PATH_SCANS . "*.xls*");
?>

<div class="adminfs">
    <form action="index.php?option=com_openshop&view=scans&layout=scan_phone" method="post" id="formScanQRCode">
        <div class="chooseFile" style="display:none">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_UPLOAD_CHOOSE_FILE'); ?></div>
                        <div class="panel-body">
                            <label><?php echo JText::_('OPENSHOP_UPLOAD_NAME'); ?></label>
                            <select class="form-control" name="chooseFile" onchange="this.form.submit()">
                                <option value=""><?php echo JText::_('OPENSHOP_CHOOSE'); ?></option>
                                <?php
                                foreach ($filelist as $key => $file) {
                                    $file = str_replace(array('\\'), '/', $file);
                                    $fname = explode("/", $file);
                                    $fname = $fname[count($fname) - 1];

                                    $selected = "";
                                    if ($fname == JFactory::getApplication()->input->getString('chooseFile')) {
                                        $selected = "selected";
                                    }
                                    ?>
                                    <option value="<?php echo $fname ?>" <?php echo $selected ?>><?php echo $fname ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        //get start
        $num = JFactory::getApplication()->input->getInt('start_scan');
        $fileName = JFactory::getApplication()->input->getString('chooseFile');
        if (isset($num)) {
            $num = $num;
        } else {
            $num = 0;
        }

        $arr_phone = array();
        $arr_phone_num = array();
        $arr_phone_error = array();

        if (isset($fileName) && !empty($fileName)) {
            //end: get start
            $filename = OPENSHOP_PATH_SCANS . $fileName;
            $inputFileType = PHPExcel_IOFactory::identify($filename);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);

            $objReader->setReadDataOnly(true);
            /**  Load $inputFileName to a PHPExcel Object  * */
            $objPHPExcel = $objReader->load($filename);

            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow = $objWorksheet->getHighestRow();
            $row_limit = 1;

            for ($row = $num; $row <= $highestRow; ++$row) {
                if ($row_limit <= 200) {
                    $value = trim($objPHPExcel->getActiveSheet()->getCell('A' . $row)->getValue());
                    $value = str_replace(array(' ', '.'), '', $value);
                    if (isset($value)) {
                        $check_0 = substr($value, 0, 1);
                        if ($check_0 != '0') {
                            $value = '0' . $value;
                        }

                        if (strlen($value) == 10) {
                            $check_2 = substr($value, 0, 2);
                            if ($check_2 == '09') {
                                if (!in_array($value, $arr_phone)) {
                                    $arr_phone[] = $value;
                                    $arr_phone_num[] = $row;
                                } else {
                                    $arr_phone_error[] = $value;
                                }
                            } else {
                                $arr_phone_error[] = $value;
                            }
                        } else if (strlen($value) == 11) {
                            $check_2 = substr($value, 0, 2);
                            if ($check_2 == '01') {
                                if (!in_array($value, $arr_phone)) {
                                    $arr_phone[] = $value;
                                    $arr_phone_num[] = $row;
                                } else {
                                    $arr_phone_error[] = $value;
                                }
                            } else {
                                $arr_phone_error[] = $value;
                            }
                        } else {
                            $arr_phone_error[] = $value;
                        }
                    }

                    ++$row_limit;
                }
            }
        }
        ?>
        <div onclick='qrCodeJquery(<?php echo json_encode($arr_phone, true); ?>, <?php echo json_encode($arr_phone_num, true); ?>)' class="show_qrcode">Hiển thị QRCode <span class="badge" style="background-color:black"><?php echo count($arr_phone) ?></span></div>
        <div onclick='qrCodeJqueryError(<?php echo json_encode($arr_phone_error, true); ?>)' class="show_error">SĐT Lỗi <span class="badge" style="background-color:black"><?php echo count($arr_phone_error) ?></span></div>
        <div style="display: inline-block;">
            <input type="text" value="<?php echo $num ?>" name="start_scan" id="start_scan" onchange="submitFormScanQRCodeStart(this.value)" style="width: 100px; margin-top: 5px;"/>
        </div>
        <div style="display: inline-block;margin-left: 50px;">
            Hiện thị SĐT: <b><?php echo $num ?></b> đến <b><?php echo $num + 200 ?></b>
        </div>
        <p>
            <?php
            foreach ($arr_phone as $key => $value) {
                ?>
            <div id="id_qrcode_<?php echo $value ?>" style="display: inline-block;padding: 0 45px 60px 0;">
                <span style="position: relative; top: 20px; left: 100px;" class="text_<?php echo $value ?>"></span>
            </div>
            <?php
        }
        ?>
        </p>
    </form>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="showError">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel">SĐT lỗi</h2>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text_center">STT</th>
                            <th class="text_center">SĐT</th>
                        </tr>
                    </thead>
                    <tbody class="content_error">
                        <!--//content-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!--//upload-->
<div class="modal fade" id="myModalUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel"><?php echo JText::_('OPENSHOP_UPLOAD_TITLE'); ?></h2>
            </div>
            <div class="modal-body">
                <form target="_blank" action="index.php?option=com_openshop&format=ajax&task=functions.uploadFilePhone" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for=""><?php echo JText::_('OPENSHOP_UPLOAD_NAME'); ?></label>
                        <input type="file" name="fileImport" id="upload" />
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submitUpload" value="<?php echo JText::_('OPENSHOP_UPLOAD'); ?>"
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function qrCodeJquery(val, num) {
        var start = $('#start_scan').val();
        $.each(val, function (index, value) {
            if (value !== null) {
                jQuery('#id_qrcode_' + value).qrcode({
                    text: value.toString(),
                    width: 150,
                    height: 150
                });

                jQuery('.text_' + value).text((parseInt(num[index])));
            }

        });
    }

    function qrCodeJqueryError(val) {
        var h = '';
        $.each(val, function (index, value) {
            h += '<tr>';
            h += '  <td class="text_canter">' + (index + 1) + '</td>';
            h += '  <td class="text_canter">' + value + '</td>';
            h += '</tr>';
        });

        $('.content_error').html(h);
        $('#showError').modal('show');
    }

    function submitFormScanQRCodeStart(val) {
        $('#formScanQRCode').submit();
    }
</script>