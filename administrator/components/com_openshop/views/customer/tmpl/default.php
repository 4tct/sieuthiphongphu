<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'customer.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
            if (form.fullname.value == '') {
                alert("<?php echo JText::_('OPENSHOP_ENTER_FULLNAME'); ?>");
                form.firstname.focus();
                return;
            }
            if (form.email.value == '') {
                alert("<?php echo JText::_('OPENSHOP_ENTER_EMAIL'); ?>");
                form.email.focus();
                return;
            }
            if (form.telephone.value == '') {
                alert("<?php echo JText::_('OPENSHOP_ENTER_TELEPHONE'); ?>");
                form.telephone.focus();
                return;
            }
<?php
if (!$this->item->id) {
    ?>
                if (form.username.value == '') {
                    alert("<?php echo JText::_('OPENSHOP_ENTER_USERNAME'); ?>");
                    form.username.focus();
                    return;
                }
                if (form.password.value == '') {
                    alert("<?php echo JText::_('OPENSHOP_ENTER_PASSWORD'); ?>");
                    form.password.focus();
                    return;
                }
    <?php
}
?>
            Joomla.submitform(pressbutton, form);
        }
    }
</script>

<style>
    .input-memdium{
        width: 400px;
    }
</style>

<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm">
        <div class="row-fluid">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#general-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_GENERAL'); ?></a></li>
            </ul>
            <div class="tab-content" style="overflow: visible !important">
                <div class="tab-pane active" id="general-page">
                    <div class="span8">
                        <table class="admintable adminform" style="width: 100%;">
                            <tr>
                                <td class="key">
                                    <span class="required">*</span>
                                    <?php echo JText::_('OPENSHOP_FULL_NAME'); ?>
                                </td>
                                <td>
                                    <input class="input-memdium" type="text" name="fullname" id="fullname" maxlength="32" value="<?php echo $this->item->fullname; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <span class="required">*</span>
                                    <?php echo JText::_('OPENSHOP_EMAIL'); ?>
                                </td>
                                <td>
                                    <input class="input-memdium" type="text" name="email" id="email" maxlength="96" value="<?php echo $this->item->email; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <span class="required">*</span>
                                    <?php echo JText::_('OPENSHOP_TELEPHONE'); ?>
                                </td>
                                <td>
                                    <input class="input-memdium" type="text" name="telephone" id="telephone" maxlength="32" value="<?php echo $this->item->telephone; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_FAX'); ?>
                                </td>
                                <td>
                                    <input class="input-memdium" type="text" name="fax" id="fax" maxlength="32" value="<?php echo $this->item->fax; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key"><?php echo JText::_('OPENSHOP_ADDRESS'); ?></td>
                                <td>
                                    <input class="input-memdium" type="text" name="address" id="address" maxlength="32" value="<?php echo $this->item->address; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="key"><?php echo JText::_('OPENSHOP_ZONES'); ?></td>
                                <td>
                                    <select name="zone_id" id="zone_id" class="input-memdium" onchange="changeZone(this.value)">
                                        <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_ZONE') ?></option>
                                        <?php
                                        foreach ($this->town as $v_z) {
                                            $slt = '';
                                            if($this->item->zone_id == $v_z->id){
                                                $slt = 'selected';
                                            }
                                            echo '<option value="' . $v_z->id . '" ' . $slt . '>' . $v_z->zone_name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="key"><?php echo JText::_('OPENSHOP_DISTRICT'); ?></td>
                                <td>
                                    <select name="district_id" id="district_id" class="input-memdium">
                                        <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_DISTRICT') ?></option>
                                        <?php
                                        foreach ($this->district as $v_d) {
                                            $slt = '';
                                            if($this->item->district_id == $v_d->id){
                                                $slt = 'selected';
                                            }
                                            echo '<option value="' . $v_d->id . '" ' . $slt . '>' . $v_d->district_name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_CUSTOMERGROUP'); ?>
                                </td>
                                <td>
                                    <?php echo $this->lists['customergroup_id']; ?>
                                </td>
                            </tr>
                            <?php
                            if (!$this->item->id) {
                                ?>
                                <tr>
                                    <td class="key">
                                        <span class="required">*</span>
                                        <?php echo JText::_('OPENSHOP_USERNAME'); ?>
                                    </td>
                                    <td>
                                        <input class="input-memdium" type="text" name="username" maxlength="150" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="key">
                                        <span class="required">*</span>
                                        <?php echo JText::_('OPENSHOP_PASSWORD'); ?>
                                    </td>
                                    <td>
                                        <input class="input-memdium" type="password" name="password" maxlength="100" value="" />
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                                </td>
                                <td>
                                    <?php echo $this->lists['published']; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
        <input type="hidden" name="customer_id" value="<?php echo $this->item->customer_id; ?>" />
        <input type="hidden" name="task" value="" />
    </form>
</fieldset>