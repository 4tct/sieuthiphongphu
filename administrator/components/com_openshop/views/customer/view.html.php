<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCustomer extends OpenShopViewForm {

    function _buildListArray(&$lists, $item) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, b.customergroup_name AS text')
                ->from('#__openshop_customergroups AS a')
                ->innerJoin('#__openshop_customergroupdetails AS b ON (a.id = b.customergroup_id)')
                ->where('a.published = 1')
                ->order('b.customergroup_name');
        $db->setQuery($query);
        $lists['customergroup_id'] = JHtml::_('select.genericlist', $db->loadObjectList(), 'customergroup_id', array(
                    'option.text.toHtml' => false,
                    'option.text' => 'text',
                    'option.value' => 'value',
                    'list.attr' => ' class="inputbox chosen" ',
                    'list.select' => $item->customergroup_id));

        //zone
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_zones'))
                ->where('published = 1');
        $this->town = $db->setQuery($query)->loadObjectList();

        //district
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_districts'))
                ->where('published = 1');
        $this->district = $db->setQuery($query)->loadObjectList();
        return true;
    }

}
