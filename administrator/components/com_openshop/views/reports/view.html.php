<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewReports extends JViewLegacy {

    /**
     * 
     * Display function
     * 
     */
    function display($tpl = null) {
        // Check access first
        $mainframe = JFactory::getApplication();
		if (!JFactory::getUser()->authorise('openshop.reports', 'com_openshop')) {
            $mainframe->enqueueMessage(JText::_('OPENSHOP_ACCESS_NOT_ALLOW'), 'error');
            $mainframe->redirect('index.php?option=com_openshop&view=dashboard');
        }
        OpenShopPermissions::CheckPermission();
        switch ($this->getLayout()) {
            case 'orders':
                $this->_displayOrders($tpl);
                break;
            case 'viewedproducts':
                $this->_displayViewedProducts($tpl);
                break;
            case 'purchasedproducts':
                $this->_displayPurchasedProducts($tpl);
                break;
            default:
                break;
        }
    }

    /**
     * 
     * Function to display orders report
     * @param string $tpl
     */
    function _displayOrders($tpl) {
        $currency = new OpenShopCurrency();
        $lists = array();
        $options = array();
        $options[] = JHtml::_('select.option', 'year', JText::_('OPENSHOP_YEARS'), 'value', 'text');
        $options[] = JHtml::_('select.option', 'month', JText::_('OPENSHOP_MONTHS'), 'value', 'text');
        $options[] = JHtml::_('select.option', 'week', JText::_('OPENSHOP_WEEKS'), 'value', 'text');
        $options[] = JHtml::_('select.option', 'day', JText::_('OPENSHOP_DAYS'), 'value', 'text');
        $groupBy = JRequest::getVar('group_by', 'week');
        $lists['group_by'] = JHtml::_('select.genericlist', $options, 'group_by', array(
                    'option.text.toHtml' => false,
                    'option.value' => 'value',
                    'option.text' => 'text',
                    'list.select' => $groupBy,
                    'list.attr' => ' class="inputbox" style="width: 100px;"'));
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, b.orderstatus_name AS text')
                ->from('#__openshop_orderstatuses AS a')
                ->innerJoin('#__openshop_orderstatusdetails AS b ON (a.id = b.orderstatus_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
        $db->setQuery($query);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDERSSTATUS_ALL'));
        $options = array_merge($options, $db->loadObjectList());
        $lists['order_status_id'] = JHtml::_('select.genericlist', $options, 'order_status_id', ' class="inputbox" style="width: 150px;" ', 'value', 'text', JRequest::getInt('order_status_id'));
        $orders = $this->get('OrdersData');
        $pagination = $this->get('OrdersPagination');
        $this->items = $orders;
        $this->pagination = $pagination;
        $this->currency = $currency;
        $this->lists = $lists;
        parent::display($tpl);
    }

    /**
     * 
     * Function to display viewed products report
     * @param unknown $tpl
     */
    function _displayViewedProducts($tpl) {
        $products = $this->get('ViewedProductsData');
        $totalHits = 0;
        foreach ($products as $product) {
            $totalHits += (int) $product->hits;
        }
        $pagination = $this->get('ViewedProductsPagination');
        $this->items = $products;
        $this->pagination = $pagination;
        $this->totalHits = $totalHits;
        parent::display($tpl);
    }

    /**
     * 
     * Function to display purchased products report
     * @param unknown $tpl
     */
    function _displayPurchasedProducts($tpl) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, b.orderstatus_name AS text')
                ->from('#__openshop_orderstatuses AS a')
                ->innerJoin('#__openshop_orderstatusdetails AS b ON (a.id = b.orderstatus_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
        $db->setQuery($query);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDERSSTATUS_ALL'));
        $options = array_merge($options, $db->loadObjectList());
        $lists = array();
        $lists['order_status_id'] = JHtml::_('select.genericlist', $options, 'order_status_id', ' class="inputbox" style="width: 150px;" ', 'value', 'text', JRequest::getInt('order_status_id'));
        $currency = new OpenShopCurrency();
        $products = $this->get('PurchasedProductsData');
        $pagination = $this->get('PurchasedProductsPagination');
        $this->items = $products;
        $this->pagination = $pagination;
        $this->currency = $currency;
        $this->lists = $lists;
        parent::display($tpl);
    }

}
