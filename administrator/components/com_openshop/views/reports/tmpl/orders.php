<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JToolBarHelper::title(JText::_('OPENSHOP_ORDERS_REPORT'), 'generic.png');
JToolBarHelper::custom('exports.process', 'download', 'download', Jtext::_('OPENSHOP_EXPORTS'), false);
JToolBarHelper::custom('order.downloadInvoice', 'print', 'print', JText::_('OPENSHOP_DOWNLOAD_INVOICE'), false);
JToolBarHelper::cancel('reports.cancel');
?>
<fieldset class="adminfs">
<form action="index.php?option=com_openshop&view=reports&layout=orders" method="post" name="adminForm" id="adminForm">
	<table width="100%">
		<tr>
			<td align="left" valign="top">
				<?php echo JText::_('OPENSHOP_START_DATE')?>:&nbsp;
				<?php echo JHtml::_('calendar', JRequest::getVar('date_start', date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'))), 'date_start', 'date_start', '%Y-%m-%d', array('style' => 'width: 100px;')); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo JText::_('OPENSHOP_END_DATE')?>:&nbsp;
				<?php echo JHtml::_('calendar', JRequest::getVar('date_end', date('Y-m-d')), 'date_end', 'date_end', '%Y-%m-%d', array('style' => 'width: 100px;')); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo JText::_('OPENSHOP_GROUP_BY'); ?>:&nbsp;
				<?php echo $this->lists['group_by']; ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo JText::_('OPENSHOP_ORDERSTATUS'); ?>:&nbsp;
				<?php echo $this->lists['order_status_id']; ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button onclick="this.form.submit();" class="btn"><?php echo JText::_( 'OPENSHOP_GO' ); ?></button>
			</td>
		</tr>
	</table>
	<table class="adminlist table table-striped">
		<thead>
			<tr>
				<th width="20%" class="text_left"><?php echo JText::_('OPENSHOP_START_DATE'); ?></th>
				<th width="20%" class="text_left"><?php echo JText::_('OPENSHOP_END_DATE'); ?></th>
				<th width="15%" class="text_center"><?php echo JText::_('OPENSHOP_NUMBER_ORDERS'); ?></th>
				<th width="15%" class="text_center"><?php echo JText::_('OPENSHOP_NUMBER_PRODUCTS'); ?></th>
				<th width="15%" class="text_center"><?php echo JText::_('OPENSHOP_TAX'); ?></th>
				<th width="15%" class="text_center"><?php echo JText::_('OPENSHOP_TOTAL'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$k = 0;
			for ($i=0, $n=count( $this->items ); $i < $n; $i++)
			{
				$row = &$this->items[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td class="text_left">
						<?php echo JHtml::_('date', $row->date_start, OpenShopHelper::getConfigValue('date_format', 'm-d-Y')); ?>
					</td>																			
					<td class="text_left">
						<?php echo JHtml::_('date', $row->date_end, OpenShopHelper::getConfigValue('date_format', 'm-d-Y')); ?>
					</td>
					<td class="text_center">
						<?php echo $row->orders; ?>
					</td>
					<td class="text_center">
						<?php echo $row->products; ?>
					</td>
					<td class="text_center">
						<?php echo $this->currency->format($row->tax ? $row->tax : 0, OpenShopHelper::getConfigValue('default_currency_code')); ?>
					</td>
					<td class="text_center">
						<?php echo $this->currency->format($row->total ? $row->total : 0, OpenShopHelper::getConfigValue('default_currency_code')); ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</tbody>
	</table>
	<?php echo JHtml::_( 'form.token' ); ?>
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="export_type" value="orders" />
	<input type="hidden" name="from_exports" value="1" />
</form>
</fieldset>