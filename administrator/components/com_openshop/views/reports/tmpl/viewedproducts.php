<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

JToolBarHelper::title(JText::_('OPENSHOP_VIEWED_PRODUCTS_REPORT'), 'generic.png');
JToolBarHelper::cancel('reports.cancel');
?>
<fieldset class="adminfs">
<form action="index.php?option=com_openshop&view=reports&layout=viewedproducts" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<table class="adminlist table table-striped">
		<thead>
			<tr>
				<th class="text_left" width="55%">
					<?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
				</th>
				<th class="text_center" width="15%">
					<?php echo JText::_('OPENSHOP_PRODUCT_SKU'); ?>
				</th>
				<th class="text_center" width="15%">
					<?php echo JText::_('OPENSHOP_PRODUCT_VIEWED'); ?>
				</th>
				<th class="text_center" width="15%">
					<?php echo JText::_('OPENSHOP_PRODUCT_PERCENT'); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="4">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php
			$k = 0;
			for ($i = 0, $n = count( $this->items ); $i < $n; $i++)
			{
				$row = &$this->items[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td class="text_left">																			
						<?php echo $row->product_name; ?>				
					</td>																			
					<td class="text_center">
						<?php echo $row->product_sku; ?>
					</td>
					<td class="text_center">
						<?php echo $row->hits; ?>
					</td>
					<td class="text_center">
						<?php echo $this->totalHits ? number_format($row->hits * 100 / $this->totalHits, 2) : '0.00'; ?>%
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</tbody>
	</table>
	<?php echo JHtml::_( 'form.token' ); ?>
	<input type="hidden" name="task" value="" />
</form>
</fieldset>