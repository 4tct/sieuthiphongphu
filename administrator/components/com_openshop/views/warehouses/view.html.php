<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewWareHouses extends OpenShopViewList{
    public static function getNameArgency($id_ar)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        
        $query->select('argency_name')
                ->from($db->quoteName('#__openshop_argencies'))
                ->where('id = ' . (int)$id_ar);
        
        $db->setQuery($query);
        
        return $res = $db->loadResult();
    }
    
  
    
}