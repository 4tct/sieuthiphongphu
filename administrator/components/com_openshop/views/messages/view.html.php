<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewMessages extends OpenShopViewList {

    /**
     * Build the toolbar for view list
     */
    public function _buildToolbar() {
        $controller = OpenShopInflector::singularize($this->getName());
        $viewName = $this->getName();
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
        if (OpenShopHelper::getPermissionAction($viewName, 'edit')) {
            JToolBarHelper::editList($controller . '.edit');
        }
    }

}
