<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
?>
<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=messages" method="post" name="adminForm" id="adminForm">
        <div id="editcell">
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th width="2%" class="text_center">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>			
                        <th width="2%" class="text_center">
                            <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                        </th>
                        <th class="text_left" width="40%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_TITLE'), 'message_title', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>	
                        <th class="text_left" width="40%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_NAME'), 'message_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th width="5%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'id', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $link = JRoute::_('index.php?option=com_openshop&task=message.edit&cid[]=' . $row->id);
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <td class="text_center">
                                <?php echo $checked; ?>
                            </td>	
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo $row->message_title; ?></a>				
                            </td>
                            <td>
                                <?php echo $row->message_name; ?>
                            </td>
                            <td class="text_center">
                                <?php echo $row->id; ?>
                            </td>
                        </tr>		
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>