<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
?>
<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=brands" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>
                </td>
                <td align="right">
                    <?php echo $this->lists['filter_state']; ?>
                </td>
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th width="2%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th width="2%" class="text_center">
                            <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                        </th>
                        <?php
                        if (OpenShopHelper::isJ3()) {
                            ?>
                            <th width="1%" class="text_center" style="min-width:55px">
                                <?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                            </th>
                            <?php
                        }
                        ?>
                        <th class="text_left" width="40%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_NAME'), 'b.brand_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <?php
                        if (!OpenShopHelper::isJ3()) {
                            ?>
                            <th width="5%" class="text_center">
                                <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_PUBLISHED'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                            </th>
                            <?php
                        }
                        ?>
                        <th width="5%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_HITS'), 'a.hits', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th width="10%" class="text_right">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ORDER'), 'a.ordering', $this->lists['order_Dir'], $this->lists['order']); ?>
                            <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'brand.save_order'); ?>
                        </th>
                        <th width="5%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>													
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $link = JRoute::_('index.php?option=com_openshop&task=brand.edit&cid[]=' . $row->id);
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        $published = JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', 'brand.');
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <td class="text_center">
                                <?php echo $checked; ?>
                            </td>
                            <?php
                            if (OpenShopHelper::isJ3()) {
                                ?>
                                <td class="text_center">
                                    <div class="btn-group">
                                        <?php
                                        echo JHtml::_('jgrid.published', $row->published, $i, 'brand.');
                                        echo $this->addDropdownList(JText::_('OPENSHOP_COPY'), 'copy', $i, 'brand.copy');
                                        echo $this->addDropdownList(JText::_('OPENSHOP_DELETE'), 'trash', $i, 'brand.remove');
                                        echo $this->renderDropdownList($this->escape($row->brand_name));
                                        ?>
                                    </div>
                                </td>
                                <?php
                            }
                            ?>
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo $row->brand_name; ?></a>
                                <p class="smallsub">
                                    <?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($row->brand_alias)); ?></p>				
                            </td>			
                            <?php
                            if (!OpenShopHelper::isJ3()) {
                                ?>
                                <td class="text_center">
                                    <?php echo $published; ?>
                                </td>
                                <?php
                            }
                            ?>
                            <td class="text_center">
                                <?php echo (int) $row->hits; ?>
                            </td>
                            <td class="order text_right">
                                <span><?php echo $this->pagination->orderUpIcon($i, true, 'brand.orderup', 'Move Up', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'brand.orderdown', 'Move Down', $ordering); ?></span>
                                <?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?>				
                                <input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="input-mini" style="text-align: center" <?php echo $disabled; ?> />
                            </td>
                            <td class="text_center">
                                <?php echo $row->id; ?>
                            </td>
                        </tr>		
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>