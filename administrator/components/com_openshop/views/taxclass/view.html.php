<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewTaxclass extends OpenShopViewForm
{

	function _buildListArray(&$lists, $item)
	{
		$db = JFactory::getDbo();
		if ($item->id)
		{
			$query = $db->getQuery(true);
			$query->select("tax_id, based_on, priority")
				->from("#__openshop_taxrules")
				->where("taxclass_id=" . (int) $item->id);
			$db->setQuery($query);
			$taxrateIds = $db->loadObjectList();
			$query->clear();
			$query->select("id, tax_name AS name")
				->from("#__openshop_taxes")
				->where("published = 1");
			$db->setQuery($query);
			$taxrates = array();
			$taxrates = array_merge($taxrates, $db->loadObjectList());
			$baseonOptions = array();
			$baseonOptions[] = JHtml::_('select.option', 'shipping', JText::_('OPENSHOP_SHIPPING_ADDRESS'));
			$baseonOptions[] = JHtml::_('select.option', 'payment', JText::_('OPENSHOP_PAYMENT_ADDRESS'));
			$baseonOptions[] = JHtml::_('select.option', 'store', JText::_('OPENSHOP_STORE_ADDRESS'));
			$this->baseonOptions = $baseonOptions;
			$this->taxrates = $taxrates;
			$this->taxrateIds = $taxrateIds;
		}
		JFactory::getDocument()->addScriptDeclaration(OpenShopHtmlHelper::getTaxrateOptionsJs())->addScriptDeclaration(
			OpenShopHtmlHelper::getBaseonOptionsJs());
		OpenShopHelper::chosen();
	}
}