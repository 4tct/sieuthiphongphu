<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$editor = JFactory::getEditor();
OpenShopHelper::chosen();
JFactory::getDocument()->addScript(JUri::base() . '/components/com_openshop/assets/js/geozone.js');
?>

<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'geozone.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
            if (form.check_error.value == '1') {
                toastr['error']('Don\'t connect Server');
                form.check_error.focus();
                return;
            }
            Joomla.submitform(pressbutton, form);
        }
    }
    var rowIndex = <?php echo count($this->zoneToGeozones); ?>;
    var defaultCountry = <?php echo $this->countryId; ?>;
    function addGeoZone() {
        var countryId = 'country' + rowIndex;
        var zoneId = 'zone' + rowIndex;
        var districtId = 'district' + rowIndex;
        html = '<tr id="zone-to-geo-zone-row' + rowIndex + '">';
        html += '<td style="text-align: center; display:none;"><select name="country_id[]"  id="country' + rowIndex + '" onchange="OpenShop.updateStateList(this.value,\'' + zoneId + '\')">';
        html += countriesOptions;
        html += '</select></td>';
        html += '<td style="text-align: center;"><select name="zone[]" style="width: 220px;" id="zone' + rowIndex + '" onchange="OpenShop.updateDistrictList(this.value,\'' + districtId + '\')"><option value="0"><?php echo JText::_('OPENSHOP_ALL_ZONES'); ?></option></select></td>';
        html += '<td style="text-align: center;"><select name="district[]" style="width: 220px;" id="district' + rowIndex + '" onchange="check_geozone('+ rowIndex +')"><option value="0"><?php echo JText::_('OPENSHOP_ALL_DISTRICTS'); ?></option></select></td>';
        html += '<td style="text-align: center;"><input type="button" onclick="jQuery(\'#zone-to-geo-zone-row' + rowIndex + '\').remove();" class="btn btn-small btn-primary" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>"></td>';
        html += '<dt><input type="hidden" name="info_zone_id" id="info_zone_id'+ rowIndex +'" value="" /></td>';
        html += '</tr>';
        jQuery('#zone-to-geo-zone').append(html);
        jQuery('#country' + rowIndex).attr('value', defaultCountry);
        OpenShop.updateStateList(defaultCountry, zoneId);
//        OpenShop.updateDistrictList('',districtId);
        rowIndex++;
        jQuery("#" + countryId).chosen();
    }
<?php
if (count($this->zoneToGeozones)) {
    $index = 0;
    ?>
        jQuery(document).ready(function () {
    <?php
    foreach ($this->zoneToGeozones as $ToGeoZone) {
        $zoneSelectTagId = 'zone' . $index;
        $districtSelectTagId = 'district' . $index;
        ?>
                //zone
                OpenShop.updateStateList(<?php echo $ToGeoZone->country_id; ?>, '<?php echo $zoneSelectTagId ?>');
                jQuery('#<?php echo $zoneSelectTagId; ?>').attr('value', <?php echo $ToGeoZone->zone_id; ?>);
                //district
                OpenShop.updateDistrictList(<?php echo $ToGeoZone->zone_id; ?>, '<?php echo $districtSelectTagId ?>');
                jQuery('#<?php echo $districtSelectTagId; ?>').attr('value', <?php echo $ToGeoZone->district_id; ?>);
        <?php
        $index++;
    }
    ?>
        });
    <?php
}
?>
</script>


<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row-fluid">
        <div class="span5">
            <fieldset class="adminfs">
                <legend><?php echo JText::_('OPENSHOP_GEOZONE_DETAILS'); ?></legend>
                <table class="admintable adminform" style="width: 100%;">
                    <tr>
                        <td class="key">
                            <span class="required">*</span>
                            <?php echo JText::_('OPENSHOP_NAME'); ?>
                        </td>
                        <td>
                            <?php echo $this->lists['transporters'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <?php echo JText::_('OPENSHOP_DESCRIPTION'); ?>
                        </td>
                        <td>
                            <textarea rows="5" cols="40" name="geozone_desc"><?php echo $this->item->geozone_desc; ?></textarea>
                        </td>
                    </tr>	
                    <tr>
                        <td class="key">
                            <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                        </td>
                        <td>
                            <?php echo $this->lists['published']; ?>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div class="span7">
            <fieldset class="adminfs">
                <legend><?php echo JText::_('OPENSHOP_GEOZONE_ZONES'); ?></legend>
                <table class="adminlist table table-bordered" style="text-align: center;">
                    <thead>
                        <tr>
                            <th class="title" style="display:none;"><?php echo JText::_('OPENSHOP_COUNTRY') ?></th>
                            <th class="title"><?php echo JText::_('OPENSHOP_ZONE') ?></th>
                            <th class="title"><?php echo JText::_('OPENSHOP_DISTRICT') ?></th>
                            <th class="title">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody id="zone-to-geo-zone">
                        <?php
                        $rowIndex = 0;
                        if (count($this->zoneToGeozones)) {
                            foreach ($this->zoneToGeozones as $ToGeoZone) {
                                ?>					
                                <tr id="zone-to-geo-zone-row<?php echo $rowIndex ?>">
                                    <td style="text-align: center; display: none;">
                                        <?php echo JHtml::_('select.genericlist', $this->countryOptions, 'country_id[]', ' class="inputbox chosen" onchange="OpenShop.updateStateList(this.value, \'zone' . $rowIndex . '\')"', 'id', 'name', $ToGeoZone->country_id, 'country' . $rowIndex); ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <select name="zone[]" class="inputbox" style="width: 220px;" id="zone<?php echo $rowIndex; ?>"></select>
                                    </td>
                                    <td style="text-align: center;">
                                        <select name="district[]" class="inputbox" style="width: 220px;" id="district<?php echo $rowIndex; ?>" onchange="check_geozone(<?php echo $rowIndex; ?>)"></select>
                                    </td>
                                    <td style="text-align: center;">
                                        <input type="button" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" class="btn btn-small btn-primary" onclick="jQuery('#zone-to-geo-zone-row<?php echo $rowIndex; ?>').remove();">
                                    </td>
                                </tr>						
                                <?php
                                $rowIndex++;
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" class="left"><input class="btn btn-small btn-primary" type="button" name="add" value="<?php echo JText::_('OPENSHOP_BTN_ADD') ?>" onclick="addGeoZone();" ></td>
                        </tr>	
                    </tfoot>									
                </table>
                
            </fieldset>
        </div>
    </div>
    <?php echo JHtml::_('form.token'); ?>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="check_error" id="check_error" value="0" />
</form>
