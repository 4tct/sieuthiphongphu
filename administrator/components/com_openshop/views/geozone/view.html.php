<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewGeozone extends OpenShopViewForm {

    function _buildListArray(&$lists, $item) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id, country_name AS name')
                ->from('#__openshop_countries')
                ->where('published=1')
                ->order('country_name');
        $db->setQuery($query);
        $countryOptions = $db->loadObjectList();
        $query->clear();
        $query->select('zone_id,country_id,district_id')
                ->from('#__openshop_geozonezones')
                ->where('geozone_id = ' . (int) $item->id);
        $db->setQuery($query);
        $zoneToGeozones = $db->loadObjectList();
        $config = OpenShopHelper::getConfig();
        JFactory::getDocument()
                ->addScriptDeclaration(OpenShopHtmlHelper::getDistrictArrayJs())
                ->addScriptDeclaration(OpenShopHtmlHelper::getZonesArrayJs())
                ->addScriptDeclaration(OpenShopHtmlHelper::getCountriesOptionsJs());
        $this->countryId = $config->country_id;
        $this->countryOptions = $countryOptions;
        $this->zoneToGeozones = $zoneToGeozones;
        
        //Transporter
        $query->clear();
        $query->select('id as value, transporter_name as text')
                ->from($db->quoteName('#__openshop_transporters'));
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_TRANSPORTERS_SELECT'));
        if(count($rows)){
            $options = array_merge($options, $rows);
        }
        $lists['transporters'] = JHtml::_('select.genericlist', $options, 'transporter_id', 'class="form-control" style="width:220px" ' , 'value', 'text' ,$item->transporter_id);
    }

}
