<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
JFactory::getDocument()->addStyleSheet(JURI::base() . '/components/com_openshop/assets/css/product.css');
JFactory::getDocument()->addScript(JUri::base() . DS . 'components/com_openshop/assets/js/jquery.form.min.js');
JFactory::getDocument()->addScript(JUri::base() . '/components/com_openshop/assets/js/product.js');
$input = JFactory::getApplication()->input;
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'product.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
<?php
if ($translatable) {
    foreach ($this->languages as $language) {
        $langId = $language->lang_id;
        ?>
                    if (document.getElementById('product_name_<?php echo $langId; ?>').value == '') {
                        alert("<?php echo addcslashes(JText::_('OPENSHOP_ENTER_NAME'), '"'); ?>");
                        document.getElementById('product_name_<?php echo $langId; ?>').focus();
                        return;
                    }
        <?php
    }
} else {
    ?>
                if (form.product_name.value == '') {
                    alert("<?php echo addcslashes(JText::_('OPENSHOP_ENTER_NAME'), '"'); ?>");
                    form.product_name.focus();
                    return;
                }
    <?php
}
?>
            if (form.product_sku.value == '') {
                alert("<?php echo addcslashes(JText::_('OPENSHOP_ENTER_PRODUCT_SKU'), '"'); ?>");
                form.product_sku.focus();
                return;
            }

            if (jQuery('#checkFormSubmit').val() !== '1') {
                alert("Mã sản phẩm đã tồn tại");
                return;
            }

            Joomla.submitform(pressbutton, form);
        }
    }
    //Add or Remove product images
    var countProductImages = '<?php echo count($this->productImages); ?>';
    function addProductImage() {
        var html = '<tr id="product_image_' + countProductImages + '" style="height: 100px;">'
        //Image column
        html += '<td style="text-align: center; vertical-align: middle;"><input type="file" class="input" size="20" accept="image/*" name="image[]" /></td>';
        //Ordering column
        html += '<td style="text-align: center; vertical-align: middle;"><input class="input-small" type="text" name="image_ordering[]" maxlength="10" value="" /></td>';
        //Published column
        html += '<td style="text-align: center; vertical-align: middle;"><select class="inputbox" name="image_published[]">';
        html += '<option selected="selected" value="1"><?php echo addcslashes(JText::_('OPENSHOP_YES'), "'"); ?></option>';
        html += '<option value="0"><?php echo addcslashes(JText::_('OPENSHOP_NO'), "'"); ?></option>';
        html += '</select></td>';
        // Remove button column
        html += '<td style="text-align: center; vertical-align: middle;"><input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo addcslashes(JText::_('OPENSHOP_BTN_REMOVE'), "'"); ?>" onclick="removeProductImage(' + countProductImages + ');" /></td>';
        html += '</tr>';
        jQuery('#product_images_area').append(html);
        countProductImages++;
    }
    function removeProductImage(rowIndex) {
        jQuery('#product_image_' + rowIndex).remove();
    }
    //Add or Remove product attributes
    var countProductAttributes = '<?php echo count($this->productAttributes); ?>';
    function addProductAttribute() {
        var html = '<tr id="product_attribute_' + countProductAttributes + '">'
        //Attribute column
        html += '<td style="text-align: center; vertical-align: middle;"><?php echo preg_replace(array('/\r/', '/\n/'), '', $this->lists['attributes']); ?></td>';
        //Value column
        html += '<td style="text-align: center;">';
<?php
if ($translatable) {
    ?>

    <?php
    foreach ($this->languages as $language) {
        $langCode = $language->lang_code;
        ?>
                html += '<input class="input-large" type="text" name="attribute_value_<?php echo $langCode; ?>[]" maxlength="255" value="" />';
                html += '<img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" /><br />';
        <?php
    }
} else {
    ?>
            html += '<input class="input-large" type="text" name="attribute_value[]" maxlength="255" value="" />';
    <?php
}
?>
        html += '</td>';
        //Published column
        html += '<td style="text-align: center; vertical-align: middle;"><select class="inputbox" name="attribute_published[]">';
        html += '<option selected="selected" value="1"><?php echo addcslashes(JText::_('OPENSHOP_YES'), "'"); ?></option>';
        html += '<option value="0"><?php echo addcslashes(JText::_('OPENSHOP_NO'), "'"); ?></option>';
        html += '</select></td>';
        // Remove button column
        html += '<td style="text-align: center; vertical-align: middle;"><input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo addcslashes(JText::_('OPENSHOP_BTN_REMOVE'), "'"); ?>" onclick="removeProductAttribute(' + countProductAttributes + ');" /></td>';
        html += '</tr>';
        jQuery('#product_attributes_area').append(html);
        countProductAttributes++;
    }
    function removeProductAttribute(rowIndex) {
        jQuery('#product_attribute_' + rowIndex).remove();
    }
    //Options
<?php
$addedOptions = array();
for ($i = 0; $n = count($this->productOptions), $i < $n; $i++) {
    $addedOptions[] = '"' . $this->productOptions[$i]->id . '"';
}
?>
    var addedOptions = new Array(<?php echo implode($addedOptions, ','); ?>);
    function addProductOption() {
        //Change active tab
        for (var i = 0; i < addedOptions.length; i++) {
            jQuery('#product_option_' + addedOptions[i]).attr('class', '');
            jQuery('#option-' + addedOptions[i] + '-page').attr('class', 'tab-pane');
        }
        var optionSel = document.getElementById('option_id');
        //Find option type
        var optionTypeSel = document.getElementById('option_type_id');
        var optionType = 'Select';
        for (var i = 0; i < optionTypeSel.length; i++) {
            if (optionTypeSel.options[i].value == optionSel.value) {
                var optionType = optionTypeSel.options[i].text;
            }
        }
        //working
        var htmlTab = '<li id="product_option_' + optionSel.value + '" class="active">';
        htmlTab += '<a data-toggle="tab" href="#option-' + optionSel.value + '-page">' + optionSel.options[optionSel.selectedIndex].text;
        htmlTab += '<img onclick="removeProductOption(' + optionSel.value + ', \'' + optionSel.options[optionSel.selectedIndex].text + '\');" src="<?php echo JURI::base(); ?>components/com_openshop/assets/images/remove.png" />';
        htmlTab += '<input type="hidden" value="' + optionSel.value + '" name="productoption_id[]">';
        htmlTab += '</a></li>';
        jQuery('#nav-tabs').append(htmlTab);
        var htmlContent = '<div id="option-' + optionSel.value + '-page" class="tab-pane active">';
        htmlContent += '<table class="adminlist" style="width: 100%;">';
        htmlContent += '<tbody>';
        htmlContent += '<tr>';
        htmlContent += '<td style="width: 150px;"><?php echo addcslashes(JText::_('OPENSHOP_REQUIRED'), "'"); ?></td>';
        htmlContent += '<td><select class="inputbox" name="required_' + optionSel.value + '" id="required">';
        htmlContent += '<option selected="selected" value="1"><?php echo addcslashes(JText::_('OPENSHOP_YES'), "'"); ?></option>';
        htmlContent += '<option value="0"><?php echo addcslashes(JText::_('OPENSHOP_NO'), "'"); ?></option></select></td>';
        htmlContent += '</tr>';
        htmlContent += '</tbody>';
        htmlContent += '</table>';
        if (optionType == 'Select' || optionType == 'Radio' || optionType == 'Checkbox')
        {
            htmlContent += '<table style="text-align: center;" class="adminlist table table-bordered">';
            htmlContent += '<thead>';
            htmlContent += '<tr><th width="" class="title"><?php echo addcslashes(JText::_('OPENSHOP_OPTION_VALUE'), "'"); ?></th><th class="title" width=""><?php echo addcslashes(JText::_('OPENSHOP_SKU'), "'"); ?></th><th width="" class="title"><?php echo addcslashes(JText::_('OPENSHOP_QUANTITY'), "'"); ?></th><th width="" class="title"><?php echo addcslashes(JText::_('OPENSHOP_PRICE'), "'"); ?></th><th width="" class="title"><?php echo addcslashes(JText::_('OPENSHOP_WEIGHT'), "'"); ?></th><th class="title" width="" nowrap="nowrap"><?php echo addcslashes(JText::_('OPENSHOP_IMAGE'), "'"); ?></th><th width="" class="title">&nbsp;</th></tr>';
            htmlContent += '</thead>';
            htmlContent += '<tbody id="product_option_' + optionSel.value + '_values_area"></tbody>';
            htmlContent += '<tfoot>';
            htmlContent += '<tr><td colspan="7"><input type="button" onclick="addProductOptionValue(' + optionSel.value + ');" value="Add" name="btnAdd" class="btn btn-small btn-primary"></td></tr>';
            htmlContent += '</tfoot>';
            htmlContent += '</table>';
        } else if (optionType == 'Text' || optionType == 'Textarea')
        {
            htmlContent += '<table class="adminlist" style="width: 100%;">';
            htmlContent += '<tbody>';
            htmlContent += '<tr>';
            htmlContent += '<td style="width: 150px;"><?php echo addcslashes(JText::_('OPENSHOP_PRODUCT_PRICE_PER_CHAR'), "'"); ?></td>';
            htmlContent += '<td>';
            htmlContent += '<input type="hidden" value="null" id="optionvalue_' + optionSel.value + '_id" name="optionvalue_' + optionSel.value + '_id[]">';
            htmlContent += '<select style="width:auto;" class="inputbox" name="optionvalue_' + optionSel.value + '_price_sign[]" id="optionvalue_' + optionSel.value + '_price_sign">';
            htmlContent += jQuery('#price_sign').html();
            htmlContent += '</select>&nbsp;';
            htmlContent += '<input type="text" value="" maxlength="255" size="10" name="optionvalue_' + optionSel.value + '_price[]" class="input-small">';
            htmlContent += '</td>';
            htmlContent += '</tr>';
            htmlContent += '</tbody>';
            htmlContent += '</table>';
        }
        htmlContent += '</div>';
        jQuery('#tab-content').append(htmlContent);
        addedOptions[addedOptions.length] = optionSel.value;
        for (var i = optionSel.length - 1; i >= 0; i--) {
            if (optionSel.options[i].selected) {
                optionSel.remove(i);
                break;
            }
        }
    }
    function removeProductOption(optionId, optionName) {
        var optionHtml = '<option value="' + optionId + '">' + optionName + '</option>';
        jQuery('#option_id').append(optionHtml);
        jQuery('#product_option_' + optionId).remove();
        jQuery('#option-' + optionId + '-page').remove();
        addedOptions.splice(addedOptions.indexOf(optionId), 1);
    }
    //Option Values
    var countProductOptionValues = new Array();
<?php
for ($i = 0; $n = count($this->productOptions), $i < $n; $i++) {
    $productOption = $this->productOptions[$i];
    ?>
        countProductOptionValues['<?php echo $productOption->id ?>'] = '<?php echo count($this->productOptionValues[$i]); ?>';
    <?php
}
for ($i = 0; $n = count($this->options), $i < $n; $i++) {
    ?>
        if (countProductOptionValues['<?php echo $this->options[$i]->id; ?>'] === undefined) {
            countProductOptionValues['<?php echo $this->options[$i]->id; ?>'] = 0;
        }
    <?php
}
?>
    function addProductOptionValue(optionId) {
        var html = '<tr id="product_option_' + optionId + '_value_' + countProductOptionValues[optionId] + '">';
        //Option Value column
        html += '<td style="text-align: center;">';
        html += '<select class="inputbox" name="optionvalue_' + optionId + '_id[]">';
        html += jQuery('#option_values_' + optionId).html();
        html += '</select>'
        html += '</td>';
        //SKU column
        html += '<td style="text-align: center;">';
        html += '<input type="text" value="" maxlength="255" name="optionvalue_' + optionId + '_sku[]" class="input-small">';
        html += '</td>';
        //Quantity column
        html += '<td style="text-align: center;">';
        html += '<input type="text" value="" maxlength="255" name="optionvalue_' + optionId + '_quantity[]" class="input-small">';
        html += '</td>';
        //Price column
        html += '<td style="text-align: center;">';
        html += '<select class="inputbox" name="optionvalue_' + optionId + '_price_sign[]">';
        html += jQuery('#price_sign').html();
        html += '</select>';
        html += '<input type="text" value="" maxlength="255" name="optionvalue_' + optionId + '_price[]" class="input-small">';
        html += '</td>';
        //Weight column
        html += '<td style="text-align: center;">';
        html += '<select class="inputbox" name="optionvalue_' + optionId + '_weight_sign[]">';
        html += jQuery('#weight_sign').html();
        html += '</select>'
        html += '<input type="text" value="" maxlength="255" name="optionvalue_' + optionId + '_weight[]" class="input-small">';
        html += '</td>';
        //Image column
        html += '<td style="text-align: center;">';
        html += '<input type="file" name="optionvalue_' + optionId + '_image[]" accept="image/*" class="input-small">';
        html += '</td>';
        //Remove button column
        html += '<td style="text-align: center;">';
        html += '<input type="button" onclick="removeProductOptionValue(' + optionId + ', ' + countProductOptionValues[optionId] + ');" value="Remove" name="btnRemove" class="btn btn-small btn-primary">';
        html += '</td>';
        html += '</tr>';
        jQuery('#product_option_' + optionId + '_values_area').append(html);
        countProductOptionValues[optionId]++;
    }
    function removeProductOptionValue(optionId, rowIndex) {
        jQuery('#product_option_' + optionId + '_value_' + rowIndex).remove();
    }
    var countProductDiscounts = '<?php echo count($this->productDiscounts); ?>';
    function addProductDiscount() {
        var html = '<tr id="product_discount_' + countProductDiscounts + '">';
        //Customer group column
        html += '<td style="text-align: center;"><?php echo preg_replace(array('/\r/', '/\n/'), '', $this->lists['discount_customer_group']); ?></td>';
        //Quantity column
        html += '<td style="text-align: center;">';
        html += '<input type="text" value="" maxlength="10" name="discount_quantity[]" class="input-mini" />';
        html += '</td>';
        //Priority column
        html += '<td style="text-align: center;">';
        html += '<input type="text" value="" maxlength="10" name="discount_priority[]" class="input-small" />';
        html += '</td>';
        //Price column
        html += '<td style="text-align: center;">';
        html += '<input type="text" value="" maxlength="10" name="discount_price[]" class="input-small" />';
        html += '</td>';
        //Start date column
        html += '<td style="text-align: center;">';
        html += '<input type="text" style="width: 100px;" class="input-medium hasTooltip" value="" id="discount_date_start_' + countProductDiscounts + '" name="discount_date_start[]">';
        html += '<button id="discount_date_start_' + countProductDiscounts + '_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
        html += '</td>';
        //End date column
        html += '<td style="text-align: center;">';
        html += '<input type="text" style="width: 100px;" class="input-medium hasTooltip" value="" id="discount_date_end_' + countProductDiscounts + '" name="discount_date_end[]">';
        html += '<button id="discount_date_end_' + countProductDiscounts + '_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
        html += '</td>';
        //Published column
        html += '<td style="text-align: center;"><select class="inputbox" name="discount_published[]">';
        html += '<option selected="selected" value="1"><?php echo addcslashes(JText::_('OPENSHOP_YES'), "'"); ?></option>';
        html += '<option value="0"><?php echo addcslashes(JText::_('OPENSHOP_NO'), "'"); ?></option>';
        html += '</select></td>';
        // Remove button column
        html += '<td style="text-align: center;"><input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo addcslashes(JText::_('OPENSHOP_BTN_REMOVE'), "'"); ?>" onclick="removeProductDiscount(' + countProductDiscounts + ');" /></td>';
        html += '</tr>';
        jQuery('#product_discounts_area').append(html);
        Calendar.setup({
            // Id of the input field
            inputField: "discount_date_start_" + countProductDiscounts,
            // Format of the input field
            ifFormat: "%Y-%m-%d",
            // Trigger for the calendar (button ID)
            button: "discount_date_start_" + countProductDiscounts + "_img",
            // Alignment (defaults to "Bl")
            align: "Tl",
            singleClick: true,
            firstDay: 0
        });
        Calendar.setup({
            // Id of the input field
            inputField: "discount_date_end_" + countProductDiscounts,
            // Format of the input field
            ifFormat: "%Y-%m-%d",
            // Trigger for the calendar (button ID)
            button: "discount_date_end_" + countProductDiscounts + "_img",
            // Alignment (defaults to "Bl")
            align: "Tl",
            singleClick: true,
            firstDay: 0
        });
        countProductDiscounts++;
    }
    function removeProductDiscount(rowIndex) {
        jQuery('#product_discount_' + rowIndex).remove();
    }
    var countProductSpecials = '<?php echo count($this->productSpecials); ?>';
    function addProductSpecial() {
        var html = '<tr id="product_special_' + countProductSpecials + '">';
        //Customer group column
        html += '<td style="text-align: center;"><?php echo preg_replace(array('/\r/', '/\n/'), '', $this->lists['special_customer_group']); ?></td>';
        //Priority column
        html += '<td style="text-align: center;">';
        html += '<input type="text" value="" maxlength="10" name="special_priority[]" class="input-small" />';
        html += '</td>';
        //Price column
        html += '<td style="text-align: center;">';
        html += '<input type="text" value="" maxlength="10" name="special_price[]" class="input-small" />';
        html += '</td>';
        //Start date column
        html += '<td style="text-align: center;">';
        html += '<input type="text" style="width: 100px; " value="" id="special_date_start_' + countProductSpecials + '" name="special_date_start[]">';
        html += '<button id="special_date_start_' + countProductSpecials + '_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
        html += '</td>';
        //End date column
        html += '<td style="text-align: center;">';
        html += '<input type="text" style="width: 100px; " value="" id="special_date_end_' + countProductSpecials + '" name="special_date_end[]">';
        html += '<button id="special_date_end_' + countProductSpecials + '_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
        html += '</td>';
        //Published column
        html += '<td style="text-align: center;"><select class="inputbox" name="special_published[]">';
        html += '<option selected="selected" value="1"><?php echo addcslashes(JText::_('OPENSHOP_YES'), "'"); ?></option>';
        html += '<option value="0"><?php echo addcslashes(JText::_('OPENSHOP_NO'), "'"); ?></option>';
        html += '</select></td>';
        // Remove button column
        html += '<td style="text-align: center;"><input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo addcslashes(JText::_('OPENSHOP_BTN_REMOVE'), "'"); ?>" onclick="removeProductSpecial(' + countProductSpecials + ');" /></td>';
        html += '</tr>';
        jQuery('#product_specials_area').append(html);
        Calendar.setup({
            // Id of the input field
            inputField: "special_date_start_" + countProductSpecials,
            // Format of the input field
            ifFormat: "%Y-%m-%d",
            // Trigger for the calendar (button ID)
            button: "special_date_start_" + countProductSpecials + "_img",
            // Alignment (defaults to "Bl")
            align: "Tl",
            singleClick: true,
            firstDay: 0
        });
        Calendar.setup({
            // Id of the input field
            inputField: "special_date_end_" + countProductSpecials,
            // Format of the input field
            ifFormat: "%Y-%m-%d",
            // Trigger for the calendar (button ID)
            button: "special_date_end_" + countProductSpecials + "_img",
            // Alignment (defaults to "Bl")
            align: "Tl",
            singleClick: true,
            firstDay: 0
        });
        countProductSpecials++;
    }
    function removeProductSpecial(rowIndex) {
        jQuery('#product_special_' + rowIndex).remove();
    }

</script>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#main_info_product" aria-controls="main_info_product" role="tab" data-toggle="tab"><?php echo JText::_('OPENSHOP_PRODUCT') ?></a></li>
            <li role="presentation"><a href="#detail_product" aria-controls="profile" role="tab" data-toggle="tab"><?php echo JText::_('OPENSHOP_DETAILS') ?></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <!--MAIN INFO PRODUCT-->
            <div role="tabpanel" class="tab-pane active" id="main_info_product" style="background: #EFEFEF;">
                <!--row-->
                <div class="col-sm-12" style="background: #EFEFEF; padding-top: 10px;">
                    <!-- PRODUCT INFORMATION -->
                    <div class="panel panel-default span6 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_INFO') ?></div>
                        <div class="panel-body">
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_NAME'); ?></label>
                                <span class="col-sm-9">
                                    <input class="form-control" type="text" name="product_name" id="product_name" size="" maxlength="250" value="<?php echo $this->item->product_name; ?>"/>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="aliasProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_ALIAS'); ?></label>
                                <span class="col-sm-9">
                                    <input class="form-control" type="text" name="product_alias" id="product_alias" size="" maxlength="250" value="<?php echo $this->item->product_alias; ?>" placeholder="<?php echo JText::_('OPENSHOP_AUTO_INPUT') ?>"/>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_SKU'); ?></label>
                                <span class="col-sm-3">
                                    <?php
                                    $product_sku = $this->item->product_sku;
                                    $last_id = $this->suggestCode();
                                    if (empty($product_sku)) {
                                        $product_sku = "ST" . $last_id;
                                    }
                                    ?>

                                    <input class="form-control" style="width:100px" type="text" name="product_sku" id="product_sku" size="" maxlength="250" value="<?php echo $product_sku; ?>" onchange="checkProductSKU(this.value)"/>

                                    <div class="error error_message_product_sku"></div>

                                </span>
                                <span class="col-sm-6">
                                    <label for="nameProduct" class="col-sm-6 control-label"><?php echo JText::_('Mã SP NCC'); ?></label>
                                    <input class="form-control" style="width:100px" type="text" name="manufacproduct_sku" id="manufacproduct_sku" size="" maxlength="250" value="<?php echo $this->item->manufacproduct_sku ?>" />
                                </span>

                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label">Chọn đơn vị</label>
                                <span class="col-sm-9">
                                    <fieldset id="myEdit" class="radio btn-group">
                                        <input type="radio" id="myEdit0" value="1" name="donvi">
                                        <label for="myEdit0" class="btn <?php echo $this->item->donvi == '1' ? 'active btn-success' : ''; ?>">Cái</label>
                                        <input type="radio" id="myEdit1" value="0" name="donvi">
                                        <label for="myEdit1" class="btn <?php echo $this->item->donvi == '0' ? 'active btn-success' : ''; ?>">Cặp</label>
                                        <input type="radio" id="myEdit2" value="2" name="donvi">
                                        <label for="myEdit2" class="btn <?php echo $this->item->donvi == '2' ? 'active btn-success' : ''; ?>">Combo - Set</label>
                                    </fieldset>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label">Chọn sản phẩm màu khác</label>
                                <span class="col-sm-9">
                                    <!--<input class="form-control" type="text" name="product_color" id="product_color" size="" maxlength="250" value=""/>-->
                                    <ul class="tags">
                                        <?php
                                        if (!empty($this->item->id)) {
                                            $proDiffs = $this->getProductDiff($this->item->id);
                                            foreach ($proDiffs as $proDiff) {
                                                ?>
                                                <li class="addedTagProduct">
                                                    <?php echo $proDiff['sku_product'] ?>
                                                    <span onclick="jQuery(this).parent().remove();" class="tagRemove icon-delete "></span>
                                                    <input type="hidden" value="<?php echo $proDiff['id_product'] ?>" name="productSKUColorDifferent[]">
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>

                                        <li class="tagAdd taglist">
                                            <input type="text" class="form-control" id="searchProSKUColorDiff" placeholder="Nhập mã sản phẩm" onkeyup="getProductDiff(<?php echo empty($this->item->id) ? 0 : $this->item->id; ?>, this.value)">
                                            <div class="showSearchProductDiff">
                                                <ul>
                                                    <!--Product-->
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>

                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_QUANTITY'); ?></label>
                                <span class="col-sm-9">
                                    <input class="form-control" type="text" name="product_quantity" id="product_quantity" size="" maxlength="250" value="<?php echo $this->item->product_quantity; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="aliasProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_CATEGORIES'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['categories']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_BRAND'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['brand']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_MANUFACTURE'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['manufacturer']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_ARGENCY'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['argency']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_TAGS'); ?></label>
                                <span class="col-sm-9">
                                    <input class="form-control" type="text" name="product_tags" id="product_tags" size="50" value="<?php echo $this->item->product_tags; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_SHORT_DESCRIPTION'); ?></label>
                                <span class="col-sm-9">
                                    <textarea rows="3" cols="30" class="form-control" name="product_short_desc"><?php echo $this->item->product_short_desc; ?></textarea>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_HOT_DESCRIPTION'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['product_hot']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_NEW_DESCRIPTION'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['product_new']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['published']; ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- MAIN IMAGE -->
                    <div class="panel panel-default span6 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_IMAGE') ?></div>
                        <div class="panel-body">
                            <div class="form-group col-sm-12">
                                <!--<input type="file" class="input-large" accept="image/*" name="product_image" id="product_image" />-->	
                                <span class="col-sm-12">
                                    <span class="btn file-upload btn-file-input" id="btn_file">            
                                        <input type="file" class="file-input" accept="image/*" id="product_image" name="product_image">
                                        <span id="name_image_show"><?php echo JText::_('OPENSHOP_CHOOSE_IMAGE') ?></span>
                                    </span>

                                    <?php
                                    if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $this->item->product_image)) {
                                        $viewImage = JFile::stripExt($this->item->product_image) . '-100x100.' . JFile::getExt($this->item->product_image);
                                        if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                            ?>
                                            <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" />
                                            <?php
                                        } else {
                                            ?>
                                            <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $this->item->product_image; ?>" height="100" />
                                            <?php
                                        }
                                        ?>
                                        <label class="checkbox">
                                            <input type="checkbox" name="remove_image" value="1" />
                                            <?php echo JText::_('OPENSHOP_REMOVE_IMAGE'); ?>
                                        </label>
                                        <?php
                                    }
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>  <!-- /col 2 -->

                    <!--TAX AND PRICE-->
                    <div class="panel panel-default span6 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_TAX_AND_PRICE') ?></div>
                        <div class="panel-body">
                            <div class="form-group col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_PRICE_INPUT'); ?></label>
                                    <span class="col-sm-9">
                                        <input class="form-control" type="text" name="product_price_input" id="product_price_input" size="" maxlength="250" value="<?php echo floatval($this->item->product_price_input); ?>" />
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_PRICE'); ?></label>
                                    <span class="col-sm-9">
                                        <input class="form-control" type="text" name="product_price" id="product_price" size="" maxlength="250" value="<?php echo floatval($this->item->product_price); ?>" />
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_PRICE_R'); ?></label>
                                    <span class="col-sm-9">
                                        <input class="form-control" type="text" name="product_price_r" id="product_price_r" size="" maxlength="250" value="<?php echo floatval($this->item->product_price_r); ?>" />
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_BUYER_VIRTUAL'); ?></label>
                                    <span class="col-sm-9">
                                        <input class="form-control" type="text" name="buyer_virtual" id="buyer_virtual" size="" maxlength="250" value="<?php echo floatval($this->item->buyer_virtual); ?>" />
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_CALL_FOR_PRICE'); ?></label>
                                    <span class="col-sm-9">
                                        <?php echo $this->lists['product_call_for_price']; ?>
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_WIDTH'); ?></label>
                                    <span class="col-sm-3">
                                        <input class="form-control" type="text" name="product_width" id="product_width" size="" maxlength="250" value="<?php echo floatval($this->item->product_width); ?>" />
                                    </span>

                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_HEIGHT'); ?></label>
                                    <span class="col-sm-3">
                                        <input class="form-control" type="text" name="product_height" id="product_height" size="" maxlength="250" value="<?php echo floatval($this->item->product_height); ?>" />
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_LENGTH'); ?></label>
                                    <span class="col-sm-3">
                                        <input class="form-control" type="text" name="product_length" id="product_length" size="" maxlength="250" value="<?php echo floatval($this->item->product_length); ?>" />
                                    </span>

                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_LENGTH_UNIT'); ?></label>
                                    <span class="col-sm-3">
                                        <?php echo $this->lists['product_length_id']; ?>
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_WEIGHT'); ?></label>
                                    <span class="col-sm-3">
                                        <input class="form-control" type="text" name="product_weight" id="product_weight" size="" maxlength="250" value="<?php echo floatval($this->item->product_weight); ?>" />
                                    </span>

                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_WEIGHT_UNIT'); ?></label>
                                    <span class="col-sm-3">
                                        <?php echo $this->lists['product_weight_id']; ?>
                                    </span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_TAX'); ?></label>
                                    <span class="col-sm-9">
                                        <?php echo $this->lists['taxclasses']; ?>
                                    </span>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_MINIMUM_QUANTITY'); ?></label>
                                    <span class="col-sm-3">
                                        <input class="form-control" type="text" name="product_minimum_quantity" id="product_minimum_quantity" size="" maxlength="250" value="<?php echo $this->item->product_minimum_quantity; ?>" />
                                    </span>

                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_MAXIMUM_QUANTITY'); ?></label>
                                    <span class="col-sm-3">
                                        <input class="form-control" type="text" name="product_maximum_quantity" id="product_maximum_quantity" size="" maxlength="250" value="<?php echo $this->item->product_maximum_quantity; ?>" />
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div> <!-- /col 3 -->

                </div>      <!-- /row 1 -->

                <!-- SEO AND ORTHER -->
                <div class="col-sm-12" style="background: #EFEFEF; padding-top: 10px;">
                    <div class="panel panel-default span6 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_SEO') ?></div>
                        <div class="panel-body">
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PAGE_TITLE'); ?></label>
                                <span class="col-sm-9">
                                    <input class="form-control" type="text" name="product_page_title" id="product_page_title" size="" maxlength="250" value="<?php echo $this->item->product_page_title; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PAGE_HEADING'); ?></label>
                                <span class="col-sm-9">
                                    <input class="form-control" type="text" name="product_page_heading" id="product_page_heading" size="" maxlength="250" value="<?php echo $this->item->product_page_heading; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_META_KEYS'); ?></label>
                                <span class="col-sm-9">
                                    <textarea rows="4" cols="30" name="meta_key" class="form-control"><?php echo $this->item->meta_key; ?></textarea>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_META_DESC'); ?></label>
                                <span class="col-sm-9">
                                    <textarea rows="4" cols="30" name="meta_desc" class="form-control"><?php echo $this->item->meta_desc; ?></textarea>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default span6 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_ORTHER') ?></div>
                        <div class="panel-body">
                            <?php
                            if (isset($this->lists['product_downloads']) && $this->lists['product_downloads'] != '') {
                                ?>
                                <div class="form-group col-sm-12">
                                    <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_DOWNLOADS'); ?></label>
                                    <span class="col-sm-9">
                                        <?php echo $this->lists['product_downloads']; ?>
                                    </span>
                                </div>  
                            <?php } ?>

                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_REQUIRES_SHIPPING'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['product_shipping']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_SHIPPING_COST'); ?></label>
                                <span class="col-sm-9">
                                    <input class="form-control" type="text" name="product_shipping_cost" id="product_shipping_cost" size="" maxlength="250" value="<?php echo floatval($this->item->product_shipping_cost); ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_RELATED_PRODUCTS'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['related_products']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_AVAILABLE_DATE'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo JHtml::_('calendar', (($this->item->product_available_date == $this->nullDate) || !$this->item->product_available_date) ? '' : JHtml::_('date', $this->item->product_available_date, 'Y-m-d', null), 'product_available_date', 'product_available_date', '%Y-%m-%d', array('style' => 'width: 100px;')); ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_FEATURED'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['featured']; ?>
                                </span>
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_CUSTOMERGROUPS'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['product_customergroups']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_OUT_OF_STOCK_STATUS'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['product_stock_status_id']; ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-3 control-label"><?php echo JText::_('OPENSHOP_PRODUCT_QUOTE_MODE'); ?></label>
                                <span class="col-sm-9">
                                    <?php echo $this->lists['product_quote_mode']; ?>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-sm-12" style="background: #EFEFEF; padding-top: 10px;">
                    <!--IMAGES--> 
                    <div class="col-sm-6">
                        <div class="panel panel-default span12 box-show-huy" style="background-color: white;">
                            <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_IMAGES') ?></div>
                            <div class="panel-body">
                                <table class="adminlist table table-bordered" style="text-align: center;">
                                    <thead>
                                        <tr>
                                            <th class="title" width="40%"><?php echo JText::_('OPENSHOP_IMAGE'); ?></th>
                                            <th class="title" width="20%"><?php echo JText::_('OPENSHOP_ORDERING'); ?></th>
                                            <th class="title" width="20%"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></th>
                                            <th class="title" width="20%">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody id="product_images_area">
                                        <?php
                                        $options = array();
                                        $options[] = JHtml::_('Openselect.option', '1', Jtext::_('OPENSHOP_YES'));
                                        $options[] = JHtml::_('Openselect.option', '0', Jtext::_('OPENSHOP_NO'));
                                        for ($i = 0; $n = count($this->productImages), $i < $n; $i++) {
                                            $productImage = $this->productImages[$i];
                                            ?>
                                            <tr id="product_image_<?php echo $i; ?>" style="height: 100px;">
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <?php
                                                    if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $productImage->image)) {
                                                        $viewImage = JFile::stripExt($productImage->image) . '-100x100.' . JFile::getExt($productImage->image);
                                                        if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                                            ?>
                                                            <img class="img-polaroid" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" />
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <img class="img-polaroid" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $productImage->image; ?>" />
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <input type="hidden" class="inputbox" name="productimage_id[]" value="<?php echo $productImage->id; ?>" />
                                                </td>
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <input class="input-small" type="text" name="productimage_ordering[]" size="5" maxlength="10" value="<?php echo $productImage->ordering; ?>" />
                                                </td>
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <?php echo JHtml::_('select.genericlist', $options, 'productimage_published[]', ' class="inputbox"', 'value', 'text', $productImage->published); ?>
                                                </td>
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" onclick="removeProductImage(<?php echo $i; ?>);" />
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4">
                                                <input type="button" class="btn btn-small btn-primary" name="btnAdd" value="<?php echo JText::_('OPENSHOP_BTN_ADD'); ?>" onclick="addProductImage();" />
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--ATTRIBUTES-->
                    <div class="col-sm-6" style="background: #EFEFEF;">
                        <div class="panel panel-default span12 box-show-huy" style="background-color: white;">
                            <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_ATTRIBUTES') ?></div>
                            <div class="panel-body">
                                <table class="adminlist table table-bordered" style="text-align: center;">
                                    <thead>
                                        <tr>
                                            <th class="title" width="30%"><?php echo JText::_('OPENSHOP_ATTRIBUTE'); ?></th>
                                            <th class="title" width="45%"><?php echo JText::_('OPENSHOP_VALUE'); ?></th>
                                            <th class="title" width="15%"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></th>
                                            <th class="title" width="10%">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody id="product_attributes_area">
                                        <?php
                                        $options = array();
                                        $options[] = JHtml::_('select.option', '1', Jtext::_('OPENSHOP_YES'));
                                        $options[] = JHtml::_('select.option', '0', Jtext::_('OPENSHOP_NO'));
                                        for ($i = 0; $n = count($this->productAttributes), $i < $n; $i++) {
                                            $productAttribute = $this->productAttributes[$i];
                                            ?>
                                            <tr id="product_attribute_<?php echo $i; ?>">
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <?php echo $this->lists['attributes_' . $productAttribute->id]; ?>
                                                </td>
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <?php
                                                    if ($translatable) {
                                                        foreach ($this->languages as $language) {
                                                            $langCode = $language->lang_code;
                                                            ?>
                                                            <input class="input-large" type="text" name="attribute_value_<?php echo $langCode; ?>[]" maxlength="255" value="<?php echo isset($productAttribute->{'value_' . $langCode}) ? $productAttribute->{'value_' . $langCode} : ''; ?>" />
                                                            <img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" />
                                                            <input type="hidden" class="inputbox" name="productattributedetails_id_<?php echo $langCode; ?>[]" value="<?php echo isset($productAttribute->{'productattributedetails_id_' . $langCode}) ? $productAttribute->{'productattributedetails_id_' . $langCode} : ''; ?>" />
                                                            <br />
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <input class="input-medium" type="text" name="attribute_value[]" maxlength="255" value="<?php echo $productAttribute->value; ?>" />
                                                        <input type="hidden" class="inputbox" name="productattributedetails_id[]" value="<?php echo $productAttribute->productattributedetails_id; ?>" />
                                                        <?php
                                                    }
                                                    ?>
                                                    <input type="hidden" class="inputbox" name="productattribute_id[]" value="<?php echo $productAttribute->productattribute_id; ?>" />
                                                </td>
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <?php echo JHtml::_('select.genericlist', $options, 'attribute_published[]', ' class="inputbox"', 'value', 'text', $productAttribute->published); ?>
                                                </td>
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" onclick="removeProductAttribute(<?php echo $i; ?>);" />
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4">
                                                <input type="button" class="btn btn-small btn-primary" name="btnAdd" value="<?php echo JText::_('OPENSHOP_BTN_ADD'); ?>" onclick="addProductAttribute();" />
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!--OPTION-->
                <div class="col-sm-12" style="background: #EFEFEF; padding-top: 10px;">
                    <div class="panel panel-default span12 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_OPTION') ?></div>
                        <div class="panel-body">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs" id="nav-tabs">
                                    <li><?php echo $this->lists['options']; ?></li>
                                    <?php
                                    echo $this->lists['options_type'];
                                    for ($i = 0; $n = count($this->options), $i < $n; $i++) {
                                        echo $this->lists['option_values_' . $this->options[$i]->id];
                                    }
                                    echo $this->lists['price_sign'];
                                    echo $this->lists['weight_sign'];
                                    ?>
                                    <?php
                                    for ($i = 0; $n = count($this->productOptions), $i < $n; $i++) {
                                        $productOption = $this->productOptions[$i];
                                        ?>
                                        <li <?php echo ($i == 0) ? 'class="active"' : 0; ?> id="product_option_<?php echo $productOption->id; ?>">
                                            <a href="#option-<?php echo $productOption->id; ?>-page" data-toggle="tab"><?php echo $productOption->option_name; ?>
                                                <img src="<?php echo JURI::base(); ?>components/com_openshop/assets/images/remove.png" onclick="removeProductOption(<?php echo $productOption->id; ?>, '<?php echo $productOption->option_name; ?>');" />
                                            </a>
                                            <input type="hidden" name="productoption_id[]" value="<?php echo $productOption->id; ?>"/>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>	
                                <div class="tab-content" id="tab-content">
                                    <?php
                                    for ($i = 0; $n = count($this->productOptions), $i < $n; $i++) {
                                        $productOption = $this->productOptions[$i];
                                        ?>
                                        <div class="tab-pane<?php echo ($i == 0) ? ' active' : ''; ?>" id="option-<?php echo $productOption->id; ?>-page">
                                            <table style="width: 100%;" class="adminlist">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 150px;"><?php echo JText::_('OPENSHOP_REQUIRED'); ?></td>
                                                        <td>
                                                            <?php echo JHtml::_('select.genericlist', $options, 'required_' . $productOption->id, ' class="inputbox"', 'value', 'text', $productOption->required); ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <?php
                                            if ($productOption->option_type == 'Select' || $productOption->option_type == 'Radio' || $productOption->option_type == 'Checkbox') {
                                                ?>
                                                <table class="adminlist table table-bordered" style="text-align: center;">
                                                    <thead>
                                                        <tr>
                                                            <th class="title" width=""><?php echo JText::_('OPENSHOP_OPTION_VALUE'); ?></th>
                                                            <th class="title" width=""><?php echo JText::_('OPENSHOP_SKU'); ?></th>
                                                            <th class="title" width=""><?php echo JText::_('OPENSHOP_QUANTITY'); ?></th>
        <!--                                                            <th class="title" width=""><?php echo JText::_('OPENSHOP_PRICE'); ?></th>
                                                            <th class="title" width=""><?php echo JText::_('OPENSHOP_WEIGHT'); ?></th>-->
                                                            <th class="title" width=""><?php echo JText::_('Thông Số Size'); ?></th>
                                                            <th class="title" width="">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="product_option_<?php echo $productOption->id; ?>_values_area">
                                                        <?php
                                                        $options = array();
                                                        $options[] = JHtml::_('select.option', '1', Jtext::_('OPENSHOP_YES'));
                                                        $options[] = JHtml::_('select.option', '0', Jtext::_('OPENSHOP_NO'));
                                                        for ($j = 0; $m = count($this->productOptionValues[$i]), $j < $m; $j++) {
                                                            $productOptionValue = $this->productOptionValues[$i][$j];
                                                            ?>
                                                            <tr id="product_option_<?php echo $productOption->id; ?>_value_<?php echo $j; ?>">
                                                                <td style="text-align: center;">
                                                                    <?php echo $this->lists['product_option_value_' . $productOptionValue->id]; ?>
                                                                </td>
                                                                <td style="text-align: center;">
                                                                    <input class="input-small" type="text" name="optionvalue_<?php echo $productOption->id; ?>_sku[]" size="10" maxlength="255" value="<?php echo $productOptionValue->sku; ?>" />
                                                                </td>
                                                                <td style="text-align: center;">
                                                                    <input class="input-small" type="text" name="optionvalue_<?php echo $productOption->id; ?>_quantity[]" size="10" maxlength="255" value="<?php echo $productOptionValue->quantity; ?>" />
                                                                    <input type="hidden" name="productoptionvalue_id" value="<?php echo $productOptionValue->id; ?>" />
                                                                </td>
            <!--                                                                <td style="text-align: center;">
                                                                <?php echo $this->lists['price_sign_' . $productOptionValue->id]; ?>
                                                                    <input class="input-small" type="text" name="optionvalue_<?php echo $productOption->id; ?>_price[]" size="10" maxlength="255" value="<?php echo $productOptionValue->price; ?>" />
                                                                </td>
                                                                <td style="text-align: center;">
                                                                <?php echo $this->lists['weight_sign_' . $productOptionValue->id]; ?>
                                                                    <input class="input-small" type="text" name="optionvalue_<?php echo $productOption->id; ?>_weight[]" size="10" maxlength="255" value="<?php echo $productOptionValue->weight; ?>" />
                                                                </td>-->
                                                                <td style="text-align: center; vertical-align: middle;" nowrap="nowrap">
                                                                    <input type="text" name="optionvalue_<?php echo $productOption->id; ?>_desc_size[]" id="desc_size" style="width:500px" value="<?php echo $productOptionValue->desc_size; ?>" />
                                                                </td>
                                                                <td style="text-align: center;">
                                                                    <input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" onclick="removeProductOptionValue(<?php echo $productOption->id; ?>, <?php echo $j; ?>);" />
                                                                </td>
                                                            </tr>	
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="7">
                                                                <input type="button" class="btn btn-small btn-primary" name="btnAdd" value="<?php echo JText::_('OPENSHOP_BTN_ADD'); ?>" onclick="addProductOptionValue(<?php echo $productOption->id; ?>);" />
                                                                <?php echo $this->lists['option_values_' . $productOption->id]; ?>										
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <?php
                                            }
                                            if ($productOption->option_type == 'Text' || $productOption->option_type == 'Textarea') {
                                                $productOptionValue = $this->productOptionValues[$i][0];
                                                ?>
                                                <table style="width: 100%;" class="adminlist">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 150px;"><?php echo JText::_('OPENSHOP_PRODUCT_PRICE_PER_CHAR'); ?></td>
                                                            <td>
                                                                <input type="hidden" name="optionvalue_<?php echo $productOption->id; ?>_id[]" id="optionvalue_<?php echo $productOption->id; ?>_id" value="null"/>
                                                                <?php echo $this->lists['price_sign_t_' . $productOption->id]; ?>
                                                                <input class="input-small" type="text" name="optionvalue_<?php echo $productOption->id; ?>_price[]" size="10" maxlength="255" value="<?php echo $productOptionValue->price; ?>" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--DISCOUNT-->
                <div class="col-sm-12" style="background: #EFEFEF; padding-top: 10px;">
                    <div class="panel panel-default span12 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_DISCOUNT') ?></div>
                        <div class="panel-body">
                            <table class="adminlist table table-bordered" style="text-align: center;">
                                <thead>
                                    <tr>
                                        <th class="title" width="20%"><?php echo JText::_('OPENSHOP_CUSTOMER_GROUP'); ?></th>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_QUANTITY'); ?></th>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_PRIORITY'); ?></th>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_PRICE'); ?></th>
                                        <th class="title" width="15%"><?php echo JText::_('OPENSHOP_START_DATE'); ?></th>
                                        <th class="title" width="15%"><?php echo JText::_('OPENSHOP_END_DATE'); ?></th>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></th>
                                        <th class="title" width="10%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody id="product_discounts_area">
                                    <?php
                                    $options = array();
                                    $options[] = JHtml::_('select.option', '1', Jtext::_('OPENSHOP_YES'));
                                    $options[] = JHtml::_('select.option', '0', Jtext::_('OPENSHOP_NO'));
                                    for ($i = 0; $n = count($this->productDiscounts), $i < $n; $i++) {
                                        $productDiscount = $this->productDiscounts[$i];
                                        ?>
                                        <tr id="product_discount_<?php echo $i; ?>">
                                            <td style="text-align: center;">
                                                <?php echo $this->lists['discount_customer_group_' . $productDiscount->id]; ?>
                                                <input type="hidden" class="inputbox" name="productdiscount_id[]" value="<?php echo $productDiscount->id; ?>" />
                                            </td>
                                            <td style="text-align: center;">
                                                <input class="input-mini" type="text" name="discount_quantity[]" maxlength="10" value="<?php echo $productDiscount->quantity; ?>" />
                                            </td>
                                            <td style="text-align: center;">
                                                <input class="input-small" type="text" name="discount_priority[]" maxlength="10" value="<?php echo $productDiscount->priority; ?>" />
                                            </td>
                                            <td style="text-align: center;">
                                                <input class="input-small" type="text" name="discount_price[]" maxlength="10" value="<?php echo $productDiscount->price; ?>" />
                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo JHtml::_('calendar', (($productDiscount->date_start == $this->nullDate) || !$productDiscount->date_start) ? '' : JHtml::_('date', $productDiscount->date_start, 'Y-m-d', null), 'discount_date_start[]', 'discount_date_start_' . $i, '%Y-%m-%d', array('style' => 'width: 100px;')); ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo JHtml::_('calendar', (($productDiscount->date_end == $this->nullDate) || !$productDiscount->date_end) ? '' : JHtml::_('date', $productDiscount->date_end, 'Y-m-d', null), 'discount_date_end[]', 'discount_date_end_' . $i, '%Y-%m-%d', array('style' => 'width: 100px;')); ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo JHtml::_('select.genericlist', $options, 'discount_published[]', ' class="inputbox"', 'value', 'text', $productDiscount->published); ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" onclick="removeProductDiscount(<?php echo $i; ?>);" />
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="9">
                                            <input type="button" class="btn btn-small btn-primary" name="btnAdd" value="<?php echo JText::_('OPENSHOP_BTN_ADD'); ?>" onclick="addProductDiscount();" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <!--SPECIAL-->
                <div class="col-sm-12" style="background: #EFEFEF;">
                    <div class="panel panel-default span12 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_SPECIAL') ?></div>
                        <div class="panel-body">
                            <table class="adminlist table table-bordered" style="text-align: center;">
                                <thead>
                                    <tr>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_CUSTOMER_GROUP'); ?></th>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_PRIORITY'); ?></th>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_PRICE'); ?></th>
                                        <th class="title" width="15%"><?php echo JText::_('OPENSHOP_START_DATE'); ?></th>
                                        <th class="title" width="15%"><?php echo JText::_('OPENSHOP_END_DATE'); ?></th>
                                        <th class="title" width="10%"><?php echo JText::_('OPENSHOP_PUBLISHED'); ?></th>
                                        <th class="title" width="10%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody id="product_specials_area">
                                    <?php
                                    $options = array();
                                    $options[] = JHtml::_('select.option', '1', Jtext::_('OPENSHOP_YES'));
                                    $options[] = JHtml::_('select.option', '0', Jtext::_('OPENSHOP_NO'));
                                    for ($i = 0; $n = count($this->productSpecials), $i < $n; $i++) {
                                        $productSpecial = $this->productSpecials[$i];
                                        ?>
                                        <tr id="product_special_<?php echo $i; ?>">
                                            <td style="text-align: center;">
                                                <?php echo $this->lists['special_customer_group_' . $productSpecial->id]; ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <input class="input-small" type="text" name="special_priority[]" maxlength="10" value="<?php echo $productSpecial->priority; ?>" />
                                            </td>
                                            <td style="text-align: center;">
                                                <input class="input-small" type="text" name="special_price[]" maxlength="10" value="<?php echo $productSpecial->price; ?>" />
                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo JHtml::_('calendar', (($productSpecial->date_start == $this->nullDate) || !$productSpecial->date_start) ? '' : JHtml::_('date', $productSpecial->date_start, 'Y-m-d', null), 'special_date_start[]', 'special_date_start_' . $i, '%Y-%m-%d', array('style' => 'width: 100px;')); ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo JHtml::_('calendar', (($productSpecial->date_end == $this->nullDate) || !$productSpecial->date_end) ? '' : JHtml::_('date', $productSpecial->date_end, 'Y-m-d', null), 'special_date_end[]', 'special_date_end_' . $i, '%Y-%m-%d', array('style' => 'width: 100px;')); ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo JHtml::_('select.genericlist', $options, 'special_published[]', ' class="inputbox"', 'value', 'text', $productSpecial->published); ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <input type="button" class="btn btn-small btn-primary" name="btnRemove" value="<?php echo JText::_('OPENSHOP_BTN_REMOVE'); ?>" onclick="removeProductSpecial(<?php echo $i; ?>);" />
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <input type="button" class="btn btn-small btn-primary" name="btnAdd" value="<?php echo JText::_('OPENSHOP_BTN_ADD'); ?>" onclick="addProductSpecial();" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!--                
                -
                -   THE END: MAIN INFORMATION PRODUCT
                -
                -->
            </div> <!-- /tab: main info product -->

            <!--DETAIL PRODUCT-->
            <div role="tabpanel" class="tab-pane" id="detail_product">
                <!--row-->
                <div class="col-sm-12" style="background: #EFEFEF; padding-top: 10px;">
                    <div class="panel panel-default span12 box-show-huy" style="background-color: white;">
                        <div class="panel-heading"><?php echo JText::_('OPENSHOP_PRODUCT_DETAIL') ?></div>
                        <div class="panel-body">
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></label>
                                <span class="col-sm-11">
                                    <?php echo $editor->display('product_desc', $this->item->product_desc, '100%', '250', '75', '10'); ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB1_TITLE'); ?></label>
                                <span class="col-sm-11">
                                    <input class="form-control" type="text" name="tab1_title" id="tab1_title" size="" maxlength="250" value="<?php echo $this->item->tab1_title; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB1_CONTENT'); ?></label>
                                <span class="col-sm-11">
                                    <?php echo $editor->display('tab1_content', $this->item->tab1_content, '100%', '250', '75', '10'); ?>
                                </span>
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB2_TITLE'); ?></label>
                                <span class="col-sm-11">
                                    <input class="form-control" type="text" name="tab2_title" id="tab2_title" size="" maxlength="250" value="<?php echo $this->item->tab2_title; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB2_CONTENT'); ?></label>
                                <span class="col-sm-11">
                                    <?php echo $editor->display('tab2_content', $this->item->tab2_content, '100%', '250', '75', '10'); ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB3_TITLE'); ?></label>
                                <span class="col-sm-11">
                                    <input class="form-control" type="text" name="tab3_title" id="tab3_title" size="" maxlength="250" value="<?php echo $this->item->tab3_title; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB3_CONTENT'); ?></label>
                                <span class="col-sm-11">
                                    <?php echo $editor->display('tab3_content', $this->item->tab3_content, '100%', '250', '75', '10'); ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB4_TITLE'); ?></label>
                                <span class="col-sm-11">
                                    <input class="form-control" type="text" name="tab4_title" id="tab4_title" size="" maxlength="250" value="<?php echo $this->item->tab4_title; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB4_CONTENT'); ?></label>
                                <span class="col-sm-11">
                                    <?php echo $editor->display('tab4_content', $this->item->tab4_content, '100%', '250', '75', '10'); ?>
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB5_TITLE'); ?></label>
                                <span class="col-sm-11">
                                    <input class="form-control" type="text" name="tab5_title" id="tab5_title" size="" maxlength="250" value="<?php echo $this->item->tab5_title; ?>" />
                                </span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="nameProduct" class="col-sm-1 control-label"><?php echo JText::_('OPENSHOP_TAB5_CONTENT'); ?></label>
                                <span class="col-sm-11">
                                    <?php echo $editor->display('tab5_content', $this->item->tab5_content, '100%', '250', '75', '10'); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>      <!-- /row 1 -->

            </div> <!-- /tab: detail product -->
        </div>
    </div>

    <?php echo JHtml::_('form.token'); ?>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
    <?php
    if ($translatable) {
        foreach ($this->languages as $language) {
            $langCode = $language->lang_code;
            ?>
            <input type="hidden" name="details_id_<?php echo $langCode; ?>" value="<?php echo isset($this->item->{'details_id_' . $langCode}) ? $this->item->{'details_id_' . $langCode} : ''; ?>" />
            <?php
        }
    } elseif ($this->translatable) {
        ?>
        <input type="hidden" name="details_id" value="<?php echo isset($this->item->{'details_id'}) ? $this->item->{'details_id'} : ''; ?>" />
        <?php
    }
    ?>
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="" id="checkFormSubmit" value="1" />
</form>
