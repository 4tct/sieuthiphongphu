<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_REASON_DELETE_ORDER'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_REASON_DELETE_ORDER_DESC'); ?></span>
            </td>
            <td>
                <?php echo $this->lists['reason_delete_order']; ?>
            </td>
	</tr>
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_REASON_DELETE_TRANSPORTER'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_REASON_DELETE_TRANSPORTER_DESC'); ?></span>
            </td>
            <td>
                <?php echo $this->lists['reason_delete_transporter']; ?>
            </td>
	</tr>
    </table>
</fieldset>