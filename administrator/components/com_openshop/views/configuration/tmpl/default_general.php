<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$editor = JEditor::getInstance();

?>
<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span><?php echo  JText::_('OPENSHOP_CONFIG_STORE_NAME'); ?>:
            </td>
            <td>
                <input class="text_area" type="text" name="store_name" id="store_name" size="15" maxlength="250" value="<?php echo $this->config->store_name; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span><?php echo  JText::_('OPENSHOP_CONFIG_STORE_OWNER'); ?>:
            </td>
            <td>
                <input class="text_area" type="text" name="store_owner" id="store_owner" size="15" maxlength="250" value="<?php echo $this->config->store_owner; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span><?php echo  JText::_('OPENSHOP_CONFIG_ADDRESS'); ?>:
            </td>
            <td>
                <textarea rows="5" cols="40" name="address"><?php echo $this->config->address; ?></textarea>					
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span>Mã số thuế:
            </td>
            <td>
                <input class="text_area" type="text" name="tax_code" id="tax_code" size="15" maxlength="250" value="<?php echo $this->config->tax_code; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span>Nơi cấp:
            </td>
            <td>
                <input class="text_area" type="text" name="tax_code_address" id="tax_code_address" size="15" maxlength="250" value="<?php echo $this->config->tax_code_address; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span>Ngày cấp:
            </td>
            <td>
                <input class="text_area" type="text" name="tax_code_date" id="tax_code_date" size="15" maxlength="250" value="<?php echo $this->config->tax_code_date; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span><?php echo  JText::_('OPENSHOP_CONFIG_EMAIL'); ?>:
            </td>
            <td>
                <input class="text_area" type="text" name="email" id="email" size="15" maxlength="100" value="<?php echo $this->config->email; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <span class="required">*</span><?php echo  JText::_('OPENSHOP_CONFIG_TELEPHONE'); ?>:
            </td>
            <td>
                <input class="text_area" type="text" name="telephone" id="telephone" size="10" maxlength="15" value="<?php echo $this->config->telephone; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo  JText::_('OPENSHOP_CONFIG_FAX'); ?>:
            </td>
            <td>
                <input class="text_area" type="text" name="fax" id="fax" size="10" maxlength="15" value="<?php echo $this->config->fax; ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo  JText::_('OPENSHOP_CONFIG_INTRODUCTION_DISPLAY_ON'); ?>:
            </td>
            <td>
                <?php echo $this->lists['introduction_display_on']; ?>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo 'Key'; ?>:
            </td>
            <td>
                <?php
                if(isset($this->config->keyDecode) && !empty($this->config->keyDecode)){
                    $key = $this->config->keyDecode;
                }
                else
                {
                    $key = 'huyhuynh';
                }
                ?>
                <input class="text_area" type="text" name="keyDecode" id="keyDecode" size="10" maxlength="15" value="<?php echo $key ?>" />
            </td>
        </tr>
    </table>
</fieldset>
