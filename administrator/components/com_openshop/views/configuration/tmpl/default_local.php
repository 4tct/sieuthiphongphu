<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die('Restricted access');
?>
<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                    <?php echo  JText::_('OPENSHOP_CONFIG_COUNTRY'); ?>:
            </td>
            <td>
                    <?php echo $this->lists['country_id']; ?>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                    <?php echo  JText::_('OPENSHOP_CONFIG_REGION_STATE'); ?>:
            </td>
            <td>
                    <?php echo $this->lists['zone_id']; ?>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                    <?php echo  JText::_('OPENSHOP_CONFIG_DEFAULT_CURRENCY'); ?>:
            </td>
            <td>
                    <?php echo $this->lists['default_currency_code']; ?>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                    <?php echo  JText::_('OPENSHOP_CONFIG_AUTO_UPDATE_CURRENCY'); ?>:
            </td>
            <td>
                    <?php echo $this->lists['auto_update_currency']; ?>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                    <?php echo  JText::_('OPENSHOP_CONFIG_LENGTH'); ?>:
            </td>
            <td>
                    <?php echo $this->lists['length_id']; ?>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                    <?php echo  JText::_('OPENSHOP_CONFIG_WEIGHT'); ?>:
            </td>
            <td>
                    <?php echo $this->lists['weight_id']; ?>
            </td>
        </tr>
    </table>
</fieldset>
