<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'configs.js');
?>

<style>
    .color-wrapper{
        position:relative;
        width:250px;
    }
    .color-wrapper p{ margin-bottom:5px; }

    input.call-picker{
        border:1px solid #aaa;
        color:#666;
        text-transform:uppercase;
        float:left;
        outline:none;
        padding:10px;
        width:85px;
    }

    .color-picker{
        background:#f3f3f3;
        padding:5px;
        border:5px solid #fff;
        box-shadow:0px 0px 3px 1px #ddd;
        position:absolute;
        top:61px;
        left:2px;
    }
    .color-holder{
        background:#fff;
        cursor:pointer;
        border:1px solid #aaa;
        width:150px;
        height:36px;
        float:left;
        margin-left:5px;
    }

    .color-picker .color-item{
        cursor:pointer;
        width:20px;
        height:20px;
        list-style-type:none;
        float:left;
        margin:2px;
        border:1px solid #ddd;
    }

    .color-picker .color-item:hover{
        border:1px solid #666;
        opacity:0.8;
        -moz-opacity:0.8;
        filter:alpha(opacity=8);
    }
    .colorS{
        background-color: <?php echo $this->config->t_background_color ?>;
    }
</style>


<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_TEMPLATE_BACKGROUND_COLOR'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_TEMPLATE_BACKGROUND_COLOR_DESC'); ?></span>
            </td>
            <td>
                <div class="color-wrapper">
                    <input type="hidden" name="t_background_color" placeholder="#ffffff" id="pickcolor" class="call-picker" value="<?php echo $this->config->t_background_color ?>">
                    <div class="color-holder call-picker colorS"></div>
                    <div class="color-picker" id="color-picker" style="display:none" ></div>
                </div>
            </td>
        </tr>

    </table>
</fieldset>

<script>
    var colorList = ['000000', '993300', '333300', '003300', '003366', '000066', '333399', '333333',
        '660000', 'FF6633', '666633', '336633', '336666', '0066FF', '666699', '666666', 'CC3333', 'FF9933', '99CC33', '669966', '66CCCC', '3366FF', '663366', '999999', 'CC66FF', 'FFCC33', 'FFFF66', '6CC550', '99CCCC', '66CCFF', '993366', 'CCCCCC', 'FF99CC', 'FFCC99', 'FFFF99', 'CCffCC', 'CCFFff', '99CCFF', 'CC99FF', 'FFFFFF'];

    var picker = $('#color-picker');

    for (var i = 0; i < colorList.length; i++)
    {
        picker.append('<li class="color-item" data-hex="' +
                '#' + colorList[i] + '" style="background-color:' +
                '#' + colorList[i] + ';"></li>');
    }

    // khi click vao bang mau, no se tu mat

    $('body').click(function () {
        picker.fadeOut();
    });

    // click de xuat hien bang mau
    $('.call-picker').click(function (event) {
        event.stopPropagation();
        picker.fadeIn();
        picker.children('li').hover(function () {
            var codeHex = $(this).data('hex');
            $('.color-holder').css('background-color', codeHex);
            $('#pickcolor').val(codeHex);
        });
    });
</script>