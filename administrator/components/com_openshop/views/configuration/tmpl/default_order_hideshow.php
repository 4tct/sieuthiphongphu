<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
//JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'configs.js');
?>

<style>
    .del_img_inOrder{
        color: red;
        margin: 0 0 0 10px;
        cursor: pointer;
    }
</style>


<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_HIDESHOW_APPOINTMENT_TYPE'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_HIDESHOW_APPOINTMENT_TYPE_DESC'); ?></span>
            </td>
            <td>
                <?php echo $this->lists['hideshow_appointment']; ?>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_IMAGE_USING_IN_ORDER'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_IMAGE_USING_IN_ORDER_DESC'); ?></span>
            </td>
            <td>
                <div class="form-group col-md-12">
                    <div class="col-md-6">
                        <label for="">
                            <b><?php echo JText::_('OPENSHOP_CONF_WAITING_INPUT') ?></b><br/>
                            <i style="font-size: 10px;"><?php echo JText::_('OPENSHOP_CONF_WAITING_INPUT_DESC') ?></i>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $imgName = isset($this->config->waiting_input_img) ? $this->config->waiting_input_img : '';
                        if (!empty($imgName) && JFile::exists(OPENSHOP_PATH_IMG_OTHER . $imgName)) {
                            ?>
                            <span class="show_img_inOrder_waiting_input_img">
                                <img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . $imgName ?>" alt="<?php echo JText::_('OPENSHOP_CONF_WAITING_INPUT') ?>" width="40px"/>
                                <span class="icon-delete del_img_inOrder" onclick="delete_img_inOrder('waiting_input_img');" title="<?php echo JText::_('OPENSHOP_DELETE_IMAGE_OPTION_TITLE'); ?>"></span>
                                <input type="hidden" name="waiting_input_img" value="<?php echo $imgName ?>"/>
                            </span>
                            <?php
                        } else {
                            ?>
                            <input type="file" name="waiting_input" id="waiting_input"/>
                            <input type="hidden" name="waiting_input_img"/>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="col-md-6">
                        <label for="">
                            <b><?php echo JText::_('OPENSHOP_CONF_WAITING_ENOUCH') ?></b><br/>
                            <i style="font-size: 10px;"><?php echo JText::_('OPENSHOP_CONF_WAITING_ENOUCH_DESC') ?></i>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $imgName = isset($this->config->waiting_enouch_img) ? $this->config->waiting_enouch_img : '';
                        if (!empty($imgName) && JFile::exists(OPENSHOP_PATH_IMG_OTHER . $imgName)) {
                            ?>
                            <span class="show_img_inOrder_waiting_enouch_img">
                                <img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . $imgName ?>" alt="<?php echo JText::_('OPENSHOP_CONF_WAITING_ENOUCH') ?>" width="40px"/>
                                <span class="icon-delete del_img_inOrder" onclick="delete_img_inOrder('waiting_enouch_img');" title="<?php echo JText::_('OPENSHOP_DELETE_IMAGE_OPTION_TITLE'); ?>"></span>
                                <input type="hidden" name="waiting_enouch_img" value="<?php echo $imgName ?>" />
                            </span>
                            <?php
                        } else {
                            ?>
                            <input type="file" name="waiting_enouch" />
                            <input type="hidden" name="waiting_enouch_img" />
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="col-md-6">
                        <label for="">
                            <b><?php echo JText::_('OPENSHOP_CONF_WAITING_APPOINTMENT') ?></b><br/>
                            <i style="font-size: 10px;"><?php echo JText::_('OPENSHOP_CONF_WAITING_APPOINTMENT_DESC') ?></i>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $imgName = isset($this->config->waiting_appointment_img) ? $this->config->waiting_appointment_img : '';
                        if (!empty($imgName) && JFile::exists(OPENSHOP_PATH_IMG_OTHER . $imgName)) {
                            ?>
                            <span class="show_img_inOrder_waiting_appointment_img">
                                <img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . $imgName ?>" alt="<?php echo JText::_('OPENSHOP_CONF_WAITING_APPOINTMENT') ?>" width="40px"/>
                                <span class="icon-delete del_img_inOrder" onclick="delete_img_inOrder('waiting_appointment_img');" title="<?php echo JText::_('OPENSHOP_DELETE_IMAGE_OPTION_TITLE'); ?>"></span>
                                <input type="hidden" name="waiting_appointment_img" value="<?php echo $imgName ?>" />
                            </span>
                            <?php
                        } else {
                            ?>
                            <input type="file" name="waiting_appointment" />
                            <input type="hidden" name="waiting_appointment_img"/>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <div class="col-md-6">
                        <label for="">
                            <b><?php echo JText::_('OPENSHOP_CONF_ORDER_COMPLETE') ?></b><br/>
                            <i style="font-size: 10px;"><?php echo JText::_('OPENSHOP_CONF_ORDER_COMPLETE_DESC') ?></i>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $imgName = isset($this->config->order_complete_img) ? $this->config->order_complete_img : '';
                        if (!empty($imgName) && JFile::exists(OPENSHOP_PATH_IMG_OTHER . $imgName)) {
                            ?>
                            <span class="show_img_inOrder_order_complete_img">
                                <img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . $imgName ?>" alt="<?php echo JText::_('OPENSHOP_CONF_ORDER_COMPLETE') ?>" width="40px"/>
                                <span class="icon-delete del_img_inOrder" onclick="delete_img_inOrder('order_complete_img');" title="<?php echo JText::_('OPENSHOP_DELETE_IMAGE_OPTION_TITLE'); ?>"></span>
                                <input type="hidden" name="order_complete_img" value="<?php echo $imgName ?>"/>
                            </span>
                            <?php
                        } else {
                            ?>
                            <input type="file" name="order_complete" />
                            <input type="hidden" name="order_complete_img"/>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</fieldset>