<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_SELECT_SIZE_COLOR'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_SELECT_SIZE_COLOR_DESC'); ?></span>
            </td>
            <td>
                <?php echo $this->lists['sizecolor']; ?>
            </td>
	</tr>
	
    </table>
</fieldset>