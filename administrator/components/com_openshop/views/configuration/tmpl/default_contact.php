<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'configs.js');
?>

<style>
    .del_img_inOrder{
        color: red;
        margin: 0 0 0 10px;
        cursor: pointer;
    }
</style>


<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_CONTACT_TITLE'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_OPENSHOP_CONTACT_TITLE_DESC'); ?></span>
            </td>
            <td>
                <textarea name="contact_title" class="form-control"><?php echo $this->config->contact_title ?></textarea>
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_CONTACT_PHONE'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_CONTACT_PHONE_DESC'); ?></span>
            </td>
            <td>
                <div class="numberphone">
                    <?php
                    $number_phones = explode(',', $this->config->contact_phone);
                    $value_phone = '';
                    $t = 1;
                    foreach ($number_phones as $k_p => $v_p) {
                        $i = ++$k_p;
                        ?>
                        <div class = "input-group phone_<?php echo $i ?>" style = "width: 300px;">
                            <span class = "input-group-addon"><i class = "icon-phone-2"></i></span>
                            <input type = "text" class = "form-control input_phone_<?php echo $i ?>" placeholder = "Số điện thoại" aria-describedby = "phone" onchange = "getNumberPhone()" value="<?php echo $v_p ?>"/>
                            <span class = "input-group-addon deleteNumP_<?php echo $i ?>" style = "background-color:white;color: red;" onclick = "deleteRowPhone(<?php echo $i ?>)"><i class = "icon-delete"></i></span>
                        </div>
                        <?php
                        if($t == 1){
                            $value_phone .= $v_p;
                            $t = 0;
                        }
                        else
                        {
                            $value_phone .= ',' . $v_p;
                        }
                    }
                    ?>
                </div>
                <div class="btn-add-numberphone"><span class="add-numberphone" onclick="addNumberPhone()"><?php echo JText::_('OPENSHOP_ADD_NUMBER_PHONE') ?></span></div>
                <input type="hidden" name="contact_phone" id="contact_phone" value="<?php echo $value_phone ?>" />
                <input type="hidden" id="max_phone" value="<?php echo count($number_phones); ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_CONTACT_EMAIL'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_CONTACT_EMAIL_DESC'); ?></span>
            </td>
            <td>
                <div class="email">
                    <?php
                    $emails = explode(',', $this->config->contact_email);
                    $value_email = '';
                    $t = 1;
                    foreach ($emails as $k_e => $v_e) {
                        $i = ++$k_e;
                        ?>
                        <div class = "input-group email_<?php echo $i ?>" style = "width: 300px;">
                            <span class = "input-group-addon"><i class = "icon-mail"></i></span>
                            <input type = "text" class = "form-control input_email_<?php echo $i ?>" placeholder = "Email" aria-describedby = "email" onchange = "getEmail()" value="<?php echo $v_e ?>"/>
                            <span class = "input-group-addon deleteEmail_<?php echo $i ?>" style = "background-color:white;color: red;" onclick = "deleteRowEmail(<?php echo $i ?>)"><i class = "icon-delete"></i></span>
                        </div>
                        <?php
                        if($t == 1){
                            $value_email .= $v_e;
                            $t = 0;
                        }
                        else
                        {
                            $value_email .= ',' . $v_e;
                        }
                    }
                    ?>
                </div>
                <div class="btn-add-numberphone"><span class="add-numberphone" onclick="addEmail()"><?php echo JText::_('OPENSHOP_ADD_EMAIL') ?></span></div>
                <input type="hidden" name="contact_email" id="contact_email" value="<?php echo $value_email ?>" />
                <input type="hidden" id="max_email" value="<?php echo count($emails); ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_CONTACT_ADDRESS'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_CONTACT_ADDRESS_DESC'); ?></span>
            </td>
            <td>
                <div class="address">
                    <?php
                    $address = explode(';', $this->config->contact_address);
                    $value_address = '';
                    $t = 1;
                    foreach ($address as $k_a => $v_a) {
                        $i = ++$k_a;
                        $addr = explode('@@', $v_a);
                        ?>
                        <div class = "input-group address_<?php echo $i ?>" style = "width: 300px;">
                            <span class = "input-group-addon"><i class = "icon-pin"></i></span>
                            <input type = "text" class = "form-control input_address_<?php echo $i ?>" placeholder = " <?php echo JText::_('OPENSHOP_CONTACT_ADDRESS'); ?>" aria-describedby = "address" onchange = "getAddress()" value="<?php echo $addr[0] ?>"/>
                            <input type="text" class="form-control input_map_<?php echo $i ?>" placeholder="Bản đồ" aria-describedby="map" onchange="getAddress()" value="<?php echo $addr[1] ?>">
                            <span class = "input-group-addon deleteAddress_<?php echo $i ?>" style = "background-color:white;color: red;" onclick = "deleteRowAddress(<?php echo $i ?>)"><i class = "icon-delete"></i></span>
                        </div>
                        <?php
                        if($t == 1){
                            $value_address .= $addr[0];
                            $value_address .= '@@'.$addr[1];
                            $t = 0;
                        }
                        else
                        {
                            $value_address .= ';' . $addr[0];
                            $value_address .= '@@'.$addr[1];
                        }
                    }
                    ?>
                </div>
                <div class="btn-add-numberphone"><span class="add-numberphone" onclick="addAddress()"><?php echo JText::_('OPENSHOP_ADD_ADDRESS') ?></span></div>
                <input type="hidden" name="contact_address" id="contact_address" value="<?php echo $value_address ?>" />
                <input type="hidden" id="max_address" value="<?php echo count($address); ?>" />
            </td>
        </tr>
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_TIME_WORK'); ?>:<br />
                <span class="help"><?php echo JText::_('OPENSHOP_TIME_WORK_DESC'); ?></span>
            </td>
            <td>
                <div class="timework">
                    <?php
                    $timework = explode(',', $this->config->start_end_work);
                    $value_timework = '';
                    $t = 1;
                    foreach ($timework as $k_t => $v_t) {
                        $i = ++$k_t;
                        ?>
                        <div class = "input-group timework_<?php echo $i ?>" style = "width: 300px;">
                            <span class = "input-group-addon"><i class = "icon-clock"></i></span>
                            <input type = "text" class = "form-control input_timework_<?php echo $i ?>" placeholder = " <?php echo JText::_('OPENSHOP_TIME_WORK'); ?>" aria-describedby = "timework" onchange = "getTimework()" value="<?php echo $v_t ?>"/>
                            <span class = "input-group-addon deleteTimework_<?php echo $i ?>" style = "background-color:white;color: red;" onclick = "deleteRowTimework(<?php echo $i ?>)"><i class = "icon-delete"></i></span>
                        </div>
                        <?php
                        if($t == 1){
                            $value_timework .= $v_t;
                            $t = 0;
                        }
                        else
                        {
                            $value_timework .= ',' . $v_t;
                        }
                    }
                    ?>
                </div>
                <div class="btn-add-numberphone"><span class="add-numberphone" onclick="addTimework()"><?php echo JText::_('OPENSHOP_ADD_TIME_WORK') ?></span></div>
                <input type="hidden" name="start_end_work" id="start_end_work" value="<?php echo $value_timework ?>" />
                <input type="hidden" id="max_timework" value="<?php echo count($timework); ?>" />
            </td>
        </tr>
    </table>
</fieldset>