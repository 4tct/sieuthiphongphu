<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<fieldset class="adminfs">
    <legend>Items</legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%"><?php echo JText::_('OPENSHOP_CONFIG_DEFAULT_SORTING'); ?></td>
            <td colspan="3">
                    <?php echo $this->lists['default_sorting']; ?>
            </td>
	</tr>
	<tr>
            <?php
            for ($i = 0; $n = count($this->sortValues), $i < $n; $i++)
            {
                    ?>
                    <td width="25%">
                            <label class="checkbox">
                                    <input <?php echo (in_array($this->sortValues[$i], $this->sortOptions) ? 'checked' : ''); ?> type="checkbox" name="sort_options[]" value="<?php echo $this->sortValues[$i]; ?>"><?php echo $this->sortTexts[$i]; ?>
                            </label>
                    </td>	
                    <?php
                    if (($i + 1) % 4 == 0)
                    {
                            ?>
                            </tr>
                            <tr>
                            <?php
                    }
            }
            ?>
	</tr>
    </table>
</fieldset>