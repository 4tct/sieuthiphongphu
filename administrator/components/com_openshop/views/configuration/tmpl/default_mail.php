<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<fieldset class="adminfs">
    <legend></legend>
    <table class="admintable table" style="width:100%"  cellspacing="1">
        <tr>
            <td class="key" width="30%">
                <?php echo JText::_('OPENSHOP_CONFIG_ORDER_ALERT_MAIL'); ?>:<br>
                <span class="help"><?php echo JText::_('OPENSHOP_CONFIG_ORDER_ALERT_MAIL_HELP'); ?></span>
            </td>
            <td>
                <?php echo $this->lists['order_alert_mail']; ?>
            </td>
	</tr>
	<tr>
            <td class="key">
                <?php echo JText::_('OPENSHOP_CONFIG_ALERT_MAILS'); ?>:<br>
                <span class="help"><?php echo JText::_('OPENSHOP_CONFIG_ALERT_MAILS_HELP'); ?></span>
            </td>
            <td>
                <input class="text_area" type="text" name="alert_emails" id="alert_emails" size="100" maxlength="250" value="<?php echo $this->config->alert_emails; ?>" />
            </td>
	</tr>
    </table>
</fieldset>