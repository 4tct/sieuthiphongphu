<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JToolbarHelper::title(JText::_('OPENSHOP_CONFIGURATION'),'generic.png');
JToolbarHelper::apply('configuration.save');
JToolbarHelper::cancel('configuration.cancel');
$canDo  = OpenShopHelper::getActions();
if($canDo->get('core.admin')){
    JToolbarHelper::preferences('com_openshop');
}
$editor = JEditor::getInstance();
JFactory::getDocument()->addStyleSheet(JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'css'.DS.'config.css');
JFactory::getDocument()->addScript(JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'js'.DS.'configs.js');
?>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="row-fluid">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#general-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_GENERAL'); ?></a></li>
            <li><a href="#local-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_LOCAL'); ?></a></li>
            <li><a href="#option-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_OPTION'); ?></a></li>
            <li><a href="#image-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_IMAGE'); ?></a></li>
            <li><a href="#layout-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_LAYOUT'); ?></a></li>
            <li><a href="#invoice-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_INVOICE'); ?></a></li>
            <li><a href="#order-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_SORTING'); ?></a></li>
            <li><a href="#social-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_SOCIAL'); ?></a></li>
            <li><a href="#mail-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONFIG_MAIL'); ?></a></li>
            <li><a href="#order-hideshow-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_ORDER'); ?></a></li>
            <li><a href="#product-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_IO'); ?></a></li>
            <li><a href="#reason-delete-page" data-toggle="tab"><?php echo JText::_('OPENSHOP_REASON_DELETE'); ?></a></li>
            <li><a href="#contact" data-toggle="tab"><?php echo JText::_('OPENSHOP_CONTACT'); ?></a></li>
            <li><a href="#template" data-toggle="tab"><?php echo JText::_('OPENSHOP_TEMPLATE'); ?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="general-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('general');?>
                </div>
            </div>
            
            <div class="tab-pane" id="local-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('local');?>
                </div>
            </div> 
            
            <div class="tab-pane" id="option-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('option');?>
                </div>
            </div>
            
            <div class="tab-pane" id="image-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('image'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="invoice-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('invoice'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="layout-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('layout'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="order-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('sorting'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="social-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('social'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="mail-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('mail'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="order-hideshow-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('order_hideshow'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="product-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('product'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="reason-delete-page">
                <div class="span12">
                    <?php echo $this->loadTemplate('reason_delete'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="contact">
                <div class="span12">
                    <?php echo $this->loadTemplate('contact'); ?>
                </div>
            </div>
            
            <div class="tab-pane" id="template">
                <div class="span12">
                    <?php echo $this->loadTemplate('template'); ?>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden"    name="option"   value="com_openshop"/>
    <input type="hidden"    name="task"     value=""/>
    <?php echo Jhtml::_('form.token');?>
    <div class="clear"></div>
</form> 