<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewQuote extends OpenShopViewForm
{

	function _buildListArray(&$lists, $item)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		//Quote products list
		$query->select('*')
			->from('#__openshop_quoteproducts')
			->where('quote_id = ' . intval($item->id));
		$db->setQuery($query);
		$quoteProducts = $db->loadObjectList();
		for ($i = 0; $n = count($quoteProducts), $i < $n; $i++)
		{
			$query->clear();
			$query->select('*')
				->from('#__openshop_quoteoptions')
				->where('quote_product_id = ' . intval($quoteProducts[$i]->id));
			$db->setQuery($query);
			$quoteProducts[$i]->options = $db->loadObjectList();
		}
		$lists['quote_products'] = $quoteProducts;
		$currency = new OpenShopCurrency();
		$this->currency = $currency;
	}

	/**
	 * Override Build Toolbar function, only need Save, Save & Close and Close
	 */
	function _buildToolbar()
	{
		$viewName = $this->getName();
		JToolBarHelper::title(JText::_('OPENSHOP_QUOTE_DETAILS'));
		JToolBarHelper::cancel($viewName . '.cancel', 'JTOOLBAR_CLOSE');
	}
}