<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewManufacturer extends OpenShopViewForm {
    public function _buildListArray(&$lists, $item) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id as value, name as text')
                ->from($db->quoteName('#__users'));
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_SELECT'));
        if(count($rows)){
            $options = array_merge($options,$rows);
        }
        
        $lists['user_manager'] = JHtml::_('select.genericlist',$options, 'user_manager', ' class="form-control" style="width:270px" ','value','text', $item->user_manager);
        return $lists;
        
    }
}
