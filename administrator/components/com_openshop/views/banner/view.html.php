<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewBanner extends OpenShopViewForm {

    public static function getLinkFeatureMenu() {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('*')
                    ->from($db->quoteName('#__menu'))
                    ->where('menutype = "featuremenu"');
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }
    
    public static function getProductList($arrPro){
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
//        try {
            $query->select('a.id,b.product_name,a.product_image,a.product_price,a.product_price_r,a.published')
                    ->from($db->quoteName('#__openshop_products','a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . ' ON a.id = b.product_id')
                    ->where('a.id IN ('. $arrPro .')')
                    ->where('a.delete_status = 0');
            return $db->setQuery($query)->loadObjectList();
//        } catch (Exception $ex) {
//            return '';
//        }
    }

}
