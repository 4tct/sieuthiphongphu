<?php
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
JFactory::getDocument()->addScript(JUri::base() . 'components/com_openshop/assets/js/banner.js');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'banner.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {

            if (form.title.value == '') {
                toastr['warning']("Nhập tiêu đề");
                form.title.focus();
                return;
            }

            if (form.time_start.value == '') {
                toastr['warning']("Chưa nhập thời gian bắt đầu");
                form.time_start.focus();
                return;
            }

            if (form.time_finish.value == '') {
                toastr['warning']("Chọn thời gian kết thúc");
                form.time_finish.focus();
                return;
            }

            //Chuyển sang ngày
            function parseDate(str) {
                var mdy = str.split('-');
                return new Date(mdy[2], mdy[1], mdy[0]);
            }

            var start = parseDate(form.time_start.value).getTime();
            var finish = parseDate(form.time_finish.value).getTime();

            if (start > finish) {
                toastr['warning']('Vui lòng chọn ngày bắt đầu NHỎ hơn ngày kết thúc');

                return;
            }

            //Validate the entered data before submitting
            Joomla.submitform(pressbutton, form);
        }
    }
</script>
<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="form-horizontal" style="padding-top: 10px;">

        <div class="col-md-12">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2">
                        Tiêu đề <span style="color:red;">*</span>
                    </label>
                    <div class="col-md-10">
                        <input class="form-control" name="title" id="title" value="<?php echo $this->item->title ?>" placeholder="Nhập tiêu đề quảng cáo" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Alias
                    </label>
                    <div class="col-md-10">
                        <input class="form-control" name="alias" id="alias" value="<?php echo $this->item->alias ?>" placeholder="Tự động nhập" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Thời gian bắt đầu <span style="color:red;">*</span>
                    </label>
                    <div class="col-md-10">
                        <?php echo JHtml::_('calendar', $this->item->time_start, 'time_start', 'time_start', '%d-%m-%Y', array('style' => 'width: 100%;', 'placeholder' => 'Chọn thời gian bắt đầu')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Thời gian kết thúc <span style="color:red;">*</span>
                    </label>
                    <div class="col-md-10">
                        <?php echo JHtml::_('calendar', $this->item->time_finish, 'time_finish', 'time_finish', '%d-%m-%Y', array('style' => 'width: 100%;', 'placeholder' => 'Chọn thời gian kết thúc')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Hình ảnh <span style="color:red;">*</span>
                    </label>
                    <div class="col-md-10">
                        <?php
                        if (empty($this->item->banner_image)) {
                            ?>
                            <input type="file" name="banner_img" id="banner_img" value=""/>
                            <?php
                        } else {
                            ?>
                            <img src="<?php echo OPENSHOP_PATH_BANNERS_HTTP . $this->item->banner_image ?>" />

                            <input type="checkbox" name="remove_image" value="1"> Xóa hình ảnh
                            <?php
                        }
                        ?>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Phát hành
                    </label>
                    <div class="col-md-10">
                        <?php echo $this->lists['published']; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Miêu tả 
                    </label>
                    <div class="col-md-10">
                        <?php echo $editor->display('description', $this->item->description, '100%', '250', '75', '10'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Từ khoá
                    </label>
                    <div class="col-md-10">
                        <textarea name="meta_keywords" cols="20" rows="5" class="form-control"><?php echo $this->item->meta_keywords ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Sự miêu tả
                    </label>
                    <div class="col-md-10">
                        <textarea name="meta_description" cols="20" rows="5" class="form-control"><?php echo $this->item->meta_description ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">
                        Robots
                    </label>
                    <div class="col-md-10">
                        <select name="robot" class="form-control">
                            <?php
                            $r1 = $r2 = $r3 = $r4 = '';
                            switch ($this->item->robot) {
                                case 'Index,Follow':
                                    $r1 = 'selected="selected"';
                                    break;
                                case 'No index,Follow':
                                    $r2 = 'selected="selected"';
                                    break;
                                case 'Index,No follow':
                                    $r3 = 'selected="selected"';
                                    break;
                                case 'No index,No follow':
                                    $r4 = 'selected="selected"';
                                    break;
                            }
                            ?>
                            <option value="index,follow" <?php echo $r1 ?>>Index, Follow</option>
                            <option value="no index,follow" <?php echo $r2 ?>>No index, Follow</option>
                            <option value="index,no follow" <?php echo $r3 ?>>Index, No follow</option>
                            <option value="no index,no follow" <?php echo $r4 ?>>No index, No follow</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Chọn sản phẩm</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-5">Nhập tên hoặc mã sản phẩm</label>
                            <div class="col-md-7 positionRelative">
                                <input type="text" value="" name="" id="txt_product" class="form-control" placeholder="Nhập tên hoặc mã sản phẩm" onkeyup="getProductSearch(this.value)"/>
                                <div class="infoSearch">
                                    <ul>
                                        <!--SEARCH-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">
                                <strong>Sản phẩm đã chọn <span style="color:red;">*</span></strong>
                            </label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Hình ảnh</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="loadProduct">
                                        <?php
                                        $arrPro = $this->item->arr_product;
                                        if (!empty($arrPro)) {
                                            $pros = $this->getProductList($arrPro);
                                            if (count($pros)) {
                                                foreach ($pros as $k => $p) {
                                                    $link = JRoute::_('index.php?option=com_openshop&task=product.edit&cid[]=' . $p->id);
                                                    ?>
                                                    <tr class="checkClass tr_<?php echo $p->id ?>">
                                                        <td><?php echo $k + 1 ?></td>
                                                        <td>
                                                            <a href="<?php echo $link ?>" target="_blank">
                                                                <?php echo $p->product_name ?>
                                                            </a>
                                                            <?php
                                                            if ($p->published == 0) {
                                                                ?>
                                                                <br/>
                                                                <span style="color:red;">
                                                                    <i class="icon-unpublish "></i>
                                                                    Sản phẩm chưa được xuất bản
                                                                </span>
                                                                <?php
                                                            }
                                                            ?>
                                                            <input type="hidden" value="<?php echo $p->id ?>" name="arr_product[]" />
                                                        </td>
                                                        <td>
                                                            <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $p->product_image ?>" width="50px"/>
                                                        </td>
                                                        <td>
                                                            <i class="icon-delete iconDeletePro" onclick="setDeletePro(<?php echo $p->id ?>)"></i>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
        <input type="hidden" name="task" value="" />
    </form>
</fieldset>