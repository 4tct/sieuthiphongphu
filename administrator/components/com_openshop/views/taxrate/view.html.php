<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewTaxrate extends OpenShopViewForm
{
	function _buildListArray(&$lists, $item)
	{
		$db = JFactory::getDbo();
		OpenShopHelper::chosen();
		$options = array();
		$options[] = JHtml::_('select.option', 'P', JText::_('OPENSHOP_PERCENTAGE'));
		$options[] = JHtml::_('select.option', 'F', JText::_('OPENSHOP_FIXED_AMOUNT'));
		$lists['tax_type'] = JHtml::_('select.genericlist', $options, 'tax_type', ' class="inputbox" ', 'value', 'text', $item->tax_type);
		//get list geozone
		$query = $db->getQuery(true);
		$query->select('id, geozone_name')
			->from('#__openshop_geozones')
			->where('published = 1')
			->order('geozone_name');
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$lists['geozone_id'] = JHtml::_('select.genericlist', $rows, 'geozone_id', ' class="inputbox" ', 'id', 'geozone_name', $item->geozone_id);
		//get list customer group
		$query->clear();
		$query->select('a.id AS value, b.customergroup_name AS text')
			->from('#__openshop_customergroups AS a')
			->innerJoin('#__openshop_customergroupdetails AS b ON (a.id = b.customergroup_id)')
			->where('a.published = 1')
			->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
			->order('b.customergroup_name');
		$db->setQuery($query);
		$options = $db->loadObjectList();
		if ($item->id)
		{
			$query->clear();
			$query->select('customergroup_id')
				->from('#__openshop_taxcustomergroups')
				->where('tax_id = ' . intval($item->id));
			$db->setQuery($query);
			$selectedItems = $db->loadColumn();
		}
		else
		{
			$selectedItems = array();
		}
		$lists['customergroup_id'] = JHtml::_('select.genericlist', $options, 'customergroup_id[]',
			array(
				'option.text.toHtml' => false, 
				'option.text' => 'text', 
				'option.value' => 'value', 
				'list.attr' => ' class="inputbox chosen" multiple ', 
				'list.select' => $selectedItems));
		
		return true;
	}
}