<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewLanguage extends JViewLegacy {

    function display($tpl = null) {
        $mainframe = JFactory::getApplication();
        $option = 'com_openshop';
        $lang = $mainframe->getUserStateFromRequest($option . 'lang', 'lang', '0', 'string');
        $type_name = $mainframe->getUserStateFromRequest($option . 'type_name', 'type_name', '0', 'string');
        $object_name = $mainframe->getUserStateFromRequest($option . 'object_name', 'object_name', '0', 'string');
        $pos = $mainframe->getUserStateFromRequest($option . 'pos', 'pos', '-1', 'int');
       
        $search = $mainframe->getUserStateFromRequest('com_openshop.language.search', 'search', '', 'string');
        $search = JString::strtolower($search);
        $lists['search'] = $search;
        
        $model = $this->getModel('language');
        $trans = $model->getTrans($lang, $type_name, $object_name, $pos);
        $languages = $model->getSiteLanguages();
        $pagination = $model->getPagination();
        
//Combobox List For Lang      
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_SELECT_LANG'));
        foreach ($languages as $language) {
            $options[] = JHtml::_('select.option', $language, $language);
        }
        $lists['lang'] = JHtml::_('select.genericlist', $options, 'lang', ' class="inputbox"  onchange="submit();" style="width:150px" ', 'value', 'text', isset($lang) ? $lang : "vi-VN");
  
        
//List Type
    
        $typies = $model->getType();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_SELECT_TYPE'));
        foreach ($typies as $type) {
            $options[] = JHtml::_('select.option', $type->type, $type->type);
        }
        $lists['type'] = JHtml::_('select.genericlist', $options, 'type_name', ' class="inputbox"  onchange="submit();" style="width:150px"', 'value', 'text', isset($type_name) ? $type_name : 0);

//object name
        
        $objects = $model->getObjectName($type_name);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_SELECT_OBJECT'));
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->element, $object->element);
        }
        $lists['object_name'] = JHtml::_('select.genericlist', $options, 'object_name', ' class="inputbox"  onchange="submit();" style="width:150px"', 'value', 'text', isset($object_name) ? $object_name : 0);
        
//Combo box list for possition        
        $options = array();
        $options[] = JHtml::_('select.option', -1, JText::_('OPENSHOP_SELECT_POS'));
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_FRONT_END'));
        $options[] = JHtml::_('select.option', 1, JText::_('OPENSHOP_BACK_END'));
        $lists['pos'] = JHtml::_('select.genericlist', $options, 'pos', ' class="inputbox"  onchange="submit();" style="width:150px"', 'value', 'text', isset($pos)? $pos : 0);
        
        $this->assignRef('lang', $lang);
        $this->assignRef('trans', $trans);
        $this->assignRef('lists', $lists);
      
        $this->pagination = $pagination;
        parent::display($tpl);
    }

}
