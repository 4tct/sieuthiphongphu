<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

JToolBarHelper::title(JText::_( 'OPENSHOP_TRANSLATION'), 'generic.png');
JToolBarHelper::addNew('new_item', 'OPENSHOP_NEW_ITEM');
JToolBarHelper::apply('language.save');
JToolBarHelper::cancel('language.cancel');

$type_name = JRequest::getVar('type_name','component');
$object_name = JRequest::getVar('object_name','com_openshop');
if(isset($type_name)&&$type_name=="template"){
    $object_name ="tpl_".$object_name;
}
if(isset($type_name) && $type_name=="library"){
    $object_name ="lib_".$object_name;
}
?>
<script type="text/javascript">
	Joomla.submitbutton = function(pressbutton)
	{
		var form = document.adminForm;
		if (pressbutton == 'new_item') {
			Joomla.newLanguageItem();
			return;				
		} else {
			Joomla.submitform(pressbutton, form);
		}
	}
	Joomla.newLanguageItem = function() {
		table = document.getElementById('lang_table');
		row = table.insertRow(1);
		cell0  = row.insertCell(0);
		cell0.innerHTML = '<input type="text" name="extra_keys[]" class="inputbox" size="50" />';
		cell1 = row.insertCell(1);
		cell2 = row.insertCell(2);
		cell2.innerHTML = '<input type="text" name="extra_values[]" class="inputbox" size="100" />';
	}
</script>
<fieldset class="adminfs">
<form action="index.php?option=com_openshop&view=language" method="post" name="adminForm" id="adminForm">
	<table width="100%">
		<tr>			
			<td style="text-align: left;">
				<?php echo JText::_( 'OPENSHOP_FILTER' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
				<button onclick="this.form.submit();" class="btn"><?php echo JText::_( 'OPENSHOP_GO' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();document.getElementById('lang').selectedIndex='0';document.getElementById('type_name').selectedIndex='0';document.getElementById('object_name').selectedIndex='0';document.getElementById('pos').selectedIndex='0'" class="btn"><?php echo JText::_( 'OPENSHOP_RESET' ); ?></button>
			</td>
			<td style="text-align: right">
				<?php echo JText::_('OPENSHOP_TRANSLATION_INTRO'); ?>
				<?php echo $this->lists['lang']; ?>
                                
                                <?php echo $this->lists['type']; ?>
                                <?php echo $this->lists['object_name']; ?>
				<?php echo $this->lists['pos']; ?>
			</td>
		</tr>
	</table>
	<table class="adminlist table table-striped" id="lang_table">
		<thead>
			<tr>
				<th class="text_left" width="20%">
					<?php echo JText::_( 'OPENSHOP_KEY' ); ?>
				</th>
				<th class="text_left" width="35%">
					<?php echo JText::_( 'OPENSHOP_ORIGINAL' ); ?>
				</th>
				<th class="text_left" width="35%">
					<?php echo JText::_( 'OPENSHOP_TRANSLATION' ); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="4"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
		<tbody>
			<?php
                        if(empty($this->trans['en-GB'])||empty($this->trans['vi-VN'])){
                            $trans = "";
                        }else{
			$original = $this->trans['en-GB'][$object_name];
                      
			$trans = $this->trans['vi-VN'][$object_name];
                        }
                       // print_r($trans);
			$search = $this->lists['search'];
                        if(is_array($trans) ||  is_object($trans)){
                            foreach ($trans as $key => $value)
                            {
                                    $show = true;
                                    if (isset($trans[$key]))
                                    {
                                            $translatedValue = $trans[$key];
                                            $missing = false;
                                    }
                                    else
                                    {
                                            $translatedValue = $value;
                                            $missing = true;
                                    }								
                                    ?>
                                    <tr>
                                            <td><?php echo $key; ?></td>
                                            <td><?php echo $value; ?></td>
                                            <td>
                                                    <input type="hidden" name="keys[]" value="<?php echo $key; ?>" />
                                                    <input type="text" name="<?php echo $key; ?>" class="input-xxlarge" value="<?php echo htmlspecialchars($translatedValue); ?>" />
                                                    <?php
                                                            if ($missing)
                                                            {
                                                                    ?>
                                                                    <span style="color:red;">*</span>
                                                                    <?php
                                                            }
                                                    ?>
                                            </td>
                                    </tr>
                                    <?php				
                            }
                        }
			?>
		</tbody>
	</table>
	<input type="hidden" name="option" value="com_openshop" />	
	<input type="hidden" name="task" value="" />			
	<?php echo JHtml::_( 'form.token' ); ?>
</form>
</fieldset>