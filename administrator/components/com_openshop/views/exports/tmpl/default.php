<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JToolBarHelper::title(JText::_('OPENSHOP_EXPORTS'), 'generic.png');
JToolBarHelper::custom('exports.process', 'download', 'download', Jtext::_('OPENSHOP_PROCESS'), false);
JToolBarHelper::cancel('exports.cancel');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(pressbutton)
	{
		var form = document.adminForm;
		if (pressbutton == 'exports.cancel') {
			Joomla.submitform(pressbutton, form);
			return;
		} else {
			if (form.export_type.value == '') {
				alert('<?php echo JText::_('OPENSHOP_EXPORT_TYPE_PROMPT'); ?>');
				form.export_type.focus();
				return;	
			}
			Joomla.submitform(pressbutton, form);
		}
	}
</script>
<fieldset class="adminfs">
<form action="index.php" method="post" name="adminForm" id="adminForm">
	<div class="row-fluid">
		<div class="span6">
			<table class="admintable adminform" style="width: 100%;">
				<tr>
					<td class="key">
						<?php echo  JText::_('OPENSHOP_EXPORT_TYPE'); ?>
					</td>
					<td>
						<?php echo $this->lists['export_type']; ?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo  JText::_('OPENSHOP_FIELD_DELIMITER'); ?>
					</td>
					<td>
						<input class="input-mini" type="text" name="field_delimiter" id="field_delimiter" maxlength="1" value="," />
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo  JText::_('OPENSHOP_IMAGE_SEPARATOR'); ?>
					</td>
					<td>
						<input class="input-mini" type="text" name="image_separator" id="image_separator" maxlength="1" value=";" />
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo  JText::_('OPENSHOP_LANGUAGE'); ?>
					</td>
					<td>
						<?php echo $this->lists['language']; ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<input type="hidden" name="option" value="com_openshop" />
	<input type="hidden" name="task" value="" />
	<div class="clearfix"></div>
</form>
</fieldset>