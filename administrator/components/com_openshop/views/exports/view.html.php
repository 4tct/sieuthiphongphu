<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewExports extends JViewLegacy
{

	function display($tpl = null)
	{
		$lists = array();
		//Export type
		$options = array();
		$options[] = JHtml::_('select.option', '', JText::_('OPENSHOP_NONE'));
		$options[] = JHtml::_('select.option', 'products', JText::_('OPENSHOP_PRODUCTS'));
		$options[] = JHtml::_('select.option', 'categories', JText::_('OPENSHOP_CATEGORIES'));
		$options[] = JHtml::_('select.option', 'customers', JText::_('OPENSHOP_CUSTOMERS'));
		$options[] = JHtml::_('select.option', 'orders', JText::_('OPENSHOP_ORDERS'));
		$lists['export_type'] = JHtml::_('select.genericlist', $options, 'export_type', ' class="inputbox" ', 'value', 'text', JRequest::getVar('export_type'));
		//Language
		jimport('joomla.filesystem.folder');
		$path = JPATH_ROOT . '/language';
		$folders = JFolder::folders($path);
		$languages = array();
		foreach ($folders as $folder)
		if ($folder != 'pdf_fonts' && $folder != 'overrides')
			$languages[] = $folder;
		$options = array();
		foreach ($languages as $language)
		{
			$options[] = JHtml::_('select.option', $language, $language);
		}
		$lists['language'] = JHtml::_('select.genericlist', $options, 'language', ' class="inputbox" ', 'value', 'text', '');
		
		$this->lists = $lists;
		parent::display($tpl);
	}
}