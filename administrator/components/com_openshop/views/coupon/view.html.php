<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewCoupon extends OpenShopViewForm
{
	function _buildListArray(&$lists, $item)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$nullDate = $db->getNullDate();
		$options = array();
		$options[] = JHtml::_('select.option', 'P', JText::_('OPENSHOP_PERCENTAGE'));
		$options[] = JHtml::_('select.option', 'F', JText::_('OPENSHOP_FIXED_AMOUNT'));
		$lists['coupon_type'] = JHtml::_('select.genericlist', $options, 'coupon_type', ' class="inputbox" ', 'value', 'text', $item->coupon_type);
		//Get multiple products
		$query->select('a.id AS value, b.product_name AS text')
			->from('#__openshop_products AS a')
			->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
			->where('a.published = 1')
			->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
			->order('b.product_name');
		$db->setQuery($query);
		$products = $db->loadObjectList();
		$query->clear();
		$query->select('product_id')
			->from('#__openshop_couponproducts')
			->where('coupon_id = ' . intval($item->id));
		$db->setQuery($query);
		$productIds = $db->loadColumn();
		$lists['product_id'] = JHtml::_('select.genericlist', $products, 'product_id[]', ' class="inputbox chosen" multiple ', 'value', 'text', $productIds);
		//Get multiple customer groups
		$query->clear();
		$query->select('a.id, b.customergroup_name AS name')
			->from('#__openshop_customergroups AS a')
			->innerJoin('#__openshop_customergroupdetails AS b ON (a.id = b.customergroup_id)')
			->where('a.published = 1')
			->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
			->order('b.customergroup_name');
		$db->setQuery($query);
		$customergroups = $db->loadObjectList();
		$query->clear();
		$query->select('customergroup_id')
			->from('#__openshop_couponcustomergroups')
			->where('coupon_id = ' . intval($item->id));
		$db->setQuery($query);
		$customergroupArr = $db->loadColumn();
		$lists['customergroup_id'] = JHtml::_('select.genericlist', $customergroups, 'customergroup_id[]', ' class="inputbox chosen" multiple ', 'id', 'name', $customergroupArr);
		$lists['coupon_shipping'] = JHtml::_('Openselect.booleanlist', 'coupon_shipping', ' class="inputbox" ', $item->coupon_shipping);
		//Get history coupon
		$query->clear();
		$query->select('*')
			->from('#__openshop_couponhistory')
			->where('coupon_id = ' . intval($item->id));
		$db->setQuery($query);
		$couponHistories = $db->loadObjectList();
		$this->lists = $lists;
		$this->couponHistories = $couponHistories;
		$this->nullDate = $nullDate;
	}
}