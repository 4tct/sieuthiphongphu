<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewField extends OpenShopViewForm
{

	function _buildListArray(&$lists, $item)
	{		
		$fieldTypes = array('Text', 'Textarea', 'List', 'Checkboxes', 'Radio', 'Countries', 'Zone');
		$options = array();
		$options[] = JHtml::_('select.option', -1, JText::_('OPENSHOP_FIELD_TYPE'));
		$options = array();
		foreach ($fieldTypes as $fieldType)
		{
			$options[] = JHtml::_('select.option', $fieldType, $fieldType);
		}
		if ($item->is_core)
		{
			$disabled = " disabled "; 
		}
		else 
		{
			$disabled = '';
		}
		$lists['fieldtype'] = JHtml::_('select.genericlist', $options, 'fieldtype', 'class="input-large" onchange="changeField(this.value)" '.$disabled, 'value', 'text', $item->fieldtype);
		
		$validateRules = array(
			'numeric' => JText::_('OPENSHOP_NUMERIC'),
			'integer' => JText::_('OPENSHOP_INTEGER'),
			'float' => JText::_('OPENSHOP_FLOAT'),						
			'max_len,32' => JText::_('OPENSHOP_MAX_LENGTH'),
			'min_len,1' => JText::_('OPENSHOP_MIN_LENGTH'),
			'exact_len,10' => JText::_('OPENSHOP_EXACT_LENGTH'),
			'max_numeric,100' => JText::_('OPENSHOP_MAX_NUMERIC'),
			'min_numeric,1' => JText::_('OPENSHOP_MIN_NUMERIC'),			
			'valid_email' => JText::_('OPENSHOP_VALID_EMAIL'),			
			'valid_url' => JText::_('OPENSHOP_VALID_URL')
		);
		$options = array();
		$options[] = JHtml::_('select.option', '', JText::_('OPENSHOP_NONE'));
		foreach ($validateRules as $rule => $title)
		{
			$options[] = JHtml::_('select.option', $rule, $title);			
		}
		$lists['validation_rule'] = JHtml::_('select.genericlist', $options, 'validation_rule[]', ' class="input-large" multiple size="10" onclick="buildValidationString();"', 'value', 'text', explode('|', $item->validation_rule));
		$options = array();
		$options[] = JHtml::_('select.option', 'A', JText::_('OPENSHOP_ALL'));
		$options[] = JHtml::_('select.option', 'B', JText::_('OPENSHOP_BILLING_ADDRESS'));
		$options[] = JHtml::_('select.option', 'S', JText::_('OPENSHOP_SHIPPING_ADDRESS'));
		$lists['address_type'] = JHtml::_('select.genericlist', $options, 'address_type', 'class="input-large" '.$disabled, 'value', 'text', $item->address_type);
		if (in_array($item->name, OpenShopModelField::$protectedFields))
		{
			$disabled = " disabled ";
		}
		else 
		{
			$disabled = "";
		}
		$lists['required'] = JHtml::_('select.booleanlist', 'required', ' class="inputbox" onclick="buildValidationString();" '.$disabled, $item->required);		
		$lists['multiple'] = JHtml::_('select.booleanlist', 'multiple', ' class="inputbox" '.$disabled, $item->multiple);
	}
}