<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewWarehouse extends OpenShopViewForm {

    function _buildListArray(&$lists, $item) {
        //Build customer groups list
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        
        $query->select('id AS value, argency_name AS text')
                ->from($db->quoteName('#__openshop_argencies'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_NONE'), 'value', 'text');
        if(count($rows))
        {
            $options = array_merge($options, $rows);
        }
        
        $lists['option_argency'] = JHtml::_('select.genericlist', $options, 'argency_id', 
                array(
                    'option.text.toHtml' => false, 
                    'option.value' => 'value', 
                    'option.text' => 'text', 
                    'list.attr' => ' class="inputbox" ', 
                    'list.select' => $item->argency_id
                )
        );
        
        $query->clear();
        $query->select('id as value, title as text')
                ->from($db->quoteName('#__usergroups'));
        $rows = $db->setQuery($query)->loadObjectList();
        $options = array();
        $options = array_merge($options, $rows);
        $usergoupArr = array();
        if(isset($item->user_group_manage)){
            $arr = explode(',', $item->user_group_manage);
            for($i=0,$l=  count($arr); $i<$l; $i++){
                $usergoupArr[] = $arr[$i];
            }
        }
        
        $lists['user_group_manage'] = JHtml::_('select.genericlist', $options, 'user_group_m[]', 'class="inputbox chosen" multiple', 'value', 'text', $usergoupArr);
    }

}
