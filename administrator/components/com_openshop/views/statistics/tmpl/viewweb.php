<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('_JEXEC') or die('Restricted access');
?>
<?php
//    print_r($this->dataView);
?>
<div class="adminfs">
    <table class="table">
        <thead style="background: #D0D0D0;">
            <tr>
                <th class="center">STT</th>
                <th class="center">IP</th>
                <th class="center">User</th>
                <th class="center">Nơi truy cập</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($this->dataView as $key => $value) {
                ?>
                <tr>
                    <td class="center"><?php echo $key + 1 ?></td>
                    <td class="center"><?php echo $value->ip ?></td>
                    <td class="center">
                        <?php
                        echo $value->user_id == 0 ? '-' : JFactory::getUser($value->user_id)->name;
                        ?>
                    </td>
                    <td><?php echo $value->location ?></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>