<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="adminfs">
    <div class="span12" style="margin-left: 0px;">
        <div class="span4 columnStatistic stsView">
            <a href="index.php?option=com_openshop&view=statistics&layout=viewweb">
                <?php echo JText::_('OPENSHOP_VIEW_WEB'); ?>
            </a>
            <img src="<?php echo JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'icons'.DS.'view.png' ?>" alt="Icon-view" />
        </div>
        <div class="span4 columnStatistic stsCus">
            <a href="index.php?option=com_openshop&view=statistics&layout=viewweb">
                <?php echo JText::_('OPENSHOP_CUSTOMER_VIEW'); ?>
            </a>
            <img src="<?php echo JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'icons'.DS.'statisticCustomer.png' ?>" alt="Icon-view" />
        </div>
        <div class="span4 columnStatistic stsOrder">
            <a href="index.php?option=com_openshop&view=statistics&layout=viewweb">
                <?php echo JText::_('OPENSHOP_ORDER_CART'); ?>
            </a>
            <img src="<?php echo JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'icons'.DS.'statisticOrder.png' ?>" alt="Icon-view" />
        </div>
    </div>
    
    <div class="span12" style="margin-left: 0px; margin-top: 10px;">
        <div class="span4 columnStatistic stsView">
            <a href="index.php?option=com_openshop&view=statistics&layout=viewproduct">
                <?php echo JText::_('OPENSHOP_PRODUCT_VIEW'); ?>
            </a>
            <img src="<?php echo JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'icons'.DS.'productView_icon.png' ?>" alt="Icon-view" />
        </div>
        <div class="span4 columnStatistic stsCus">
            <a href="index.php?option=com_openshop&view=statistics&layout=viewweb">
                <?php echo JText::_('OPENSHOP_PRODUCT_ORDER'); ?>
            </a>
            <img src="<?php echo JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'icons'.DS.'productcart_icon.png' ?>" alt="Icon-view" />
        </div>
        <div class="span4 columnStatistic stsOrder">
            <a href="index.php?option=com_openshop&view=statistics&layout=viewweb">
                <?php echo JText::_('OPENSHOP_NUMBER_USER'); ?>
            </a>
            <img src="<?php echo JUri::base() . 'components'.DS.'com_openshop'.DS.'assets'.DS.'icons'.DS.'user.png' ?>" alt="Icon-view" />
        </div>
    </div>
</div>