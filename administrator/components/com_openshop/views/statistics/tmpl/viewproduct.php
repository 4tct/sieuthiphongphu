<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('_JEXEC') or die('Restricted access');
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'canvasjs.min.js');
?>

<div class="adminfs">
    <div id="chartContainer" style="height: 400px; width: 100%;"></div>
    <?php
    echo '<script type="text/javascript">';
    echo ' window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "TOP 15 sản phẩm được xem nhiều nhất"
                },
                data: [{
                        type: "column",
                        dataPoints: [';
            foreach($this->dataViewPro as $vp){
                            echo '{y: '. $vp->hits .', label: "'. $vp->product_name .'", indexLabel: "'. $vp->hits .'"},';
            }
                        echo ']
                    }]
            });
            chart.render();
        }';
    echo '</script>';
    ?>
<!--    <script type="text/javascript">
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Danh sách các sản phẩm được xem nhiều nhất"
                },
                data: [{
                        type: "column",
                        dataPoints: [
                            {y: 1, label: <?php echo 'a'; ?>}
                        ]
                    }]
            });
            chart.render();
        };
    </script>-->

</div>   