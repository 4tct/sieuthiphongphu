<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;

?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'argency.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
<?php
if ($translatable) {
    foreach ($this->languages as $language) {
        $langId = $language->lang_id;
        ?>
                    if (document.getElementById('argency_name_<?php echo $langId; ?>').value == '') {
                        alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                        document.getElementById('argency_name_<?php echo $langId; ?>').focus();
                        return;
                    }
        <?php
    }
} else {
    ?>
                if (form.argency_name.value == '') {
                    alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                    form.argency_name.focus();
                    return;
                }
    <?php
}
?>
            Joomla.submitform(pressbutton, form);
        }
    }
</script>
<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <div class="row-fluid">
            <div class="tab-content">
                <div class="tab-pane active" id="general-page">
                    <div class="span112">
                        <?php
                        if ($translatable) {
                        ?>
                            <ul class="nav nav-tabs">
                            <?php
                            $i = 0;
                            foreach ($this->languages as $language) {
                                $langCode = $language->lang_code;
                                ?>
                                    <li <?php echo $i == 0 ? 'class="active"' : ''; ?>><a href="#general-page-<?php echo $langCode; ?>" data-toggle="tab"><?php echo $this->languageData['title'][$langCode]; ?>
                                            <img src="<?php echo JURI::root(); ?>media/com_openshop/flags/<?php echo $this->languageData['flag'][$langCode]; ?>" /></a></li>
                                    <?php
                                    $i++;
                                }
                                ?>																					
                            </ul>
                            <div class="tab-content">
                                <?php
                                $i = 0;
                                foreach ($this->languages as $language) {
                                    $langId = $language->lang_id;
                                    $langCode = $language->lang_code;
                                    ?>
                                
                                    <div class="tab-pane<?php echo $i == 0 ? ' active' : ''; ?>" id="general-page-<?php echo $langCode; ?>">													
                                        <table class="admintable adminform" style="width: 100%;">
                                            <tr>
                                                <td class="key" width="20%">
                                                    <span class="required">*</span>
                                                    <?php echo JText::_('OPENSHOP_NAME'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="argency_name_<?php echo $langCode; ?>" id="argency_name_<?php echo $langId; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'argency_name_' . $langCode}) ? $this->item->{'argency_name_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_ALIAS'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="argency_alias_<?php echo $langCode; ?>" id="argency_alias_<?php echo $langId; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'argency_alias_' . $langCode}) ? $this->item->{'argency_alias_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_PHONE'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="argency_phone_<?php echo $langCode; ?>" id="argency_phone_<?php echo $langCode; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'argency_phone_' . $langCode}) ? $this->item->{'argency_phone_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_EMAIL'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="argency_email_<?php echo $langCode; ?>" id="argency_email_<?php echo $langCode; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'argency_email_' . $langCode}) ? $this->item->{'argency_email_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_ADDRESS'); ?>
                                                </td>
                                                <td>
                                                    <input class="input-xlarge" type="text" name="argency_address_<?php echo $langCode; ?>" id="argency_address_<?php echo $langCode; ?>" size="" maxlength="250" value="<?php echo isset($this->item->{'argency_address_' . $langCode}) ? $this->item->{'argency_address_' . $langCode} : ''; ?>" />
                                                </td>								
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                                                </td>
                                                <td>
                                                    <?php echo $this->lists['published']; ?>
                                                </td>							
                                            </tr>
                                            <tr>
                                                <td class="key">
                                                    <?php echo JText::_('OPENSHOP_DESCRIPTION'); ?>
                                                </td>
                                                <td>
                                                    <?php echo $editor->display('argency_desc_' . $langCode, isset($this->item->{'argency_desc_' . $langCode}) ? $this->item->{'argency_desc_' . $langCode} : '', '100%', '250', '75', '10'); ?>
                                                </td>								
                                            </tr>
                                        </table>
                                    </div>							
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                            </div>
                                <?php
                            } else {
                                ?>
                            <table class="admintable adminform" style="width: 100%;">
                                <tr>
                                    <td class="key" width="15%">
                                        <span class="required">*</span>
                            <?php echo JText::_('OPENSHOP_NAME'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="argency_name" id="argency_name" size="" maxlength="250" value="<?php echo $this->item->argency_name; ?>" />
                                    </td>								
                                </tr>																		
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_ALIAS'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="argency_alias" id="argency_alias" size="" maxlength="250" value="<?php echo $this->item->argency_alias; ?>" />
                                    </td>								
                                </tr>
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_PHONE'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="argency_phone" id="argency_phone" size="" maxlength="250" value="<?php echo $this->item->argency_phone; ?>" />
                                    </td>								
                                </tr>
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_ADDRESS'); ?>
                                    </td>
                                    <td>
                                        <input class="input-xlarge" type="text" name="argency_address" id="argency_address" size="" maxlength="250" value="<?php echo $this->item->argency_address; ?>" />
                                    </td>								
                                </tr>
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->lists['published']; ?>
                                    </td>							
                                </tr>
                                <tr>
                                    <td class="key">
                                        <?php echo JText::_('OPENSHOP_DESCRIPTION'); ?>
                                    </td>
                                    <td>
                                        <?php echo $editor->display('argency_desc', $this->item->argency_desc, '100%', '250', '75', '10'); ?>
                                    </td>
                                </tr>
                            </table>
                                        <?php
                                    }
                                    ?>
                    </div>
                </div><!-- End General page -->
            </div>
        </div>
                                    <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
                                    <?php
                                    if ($translatable) {
                                        foreach ($this->languages as $language) {
                                            $langCode = $language->lang_code;
                                            ?>
                <input type="hidden" name="details_id_<?php echo $langCode; ?>" value="<?php echo isset($this->item->{'details_id_' . $langCode}) ? $this->item->{'details_id_' . $langCode} : ''; ?>" />
        <?php
    }
} elseif ($this->translatable) {
    ?>
            <input type="hidden" name="details_id" value="<?php echo isset($this->item->{'details_id'}) ? $this->item->{'details_id'} : ''; ?>" />
            <?php
        }
        ?>
        <input type="hidden" name="task" value="" />
    </form>
</fieldset>