<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCompare_Inventories extends OpenShopViewList {

    function _buildToolbar() {
        JToolbarHelper::title(JText::_('OPENSHOP_COMPARE_INVENTORIES'));
        JToolbarHelper::custom('show_upload', 'upload', 'upload', JText::_('OPENSHOP_SHOW_UPLOAD'), FALSE);
        JToolbarHelper::custom('download_compare','download','download', JText::_('OPENSHOP_DOWNLOAD_COMPARE'), FALSE);
        JToolbarHelper::custom('product_not_import','expand-2','expand-2', JText::_('OPENSHOP_PRODUCT_NOT_IMPORT'), FALSE);
    }

    public function getQuantityScan($product, $size) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->clear();
        $query->select('optionvalue_id')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'b') . 'ON a.option_id = b.option_id')
                ->where('UPPER(b.option_name) = "SIZE"')
                ->where('UPPER(a.value) = "' . strtoupper($size) . '"');
        $id_size = $db->setQuery($query)->loadResult();

        if (!empty($id_size)) {
            $query->clear();
            $query->select('sum(quantity_import) as quantity_scan')
                    ->from($db->quoteName('#__openshop_kiemkhos'))
                    ->where('UPPER(product_sku) = "' . $product . '"')
                    ->where('size_id = ' . $id_size);
            $q = $db->setQuery($query)->loadResult();
            return empty($q) ? '0' : $q;
        }
        else{
            return 0;
        }
    }
    
    function proNotImport(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.product_sku, a.quantity_import,'
                        . '(SELECT VALUE FROM #__openshop_optionvaluedetails as z WHERE a.size_id = z.optionvalue_id) as size'
                        . '')
                ->from($db->quoteName('#__openshop_kiemkhos', 'a'))
                ->where('a.product_sku NOT IN('
                        . 'SELECT DISTINCT b.product_sku '
                        . 'FROM #__openshop_compare_inventories as b '
                        . 'INNER JOIN #__openshop_kiemkhos as c ON c.product_sku = b.product_sku '
                        . ')');
        return $db->setQuery($query)->loadObjectList();
    }
    
    function getCompareP($code){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_kiemkhos'))
                ->where('product_sku = "'. $code .'"');
        $sql = 'SELECT * FROM #__openshop_kiemkhos WHERE product_sku = "'. $code .'"';
        return $db->setQuery($query)->loadObjectList();
//        return $sql;
    }
    
    function TotlaQuantityProFile(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('sum(quantity)')
                ->from($db->quoteName('#__openshop_compare_inventories'));
        return $db->setQuery($query)->loadResult();
    }
    
    function TotlaQuantityProScan(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('sum(quantity_import)')
                ->from($db->quoteName('#__openshop_kiemkhos'));
        return $db->setQuery($query)->loadResult();
    }

}
