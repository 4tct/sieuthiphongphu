<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(OPENSHOP_ADMIN_JS . DS . 'script_admin.js');
?>

<?php
$db = JFactory::getDbo();
$query = $db->getQuery(TRUE);
$query->select('optionvalue_id,value')
        ->from($db->quoteName('#__openshop_optionvaluedetails'))
        ->where('option_id = 2');
$s = $db->setQuery($query)->loadObjectList();

$sizes = array();
foreach ($s as $key => $value) {
    $sizes[$value->optionvalue_id] = $value->value;
}
?>

<style>
    .btn-upload-file{
        border: none;
        padding: 10px 50px;
        background: #1F9A29;
        color: white;
        cursor: pointer;
    }
    .btn-upload-file:hover{
        background: #26B932;
        box-shadow: 0 2px 15px 0 #444444;
    }
    #right{
        background: red;
        color: white;
        display: table-cell;
    }
    #left{
        background: #3543FF;
        color: white;
        display: table-cell;
    }
    .limit{
        width: 80px;
        float: right;
        margin: 0 5px 0 0;
    }
    .continue_show{
        text-align: center;
        padding: 30px 0 0 0;
    }
    .continue_show > span{
        padding: 10px 50px;
        background: #2B8E1A;
        color: white;
        cursor: pointer;
    }

    .continue_show > span:hover{
        box-shadow: 0 3px 15px 0px black;
        background: #4FC12A;
    }
    .title_productFile{
        text-align: center;
        background: #D2E3EF;
        font-size: 20px;
        font-weight: bold;
    }
    .total_quantity{
        padding: 5px;
        background: #354677;
        border-radius: 5px;
        color: white;
        margin: 0 0 0 10px;
    }
</style>


<script>
    Joomla.submitbutton = function (task) {
        switch (task) {
            case 'show_upload':
                jQuery('#formUploadFile').toggle(300);
                break;
            case 'download_compare':
                $('#fromDownloadCompare').submit();
                break;
            case 'product_not_import':
                product_not_import();
                break;
            default:
                break;
        }
    };
</script>

<fieldset class="adminfs">
    <form id="formUploadFile" action="index.php?option=com_openshop&task=functions.uploadfile" method="post" enctype="multipart/form-data" style="display: none;">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo JText::_('OPENSHOP_UPLOAD_FILE') ?></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3"><?php echo JText::_('OPENSHOP_FILE_NAME'); ?></label>
                        <div class="col-md-9">
                            <input type="file" name="fileImport" id="fileImport" />
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group" style="text-align: center">
                        <input type="submit" value="Up" name="up" class="btn-upload-file"/>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="index.php?option=com_openshop&view=compare_inventories" method="post" >
        <div>
            <div style="display: inline-block;">
                <div style="margin: 10px 0 0 10px;">
                    <span style="padding: 5px; background: red;"></span>&nbsp;
                    <span><?php echo JText::_('OPENSHOP_RIGHT_TITLE'); ?></span>
                </div>
            </div>
            <div style="display: inline-block;">
                <div style="margin: 10px 0 0 10px;">
                    <span style="padding: 5px; background: blue;"></span>&nbsp;
                    <span><?php echo JText::_('OPENSHOP_LEFT_TITLE'); ?></span>
                </div>
            </div>
            <div class="limit" style="display: inline-block;">
                <select class="form-control" name="limit" id="limit" onchange="this.form.submit()">
                    <?php
                    $l = array('20', '50', '100', '200', '500','2000');
                    foreach ($l as $value) {
                        $selected = '';
                        if ($value == JFactory::getApplication()->input->getInt('limit')) {
                            $selected = 'selected';
                        }
                        ?>
                        <option value="<?php echo $value ?>" <?php echo $selected ?>><?php echo $value ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th class="text_center" colspan="4"><?php echo JText::_('OPENSHOP_IMPORT_NUM_PRODUCT'); ?></th>
                        <th class="text_center"><?php echo JText::_('OPENSHOP_SCAN_NUM_PRODUCT'); ?></th>
                    </tr>
                    <tr>
                        <th class="text_center"><?php echo JText::_('OPENSHOP_STT'); ?></th>
                        <th class="text_center"><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
                        <th class="text_center"><?php echo JText::_('OPENSHOP_PRODUCT_SKU'); ?></th>
                        <th class="text_center">
                            <?php 
                                echo JText::_('OPENSHOP_QUANTITY');
                                echo '<span class="total_quantity"> '. $this->TotlaQuantityProFile() .'</span>';
                            ?>
                        </th>
                        <th class="text_center">
                            <?php 
                                echo JText::_('OPENSHOP_QUANTITY_SCAN'); 
                                echo '<span class="total_quantity"> '. $this->TotlaQuantityProScan() .'</span>';
                            ?>
                        </th>
                    </tr>
                </thead>
                <tbody class="compare_row">
                    <tr>
                        <td colspan="5" class="title_productFile">Các sản phẩm không có trong File</td>
                    </tr>
                    <?php
                    foreach ($this->proNotImport() as $key => $value) {
                        ?>
                        <tr>
                            <td class="text_center"><?php echo ($key + 1) ?></td>
                            <td><?php echo '-' ?></td>
                            <td class="text_center">
                                <?php
                                echo $value->product_sku . '-' . $value->size
                                ?>
                            </td>
                            <td class="text_center" id="left"><?php echo '0' ?></td>
                            <td class="text_center" id="left"><?php echo $value->quantity_import ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td colspan="5" class="title_productFile">Các sản phẩm có trong File</td>
                    </tr>
                    <?php
                    foreach ($this->items as $key => $value) {
                        $code = '';
                        $quantity_scan = $this->getQuantityScan($value->product_sku, $value->product_size);
                        if ($value->quantity == $quantity_scan) {
                            $class = "";
                        } else if ($value->quantity > $quantity_scan) {
                            $class = "right";
                            $code = $this->getCompareP($value->product_sku);
                        } else if ($value->quantity < $quantity_scan) {
                            $class = "left";
                        }
                        ?>
                        <tr>
                            <td class="text_center"><?php echo ($key + 1) ?></td>
                            <td><?php echo $value->product_name ?></td>
                            <td class="text_center">
                                <?php
                                echo $value->product_sku . '-' . $value->product_size
                                ?>
                            </td>
                            <td class="text_center" id="<?php echo $class ?>"><?php echo $value->quantity ?></td>
                            <td class="text_center" id="<?php echo $class ?>">
                                <?php
                                    echo $quantity_scan
                                ?>
                                <br>
                                <?php
                                if ($code != '') {
                                    foreach ($code as $key => $c) {
                                        echo $c->product_sku . '-' . $sizes[$c->size_id];
                                        echo '<br>';
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <div class="">
                <div class="continue_show">
                    <span onclick="continueShowCompareInventory()"><?php echo JText::_('OPENSHOP_CONTINUE_SHOW') ?></span>
                    <input type="hidden" id="num_row" value="<?php echo count($this->items) ?>" />
                </div>
                <div class="loading_data_compare" style="display: none;text-align: center;">
                    <img src="/administrator/components/com_openshop/assets/images/loading3.gif" />
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="task" value="" />	
    <?php echo JHtml::_('form.token'); ?>	
</fieldset>

<!--//download-->
<form target="_blank" action="index.php?option=com_openshop&format=ajax&task=compare.downloadCompare" method="post" id="fromDownloadCompare">
</form>

<!--//product not import-->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="product_not_import">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo JText::_('OPENSHOP_PRODUCT_NOT_IMPORT'); ?></h4>
            </div>
            <div class="modal-body">
                <table class="adminlist table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo JText::_('OPENSHOP_STT'); ?></th>
                            <th><?php echo JText::_('OPENSHOP_PRODUCT_SKU'); ?></th>
                            <th><?php echo JText::_('OPENSHOP_QUANTITY'); ?></th>
                        </tr>
                    </thead>
                    <tbody class="content_product_not_import">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
