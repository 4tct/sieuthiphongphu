<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(JUri::base() . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'openshop.js');
//print_r($this->items);exit();
?>

<style>
    #tbl_discount tr td, #tbl_discount tr th{
        text-align: center;
        vertical-align: middle;
    }
</style>

<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=discountproducts" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>		
                </td>
                <td align="right" class="text-right">
                    <?php echo $this->lists['filter_state']; ?>
                </td>	
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped" id="tbl_discount">
                <thead>
                    <tr>
                        <th class="text_center" width="2%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_center" width="2%">
                            <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                        </th>
                        <th width="6%" class="text_center" style="min-width:55px">
                            <?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_left" width="20%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_NAME'), 'b.product_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th class="text_center" width="6%">
                            <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_PRODUCT_PRICE'); ?>
                        </th>
                        <th class="text_center" width="6%">
                            <?php echo JText::_('OPENSHOP_PERCENT_DISCOUNT'); ?>
                        </th>
                        <th class="text_center" width="6%">
                            <?php echo JText::_('OPENSHOP_PRODUCT_QUANTITY'); ?>
                        </th>
                        <th class="text_center" width="8%">
                            <?php echo JText::_('OPENSHOP_DISCOUNT_START_DATE'); ?>
                        </th>
                        <th class="text_center" width="8%">
                            <?php echo JText::_('OPENSHOP_DISCOUNT_END_DATE'); ?>
                        </th>
                        <th class="text_center" width="4%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>													
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="12">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $link = JRoute::_('index.php?option=com_openshop&task=product.edit&cid[]=' . $row->product_id);
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <td class="text_center">
                                <?php echo JHtml::_('grid.id', $i, $row->id); ?>
                            </td>
                            <td class="text_center">
                                <div class="btn-group">
                                    <?php
                                    $s_p = 1 - $row->published;
                                    $i_p = $s_p === 0 ? "icon-ok" : "icon-cancel";
                                    echo '<div class="btn btn-toggle ' . $i_p . '"  onclick=\'update_status_toggle("published_' . $row->id . '_' . $s_p . '","discountproducts","published","' . $s_p . '","id","' . $row->id . '","menus.update_access","' . JUri::base() . '");\' id="published_' . $row->id . '_' . $s_p . '">';
                                    echo '</div>';
                                    ?>
                                </div>
                            </td>
                            <td class="text_left">																			
                                <a href="<?php echo $link; ?>" target="_blank"><?php echo $row->product_name . ' <br/><b>' . $row->product_sku . '</b>'; ?></a>				
                            </td>	
                            <td class="text_center">
                                <?php
                                if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $row->product_image)) {
                                    $viewImage = JFile::stripExt($row->product_image) . '-100x100.' . JFile::getExt($row->product_image);
                                    if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                        ?>
                                        <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" width="70" />
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image; ?>" width="70" />
                                        <?php
                                    }
                                }
                                ?>
                            </td>
                            <td>
                                <span style="text-decoration: line-through; color: red;" title="<?php echo JText::_('OPENSHOP_PRICE_R') ?>"><?php echo number_format((int) $row->product_price, 0, ',', '.'); ?></span>
                                <br>
                                <span style="font-size: 20px; color: #001DFF;" title="<?php echo JText::_('OPENSHOP_PRICE_DISCOUNT') ?>"><?php echo number_format((int) $row->price_discount, 0, ',', '.') ?></span>
                            </td>
                            <td>
                                <?php echo $row->percent_discount . ' %'; ?>
                            </td>
                            <td>
                                <?php echo $row->quantity_discount; ?>
                            </td>
                            <td>
                                <?php
                                echo date('d-m-Y', strtotime($row->start_date));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo date('d-m-Y', strtotime($row->end_date));
                                ?>
                            </td>
                            <td class="text_center">
                                <?php echo $row->id; ?>
                            </td>
                        </tr>
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
            </table>
        </div>


        <!--PRODUCT IN ORDER-->
        <div class="modal fade" id="modal_productInOrder" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_PRODUCT_IN_ORDER') ?>: <i class="productName"></i></h3>
                        <div style="float: right;">
                            <button type="button" class="btn btn-default delete_order_cancel" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered" id="editcell">  
                            <thead>
                            <th><?php echo JText::_('OPENSHOP_STT') ?></th>
                            <th><?php echo JText::_('OPENSHOP_NAME') ?></th>
                            <th><?php echo JText::_('OPENSHOP_TELEPHONE') ?></th>
                            <th><?php echo JText::_('OPENSHOP_EMAIL') ?></th>
                            <th><?php echo JText::_('OPENSHOP_ADDRESS') ?></th>
                            <th><?php echo JText::_('OPENSHOP_QUANTITY') ?></th>
                            </thead>
                            <tbody id="info_productInOrder">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>
    </form>
</fieldset>



