<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCountry extends OpenShopViewForm
{
	function _buildListArray(&$lists, $item)
	{
		$lists['published'] = JHtml::_('Openselect.booleanlist', 'published', ' class="inputbox" ', $item->published);
		$lists['postcode_required'] = JHtml::_('Openselect.booleanlist', 'postcode_required', ' class="inputbox" ', $item->postcode_required);
		return true;
	}
}