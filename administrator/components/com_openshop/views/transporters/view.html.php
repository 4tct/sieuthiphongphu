<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewTransporters extends OpenShopViewList{
    public function _buildListArray(&$lists, $state) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.id as value , reason_detail_description as text')
                ->from($db->quoteName('#__openshop_reasons','a'))
                ->join('LEFT', $db->quoteName('#__openshop_reasondetails','b') . 'ON a.id = b.reason_id')
                ->where('a.id IN (select config_value from #__openshop_configs where config_key = "reason_delete_transporter")');
        $rows = $db->setQuery($query)->loadObjectList();
        $option = array();
        $option[] = JHtml::_('select.option' ,'0', JText::_('OPENSHOP_REASON_DELETE_TRANSPORTER_SELECT'));
        if(count($rows))
        {
            $option = array_merge($option, $rows);
        }
        
        $lists['reason_delete_transporter'] = JHtml::_('select.genericlist', $option, 'reason_delete_transporter', 'class="form-control reason_delete_transporter"', 'value', 'text', '0');
    }
}