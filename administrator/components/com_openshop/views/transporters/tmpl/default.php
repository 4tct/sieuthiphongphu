<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(JUri::base() . DS . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'trans.js');
JFactory::getDocument()->addScript(JUri::base() . DS . 'components' . DS . 'com_openshop' . DS . 'assets' . DS . 'js' . DS . 'openshop.js');
?>
<style type="text/css">
    table tr th, table tr td{
        vertical-align: middle !important;
    }
    div.modal{
        overflow-y: auto;
    }
</style>
<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=transporters" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>
                </td>
                <td align="right">
                    <?php echo $this->lists['filter_state']; ?>
                </td>
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped">
                <thead>
                    <tr>
                        <th width="3%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th width="5%" class="text_center" style="min-width:55px">
                            <?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_left" width="15%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_NAME'), 'b.transporter_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                        </th>
                        <th width="10%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_PHONE'), 'a.transporter_phone', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th width="10%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_EMAIL'), 'a.transporter_email', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th width="10%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ADDRESS'), 'a.transporter_address', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th width="10%" class="text_center">
                            <?php echo JText::_('OPENSHOP_WEBSITE'); ?>
                        </th>
                        <th width="10%" class="text_center">
                            <?php echo JText::_('OPENSHOP_TRANSPOTER_MANAGER'); ?>
                        </th>
                        <th width="10%" class="text_right">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ORDER'), 'a.ordering', $this->lists['order_Dir'], $this->lists['order']); ?>
                            <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'transporter.save_order'); ?>
                        </th>
                        <th width="5%" class="text_center">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th width="3%" class="text_center">
                            
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="11">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $link = JRoute::_('index.php?option=com_openshop&task=transporter.edit&cid[]=' . $row->id);
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        $published = JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', 'transporter.');
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center">
                                <?php echo $this->pagination->getRowOffset($i); ?>
                            </td>
                            <?php
                            if (OpenShopHelper::isJ3()) {
                                ?>
                                <td class="text_center">
                                    <div class="btn-group">
                                        <?php
                                        echo JHtml::_('jgrid.published', $row->published, $i, 'transporter.');
                                        echo $this->addDropdownList(JText::_('OPENSHOP_COPY'), 'copy', $i, 'transporter.copy');
                                        echo $this->addDropdownList(JText::_('OPENSHOP_DELETE'), 'trash', $i, 'transporter.remove');
                                        echo $this->renderDropdownList($this->escape($row->transporter_name));
                                        ?>
                                    </div>
                                </td>
                                <?php
                            }
                            ?>
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo $row->transporter_name; ?></a>
                            </td>			
                            <?php
                            if (!OpenShopHelper::isJ3()) {
                                ?>
                                <td class="text_center">
                                    <?php echo $published; ?>
                                </td>
                                <?php
                            }
                            ?>
                            <td class="text_center">
                                <?php echo $row->transporter_phone; ?>
                            </td>
                            <td class="text_center">
                                <?php echo $row->transporter_email; ?>
                            </td>
                            <td class="text_center">
                                <?php echo $row->transporter_address; ?>
                            </td>
                            <td class="text_center">
                                <?php echo $row->transporter_website == '' ? JText::_('OPENSHOP_NOT_WEBSITE') : $row->transporter_website; ?>
                            </td>
                            <td class="text_center">
                                <?php echo $row->transporter_manager; ?>
                            </td>
                            <td class="order text_right">
                                <span><?php echo $this->pagination->orderUpIcon($i, true, 'transporter.orderup', 'Move Up', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'transporter.orderdown', 'Move Down', $ordering); ?></span>
                                <?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?>				
                                <input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="input-mini" style="text-align: center" <?php echo $disabled; ?> />
                            </td>
                            <td class="text_center">
                                <?php echo $row->id; ?>
                            </td>
                            <td class="text_center">
                                <i class="icon-delete" style="color:red; cursor: pointer;" onclick="delete_trasporter(<?php echo $row->id ?>);"></i>
                            </td>
                        </tr>		
                        <?php
                        $k = 1 - $k;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <!--DELETE TRANSPORTER-->
        <div class="modal fade" id="modal_reason_delete_trasnsporter" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_REASON_DELETE_TRANSPORTER') ?></h3>
                        <div style="float: right;">
                            <button type="button" class="btn btn-danger delete_transporter" onclick="" ><i class="icon-delete"></i> <?php echo JText::_('OPENSHOP_DELETE') ?></button>
                            <button type="button" class="btn btn-default delete_order_cancel" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 reason_delete_transporter_label"><?php echo JText::_('OPENSHOP_SELECT_REASON') ?></label>
                                    <span class="col-md-9">
                                        <?php echo $this->lists['reason_delete_transporter'] ?>
                                        <span class="error-validate error_reason_delete_transporter"></span>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3"><?php echo JText::_('OPENSHOP_SELECT_REASON_DESC') ?></label>
                                    <span class="col-md-9">
                                        <textarea class="form-control reason_delete_order_desc" rows="5" name="reason_delete_transporter_desc" id="reason_delete_transporter_desc" style="width:625px"></textarea>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>