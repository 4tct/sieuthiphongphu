<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
JFactory::getDocument()->addStyleSheet(JURI::base() . '/components/com_openshop/assets/css/product.css');
JFactory::getDocument()->addScript(JUri::base() . '/components/com_openshop/assets/js/product.js');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'product.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        }
</script>
<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <table width="100%" class="adminlist table table-striped" id="tbl-discount-product">
            <thead>
                <tr>
                    <td width="5%"> <?php echo JText::_('OPENSHOP_STT') ?> </td>
                    <td width="20%"> <?php echo JText::_('OPENSHOP_PRODUCT_NAME') ?> </td>
                    <td width="10%"> <?php echo JText::_('OPENSHOP_IMAGE') ?> </td>
                    <td width="10%"> <?php echo JText::_('OPENSHOP_PRICE') ?> </td>
                    <td width="10%"> <?php echo JText::_('OPENSHOP_PRICE_DISCOUNT') ?> </td>
                    <td width="10%"> <?php echo JText::_('OPENSHOP_PERCENT_DISCOUNT') ?> </td>
                    <td width="10%"> <?php echo JText::_('OPENSHOP_START_DATE') ?> </td>
                    <td width="10%"> <?php echo JText::_('OPENSHOP_END_DATE') ?> </td>
                </tr>
            </thead>
            <tbody id="discountProducts">

            </tbody>
        </table>
        <div style="margin-top: 15px; text-align: center;">
            <span class="btn-add-discount" onclick="add_row_discountProducts()"><?php echo JText::_('OPENSHOP_ADD_DISCOUNT') ?></span>
        </div>
        <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="max_value" id="max_value" value="0" />
    </form>
</fieldset>

<div class="modal fade" id="modal_choose_product" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel"><?php echo JText::_('OPENSHOP_CHOOSE_PRODUCT') ?></h3>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="col-md-3" style="margin-top: 8px;text-align: right;"><?php echo JText::_('OPENSHOP_SEARCH_PRODUCT') ?></div>
                    <div class="col-md-5"><input type="text" name="searchProduct" id="searchProduct" class="form-control" placeholder="<?php echo JText::_('OPENSHOP_SEARCH_PRODUCT_DESC'); ?>"/></div>
                    <div class="col-md-4" style="margin-top: 5px;"><span class="btn-searchProduct" onclick="searchProductDiscount()"><?php echo JText::_('OPENSHOP_GO') ?></span></div>
                </div>
                <table class="adminlist table table-striped" id="tbl_choose_product_discount">
                    <thead>
                        <tr>
                            <td width="5%"> <?php echo JText::_('OPENSHOP_STT') ?> </td>
                            <td width="20%"> <?php echo JText::_('OPENSHOP_PRODUCT_NAME') ?> </td>
                            <td width="10%"> <?php echo JText::_('OPENSHOP_IMAGE') ?> </td>
                            <td width="10%"> <?php echo JText::_('OPENSHOP_PRICE') ?> </td>
                            <td width="10%"></td>
                        </tr>
                    </thead>
                    <tbody id="show_product_choose_discount">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

