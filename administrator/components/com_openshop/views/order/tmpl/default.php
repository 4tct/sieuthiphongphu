<?php
defined('_JEXEC') or die;
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/jquery.min.js');
JFactory::getDocument()->addStyleSheet(JURI::base() . '/components/com_openshop/assets/css/order.css');
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/order.js');
JFactory::getDocument()->addScript(JURI::base() . 'components/com_openshop/assets/js/script_admin.js');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'order.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        }
        switch (pressbutton) {
            case 'order_save':
                var rules = [
                    {label: 'cus_name_label', class_input: 'cus_name', cond: 'required'},
                    {label: 'cus_phone_label', class_input: 'cus_phone', cond: 'number'},
                    {label: 'cus_email_label', class_input: 'cus_email', cond: 'required'},
                    {label: 'cus_address_label', class_input: 'cus_address', cond: 'required'}
                ];
                if (validator(rules))
                    btn_save_ajax();
                break;
            case 'order_cancel':
                btn_close_ajax('order.cancel');
                break;
            default:
                break;
        }
    }


</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
    <div class="order"> 
        <div class="panel panel-default box-show-huy">
            <div class="panel-heading panel-heading-edit"><?php echo JText::_('OPENSHOP_ORDER_CUSTOMER_TITLE') ?></div>
            <div class="panel-body panel-body-huy">
                <div class="row-fluid">
                    <div class="span6 offset3">
                        <div class="search_box">
                            <div class="input-group input-group-size">
                                <input type="text" name="search_cus_info" id="search_cus_info" class="form-control search_cus_info" placeholder="<?php echo JText::_('OPENSHOP_ORDER_SEARCH_CUS_INFO') ?>" />
                                <span class="input-group-btn">
                                    <div class="btn btn-default" id="search_cus_button" onclick="check_user();"><span class="icon-search"></span></div>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="span6">
                        <!--SEARCH-->
                        <div class="panel panel-default">
                            <div class="panel-heading ">
                                <?php echo JText::_('OPENSHOP_ORDER_CUSTOMER_INFORMATION') ?>
                                <span style="float: right">
                                    <span class="active_info_cus" id="change_info_shipping" onclick="change_info_payment(0);"><?php echo JText::_('OPENSHOP_ORDER_ADDRESS_SHIPPING') ?></span>
                                    <span class="not_active_info_cus" id="change_info_customer" onclick="change_info_payment(1);"><?php echo JText::_('OPENSHOP_ORDER_ADDRESS_CUSTOMER') ?></span>
                                    <!--0 : save information of payment-->
                                    <!--1 : save information of customer-->
                                    <input type="hidden" name="change_info" id="change_info" value="0" />
                                </span>
                            </div>
                            <div class="panel-body cus responsive-edit">
                                <table class="table-edit" id="abc">
                                    <tr>
                                        <th class="cus_name_label"><?php echo JText::_('OPENSHOP_ORDER_CUS_NAME') ?></th>
                                        <td>
                                            <input type="text" name="payment_fullname" id="cus_name" class="form-control cus_name" value="<?php echo $this->item->payment_fullname; ?>" placeholder="<?php echo JText::_('OPENSHOP_CUSTOMER_NAME') ?>"/>
                                            <span class="error_cus_name error-validate"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="cus_phone_label"><?php echo JText::_('OPENSHOP_ORDER_CUS_PHONE') ?></th>
                                        <td>
                                            <input type="text" name="payment_telephone" id="cus_phone" class="form-control cus_phone" value="<?php echo $this->item->payment_telephone; ?>" onchange="check_input('customers', 'telephone', this.value, 'cus_phone');" placeholder="<?php echo JText::_('OPENSHOP_CUSTOMER_TELEPHONE') ?>"/>
                                            <span class="error_cus_phone error-validate"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="cus_email_label"><?php echo JText::_('OPENSHOP_ORDER_CUS_EMAIL') ?></th>
                                        <td>
                                            <input type="text" name="payment_email" id="cus_email" class="form-control cus_email" value="<?php echo $this->item->payment_email; ?>" onchange="check_input('customers', 'email', this.value, 'cus_email');" placeholder="<?php echo JText::_('OPENSHOP_CUSTOMER_EMAIL') ?>" />
                                            <span class="error_cus_email error-validate"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="cus_address_label"><?php echo JText::_('OPENSHOP_ORDER_CUS_ADDRESS') ?></th>
                                        <td>
                                            <input type="text" name="payment_address" id="cus_address" class="form-control cus_address" value="<?php echo $this->item->payment_address; ?>" placeholder="<?php echo JText::_('OPENSHOP_CUSTOMER_ADDRESS') ?>"/>
                                            <br>
                                            <div class="">
                                                <div class="col-md-5">
                                                    <?php echo $this->lists['payment_zone_id']; ?>
                                                </div>
                                                <div class="col-md-1" id="loading_districts" style="width: 13%;">
                                                    <span id="loading_d" style="display:none;">
                                                        <img src="<?php echo OPENSHOP_ADMIN_PATH_IMG_HTTP; ?>/loading_1.gif" width='46%'>
                                                    </span>
                                                </div>
                                                <div class="col-md-5" id="show_districts">
                                                    <?php echo $this->lists['payment_district_id']; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <!--id customer-->
                                <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $this->item->customer_id ?>"/>
                            </div>
                        </div>
                    </div>

                    <!-- FROM WHERE -->
                    <div class="span3 info_orther">
                        <div class="panel panel-default">
                            <div class="panel-heading"><?php echo JText::_('OPENSHOP_ORDER_FROM_WHERE') ?></div>
                            <div class="panel-body">
                                <div class="row-fluid">
                                    <div class="list-group list-group-edit">
                                        <?php
                                        if (!isset($this->item->buyfrom_id) || $this->item->buyfrom_id == 0)
                                            $a = 'active';
                                        else
                                            $a = '';
                                        ?>
                                        <a class="list-group-item <?php echo $a; ?>" id="list0" onclick="active_buyfrom(0);"><?php echo JText::_('OPENSHOP_ORDER_FROM_WHERE_NONE') ?></a>
                                        <?php
                                        if (count($this->lists['buyfrom'])) {
                                            foreach ($this->lists['buyfrom'] as $k => $v) {
                                                $active_buyfrom = '';
                                                if (isset($this->item->buyfrom_id)) {
                                                    if ($v->id == $this->item->buyfrom_id) {
                                                        $active_buyfrom = 'active';
                                                    }
                                                }
                                                echo '<a class="list-group-item ' . $active_buyfrom . '" id="list' . $v->id . '" onclick="active_buyfrom(' . $v->id . ')">';
                                                echo '<span class="title-buyfromname">' . $v->buyfrom_name . '</span>';
                                                echo '<span class="key-buyfromname"> Alt+' . strtoupper($v->shortcut_key) . '</span> ';
                                                echo '</a>';
                                            }
                                        }
                                        ?>
                                        <!--Id default to active item-->
                                        <input type="text" id="curent_active_list" name="buyfrom_id" value="0" style="display:none;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- CODE -->
                    <div class="span3 info_orther">
                        <div class="panel panel-default">
                            <div class="panel-heading"><?php echo JText::_('OPENSHOP_ORDER_CODE') ?></div>
                            <div class="panel-body">
                                <label for="cus_name"><?php echo JText::_('OPENSHOP_ORDER_CODE_TITLE'); ?></label>
                                <input type="text" class="form-control input-edit" id="code_input" name="code_input" placeholder="<?php echo JText::_('OPENSHOP_ORDER_CODE_INPUT') ?>" />
                                <div id="check_code" class="btn-edit btn-primary-edit" onclick="check_code();">
                                    <?php echo JText::_('OPENSHOP_ORDER_CHECK_CODE'); ?>
                                    <span class="icon_code"></span>
                                </div>
                                <!--information code-->
                                <div id="info_code" class="" style="margin-top: 10px;"></div>
                                <!--id code-->
                                <input type="hidden" name="code_id" id="code_id" value="" />
                            </div>
                        </div>
                    </div>    

                    <!--USERS FOUNDED-->
                    <div class="info_users_search span6" style="display:none;">
                        <div class="span12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo JText::_('OPENSHOP_ORDER_CUS_INFO_SEARCH') ?>
                                    <span class="hidden_user_found"><i class="icon-exit" title="<?php echo JText::_('OPENSHOP_HIDDEN_USER_FOUND') ?>"></i></span>
                                </div>
                                <div class="panel-body responsive-edit">
                                    <table class="table">
                                        <tr style="background-color: bisque;">
                                            <th><?php echo JText::_('OPENSHOP_ORDE_CHOOSE_CUS'); ?></th>
                                            <th><?php echo JText::_('OPENSHOP_ORDER_CUS_NAME_SEARCH'); ?></th>
                                            <th><?php echo JText::_('OPENSHOP_ORDER_CUS_ADDRESS_SEARCH'); ?></th>
                                        </tr>
                                        <tbody class="info_cus_body">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>

        <!--PRODUCT-->

        <div class="panel panel-default box-show-huy">
            <div class="panel-heading panel-heading-edit">
                <?php echo JText::_('OPENSHOP_ORDER_PRODUCT_TITLE') ?>
            </div>
            <div class="panel-body responsive-edit panel-body-huy">
                <div class="row-fluid">
                    <div class="span12" style="background-color: white;">
                        <table class="table table-edit">
                            <thead>
                                <tr>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_N') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_ID') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_NAME') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_IMG') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_SIZE') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_PRICE') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_QUANTITY') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_TOTAL') ?></th>
                                    <th class="center"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_DESC') ?></th>
                                    <th class="center"><?php echo JText::_('') ?></th>
                                </tr>
                            </thead>
                            <tbody id="add_product">
                                <?php
                                $quantity = 0;
                                if (isset($this->lists['order_products'])) {
                                    foreach ($this->lists['order_products'] as $key => $value) {
                                        $i = $key + 1;
                                        $img_name = $value->product_image == '' ? 'no-image_1.png' : $value->product_image;
                                        echo "<tr id='a_" . $i . "'>";
//                                        echo "<td style='width: 5px;'><input type='text' name='stt_" . $i . "' id='stt_" . i . "' value='" . $i . "' disabled='disabled' class='input-num'/></td>";
                                        echo "<td style='width: 35px;' class='center'><span name='stt_" . $i . "' id='stt_" . $i . "'>" . $i . "</span></td>";
                                        echo "<td style='width: 100px;'><input type='text' name='product_id_" . $i . "' id='product_id_" . $i . "' class='input-small' onchange='search_product(" . $i . ");' value='" . $value->product_id . "'/></td>";
                                        echo "<td style='width: 200px;'><span name='product_name_s" . $i . "' id='product_name_s" . $i . "'>" . $value->product_name . "<br/><b>" . $value->product_sku . "</b></span>"
                                        . "<input type='hidden' value='" . $value->product_name . "' name='product_name_" . $i . "' id='product_name_" . $i . "'/></td>";
                                        echo "<td style='width: 100px;'><img src='" . OPENSHOP_PATH_IMG_PRODUCT_HTTP . $img_name . "' alt='image' name='img_product_" . $i . "' id='img_product_" . $i . "'/></td>";
//                                        check manage size & color
                                        $disabled = '';
                                        if (OpenShopHelper::getConfigValue('managesizecolor') != '2') {
                                            $disabled = 'disabled';
                                        }
                                        echo "<td style='width: 14%;' class=''>" .
                                        $this->getSizeColorList($i, $value->product_id, $value->product_size, 'size', '') .
                                        $this->getSizeColorList($i, $value->product_id, $value->product_color, 'color', $disabled) .
                                        $this->getWeightList($i, $value->optionweight_id) .
                                        $this->getHeightList($i, $value->optionheight_id) .
                                        "</td>";
                                        echo "<td style='width: 100px;'><input type='text' name='product_price_" . $i . "' id='product_price_" . $i . "' class='input-small' readonly='readonly' value='" . (float) $value->price . "' /></td>";
                                        echo "<td style='width: 100px;'><input type='text' name='product_quantity_" . $i . "' id='product_quantity_" . $i . "' class='input-small' onchange=\"total_price(" . $i . ");\" value='" . $value->quantity . "' /></td>";
                                        echo "<td style='width: 100px;'><input type='text' name='product_total_" . $i . "' id='product_total_" . $i . "' class='input-small' readonly='readonly' value='" . (float) $value->total_price . "' /></td>";
                                        echo "<td style='width: 250px;'><textarea rows='3' cols='50' name='order_product_desc_" . $i . "' id='order_product_desc_" . $i . "'>" . $value->order_product_desc . "</textarea></td>";
                                        echo '<td><div id="remove_tr_' . $i . '" class="btn-edit btn-not-execute">Xóa</div></td>';
                                        echo "<td style='display: none;'><input type='text' name='product_sku_" . $i . "' id='product_sku_" . $i . "' class='input-small' value='" . $value->product_sku . "' /></td>";
                                        echo "</tr>";
                                        $quantity += $value->quantity;
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6" class=""><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_TOTAL') ?></td>
                                    <td class="total_quantity"><?php echo $quantity; ?></td>
                                    <td colspan="3" class="total_total"><?php echo isset($this->item->total) ? (float) $this->item->total : 0 ?></td>
                                    <!--total but not discount-->
                                    <td style="display: none;"><input type="hidden"  name="total" value="<?php echo isset($this->item->total) ? $this->item->total : 0; ?>" id="total_payable"/></td>  
                                </tr>
                                <tr>
                                    <td colspan="7" class=""><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_DISCOUNT') ?></td>
                                    <td colspan="3" class="">- <input type="text" class="input-small" name="discount" id="discount" value="<?php echo isset($this->item->discount) ? $this->item->discount : 0; ?>" onchange="total_price_not_discount();"/></td>
                                </tr>
                                <tr>
                                    <td colspan="7" class=""><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_TOTAL_PRICE_BILL') ?></td>
                                    <td colspan="3" class="money_payable"><?php echo isset($this->item->total) ? (float) $this->item->total : 0 - isset($this->item->discount) ? $this->item->discount : 0 ?></td>
                                </tr>
                            </tfoot>
                        </table>
                        <br />
                        <!--PRODUCTS SEARCHED-->
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading"><?php echo JText::_('OPENSHOP_ORDER_PRODUCT_SEARCH') ?></div>
                                <div class="panel-body">
                                    <table class="table table-edit product-searched">
                                    </table>
                                </div>
                            </div>
                        </div>    
                        <br>
                        <div class="position-center">
                            <div class="btn-edit btn-primary-edit btn-width" id="add_new_product" onclick="add_new_product()"> <span class="icon-save-new"></span> <?php echo JText::_('OPENSHOP_ORDER_ADD_NEW_PRODUCT') ?> ( Ctrl+Space )</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--ORDERS OLD-->

        <?php
        $app = JFactory::getApplication()->input;
        $customer_id = $app->getInt('customer_id');
        if (isset($customer_id)) {
            ?>
            <div class="panel panel-default box-show-huy">
                <div class="panel-heading panel-heading-edit"><?php echo JText::_('OPENSHOP_ORDER_ORDER_OLD') ?></div>
                <div class="panel-body responsive-edit panel-body-huy">
                    <div id="editcell">
                        <table class="adminlist table table-striped" style="vertical-align: middle;">
                            <thead>
                                <tr>
                                    <th width="2%" class="text_center">
                                        <?php echo JText::_('OPENSHOP_NUM'); ?>
                                    </th>
                                    <th class="text_center" width="15%">
                                        <?php echo JText::_('OPENSHOP_CUSTOMER') ?>
                                    </th>
                                    <th class="text_center"  width="10%">
                                        <?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
                                    </th>
                                    <th class="text_center"  width="10%">
                                        <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                                    </th>
                                    <th class="text_center" width="10%">
                                        <?php echo JText::_('OPENSHOP_ORDER_SIZE') ?>
                                    </th>
                                    <th class="text_center" width="6%">
                                        <?php echo JText::_('OPENSHOP_QUANTITY'); ?>
                                    </th>
                                    <th class="text_center" width="10%">
                                        <?php echo JText::_('OPENSHOP_PRICE'); ?>
                                    </th>
                                    <th class="text_center" width="10%">
                                        <?php echo JText::_('OPENSHOP_CREATED_DATE') ?>
                                    </th>
                                    <th class="text_center" width="10%">
                                        <?php echo JText::_('OPENSHOP_DESCRIPTION') ?>
                                    </th>
                                    <th class="text_center" width="10%">
                                        <?php echo JText::_('OPENSHOP_ORDER_STATUS'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $k = 0;
                                $stt = 0;
                                $hide_show = 0;
                                $rows_product = $this->getOrderOlds($customer_id);
//                                print_r($rows_product);
                                foreach ($rows_product as $key => $row) {
                                    $id = $row->id;
                                    $rowspan = $this->getRowProductInOrder($id);
                                    $phone = substr($row->payment_telephone, 0, 4) . ' ' . substr($row->payment_telephone, 4, 3) . ' ' . substr($row->payment_telephone, 7, (int) (strlen($row->payment_telephone) - 7));
                                    ?>
                                    <tr class="<?php echo "row$k"; ?>">
                                        <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                            <?php echo ($stt + 1); ?> 
                                        </td>
                                        <td class="text_left" rowspan="<?php echo $rowspan ?>">

                                            <b><?php echo $row->payment_fullname ?></b><br>

                                            ( <b><?php echo $phone; ?></b> )
                                            <br>
                                            <span><?php echo $row->payment_address ?></span>
                                            <br>
                                            <!--<span><b>[ <?php echo $row->zone_name; ?> - <?php echo $row->district_name; ?> ]</b></span>-->
                                        </td>
                                        <?php
                                        $row_pro = array();
                                        foreach ($this->productListItem($id) as $row_product) {
                                            array_push($row_pro, $row_product);
                                        }
                                        foreach ($this->productListItem($id) as $row_product) {
                                            //separating the phone number
                                            ?>
                                            <td class="text_center">
                                                <span><?php echo $row_product->product_name; ?></span>
                                            </td>
                                            <td class="text_center">
                                                <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row_product->product_image; ?>" alt="Product Image" width="90px" title="<?php echo $row_product->product_name ?>"/>
                                            </td>
                                            <td class="text_left">
                                                <?php echo $this->getSizeColor($row_product->product_id, $row_product->product_size, 'size'); ?>
                                                <?php echo $this->getSizeColor($row_product->product_id, $row_product->product_color, 'color'); ?>
                                                <?php echo $this->getWeight($row_product->optionweight_id, 'weight'); ?>
                                                <?php echo $this->getWeight($row_product->optionheight_id, 'height'); ?>
                                            </td>
                                            <td class="text_center">
                                                <?php echo $row_product->quantity; ?>
                                            </td>
                                            <td class="text_center">
                                                <?php echo (float) $row_product->price; ?>
                                            </td>
                                            <td class="text_center">
                                                <?php
                                                if ($row->created_date != $this->nullDate) {
                                                    echo JHtml::_('date', $row->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y'));
                                                }
                                                ?>
                                            </td>
                                            <td class="text_center">
                                                <textarea rows="4" style="width:120px;"><?php echo $row_product->order_product_desc; ?></textarea>
                                            </td>
                                            <?php
                                            if ($hide_show == 0) {
                                                ?>
                                                <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                                    <?php
                                                    $t = OpenShopHelper::getOrderStatusName($row->order_status_id, JComponentHelper::getParams('com_languages')->get('site', 'en-GB'));
                                                    ?>
                                                    <span class="border_background_<?php echo strtolower($t); ?>">
                                                        <?php echo $t; ?>
                                                    </span>
                                                </td>
                                                <?php
                                                $hide_show = 1;
                                            }
                                            ?>
                                        </tr>		
                                        <?php
                                    }
                                    $hide_show = 0;
                                    $k = 1 - $k;
                                    $stt += 1;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!--/-->
                </div>
            </div>
            <?php
        }
        ?>


    </div>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="max_value" id="max_value" value="<?php echo isset($this->item->max_value) ? $this->item->max_value : 0 ?>"/>
    <input type="hidden" name="cid[]" id="cid" value="<?php echo $this->item->id; ?>" />
    <input type="hidden" name="manageSizeColor" id="manageSizeColor" value="<?php echo OpenShopHelper::getConfigValue('managesizecolor') ?>" />
    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
</form>

<div class="modal fade" tabindex="-1" role="dialog" id="loading_modal">
    <div class="modal-dialog" style="padding-top: 200px;">
<!--        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>-->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
