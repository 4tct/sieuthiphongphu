<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewOrder extends OpenShopViewForm {

    function _buildListArray(&$lists, $item) {
        $db = JFactory::getDbo();
        //Customer list
        $query = $db->getQuery(true);
        $query->select('c.customer_id AS value, u.name AS text')
                ->from('#__openshop_customers AS c')
                ->innerJoin('#__users AS u ON (c.customer_id = u.id)')
                ->where('c.published = 1')
                ->where('u.block = 0');
        $db->setQuery($query);
        $lists['customer_id'] = JHtml::_('select.genericlist', $db->loadObjectList(), 'customer_id', array(
                    'option.text.toHtml' => false,
                    'option.text' => 'text',
                    'option.value' => 'value',
                    'list.attr' => ' class="inputbox chosen" ',
                    'list.select' => isset($item->customer_id) ? $item->customer_id : ''));
        //Customergroup list
        $query->clear();
        $query->select('a.id AS value, b.customergroup_name AS text')
                ->from('#__openshop_customergroups AS a')
                ->innerJoin('#__openshop_customergroupdetails AS b ON (a.id = b.customergroup_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
                ->order('b.customergroup_name');
        $db->setQuery($query);
        $lists['customergroup_id'] = JHtml::_('select.genericlist', $db->loadObjectList(), 'customergroup_id', array(
                    'option.text.toHtml' => false,
                    'option.text' => 'text',
                    'option.value' => 'value',
                    'list.attr' => ' class="inputbox chosen form-control" ',
                    'list.select' => isset($item->customergroup_id) ? $item->customergroup_id : ''));
        //Order products list
        $query->clear();
        if (isset($item->id))
            $id = (int) $item->id;
        else
            $id = '0';
        $query->select('*')
                ->from('#__openshop_orderproducts')
                ->where('order_id = ' . $id);
        $db->setQuery($query);
        $orderProducts = $db->loadObjectList();
        for ($i = 0; $n = count($orderProducts), $i < $n; $i++) {
            $query->clear();
            $query->select('*')
                    ->from('#__openshop_orderoptions')
                    ->where('order_product_id = ' . intval($orderProducts[$i]->id));
            $db->setQuery($query);
            $orderProducts[$i]->options = $db->loadObjectList();
        }
        $lists['order_products'] = $orderProducts;
        //BuyFrom
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_buyfroms'))
                ->where('published = 1');
        $db->setQuery($query);
        $lists['buyfrom'] = $db->loadObjectList();
        //product
        if (isset($item->id)) {
            $query->clear();
            $query->select('pro.product_image,opro.*')
                    ->from($db->quoteName('#__openshop_orderproducts', 'opro'))
                    ->join('LEFT', $db->quoteName('#__openshop_products', 'pro') . ' ON pro.id = opro.product_id')
                    ->where('opro.order_id = ' . $item->id);
            $db->setQuery($query);
            $lists['order_products'] = $db->loadObjectList();
        }
        //max value product
        if (isset($item->id)) {
            $query->clear();
            $query->select('count(id)')
                    ->from($db->quoteName('#__openshop_orderproducts'))
                    ->where('order_id = ' . $item->id);
            $db->setQuery($query);
            $item->max_value = $db->loadResult();
        }
        //Order status
        $query->clear();
        $query->select('a.id, b.orderstatus_name')
                ->from('#__openshop_orderstatuses AS a')
                ->innerJoin('#__openshop_orderstatusdetails AS b ON (a.id = b.orderstatus_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
        $db->setQuery($query);
        $lists['order_status_id'] = JHtml::_('select.genericlist', $db->loadObjectList(), 'order_status_id', ' class="inputbox" ', 'id', 'orderstatus_name', isset($item->order_status_id) ? $item->order_status_id : '');

        //Payment and Shipping country, zone list
        $lists['payment_zone_id'] = JHtml::_('select.genericlist', $this->_getZoneList(isset($item->payment_country_id) ? $item->payment_country_id : ''), 'payment_zone_id', ' class="inputbox form-control" onchange="check_town();"', 'id', 'zone_name', isset($item->payment_zone_id) ? $item->payment_zone_id : '');
//        $lists['shipping_zone_id'] = JHtml::_('select.genericlist', $this->_getZoneList(isset($item->payment_country_id) ? $item->payment_country_id : ''), 'shipping_zone_id', ' class="inputbox form-control" ', 'id', 'zone_name', $item->shipping_zone_id);
        $lists['payment_district_id'] = JHtml::_('select.genericlist', $this->_getDistrict(), 'payment_district_id', 'class="inputbox form-control"', 'id', 'district_name', $item->payment_district_id);

        $currency = new OpenShopCurrency();

        //Form for billing and shipping address
//        $fields = array_keys($db->getTableColumns('#__openshop_orders'));
//        $data = array();
//        foreach ($fields as $field) {
////            $data[$field] = $item->{$field};
//        }
//        $personalFields = array(
//            'email',
//            'telephone',
//            'fax'
//        );
//        $billingFields = OpenShopHelper::getFormFields('B');
//        $shippingFields = OpenShopHelper::getFormFields('S');
//        for ($i = 0, $n = count($billingFields); $i < $n; $i++) {
//            if (in_array($billingFields[$i]->name, $personalFields)) {
//                unset($billingFields[$i]);
//            }
//        }
//        for ($i = 0, $n = count($shippingFields); $i < $n; $i++) {
//            if (in_array($shippingFields[$i]->name, $personalFields)) {
//                unset($shippingFields[$i]);
//            }
//        }
//        foreach ($billingFields as $field) {
//            $field->name = 'payment_' . $field->name;
//        }
//        $billingForm = new RADForm($billingFields);
//        $billingForm->bind($data);
//        foreach ($shippingFields as $field) {
//            $field->name = 'shipping_' . $field->name;
//        }
//        $shippingForm = new RADForm($shippingFields);
//        $shippingForm->bind($data);
//
//        $paymentCountryId = $item->payment_country_id;
//        $paymentZoneId = (int) $item->payment_zone_id;
//        if ($paymentCountryId) {
//            $countryField = $billingForm->getField('payment_country_id');
//            if ($countryField instanceof RADFormFieldCountries) {
//                $countryField->setValue($paymentCountryId);
//            }
//        }
//        $zoneField = $billingForm->getField('payment_zone_id');
//        if ($zoneField instanceof RADFormFieldZone) {
//            $zoneField->setCountryId($paymentCountryId);
//            if ($paymentZoneId) {
//                $zoneField->setValue($paymentZoneId);
//            }
//        }
//
//
//        $shippingCountryId = $item->shipping_country_id;
//        $shippingZoneId = (int) $item->shipping_zone_id;
//        if ($shippingCountryId) {
//            $countryField = $shippingForm->getField('shipping_country_id');
//            if ($countryField instanceof RADFormFieldCountries) {
//                $countryField->setValue($shippingCountryId);
//            }
//        }
//        $zoneField = $shippingForm->getField('shipping_zone_id');
//        if ($zoneField instanceof RADFormFieldZone) {
//            $zoneField->setCountryId($shippingCountryId);
//            if ($shippingZoneId) {
//                $zoneField->setValue($shippingZoneId);
//            }
//        }
//
//
//        $this->billingForm = $billingForm;
//        $this->shippingForm = $shippingForm;
        $this->currency = $currency;
        $nullDate = $db->getNullDate();
        $this->nullDate = $nullDate;
    }

    /**
     *
     * Private method to get Country Options
     * @param array $lists
     */
    function _getCountryOptions() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id, country_name AS name')
                ->from('#__openshop_countries')
                ->where('published = 1')
                ->order('country_name');
        $db->setQuery($query);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_PLEASE_SELECT'), 'id', 'name');
        $options = array_merge($options, $db->loadObjectList());
        return $options;
    }

    /**
     *
     * Private method to get Zone Options
     * @param array $lists
     */
    function _getZoneList($countryId = '') {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id, zone_name')
                ->from('#__openshop_zones')
//			->where('country_id = ' . (int) $countryId)
                ->where('published = 1');
        $db->setQuery($query);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_PLEASE_SELECT_ZONE'), 'id', 'zone_name');
        $options = array_merge($options, $db->loadObjectList());
        return $options;
    }

    function _getDistrict() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id, district_name')
                ->from($db->quoteName('#__openshop_districts'))
                ->where('published = 1');
        $db->setQuery($query);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_PLEASE_SELECT_DISTRICT'), 'id', 'district_name');
        $options = array_merge($options, $db->loadObjectList());
        return $options;
    }

    /**
     * Override Build Toolbar function, only need Save, Save & Close and Close
     */
    function _buildToolbar() {
        $viewName = $this->getName();
        $view = OpenShopInflector::pluralize($viewName);
        $text = JText::_($this->lang_prefix . '_EDIT');
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . $viewName) . ': <small><small class="title-product">[ ' . $text . ' ]</small></small>');
        if (OpenShopHelper::getPermissionAction($view, 'create')) {
            JToolbarHelper::custom('order_save', 'apply', 'apply', JText::_('OPENSHOP_SAVE'), false);
            JToolBarHelper::save($viewName . '.save');
        }
        if (OpenShopHelper::getConfigValue('invoice_enable')){
            JToolBarHelper::custom('order.downloadInvoice', 'print', 'print', Jtext::_('OPENSHOP_DOWNLOAD_INVOICE'), false);
        }
        JToolbarHelper::custom('order_cancel', 'delete', 'delete', JText::_('JTOOLBAR_CLOSE'), false);
    }

    /*
     * $i               : position
     * $id_product      : id of product 
     * $size_id         : if data not empty => get size
     */

    public static function getSizeColorList($i, $id_product, $id_cond, $cond, $manaSizeColor = null) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('distinct a.optionvalue_id as value, a.value as text')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'));
        $options = array();
        switch ($cond) {
            case 'size':
                $query->join('LEFT', $db->quoteName('#__openshop_inventories', 'b') . 'ON a.optionvalue_id = b.idsize')
                        ->where('b.product_id = ' . $id_product);
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'), 'value', 'text');
                $t_id = 'product_size_';
                $disabled = '';
                break;
            case 'color':
                $query->join('LEFT', $db->quoteName('#__openshop_inventories', 'b') . 'ON a.optionvalue_id = b.idcolor')
                        ->where('b.product_id = ' . $id_product);
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'), 'value', 'text');
                $t_id = 'product_color_';
                $disabled = '';
                if(!empty($manaSizeColor)){
                    $disabled = 'disabled';
                }
                break;
            default:
                break;
        }
       

        $db->setQuery($query);
        $objects = $db->loadObjectList();
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->value, $object->text);
        }
        return JHtml::_('select.genericlist', $options, $t_id . $i, 'class="inputbox" '. $disabled .' style="width: 70px"', "value", "text", $id_cond);
    }

    /*
     * $i               : position
     * $id_product      : id of product 
     * $size_id         : if data not empty => get weight
     */

    public static function getWeightList($i, $weight_id = 0) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('opv.id AS value,opv.value AS text')
                ->from($db->quoteName('#__openshop_optiondetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'opv') . 'ON opv.option_id = opd.option_id')
                ->where('opd.option_name = "WEIGHT"');
        $db->setQuery($query);
        $objects = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'), 'value', 'text');
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->value, $object->text);
        }

        return JHtml::_('select.genericlist', $options, 'optionweight_id_' . $i, 'class="inputbox" style="width: 120px" ', "value", "text", $weight_id);
    }

    /*
     * $i               : position
     * $id_product      : id of product 
     * $size_id         : if data not empty => get height
     */

    public static function getHeightList($i, $height_id = 0) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('opv.id AS value,opv.value AS text')
                ->from($db->quoteName('#__openshop_optiondetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'opv') . 'ON opv.option_id = opd.option_id')
                ->where('opd.option_name = "HEIGHT"');
        $db->setQuery($query);
        $objects = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'), 'value', 'text');
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->value, $object->text);
        }

        return JHtml::_('select.genericlist', $options, 'optionheight_id_' . $i, 'class="inputbox" style="width: 120px" ', "value", "text", $height_id);
    }

    public static function getOrderOlds($id_cus) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_orders'))
                ->where('customer_id = ' . $id_cus);
        return $db->setQuery($query)->loadObjectList();
    }

    function getRowProductInOrder($id_order) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(True);

        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('order_id = ' . $id_order);
        $db->setQuery($query);
        return (int) ($db->loadResult());
    }

    public function productListItem($i) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('ord.*, pro.product_image')
                ->from($db->quoteName('#__openshop_orderproducts', 'ord'))
                ->join('LEFT', $db->quoteName('#__openshop_products', 'pro') . 'ON ord.product_id = pro.id')
                ->where('order_id = ' . $i);
        $db->setQuery($query);

        return $db->loadObjectList();
    }

    public function getSizeColor($i_pro, $id_cond, $cond) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->clear();
        $query->select('opd.value AS text, opd.id AS value')
                ->from($db->quoteName('#__openshop_productoptionvalues', 'prov'))
                ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'opd') . 'ON prov.option_value_id = opd.optionvalue_id')
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'optd') . 'ON opd.option_id = optd.option_id')
                ->where('prov.product_id = ' . $i_pro);

        $options = array();
        switch ($cond) {
            case 'size':
                $query->where('optd.option_name = "SIZE"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $t_id = 'product_size';
                $t_witdh = '50px';
                break;
            case 'color':
                $query->where('optd.option_name = "COLOR"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $t_id = 'product_color';
                $t_witdh = '70px';
                break;
            default :
                break;
        }

        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" style="width: ' . $t_witdh . '; ', 'value', 'text', $id_cond);
        return $lists;
    }

    public function getWeight($id_cond, $cond) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('opd.value AS text, opd.id AS value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'optd') . 'ON opd.option_id = optd.option_id');

        $options = array();
        switch ($cond) {
            case 'weight':
                $query->where('optd.option_name = "WEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $t_id = 'optionweight_id';
                break;
            case 'height':
                $query->where('optd.option_name = "HEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $t_id = 'optionheight_id';
                break;
            default:
                break;
        }
        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" style="width: 120px; ', 'value', 'text', $id_cond);
        return $lists;
    }

}
