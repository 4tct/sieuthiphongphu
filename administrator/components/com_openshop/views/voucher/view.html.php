<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewVoucher extends OpenShopViewForm
{
	function _buildListArray(&$lists, $item)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$nullDate = $db->getNullDate();
		//Get history voucher
		$query->clear();
		$query->select('*')
			->from('#__openshop_voucherhistory')
			->where('voucher_id = ' . intval($item->id));
		$db->setQuery($query);
		$voucherHistories = $db->loadObjectList();
		$this->voucherHistories = $voucherHistories;
		$this->lists = $lists;
		$this->nullDate = $nullDate;
	}
}