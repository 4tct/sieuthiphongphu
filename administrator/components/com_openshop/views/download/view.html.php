<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewDownload extends OpenShopViewForm
{
	
	function _buildListArray(&$lists, $item)
	{
		jimport('joomla.filesystem.folder');
		$files = JFolder::files(JPATH_ROOT . '/media/com_openshop/downloads');
		sort($files);
		$options = array();
		$options[] = JHtml::_('select.option', '', JText::_('OPENSHOP_NONE'));
		for ($i = 0, $n = count($files); $i < $n; $i++)
		{
			$file = $files[$i];
			$options[] = JHtml::_('select.option', $file, $file);
		}
		$lists['existed_file'] = JHtml::_('select.genericlist', $options, 'existed_file', 'class="inputbox advselect"', 'value', 'text', $item->filename);
		parent::_buildListArray($lists, $item);
	}
}