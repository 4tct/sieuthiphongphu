<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewTaxrates extends OpenShopViewList
{
	function _buildListArray(&$lists, $state)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id, geozone_name')
			->from('#__openshop_geozones')
			->where('published=1')
			->order('geozone_name');
		$db->setQuery($query);
		$options = array();
		$options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_GEOZONE'), 'id', 'geozone_name');
		$options = array_merge($options, $db->loadObjectList());
		$lists['geozone_id'] = JHtml::_('select.genericlist', $options, 'geozone_id', ' class="inputbox" onchange="submit();" ', 'id', 'geozone_name', 
			$state->geozone_id);
		return true;
	}
}