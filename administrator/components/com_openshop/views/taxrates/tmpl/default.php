<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
?>
<fieldset class="adminfs">
<form action="index.php?option=com_openshop&view=taxrates" method="post" name="adminForm" id="adminForm">
	<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'OPENSHOP_FILTER' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
				<button onclick="this.form.submit();" class="btn"><?php echo JText::_( 'OPENSHOP_GO' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();" class="btn"><?php echo JText::_( 'OPENSHOP_RESET' ); ?></button>		
			</td>
			<td align="right">
				<?php echo $this->lists['geozone_id']; ?>
			</td>
		</tr>
	</table>
	<div id="editcell">
		<table class="adminlist table table-striped">
			<thead>
				<tr>
					<th width="2%" class="text_center">
						<?php echo JText::_( 'OPENSHOP_NUM' ); ?>
					</th>			
					<th width="2%" class="text_center">
						<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
					</th>
					<?php
					if (OpenShopHelper::isJ3())
					{
						?>
						<th width="1%" class="text_center" style="min-width:55px">
							<?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order'] ); ?>
						</th>
                    	<?php
					}
					?>
					<th class="text_left" width="20%">
						<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_TAX_NAME'), 'a.tax_name', $this->lists['order_Dir'], $this->lists['order'] ); ?>
					</th>	
					<th class="text_center" width="10%">
						<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_TAX_RATE'), 'a.tax_rate', $this->lists['order_Dir'], $this->lists['order'] ); ?>				
					</th>														
					<th class="text_center" width="10%">
						<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_TAX_TYPE'), 'a.tax_type', $this->lists['order_Dir'], $this->lists['order'] ); ?>				
					</th>		
					<th class="text_center" width="15%">
						<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_GEO_ZONE'), 'b.geozone_name', $this->lists['order_Dir'], $this->lists['order'] ); ?>
					</th>	
					<th class="text_center" width="10%">
						<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_DATE_ADDED'), 'a.created_date', $this->lists['order_Dir'], $this->lists['order'] ); ?>				
					</th>														
					<th class="text_center" width="10%">
						<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_DATE_MODIFIED'), 'a.modified_date', $this->lists['order_Dir'], $this->lists['order'] ); ?>				
					</th>
					<?php
					if (!OpenShopHelper::isJ3())
					{
						?>
						<th width="5%" class="text_center">
							<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_PUBLISHED'), 'a.published', $this->lists['order_Dir'], $this->lists['order'] ); ?>
						</th>
						<?php
					}
					?>
					<th width="5%" class="text_center">
						<?php echo JHtml::_('grid.sort',  JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order'] ); ?>
					</th>													
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="10">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0;
			for ($i=0, $n=count( $this->items ); $i < $n; $i++)
			{
				$row = &$this->items[$i];
				$link 	= JRoute::_( 'index.php?option=com_openshop&task=taxrate.edit&cid[]='. $row->id);
				$checked 	= JHtml::_('grid.id',   $i, $row->id );				
				$published 	= JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', 'taxrate.' );			
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td class="text_center">
						<?php echo $this->pagination->getRowOffset( $i ); ?>
					</td>
					<td class="text_center">
						<?php echo $checked; ?>
					</td>
					<?php
					if (OpenShopHelper::isJ3())
					{
						?>
						<td class="text_center">
							<div class="btn-group">
								<?php
								echo JHtml::_('jgrid.published', $row->published, $i, 'taxrate.');
								echo $this->addDropdownList(JText::_('OPENSHOP_COPY'), 'copy', $i, 'taxrate.copy');
								echo $this->addDropdownList(JText::_('OPENSHOP_DELETE'), 'trash', $i, 'taxrate.remove');
								echo $this->renderDropdownList($this->escape($row->tax_name));
								?>
							</div>
						</td>
						<?php
					}
					?>
					<td>
						<a href="<?php echo $link; ?>"><?php echo $row->tax_name; ?></a>				
					</td>	
					<td class="text_center">
						<?php echo number_format($row->tax_rate, 2); ?>
					</td>	
					<td class="text_center">
						<?php
						if ($row->tax_type=='P') { 
							echo JText::_('OPENSHOP_PERCENTAGE'); 
						} elseif ($row->tax_type=='F'){ 
							echo JText::_('OPENSHOP_FIXED_AMOUNT');
						}
						?>
					</td>
					<td class="text_center">
						<?php echo $row->geozone_name; ?>
					</td>
					<td class="text_center">
						<?php echo JHtml::_('date', $row->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y')); ?>
					</td>	
					<td class="text_center">
						<?php echo JHtml::_('date', $row->modified_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y')); ?>
					</td>
					<?php
					if (!OpenShopHelper::isJ3())
					{
						?>
						<td class="text_center">
							<?php echo $published; ?>
						</td>
						<?php
					}
					?>
					<td class="text_center">
						<?php echo $row->id; ?>
					</td>
				</tr>		
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
		</table>
	</div>
	<input type="hidden" name="option" value="com_openshop" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
	<?php echo JHtml::_( 'form.token' ); ?>			
</form>
</fieldset>