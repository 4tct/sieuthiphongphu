<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
OpenShopHelper::chosen();
$editor = JFactory::getEditor();
$translatable = JLanguageMultilang::isEnabled() && count($this->languages) > 1;
?>
<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton)
    {
        var form = document.adminForm;
        if (pressbutton == 'buyfrom.cancel') {
            Joomla.submitform(pressbutton, form);
            return;
        } else {
            //Validate the entered data before submitting
<?php
if ($translatable) {
    foreach ($this->languages as $language) {
        $langId = $language->lang_id;
        ?>
                    if (document.getElementById('buyfrom_name_<?php echo $langId; ?>').value == '') {
                        alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                        document.getElementById('buyfrom_name_<?php echo $langId; ?>').focus();
                        return;
                    }
        <?php
    }
} else {
    ?>
                if (form.buyfrom_name.value == '') {
                    alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
                    form.buyfrom_name.focus();
                    return;
                }
    <?php
}
?>
            Joomla.submitform(pressbutton, form);
        }
    }
</script>
<fieldset class="adminfs">
    <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <div class="row-fluid">
            <div class="span10">
                <table class="admintable adminform" style="width: 100%;">
                    <tr>
                        <td class="key">
                            <span class="required">*</span>
                            <?php echo JText::_('OPENSHOP_NAME'); ?>
                        </td>
                        <td>
                            <input class="input-xlarge" type="text" name="buyfrom_name" id="buyfrom_name" size="" maxlength="250" value="<?php echo $this->item->buyfrom_name; ?>" />
                        </td>								
                    </tr>	
                    <tr>
                        <td class="key">
                            <?php echo JText::_('OPENSHOP_SHORTCUT_KEY'); ?>
                        </td>
                        <td>
                            <input class="input-xlarge" type="text" name="shortcut_key" id="shortcut_key" size="" maxlength="250" value="<?php echo $this->item->shortcut_key; ?>" />
                        </td>								
                    </tr>
                    <tr>
                        <td class="key">
                            <?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
                        </td>
                        <td>
                            <?php echo $this->lists['published']; ?>
                        </td>							
                    </tr>
                    <tr>
                        <td class="key">
                            <?php echo JText::_('OPENSHOP_DESCRIPTION'); ?>
                        </td>
                        <td>
                            <?php echo $editor->display('buyfrom_description', $this->item->buyfrom_description, '100%', '250', '75', '10'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
        <input type="hidden" name="task" value="" />
    </form>
</fieldset>