<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addStyleSheet(JURI::base() . '/components/com_openshop/assets/css/order.css');
JFactory::getDocument()->addStyleSheet('/media/jui/css/chosen.css');

JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/jquery.form.js');
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/order.js');
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/openshop.js');
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/cart.js');
JFactory::getDocument()->addScript('/media/jui/js/chosen.jquery.min.js');
?>

<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton) {
        switch (pressbutton) {
            case 'cart_export':
                show_modal_export();
                break;
            default:
                break;
        }
    }
</script>

<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=orders" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>		
                </td>
                <td align="right">
                    <?php echo $this->lists['order_status_id']; ?>
                </td>	
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped" style="vertical-align: middle;">
                <thead>
                    <tr>
                        <th width="2%" class="text_center">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_center" width="17%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_CUSTOMER'), 'a.payment_fullname', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_center"  width="15%">
                            <?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
                        </th>
                        <th class="text_center"  width="10%">
                            <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                        </th>
                        <th class="text_center" width="11%">
                            <?php echo JText::_('OPENSHOP_ORDER_SIZE') ?>
                        </th>
                        <th class="text_center" width="8%" title="<?php echo JText::_('OPENSHOP_QUANTITY_X_PRICE') ?>">
                            <?php echo JText::_('OPENSHOP_PRICE'); ?>
                        </th>
                        <th class="text_center" width="8%" title="<?php echo JText::_('OPENSHOP_QUANTITY_WAREHOUSE') ?>">
                            <?php echo JText::_('OPENSHOP_QUANLITY_WAREHOUSE'); ?>
                        </th>
                        <th class="text_center" width="8%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_CREATED_DATE'), 'a.created_date', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_DESCRIPTION') ?>
                        </th>
                        <th class="text_center" width="11%">
                            <?php echo JText::_('OPENSHOP_ORDER_STATUS'); ?>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="11">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    $stt = 0;
                    $hide_show = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $row->link_more_product = JRoute::_('index.php?option=com_openshop&task=order.edit&cid[]=' . $row->id);
                        $link = JRoute::_('index.php?option=com_openshop&task=order.edit&cid[]=' . $row->id);
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        $rowspan = $this->getRowProductInOrder($row->id);
                        $phone = substr($row->payment_telephone, 0, 4) . ' ' . substr($row->payment_telephone, 4, 3) . ' ' . substr($row->payment_telephone, 7, (int) (strlen($row->payment_telephone) - 7));
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                <?php echo ($stt + 1); ?> 
                            </td>
                            <td class="text_left" rowspan="<?php echo $rowspan ?>">
                                <?php
                                if (strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'PENDING') {
                                    echo '<a href="' . $link . '">' . $row->payment_fullname . '</a><br>';
                                } else {
                                    echo '<b>' . $row->payment_fullname . '</b><br>';
                                }
                                ?>
                                ( <b><?php echo $phone; ?></b> )
                                <br>
                                <span><?php echo $row->payment_address ?></span>
                                <br>
                                <span><b>[ <?php echo $row->zone_name; ?> - <?php echo $row->district_name; ?> ]</b></span>
                            </td>
                            <?php
                            $row_pro = array();
                            foreach ($this->productListItem($row->id) as $row_product) {
                                array_push($row_pro, $row_product);
                            }
                            foreach ($this->productListItem($row->id) as $row_product) {
                                //separating the phone number
                                ?>
                                <td class="text_center">
                                    <span>
                                        <?php echo $row_product->product_name; ?>
                                        <br>
                                        <b> <?php echo $row_product->product_sku; ?> </b>
                                    </span>
                                </td>
                                <td class="text_center">
                                    <?php
                                    if (empty($row_product->product_image)) {
                                        $img_product = 'no-image_1.png';
                                    } else {
                                        $img_product = $row_product->product_image;
                                    }
                                    ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $img_product; ?>" alt="Product Image" width="90px" title="<?php echo $img_product ?>"/>
                                </td>
                                <td class="text_left">
                                    <?php echo $this->getSizeColor($row_product->product_id, $row_product->product_size, 'size'); ?>
                                    <?php echo $this->getSizeColor($row_product->product_id, $row_product->product_color, 'color'); ?>
                                    <?php echo $this->getWeight($row_product->optionweight_id, 'weight'); ?>
                                    <?php echo $this->getWeight($row_product->optionheight_id, 'height'); ?>
                                </td>
                                <td class="text_center">
                                    <?php echo $row_product->quantity; ?> x
                                    <br/>
                                    <?php echo (float) $row_product->price; ?>
                                    <hr/>
                                    <b><?php echo (float) $row_product->price * (int) $row_product->quantity; ?></b>
                                </td>
                                <td class="text_center">
                                    <?php
                                    $t = OpenShopHelper::getOrderStatusName($row->order_status_id, JComponentHelper::getParams('com_languages')->get('site', 'en-GB'));
                                    if (strtoupper($t) == 'COMPLETE') {
                                        echo '<img src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('order_complete_img')  .'" width="64px" title="' . JText::_('OPENSHOP_WARTING_SHIPPING') . '" />';
                                    } else {
                                        $quantity = $this->getQuantityProductInWarehouse($row_product->product_id, $row_product->product_size, $row_product->product_color);
                                        echo $row_product->quantity . ' / <b>'.$quantity.'</b><br/><br/>';

                                        if ($quantity >= $row_product->quantity) {
                                            if ($this->checkOrderEnouch($row->id)) {
                                                echo '<img src="'. OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_appointment_img') .'" width="64px" title="' . JText::_('OPENSHOP_WARTING_SHIPPING') . '" />';
                                            } else {
                                                echo '<img src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_enouch_img') .'" width="64px" title="' . JText::_('OPENSHOP_WARTING_ENOUCH') . '" />';
                                            }
                                        } else {
                                            echo '<img src="' . OPENSHOP_PATH_IMG_OTHER_HTTP . OpenShopHelper::getConfigValue('waiting_input_img') .'" width="64px" title="' . JText::_('OPENSHOP_WARTING_IMPORT') . '" />';
                                        }
                                    }
                                    ?>
                                </td>
                                <td class="text_center">
                                    <?php
                                    if ($row->created_date != $this->nullDate) {
                                        echo JHtml::_('date', $row->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y'));
                                    }
                                    ?>
                                </td>
                                <td class="text_center">
                                    <textarea rows="4" style="width:120px;"><?php echo $row_product->order_product_desc; ?></textarea>
                                </td>
                                <?php
                                if ($hide_show == 0) {
                                    ?>
                                    <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                        <?php
                                        $t = OpenShopHelper::getOrderStatusName($row->order_status_id, JComponentHelper::getParams('com_languages')->get('site', 'en-GB'));
                                        ?>
                                        <span class="border_background_<?php echo strtolower($t); ?>" style="margin-top: 10px;">
                                            <?php echo $t; ?>
                                        </span>
                                        <br/><br/>
                                        <?php
                                        if (OpenShopHelper::getConfigValue('hideshow_appointment') == '1') { //modal
                                            if (strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'PENDING') {
                                                if($this->checkOrderEnouch($row->id) == 1)
                                                {
                                                    ?>
                                                    <div class="btn btn-primary margin-btn" title="<?php echo JText::_('OPENSHOP_ORDER_APPOINTMENT_DESC') ?>" onclick='show_modal_appointment(<?php echo json_encode($row) ?>, <?php echo json_encode($row_pro) ?>);'><i class="icon-tags-2"></i></div>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <div class="btn btn-primary margin-btn btn-appointment-disable" title="<?php echo JText::_('OPENSHOP_ORDER_APPOINTMENT_DESC') ?>" '><i class="icon-tags-2"></i></div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="btn btn-danger margin-btn" onclick='show_delete_order(<?php echo $row->id ?>)' title="<?php echo JText::_('OPENSHOP_ORDER_DELETE_DESC') ?>"><i class="icon-delete"></i></div>
                                                <!--<div class="add-order-product" onclick="derict_more_product('<?php echo $link_new_order ?>')"><i class="icon-plus-2"></i><i class="icon-cart"></i></div>-->
                                                <?php
                                            } else if (strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'COMPLETE') {
                                                ?>
                                                <div class="btn btn-default" onclick='' title="<?php echo JText::_('OPENSHOP_ORDER_RETURN_DESC') ?>"><i class="icon-enter"></i></div>
                                                <?php
                                            }
                                        } else if (OpenShopHelper::getConfigValue('hideshow_appointment') == '2' && strtoupper(OpenShopHelper::getStatusOrder($row->id)) == 'PENDING') { //_blank
                                            ?>
                                            <div class="checkout">
                                                <a class="checkout_a" href="#" target="_blank" style="text-decoration: none; color: black;"><?php echo JText::_('OPENSHOP_APPOINTMENT') ?></a>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <?php
                                    $hide_show = 1;
                                }
                                ?>
                            </tr>		
                            <?php
                        }
                        $hide_show = 0;
                        $k = 1 - $k;
                        $stt += 1;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <!--APPOINTMENT-->
        <div class="modal fade" id="modal-appointment" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px;">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_APPOINTMENT') ?></h3>
                        <div class="btn-right" style="float: right;">
                            <button type="button" class="btn btn-primary" onclick="order_ok()"><i class="icon-apply"></i> <?php echo JText::_('OPENSHOP_OK') ?></button>
                            <button type="button" class="btn btn-default btn-more-product" onclick=""><i class="icon-plus-2"></i> <?php echo JText::_('OPENSHOP_MORE_PRODUCTS') ?></button>
                            <button type="button" class="btn btn-default check_delete_order" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!--INFO CUSTOMER + APPOINTMENT-->
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_ORDER_CUSTOMER_INFORMATION') ?>
                                        </div>
                                        <div class="panel-body responsive-edit row">
                                            <div class="form-group col-md-12">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_NAME') ?></label>
                                                <span id="customer_name" class="col-md-9"></span>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_PHONE') ?></label>
                                                <span id="customer_phone" class="col-md-9"></span>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_ADDRESS') ?></label>
                                                <span id="customer_address" class="col-md-9"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_ORDER_TOTAL_PAID') ?>
                                        </div>
                                        <div class="panel-body responsive-edit">
                                            <div class="total_paid">
                                                <!--//paid-->
                                            </div>
                                            <div class="unit_money">( VNĐ )</div>
                                            <div>
                                                Số lượng sản phẩm: <b><span class="total_buy_product label label-default"></span></b>
                                                <br/>
                                                Tổng tiền: <b><span class="total_money"></span></b>
                                                <br/>
                                                Giảm giá: <b><span class="sale_money">0</span></b>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_STATUS_APPOINTMENT') ?>
                                        </div>
                                        <div class="panel-body cus responsive-edit">
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_STATUS_1') ?></label>
                                                <span class="col-md-9">
                                                    <div class="btn-group" role="group" aria-label="..." style="margin-bottom: 10px;">
                                                        <button type="button" class="btn btn-default active status-shipping order-appointment"><?php echo JText::_('OPENSHOP_STATUS_SHIP'); ?></button>
                                                    </div>
                                                </span>
                                                <input type="hidden" value="2" id="status_appointment" name="status_appointment" />
                                            </div>
                                            <div class="form-group transporter_hideshow">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_TRANSPORTER') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" disabled="disable" class="form-control" name="appointment_transporter" id="appointment_transporter" value="" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DATE') ?></label>
                                                <span class="col-md-9">
                                                    <?php
                                                    $date = getdate();
                                                    $y = $date['year'];
                                                    $m = $date['mon'] < 10 ? '0' . $date['mon'] : $date['mon'];
                                                    $d = $date['mday'] < 10 ? '0' . $date['mday'] : $date['mday'];
                                                    echo JHtml::_('calendar', $y . '-' . $m . '-' . $d, 'appointment_date', 'appointment_date', '%Y-%m-%d', array('style' => 'width: 208px;'));
                                                    ?>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_SALE') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" class="form-control" name="appointment_sale" id="appointment_sale" value="" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DISCOUNT') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" class="form-control" name="appointment_discount" id="appointment_discount" value="" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DESCRIPTION') ?></label>
                                                <span class="col-md-9">
                                                    <textarea rows="3" class="form-control" width="" name="appointment_description" id="appointment_description"></textarea>
                                                </span>
                                            </div>

                                            <!--ID ORDER-->
                                            <input type="hidden" name="order_id" id="order_id" value=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--PRODUCTS IN ORDER-->
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_NUM'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_IMAGE'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_PRICE_TOTAL'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="show-pro-appointment">
                                        <!--show product information-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--//foot-->
                    </div>
                </div>
            </div>
        </div>


        <!--DELETE APPOINTMENT-->
        <div class="modal fade" id="modal-reason-delete-order" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_REASON_DELETE_ORDER') ?></h3>
                        <div style="float: right;">
                            <button type="button" class="btn btn-danger delete_order" onclick="" ><i class="icon-delete"></i> <?php echo JText::_('OPENSHOP_DELETE') ?></button>
                            <button type="button" class="btn btn-default delete_order_cancel" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3"><?php echo JText::_('OPENSHOP_SELECT_REASON') ?></label>
                                    <span class="col-md-9">
                                        <?php echo $this->lists['reason_delete_order'] ?>
                                        <div class="error_reason"></div>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3"><?php echo JText::_('OPENSHOP_SELECT_REASON_DESC') ?></label>
                                    <span class="col-md-9">
                                        <textarea class="form-control reason_delete_order_desc" rows="5" name="reason_delete_order_desc" id="reason_delete_order_desc" style="width:625px"></textarea>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//foot-->
                </div>
            </div>
        </div>

        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
        <input type="hidden" name="view" id="view" value="orders" />
        <?php echo JHtml::_('form.token'); ?>			
    </form>

    <div class="modal fade" id="modal-cart-export" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
        <div class="modal-dialog modal-lg" role="document">
            <form action="index.php?option=com_openshop&task=functions.export_excel&format=ajax&v=<?php echo JFactory::getApplication()->input->getString('view') ?>&id=<?php echo JFactory::getApplication()->input->getInt('id_transporter') ?>" method="post" id="submit_export">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_EXPORT_EXCEL') ?></h3>
                        <div style="float: right;">
                            <button type="submit" class="btn btn-primary btn-export" onclick=""><i class="icon-download"></i> <?php echo JText::_('OPENSHOP_EXPORT') ?></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 export_name_label"><?php echo JText::_('OPENSHOP_EXPORT_NAME') ?></label>
                                    <span class="col-md-9">
                                        <input type="text" name="export_name" id="export_name" value="" class="form-control export_name" placeholder="<?php echo JText::_('OPENSHOP_EXPORT_NAME') ?>"/>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 status_export_label"><?php echo JText::_('OPENSHOP_STATUS_EXPORT') ?></label>
                                    <span class="col-md-9">
                                        <?php echo $this->lists['status_export'] ?>
                                    </span>
                                </div>
                                <br/><br/><br/>
                                <div class="form-group">
                                    <label class="col-md-3 column-choose-label"><?php echo JText::_('OPENSHOP_CONTENT_COLUMN') ?></label>
                                    <span class="col-md-9">
                                        <div class="col-md-12 column-heigth" style="margin-top: 30px;">
                                            <span class="col-md-4">
                                                <span class="column-export column-export-1" onclick="choose_column_export('1');">Mã sản phẩm</span>
                                            </span>
                                            <span class="col-md-4">
                                                <span class="column-export column-export-2" onclick="choose_column_export('2')">Tên sản phẩm</span>
                                            </span>
                                            <span class="col-md-4">
                                                <span class="column-export column-export-3" onclick="choose_column_export('3');">Thành tiền</span>
                                            </span>
                                        </div>
                                        
                                        <div class="col-md-12 column-heigth">
                                            <span class="col-md-4">
                                                <span class="column-export column-export-4" onclick="choose_column_export('4');">Tên khách hàng</span>
                                            </span>
                                            <span class="col-md-4">
                                                <span class="column-export column-export-5" onclick="choose_column_export('5');">Số điện thoại</span>
                                            </span>
                                            <span class="col-md-4">
                                                <span class="column-export column-export-6" onclick="choose_column_export('6');">Địa chỉ</span>
                                            </span>
                                        </div>
                                    </span>
                                    
                                    <input type="hidden" class="form-control content-export-column" name="content-export-column" value="" />
                                </div>
                                <br/><br/><br/><br/><br/><br/>
                                <div class="export-column_title"><?php echo JText::_('OPENSHOP_ORDER_COLUMN_EXPORT') ?></div>
                                <div class="form-group">
                                    <span class="order_column">
                                        <!--//code-->
                                    </span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


</fieldset>