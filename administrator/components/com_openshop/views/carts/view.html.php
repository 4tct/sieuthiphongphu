<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewCarts extends OpenShopViewList {

    function _buildListArray(&$lists, $state) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, b.orderstatus_name AS text')
                ->from('#__openshop_orderstatuses AS a')
                ->innerJoin('#__openshop_orderstatusdetails AS b ON (a.id = b.orderstatus_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
        $db->setQuery($query);
        $options = array();
        $rows = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDERSSTATUS_ALL'));
        foreach ($db->loadObjectList() as $value) {
            $rows[] = JHtml::_('select.option', $value->value, JText::_('OPENSHOP_STATUS_ORDER_' . strtoupper($value->text)));
        }
        $options = array_merge($options, $rows);
        $lists['order_status_id'] = JHtml::_('select.genericlist', $options, 'order_status_id', ' class="inputbox" style="width: 150px;" onchange="this.form.submit();"', 'value', 'text', JRequest::getInt('order_status_id'));

        //get reason delete order
        $query->clear();
        $query->select('config_value')->from($db->quoteName('#__openshop_configs'))->where('config_key = "reason_delete_order"');
        $reason_id = $db->setQuery($query)->loadResult();
        $query->clear();
        $query->select('b.id as value, b.reason_detail_description as text')
                ->from($db->quoteName('#__openshop_reasons', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_reasondetails', 'b') . 'ON a.id = b.reason_id')
                ->where('a.id = "' . $reason_id . '"');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', JText::_('OPENSHOP_REASON_DELETE_ORDER_SELECT'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }

        $lists['reason_delete_order'] = JHtml::_('select.genericlist', $options, 'reason_delete_order', 'class="form-control reason_delete_order"', 'value', 'text', '0');

        //status order using export
        $query->clear();
        $query->select('orderstatus_id as value, orderstatus_name as text')
                ->from($db->quoteName('#__openshop_orderstatusdetails'));
        $rows = $db->setQuery($query)->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '-1', JText::_('OPENSHOP_SELECT_ALL'));
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }

        $lists['status_export'] = JHtml::_('select.genericlist', $options, 'status_export[]', 'class="form-control chosen" multiple', 'value', 'text', '');
        $db = JFactory::getDbo();
        $nullDate = $db->getNullDate();
        $this->nullDate = $nullDate;
        $currency = new OpenShopCurrency();
        $this->currency = $currency;
    }

    /**
     * Override Build Toolbar function, only need Delete, Edit and Download Invoice
     */
    function _buildToolbar() {
        $viewName = $this->getName();
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
        $view = 'openshop_transporter';
        $id_trans = JFactory::getApplication()->input->getString('id_transporter');
        $view .= '_' . OpenShopHelper::get1RecordTable('#__openshop_transporters', $id_trans, 'transporter_name');
        $view = strtoupper($view);
        
        if (OpenShopHelper::getPermissionAction($view, 'create') && OpenShopHelper::getPermissionAction($view, 'edit')) {
            JToolbarHelper::custom('cart_export', 'download', 'download', JText::_('OPENSHOP_EXPORT'), false);
        }
    }

    public function productListItem($i) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('ord.*, pro.product_image')
                ->from($db->quoteName('#__openshop_orderproducts', 'ord'))
                ->join('LEFT', $db->quoteName('#__openshop_products', 'pro') . 'ON ord.product_id = pro.id')
                ->where('order_product_delete = 0')
                ->where('order_id = ' . $i);
        $db->setQuery($query);

        return $db->loadObjectList();
    }

    public function getSizeColor($i_pro, $id_cond, $cond) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->clear();
        $query->select('opd.value AS text, opd.id AS value')
                ->from($db->quoteName('#__openshop_productoptionvalues', 'prov'))
                ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'opd') . 'ON prov.option_value_id = opd.optionvalue_id')
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'optd') . 'ON opd.option_id = optd.option_id')
                ->where('prov.product_id = ' . $i_pro);

        $options = array();
        switch ($cond) {
            case 'size':
                $query->where('optd.option_name = "SIZE"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $t_id = 'product_size';
                $t_witdh = '50px';
                break;
            case 'color':
                $query->where('optd.option_name = "COLOR"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $t_id = 'product_color';
                $t_witdh = '70px';
                break;
            default :
                break;
        }

        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" style="width: ' . $t_witdh . '; ', 'value', 'text', $id_cond);
        return $lists;
    }

    public function getWeight($id_cond, $cond) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('opd.value AS text, opd.id AS value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'optd') . 'ON opd.option_id = optd.option_id');

        $options = array();
        switch ($cond) {
            case 'weight':
                $query->where('optd.option_name = "WEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $t_id = 'optionweight_id';
                break;
            case 'height':
                $query->where('optd.option_name = "HEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $t_id = 'optionheight_id';
                break;
            default:
                break;
        }

        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" style="width: 120px; ', 'value', 'text', $id_cond);
        return $lists;
    }

    function getRowProductInOrder($id_order) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(True);

        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('order_product_delete = 0')
                ->where('order_id = ' . $id_order);
        $db->setQuery($query);
        return (int) ($db->loadResult());
    }

    function getDataCancel($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.reason_detail_description,a.reason_description,a.created_date')
                ->from($db->quoteName('#__openshop_orderreasons', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_reasondetails', 'b') . 'ON a.reasondetail_id = b.id')
                ->where('a.order_id = ' . $id);
        return $db->setQuery($query)->loadObject();
    }

    function getQuantityProductInWarehouse($id_product, $id_size, $id_color) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('total_import_product')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_inventories', 'b') . 'ON a.id = b.idsize')
                ->where('b.idsize = ' . $id_size)
                ->where('b.idcolor = ' . $id_color)
                ->where('b.product_id = ' . $id_product);
        $res = $db->setQuery($query)->loadResult();
        return $res == '' ? 0 : $res;
    }

    function checkOrderEnouch($id_order) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('product_id,product_size,product_color,quantity')
                ->from($db->quoteName('#__openshop_orderproducts', 'a'))
                ->where('order_id = ' . $id_order);

        $rows_product = $db->setQuery($query)->loadObjectList();

        $res = 1;
        foreach ($rows_product as $value) {
            $id_p = $value->product_id;
            $size = $value->product_size;
            $color = $value->product_color;
            $quantity_order = $value->quantity;
            $query->clear();
            $query->select('total_import_product')
                    ->from($db->quoteName('#__openshop_inventories'))
                    ->where('idsize = ' . $size)
                    ->where('idcolor = ' . $color)
                    ->where('product_id = ' . $id_p);
            $quantity = $db->setQuery($query)->loadResult();    //quantity product in warehouse

            if ($quantity == 0 || $quantity_order > $quantity) {
                $res = 0;
            }
        }

        return $res;
    }

}
