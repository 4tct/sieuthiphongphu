<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewShipping extends OpenShopViewForm
{

	function _buildListArray(&$lists, $item)
	{
		$registry = new JRegistry();
		$registry->loadString($item->params);
		$data = new stdClass();
		$data->params = $registry->toArray();
		$form = JForm::getInstance('openshop', JPATH_ROOT . '/components/com_openshop/plugins/shipping/' . $item->name . '.xml', array(), false, '//config');
		$form->bind($data);
		$this->form = $form;
		return true;
	}
	
	/**
	 * Override Build Toolbar function, only need Save, Save & Close and Close
	 */
	function _buildToolbar()
	{
		$viewName = $this->getName();
		$canDo = OpenShopHelper::getActions($viewName);
		$text = JText::_($this->lang_prefix . '_EDIT');
		JToolBarHelper::title(JText::_($this->lang_prefix . '_' . $viewName) . ': <small><small class="title-product">[ ' . $text . ' ]</small></small>');
		if ($canDo->get('core.edit'))
		{
			JToolBarHelper::apply($viewName . '.apply');
			JToolBarHelper::save($viewName . '.save');
		}
		JToolBarHelper::cancel($viewName . '.cancel', 'JTOOLBAR_CLOSE');
	}
}