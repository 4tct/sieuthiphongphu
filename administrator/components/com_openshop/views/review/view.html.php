<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewReview extends OpenShopViewForm
{

	function __construct($config)
	{
		$config['name'] = 'review';
		parent::__construct($config);
	}
	
	function _buildListArray(&$lists, $item)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		//Build products list
		$query->select('a.id AS value, b.product_name AS text')
			->from('#__openshop_products AS a')
			->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
			->where('a.published = 1')
			->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"')
			->order('a.ordering');
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		if (count($rows))
		{
			$lists['products'] = JHtml::_('select.genericlist', $rows, 'product_id',
				array(
					'option.text.toHtml' => false,
					'option.text' => 'text',
					'option.value' => 'value',
					'list.attr' => ' class="inputbox chosen" ',
					'list.select' => $item->product_id));
		}
		else
		{
			$lists['products'] = '';
		}
		$ratingHtml = '<b>' . JText::_('OPENSHOP_BAD') . '</b>';
		for ($i = 1; $i <= 5; $i++)
		{
			if ($i == $item->rating)
				$checked = ' checked';
			else 
				$checked = '';
			$ratingHtml .= '<input type="radio" name="rating" value="' . $i . '" style="width: 25px;"' . $checked .' />';
		}
		$ratingHtml .= '<b>' . JText::_('OPENSHOP_EXCELLENT') . '</b>';
		$lists['rating'] = $ratingHtml;
	}
}