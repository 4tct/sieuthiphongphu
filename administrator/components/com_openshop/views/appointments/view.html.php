<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewAppointments extends OpenShopViewList {

    function _buildListArray(&$lists, $state) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, b.orderstatus_name AS text')
                ->from('#__openshop_orderstatuses AS a')
                ->innerJoin('#__openshop_orderstatusdetails AS b ON (a.id = b.orderstatus_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . JComponentHelper::getParams('com_languages')->get('site', 'en-GB') . '"');
        $db->setQuery($query);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDERSSTATUS_ALL'));
        $options = array_merge($options, $db->loadObjectList());
        $lists['order_status_id'] = JHtml::_('select.genericlist', $options, 'order_status_id', ' class="inputbox" style="width: 150px;" onchange="this.form.submit();"', 'value', 'text', JRequest::getInt('order_status_id'));

        $db = JFactory::getDbo();
        $nullDate = $db->getNullDate();
        $this->nullDate = $nullDate;
        $currency = new OpenShopCurrency();
        $this->currency = $currency;
    }

    /**
     * Override Build Toolbar function, only need Delete, Edit and Download Invoice
     */
    function _buildToolbar() {
        $viewName = $this->getName();

        $controller = OpenShopInflector::singularize($this->getName());
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
    }

    public function productListItem($i) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('ord.*, pro.product_image')
                ->from($db->quoteName('#__openshop_orderproducts', 'ord'))
                ->join('LEFT', $db->quoteName('#__openshop_products', 'pro') . 'ON ord.product_id = pro.id')
                ->where('order_product_delete = 0')
                ->where('order_id = ' . $i);
        $db->setQuery($query);

        return $db->loadObjectList();
    }

    public function getSizeColor($i_pro, $id_cond, $cond) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->clear();
        $query->select('opd.value AS text, opd.id AS value')
                ->from($db->quoteName('#__openshop_productoptionvalues', 'prov'))
                ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'opd') . 'ON prov.option_value_id = opd.optionvalue_id')
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'optd') . 'ON opd.option_id = optd.option_id')
                ->where('prov.product_id = ' . $i_pro);
        
        $options = array();
        switch ($cond){
            case 'size':
                $query->where('optd.option_name = "SIZE"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
                $t_id = 'product_size';
                $t_witdh = '50px';
                break;
            case 'color':
                $query->where('optd.option_name = "COLOR"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_COLOR'));
                $t_id = 'product_color';
                $t_witdh = '70px';
                break;
            default :
                break;
        }
        
        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" style="width: '. $t_witdh .'; ', 'value', 'text', $id_cond);
        return $lists;
    }
    
    public function getWeight($id_cond, $cond) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('opd.value AS text, opd.id AS value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'optd') . 'ON opd.option_id = optd.option_id');
        
        $options = array();
        switch ($cond){
            case 'weight':
                $query->where('optd.option_name = "WEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'));
                $t_id = 'optionweight_id';
                break;
            case 'height':
                $query->where('optd.option_name = "HEIGHT"');
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'));
                $t_id = 'optionheight_id';
                break;
            default:
                break;
        }
        $db->setQuery($query);
        $options = array_merge($options, $db->loadObjectList());
        $lists = JHtml::_('select.genericlist', $options, $t_id, ' class="inputbox" style="width: 120px; ', 'value', 'text', $id_cond);
        return $lists;
    }
    
    function getRowProductInOrder($id_order)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(True);
        
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('order_product_delete = 0')
                ->where('order_id = ' . $id_order);
        $db->setQuery($query);
        return (int)($db->loadResult());
    }

}
