<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$ordering = ($this->lists['order'] == 'a.ordering');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addStyleSheet(JURI::base() . '/components/com_openshop/assets/css/order.css');
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/order.js');
//print_r($this->items);
?>
<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=orders" method="post" name="adminForm" id="adminForm">
        <table width="100%">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />		
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>		
                </td>
                <td align="right">
                    <?php echo $this->lists['order_status_id']; ?>
                </td>	
            </tr>
        </table>
        <div id="editcell">
            <table class="adminlist table table-striped" style="vertical-align: middle;">
                <thead>
                    <tr>
                        <th width="2%" class="text_center">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_center" width="15%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_CUSTOMER'), 'a.payment_fullname', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_center"  width="10%">
                            <?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
                        </th>
                        <th class="text_center"  width="10%">
                            <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_ORDER_SIZE') ?>
                        </th>
                        <th class="text_center" width="6%">
                            <?php echo JText::_('OPENSHOP_QUANTITY'); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_PRICE'); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_APPOINTMENT_DATE'), 'a.created_date', $this->lists['order_Dir'], $this->lists['order']); ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_DESCRIPTION') ?>
                        </th>
                        <th class="text_center" width="10%">
                            <?php echo JText::_('OPENSHOP_ORDER_STATUS'); ?>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="11">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $k = 0;
                    $stt = 0;
                    $hide_show = 0;
                    for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                        $row = &$this->items[$i];
                        $checked = JHtml::_('grid.id', $i, $row->id);
                        $rowspan = $this->getRowProductInOrder($row->id);
                        $phone = substr($row->payment_telephone, 0, 4) . ' ' . substr($row->payment_telephone, 4, 3) . ' ' . substr($row->payment_telephone, 7, (int) (strlen($row->payment_telephone) - 7));

                        if ($row->appointment_type == '1') {
                            $row->message_appointment_status = JText::_('OPENSHOP_STATUS_OFFICE');
                        }
                        if ($row->appointment_type == '2') {
                            $row->message_appointment_status = JText::_('OPENSHOP_STATUS_SHIP');
                        }
                        ?>
                        <tr class="<?php echo "row$k"; ?>">
                            <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                <?php echo ($stt + 1); ?> 
                            </td>
                            <td class="text_left" rowspan="<?php echo $rowspan ?>">
                                <span><b><?php echo $row->payment_fullname; ?></b></span><br>
                                ( <b><?php echo $phone; ?></b> )
                                <br>
                                <span><?php echo $row->payment_address ?></span>
                                <br>
                                <span><b>[ <?php echo $row->zone_name; ?> - <?php echo $row->district_name; ?> ]</b></span>
                            </td>
                            <?php
                            $row_pro = array();
                            foreach ($this->productListItem($row->id) as $row_product) {
                                array_push($row_pro, $row_product);
                            }
                            foreach ($this->productListItem($row->id) as $row_product) {
                                //separating the phone number
                                ?>
                                <td class="text_center">
                                    <span><?php echo $row_product->product_name; ?></span>
                                </td>
                                <td class="text_center">
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row_product->product_image; ?>" alt="Product Image" width="90px" title="<?php echo $row_product->product_name ?>"/>
                                </td>
                                <td class="text_left">
                                    <?php echo $this->getSizeColor($row_product->product_id, $row_product->product_size, 'size'); ?>
                                    <?php echo $this->getSizeColor($row_product->product_id, $row_product->product_color, 'color'); ?>
                                    <?php echo $this->getWeight($row_product->optionweight_id, 'weight'); ?>
                                    <?php echo $this->getWeight($row_product->optionheight_id, 'height'); ?>
                                </td>
                                <td class="text_center">
                                    <?php echo $row_product->quantity; ?>
                                </td>
                                <td class="text_center">
                                    <?php echo (float) $row_product->price; ?>
                                </td>
                                <td class="text_center">
                                    <?php
                                    if ($row->created_date != $this->nullDate) {
                                        echo JHtml::_('date', $row->appointment_date, OpenShopHelper::getConfigValue('date_format', 'd-m-Y'));
                                    }
                                    ?>
                                </td>
                                <td class="text_center">
                                    <textarea rows="4" style="width:120px;"><?php echo $row->appointment_description; ?></textarea>
                                </td>
                                <?php
                                if ($hide_show == 0) {
                                    ?>
                                    <td class="text_center" rowspan="<?php echo $rowspan ?>">
                                        <?php
                                        if ($row->appointment_type == '2') {   //shipping
                                            echo '<div class="">[ ' . $row->transporter_name . ' ]</div>';
                                            ?>
                                            <div class="checkout" onclick='show_modal_appointment_cart(<?php echo json_encode($row) ?>);'><?php echo JText::_('OPENSHOP_APPOINTMENT_CHECKOUT') ?></div>
                                            <?php
                                        }

                                        if ($row->appointment_type == '1') {   //office order
                                            ?>
                                            <div class="">[ <?php echo JText::_('OPENSHOP_ORDER_OFFICE') ?> ]</div>
                                            <div class="checkout" onclick='show_modal_appointment_cart(<?php echo json_encode($row) ?>);'><?php echo JText::_('OPENSHOP_APPOINTMENT_CHECKOUT') ?></div>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <?php
                                    $hide_show = 1;
                                }
                                ?>
                            </tr>		
                            <?php
                        }
                        $hide_show = 0;
                        $k = 1 - $k;
                        $stt += 1;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <!--APPOINTMENT-->
        <div class="modal fade" id="modal-appointment" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="position: absolute;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height: 65px;">
                        <h3 class="modal-title" id="myModalLabel" style="float: left;"><?php echo JText::_('OPENSHOP_APPOINTMENT') ?></h3>
                        <div class="btn-right" style="float: right;">
                            <button type="button" class="btn btn-primary" onclick="order_ok()"><i class="icon-apply"></i> <?php echo JText::_('OPENSHOP_APPOINTMENT_CHECKOUT') ?></button>
                            <button type="button" class="btn btn-default check_delete_order" data-dismiss="modal" onclick=""><i class="icon-cancel-circle" style="color: red;"></i> <?php echo JText::_('OPENSHOP_CLOSE') ?></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!--INFO CUSTOMER + APPOINTMENT-->
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_ORDER_CUSTOMER_INFORMATION') ?>
                                        </div>
                                        <div class="panel-body responsive-edit">
                                            <div class="form-group col-md-12">
                                                <label class="col-md-4"><?php echo JText::_('OPENSHOP_NAME') ?></label>
                                                <span id="customer_name" class="col-md-8"></span>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-4"><?php echo JText::_('OPENSHOP_PHONE') ?></label>
                                                <span id="customer_phone" class="col-md-8"></span>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-4"><?php echo JText::_('OPENSHOP_ADDRESS') ?></label>
                                                <span id="customer_address" class="col-md-8"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="height: 250px;">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_ORDER_TOTAL_PAID') ?>
                                        </div>
                                        <div class="panel-body responsive-edit">
                                            <div class="total_paid">
                                                <!--//paid-->
                                            </div>
                                            <div class="unit_money">( VNĐ )</div>
                                            <div>
                                                Số lượng sản phẩm: <b><span class="total_buy_product label label-default"></span></b>
                                                <br/>
                                                Tổng tiền: <b><span class="total_money"></span></b>
                                                <br/>
                                                Giảm giá: <b><span class="sale_money">0</span></b>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <?php echo JText::_('OPENSHOP_STATUS_APPOINTMENT') ?>
                                        </div>
                                        <div class="panel-body cus responsive-edit">
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_STATUS_1') ?></label>
                                                <span class="col-md-9">
                                                    <div class="btn-group appointment_check" role="group" aria-label="..." style="margin-bottom: 10px;">
                                                        <!--Code-->
                                                    </div>
                                                    <input type="hidden" value="0" id="status_appointment" name="status_appointment" />
                                                </span>
                                            </div>
                                            <div class="form-group transporter_hideshow" style="display:none;">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_TRANSPORTER') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" disabled="disable" class="form-control" name="appointment_transporter" id="appointment_transporter" value="" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DATE') ?></label>
                                                <span class="col-md-9">
                                                    <?php
                                                    $date = getdate();
                                                    $y = $date['year'];
                                                    $m = $date['mon'] < 10 ? '0' . $date['mon'] : $date['mon'];
                                                    $d = $date['mday'] < 10 ? '0' . $date['mday'] : $date['mday'];
                                                    echo JHtml::_('calendar', $y . '-' . $m . '-' . $d, 'appointment_date', 'appointment_date', '%Y-%m-%d', array('style' => 'width: 208px;'));
                                                    ?>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_SALE') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" class="form-control" name="appointment_sale" id="appointment_sale" value="" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DISCOUNT') ?></label>
                                                <span class="col-md-9">
                                                    <input type="text" class="form-control" name="appointment_discount" id="appointment_discount" value="" />
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo JText::_('OPENSHOP_DESCRIPTION') ?></label>
                                                <span class="col-md-9">
                                                    <textarea rows="3" class="form-control" width="" name="appointment_description" id="appointment_description"><?php echo $row->appointment_description ?></textarea>
                                                </span>
                                            </div>

                                            <!--ID ORDER-->
                                            <input type="hidden" name="order_id" id="order_id" value=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--PRODUCTS IN ORDER-->
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_NUM'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_IMAGE'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_PRICE_TOTAL'); ?></th>
                                            <th style="text-align: center;"><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="show-pro-appointment">
                                        <!--show product information-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
        <input type="hidden" id="view" value="<?php echo JFactory::getApplication()->input->getString('view') ?>" />
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>