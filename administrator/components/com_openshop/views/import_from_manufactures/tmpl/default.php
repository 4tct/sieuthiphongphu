<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
//print_r($this->items); exit();
?>

<style>
    #tbl-quantity-import tr td, #tbl-quantity-import tr th{
        padding: 5px;
        vertical-align: middle;
    }
    #tbl-quantity-import{
        margin: 0 auto;
    }
    #tbl_import_from_manufacturer tr td, #tbl_import_from_manufacturer tr th{
        vertical-align: middle;
    }
</style>

<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=import_from_manufactures" method="post" name="adminForm" id="adminForm">
        <div id="editcell">
            <table class="adminlist table table-striped" id="tbl_import_from_manufacturer">
                <thead>
                    <tr>
                        <th width="2%">
                            <?php echo JText::_('OPENSHOP_NUM'); ?>
                        </th>
                        <th class="text_left" width="15%">
                            <?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>				
                        </th>
                        <th width="5%" class="text_center">
                            <?php echo JText::_('OPENSHOP_IMAGE'); ?>
                        </th>
                        <th width="10%" class="text_center">
                            <?php echo JText::_('OPENSHOP_IMPORT_PRODUCT'); ?>
                        </th>
                        <th width="10%" class="text_center">
                            <?php echo JText::_('OPENSHOP_ADDRESS'); ?>
                        </th>										
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $temp = 0;
                    if (!empty($this->items)) {
                        $arr_order_id = array();
                        foreach ($this->items as $k_item => $v_item) {
                            $arr_order_id[] = $v_item->order_id;
                        }
                        foreach ($this->items as $k_item => $v_item) {
                            if ($temp != $v_item->product_id) {
                                $temp = $v_item->product_id;
                                echo '<tr>';
                                echo '  <td>' . ($k_item + 1) . '</td>';
                                echo '  <td>' . $v_item->product_name . ' - <b>' . $v_item->product_sku . '</b></td>';
                                echo '  <td style="text-align:center">';
                                if (JFile::exists(OPENSHOP_PATH_IMG_PRODUCT . $v_item->product_image)) {
                                    $viewImage = JFile::stripExt($v_item->product_image) . '-100x100.' . JFile::getExt($v_item->product_image);
                                    if (Jfile::exists(OPENSHOP_PATH_IMG_PRODUCT_RESIZED . $viewImage)) {
                                        ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_RESIZED_HTTP . $viewImage; ?>" width="90" />
                                    <?php
                                } else {
                                    ?>
                                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $v_item->product_image; ?>" width="90" />
                                    <?php
                                }
                            }
                            echo '  </td>';

                            echo '  <td>';

                            $size = $this->getSizeColorInInventory($v_item->product_id, $v_item->product_size, 'size');
                            $color = $this->getSizeColorInInventory($v_item->product_id, $v_item->product_color, 'color');

                            echo '<table id="tbl-quantity-import">';
                            echo '  <tr>';
                            if (OpenShopHelper::getConfigValue('managesizecolor') == '2') {
                                echo '      <td></td>';
                            }
                            foreach ($size as $v_size) {
                                echo '<td>' . $v_size->value . '</td>';
                            }
                            echo '  </tr>';

                            foreach ($color as $v_color) {
                                echo '  <tr>';
                                if (OpenShopHelper::getConfigValue('managesizecolor') == '2') {
                                    echo '      <td>' . $v_color->value . '</td>';
                                }
                                foreach ($size as $v_size) {
                                    echo '<td>' . $this->getQuantitySizeColorWarehouse($v_item->product_id, $v_size->idsize, $v_color->idcolor, $arr_order_id) . '</td>';
                                }
                                echo '  </tr>';
                            }

                            echo '</table>';
                            echo '  </td>';

                            echo '  <td style="text-align:center">';
                            $man = $this->getManufactureName($v_item->manufacturer_id);
                            echo $man->manufacturer_name;
                            echo '<br>';
                            echo $man->manufacturer_phone == '' ? '( Không có SĐT )' : '( <b>' . $man->manufacturer_phone . '</b> )';
                            echo '<br>';
                            echo $man->manufacturer_address == '' ? '[ Không có địa chỉ ]' : '[ <b>' . $man->manufacturer_address . '</b> ]';
                            echo '  </td>';
                            echo '</tr>';
                        }
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <input type="hidden" name="option" value="com_openshop" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
        <?php echo JHtml::_('form.token'); ?>			
    </form>
</fieldset>