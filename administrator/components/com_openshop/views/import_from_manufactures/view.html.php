<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class openshopViewimport_from_manufactures extends JViewLegacy {

    function display($tpl = null) {
        OpenShopPermissions::CheckPermission();

        $this->items = $this->get('Items');
        $this->buildToolbar();
        parent::display($tpl);
    }
    
    public function buildToolbar(){
        JToolbarHelper::title(JText::_('OPENSHOP_IMPORT_FROM_MANUFACTURER'));
    }
                function getQuantityInWareHouse($id_product) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('DISTINCT a.product_size,a.product_color,a.product_id,SUM(a.quantity) as total')
                ->from($db->quoteName('#__openshop_orderproducts', 'a'))
                ->where('a.product_id = ' . $id_product)
                ->where('a.order_id NOT IN '
                        . '(SELECT b.order_id FROM #__openshop_carts as b INNER JOIN #__openshop_appointments as c ON b.id = c.cart_id)')
                ->order('a.product_size,a.product_color,a.product_id');
        $rows = $db->setQuery($query)->loadObjectList();

        return $rows;
    }

    function getSizeColorName($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('optionvalue_id = ' . $id);
        return $db->setQuery($query)->loadResult() == '' ? 'NO ITEM' : $db->setQuery($query)->loadResult();
    }

    function getQuantityImportInWarehouse($pro, $size, $color) {
        $db = JFactory::getDbo();
        $id_warehouse = OpenShopHelper::getIdWarehouseUserF();
        $res = $db->setQuery('CALL TotalInventory("import",' . $pro . ',' . $size . ',' . $color . ',' . $id_warehouse . ')')->loadResult();
        return $res;
    }

    function getManufactureName($id_pro) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('manufacturer_name,manufacturer_address,manufacturer_phone')
                ->from($db->quoteName('#__openshop_manufacturers'))
                ->where('id = ' . $id_pro);
        return $db->setQuery($query)->loadObject();
    }

    function getSizeColorInInventory($id_pro, $id_sizecolor, $act) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->from($db->quoteName('#__openshop_inventories', 'a'));
        switch ($act) {
            case 'size':
                $query->select('b.value,a.idsize')
                        ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON a.idsize = b.optionvalue_id')
//                        ->where('b.optionvalue_id = ' . $id_sizecolor)
                        ->where('a.product_id = ' . $id_pro)
                        ->group('a.idsize');
                break;
            case 'color':
                $query->select('b.value,a.idcolor')
                        ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON a.idcolor = b.optionvalue_id')
//                        ->where('b.optionvalue_id = ' . $id_sizecolor)
                        ->where('a.product_id = ' . $id_pro)
                        ->group('a.idcolor');
                break;
            default :
                break;
        }

        return $db->setQuery($query)->loadObjectList();
    }

    function getQuantitySizeColorWarehouse($id_pro, $idsize, $idcolor, $order_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $id_warehouse = OpenShopHelper::getIdWarehouseUserF();
        $where = '';
        $t = 1;
        $quantity_warehouse = $db->setQuery('CALL TotalInventory("import",' . $id_pro . ', ' . $idsize . ',' . $idcolor . ', ' . $id_warehouse . ')')->loadResult();

        $query->clear();
        $query->select('sum(quantity)')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('product_size = ' . $idsize)
                ->where('product_color = ' . $idcolor)
                ->where('product_id = ' . $id_pro);
        foreach ($order_id as $o) {
            if ($t == '1') {
                $where .= 'order_id = ' . $o;
                $t = 0;
            } else {
                $where .= ' OR order_id = ' . $o;
            }
        }

        $query->where('(' . $where . ')');
        $quantity_order = $db->setQuery($query)->loadResult() ? $db->setQuery($query)->loadResult() : 0;

        $res = (int) $quantity_order - (int) $quantity_warehouse;
        return $res < 0 ? '0' : $res;
    }

}
