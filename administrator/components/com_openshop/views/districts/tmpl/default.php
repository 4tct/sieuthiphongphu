<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
//print_r($this->items);exit();
?>
<fieldset class="adminfs">
<form action="index.php?option=com_openshop&view=districts" method="post" name="adminForm" id="adminForm">
    <table width="100%">
        <tr>
            <td align="left">
                <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />
                <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
    <div id="editcell">
        <table class="adminlist table table-striped">
            <thead>
                <tr>
                    <th width="2%" class="text_center">
                        <?php echo JText::_('OPENSHOP_NUM'); ?>
                    </th>			
                    <th width="2%" class="text_center">
                        <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                    </th>
                    <th width="1%" class="text_center" style="min-width:55px">
                        <?php echo JHtml::_('grid.sort', JText::_('JSTATUS'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
                    </th>
                    <th class="text_center" width="30%">
                        <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_TOWN'), 'b.zone_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                    </th>
                    <th class="text_center" width="30%">
                        <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_DISTRICT'), 'a.district_name', $this->lists['order_Dir'], $this->lists['order']); ?>				
                    </th>
                    <th class="text_center" width="15%">
                        <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ZONE_CODE'), 'a.district_code', $this->lists['order_Dir'], $this->lists['order']); ?>				
                    </th>
                    <th width="5%" class="text_center">
                        <?php echo JHtml::_('grid.sort', JText::_('OPENSHOP_ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
                    </th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="7">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $k = 0;
                for ($i = 0, $n = count($this->items); $i < $n; $i++) {
                    $row = &$this->items[$i];
                    $link = JRoute::_('index.php?option=com_openshop&task=district.edit&cid[]=' . $row->id);
                    $checked = JHtml::_('grid.id', $i, $row->id);
                    $published = JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', 'district.');
                    ?>
                    <tr class="<?php echo "row$k"; ?>">
                        <td>
                            <?php echo $this->pagination->getRowOffset($i); ?>
                        </td>
                        <td>
                            <?php echo $checked; ?>
                        </td>
                        <td class="text_center">
                            <div class="btn-group">
                                <?php
                                echo JHtml::_('jgrid.published', $row->published, $i, 'district.');
                                echo $this->addDropdownList(JText::_('OPENSHOP_COPY'), 'copy', $i, 'district.copy');
                                echo $this->addDropdownList(JText::_('OPENSHOP_DELETE'), 'trash', $i, 'district.remove');
                                echo $this->renderDropdownList($this->escape($row->district_name));
                                ?>
                            </div>
                        </td>
                        <td class="text_center">
                            <?php echo $row->zone_name; ?>
                        </td>
                        <td class="text_center">
                            <a href="<?php echo $link; ?>"><?php echo $row->district_name; ?></a>
                        </td>
                        <td class="text_center">
                            <?php echo $row->district_code; ?>
                        </td>
                        <td class="text_center">
                            <?php echo $row->id; ?>
                        </td>
                    </tr>
                    <?php
                    $k = 1 - $k;
                }
                ?>
            </tbody>
        </table>
    </div>
    <input type="hidden" name="option" value="com_openshop" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />	
    <?php echo JHtml::_('form.token'); ?>			
</form>
</fieldset>