<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$editor = JFactory::getEditor(); 	
?>
<script type="text/javascript">	
	Joomla.submitbutton = function(pressbutton)
	{
		var form = document.adminForm;
		if (pressbutton == 'district.cancel') {
			Joomla.submitform(pressbutton, form);
			return;				
		} else {
			//Validate the entered data before submitting
			if (form.district_name.value == '') {
				alert("<?php echo JText::_('OPENSHOP_ENTER_NAME'); ?>");
				form.district_name.focus()
				return;;
			}
			if (form.parent_id.value == '') {
				alert("<?php echo JText::_('OPENSHOP_SELECT_COUNTRY'); ?>");
				form.country_id.focus();
				return;
			}
//			if (form.district_code.value == '') {
//				alert("<?php echo JText::_('OPENSHOP_ENTER_CODE'); ?>");
//				form.district_code.focus();
//				return;
//			}
			Joomla.submitform(pressbutton, form);
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
	<div class="row-fluid">
		<div class="span6">
			<table class="admintable adminform" style="width: 100%;">
				<tr>
					<td class="key">
						<span class="required">*</span>
						<?php echo  JText::_('OPENSHOP_NAME'); ?>
					</td>
					<td>
						<input class="text-xlarge" type="text" name="district_name" id="district_name" maxlength="128" value="<?php echo $this->item->district_name; ?>" />
					</td>
				</tr>				
				<tr>
					<td class="key">
						<span class="required">*</span>
						<?php echo  JText::_('OPENSHOP_TOWN'); ?>
					</td>
					<td >
						<?php echo $this->lists['town']; ?>
					</td>
				</tr>				
				<tr>
					<td class="key">
						<span class="required">*</span>
						<?php echo  JText::_('OPENSHOP_CODE'); ?>
					</td>
					<td>
						<input class="text-small" type="text" name="district_code" id="district_code" maxlength="32" value="<?php echo $this->item->district_code; ?>" />
					</td>
				</tr>				
				<tr>
					<td class="key">
						<?php echo JText::_('OPENSHOP_PUBLISHED'); ?>
					</td>
					<td>
						<?php echo $this->lists['published']; ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</fieldset>
	<div class="clearfix"></div>
	<?php echo JHtml::_( 'form.token' ); ?>
	<input type="hidden" name="option" value="com_openshop" />
	<input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="task" value="" />
</form>