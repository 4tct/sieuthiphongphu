<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/iosconfirms.js');
//print_r($this->items);exit();
?>
<style>
    .btnxacnhan{
        float: right;
    }

    #dataTables-example tr th, #dataTables-example tr td{
        text-align: center;
        vertical-align: middle;
    }
</style>

<fieldset class="adminfs">
    <form action="index.php?option=com_openshop&view=iosconfirms" method="post" name="adminForm" id="adminForm" >
        <table width="100%" style="margin-bottom: 5px;">
            <tr>
                <td align="left">
                    <?php echo JText::_('OPENSHOP_FILTER'); ?>:
                    <input type="text" name="search" id="search" value="<?php echo $this->state->search; ?>" class="text_area search-query" onchange="document.adminForm.submit();" />
                    <button onclick="this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_GO'); ?></button>
                    <button onclick="document.getElementById('search').value = '';this.form.submit();" class="btn"><?php echo JText::_('OPENSHOP_RESET'); ?></button>
                    <span class="btn btn-default hidden-phone" onclick="show_search_tools();"><?php echo JText::_('OPENSHOP_SEARCHTOOLS') ?><span class="icon-arrow-down-3 edit-arrow-icon"></span></span>
                </td>
            </tr>
        </table>

        <!--search tools-->
        <?php
        $display = "none";
        if ($this->state->export_warehouse_id != 0 || $this->state->import_warehouse_id != 0) {
            $display = "block";
        }
        ?>
        <div class="search-tools" style="display: <?php echo $display ?>;">
            <div class="search-tool-edit">
                <?php
                echo $this->lists['filter_warehouse_export'];
                ?>
            </div>
            <div class="search-tool-edit">
                <?php
                echo $this->lists['filter_warehouse_import'];
                ?>
            </div>

        </div>

        <div class="col-ms-12">
            <table class="adminlist table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th width="4%"><?php echo JText::_('OPENSHOP_IOSCOFIRM_AUTO'); ?></th>
                        <th width="4%">
                            <input class="cb" name="cbxacnhan" id="cbxacnhan" type="checkbox" onchange="checkIdIOAll()"/>
                        </th>
                        <th width="30%"><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
                        <th width="7%"><?php echo JText::_('OPENSHOP_SIZE_COLOR'); ?></th>
                        <th width="7%"><?php echo JText::_('OPENSHOP_SCAN'); ?></th>
                        <th width="7%"><?php echo JText::_('OPENSHOP_QUANTITY_EXPORT'); ?></th>
                        <th width="12%"><?php echo JText::_('OPENSHOP_EXPORT_PLACE'); ?></th>
                        <th width="12%"><?php echo JText::_('OPENSHOP_IMPORT_PLACE'); ?></th>
                        <th width="12%"><?php echo JText::_('OPENSHOP_EXPORT_DATE'); ?></th>
                        <th width="12%"><?php echo JText::_('OPENSHOP_EXPORT_BY'); ?></th>
                        <th><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($this->items as $key => $row) {
                        ?>
                        <tr class="odd gradeX" align="center">
                            <td><?php echo ++$key; ?></td>
                            <td>
                                <span class="icon-radio-unchecked" name="cbxacnhan_<?php echo $row->id; ?>" id="cbxacnhan_<?php echo $row->id; ?>" type="checkbox"
                                      onclick="checkitem(<?php echo $row->id; ?>)"></span>
                            </td>
                            <td><?php echo $row->product_name . '<br/><b>' . $row->product_sku . '</b>' ?></td>
                            <td>
                                <?php
                                echo $this->getNameSizeColor($row->id, $row->id_optionsize, 'size');
                                if (OpenShopHelper::getConfigValue('managesizecolor') == '2') {
                                    echo '<br/>-<br/>';
                                    echo $this->getNameSizeColor($row->id, $row->id_optioncolor, 'color');
                                }
                                ?>
                            </td>
                            <td>0</td>
                            <td><?php echo $row->quantity ?></td>
                            <td><?php echo $this->getNameWarehouse($row->id_warehouse); ?></td>
                            <td><?php echo $this->getNameWarehouse($row->id_warehouse_to); ?></td>
                            <td><?php echo date("H:m:s", strtotime($row->created_date)) . '<br/>' . date("d/m/Y", strtotime($row->created_date)); ?></td>
                            <td><?php echo OpenShopHelper::get1RecordTable('#__users', $row->created_by, 'name') ?></td>
                            <td><?php echo $row->description; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php
            $all = '';
            foreach ($this->items as $v) {
                $all .= '_' . $v->id;
            }
            ?>
            <input type="hidden" name="idAll" id="idAll" value=""/>
            <input type="hidden" name="idALLIO" id="idALLIO" value="<?php echo $all; ?>"/>
        </div>
    </form>
</fieldset>

<!--loading-->
<div class="loading" style="display: none;">
    <div class="loading_access" >
    </div>
    <div class="content_access">
        <img src="<?php echo OPENSHOP_ADMIN_PATH_IMG_HTTP . DS . 'loading_setup.gif' ?>" />
    </div>
</div>

<!-- Modal Export -->
<form action="index.php?option=com_openshop&format=ajax&task=product.exportExcelIO" method="post" id="exportExcelModal" enctype="multipart/form-data">
    <div class="modal fade" id="myModalExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Xuất File </h4>
                </div>
                <div class="modal-body form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3"> Dành cho </label>
                        <div class="btn-group col-md-9" role="group">
                            <button type="button" class="btn btn-default btn-primary active onchangeManu_1 onchangeManuReset" onclick="changeExportManu(1)">Lazada</button>
                            <button type="button" class="btn btn-default onchangeManu_2 onchangeManuReset" onclick="changeExportManu(2)">Nhà Cung Cấp</button>
                            <input type="hidden" name="typeExport" id="typeExport" value="1" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3"> Nơi xuất </label>
                        <div class="btn-group col-md-9" role="group">
                            <?php
                            echo $this->lists['warehouse_export'];
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3"> Nơi nhập </label>
                        <div class="btn-group col-md-9" role="group">
                            <?php
                            echo $this->lists['warehouse_import'];
                            ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary" onclick="exportExcel()">Xuất</button>
                </div>
            </div>
        </div>
    </div>
</form>