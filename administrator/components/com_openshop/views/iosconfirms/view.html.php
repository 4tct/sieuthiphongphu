<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('_JEXEC') or die('Restricted access');
class OpenShopViewIosconfirms extends OpenShopViewList {
    
    public function _buildListArray(&$lists, $state) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        //warehouse
        $query->select('id AS value, warehouse_name AS text')
                ->from($db->quoteName('#__openshop_warehouses'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '0', '- Nơi xuất -');
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['filter_warehouse_export'] = JHtml::_('select.genericlist', $options, 'export_warehouse_id', ' class="form-control select-width-180 inputbox" onchange="this.form.submit();" ', 'value', 'text', $state->export_warehouse_id);
        $lists['warehouse_export'] = JHtml::_('select.genericlist', $options, 'export_warehouse', ' class="form-control select-width-180 inputbox" ', 'value', 'text', '');
        
        $options = array();
        $options[] = JHtml::_('select.option', '0', '- Nơi nhập -');
        if (count($rows)) {
            $options = array_merge($options, $rows);
        }
        $lists['filter_warehouse_import'] = JHtml::_('select.genericlist', $options, 'import_warehouse_id', ' class="form-control select-width-180 inputbox" onchange="this.form.submit();" ', 'value', 'text', $state->import_warehouse_id);
        $lists['warehouse_import'] = JHtml::_('select.genericlist', $options, 'import_warehouse', ' class="form-control select-width-180 inputbox"', 'value', 'text', '');
    }
    
    function _buildToolbar() {
        $viewName = $this->getName();
        $controller = OpenShopInflector::singularize($this->getName());
        JToolBarHelper::title(JText::_($this->lang_prefix . '_' . strtoupper($viewName)));
        JToolBarHelper::custom('confirm', 'apply', 'apply', JText::_('OPENSHOP_CONFIRM'), false);
        JToolBarHelper::custom('export_excel', 'download', 'download', 'Export', false);
    }
    
    function getNameSizeColor($id_io,$id_sizecolor,$act){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.value')
                ->from($db->quoteName('#__openshop_ios','a'))
                ->where('a.id = ' . $id_io);
        switch ($act){
            case 'size':
                $query->join('INNER', $db->quoteName('#__openshop_optionvaluedetails','b') . 'ON a.id_optionsize = b.optionvalue_id')
                    ->where('b.optionvalue_id = ' . $id_sizecolor);
                break;
            case 'color':
                $query->join('INNER', $db->quoteName('#__openshop_optionvaluedetails','b') . 'ON a.id_optioncolor = b.optionvalue_id')
                    ->where('b.optionvalue_id = ' . $id_sizecolor);
                break;
            default:
                break;
        }
        
        return $db->setQuery($query)->loadResult();
    }
    
    function getNameWarehouse($id_warehouse){
        return OpenShopHelper::get1RecordTable('#__openshop_warehouses', $id_warehouse, 'warehouse_name');
    }
}