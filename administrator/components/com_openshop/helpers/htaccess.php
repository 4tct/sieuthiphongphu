<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');

class OpenShopPermissions {
    
    public static function CheckPermission() {
        $mainframe = JFactory::getApplication();
        if(OpenShopPermissions::checkPerOA())
        {
            $view = JFactory::getApplication()->input->getString('view');
            if($view == 'carts')        //check per cart
            {
                $view = 'transporter';
                
                $id_transporter = JFactory::getApplication()->input->getString('id_transporter');
                $db = JFactory::getDbo();
                $query = $db->getQuery(TRUE);
                $query->select('transporter_name')
                        ->from($db->quoteName('#__openshop_transporters'))
                        ->where('id = ' . $id_transporter);
                $trans_name = $db->setQuery($query)->loadResult();
                $trans_name = OpenShopHelper::stringAlias($trans_name);
                
                $view .= '_' . $trans_name;
                $view = strtoupper($view);
            }
            
            if(!OpenShopPermissions::checkPerView($view, JFactory::getUser()->id)){
                $mainframe->redirect('index.php');
            }
        }
        else
        {
            $mainframe->redirect('index.php?option=com_openshop&view=dashboard');
        }
    }

    public static function getPermissionsFromXmlFile() {
        $app = JFactory::getApplication();
        $user_id = JFactory::getUser()->id;
        $view = $app->input->getCmd('view');


//        if ($app->isAdmin() && $user_id > 0) {

            $xmlfile = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_openshop' . DS . 'access.xml';
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query->select("menu_name")
                    ->from('#__openshop_menus');
            $db->setQuery($query);
            $rs = $db->loadObjectList();
            $nums_rs = count($rs);
            $writer = new XMLWriter();
            $writer->openMemory();
            if (file_exists($xmlfile)) {
                $xml = simplexml_load_file($xmlfile);
                $permissions_file = $xml->section->action;
                $nums_access = count($permissions_file);

                if ($nums_rs != $nums_access) {
                    $writer->startDocument("1.0", "utf-8");
                    $writer->startElement("access");
                    $writer->writeAttribute("component", "com_openshop");
                    $writer->writeRaw("\n\t");
                    $writer->startElement("section");
                    $writer->writeAttribute("name", "component");
                    $writer->writeRaw("\n\t\t");
                    foreach ($rs as $row) {
                        $writer->writeRaw('<action name="' . strtolower(str_replace('_', '.', $row->menu_name)) . '" title="' . $row->menu_name . '" description="' . $row->menu_name . '_DESC"/>');
                        $writer->writeRaw("\n\t\t");
                    }
                    $writer->endElement();
                    $writer->endElement();
                    $writer->endDocument();
                    $file = fopen($xmlfile, "w");
                    fwrite($file, $writer->outputMemory());
                    fclose($file);
                }
            } else {
                $writer->startDocument("1.0", "utf-8");
                $writer->startElement("access");
                $writer->writeAttribute("component", "com_openshop");
                $writer->writeRaw("\n\t");
                $writer->startElement("section");
                $writer->writeAttribute("name", "component");
                $writer->writeRaw("\n\t\t");
                foreach ($rs as $row) {
                    $writer->writeRaw('<action name="' . strtolower(str_replace('_', '.', $row->menu_name)) . '" title="' . $row->menu_name . '" description="' . $row->menu_name . '_DESC"/>');
                    $writer->writeRaw("\n\t\t");
                }
                $writer->endElement();
                $writer->endElement();
                $writer->endDocument();
                $file = fopen($xmlfile, "w");
                fwrite($file, $writer->outputMemory());
                fclose($file);

                $xml = simplexml_load_file($xmlfile);
                $permissions_file = $xml->section->action;
            }
            return $permissions_file = $xml->section->action;
//        } else {
//            $app->enqueueMessage(JText::_('OPENSHOP_ACCESS_NOT_ALLOW'), 'error');
//            $app->redirect('index.php?option=com_openshop&view=dashboard');
//        }
    }

    public static function checkPerOA() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__usergroups'))
                ->where('LOWER(title) = "operation administrator"');
        return $db->setQuery($query)->loadResult();
    }

    public static function checkPerView($nameView, $id_user) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('group_id')
                ->from($db->quoteName('#__user_usergroup_map'))
                ->where('user_id = ' . $id_user);
        $id_usergroup = $db->setQuery($query)->loadObjectList();    //get all user group

        $menu_name = 'OPENSHOP_' . strtoupper($nameView);
        
        $query->clear();
        $query->select('permission')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('menu_name = "' . $menu_name . '"');
        $per = json_decode($db->setQuery($query)->loadResult(), TRUE);  //array
        $checkView = 0;
        foreach($id_usergroup as $id){
            $checkView = $per[$id->group_id]['view'];
            if($checkView){
                break;
            }
        }
        
        return $checkView;
    }
    
    public static function checkPerAction($nameView, $id_user, $action) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('group_id')
                ->from($db->quoteName('#__user_usergroup_map'))
                ->where('user_id = ' . $id_user);
        $id_usergroup = $db->setQuery($query)->loadResult();

        $menu_name = 'OPENSHOP_' . strtoupper($nameView);
        $query->clear();
        $query->select('permission')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('menu_name = "' . $menu_name . '"');
        $per = json_decode($db->setQuery($query)->loadResult(), TRUE);

        $checkView = $per[$id_usergroup][$action];
        return $checkView;
    }
    
    public static function checkAdmin($groups){
        $check = 0;
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        foreach ($groups as $group) {
            $query->clear();
            $query->select('count(id)')
                    ->from($db->quoteName('#__usergroups'))
                    ->where('UPPER(title) = "ADMINISTRATOR"')
                    ->where('id = ' . $group);
            $grp = $db->setQuery($query)->loadResult();
            if($grp){
                $check = 1;
            }
        }
        
        return $check;
    }
    
    public static function checkSuperUser($groups){
        $check = 0;
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        foreach ($groups as $group) {
            $query->clear();
            $query->select('count(id)')
                    ->from($db->quoteName('#__usergroups'))
                    ->where('UPPER(title) = "SUPER USERS"')
                    ->where('id = ' . $group);
            $grp = $db->setQuery($query)->loadResult();
            if($grp){
                $check = 1;
                break;
            }
        }
        
        return $check;
    }

}
