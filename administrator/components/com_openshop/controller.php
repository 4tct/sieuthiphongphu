<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopController extends JControllerLegacy {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * Display information
     *
     */
    function display($cachable = false, $urlparams = false) {
        $task = $this->getTask();
        $view = JRequest::getVar('view', '');
        if (!$view) {
            JRequest::setVar('view', 'dashboard');
        }
        if(OpenShopPermissions::checkSuperUser(JFactory::getUser()->groups)){
            OpenShopHelper::renderSubmenuPer(JRequest::getVar('view', 'configuration'));
        }
        ?>
        <script type="text/javascript">

            function removecssjsfile(filename, type)
            {
                var ele = (type == 'js') ? 'script' : (type == 'css') ? 'link' : 'none';
                var attr = (type == 'js') ? 'src' : (type == 'css') ? 'href' : 'none';
                var t = document.getElementsByTagName(ele);
                for (i = 0; i < t.length; i++)
                {
                    if (t[i][attr].search(filename) != -1)
                    {
                        t[i].remove();
                    }
                }
            }

//            removecssjsfile('jquery.min.js', 'js');
//            removecssjsfile('jquery-noconflict.js', 'js');

        </script>
        <?php
        parent::display();
    }

}
