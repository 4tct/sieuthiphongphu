<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');

class JFormFieldOpenShopMedia extends JFormField
{

	/**
	 * Element name
	 *
	 * @access	protected
	 * @var		string
	 */
        var $_name = 'media';

	function getInput()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id as value,media_name as text')
                        ->from($db->quoteName('#__openshop_medias'))
			->where('published = 1');
                        
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$brand = JHtml::_('select.genericlist', $rows, $this->name, 
			array(
				'option.text.toHtml' => false, 
				'option.value' => 'value', 
				'option.text' => 'text', 
				'list.attr' => ' class="inputbox"', 
				'list.select' => $this->value));
		return $brand;
	}
}