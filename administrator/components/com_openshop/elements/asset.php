<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');

class JFormFieldAsset extends JFormField {
    protected $type = 'Asset';
    protected function getInput() {
        $doc = JFactory::getDocument();
        $doc->addScript(JURI::root().$this->element['path'].'script.js');
        $doc->addStyleSheet(JURI::root().$this->element['path'].'style.css');        
        return null;
    }
}