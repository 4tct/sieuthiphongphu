<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');

//Require the controller
//OS Framework
require_once JPATH_ROOT.'/components/com_openshop/helpers/toggle.php';
require_once JPATH_ROOT.'/administrator/components/com_openshop/libraries/defines.php';
require_once JPATH_ROOT.'/administrator/components/com_openshop/libraries/inflector.php';
require_once JPATH_ROOT.'/administrator/components/com_openshop/libraries/autoload.php';
require_once JPATH_ROOT.'/administrator/components/com_openshop/helpers/htaccess.php';
require_once JPATH_ADMINISTRATOR . '/components/com_openshop/libraries/rad/bootstrap.php';

$command = JRequest::getVar('task', 'display');

// Check for a controller.task command.
if (strpos($command, '.') !== false)
{
	list ($controller, $task) = explode('.', $command);	
	$path = JPATH_COMPONENT . '/controllers/' . $controller.'.php';
	if (file_exists($path)) {		
		require_once $path;
		$className      =  'OpenShopController'.ucfirst($controller);
		$controller	= new $className();
	} else {
		//Fallback to default controller
		$controller = new OpenShopController( array('entity_name' => $controller, 'name' => 'OpenShop'));	
	}	
		
	JRequest::setVar('task', $task);
}
else
{			
	$path =  JPATH_COMPONENT.'/controller.php';	
	require_once $path;	
	$controller	= new OpenShopController();
}

OpenShopHelper::addCustoms();

JFactory::getDocument()->addStyleSheet(JURI::base().'components/com_openshop/assets/css/style.css');
JFactory::getDocument()->addStyleSheet(JURI::base().'components/com_openshop/assets/css/style_admin.css');
JFactory::getDocument()->addStyleSheet(JURI::base().'components/com_openshop/assets/css/toastr.min.css');
JFactory::getDocument()->addScript(JUri::base() . 'components/com_openshop/assets/js/openshop.js');
JFactory::getDocument()->addScript(JUri::base() . 'components/com_openshop/assets/js/toastr.min.js');

// Perform the Request task
$controller->execute( JRequest::getCmd('task'));
$controller->redirect();
?>
