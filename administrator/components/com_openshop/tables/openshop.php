<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * Config Table class
 */
class ConfigOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_configs', 'id', $db);
	}
}

/**
 * Address Table class
 */
class AddressOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_addresses', 'id', $db);
	}
}

/**
 * Customer Table class
 */
class CustomerOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_customers', 'id', $db);
	}
}

/**
 * Order Table class
 */
class OrderOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_orders', 'id', $db);
	}
}

/**
 * Order Products Table class
 */
class OrderproductsOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_orderproducts', 'id', $db);
	}
}

/**
 * Order Options Table class
 */
class OrderoptionsOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_orderoptions', 'id', $db);
	}
}

/**
 * Order Downloads Table class
 */
class OrderdownloadsOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_orderdownloads', 'id', $db);
	}
}

/**
 * Order Totals Table class
 */
class OrdertotalsOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_ordertotals', 'id', $db);
	}
}

/**
 * Quote Table class
 */
class QuoteOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_quotes', 'id', $db);
	}
}

/**
 * Quote Products Table class
 */
class QuoteproductsOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_quoteproducts', 'id', $db);
	}
}

/**
 * Quote Options Table class
 */
class QuoteoptionsOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_quoteoptions', 'id', $db);
	}
}

/**
 * Coupon History Table class
 */
class CouponhistoryOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_couponhistory', 'id', $db);
	}
}

/**
 * Voucher History Table class
 */
class VoucherhistoryOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_voucherhistory', 'id', $db);
	}
}

/**
 * Review Table class
 */
class ReviewOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_reviews', 'id', $db);
	}
}

/**
 * Custom field Table class
 */
class FieldOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_fields', 'id', $db);
	}
}

/**
 * Wishlist Table class
 */
class WishlistOpenShop extends JTable
{

	/**
	 * Constructor
	 *
	 * @param
	 *        	object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db)
	{
		parent::__construct('#__openshop_wishlists', 'id', $db);
	}
}