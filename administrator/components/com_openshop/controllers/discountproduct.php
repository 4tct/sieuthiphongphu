<?php
defined('_JEXEC') or die('Restricted access');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of product
 *
 * @author HUY HUYNH
 */
class OpenShopControllerDiscountProduct extends OpenShopController {
    function __construct($config = array()) {
        parent::__construct($config);
    }
    
    function searchProductDiscount()
    {
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $key = $app->getString('key');
        
        $query->select('b.product_name,a.product_image,a.product_price,a.product_sku')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . 'ON a.id = b.product_id')
                ->where('a.product_sku LIKE "%'. $key .'%"');
        echo (json_encode($db->setQuery($query)->loadObjectList()));
    }
}