<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.model');
JLoader::import('group', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'models');

class OpenShopControllerPermissions extends JControllerLegacy {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    public function save_per() {
        $res = array('status' => 'warning', 'message' => JText::_('OPENSHOP_FAILD'));
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $data = JFactory::getApplication()->input->getString('data');
        $value_permission = JFactory::getApplication()->input->getString('value');

        foreach ($data as $value) {
            $arr_s = explode('_', $value);
            $menu_name = '';
            $c = 1;
            for ($i = 1; $i < count($arr_s); $i++) {
                if ($arr_s[$i] != 'core') {
                    if ($c == 1) {
                        $menu_name .= $arr_s[$i];
                        $c = 0;
                    } else {
                        $menu_name .= '_' . $arr_s[$i];
                    }
                } else {
                    break;
                }
            }

            /*
             * Check all Permission in MENU
             */
            $query->clear();
            $query->select('id,title')->from($db->quoteName('#__usergroups'));
            $rows_usg = $db->setQuery($query)->loadObjectList();
            
            $query->clear();
            $query->select('permission')
                    ->from($db->quoteName('#__openshop_menus'))
                    ->where('menu_name = "' . strtoupper($menu_name) . '"');
            $r = $db->setQuery($query)->loadResult();

            if (!empty($r)) {
                $rows_menu = json_decode($r, TRUE);
            } else {
                $rows_menu = array();
            }

            //create permission default
            foreach ($rows_usg as $value) {
                $id_per = $value->id;
                $usg_name = isset($rows_menu[$id_per]) ? $rows_menu[$id_per] : '';
                if (empty($usg_name)) { //if not permission => add
                    $rows_menu[$id_per] = array(
                        'view' => 0,
                        'create' => 0,
                        'delete' => 0,
                        'edit' => 0,
                        'editstate' => 0
                    );
                }
            }
            /*
             * / END - Check all Permission in MENU
             */

            $query->clear();
            $query->select('id,title')->from($db->quoteName('#__usergroups'))->where('id = ' . $arr_s[0]);
            $id_permission = $db->setQuery($query)->loadObject();      //get usergroup of id

            $id_per = $id_permission->id;
            if (!empty($rows_menu[$id_per])) {
                foreach ($rows_menu[$id_per] as $key => $v) {
                    if ($key == $arr_s[count($arr_s) - 1]) {
                        $rows_menu[$id_per][$key] = $value_permission;
                    } else {
                        $rows_menu[$id_per][$key] = $v;
                    }
                };

                $query->clear();
                $query->update($db->quoteName('#__openshop_menus'))
                        ->set("permission = '" . json_encode($rows_menu) . "'")
                        ->where('menu_name = "' . strtoupper($menu_name) . '"');
                if ($db->setQuery($query)->execute()) {
                    $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
                }
            }
        }

        echo json_encode($res);
    }

    public function apply_access() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,title')
                ->from($db->quoteName('#__usergroups'));
        $rows_usg = $db->setQuery($query)->loadObjectList();

        $query->clear();
        $query->select('menu_name,permission')
                ->from($db->quoteName('#__openshop_menus'));
        $rows_permission = $db->setQuery($query)->loadObjectList();

        $permission = array();
        foreach ($rows_permission as $value) {
            $permission[strtolower($value->menu_name)] = $value->permission;
        }

        $path_file_per = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_openshop' . DS . 'permissions.xml';
        $fp = fopen($path_file_per, 'w+');
        $content = '';
        $content .= '<?xml version="1.0" encoding="UTF-8"?>';
        $content .= "\r\n" . '<access component="com_openshop">';

        foreach ($rows_usg as $value) {
            $content .= "\r\n\t" . '<permision name="' . $value->id . '">';
            foreach ($permission as $k => $v) {
                $content .= "\r\n\t\t" . '<section name="' . strtolower($k) . '">';
                if (!empty($v)) {
                    $value_permission = json_decode($v, TRUE);
//                    $content .= "\r\n\t\t\t" . '<action name="'. $value_permission[ strtolower($value->title) ]['select'] .'" title="JACTION_SELECT" description="JACTION_SELECT_COMPONENT_DESC" />';
                    foreach ($value_permission[$value->id] as $k_a => $v_a) {
                        if ($v_a == '1') {
                            $content .= "\r\n\t\t\t" . '<action name="core.' . $k_a . '" title="JACTION_' . strtoupper($k_a) . '" description="JACTION_' . strtoupper($k_a) . '_COMPONENT_DESC" />';
                        }
                    }
                }

                $content .= "\r\n\t\t" . '</section>';
            }

            $content .= "\r\n\t" . '</permision>';
        }



        $content .= "\r\n" . '</access>';
        fwrite($fp, $content);
        fclose($fp);
    }

    function save_per_new() {
        $app = JFactory::getApplication()->input;
        $name = $app->getString('per_name');
        $id_parent = $app->getInt('id_per');

        //get other model in different component
        JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'models');

        $usergroup_model = JModelLegacy::getInstance('group', 'UsersModel');    //get model

        $data = array();
        $data['title'] = $name;
        $data['parent_id'] = $id_parent;
        $data['tags'] = '';
        $data['id'] = 0;

        if ($usergroup_model->save($data)) {
            $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
        } else {
            $res = array('status' => 'warning', 'message' => JText::_('OPENSHOP_FAILD'));
        }

        echo json_encode($res);
    }

    function checkPermissionName() {
        $app = JFactory::getApplication()->input;
        $per_name = $app->getString('per_name');

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__usergroups'))
                ->where('LOWER(title) = "' . strtolower($per_name) . '"');

        $res = $db->setQuery($query)->loadResult();

        if ($res) {
            echo 'exists';     //exists
        } else {
            echo 'dexists';   //don't exists
        }
    }

    function delete_per() {
        $app = JFactory::getApplication()->input;
        $id_per = $app->getInt('id');

        if ($id_per != '0' && !empty($id_per)) {
            JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'models');
            $usergroup_model = JModelLegacy::getInstance('group', 'UsersModel');    //get model

            $data = array('0' => $id_per);

            if ($usergroup_model->delete($data)) {
                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
            } else {
                $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD'));
            }
        } else {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD'));
        }

        echo json_encode($res);
    }

}
