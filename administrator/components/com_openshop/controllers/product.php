<?php

defined('_JEXEC') or die('Restricted access');

require_once OPENSHOP_ADMIN_COMPONENTS . '/libraries/model/product.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of product
 *
 * @author HUY HUYNH
 */
class OpenShopControllerProduct extends OpenShopController {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    public function getProductInOrder() {
        $id_pro = JFactory::getApplication()->input->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.id as id_order,c.fullname,c.telephone,c.email,h.zone_name,e.district_name,d.address,c.customer_id,a.product_name,a.product_sku,a.quantity')
                ->from($db->quoteName('#__openshop_orderproducts', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_orders', 'b') . 'ON a.order_id = b.id')
                ->join('LEFT', $db->quoteName('#__openshop_customers', 'c') . 'ON b.customer_id = c.customer_id')
                ->join('LEFT', $db->quoteName('#__openshop_addresses', 'd') . 'ON d.customer_id = c.customer_id')
                ->join('LEFT', $db->quoteName('#__openshop_districts', 'e') . 'ON e.id = d.district_id')
                ->join('LEFT', $db->quoteName('#__openshop_zones', 'h') . 'ON h.id = d.zone_id')
                ->where('a.product_id = ' . $id_pro)
                ->order('b.created_date DESC');
        echo json_encode($db->setQuery($query, 0, 20)->loadObjectList());
    }

    public function restore_product() {
        $app = JFactory::getApplication()->input;
        $id = $app->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->update($db->quoteName('#__openshop_products'))
                    ->set('delete_status = 0')
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                echo 'success';
            } else {
                echo 'error';
            }
        } catch (Exception $ex) {
            echo 'error';
        }
    }

    public function updateNew() {
        try {
            $app = JFactory::getApplication()->input;
            $valUp = $app->getInt('valUp');
            $id = $app->getInt('idP');
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->update('#__openshop_products')
                    ->set('product_new = ' . $valUp)
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                if ($valUp == 1) {
                    $mes = 'Đã thêm vào sản phẩm mới';
                } else {
                    $mes = 'Đã hủy bỏ sản phẩm mới';
                }
                $res = array(
                    'status' => 'success',
                    'message' => $mes,
                );
            } else {
                $res = array(
                    'status' => 'warning',
                    'message' => 'Hệ thống đang quá tải. Vui lòng refresh lại',
                );
            }
            echo json_encode($res);
        } catch (Exception $ex) {
            $res = array(
                'status' => 'error',
                'message' => 'Không kết nối được với máy chủ',
            );
            echo json_encode($res);
        }
    }

    public function updateHOT() {
        try {
            $app = JFactory::getApplication()->input;
            $valUp = $app->getInt('valUp');
            $id = $app->getInt('idP');
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->update('#__openshop_products')
                    ->set('product_hot = ' . $valUp)
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                if ($valUp == 1) {
                    $mes = 'Đã thêm vào sản phẩm HOT';
                } else {
                    $mes = 'Đã hủy bỏ sản phẩm HOT';
                }
                $res = array(
                    'status' => 'success',
                    'message' => $mes,
                );
            } else {
                $res = array(
                    'status' => 'warning',
                    'message' => 'Hệ thống đang quá tải. Vui lòng refresh lại',
                );
            }
            echo json_encode($res);
        } catch (Exception $ex) {
            $res = array(
                'status' => 'error',
                'message' => 'Không kết nối được với máy chủ',
            );
            echo json_encode($res);
        }
    }

    /*
     * lấy danh sách tìm kiếm sản phẩm theo từ khoá
     */

    function getProductSearch() {
        $app = JFactory::getApplication()->input;
        $key = $app->getString('v');
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {

            $query->select('a.id,b.product_name,a.product_image,a.product_price,a.product_price_r')
                    ->from($db->quoteName('#__openshop_products', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id')
                    ->where('b.product_name LIKE "%' . $key . '%"');

            $rows = $db->setQuery($query)->loadObjectList();

            $res = array();
            $html = '';

            if (count($rows)) {
                foreach ($rows as $row) {
                    $html .= '<li class="li_' . $row->id . '" onclick="setChooseProduct(' . $row->id . ', \'' . $row->product_name . '\',\'' . OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image . '\')">';
                    $html .= '  <div class="col-md-3" style="padding-left: 10px;">';
                    $html .= '      <img src="' . OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image . '" width="50px"/>';
                    $html .= '  </div>';
                    $html .= '  <div class="col-md-9" style="padding-left: 0;">';
                    $html .= '      <div class="product_name">';
                    $html .= '          ' . $row->product_name;
                    $html .= '      </div>';
                    $html .= '      <div class="product_price">';
                    $html .= '          <span class="buy">' . number_format($row->product_price, 0, ',', '.') . '<sup>đ</sup></span>';

                    if ($row->product_price_r > 0) {
                        $html .= '          <span class="notbuy">' . number_format($row->product_price_r, 0, ',', '.') . '<sup>đ</sup></span>';
                    }
                    $html .= '      </div>';
                    $html .= '  </div>';
                    $html .= '<div style="clear: both;"></div>';
                    $html .= '</li>';
                }
            } else {
                $html .= '<li>Không có sản phẩm</li>';
            }

            $res['html'] = $html;

            echo json_encode($res);
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    /*
     * 
     */

    function getIDNameCategory($id, $name) {
        $res = array();
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,b.category_name')
                ->from($db->quoteName('#__openshop_categories', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_categorydetails', 'b') . 'ON a.id = b.category_id')
                ->where('category_parent_id = ' . $id);
        $cats = $db->setQuery($query)->loadObjectList();

        if (count($cats)) {
            foreach ($cats as $cat) {

                $id = $cat->id;
                $name = $name . '/' . $cat->category_name;

                self::getIDNameCategory($id, $name);
            }
        } else {
            $res[] = $id . '-' . $name;
        }
    }

    function showCategories($categories, $parent_id = 0, $char = '', &$res = array()) {
        foreach ($categories as $key => $item) {

            // Nếu là chuyên mục con thì hiển thị

            if ($item->parent_id == $parent_id) {

                $res[$item->id] = $item->id . '-' . $char . $item->title;

                // Xóa chuyên mục đã lặp
                unset($categories[$key]);

                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                self::showCategories($categories, $item->id, $char . $item->title . '/', $res);
            }
        }
    }

    /*
     * patern import
     */

    function paternimportProduct() {
        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);

        $phpExcel = new PHPExcel();

        //Category Líst
        $objWorkSheet = $phpExcel->createSheet(1); //Setting index when creating
        $objWorkSheet->setTitle('Category');
        $i = 2;
        $objWorkSheet->setCellValue('A' . $i, 'Mã');
        $objWorkSheet->setCellValue('B' . $i, 'Loại sản phẩm');

        $query->select('a.category_parent_id as parent_id,b.category_name as title,a.id')
                ->from($db->quoteName('#__openshop_categories', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_categorydetails', 'b') . 'ON a.id = b.category_id');
        $cats = $db->setQuery($query)->loadObjectList();

        if (count($cats)) {
            foreach ($cats as $v) {
                $pt = $v->parent_id;
                $list = @$children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
        }
        $res = array();
        self::showCategories($cats, 0, '', $res);
        $i++;
        $total_cat_start = $i;
        foreach ($res as $key => $r) {
            $objWorkSheet->setCellValue('A' . $i, $key);
            $objWorkSheet->setCellValue('B' . $i, $r);
            $i++;
        }
        $total_cat_finish = $i;

        //Brand List
        $objWorkSheet = $phpExcel->createSheet(2); //Setting index when creating
        $objWorkSheet->setTitle('Brand');
        $i = 2;
        $objWorkSheet->setCellValue('A' . $i, 'Mã');
        $objWorkSheet->setCellValue('B' . $i, 'Tên thương hiệu');

        //thương hiệu
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_branddetails'));
        $brands = $db->setQuery($query)->loadObjectList();

        $i++;
        $total_brand_start = $i;
        foreach ($brands as $brand) {
            $objWorkSheet->setCellValue('A' . $i, $brand->brand_id);
            $objWorkSheet->setCellValue('B' . $i, $brand->brand_id . '-' . $brand->brand_name);
            $i++;
        }
        $total_brand_finish = $i;


        //Manufacture List
        $objWorkSheet = $phpExcel->createSheet(3); //Setting index when creating
        $objWorkSheet->setTitle('Manf');
        $i = 2;
        $objWorkSheet->setCellValue('A' . $i, 'Mã');
        $objWorkSheet->setCellValue('B' . $i, 'Tên nhà cung cấp');

        //
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_manufacturers'));
        $manf = $db->setQuery($query)->loadObjectList();

        $i++;
        $total_manf_start = $i;
        foreach ($manf as $m) {
            $objWorkSheet->setCellValue('A' . $i, $m->id);
            $objWorkSheet->setCellValue('B' . $i, $m->id . '-' . $m->manufacturer_name);
            $i++;
        }
        $total_manf_finish = $i;


        //Manufacture List
        $objWorkSheet = $phpExcel->createSheet(4); //Setting index when creating
        $objWorkSheet->setTitle('Agency');
        $i = 2;
        $objWorkSheet->setCellValue('A' . $i, 'Mã');
        $objWorkSheet->setCellValue('B' . $i, 'Tên chi nhánh');

        //
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_argencies'));
        $agencies = $db->setQuery($query)->loadObjectList();

        $i++;
        $total_agencies_start = $i;
        foreach ($agencies as $a) {
            $objWorkSheet->setCellValue('A' . $i, $a->id);
            $objWorkSheet->setCellValue('B' . $i, $a->id . '-' . $a->argency_name);
            $i++;
        }
        $total_agencies_finish = $i;

        //Color List
        $objWorkSheet = $phpExcel->createSheet(5); //Setting index when creating
        $objWorkSheet->setTitle('Color');
        $i = 2;
        $objWorkSheet->setCellValue('A' . $i, 'Mã');
        $objWorkSheet->setCellValue('B' . $i, 'Tên màu');

        //
        $query->clear();
        $query->select('c.id, b.value')
                ->from($db->quoteName('#__openshop_optiondetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . ' ON a.option_id = b.option_id')
                ->join('INNER', $db->quoteName('#__openshop_optionvalues', 'c') . ' ON c.id = b.optionvalue_id')
                ->where('c.published = 1')
                ->where('a.option_name = "COLOR"');
        $colors = $db->setQuery($query)->loadObjectList();

        $i++;
        $total_color_start = $i;
        foreach ($colors as $color) {
            $objWorkSheet->setCellValue('A' . $i, $color->id);
            $objWorkSheet->setCellValue('B' . $i, $color->id . '-' . $color->value);
            $i++;
        }
        $total_color_finish = $i;




        // Add new sheet
        $objWorkSheet = $phpExcel->createSheet(0); //Setting index when creating
        $objWorkSheet->setTitle('Import');

        //Tiêu đề Báo Cáo trong mỗi sheet
        $i = 4;
        $objWorkSheet->setCellValue('A' . $i, 'STT');
        $objWorkSheet->setCellValue('B' . $i, 'Nhập nhanh');
        $objWorkSheet->setCellValue('C' . $i, 'Loại sản phẩm');
        $objWorkSheet->setCellValue('D' . $i, 'Mã sản phẩm');
        $objWorkSheet->setCellValue('E' . $i, 'Tên sản phẩm');
        $objWorkSheet->setCellValue('F' . $i, 'Thương hiệu');
        $objWorkSheet->setCellValue('G' . $i, 'Nhà cung cấp');
        $objWorkSheet->setCellValue('H' . $i, 'Chi nhánh');
        $objWorkSheet->setCellValue('I' . $i, 'Giá nhập');
        $objWorkSheet->setCellValue('J' . $i, 'Giá bán');
        $objWorkSheet->setCellValue('K' . $i, 'Giá gốc');
        $objWorkSheet->setCellValue('L' . $i, 'Người mua');
        $objWorkSheet->setCellValue('M' . $i, 'Kích thước');
        $objWorkSheet->setCellValue('N' . $i, 'Màu sắc');
        $objWorkSheet->setCellValue('O' . $i, 'Chiều rộng');
        $objWorkSheet->setCellValue('P' . $i, 'Chiều dài');
        $objWorkSheet->setCellValue('Q' . $i, 'Phát hành');
        $objWorkSheet->setCellValue('R' . $i, 'Sản phẩm HOT');
        $objWorkSheet->setCellValue('S' . $i, 'Sản phẩm Mới');
        $objWorkSheet->setCellValue('T' . $i, 'Hình ảnh');

        //set required
        $objWorkSheet->getStyle('C' . $i . ':E' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff7878');
        $objWorkSheet->getStyle('I' . $i . ':J' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff7878');
        $objWorkSheet->getStyle('M' . $i . ':N' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff7878');

        $objWorkSheet->getColumnDimension('B')->setWidth(12);
        $objWorkSheet->getColumnDimension('C')->setWidth(20);
        $objWorkSheet->getColumnDimension('D')->setWidth(20);
        $objWorkSheet->getColumnDimension('E')->setWidth(20);
        $objWorkSheet->getColumnDimension('F')->setWidth(20);
        $objWorkSheet->getColumnDimension('G')->setWidth(20);
        $objWorkSheet->getColumnDimension('H')->setWidth(20);
        $objWorkSheet->getColumnDimension('I')->setWidth(20);
        $objWorkSheet->getColumnDimension('J')->setWidth(20);
        $objWorkSheet->getColumnDimension('K')->setWidth(20);
        $objWorkSheet->getColumnDimension('L')->setWidth(20);
        $objWorkSheet->getColumnDimension('M')->setWidth(20);
        $objWorkSheet->getColumnDimension('N')->setWidth(20);
        $objWorkSheet->getColumnDimension('O')->setWidth(20);
        $objWorkSheet->getColumnDimension('P')->setWidth(20);
        $objWorkSheet->getColumnDimension('Q')->setWidth(20);
        $objWorkSheet->getColumnDimension('R')->setWidth(20);
        $objWorkSheet->getColumnDimension('S')->setWidth(20);

        $i++;

        for ($i; $i < 200; $i++) {
            //tạo drop-down loại sản phẩm
            $objValidation2 = $objWorkSheet->getCell('C' . $i)->getDataValidation();
            $objValidation2->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation2->setAllowBlank(false);
            $objValidation2->setShowInputMessage(true);
            $objValidation2->setShowDropDown(true);
            $objValidation2->setPromptTitle('Loại sản phẩm');
            $objValidation2->setPrompt('Có thể nhập mã của loại sản phẩm');

            $objValidation2->setFormula1('Category!$B$' . $total_cat_start . ':$B$' . $total_cat_finish);

            $objWorkSheet->setCellValue('C' . $i, '=IF(B' . $i . '="","",VLOOKUP(B' . $i . ',Category!$A$' . $total_cat_start . ':$B$' . $total_cat_finish . ',2,0))');


            //tạo drop-down thương hiệu
            $objValidation2 = $objWorkSheet->getCell('F' . $i)->getDataValidation();
            $objValidation2->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation2->setAllowBlank(false);
            $objValidation2->setShowInputMessage(true);
            $objValidation2->setShowDropDown(true);
            $objValidation2->setPromptTitle('Thương hiệu');
            $objValidation2->setPrompt('Thương hiệu');
            $objValidation2->setFormula1('Brand!$B$' . $total_brand_start . ':$B$' . $total_brand_finish);

            //tạo drop-down nhà cung cấp
            $objValidation2 = $objWorkSheet->getCell('G' . $i)->getDataValidation();
            $objValidation2->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation2->setAllowBlank(false);
            $objValidation2->setShowInputMessage(true);
            $objValidation2->setShowDropDown(true);
            $objValidation2->setPromptTitle('Nhà cung cấp');
            $objValidation2->setPrompt('Nhà cung cấp');
            $objValidation2->setFormula1('Manf!$B$' . $total_manf_start . ':$B$' . $total_manf_finish);

            //tạo drop-down chi nhánh
            $objValidation2 = $objWorkSheet->getCell('H' . $i)->getDataValidation();
            $objValidation2->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation2->setAllowBlank(false);
            $objValidation2->setShowInputMessage(true);
            $objValidation2->setShowDropDown(true);
            $objValidation2->setPromptTitle('Chi nhánh');
            $objValidation2->setPrompt('Chi nhánh');
            $objValidation2->setFormula1('Agency!$B$' . $total_manf_start . ':$B$' . $total_manf_finish);

            //tạo drop-down chi nhánh
            $objValidation2 = $objWorkSheet->getCell('N' . $i)->getDataValidation();
            $objValidation2->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation2->setAllowBlank(false);
            $objValidation2->setShowInputMessage(true);
            $objValidation2->setShowDropDown(true);
            $objValidation2->setPromptTitle('Màu');
            $objValidation2->setPrompt('Chọn màu sắc');
            $objValidation2->setFormula1('Color!$B$' . $total_color_start . ':$B$' . $total_color_finish);

            //set default
            $objWorkSheet->setCellValue('O' . $i, '0');
            $objWorkSheet->setCellValue('P' . $i, '0');
            $objWorkSheet->setCellValue('Q' . $i, '0');
            $objWorkSheet->setCellValue('R' . $i, '1');
            $objWorkSheet->setCellValue('S' . $i, '1');
        }

        $write = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');

        //        $write->save(OPENSHOP_PATH_FILES . 'test.xlsx');                      // only save + not download
        // We'll be outputting an excel file
        //        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="Mau_Import_San_Pham.xls"');

        // Write file to the browser
        $write->save('php://output');
    }

    function getCategoryPro($id) {
        return array('31', '32');
    }

    function exportProduct($filename) {

        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);

        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objPHPExcel = $objReader->load($filename);
        $objPHPExcel->setActiveSheetIndex(0);
        //tổng số dòng hiện có
        $maxRow = $objPHPExcel->getActiveSheet()->getHighestRow();
//        echo $objPHPExcel->getActiveSheet()->getCell('B1')->getValue();
        //gọi model Product -> sử dụng config ->export 
        $Product = new Product();
        $config = $Product->consfig_export;

        $columnInsert = join(',', $config['column']);

        //tạo đối tượng để insert
//        $data = new stdClass();

        $t = 1;
        for ($i = $config['start']; $i <= $maxRow; $i++) {
            $q = 'INSERT INTO ' . $Product->table . ' (' . $columnInsert . ') VALUES';

            $tc = 1;
            $q .= ' (';
            foreach ($config['column'] as $k => $v) {
                if ($tc == 1) {
                    $q .= '"' . $objPHPExcel->getActiveSheet()->getCell($k . $i)->getValue() . '"';
                    $tc = 0;
                } else {
                    $q .= ',"' . $objPHPExcel->getActiveSheet()->getCell($k . $i)->getValue() . '"';
                }
            }

            $q .= ')';
            //insert vào bảng chính
//            $db->setQuery($q)->execute();

            $id = '1';
            $db->insertid();

            //kiểm tra có insert vào bảng phụ k?
            if ($config['join'] == '1') {     //insert vào cá bảng khác
                $tbjoin = $config['tablejoin'];
                foreach ($tbjoin as $kj => $tj) {

                    $qjoin = 'INSERT INTO ' . $kj . ' (' . $tj['id'] . ',' . join(',', $tj['columnjoin']) . ') VALUES';


                    if (isset($tj['constant']) && $tj['constant']) {    //insert nhiểu + sử dụng 1 id
                        $function = $tj['function'];

                        $values = self::$function();    //gọi hàm -> trả về 1 mảng

                        foreach ($values as $k_const => $v_const) {
                            if ($k_const != '0') {
                                $qjoin .= ',';
                            }

                            $qjoin .= ' (';

                            foreach ($tj['columnjoin'] as $kj_column => $v_column) {
                                $qjoin .= $id;
                                $qjoin .= ',"' . $v_const . '"';
                            }

                            $qjoin .= ')';
                        }
                    } else {
                        $qjoin .= ' (';
                        foreach ($tj['columnjoin'] as $kj_column => $v_column) {
                            $qjoin .= $id;
                            $qjoin .= ',"' . $objPHPExcel->getActiveSheet()->getCell($kj_column . $i)->getValue() . '"';
                        }
                        $qjoin .= ')';
                    }



                    echo $qjoin;
                    //insert vào bảng phụ
//                    $db->setQuery($qjoin)->execute();
                }
            }
        }

        //xoá file
        unlink($filename);
    }

    function exportProductValue($filename) {
        $user = JFactory::getSession()->get('user');
        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);

        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objPHPExcel = $objReader->load($filename);
        $objPHPExcel->setActiveSheetIndex(0);
        //tổng số dòng hiện có
        $maxRow = $objPHPExcel->getActiveSheet()->getHighestRow();
        //echo $objPHPExcel->getActiveSheet()->getCell('B1')->getValue();
        //gọi model Product -> sử dụng config ->export 
        //Insert vào bảng product
//        print_r($objPHPExcel->getActiveSheet()->getCell('C5')->getValue());
//        exit();

        for ($i = 5; $i <= $maxRow; $i++) {
            $price_input = $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getValue();
            $price = $objPHPExcel->getActiveSheet()->getCell('J' . $i)->getValue();
            if (!empty($price_input) && !empty($price)) {
                $id_brand = explode('-', (string) $objPHPExcel->getActiveSheet()->getCell('H' . $i)->getValue());
                $id_manufacturer = explode('-', (string) $objPHPExcel->getActiveSheet()->getCell('G' . $i)->getValue());
                $brand_id = explode('-', (string) $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getValue());

                $d_product = new stdClass();
                $d_product->product_sku = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getValue();
                $d_product->product_price_input = (int) $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getValue();
                $d_product->product_price = (int) $objPHPExcel->getActiveSheet()->getCell('J' . $i)->getValue();
                $d_product->brand_id = $brand_id[0];
                $d_product->argency_id = $id_brand[0];
                $d_product->manufacturer_id = $id_manufacturer[0];
                $d_product->created_by = $user->id;
                $d_product->product_price_r = $objPHPExcel->getActiveSheet()->getCell('K' . $i)->getValue();
                $d_product->product_height = $objPHPExcel->getActiveSheet()->getCell('O' . $i)->getValue();
                $d_product->product_length = $objPHPExcel->getActiveSheet()->getCell('P' . $i)->getValue();
                $d_product->buyer_virtual = $objPHPExcel->getActiveSheet()->getCell('L' . $i)->getValue();
                $d_product->published = $objPHPExcel->getActiveSheet()->getCell('Q' . $i)->getValue();

                $product_hot = $objPHPExcel->getActiveSheet()->getCell('R' . $i)->getValue();
                if (isset($product_hot)) {
                    $d_product->product_hot = $product_hot;
                }

                $product_new = $objPHPExcel->getActiveSheet()->getCell('S' . $i)->getValue();
                if (isset($product_new)) {
                    $d_product->product_new = $product_new;
                }

                $d_product->product_image = $objPHPExcel->getActiveSheet()->getCell('T' . $i)->getValue();
                //insert vào bảng product
                $db->insertObject('#__openshop_products', $d_product);

                $id_pro = $db->insertid();  //id product mới thêm
                //insert vào bảng product details
                $d_prodetail = new stdClass();
                $d_prodetail->product_id = $id_pro;
                $d_prodetail->product_name = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getValue();
                $d_prodetail->product_alias = JFilterOutput::stringURLSafe($d_prodetail->product_name);
                $d_prodetail->language = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');

                $db->insertObject('#__openshop_productdetails', $d_prodetail);


                //thêm category
                $category = explode('-', $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getValue());
//                $nameCat = explode('/', $category[1]);
//
//                $query->clear();
//                $query->select('id')
//                        ->from($db->quoteName('#__openshop_categorydetails'))
//                        ->where('category_name IN ("' . join('","', $nameCat) . '")');
//                $arr_cat_ids = $db->setQuery($query)->loadObjectList();
//
//                foreach ($arr_cat_ids as $arr_cart_id) {
//                    $d_productCat = new stdClass();
//                    $d_productCat->product_id = $id_pro;
//                    $d_productCat->category_id = $arr_cart_id->id;
//
//                    //thêm vào loại danh mục
//                    $db->insertObject('#__openshop_productcategories', $d_productCat);
//                }

                $d_productCat = new stdClass();
                $d_productCat->product_id = $id_pro;
                $d_productCat->category_id = $category[0];

                //thêm vào loại danh mục
                $db->insertObject('#__openshop_productcategories', $d_productCat);

                //thêm option
                //*************** Màu
                $color = explode('-', $objPHPExcel->getActiveSheet()->getCell('N' . $i)->getValue());
                $query->clear();
                $query->select('id')->from($db->quoteName('#__openshop_optiondetails'))->where('option_name = "COLOR"');
                $id_option_color = $db->setQuery($query)->loadResult();

                //thêm vào bảng product_option
                $query->clear();
                $d_coloroption = new stdClass();
                $d_coloroption->product_id = $id_pro;
                $d_coloroption->option_id = $id_option_color;
                $d_coloroption->required = '1';

                $db->insertObject('#__openshop_productoptions', $d_coloroption);
                $id_insertColor = $db->insertid();      //id mới thêm vào bảng productOption

                $query->clear();
                $d_productcolor = new stdClass();
                $d_productcolor->product_option_id = $id_insertColor;
                $d_productcolor->product_id = $id_pro;
                $d_productcolor->option_id = $id_option_color;
                $d_productcolor->option_value_id = $color[0];
                $db->insertObject('#__openshop_productoptionvalues', $d_productcolor);

                //THÊM KÍCH THƯỚC - SIZE    
                $sizes = $objPHPExcel->getActiveSheet()->getCell('M' . $i)->getValue();
                $query->clear();
                $query->select('id')->from($db->quoteName('#__openshop_optiondetails'))->where('option_name = "SIZE"');
                $id_option_size = $db->setQuery($query)->loadResult();

                $query->clear();
                $query->select('a.optionvalue_id,a.value')
                        ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'b') . ' ON a.option_id = b.option_id')
                        ->where('b.option_name = "SIZE"')
                        ->where('a.value IN ("' . str_replace(',', '","', $sizes) . '")');
                $size_id = $db->setQuery($query)->loadObjectList();
                if (count($size_id)) {
                    //thêm vào bảng product_option
                    $query->clear();
                    $d_coloroption = new stdClass();
                    $d_coloroption->product_id = $id_pro;
                    $d_coloroption->option_id = $id_option_size;
                    $d_coloroption->required = '1';

                    $db->insertObject('#__openshop_productoptions', $d_coloroption);
                    $id_insertSize = $db->insertid();      //id mới thêm vào bảng productOption

                    foreach ($size_id as $sID) {
                        $query->clear();
                        $d_productcolor = new stdClass();
                        $d_productcolor->product_option_id = $id_insertSize;
                        $d_productcolor->product_id = $id_pro;
                        $d_productcolor->option_id = $id_option_size;
                        $d_productcolor->option_value_id = $sID->optionvalue_id;
                        $db->insertObject('#__openshop_productoptionvalues', $d_productcolor);
                    }
                }
            }
        }

        //xoá file
        unlink($filename);
    }

    function checkFileImportProduct($filename) {
        
    }

    function uploadFile() {
        $res = array();
        $fileName = $_FILES['fileUp'];
        $path = OPENSHOP_PATH_TEMP;
        if (isset($fileName['name'])) {
            $FName = rand(1000, 10000) . '-' . JFile::makeSafe($fileName['name']);
            $up = JFile::upload($fileName['tmp_name'], $path . $FName);
            if ($up) {
                $filename = OPENSHOP_PATH_TEMP . $FName;

//                self::checkFileImportProduct($filename);

                $res['status'] = 'success';
                $res['message'] = 'Upload thành công';

                $res['filename'] = $filename;


                self::exportProductValue($filename);
            } else {
                $res['status'] = 'error';
                $res['message'] = 'Lỗi file';
            }
        } else {
            $res['status'] = 'error';
            $res['message'] = 'Lỗi file';
        }

        echo json_encode($res);
    }

    function checkProductSKU() {
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $res = array();
        $sku = $app->getString('v');
        try {
            $query->select('count(id)')
                    ->from($db->quoteName('#__openshop_products'))
                    ->where('product_sku = "' . $sku . '"');
            $row = $db->setQuery($query)->loadResult();
            if ($row) {
                $res['status'] = 'warning';
                $res['message'] = 'Đã tồn tại sản phẩm theo mã: <b>' . $sku . '</b>';
            }

            echo json_encode($res);
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    /*
     * 
     */

    function getProductDiff() {
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $sku = $app->getString('v');
        $id = intval($app->getInt('i'));
        $res = array();
        $html = '';
        try {
            $query->select('a.*,b.product_name')
                    ->from($db->quoteName('#__openshop_products', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                    ->where('a.product_sku LIKE "%' . $sku . '%"');
            if($id){
                $query->where('a.id != ' . $id);
            }
                    
            $rows = $db->setQuery($query)->loadObjectList();
            if (count($rows)) {
                foreach ($rows as $k => $row) {
                    $html .= '<li onclick="getSKUProDiff(' . $k . ')">';
                    $html .= '  <div class="display_inline_block">';
                    $html .= '      <img src="' . OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image . '" width="50px"/>';
                    $html .= '  </div>';
                    $html .= '  <div class="display_inline_block" style="margin-left: 10px;position: absolute;top: 30px;";>';
                    $html .= '      ' . $row->product_name;
                    $html .= '  </div>';
                    $html .= '  <input type="hidden" class="productSKUDiff_' . $k . '" value="' . $row->product_sku . '" />';
                    $html .= '  <input type="hidden" class="productIDDiff_' . $k . '" value="' . $row->id . '" />';
                    $html .= '</li>';
                }
            } else {
                $html .= '<li>';
                $html .= '  Không có sản phẩm';
                $html .= '</li>';
            }
            $res['status'] = 'success';
            $res['message'] = 'Đã tải dữ liệu';
            $res['html'] = $html;

            echo json_encode($res);
        } catch (Exception $ex) {
            $res['status'] = 'errror';
            $res['message'] = 'Không kết nối được với máy chủ';
            echo json_encode($res);
        }
    }

    /*
     * export excel Product
     */

    function exportPro() {
        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';
        $app = JFactory::getApplication()->input;
        $man_id = intval($app->getInt('slt_manu'));
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $statusExcel = '';
        $app = JFactory::getApplication()->input;
        $fileNum = intval($app->getInt('file'));       //file cần tải. mỗi file chứa 100 dòng

        $phpExcel = new PHPExcel();

        //Category Líst
        $objWorkSheet = $phpExcel->createSheet(0); //Setting index when creating
        $objWorkSheet->setTitle('CapNhatKho');

        $i = 2;
        $objWorkSheet->setCellValue('A' . $i, 'STT');
        $objWorkSheet->setCellValue('B' . $i, 'Mã SP');
        $objWorkSheet->setCellValue('C' . $i, 'Tên SP');
        $objWorkSheet->setCellValue('D' . $i, 'Hình ảnh');
        $objWorkSheet->setCellValue('E' . $i, 'Tình trạng');
        $objWorkSheet->setCellValue('F' . $i, 'Mã NCC');
        $objWorkSheet->setCellValue('G' . $i, 'Size');
        $objWorkSheet->setCellValue('H' . $i, 'Kho NCC');

        $objWorkSheet->getColumnDimension('B')->setWidth(12);
        $objWorkSheet->getColumnDimension('C')->setWidth(55);
        $objWorkSheet->getColumnDimension('D')->setWidth(15);
        $objWorkSheet->getColumnDimension('E')->setWidth(15);
        $objWorkSheet->getColumnDimension('F')->setWidth(15);
        $objWorkSheet->getColumnDimension('G')->setWidth(15);
        $objWorkSheet->getColumnDimension('H')->setWidth(15);

        $objWorkSheet->getStyle('A' . $i . ':H' . $i)->getFont()->setBold(true);
        $objWorkSheet->getStyle('A' . $i . ':H' . $i)->getFont()->setSize(14);
        $objWorkSheet->getStyle('A' . $i . ':H' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('35FFE4');

        /*
         * Data
         */
        $query->select('a.*,b.product_name,c.manufacturer_name,e.value')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                ->join('INNER', $db->quoteName('#__openshop_manufacturers', 'c') . ' ON c.id = a.manufacturer_id')
                ->join('INNER', $db->quoteName('#__openshop_productoptionvalues', 'd') . 'ON d.product_id = a.id')
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'e') . ' ON d.option_value_id = e.optionvalue_id')
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'f') . ' ON f.option_id = e.option_id')
                ->where('c.id = ' . intval($app->getInt('slt_manu')))
                ->where('a.delete_status = 0')
                ->where('a.published = 1')
                ->where('UPPER(f.option_name) = "SIZE"');

        if (intval($app->getInt('slt_typeP')) == 1) {       //các sản phẩm ON
            $query->where('a.published = 1');
            $statusExcel = 'ON';
        } else {
            $query->where('a.published = 0');
            $statusExcel = 'OFF';
        }
        $rows = $db->setQuery($query)->loadObjectList();

        $i++;
        $stt = 1;
        for ($j = 0; $j < count($rows); $j++) {
            $row = $rows[$j];
            if ($j > (100 * ($fileNum - 1))) {
                if ($stt > 100) {
                    break;
                }

                $objWorkSheet->setCellValue('A' . $i, $stt);
                $objWorkSheet->setCellValue('B' . $i, $row->product_sku);
                $objWorkSheet->setCellValue('C' . $i, $row->product_name);
                $objWorkSheet->setCellValue('E' . $i, $statusExcel);
                $objWorkSheet->setCellValue('F' . $i, $row->manufacturer_sku);
                $objWorkSheet->setCellValue('H' . $i, '');
                $objWorkSheet->setCellValue('G' . $i, $row->value);

                //img
                $path = OPENSHOP_PATH_IMG_PRODUCT . $row->product_image;
                if (JFile::exists($path)) {
                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $signature = $path;
                    $objDrawing->setPath($signature);
                    $objDrawing->setCoordinates('D' . $i);
                    $objDrawing->setWidth(100);
                    $objDrawing->setHeight(100);
                    $objDrawing->setOffsetX(15);
                    $objDrawing->setOffsetY(7);
                    $objDrawing->setWorksheet($objWorkSheet);  //save
                }

                $objWorkSheet->getRowDimension((int) $i)->setRowHeight(85);
                $i++;
                $stt++;
            }
        }

        $objWorkSheet->getStyle('A1:H' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objWorkSheet->getStyle('A1:H' . $i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objWorkSheet->getStyle('A2:H' . $i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


        $write = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        header('Content-Disposition: attachment; filename="CKN-' .  OpenShopHelper::getManufactureName($man_id)."-".date("d.m.Y")."-L".$fileNum . '.xls"');

        // Write file to the browser
        $write->save('php://output');
    }

    function getSize($idOption) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('optionvalue_id = ' . $idOption);
        return $db->setQuery($query)->loadResult();
    }

    /*
     * Export Excel IO
     */

    function exportExcelIO() {
        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $app = JFactory::getApplication()->input;

        $phpExcel = new PHPExcel();

        //Category Líst
        $objWorkSheet = $phpExcel->createSheet(0); //Setting index when creating
        $objWorkSheet->setTitle('Upload Lazada');
        $i = 2;
        $objWorkSheet->setCellValue('A' . $i, 'PrimaryCategory');
        $objWorkSheet->setCellValue('B' . $i, 'name');
        $objWorkSheet->setCellValue('C' . $i, 'short_description');
        $objWorkSheet->setCellValue('D' . $i, 'description');
        $objWorkSheet->setCellValue('F' . $i, 'brand');
        $objWorkSheet->setCellValue('G' . $i, 'model');
        $objWorkSheet->setCellValue('H' . $i, 'color_family');
        $objWorkSheet->setCellValue('K' . $i, 'clothing_material');
        $objWorkSheet->setCellValue('M' . $i, 'fa_pattern');
        $objWorkSheet->setCellValue('P' . $i, 'collar_type');
        $objWorkSheet->setCellValue('Q' . $i, 'pants_length');
        $objWorkSheet->setCellValue('R' . $i, 'pants_fit_type');
        $objWorkSheet->setCellValue('T' . $i, 'sleeves');
        $objWorkSheet->setCellValue('X' . $i, 'occasion');
        $objWorkSheet->setCellValue('AB' . $i, 'dress_shape');
        $objWorkSheet->setCellValue('AC' . $i, 'dress_length');
        $objWorkSheet->setCellValue('AH' . $i, 'material_filter');
        $objWorkSheet->setCellValue('CG' . $i, 'm_underwear_style');
        $objWorkSheet->setCellValue('CH' . $i, 'length');
        $objWorkSheet->setCellValue('CM' . $i, 'skirt_style');
        $objWorkSheet->setCellValue('CP' . $i, 'warranty_type');
        $objWorkSheet->setCellValue('CP' . $i, 'warranty_type');
        $objWorkSheet->setCellValue('CW' . $i, 'SellerSku');
        $objWorkSheet->setCellValue('CX' . $i, 'AssociatedSku');
        $objWorkSheet->setCellValue('CZ' . $i, 'quantity');
        $objWorkSheet->setCellValue('DA' . $i, 'price');
        $objWorkSheet->setCellValue('DE' . $i, 'package_content');
        $objWorkSheet->setCellValue('DF' . $i, 'package_weight');
        $objWorkSheet->setCellValue('DG' . $i, 'package_length');
        $objWorkSheet->setCellValue('DH' . $i, 'package_width');
        $objWorkSheet->setCellValue('DI' . $i, 'package_height');
        $objWorkSheet->setCellValue('DU' . $i, 'production_country');
        $objWorkSheet->setCellValue('DW' . $i, 'size');

        $query = $db->getQuery(TRUE);
        $typeExport = $app->getInt('typeExport');
        $query->select('*')
                ->from($db->quoteName('#__openshop_ios', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . ' ON a.id_product = b.id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . ' ON c.product_id = b.id')
                ->where('a.id_warehouse = ' . intval($app->getInt('export_warehouse')))
                ->where('a.id_warehouse_to = ' . intval($app->getInt('import_warehouse')))
                ->where('a.xacnhan = 0');
        $rows = $db->setQuery($query)->loadObjectList();

//        print_r($rows);exit();


        /*
         * get color in product
         */
        $idPro = array();
        foreach ($rows as $row) {
            $idPro[] = $row->id_product;
        }

        /*
         * get COLOR in ID
         */
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON b.optionvalue_id = a.option_value_id')
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . 'ON c.option_id = a.option_id')
                ->where('UPPER(c.option_name) = "COLOR"')
                ->where('a.product_id IN (' . implode(',', $idPro) . ')');
        $arrColor = $db->setQuery($query)->loadObjectList();

        $color = array();
        foreach ($arrColor as $c) {
            $color[$c->product_id] = $c->value;
        }


        $i++;
        foreach ($rows as $key => $row) {
            $size = self::getSize($row->id_optionsize);

            $objWorkSheet->setCellValue('B' . $i, $row->product_name);
            $objWorkSheet->setCellValue('C' . $i, $row->product_short_desc);
            $objWorkSheet->setCellValue('D' . $i, $row->product_short_desc);
            $objWorkSheet->setCellValue('F' . $i, 'Siêu Thị Phong Phú');
            $objWorkSheet->setCellValue('G' . $i, '-');
            $objWorkSheet->setCellValue('H' . $i, $color[$row->id_product]);
            $objWorkSheet->setCellValue('K' . $i, 'Chất liệu khác');
            $objWorkSheet->setCellValue('M' . $i, 'Trơn');
            $objWorkSheet->setCellValue('P' . $i, 'Not specific');
            $objWorkSheet->setCellValue('Q' . $i, 'Cardigan Lửng');
            $objWorkSheet->setCellValue('R' . $i, 'Bình thường');
            $objWorkSheet->setCellValue('T' . $i, 'Tay dài');
            $objWorkSheet->setCellValue('X' . $i, 'Trang trọng');
            $objWorkSheet->setCellValue('AB' . $i, 'Đầm xoè vintage');
            $objWorkSheet->setCellValue('AC' . $i, 'Dài qua gối');
            $objWorkSheet->setCellValue('AH' . $i, 'Non-Leather');
            $objWorkSheet->setCellValue('CG' . $i, 'Not specific');
            $objWorkSheet->setCellValue('CH' . $i, 'Long');
            $objWorkSheet->setCellValue('CM' . $i, 'Dáng xòe');
            $objWorkSheet->setCellValue('CP' . $i, 'No Warranty');
            $objWorkSheet->setCellValue('CW' . $i, $row->product_sku . '-' . $size);
            $objWorkSheet->setCellValue('CX' . $i, $row->product_sku . '-' . $size);
            $objWorkSheet->setCellValue('CZ' . $i, $row->product_quantity);
            $objWorkSheet->setCellValue('DA' . $i, $row->product_price);
            $objWorkSheet->setCellValue('DE' . $i, '1 sản phẩm');
            $objWorkSheet->setCellValue('DF' . $i, number_format($row->product_weight, 1, '.', ','));
            $objWorkSheet->setCellValue('DG' . $i, number_format($row->product_length, 1, '.', ','));
            $objWorkSheet->setCellValue('DH' . $i, number_format($row->product_width, 1, '.', ','));
            $objWorkSheet->setCellValue('DI' . $i, number_format($row->product_height, 1, '.', ','));
            $objWorkSheet->setCellValue('DU' . $i, 'Việt Nam');
            $objWorkSheet->setCellValue('DW' . $i, 'Int:' . $size);
            $i++;
        }

        $write = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        header('Content-Disposition: attachment; filename="Mau_Import_San_Pham.xls"');

        // Write file to the browser
        $write->save('php://output');
    }

    /*
     * get File Download
     */

    public function getFileDownLoad() {
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $chooseFile = intval($app->getInt('chooseFile'));
        $manu = intval($app->getInt('manu'));
        $manu_name = OpenShopHelper::getManufactureName($manu);
        $status = intval($app->getInt('status'));

        /*
         * Data
         */
        $query->select('a.*,b.product_name,c.manufacturer_name')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                ->join('INNER', $db->quoteName('#__openshop_manufacturers', 'c') . ' ON c.id = a.manufacturer_id')
                ->where('a.delete_status = 0')
                ->where('a.published = 1')
                ->where('c.id = ' . $manu);

        if ($status == 1) {       //các sản phẩm ON
            $query->where('a.published = 1');
            $statusExcel = 'ON';
        } else {
            $query->where('a.published = 0');
            $statusExcel = 'OFF';
        }
        $rows = $db->setQuery($query)->loadObjectList();
      
        $idPro = array();
        foreach ($rows as $row) {
            $idPro[] = $row->id;
        }

        /*
         * get Size in all pro
         */
        $query->clear();
        $query->select('a.id,d.product_name')
                ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON a.option_value_id = b.optionvalue_id')
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . 'ON c.option_id = b.option_id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'd') . 'ON d.product_id = a.product_id')
                ->where('UPPER(c.option_name) = "SIZE"')
                ->where('a.product_id IN (' . implode(',', $idPro) . ')');
        $arrSize = $db->setQuery($query)->loadObjectList();

        $num = count($arrSize);
        $numTemp = (int) ($num / 100);
        if ($num % 100 > 0) {
            $numTemp++;
        }

        $html = '';
        $html .= '<div>';
     
        for ($i = 1; $i <= $numTemp; $i++) {
            $html .= '<form action="index.php?option=com_openshop&format=ajax&task=product.exportPro&file=' . $i . '" method="post" enctype="multipart/form-data" style="float: left;">';
            $html .= '<input type="hidden" name="slt_chooseFile" value="' . $chooseFile . '">';
            $html .= '<input type="hidden" name="slt_manu" value="' . $manu . '">';
            $html .= '<input type="hidden" name="slt_typeP" value="' . $status . '">';
            $html .= '  <button class="btn btn-primary" style="margin: 0 5px;"> CNK-'.$manu_name .'-File-'. $i . ' </button>';
            $html .= '</form>';
        }

        $html .= '</div>';

        $res = array();
        $res['data'] = $html;

        echo json_encode($res);
    }

}
