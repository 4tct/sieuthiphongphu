

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopControllerIos extends OpenShopController {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    public function check_idproduct() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');

        if (trim($id_product) != NULL) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('product_sku, product_image')
                    ->from('#__openshop_products')
                    ->where('product_sku LIKE "' . $id_product . '%"');
            $db->setQuery($query);

            $objects = $db->loadObjectList();

            echo json_encode($objects);
        }
    }

    public function select_input() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        //warehouse
        $query->select('id, warehouse_name')
                ->from($db->quoteName('#__openshop_warehouses'))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        echo json_encode($rows);
    }

    public function load_product() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $db = JFactory::getDbo();

        $query = $db->getQuery(TRUE);
        $query->select('d.id, b.id AS MANCC, c.product_name,d.product_sku AS MASP, d.product_image AS HINH,
        b.manufacturer_name AS TEN_NCC,d.manufacproduct_sku AS MASP_NCC,product_price_input as price_input')
                ->from($db->quoteName('#__openshop_productdetails', 'c'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'd') . 'ON d.id=c.product_id')
                ->join('LEFT', $db->quoteName('#__openshop_manufacturers', 'b') . 'ON d.manufacturer_id = b.id')
                ->where('d.product_sku = "' . $id_product . '"')
                ->group('d.product_sku');
        $db->setQuery($query);
        $objects = $db->loadObjectList();

        echo json_encode($objects);
    }

    public function load_color() {
        //and a.id=\''.$id_product.'\' 
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id_product');
        $db = JFactory::getDbo();
        $db->setQuery('select a.id as idaotu_pro,c.value AS attr,c.optionvalue_id
        from #__openshop_products as a, #__openshop_productoptions as b,
        #__openshop_optionvaluedetails as c
        where a.id= b.product_id
        and b.option_id=1
        and c.option_id = b.option_id
        GROUP BY c.value');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function load_size() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id_product2');
        $db = JFactory::getDbo();
        $db->setQuery('select a.id as idaotu_pro,c.value AS attr,c.optionvalue_id
        from #__openshop_products as a, #__openshop_productoptions as b,
        #__openshop_optionvaluedetails as c
        where a.id= b.product_id
        and b.option_id=2
        and c.option_id = b.option_id
        GROUP BY c.value
        ');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function add_row_TH_TNCC() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $id_warehouse = $app->getString('id_warehouse');
        $db = JFactory::getDbo();
        $db->setQuery('SELECT a.total_import_product as soluong,idsize,idcolor,
            (SELECT e.warehouse_name FROM #__openshop_warehouses as e where e.id=a.warehouse_id) as warehouse_name,
            (select value from #__openshop_optionvaluedetails as c where c.optionvalue_id=a.idcolor ) as color,
            (select value from #__openshop_optionvaluedetails as d where d.optionvalue_id=a.idsize ) as size
            FROM #__openshop_inventories as a
            WHERE a.warehouse_id=' . $id_warehouse . '
            and a.product_id="' . $id_product . '"');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function add_row() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $db->setQuery('SELECT a.id_product,id_optioncolor, id_optionsize,
            (select value from #__openshop_optionvaluedetails as c where c.optionvalue_id=a.id_optioncolor ) as color,
            (select value from #__openshop_optionvaluedetails as b where b.optionvalue_id=a.id_optionsize ) as size
            FROM #__openshop_ios as a
            WHERE a.id_product=' . $id_product . '
            GROUP BY a.id_optioncolor, a.id_optionsize');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function load_price_input_ios() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $db = JFactory::getDbo();
        $db->setQuery('SELECT a.price_input AS GIANHAP, a.date_input AS NGAY,
        (select value from #__openshop_optionvaluedetails as b where a.id_optioncolor=b.optionvalue_id) as color,
        (select value from #__openshop_optionvaluedetails as b where a.id_optionsize=b.optionvalue_id) as size
        from #__openshop_ios as a
        where a.id_product="' . $id_product . '"
        ORDER BY a.date_input DESC LIMIT 0,3
        ');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function load_price_input_product() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $db = JFactory::getDbo();
        $db->setQuery('SELECT a.product_price as GIANHAP,a.modified_date AS NGAY
        from #_openshop_products as a
        where a.id="' . $id_product . '"
        ORDER BY a.modified_date DESC LIMIT 0,1
        ');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function getFormAjax() {
        $data = JRequest::get('data');
        $product_img = JRequest::get('product_img');
//        print_r($product_img);
        $arr_form = array();
        foreach ($data['data'] as $k => $v) {
            if ($v['name'] == 'cid[]') {
                $arr_form['cid'] = array($v['value']);
            } else {
                $arr_form[$v['name']] = $v['value'];
            }
        }

//        print_r($arr_form);
        $this->save($arr_form);
    }

    public function add_row_new() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $db = JFactory::getDbo();
        $db->setQuery('SELECT a.product_id,a.option_id, a.option_value_id,b.value
        FROM #__openshop_productoptionvalues as a, #__openshop_optionvaluedetails as b
        WHERE a.option_value_id=b.optionvalue_id GROUP BY a.option_value_id');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function add_row_output() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $id_warehouse_from = $app->getString('id_warehouse_from');
        $db = JFactory::getDbo();
        $db->setQuery('select a.id_product, a.id_optioncolor, a.id_optionsize,
        (select value from #__openshop_optionvaluedetails as c where c.optionvalue_id=a.id_optioncolor ) as color,
        (select value from #__openshop_optionvaluedetails as d where d.optionvalue_id=a.id_optionsize ) as size,
        (SELECT sum(b.quantity) from #__openshop_ios as b where b.id_optioncolor = a.id_optioncolor and b.id_optionsize = a.id_optionsize
        and b.id_product="' . $id_product . '" and b.id_warehouse="' . $id_warehouse_from . '" ) as soluong,
        a.id_warehouse
        from #__openshop_ios as a
        WHERE a.id_product="' . $id_product . '"
        and a.id_warehouse="' . $id_warehouse_from . '"
        GROUP BY a.id_optioncolor, a.id_optionsize');
        $objects = $db->loadObjectList();
        echo json_encode($objects);
    }

    public function load_product_output() {
        $app = JFactory::getApplication()->input;
        $id_product = $app->getString('id');
        $id_warehouse_from = $app->getString('id_warehouse_from');
        $id_status = $app->getString('statusios');
        $db = JFactory::getDbo();


        $db->setQuery('SELECT a.id,a.date_input as NGAY,a.id_warehouse_to,a.id_optioncolor, a.id_optionsize,a.quantity,a.date_input,
(select value from #__openshop_optionvaluedetails as c where c.optionvalue_id=a.id_optioncolor ) as color,
(select value from #__openshop_optionvaluedetails as d where d.optionvalue_id=a.id_optionsize ) as size,
(SELECT e. warehouse_name FROM #__openshop_warehouses as e WHERE a.id_warehouse = e.id ) as warehouse_name, a.xacnhan '
                . 'from #__openshop_ios as a WHERE (a.id_warehouse="' . $id_warehouse_from . '" or a.id_warehouse_to="' . $id_warehouse_from . '")
and a.id_product="' . $id_product . '"');
        $lists = $db->loadObjectList();

        $query = $db->getQuery(true);
        $db->setQuery('SELECT d.id_optioncolor, d.id_optionsize,
(select value from #__openshop_optionvaluedetails as c where c.optionvalue_id=d.id_optioncolor ) as color,
(select value from #__openshop_optionvaluedetails as b where b.optionvalue_id=d.id_optionsize ) as size
from #__openshop_ios as d
WHERE d.id_product="' . $id_product . '"
GROUP BY d.id_optioncolor, d.id_optionsize');

        $list_attr = $db->loadObjectList();
        $soluong = 0;
        $array_add = array();

        $stt = 0;

        foreach ($list_attr as $attr) {// luu 
            foreach ($lists as $val) {// lap 
                //echo $val->id;
                if (($val->id_warehouse_to == $id_warehouse_from) && ($val->id_optioncolor == $attr->id_optioncolor) && ($val->id_optionsize == $attr->id_optionsize) && ($val->xacnhan == 1)) {
                    $soluong+=$val->quantity;
                } else if (($val->id_warehouse_to != $id_warehouse_from) && ($val->id_optioncolor == $attr->id_optioncolor) && ($val->id_optionsize == $attr->id_optionsize)) {
                    $soluong-=$val->quantity;
                }
                $warehouse = $val->warehouse_name;
                $color = $attr->color;
                $size = $attr->size;
                $colorid = $attr->id_optioncolor;
                $sizeid = $attr->id_optionsize;
                $ngay = $val->NGAY;
                $id = $val->id;
            }
            if ($soluong != 0 && $soluong > 0) {
                $array_add[$stt++] = array(
                    "warehouse_name" => $warehouse,
                    "color" => $color,
                    "size" => $size,
                    "colorid" => $colorid,
                    "sizeid" => $sizeid,
                    "NGAY" => $ngay,
                    "id" => $id,
                    "soluong" => $soluong
                );
                $soluong = 0;
            }
        }
        echo json_encode($array_add);
    }

//    public function uploadfile() {
//        $imagePath = OPENSHOP_PATH_IMG_PRODUCT;
//        $productImage = $_FILES['userImage'];
//        if (is_uploaded_file($productImage['tmp_name'])) {
//            if (JFile::exists($imagePath . $productImage['name'])) {
//                $imageFileName = uniqid('image_') . '_' . JFile::makeSafe($productImage['name']);
//            } else {
//                $imageFileName = JFile::makeSafe($productImage['name']);
//            }
//            JFile::upload($productImage['tmp_name'], $imagePath . $imageFileName);
//        }
//    }

    public function saveimages() {
        //$nameimage = $_FILES["myfile"]["name"];
        $idsp = $_POST["masanpham"];
        $taptin = $_FILES["myfile"];
        if ($taptin["type"] == "image/jpg" || $taptin["type"] == "image/png" || $taptin["type"] == "image/gif" ||
                $taptin["type"] == "image/jpeg") {
            //$ngay = date("d_m_Y_G_i_s");
            //$tentaptin = $ngay . "_" . $taptin["name"];
            $imagePath = OPENSHOP_PATH_IMG_PRODUCT;
            if (JFile::exists($imagePath . $_FILES["myfile"]["name"])) {
                $imageFileName = uniqid('image_') . '_' . JFile::makeSafe($_FILES["myfile"]["name"]);
            } else {
                $imageFileName = JFile::makeSafe($_FILES["myfile"]["name"]);
            }
            JFile::upload($taptin['tmp_name'], $imagePath . $idsp . "_" . $imageFileName);

            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            // Fields to update.
            $fields = array(
                $db->quoteName('product_image') . ' = "' . $idsp . '_' . $imageFileName . '"'
            );

            // Conditions for which records should be updated.
            $conditions = array(
                $db->quoteName('id') . ' = ' . $idsp
            );

            $query->update($db->quoteName('#__openshop_products'))->set($fields)->where($conditions);

            $db->setQuery($query);

            $result = $db->execute();
        }
    }

    public function getColorSizeProduct() {
        $app = JFactory::getApplication()->input;
        $id_product_sku = $app->getString('id'); //product_sku
        $status_io = $app->getInt('status_io');
        $lists = array();

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        if ($status_io == '2') {      //xuất kho
            $id_warehouse = $app->getInt('id_warehouse');
            $query->select('b.optionvalue_id as id, b.value')
                    ->from($db->quoteName('#__openshop_inventories', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON a.idsize = b.optionvalue_id')
                    ->join('INNER', $db->quoteName('#__openshop_products', 'c') . 'ON a.product_id = c.id')
                    ->where('c.product_sku = "' . $id_product_sku . '"')
                    ->where('a.warehouse_id = ' . $id_warehouse)
                    ->group('idsize');
            $lists['size'] = $db->setQuery($query)->loadObjectList();

            $query->clear();
            $query->select('b.optionvalue_id as id, b.value')
                    ->from($db->quoteName('#__openshop_inventories', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON a.idcolor = b.optionvalue_id')
                    ->join('INNER', $db->quoteName('#__openshop_products', 'c') . 'ON a.product_id = c.id')
                    ->where('c.product_sku = "' . $id_product_sku . '"')
                    ->where('a.warehouse_id = ' . $id_warehouse)
                    ->group('idcolor');
            $lists['color'] = $db->setQuery($query)->loadObjectList();
        } else {
            $query->select('a.option_value_id as id, '
                            . '(select c.value from #__openshop_optionvaluedetails as c where a.option_value_id = c.optionvalue_id) as value')
                    ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                    ->join('LEFT', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                    ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'd') . 'ON d.option_id = a.option_id')
                    ->where('d.option_name = "COLOR"')
                    ->where('b.product_sku = "' . $id_product_sku . '"');

            $lists['color'] = $db->setQuery($query)->loadObjectList();

            if (empty($lists['color'])) {
                $query->clear();
                $query->select('a.option_id,a.value')
                        ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                        ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'b') . 'ON b.option_id = a.option_id')
                        ->where('b.option_name = "COLOR"');

                $lists['color'] = $db->setQuery($query)->loadObjectList();
            }

            $query->clear();
            $query->select('a.option_value_id as id, '
                            . '(select c.value from #__openshop_optionvaluedetails as c where a.option_value_id = c.optionvalue_id) as value')
                    ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                    ->join('LEFT', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                    ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'd') . 'ON d.option_id = a.option_id')
                    ->where('d.option_name = "SIZE"')
                    ->where('b.product_sku = "' . $id_product_sku . '"');

            $lists['size'] = $db->setQuery($query)->loadObjectList();

            if (empty($lists['size'])) {
                $query->clear();
                $query->select('a.option_id,a.value')
                        ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                        ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'b') . 'ON b.option_id = a.option_id')
                        ->where('b.option_name = "SIZE"');

                $lists['size'] = $db->setQuery($query)->loadObjectList();
            }
        }

        echo json_encode($lists);
    }

    function getQuantityInventory() {
        $app = JFactory::getApplication()->input;
        $size = $app->getInt('size');
        $color = $app->getInt('color');
        $pro = $app->getString('pro');
        $id_warehouse = $app->getInt('id_warehouse');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id')
                ->from($db->quoteName('#__openshop_products'))
                ->where('product_sku = "' . $pro . '"');
        $id_pro = $db->setQuery($query)->loadResult();
        $quantity = $db->setQuery('CALL TotalInventory("import",' . $id_pro . ',' . $size . ',' . $color . ',' . $id_warehouse . ')')->loadResult();
        echo empty($quantity) ? '0' : $quantity;
    }

    function recoverIO() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_RECOVER_FAIL'));
        $app = JFactory::getApplication()->input;
        $idIO = $app->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('*')
                    ->from($db->quoteName('#__openshop_ios'))
                    ->where('id = ' . $idIO);
            $row_IO = $db->setQuery($query)->loadObject();

            $data_recover = new stdClass();
            $data_recover->id_product = $row_IO->id_product;
            $data_recover->id_optionsize = $row_IO->id_optionsize;
            $data_recover->id_optioncolor = $row_IO->id_optioncolor;
            $data_recover->quantity = $row_IO->quantity;
            $data_recover->price_input = $row_IO->price_input;
            $data_recover->id_status = 4;   //recover
            $data_recover->date_input = $data_recover->created_date = date('Y-m-d H:m:s');
            $data_recover->created_by = JFactory::getUser()->id;
            $data_recover->id_option = 1;
            $data_recover->id_warehouse = $row_IO->id_warehouse;
            $data_recover->invoice_sku = 'THUHOI_' . date('Y-m-d H:m:s');
            $data_recover->id_warehouse_to = 0;
            $data_recover->xacnhan = 1;
            if ($db->insertObject('#__openshop_ios', $data_recover)) {
                $query->clear();
                $total_quantity_import = $db->setQuery('CALL TotalInventory("import",' . $row_IO->id_product . ',' . $row_IO->id_optionsize . ',' . $row_IO->id_optioncolor . ',' . $row_IO->id_warehouse . ')')->loadResult();
                $total = (int) $total_quantity_import + (int) $row_IO->quantity;
                if ($db->setQuery('CALL UpdateInInventory("import",' . $row_IO->id_product . ',' . $row_IO->id_optionsize . ',' . $row_IO->id_optioncolor . ',' . $row_IO->id_warehouse . ',' . $total . ')')->execute()) {
                    $query->clear();
                    $query->update($db->quoteName('#__openshop_ios'))
                            ->set(array('xacnhan = 1'))
                            ->where('id = ' . $idIO);
                    $db->setQuery($query)->execute();   //update IO
                    $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_RECOVER_SUCCESS'));
                }
            }
        } catch (Exception $ex) {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_RECOVER_FAIL'));
        }

        echo json_encode($res);
    }

}
