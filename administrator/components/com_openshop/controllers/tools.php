<?php
/**
 * @version		1.4.2
 * @package		Joomla
 * @subpackage	openshop
 * @author  	LMNX
 * @copyright	LMNX
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die();

/**
 * openshop controller
 *
 * @package		Joomla
 * @subpackage	openshop
 * @since 1.5
 */
class openshopControllerTools extends JControllerLegacy
{

	/**
	 * Constructor function
	 *
	 * @param array $config
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	/**
	 *
	 * Migrate customers from Joomla core users
	 */
	public function migrateFromJoomla()
	{
		$model = $this->getModel('tools');
		$model->migrateFromJoomla();
		$this->setRedirect('index.php?option=com_openshop&view=customers', JText::_('OPENSHOP_MIGRATE_FROM_JOOMLA_SUCESS'));
	}

	/**
	 * 
	 * Migrate customers from Membership Pro subscribers
	 */
	public function migrateFromMembershipPro()
	{
		if (file_exists(JPATH_ADMINISTRATOR.'/components/com_osmembership/osmembership.php'))
		{
			$model = $this->getModel('tools');
			$model->migrateFromMembershipPro();
			$this->setRedirect('index.php?option=com_openshop&view=customers', JText::_('OPENSHOP_MIGRATE_FROM_MEMBERSHIP_SUCCESS'));
		}
		else 
		{
			$this->setRedirect('index.php?option=com_openshop&view=dashboard', JText::_('OPENSHOP_MIGRATE_FROM_MEMBERSHIP_NOT_INSTALL'));	
		}
	}
	
	/**
	 * 
	 * Clean data
	 */
	public function cleanData()
	{
		$model = $this->getModel('tools');
		$model->cleanData();
		$this->setRedirect('index.php?option=com_openshop&view=dashboard', JText::_('OPENSHOP_CLEAN_DATA_SUCCESS'));
	}
	
	/**
	 *
	 * Add sample data
	 */
	public function addSampleData()
	{
		$model = $this->getModel('tools');
		$model->addSampleData();
		$this->setRedirect('index.php?option=com_openshop&view=dashboard', JText::_('OPENSHOP_ADD_SAMPLE_DATA_SUCCESS'));
	}
	
	/**
	 * 
	 * Function to synchronize data
	 */
	public function synchronizeData()
	{
		$model = $this->getModel('tools');
		$model->synchronizeData();
		$this->setRedirect('index.php?option=com_openshop&view=dashboard', JText::_('OPENSHOP_SYNCHRONIZE_DATA_SUCCESS'));
	}
	
	/**
	 * 
	 * Function to migrate virtuemart
	 */
	public function migrateVirtuemart()
	{
		$model = $this->getModel('tools');
		$model->migrateVirtuemart();
		$this->setRedirect('index.php?option=com_openshop&view=dashboard', JText::_('OPENSHOP_MIGRATE_VIRTUEMART_SUCCESS'));
	}
}