<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerField extends OpenShopController
{

	/**
	 * Constructor function
	 *
	 * @param array $config
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask('un_required', 'required');
	}
	
	public function required()
	{
		$cid = JRequest::getVar('cid', array());
		JArrayHelper::toInteger($cid);		
		$task = $this->getTask();
		if ($task == 'required')
		{
			$state = 1;
		}			
		else
		{
			$state = 0;
		}			
		$model = $this->getModel('Field');
		$model->required($cid , $state);
		$msg = JText::_('OPENSHOP_FIELD_REQUIRED_STATE_UPDATED');
		$this->setRedirect(JRoute::_('index.php?option=com_openshop&view=fields', false), $msg);
	}

	
}