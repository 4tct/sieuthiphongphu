<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerLanguage extends JControllerLegacy
{

	function save()
	{
		$model = $this->getModel('language');
		$data = JRequest::get('post', JREQUEST_ALLOWRAW);
		$model->save($data);
		$lang = $data['lang'];
		$item = $data['item'];
		$url = 'index.php?option=com_openshop&view=language&lang=' . $lang . '&item=' . $item;
		$msg = JText::_('OPENSHOP_TRANSLATION_SAVED');
		$this->setRedirect($url, $msg);
	}

	function cancel()
	{
		$this->setRedirect('index.php?option=com_openshop&view=dashboard');
	}
}