<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('_JEXEC') or die('Restricted access');

class OpenShopControllerIosconfirms extends OpenShopController {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    public function getFormAjax() {
        $data = JRequest::get('data');
        $arr_form = array();
        foreach ($data['data'] as $k => $v) {
            if ($v['name'] == 'cid[]') {
                $arr_form['cid'] = array($v['value']);
            } else {
                $arr_form[$v['name']] = $v['value'];
            }
        }
        $this->save($arr_form);
    }

    function confirmImportWarehouse() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_CONFIRM_FIELD'));
        $app = JFactory::getApplication()->input;
        $idIO = $app->getString('idIO');
        if (!empty($idIO)) {
            $arr_idIO = explode('_', $idIO);
            for ($i = 1; $i < count($arr_idIO); $i++) {
                if (is_int((int) $arr_idIO[$i])) {
                    if ($this->actConfirm((int) $arr_idIO[$i])) {
                        $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_CONFIRM_SUCCESS'));
                    }
                }
            }
        }

        echo json_encode($res);
    }

    /*
     * ACTION Confirm
     */

    function actConfirm($idIO) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_ios'))
                ->where('id = ' . $idIO);
        $row_IO = $db->setQuery($query)->loadObject();


        try {
            $query->clear();
            $query->update($db->quoteName('#__openshop_ios'))
                    ->set(array('xacnhan = 1'))
                    ->where('id = ' . $idIO);
            if ($db->setQuery($query)->execute()) {
                $query->clear();
                $query->select('id,total_import_product')
                        ->from($db->quoteName('#__openshop_inventories'))
                        ->where('product_id = ' . $row_IO->id_product)
                        ->where('idsize = ' . $row_IO->id_optionsize)
                        ->where('idcolor = ' . $row_IO->id_optioncolor)
                        ->where('warehouse_id = ' . $row_IO->id_warehouse_to);
                $check_inventory = $db->setQuery($query)->loadObject();
                if (count($check_inventory)) {      //product exists in warehouse 
                    $total = (int) $check_inventory->total_import_product + (int) $row_IO->quantity;
                    if ($db->setQuery('CALL UpdateInInventory("import",' . $row_IO->id_product . ',' . $row_IO->id_optionsize . ',' . $row_IO->id_optioncolor . ',' . $row_IO->id_warehouse_to . ',' . $total . ')')->execute()) {
                        return TRUE;
                    }
                    else
                    {
                        return FALSE;
                    }
                }
                else        //Don't exists
                {
                    $data_inventory = new stdClass();
                    $data_inventory->product_id = $row_IO->id_product;
                    $data_inventory->idsize = $row_IO->id_optionsize;
                    $data_inventory->idcolor = $row_IO->id_optioncolor;
                    $data_inventory->warehouse_id = $row_IO->id_warehouse_to;
                    $data_inventory->total_import_product = $row_IO->quantity;
                    if($db->insertObject('#__openshop_inventories', $data_inventory)){
                        return TRUE;
                    }
                    else
                    {
                        return FALSE;
                    }
                }
            } else {
                return FALSE;
            }
        } catch (Exception $ex) {
            return FALSE;
        }
    }

}
