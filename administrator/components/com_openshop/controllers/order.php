<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerOrder extends OpenShopController {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * 
     * Function to download attached file for order
     */
    public function downloadFile() {
        $id = JRequest::getInt('id');
        $model = $this->getModel('Order');
        $model->downloadFile($id);
    }

    /**
     * 
     * Function to download invoice for an order
     */
    public function downloadInvoice() {
        $fromExports = JRequest::getVar('from_exports');
        if ($fromExports) {
            $dateStart = JRequest::getVar('date_start', date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01')));
            $dateEnd = JRequest::getVar('date_end', date('Y-m-d'));
            $groupBy = JRequest::getVar('group_by', 'week');
            $orderStatusId = JRequest::getVar('order_status_id', 0);
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_orders');
            if ($orderStatusId)
                $query->where('order_status_id = ' . (int) $orderStatusId);
            if (!empty($dateStart))
                $query->where('created_date >= "' . $dateStart . '"');
            if (!empty($dateEnd))
                $query->where('created_date <= "' . $dateEnd . '"');
            $db->setQuery($query);
            $cid = $db->loadColumn();
            if (!count($cid)) {
                $mainframe = JFactory::getApplication();
                $mainframe->enqueueMessage(JText::_('OPENSHOP_NO_DATA_TO_DOWNLOAD_INVOICE'));
                $mainframe->redirect('index.php?option=com_openshop&view=reports&layout=orders&date_start=' . $dateStart . '&date_end=' . $dateEnd . '&group_by=' . $groupBy . '&order_status_id=' . $orderStatusId);
            }
        } else {
            $cid = JRequest::getVar('cid', array(0), '', 'array');
        }
        OpenShopHelper::downloadInvoice($cid);
    }

    public function check_user() {
        $app = JFactory::getApplication()->input;
        $info_search = $app->getString('i');

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('c.customer_id,c.fullname,c.email,c.telephone,add.zone_id,add.address AS address,add.district_id')
                ->from('#__openshop_customers AS c')
                ->join('LEFT', $db->quoteName('#__openshop_addresses', 'add') . 'ON add.customer_id = c.customer_id')
                ->where('c.telephone LIKE "%' . $info_search . '"');
        $db->setQuery($query);

        $rows = $db->loadObjectList();
        $h = '';
        if (!count($rows)) {
            $h = JText::_("OPENSHOP_SEARCH_NOT_FOUND");
        }
        $t = array('data_empty' => $h, 'data' => $rows);
        echo json_encode($t);
    }

    public function check_input() {
        $app = JFactory::getApplication()->input;
        $table = $app->getString('table');
        $col = $app->getString('col');
        $val = $app->getString('val');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')->from($db->quoteName('#__openshop_' . $table))->where($col . ' = "' . $val . '"');
        $db->setQuery($query);

        echo count($db->loadObjectList());
    }

    public function check_town() {
        $app = JFactory::getApplication()->input;
        $id_town = $app->getInt('id');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        if (isset($id_town)) {
            $where = "parent_id=" . $id_town;
        } else {
            $where = "true";
        }
        $query->select('*')
                ->from($db->quoteName('#__openshop_districts'))
                ->where('published = 1')
                ->where($where);

        $db->setQuery($query);

        $objects = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_CHOOSE'));
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->id, $object->district_name);
        }
        $lists['district'] = JHtml::_('select.genericlist', $options, 'payment_district_id', ' class="inputbox form-control" ', 'value', 'text', isset($object_name) ? $object_name : 0);

        echo json_encode($lists);
    }

    public function check_code() {
        $app = JFactory::getApplication()->input;
        $code = $app->getString('code');

        $date = getdate();
        $day = $date['mday'] < 10 ? '0' . $date['mday'] : $date['mday'];
        $mon = $date['mon'] < 10 ? '0' . $date['mon'] : $date['mon'];
        $data_cur = $date['year'] . '-' . $mon . '-' . $day;

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('id')
                ->from($db->quoteName('#__openshop_coupons'))
                ->where('coupon_code = "' . $code . '"')
                ->where('published = 1')
                ->where('status = 0');
//                ->where('coupon_start_date <= '. $data_cur)
//                ->where('coupon_end_date >= '. $data_cur);

        $db->setQuery($query);

        $row = $db->loadObjectList();

        echo json_encode($row);
    }

    public function shortcut_key_buyfrom() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('id, shortcut_key')
                ->from($db->quoteName('#__openshop_buyfroms'))
                ->where('published = 1');

        $db->setQuery($query);

        echo json_encode($db->loadObjectList());
    }

    public function search_product() {
        $app = JFactory::getApplication()->input;
        $product_id = $app->getString('id_product');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

//show quantity product
        $id_warehouse = implode(OpenShopHelper::getIdWarehouseInUserGroupArray(), ',');

        $query->select('pro.id,pro.product_sku,prod.product_name, pro.product_sku, pro.product_image,pro.product_price,pro.product_price_input')
                ->from($db->quoteName('#__openshop_products', 'pro'))
                ->join('LEFT', $db->quoteName('#__openshop_productdetails', 'prod') . 'ON prod.product_id = pro.id')
                ->join('LEFT', $db->quoteName('#__openshop_inventories', 'z') . 'ON z.product_id = pro.id')
                ->where('pro.product_sku LIKE "%' . $product_id . '%"')
                ->where('pro.delete_status = 0')
                ->where('z.warehouse_id IN (' . $id_warehouse . ')')
                ->where('pro.published = 1')
                ->group('pro.id');

        $rowsPI = $db->setQuery($query)->loadObjectList();
        $lists = array();
        if (empty($rowsPI)) {
            $query->clear();
            $query->select('pro.id,pro.product_sku,prod.product_name, pro.product_sku, pro.product_image,pro.product_price,pro.product_price_input')
                    ->from($db->quoteName('#__openshop_products', 'pro'))
                    ->join('LEFT', $db->quoteName('#__openshop_productdetails', 'prod') . 'ON prod.product_id = pro.id')
                    ->where('pro.product_sku LIKE "%' . $product_id . '%"')
                    ->where('pro.delete_status = 0')
                    ->where('pro.published = 1')
                    ->group('pro.id');
            $rowsPI = $db->setQuery($query)->loadObjectList();
        }
        $lists['productList'] = $rowsPI;

        $query->clear();
        $query->select('a.*, '
                        . '(select value from #__openshop_optionvaluedetails as c where c.optionvalue_id = a.idcolor) as color,'
                        . '(select value from #__openshop_optionvaluedetails as c where c.optionvalue_id = a.idsize) as size')
                ->from($db->quoteName('#__openshop_inventories', 'a'))
                ->join('LEFT ', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                ->where('warehouse_id IN (' . $id_warehouse . ')')
                ->where('b.product_sku LIKE "%' . $product_id . '%"')
                ->order('idcolor');
        $rowsPQ = $db->setQuery($query)->loadObjectList();
        if (empty($rowsPQ)) {
            $query->clear();
            $query->select('b.value as size,a.product_id')
                    ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON a.option_value_id = b.optionvalue_id')
                    ->join('INNER', $db->quoteName('#__openshop_products', 'c') . 'ON a.product_id = c.id')
                    ->where('c.product_sku LIKE "%' . $product_id . '%"');
            $rowsPQ = $db->setQuery($query)->loadObjectList();
        }
        $lists['quantityProduct'] = $rowsPQ;
        echo json_encode($lists);
    }

    function getSizeColor() {
        $app = Jfactory::getApplication()->input;
        $id = $app->getString('id_pro');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('distinct a.optionvalue_id as id, a.value as size')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_productoptionvalues', 'b') . 'ON a.optionvalue_id = b.option_value_id')
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'c') . 'ON c.option_id = a.option_id')
                ->where('UPPER(c.option_name) = "SIZE"')
                ->where('b.product_id = ' . $id);
        $rows = $db->setQuery($query);

        $objects = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_SIZE'));
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->id, $object->size);
        }
        $lists['size'] = JHtml::_('select.genericlist', $options, 'size_id', ' class="inputbox" ', 'value', 'text', isset($object_name) ? $object_name : 0);

        $query->clear();
        $query->select('distinct a.optionvalue_id as id, a.value as color')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_productoptionvalues', 'b') . 'ON a.optionvalue_id = b.option_value_id')
                ->join('LEFT', $db->quoteName('#__openshop_optiondetails', 'c') . 'ON c.option_id = a.option_id')
                ->where('UPPER(c.option_name) = "COLOR"')
                ->where('b.product_id = ' . $id);
        $db->setQuery($query);

        $objects = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_ORDER_COLOR'));
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->id, $object->color);
        }
        $lists['color'] = JHtml::_('select.genericlist', $options, 'color_id', ' class="inputbox" ', 'value', 'text', isset($object_name) ? $object_name : 0);

        echo json_encode($lists);
    }

    function getWeightHeight() {
        $i = JFactory::getApplication()->input->getInt('i');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

//weight
        $query->select('opv.option_id AS value,opv.value AS text')
                ->from($db->quoteName('#__openshop_optiondetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'opv') . 'ON opd.option_id = opv.option_id')
                ->where('opd.option_name = "WEIGHT"');
        $db->setQuery($query);
        $objects = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_WEIGHT'), 'value', 'text');
        foreach ($objects as $object) {
            $options[] = JHtml::_('select.option', $object->value, $object->text);
        }
        $lists['weight'] = JHtml::_('select.genericlist', $options, 'personofweight_id_' . $i, 'class="inputbox"', "value", "text", 0);

//height
        $query->clear();
        $query->select('opv.option_id AS value,opv.value AS text')
                ->from($db->quoteName('#__openshop_optiondetails', 'opd'))
                ->join('LEFT', $db->quoteName('#__openshop_optionvaluedetails', 'opv') . 'ON opd.option_id = opv.option_id')
                ->where('opd.option_name = "HEIGHT"');
        $db->setQuery($query);
        $objs = $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_HEIGHT'), 'value', 'text');
        foreach ($objs as $obj) {
            $options[] = JHtml::_('select.option', $obj->value, $obj->text);
        }
        $lists['height'] = JHtml::_('select.genericlist', $options, 'personofheight_id_' . $i, 'class="inputbox"', "value", "text", 0);

        echo json_encode($lists);
    }

    public function getFormAjax() {
        $data = JRequest::get('data');
        $arr_form = array();
        foreach ($data['data'] as $k => $v) {
            if ($v['name'] == 'cid[]') {
                $arr_form['cid'] = array($v['value']);
            } else {
                $arr_form[$v['name']] = $v['value'];
            }
        }

        $this->save($arr_form);
    }

    public function getFormTransAjax() {
        $data = JRequest::get('data');
        $arr_form = array();
        foreach ($data['data'] as $k => $v) {
            if ($v['name'] == 'cid[]') {
                $arr_form['cid'] = array($v['value']);
            } else {
                $arr_form[$v['name']] = $v['value'];
            }
        }
        print_r($arr_form);
        exit();
        $model = $this->getModel('order');
        $ret = $model->store_transporter($arr_form);
    }

    public function appointment_order() {
        $app = JFactory::getApplication()->input;
        $id_order = $app->getInt('id_order');

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('transporter_id')
                ->from($db->quoteName('#__openshop_carts'))
                ->where('order_id = ' . $id_order);
        $db->setQuery($query);
        $transporter_id = $db->loadResult();
    }

    public function order_ok() {
//        default message
        $res_return = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD_SALES_SHOP'));

        $app = JFactory::getApplication()->input;
        $view = $app->getString('view');
        $status_appointment = $app->getInt('stauts_appointment');
        $order_id = $app->getString('order_id');  //multi id
        $appointment_description = $app->getString('appointment_description');
        $sale = $app->getInt('sale');
        $total = $app->getInt('total');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

//get id Cart
        $query->select('id')
                ->from($db->quoteName('#__openshop_carts'))
                ->where('order_id IN (' . $order_id . ')');
        $db->setQuery($query);
        $cart_id = $db->loadObjectList();

        switch ($status_appointment) {
            case '3':       //sales shop
//save appointment
                $data_appointment = array(
                    'type' => '3', //sales shop
                    'status' => 'success', //success
                    'cart_id' => $cart_id,
                    'desc' => $appointment_description,
                );

//save incomes
                $data_income = array(
                    'cart_id' => $cart_id,
                    'total' => $total,
                    'sale' => $sale
                );

//update status order
                $data_order = array(
                    'order_id' => $order_id
                );

                if ($view == 'orders') {
                    if ($this->saveAppointment($data_appointment)) {
                        if ($this->saveIncome($data_income)) {
                            $this->updateStatusOrderCheckOut($data_order, 'COMPLETE');

                            foreach ($cart_id as $c) {
                                $query->clear();
                                $query->update($db->quoteName('#__openshop_carts'))
                                        ->set(array('sale = ' . $sale))
                                        ->where('id = ' . $c->id);
                                $db->setQuery($query)->execute();
                            }

                            $res_return = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS_SALES_SHOP'));
                        } else {
                            $query->clear();
                            $query->select('max(id)')->from($db->quoteName('#__openshop_appointments'));
                            $id_app = $db->setQuery($query)->loadResult();
                            OpenShopHelper::deleteRecordTable('#__openshop_appointments', 'id', $id_app);
                        }
                    }
                }

                break;
            case '2':   //shipping
                //save appointment
                $data_appointment = array(
                    'type' => '2', //sales shop
                    'status' => 'waiting', //success
                    'cart_id' => $cart_id,
                    'desc' => $appointment_description,
                );
                //update status order
                $data_order = array(
                    'order_id' => $order_id
                );

                //save incomes
                $data_income = array(
                    'cart_id' => $cart_id,
                    'total' => $total,
                    'sale' => $sale
                );

                if ($view == 'orders') {
                    if ($this->saveAppointment($data_appointment)) {
                        if ($this->updateStatusOrderCheckOut($data_order, 'PROCESSING')) {

                            foreach ($cart_id as $c) {
                                $query->clear();
                                $query->update($db->quoteName('#__openshop_carts'))
                                        ->set(array('sale = ' . $sale))
                                        ->where('id = ' . $c->id);
                                $db->setQuery($query)->execute();
                            }

                            $res_return = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS_SALES_SHOP'));
                        } else {
                            $query->clear();
                            $query->select('max(id)')->from($db->quoteName('#__openshop_appointments'));
                            $id_app = $db->setQuery($query)->loadResult();
                            OpenShopHelper::deleteRecordTable('#__openshop_appointments', 'id', $id_app);
                        }
                    }
                } else if ($view == 'appointments') {
                    if ($this->saveIncome($data_income)) {
                        $this->updateStatusOrderCheckOut($data_order, 'COMPLETE');

                        foreach ($cart_id as $c) {
                            $query->clear();
                            $query->update($db->quoteName('#__openshop_carts'))
                                    ->set(array('sale = ' . $sale))
                                    ->where('id = ' . $c->id);
                            $db->setQuery($query)->execute();

                            $this->updateStatusAppointmentCheckout('SUCCESS', array(
                                'cart_id' => $c->id
                            ));
                        }

                        $res_return = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS_SALES_SHOP'));
                    } else {
                        $query->clear();
                        $query->select('max(id)')->from($db->quoteName('#__openshop_appointments'));
                        $id_app = $db->setQuery($query)->loadResult();
                        OpenShopHelper::deleteRecordTable('#__openshop_appointments', 'id', $id_app);
                    }
                }
                break;
            case '1':   //office
                //save appointment
                $data_appointment = array(
                    'type' => '1', //sales shop
                    'status' => 'waiting', //waiting
                    'cart_id' => $cart_id,
                    'desc' => $appointment_description,
                );

                //update status order
                $data_order = array(
                    'order_id' => $order_id
                );

                //save incomes
                $data_income = array(
                    'cart_id' => $cart_id,
                    'total' => $total,
                    'sale' => $sale
                );

                if ($view == 'orders') {
                    if ($this->saveAppointment($data_appointment)) {
                        if ($this->updateStatusOrderCheckOut($data_order, 'PROCESSING')) {

                            foreach ($cart_id as $c) {
                                $query->clear();
                                $query->update($db->quoteName('#__openshop_carts'))
                                        ->set(array('sale = ' . $sale))
                                        ->where('id = ' . $c->id);
                                $db->setQuery($query)->execute();
                            }

                            $res_return = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS_SALES_SHOP'));
                        } else {
                            $query->clear();
                            $query->select('max(id)')->from($db->quoteName('#__openshop_appointments'));
                            $id_app = $db->setQuery($query)->loadResult();
                            OpenShopHelper::deleteRecordTable('#__openshop_appointments', 'id', $id_app);
                        }
                    }
                } else if ($view == 'appointments') {
                    if ($this->saveIncome($data_income)) {
                        $this->updateStatusOrderCheckOut($data_order, 'COMPLETE');

                        $data_appointment_status = array(
                            'cart_id' => $cart_id,
                        );

                        $query->clear();
                        $query->update($db->quoteName('#__openshop_carts'))
                                ->set(array('sale = ' . $sale))
                                ->where('id = ' . $cart_id);
                        $db->setQuery($query)->execute();

                        $this->updateStatusAppointmentCheckout('SUCCESS', $data_appointment_status);
                        $res_return = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS_SALES_SHOP'));
                    } else {
                        $query->clear();
                        $query->select('max(id)')->from($db->quoteName('#__openshop_appointments'));
                        $id_app = $db->setQuery($query)->loadResult();
                        OpenShopHelper::deleteRecordTable('#__openshop_appointments', 'id', $id_app);
                    }
                }
                break;
            default:
                break;
        }

        if ($view == 'orders') {
//update quantity product in warehouse
            $query->clear();
            $query->select('b.id,b.product_id,b.quantity, product_size, product_color')
                    ->from($db->quoteName('#__openshop_orders', 'a'))
                    ->join('LEFT', $db->quoteName('#__openshop_orderproducts', 'b') . 'ON a.id = b.order_id')
                    ->where('a.id IN  (' . $order_id . ') ');
            $row_order = $db->setQuery($query)->loadObjectList(); //get all order of order id

            $res_warehouse = OpenShopHelper::getIdWarehouseInUserGroupArray();

            foreach ($row_order as $value) {
                $total_import = $db->setQuery('CALL TotalInventory("import",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $res_warehouse[0] . ')')->loadResult();
                $total_number_sale = $db->setQuery('CALL TotalInventory("number_sale",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $res_warehouse[0] . ')')->loadResult();
                if ($this->getQuantityPriority($value->id) == '0') {            //don't quantity priority
//                                                                              total import product
                    $total_inventory = (int) ($total_import) - (int) ($value->quantity);
//                                                          procedure in DB: update total import 
                    $sql_update = 'CALL UpdateInInventory("import",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $res_warehouse[0] . ',' . $total_inventory . ')';
                    $db->setQuery($sql_update)->execute();
//                                                          procedure in DB: update total munber sale 
                    $sql_update = 'CALL UpdateInInventory("number_sale",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $res_warehouse[0] . ',' . ($total_number_sale + $value->quantity) . ')';
                    $db->setQuery($sql_update)->execute();
                } else {                                                        //have priority
                    $quantity_priority = $this->getQuantityPriority($value->id);
                    if ($value->quantity == $quantity_priority) {
//        procedure in DB
                        $sql_update = 'CALL UpdateInInventory("number_sale",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $res_warehouse[0] . ',' . ($total_number_sale + $value->quantity) . ')';
                        $db->setQuery($sql_update)->execute();
                    } else {
                        $temp_quantity = $total_import - ($value->quantity - $quantity_priority);
//                                                      procedure in DB: update total import 
                        $sql_update = 'CALL UpdateInInventory("import",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $res_warehouse[0] . ',' . $temp_quantity . ')';
                        $db->setQuery($sql_update)->execute();
//                                                       procedure in DB
                        $sql_update = 'CALL UpdateInInventory("number_sale",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $res_warehouse[0] . ',' . ($total_number_sale + $value->quantity) . ')';
                        $db->setQuery($sql_update)->execute();
                    }

//                                                          update quantity priority = 0 in order table
                    $query->clear();
                    $query->update($db->quoteName('#__openshop_orderproducts'))
                            ->set(array('quantity_priority = 0'))
                            ->where('order_id = ' . $order_id);
                    $db->setQuery($query)->execute();
                }
            }
        }

        echo json_encode($res_return);
    }

    public function getQuantityPriority($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('quantity_priority')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('id = ' . $id);

        return $db->setQuery($query)->loadResult();
    }

    /*
     * SAVE: SAVING IN APPOINTMENTS TABLE
     */

    public function saveAppointment($data) {
        $db = JFactory::getDbo();
        $c = 1;
        $data_save_appointment = new stdClass();
        $data_save_appointment->appointment_type = $data['type']; //sales Shop
        $data_save_appointment->appointment_status = $data['status']; //success -> appointment types
        $data_save_appointment->appointment_description = $data['desc'];
        $data_save_appointment->appointment_date = date('Y-m-d');
        $data_save_appointment->printed = '0';
        $data_save_appointment->created_date = date('Y-m-d H:i');
        $data_save_appointment->modified_date = '';
        $data_save_appointment->created_by = JFactory::getUser()->get('id');
        $data_save_appointment->modified_by = '';

        foreach ($data['cart_id'] as $cart) {
            $data_save_appointment->cart_id = $cart->id;
            if (!$db->insertObject('#__openshop_appointments', $data_save_appointment))
                $c = 0;
        }

        return $c;
    }

    /*
     * SAVE: SAVING IN INCOMES TABLE
     */

    public function saveIncome($data) {
        $db = JFactory::getDbo();
        $data_save_income = new stdClass();
        $data_save_income->cart_id = $data['cart_id'];
        $data_save_income->total = $data['total'];
        $data_save_income->sale = $data['sale'];
        $data_save_income->created_date = date('Y-m-d H:i');
        $data_save_income->created_by = JFactory::getUser()->get('id');
        if ($db->insertObject('#__openshop_incomes', $data_save_income))
            return 1;
        else
            return 0;
    }

    /*
     * UPDATE: UPDATING STATUS IN ORDER TABLE
     */

    public function updateStatusOrderCheckOut($data, $status) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $c = 1;
//------------ STATUS ORDER
        $query->clear();
        $query->select('orderstatus_id')
                ->from($db->quoteName('#__openshop_orderstatusdetails'))
                ->where('UPPER(orderstatus_name) = "' . $status . '"');
        $db->setQuery($query);
        $order_status = $db->loadResult();

        $data_update_order = new stdClass();
        $data_update_order->order_status_id = $order_status;

        $order_arr = explode(',', $data['order_id']);

        foreach ($order_arr as $o) {
            $data_update_order->id = $o;
            if (!$db->updateObject('#__openshop_orders', $data_update_order, 'id'))
                $c = 0;
        }

        return $c;
    }

    /*
     * UPDATE: UPDATING STATUS IN APPOINTMENT TABLE AFTER CHECKOUT SUCCESS
     */

    function updateStatusAppointmentCheckout($status, $data = null) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $data_update = new stdClass();
        $data_update->cart_id = $data['cart_id'];
        $data_update->appointment_status = strtolower($status);
        if ($db->updateObject('#__openshop_appointments', $data_update, 'cart_id'))
            return 1;
        else
            return 0;
    }

    /*
     * DELETE PRODUCT IN ORDERPRODUCT TABLE
     */

    function order_delete_product() {
        $money = 0;
        $res = array(
            'status' => 'error',
            'mesage' => 'Error'
        );

        $app = JFactory::getApplication()->input;
        $order_id_product = $app->getInt('order_id_product');
        $order_id = $app->getInt('order_id');

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->clear();
        $query->update($db->quoteName('#__openshop_orderproducts'))
                ->set('order_product_delete = 1')
                ->where('id = ' . $order_id_product);

        $db->setQuery($query);

        if ($db->execute()) {
//UPDATE ORDER TABLE
            $query->clear();
            $query->select('count(id)')
                    ->from($db->quoteName('#__openshop_orderproducts'))
                    ->where('order_id = ' . $order_id)
                    ->where('order_product_delete = 0');
            if (!$db->setQuery($query)->loadResult()) {                              //check number record : == 0
//update status in order table
                $query->clear();
                $query->update($db->quoteName('#__openshop_orders'))
                        ->set('order_status_id = 1')
                        ->where('id = ' . $order_id);

                $db->setQuery($query)->execute();
            }
// END: UPDATE ORDER TABLE

            $query->clear();
            $query->select('total_price')
                    ->from($db->quoteName('#__openshop_orderproducts'))
                    ->where('order_product_delete = 0')
                    ->where('order_id = ' . $order_id);
            $db->setQuery($query);

            foreach ($db->loadAssocList() as $value) {
                $money += (int) ($value['total_price']);
            }

            $query->clear();
            $data_update_total = new stdClass();
            $data_update_total->id = $order_id;
            $data_update_total->total = $money;

            $db->updateObject('#__openshop_orders', $data_update_total, 'id');

            $res = array(
                'status' => 'success',
                'mesage' => 'Delete. Success!',
                'total_money' => $money
            );
        }

        echo json_encode($res);
    }

    /*
     * DELETE: UPDATE ORDER STATUS IN ORDERS TABLE
     */

    function check_delete_order() {
        $res_return = array('status' => 'error', 'message' => JText::_('OPENSHOP_ERROR'));

        $app = JFactory::getApplication()->input;
        $order_id = $app->getInt('order_id');
        $order_delete = $app->getInt('order_delete');

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('order_id = ' . $order_id);
        $db->setQuery($query);

        if ($db->loadResult() == '0') {
            $query->clear();
            $query->select('id')
                    ->from($db->quoteName('#__openshop_orderstatusdetails'))
                    ->where('UPPER(orderstatus_name) = "CANCELED"');
            $db->setQuery($query);

            $data = new stdClass();
            $data->order_status_id = $db->loadResult();
            $data->id = $order_id;

            if ($db->updateObject('#__openshop_orders', $data, 'id')) {
                $res_return = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS_DELETE_ORDER'));
            }
        }

        echo json_encode($res_return);
    }

    public function delete_order() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $res_return = array('status' => 'error', 'message' => JText::_('OPENSHOP_ERROR'));

        $app = JFactory::getApplication()->input;
        $order_id = $app->getInt('order_id');
        $reason = $app->getInt('reason');
        $desc = $app->getString('desc');
        $act = $app->getString('act');

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $data_order_option = new stdClass();
        $data_order_option->order_id = $order_id;
        $data_order_option->reasondetail_id = $reason;
        $data_order_option->reason_description = $desc;
        $data_order_option->created_date = date('Y-m-d H:m:s');
        $data_order_option->created_by = JFactory::getUser()->get('id');

        $query->clear();
        $query->select('id')
                ->from($db->quoteName('#__openshop_orderstatusdetails'))
                ->where('UPPER(orderstatus_name) = "CANCELED"');
        $status_order = $db->setQuery($query)->loadResult();

        $data = new stdClass();
        $data->order_status_id = $status_order;
        $data->id = $order_id;

        if ($db->insertObject('#__openshop_orderreasons', $data_order_option)) {
            if ($db->updateObject('#__openshop_orders', $data, 'id')) {

                //update inventory  
                if ($act == 'return') {
                    $this->returnUpdateInventory($order_id);
                }

                $query->clear();
                $query->update($db->quoteName('#__openshop_orderproducts'))
                        ->set('order_product_delete = 1')
                        ->where('order_id = ' . $order_id);
                $db->setQuery($query)->execute();

                $res_return = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS_DELETE_ORDER'));
            } else {
//                $query->clear();
//                $query->select('max(id)')
//                        ->from($db->quoteName('#__openshop_orderreasons'));
//                $db->setQuery($query);
//                $id_reason = $db->loadResult();
//                
//                
            }
        }

        echo json_encode($res_return);
    }

    public function returnUpdateInventory($order_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->clear();
        $query->select('b.product_id,b.quantity, product_size, product_color')
                ->from($db->quoteName('#__openshop_orders', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_orderproducts', 'b') . 'ON a.id = b.order_id')
                ->where('a.id = ' . $order_id);
        $row_order = $db->setQuery($query)->loadObjectList();

        $query->clear();
        $query->select('id,user_group_manage')
                ->from($db->quoteName('#__openshop_warehouses'));
        $rows_warehouse = $db->setQuery($query)->loadObjectList();

        $usergroup_id = OpenShopHelper::getUsergroupId(JFactory::getUser()->id);
//manage warehouse: id warehouse
        $res_warehouse = array();
        foreach ($rows_warehouse as $v_id) {
            $arr = explode(',', $v_id->user_group_manage);
            foreach ($arr as $v_arr) {
                if ($v_arr == $usergroup_id) {
                    $res_warehouse[] = $v_id->id;
                    break;
                }
            }
        }

        foreach ($row_order as $value) {
            $query->clear();
            $query->select('warehouse_id')
                    ->from($db->quoteName('#__openshop_inventories'))
                    ->where('product_id = ' . $value->product_id);
            if (isset($value->product_size)) {
                $query->where('idsize = ' . $value->product_size);
            }
            if (isset($value->product_color)) {
                $query->where('idcolor = ' . $value->product_color);
            }
            $rows_in = $db->setQuery($query)->loadObjectList();

            $warehouse_id = '0';
            foreach ($rows_in as $row_in) {
                if (in_array($row_in->warehouse_id, $res_warehouse)) {
                    $warehouse_id = $row_in->warehouse_id;
                    break;
                }
            }

            $total_import = $db->setQuery('CALL TotalInventory("import",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $warehouse_id . ')')->loadResult();
            $total_inventory = (int) ($total_import) + (int) ($value->quantity);

            $total_number_sale = $db->setQuery('CALL TotalInventory("number_sale",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $warehouse_id . ')')->loadResult();
            $total_numSale = (int) ($total_number_sale) - (int) ($value->quantity);

//        procedure in DB
            $sql_update = 'CALL UpdateInInventory("import",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $warehouse_id . ',' . $total_inventory . ')';
            $db->setQuery($sql_update)->execute();
//        procedure in DB
            $sql_update = 'CALL UpdateInInventory("number_sale",' . $value->product_id . ',' . $value->product_size . ',' . $value->product_color . ',' . $warehouse_id . ',' . $total_numSale . ')';
            $db->setQuery($sql_update)->execute();
        }
    }

    public function updateDescOrder() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD'));
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $app = JFactory::getApplication()->input;
        $desc = $app->getString('desc');
        $id_orderpro = $app->getInt('id_orderpro');

        $query->update($db->quoteName('#__openshop_orderproducts'))
                ->set(array('order_product_desc = "' . $desc . '"'))
                ->where('id = ' . $id_orderpro);
        if ($db->setQuery($query)->execute()) {
            $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
        }

        echo json_encode($res);
    }

    function quantityPriority() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD'));
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $quantity = $app->getInt('quan');
        $id_orderpro = $app->getInt('id_orderpro');

        $query->select('product_id,product_size,product_color,quantity_priority')
                ->from($db->quoteName('#__openshop_orderproducts'))
                ->where('id = ' . $id_orderpro);

        $row_product = $db->setQuery($query)->loadObject();

//get id warehouse in user
        $id_warehouse = OpenShopHelper::getIdWarehouseInUserGroupArray();

//get total quantity of the product in warehouse
        $temp_w = '0';
        foreach ($id_warehouse as $id_w) {
            $quantity_warehouse = $db->setQuery('CALL TotalInventory("import",' . $row_product->product_id . ', ' . $row_product->product_size . ', ' . $row_product->product_color . ', ' . $id_w . ')')->loadResult();
            if (isset($quantity_warehouse)) {
                $temp_w = $id_w;
                break;
            }
        }

        if (!empty($row_product->quantity_priority) || $row_product->quantity_priority != '0') {
            $total_up = (int) ($quantity_warehouse - $quantity + $row_product->quantity_priority) < 0 ? '0' : (int) ($quantity_warehouse - $quantity + $row_product->quantity_priority);
        } else {
            $total_up = (int) ($quantity_warehouse - $quantity) < 0 ? '0' : (int) ($quantity_warehouse - $quantity);
        }

        if ($db->setQuery('CALL UpdateInInventory("import",' . $row_product->product_id . ', ' . $row_product->product_size . ', ' . $row_product->product_color . ', ' . $temp_w . ', ' . $total_up . ')')->execute()) {
            $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
        }

        if ((int) ($quantity_warehouse - $quantity) < 0) {
            $query->clear();
            $query->update($db->quoteName('#__openshop_orderproducts'))
                    ->where('id = ' . $id_orderpro);
            if (!empty($row_product->quantity_priority) || $row_product->quantity_priority != '0') {
                $query->set(array('quantity_priority = ' . ($quantity_warehouse + $row_product->quantity_priority)));
            } else {
                $query->set(array('quantity_priority = ' . $quantity_warehouse));
            }
            if ($db->setQuery($query)->execute()) {
                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
            }
        } else {
            $query->clear();
            $query->update($db->quoteName('#__openshop_orderproducts'))
                    ->set(array('quantity_priority = ' . $quantity))
                    ->where('id = ' . $id_orderpro);
            if ($db->setQuery($query)->execute()) {
                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
            }
        }

        echo json_encode($res);
    }

    public function reversalOrder() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD'));
        $app = JFactory::getApplication()->input;
        $id_order = $app->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->clear();
            $query->update($db->quoteName('#__openshop_orders'))
                    ->set('order_status_id = 2')
                    ->where('id = ' . $id_order);
            if ($db->setQuery($query)->execute()) {
                $query->clear();
                $query->update($db->quoteName('#__openshop_orderproducts'))
                        ->set('order_product_delete = 0')
                        ->where('order_id = ' . $id_order);
                if ($db->setQuery($query)->execute()) {
                    $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'), 'link' => 'index.php?option=com_openshop&view=orders');
                }
            }
            echo json_encode($res);
        } catch (Exception $ex) {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_DONT_CONNECT_SERVER'));
            echo json_encode($res);
        }
    }

    function getOrderByPhone() {
        $app = JFactory::getApplication()->input;
        $phone = $app->getInt('phone');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $res_1 = array();
        $res_0 = array();
        try {
            $query->select('a.id,b.product_id,b.product_name,b.product_size,b.quantity,b.price,b.order_product_desc,c.product_image')
                    ->from($db->quoteName('#__openshop_orders', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_orderproducts', 'b') . ' ON a.id = b.order_id')
                    ->join('INNER', $db->quoteName('#__openshop_products', 'c') . ' ON b.product_id = c.id')
                    ->where('payment_telephone =' . $phone)
                    ->where('order_status_id = 8');
            $rows = $db->setQuery($query)->loadObjectList();

            foreach ($rows as $row) {
                $query->clear();
                $query->select('id')
                        ->from($db->quoteName('#__openshop_inventories'))
                        ->where('product_id = ' . $row->product_id)
                        ->where('idsize = ' . $row->product_size)
                        ->where('total_import_product >= ' . $row->quantity);
                $pros = $db->setQuery($query)->loadObjectList();

                if (count($pros)) {
                    $res_1[] = array(
                        'check_inventory' => 1,
                        'id' => $row->id,
                        'product_name' => $row->product_name,
                        'image' => $row->product_image,
                        'quantity' => $row->quantity,
                        'price' => $row->price,
                        'order_product_desc' => $row->order_product_desc
                    );
                } else {
                    $res_0[] = array(
                        'check_inventory' => 0,
                        'id' => $row->id,
                        'product_name' => $row->product_name,
                        'image' => $row->product_image,
                        'quantity' => $row->quantity,
                        'price' => $row->price,
                        'order_product_desc' => $row->order_product_desc
                    );
                }
            }

            echo json_encode(array($res_0, $res_1));
        } catch (Exception $ex) {
            //code here
        }
    }

    function getAppointmentByPhone() {
        $app = JFactory::getApplication()->input;
        $phone = $app->getInt('phone');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $res_1 = array();
        $res_0 = array();
        try {
            $query->select('a.id,b.product_id,b.product_name,b.product_size,b.quantity,b.price,b.order_product_desc,c.product_image')
                    ->from($db->quoteName('#__openshop_orders', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_orderproducts', 'b') . ' ON a.id = b.order_id')
                    ->join('INNER', $db->quoteName('#__openshop_products', 'c') . ' ON b.product_id = c.id')
                    ->where('payment_telephone =' . $phone)
                    ->where('order_status_id = 10');        //processing... (đang đặt hàng)
            $rows = $db->setQuery($query)->loadObjectList();

            foreach ($rows as $row) {
                $query->clear();
                $query->select('id')
                        ->from($db->quoteName('#__openshop_inventories'))
                        ->where('product_id = ' . $row->product_id)
                        ->where('idsize = ' . $row->product_size)
                        ->where('total_import_product >= ' . $row->quantity);
                $pros = $db->setQuery($query)->loadObjectList();

                if (count($pros)) {
                    $res_1[] = array(
                        'check_inventory' => 1,
                        'id' => $row->id,
                        'product_name' => $row->product_name,
                        'image' => $row->product_image,
                        'quantity' => $row->quantity,
                        'price' => $row->price,
                        'order_product_desc' => $row->order_product_desc
                    );
                } else {
                    $res_0[] = array(
                        'check_inventory' => 0,
                        'id' => $row->id,
                        'product_name' => $row->product_name,
                        'image' => $row->product_image,
                        'quantity' => $row->quantity,
                        'price' => $row->price,
                        'order_product_desc' => $row->order_product_desc
                    );
                }
            }

            echo json_encode(array($res_0, $res_1));
        } catch (Exception $ex) {
            //code here
        }
    }

}
