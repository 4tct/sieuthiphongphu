<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopControllerConfiguration extends JControllerLegacy {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * Save the category
     *
     */
    function save() {
        $post = JRequest::get('post', JREQUEST_ALLOWRAW);
        if (!isset($post['customer_group_display'])) {
            $post['customer_group_display'] = '';
        }
        if (!isset($post['sort_options'])) {
            $post['sort_options'] = '';
        }
        $model = $this->getModel('configuration');
        $ret = $model->store($post);
        if ($ret) {
            $msg = JText::_('OPENSHOP_CONFIGURATION_SAVED');
        } else {
            $msg = JText::_('OPENSHOP_CONFIGURATION_SAVING_ERROR');
        }
        $this->setRedirect('index.php?option=com_openshop&view=configuration', $msg);
    }

    /**
     * Cancel the configuration
     *
     */
    function cancel() {
        $this->setRedirect('index.php?option=com_openshop&view=dashboard');
    }
    
    function delete_img_inOrder()
    {
        $key = JFactory::getApplication()->input->getString('key');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('config_value')
                ->from($db->quoteName('#__openshop_configs'))
                ->where('config_key = "'. $key .'"');
        $imgName = $db->setQuery($query)->loadResult();
        
        if(JFile::exists(OPENSHOP_PATH_IMG_OTHER . $imgName)){
            JFile::delete(OPENSHOP_PATH_IMG_OTHER . $imgName);
        }
        
        $query->clear();
        $query->update($db->quoteName('#__openshop_configs'))
                ->set('config_value = ""')
                ->where('config_key = "'. $key .'"');
        if($db->setQuery($query)->execute()){
            $res = array('status' => 'success' , 'message' => JText::_('OPENSHOP_SUCCESS'));
        }
        else
        {
            $res = array('status' => 'error' , 'message' => JText::_('OPENSHOP_FAILD'));
        }
        
        echo json_encode($res);
    }
}
