

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopControllerScans extends OpenShopController {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    function scanValue() {
        $res = array('status' => 'error', 'message' => 'Không zô chế ơi!');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $app = JFactory::getApplication()->input;
        $val = $app->getString('val');
        $zone_id = $app->getInt('zone');
        $mode = $app->getInt('mode');

//cắt product_id - size - color
        $id_size_color = explode('-', $val);

        $data = new stdClass();
        $data->product_sku = strtoupper($id_size_color[0]);
        $data->size_id = $id_size_color[1];
        $data->product_name = strtoupper($val);

        if (isset($id_size_color[2])) {
            $data->color_id = $id_size_color[2];
        } else {
            $data->color_id = 0;
        }

        $data->quantity_import = 1;
        $data->import_by = JFactory::getUser()->id;

        $now = getdate();
        $hour = $now["hours"] < 10 ? '0' . $now["hours"] : $now["hours"];
        $min = $now["minutes"] < 10 ? '0' . $now["minutes"] : $now["minutes"];
        $sec = $now["seconds"] < 10 ? '0' . $now["seconds"] : $now["seconds"];
        $time = $hour . ":" . $min . ":" . $sec;
        $day = $now["mday"] < 10 ? '0' . $now["mday"] : $now["mday"];
        $month = $now["mon"] < 10 ? '0' . $now["mon"] : $now["mon"];
        $date_now = $now["year"] . '-' . $month . '-' . $day;
        $date_scan = $date_now . ' ' . $time;

        $data->date_import = $date_scan;
        $data->zone_import = $zone_id;

        $scan_mode = 'Nhập tay';
        if ($mode == '231') {
            $scan_mode = 'Quét máy';
        }
        $data->scan_mode = $scan_mode;

        $date_scan_vn = $time . ' ' . date('d-m-Y', strtotime($date_now));

        try {
            if ($db->insertObject('#__openshop_kiemkhos', $data)) {
                $res = array('status' => 'success',
                    'message' => 'Zô rùi chế ơi!',
                    'add' => '0',
                    'product_sku' => strtoupper($id_size_color[0]),
                    'product_name' => strtoupper($val),
                    'quantity' => '1',
                    'size' => $this->getSizeColor($id_size_color[1]),
                    'color' => $this->getSizeColor(isset($id_size_color[2]) ? $id_size_color[2] : '0'),
                    'scan_by' => JFactory::getUser()->name,
                    'zone' => $this->getZoneScan($zone_id),
                    'mode' => $scan_mode,
                    'date_scan' => $date_scan_vn
                );
            }
        } catch (Exception $ex) {
            $res = array('status' => 'error',
                'message' => 'Không kết nối được với máy chủ!'
            );
        }

        echo json_encode($res);
    }

    function getSizeColor($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('optionvalue_id = ' . $id);

        $res = $db->setQuery($query)->loadResult();

        return is_null($res) ? 'No Item' : $res;
    }

    function delete_all_scaned() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FIELD'));
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->delete($db->quoteName('#__openshop_kiemkhos'));
            if ($db->setQuery($query)->execute()) {
                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
            } else {
                $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FIELD'));
            }
        } catch (Exception $ex) {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FIELD'));
        }

        echo json_encode($res);
    }

    function getZoneScan($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('optionvalue_id = ' . $id);
        return $db->setQuery($query)->loadResult();
    }

    function writeError() {
        $app = JFactory::getApplication()->input;
        $error = $app->getString('error');
        $mode = $app->getInt('mode');
        $zone = $app->getInt('zone');
        $path = OPENSHOP_PATH_FILES . 'error_scan.txt';
        $fp = fopen($path, 'a+');

        $now = getdate();
        $hour = $now["hours"] < 10 ? '0' . $now["hours"] : $now["hours"];
        $min = $now["minutes"] < 10 ? '0' . $now["minutes"] : $now["minutes"];
        $sec = $now["seconds"] < 10 ? '0' . $now["seconds"] : $now["seconds"];
        $time = $hour . ":" . $min . ":" . $sec;
        $day = $now["mday"] < 10 ? '0' . $now["mday"] : $now["mday"];
        $month = $now["mon"] < 10 ? '0' . $now["mon"] : $now["mon"];
        $date_now = $now["year"] . '-' . $month . '-' . $day;
        //get date
        $date_scan_vn = $time . ' ' . date('d-m-Y', strtotime($date_now));
        //mode scan
        $scan_mode = 'Nhập tay';
        if ($mode == '231') {
            $scan_mode = 'Quét máy';
        }

        $data = $error . ' *** ' . $date_scan_vn . ' *** ' . JFactory::getUser()->name . ' *** ' . $scan_mode . ' *** ' . $this->getZoneScan($zone) . "\r\n";
        fwrite($fp, $data);
        fclose($fp);
    }

    public function getUrlErrorScan() {
        $path = OPENSHOP_PATH_FILES . 'error_scan.txt';
        if (!JFile::exists($path)) {
            $fp = fopen($path, 'w+');
            fclose($fp);
        }

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($path));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile($path);
        exit;
    }

    public function deleteError() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_DELETE_ERROR_FIELD'));
        $path = OPENSHOP_PATH_FILES . 'error_scan.txt';
        if (JFile::exists($path)) {
            unlink($path);
            $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_DELETE_ERROR_SUCCESS'));
        }

        echo json_encode($res);
    }
    
    function deleteScanPro(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('product_name')
                ->from($db->quoteName('#__openshop_kiemkhos'))
                ->where('id = ' . JFactory::getApplication()->input->getString('id'));
        $nameP = $db->setQuery($query)->loadResult();
        $query->clear();
        $query->delete($db->quoteName('#__openshop_kiemkhos'))
                ->where('id = ' . JFactory::getApplication()->input->getString('id'));
        if($db->setQuery($query)->execute()){
           $path = OPENSHOP_PATH_FILES . 'error_scan.txt';
            $fp = fopen($path, 'a+');

            $now = getdate();
            $hour = $now["hours"] < 10 ? '0' . $now["hours"] : $now["hours"];
            $min = $now["minutes"] < 10 ? '0' . $now["minutes"] : $now["minutes"];
            $sec = $now["seconds"] < 10 ? '0' . $now["seconds"] : $now["seconds"];
            $time = $hour . ":" . $min . ":" . $sec;
            $day = $now["mday"] < 10 ? '0' . $now["mday"] : $now["mday"];
            $month = $now["mon"] < 10 ? '0' . $now["mon"] : $now["mon"];
            $date_now = $now["year"] . '-' . $month . '-' . $day;
            //get date
            $date_scan_vn = $time . ' ' . date('d-m-Y', strtotime($date_now));
            //mode scan

            $data = 'Xóa sản phẩm: ' . $nameP . ' *** ' . $date_scan_vn . ' *** ' . JFactory::getUser()->name . "\r\n";
            fwrite($fp, $data);
            fclose($fp); 
        }
    }

}
