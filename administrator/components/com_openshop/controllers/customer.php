<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerCustomer extends OpenShopController {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    function country() {
        $json = array();
        $countryId = JRequest::getVar('country_id');
        $zones = OpenShopHelper::getCountryZones($countryId);
        if (count($zones)) {
            $json['zones'] = $zones;
        } else {
            $json['zones'] = '';
        }
        echo json_encode($json);
        exit();
    }

    function changeZone() {
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,district_name')
                ->from($db->quoteName('#__openshop_districts'))
                ->where('published = 1')
                ->where('parent_id = ' . $app->getInt('id'));
        $rows = $db->setQuery($query)->loadObjectList();
        $h = '';
        $h .= '<select name="district_id" id="district_id" class="input-memdium">';
        $h .= '<option value="0">'. JText::_('OPENSHOP_CHOOSE_DISTRICT') .'</option>';
        foreach ($rows as $row) {
            $h .= '<option value="' . $row->id . '">' . $row->district_name . '</option>';
        }
        $h .= '</select>';
        
        echo $h;
    }

}
