<?php

defined('_JEXEC') or die;

class OpenShopControllerFunctions extends JControllerLegacy {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    public function check_input() {
        $app = JFactory::getApplication()->input;
        $str = $app->getString('d');
        $table_check = $app->getString('t');
        $alias = JFilterOutput::stringURLSafe($str);

        //check databse
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('*')
                ->from($db->quoteName('#__' . $table_check))
                ->where('product_alias = "' . $alias . '"');
        $db->setQuery($query);
        $res = $db->loadObjectList();
        if (count($res) != 0) {        //duplicate alias
            $alias = $alias . '-' . count($res);
        }
        $r = array('alias' => $alias);
        echo json_encode($r);
    }

    public function check_town() {
        $app = JFactory::getApplication()->input;
        $id_town = $app->getInt('i');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('*')
                ->from($db->quoteName('#__openshop_districts'))
                ->where('parent_id = ' . $id_town)
                ->where('published = 1');
        $db->setQuery($query);

        $row = $db->loadObjectList();
        $h = '<option value="0">' . JText::_("OPENSHOP_CHOOSE") . '</option>';
        if (count($row) > 0) {     //It has data
            foreach ($row as $value) {
                $h .= '<option value="' . $value->id . '">' . $value->district_name . '</option>';
            }
        }

        echo $h;
    }

    public function check_geozone() {
        $app = JFactory::getApplication()->input;
        $zone_id = $app->getInt('zone_id');
        $district_id = $app->getInt('district_id');

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('transporter_name')
                ->from($db->quoteName('#__openshop_geozones', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_geozonezones', 'b') . 'ON a.id = b.geozone_id')
                ->join('LEFT', $db->quoteName('#__openshop_transporters', 'c') . 'ON c.id = a.transporter_id')
                ->where('b.zone_id = ' . $zone_id)
                ->where('b.district_id = ' . $district_id);
        $row = $db->setQuery($query)->loadObject();

        if (count($row) == 0) {  //true
            $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_CHECK_GEOZONE_SUCCESS'));
        } else {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_CHECK_GEOZONE_FAILD'), 'transporter' => $row->transporter_name);
        }

        echo json_encode($res);
    }

    public function export_excel() {
        require_once JPATH_ROOT . DS . 'administrator' . DS . 'includes' . DS . 'phpexcel' . DS . 'PHPExcel.php';
        $app = JFactory::getApplication()->input;
        $view = $app->getString('v');
        switch ($view) {
            case 'carts':
                $this->cart_export();
                break;
            default :
                break;
        }
    }

    public function cart_export() {
        $app = JFactory::getApplication()->input;
        $id_transporter = $app->getInt('id');
        $status_export = $app->get('status_export');
        $column_export = $app->getString('content-export-column');
        $export_name = $app->getString('export_name');

        $arr_column_export = explode(',', $column_export);

        if (isset($status_export) || $status_export != '') {
            $id = '';
            $t = 1;
            foreach ($status_export as $v) {
                if ($t == 0) {
                    $id .= ',';
                }
                $id .= $v;
                $t = 0;
            }

            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);

            $query->select('c.product_sku,c.product_name,c.total_price,b.payment_fullname,b.payment_telephone,b.payment_address')
                    ->from($db->quoteName('#__openshop_carts', 'a'))
                    ->join('LEFT', $db->quoteName('#__openshop_orders', 'b') . 'ON a.order_id = b.id')
                    ->join('LEFT', $db->quoteName('#__openshop_orderproducts', 'c') . 'ON c.order_id = b.id')
                    ->where('b.order_status_id IN (' . $id . ')')
                    ->where('a.transporter_id = ' . $id_transporter);
            $rows = $db->setQuery($query)->loadObjectList();

            $query->clear();
            $query->select('transporter_name')
                    ->from($db->quoteName('#__openshop_transporters'))
                    ->where('id = ' . $id_transporter);
            $row_transporter_name = $db->loadResult();

            $objPHPExcel = new PHPExcel();
            $cell = $objPHPExcel->setActiveSheetIndex(0);

            $position_column_act = 3;   //start position
            $cell->setCellValue('A' . $position_column_act, JText::_('OPENSHOP_STT'));

            $j = 1;
            foreach ($arr_column_export as $key => $value) {
                if ($value == 1) {
                    $cell->setCellValue(chr(65 + $j) . $position_column_act, 'Mã sản phẩm');
                    $objPHPExcel->getActiveSheet()->getColumnDimension(chr(65 + $j))->setWidth('15');
                }

                if ($value == 2) {
                    $cell->setCellValue(chr(65 + $j) . $position_column_act, 'Tên sản phẩm');
                    $objPHPExcel->getActiveSheet()->getColumnDimension(chr(65 + $j))->setWidth('50');
                }

                if ($value == 3) {
                    $cell->setCellValue(chr(65 + $j) . $position_column_act, 'Thành Tiền');
                    $objPHPExcel->getActiveSheet()->getColumnDimension(chr(65 + $j))->setWidth('15');
                }

                if ($value == 4) {
                    $cell->setCellValue(chr(65 + $j) . $position_column_act, 'Tên khách hàng');
                    $objPHPExcel->getActiveSheet()->getColumnDimension(chr(65 + $j))->setWidth('30');
                }

                if ($value == 5) {
                    $cell->setCellValue(chr(65 + $j) . $position_column_act, 'Số điện thoại');
                    $objPHPExcel->getActiveSheet()->getColumnDimension(chr(65 + $j))->setWidth('15');
                }

                if ($value == 6) {
                    $cell->setCellValue(chr(65 + $j) . $position_column_act, 'Địa chỉ');
                    $objPHPExcel->getActiveSheet()->getColumnDimension(chr(65 + $j))->setWidth('50');
                }

                $j = $j + 1;
            }

            $i = $position_column_act + 1;
            $set_data = $objPHPExcel->setActiveSheetIndex(0);
            foreach ($rows as $key => $row) {
                $j = 1;
                $set_data->setCellValue(chr(65) . $i, ($key + 1));
                foreach ($arr_column_export as $key => $value) {
                    if ($value == 1) {
                        $set_data->setCellValue(chr(65 + $j) . $i, $row->product_sku);
                    }

                    if ($value == 2) {
                        $set_data->setCellValue(chr(65 + $j) . $i, $row->product_name);
                    }

                    if ($value == 3) {
                        $set_data->setCellValue(chr(65 + $j) . $i, $row->total_price);
                    }

                    if ($value == 4) {
                        $set_data->setCellValue(chr(65 + $j) . $i, $row->payment_fullname);
                    }

                    if ($value == 5) {
                        $set_data->setCellValue(chr(65 + $j) . $i, $row->payment_telephone);
                    }

                    if ($value == 6) {
                        $set_data->setCellValue(chr(65 + $j) . $i, $row->payment_address);
                    }

                    $j++;
                }
                $i++;
            }


//            format cell
            if ($position_column_act >= 2) {
                $cell->setCellValue('A1', 'THÔNG TIN ĐƠN HÀNG GỬI - ' . $row_transporter_name);
                $objPHPExcel->getActiveSheet()->mergeCells('A1:' . chr(65 + ($j - 1)) . '1');
                $objPHPExcel->getActiveSheet()->getStyle('A1:' . chr(65 + ($j - 1)) . $position_column_act)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            if ($position_column_act >= 3) {
                $cell->setCellValue(chr(65 + ($j - 2)) . '2', JText::_('OPENSHOP_TOTAL_ORDER'));
                $cell->setCellValue(chr(65 + ($j - 1)) . '2', '=COUNT(A4:A' . $j . ')');
            }

            $BStyle = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('A1:' . chr(65 + ($j - 1)) . $i)->applyFromArray($BStyle); //set border
            $objPHPExcel->getActiveSheet()->getStyle('A1' . ':' . chr(65 + ($j - 1)) . $position_column_act)->getFont()->setBold(true);  //set bold

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=" . $export_name . '-' . date('d-m-Y') . ".xlsx");
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        } else {
            $this->setRedirect('index.php?option=com_openshop&view=carts&id_transporter=' . $id_transporter);
        }
    }

    function delete_image_option_color() {
        $id = JFactory::getApplication()->input->getInt('id');
        $img = JFactory::getApplication()->input->getString('img');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__openshop_optionvaluedetails'))
                ->set('img_color = ""')
                ->where('id = ' . $id);
        if ($db->setQuery($query)->execute()) {
            if (JFile::exists(OPENSHOP_PATH_IMG_COLOR . $img)) {
                JFile::delete(OPENSHOP_PATH_IMG_COLOR . $img);
            }

            $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_SUCCESS'));
        } else {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_FAILD'));
        }

        echo json_encode($res);
    }

    function uploadfile() {
        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';
        $fileName = $this->uploadFileToServer();
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $filename = OPENSHOP_PATH_FILES . $fileName;
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objReader->setReadDataOnly(true);

        /**  Load $inputFileName to a PHPExcel Object  * */
        $objPHPExcel = $objReader->load("$filename");

        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
//        $highestColumn = $objWorksheet->getHighestColumn();
//        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        for ($row = 4; $row <= $highestRow; ++$row) {
            $name = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
            $sku = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
            $size = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
            $quantity = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();

            if (empty($size)) {
                $size = 'F';
            }

            $query->clear();
            $d = new stdClass();
            $d->product_name = $name;
            $d->product_sku = $sku;
            $d->product_size = $size;
            $d->quantity = $quantity;
            $db->insertObject('#__openshop_compare_inventories', $d);
        }
        unlink($filename);
        JFactory::getApplication()->redirect('index.php?option=com_openshop&view=compare_inventories');
    }

    public function uploadFilePhone() {
        $this->uploadFileToServer(OPENSHOP_PATH_SCANS, 'index.php?option=com_openshop&view=scans&layout=scan_phone', 0);
    }

    function uploadFileToServer($pathIni = null, $url = null, $return_file = 1) {
        if (isset($pathIni) && !empty($pathIni)) {
            $path = $pathIni;
        } else {
            $path = OPENSHOP_PATH_FILES;
        }
        $file = $_FILES['fileImport'];
        if (!empty($file['name'])) {
            $nameFile = rand(10000, 999999) . '_' . $file['name'];
            JFile::upload($file['tmp_name'], $path . $nameFile);
            if ($return_file) {
                return $nameFile;
            }
        }

        if (isset($url) && !empty($url)) {
            $this->redirect($url);
        }
    }

    function setSession() {
        $session = JFactory::getSession();
        $t = $session->get('menu_toggle');
        if (isset($t)) {
            if ($t == 'small') {
                $session->set('menu_toggle', 'large');
            } else {
                $session->set('menu_toggle', 'small');
            }
        } else {
            $session->set('menu_toggle', 'small');
        }
    }

}
