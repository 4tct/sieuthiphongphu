<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerTheme extends OpenShopController
{

	/**
	 * Constructor function
	 *
	 * @param array $config
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
	
	}

	function install()
	{
		$model = & $this->getModel('theme');
		$ret = $model->install();
		if ($ret)
		{
			$msg = JText::_('OPENSHOP_THEME_INSTALLED');
		}
		else
		{
			$msg = JRequest::setVar('msg', 'OPENSHOP_THEME_INSTALL_ERROR');
		}
		$this->setRedirect('index.php?option=com_openshop&view=themes', $msg);
	}
}