<?php
defined('_JEXEC') or die;

class OpenShopControllerMenus extends JControllerLegacy
{
    function __construct($config = array()) {
        parent::__construct($config);
    }
    
    public function update_access()
    {
        $app = JFactory::getApplication()->input;
        $id = $app->get('id');
        $access = $app->get('access');
        $table = $app->get('table');
        $col_u = $app->get('col_u');
        $col_w = $app->get('col_w');
        
        //UPDATE STATE ACCESS
        echo OpenShopHelper::update_state($table, $col_u, $access, $col_w, $id);
       
    }
}
