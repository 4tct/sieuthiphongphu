<?php

defined('_JEXEC') or die;

class OpenShopControllerReason extends OpenShopController {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    function saveReason() {
        $data = JRequest::get('data');
        $arr_form = array();
        foreach ($data['data'] as $k => $v) {
            if ($v['name'] == 'cid[]') {
                $arr_form['cid'] = array($v['value']);
            } else {
                $arr_form[$v['name']] = $v['value'];
            }
        }
        if ($arr_form['reason_name'] != '') {
            $this->save($arr_form);
        } else {
            echo json_encode(array('status' => 'error', 'message' => JText::_('OPENSHOP_DELETE_FAILD')));
        }
    }
    
    public function delete_reason_detail()
    {
        $app = JFactory::getApplication()->input;
        $id_reason_detail = $app->getInt('id_reason_detail');
        
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        
        $query->delete($db->quoteName('#__openshop_reasondetails'))
                ->where( array('id = ' . $id_reason_detail) );
        
        if($db->setQuery($query)->execute())
        {
            echo json_encode( array('status' => 'success' , 'message' => JText::_('OPWENSHOP_DELETE_SUCCESS')) );
        }
        else
        {
            echo json_encode( array('status' => 'success' , 'message' => JText::_('OPWENSHOP_DELETE_FAILD')) );
        }
    }

}
