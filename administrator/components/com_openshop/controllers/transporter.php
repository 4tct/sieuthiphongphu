<?php

defined('_JEXEC') or die;

class OpenShopControllerTransporter extends OpenShopController {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    function getFormTransporterAjax() {
        $data = JRequest::get('data');
        $arr_form = array();
        foreach ($data['data'] as $k => $v) {
            if ($v['name'] == 'cid[]') {
                $arr_form['cid'] = array($v['value']);
            } else {
                $arr_form[$v['name']] = $v['value'];
            }
        }
        $this->save($arr_form);
    }

    public function delete_ajax() {
        $res = array(
            'status' => 'error',
            'message' => JText::_('OPENSHOP_DELETE_FAILD')
        );

        $app = JFactory::getApplication()->input;
        $id = $app->getInt('id');
        $reason = $app->getString('reason');

        $cond = array('delete_status = 1', 'reason_delete_id = ' . $reason);
        if ($app->getString('desc') != '') {
            array_push($cond, 'reason_delete_desc = "' . $app->getString('desc') . '"');
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->update('#__openshop_transporters')
                ->set($cond)
                ->where('id = ' . $id);

        if ($db->setQuery($query)->execute()) {
            $res = array(
                'status' => 'success',
                'message' => JText::_('OPENSHOP_DELETE_SUCCESS')
            );
        }

        echo json_encode($res);
    }

}
