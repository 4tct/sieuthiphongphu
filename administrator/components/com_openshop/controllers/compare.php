<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerCompare extends OpenShopController {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    function continueShowCompareInventory() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $app = JFactory::getApplication()->input;
        $start = $app->getInt('start');
        $limit = $app->getInt('limit');
        try {
            $query->select('*,'
                            . '(SELECT sum(quantity_import) FROM #__openshop_kiemkhos as b '
                            . 'WHERE a.product_sku = b.product_sku '
                            . 'AND b.size_id IN '
                            . ' ('
                            . '     SELECT optionvalue_id FROM #__openshop_optionvaluedetails as c '
                            . '     INNER JOIN #__openshop_optiondetails as d ON c.option_id = d.option_id '
                            . '     WHERE UPPER(d.option_name) = "SIZE" AND UPPER(c.value) = UPPER(a.product_size) COLLATE utf8_unicode_ci '
                            . ' )'
                            . ') as quantity_scan'
                            . '')
                    ->from($db->quoteName('#__openshop_compare_inventories', 'a'));
            echo json_encode($db->setQuery($query, $start, $limit)->loadObjectList());
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function downloadCompare() {
        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('optionvalue_id,value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('option_id = 2');
        $s = $db->setQuery($query)->loadObjectList();

        $sizes_value_id = array();
        $sizes_id_value = array();
        foreach ($s as $key => $value) {
            $sizes_value_id[$value->value] = $value->optionvalue_id;
            $sizes_id_value[$value->optionvalue_id] = $value->value;
        }

        $phpExcel = new PHPExcel();
        $phpExcel->setActiveSheetIndex(0);

        //merge Cell
        $phpExcel->getActiveSheet()->mergeCells('A1:F2');
        $phpExcel->getActiveSheet()->setCellValue('A1', 'Kiểm kho');
        $phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);

        $phpExcel->getActiveSheet()->setCellValue('A3', 'STT');
        $phpExcel->getActiveSheet()->setCellValue('B3', 'Tên sản phẩm');
        $phpExcel->getActiveSheet()->setCellValue('C3', 'Mã SP');
        $phpExcel->getActiveSheet()->setCellValue('D3', 'Kích thước');
        $phpExcel->getActiveSheet()->setCellValue('E3', 'Số lượng quét');
        $phpExcel->getActiveSheet()->setCellValue('F3', 'Số lượng xuất');
        $phpExcel->getActiveSheet()->setCellValue('G1', 'Tống số lượng tăng kho');
        $phpExcel->getActiveSheet()->setCellValue('G2', 'Tống số lượng giảm kho');
        //style
        $phpExcel->getActiveSheet()->getStyle('A3:F3')->getFont()->setBold(true);
        $phpExcel->getActiveSheet()->getStyle('A3:F3')->getFont()->setSize(14);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        
        $phpExcel->getActiveSheet()->getStyle('A1:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $row = 4;
        $total_row = 1;
        $stt = 0;
        $tangkho = 0;
        $giamkho = 0;
        $proSize = array();
        $query->clear();
        $query->select('product_sku,sum(quantity_import) as quantity,size_id')
                ->from($db->quoteName('#__openshop_kiemkhos'))
                ->group('product_name');
        $row_scan = $db->setQuery($query)->loadObjectList();
        foreach ($row_scan as $key => $value) {
            ++$stt;
            $product_name = self::getProductnameCompare($value->product_sku);
            
            $phpExcel->getActiveSheet()->setCellValue('A' . $row, $stt);
            $phpExcel->getActiveSheet()->setCellValue('B' . $row, $product_name);
            $phpExcel->getActiveSheet()->setCellValue('C' . $row, $value->product_sku);
            $phpExcel->getActiveSheet()->setCellValue('D' . $row, $sizes_id_value[$value->size_id]);
            $phpExcel->getActiveSheet()->setCellValue('E' . $row, $value->quantity);
            
            $q = self::getQuantityFileImportInventory($value->product_sku,$sizes_id_value[$value->size_id]);
            $phpExcel->getActiveSheet()->setCellValue('F' . $row, $q == '' ? '0' : $q);
            if(empty($q) || (int)$value->quantity > (int)$q){
                ++$tangkho;
                $phpExcel->getActiveSheet()->setCellValue('G' . $row, 'Tăng kho: ' . ((int)$value->quantity - (int)$q));
                $phpExcel->getActiveSheet()->getStyle('A'. $row .':F' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
                $phpExcel->getActiveSheet()->getStyle('G' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('35FF46');
            }
            else if((int)$value->quantity < (int)$q)
            {
                ++$giamkho;
                $phpExcel->getActiveSheet()->setCellValue('G' . $row, 'Giảm kho: ' . ((int)$q - (int)$value->quantity));
                $phpExcel->getActiveSheet()->getStyle('A'. $row .':F' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
                $phpExcel->getActiveSheet()->getStyle('G' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('35FFE4');
            }
            ++$row;
            $proSize[$value->product_sku] = $sizes_id_value[$value->size_id];
        }
        
        //sản phẩm không có khi quét
        $stt = 0;
        $row += 1;
        $sql = 'select * from #__openshop_compare_inventories';
        $rows_import = $db->setQuery($sql)->loadObjectList();
        foreach ($rows_import as $key => $value) {
            $sql1 = 'select count(id) from #__openshop_kiemkhos where product_sku = "'. $value->product_sku .'" AND size_id = "'. $sizes_value_id[$value->product_size] .'"';
            $cI = $db->setQuery($sql1)->loadResult();
            if(!$cI){
                ++$stt;
                ++$giamkho;
                $phpExcel->getActiveSheet()->setCellValue('A' . $row, $stt);
                $phpExcel->getActiveSheet()->setCellValue('B' . $row, $value->product_name);
                $phpExcel->getActiveSheet()->setCellValue('C' . $row, $value->product_sku);
                $phpExcel->getActiveSheet()->setCellValue('D' . $row, $value->product_size);
                $phpExcel->getActiveSheet()->setCellValue('E' . $row, '0');
                $phpExcel->getActiveSheet()->setCellValue('F' . $row, $value->quantity);
                $phpExcel->getActiveSheet()->setCellValue('G' . $row, 'Giảm kho');
                $phpExcel->getActiveSheet()->getStyle('A'. $row .':F' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5AB517');
                $phpExcel->getActiveSheet()->getStyle('G' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('35FFE4');
                ++$row;
            }
        }
        
        $phpExcel->getActiveSheet()->getStyle('A1:F' . ($row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->getStyle('B4:B' . ($row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        //border
        $phpExcel->getActiveSheet()->getStyle('A1:F' . ($row))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        
        
        //số lượng tăng kho. Giảm kho
        $phpExcel->getActiveSheet()->setCellValue('H1', $tangkho);
        $phpExcel->getActiveSheet()->setCellValue('H2', $giamkho);
        $phpExcel->getActiveSheet()->getStyle('H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('35FF46');
        $phpExcel->getActiveSheet()->getStyle('H2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('35FFE4');
        
        //sheet name
        // đinh dạng ngày giờ
        $dstring = 'd-m-Y';
        $time = time();
        $phpExcel->getActiveSheet()->setTitle('Kiem_kho_' . date($dstring, $time));

        $write = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        header('Content-Disposition: attachment; filename="inventories.xls"');

        // Write file to the browser
        $write->save('php://output');                                           // only dơnload
    }
    
    public static function getQuantityFileImportInventory($name,$size){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('sum(quantity)')
                ->from($db->quoteName('#__openshop_compare_inventories'))
                ->where('product_sku = "' . $name .'"')
                ->where('product_size = "' . $size .'"')
                ->group('product_sku');
        return $db->setQuery($query)->loadResult();
    }


    public function downloadCompare_1() {
        require_once OPENSHOP_PATH_INCLUDES . 'PHPExcel' . DS . 'PHPExcel.php';

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('optionvalue_id,value')
                ->from($db->quoteName('#__openshop_optionvaluedetails'))
                ->where('option_id = 2');
        $s = $db->setQuery($query)->loadObjectList();

        $sizes = array();
        foreach ($s as $key => $value) {
            $sizes[$value->value] = $value->optionvalue_id;
        }

        $query->clear();
        $query->select('a.product_sku,sum(a.quantity_import) as quantity, '
                        . '( '
                        . '	SELECT value FROM #__openshop_optionvaluedetails as b '
                        . '     WHERE a.size_id = b.optionvalue_id '
                        . ') as size_name '
                        . '')
                ->from('#__openshop_kiemkhos as a')
                ->group('a.product_name')
                ->order('a.product_name');
        $rows_k = $db->setQuery($query)->loadObjectList();

        $phpExcel = new PHPExcel();
        $phpExcel->setActiveSheetIndex(0);

        //merge Cell
        $phpExcel->getActiveSheet()->mergeCells('A1:E2');
        $phpExcel->getActiveSheet()->setCellValue('A1', 'Kiểm kho');
        $phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);

        $phpExcel->getActiveSheet()->setCellValue('A3', 'STT');
        $phpExcel->getActiveSheet()->setCellValue('B3', 'Tên sản phẩm');
        $phpExcel->getActiveSheet()->setCellValue('C3', 'Mã SP');
        $phpExcel->getActiveSheet()->setCellValue('D3', 'Kích thước');
        $phpExcel->getActiveSheet()->setCellValue('E3', 'Tồn');
        //style
        $phpExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);
        $phpExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setSize(14);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        
        
        $row = 4;
        $total_row = 1;
        $empty_name = array();
        $impportFileNotPro = array();
        foreach ($rows_k as $key => $value) {
            $stt = ($key + 1);
            $product_name = self::getProductnameCompare($value->product_sku);
            if (empty($product_name)) {
                $empty_name[] = $row;
            }
            $phpExcel->getActiveSheet()->setCellValue('A' . $row, $stt);
            $phpExcel->getActiveSheet()->setCellValue('B' . $row, $product_name);
            $phpExcel->getActiveSheet()->setCellValue('C' . $row, $value->product_sku);
            $phpExcel->getActiveSheet()->setCellValue('D' . $row, $value->size_name);
            $phpExcel->getActiveSheet()->setCellValue('E' . $row, $value->quantity);
            
            $impportFileNotPro[$value->product_sku] = $value->size_name;
            
            $query->clear();
            $query->select('product_size')
                    ->from($db->quoteName('#__openshop_compare_inventories'))
                    ->where('product_sku = "' . $value->product_sku . '"')
                    ->where('product_size = "' . $value->size_name . '"');
            $cS = $db->setQuery($query)->loadObjectList();

            if (empty($cS)) {
                $query->clear();
                $query->select('product_size')
                        ->from($db->quoteName('#__openshop_compare_inventories'))
                        ->where('product_sku = "' . $value->product_sku . '"');
                $cS1 = $db->setQuery($query)->loadObjectList();

                $tCS = '';
                if (!empty($cS1)) {
                    foreach ($cS1 as $vCS) {
                        $query->clear();
                        $query->select('size_id')
                                ->from($db->quoteName('#__openshop_kiemkhos'))
                                ->where('product_sku = "' . $value->product_sku . '"')
                                ->where('size_id = "' . $sizes[$vCS->product_size] . '"');
                        $cSS = $db->setQuery($query)->loadObjectList();
                        if (empty($cSS)) {
                            $tCS .= $value->product_sku . '-' . $vCS->product_size;
                            $phpExcel->getActiveSheet()->setCellValue('G' . $row, 'Nhầm Size. Tăng kho');
                            $phpExcel->getActiveSheet()->getStyle('G' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5AB517');
                        } else {
                            $phpExcel->getActiveSheet()->setCellValue('G' . $row, 'Không có sản phẩm. Tăng kho');
                            $phpExcel->getActiveSheet()->getStyle('G' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5AB517');
                        }
                    }
                }
                
                $phpExcel->getActiveSheet()->setCellValue('F' . $row, $tCS);
                $phpExcel->getActiveSheet()->getStyle('C' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
            }

            ++$row;
            ++$total_row;
        }
        
        $query->clear();
        $query->select('*')
                ->from($db->quoteName('#__openshop_compare_inventories'));
        $PInventory = $db->setQuery($query)->loadObjectList();
        $stt = 0;
        $stgiam = $row+1;
        $edgiam = $row;
        foreach ($PInventory as $pi => $piname) {
            $query->clear();
            $s = 'SELECT * FROM op_openshop_kiemkhos WHERE product_sku = "'. $piname->product_sku .'" AND size_id = "'. $sizes[$piname->product_size] .'"';

            if(!($db->setQuery($s)->loadResult())){ //empty
                ++$edgiam;
                $row += 1;
                $phpExcel->getActiveSheet()->setCellValue('A' . $row, ++$stt);
                $phpExcel->getActiveSheet()->setCellValue('B' . $row, $piname->product_name);
                $phpExcel->getActiveSheet()->setCellValue('C' . $row, $piname->product_sku);
                $phpExcel->getActiveSheet()->setCellValue('D' . $row, $piname->product_size);
                $phpExcel->getActiveSheet()->setCellValue('E' . $row, $piname->quantity);
                $phpExcel->getActiveSheet()->setCellValue('G' . $row, 'Giảm kho.');
                $phpExcel->getActiveSheet()->getStyle('G' . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5AB517');
            }
        }
        //phần giảm
        $phpExcel->getActiveSheet()->getStyle('A'. $stgiam .':E' . $edgiam)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $phpExcel->getActiveSheet()->getStyle('A'. $stgiam .':E' . $edgiam)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('67C2E6');
        
        //phần tăng
        $phpExcel->getActiveSheet()->getStyle('A1:A' . ($total_row + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->getStyle('C1:E' . ($total_row + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //border
        $phpExcel->getActiveSheet()->getStyle('A1:E' . ($total_row + 2))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //sheet name
        // đinh dạng ngày giờ
        $dstring = 'd-m-Y';
        $time = time();
        $phpExcel->getActiveSheet()->setTitle('Kiem_kho_' . date($dstring, $time));

        foreach ($empty_name as $v) {
            $phpExcel->getActiveSheet()->setCellValue('G' . $v, 'Không có sản phẩm. Tăng kho');
            $phpExcel->getActiveSheet()->getStyle('C' . $v)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
            $phpExcel->getActiveSheet()->getStyle('G' . $v)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5AB517');
        }

        $write = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
//        $write->save(OPENSHOP_PATH_FILES . 'test.xlsx');                      // only save + not download
        // We'll be outputting an excel file
//        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="inventories.xls"');

        // Write file to the browser
        $write->save('php://output');                                           // only dơnload
    }
    
    public static function getProductnameCompare($product_sku) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('product_name')
                ->from($db->quoteName('#__openshop_compare_inventories'))
                ->where('product_sku = "' . $product_sku . '"');
        return $db->setQuery($query)->loadResult();
    }

    function productNotImport() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.product_sku, a.quantity_import,'
                        . '(SELECT VALUE FROM #__openshop_optionvaluedetails as z WHERE a.size_id = z.optionvalue_id) as size'
                        . '')
                ->from($db->quoteName('#__openshop_kiemkhos', 'a'))
                ->where('a.product_sku NOT IN('
                        . 'SELECT DISTINCT b.product_sku '
                        . 'FROM #__openshop_compare_inventories as b '
                        . 'INNER JOIN #__openshop_kiemkhos as c ON c.product_sku = b.product_sku '
                        . ')');
        echo json_encode($db->setQuery($query)->loadObjectList());
    }

    function compareProooo() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_kiemkhos'))
                ->where('product_sku = "' . JFactory::getApplication()->input->getString('code') . '"');
        echo json_encode($db->setQuery($query)->loadObjectList());
    }

}
