<?php

defined('_JEXEC') or die;

class OpenShopControllerMedia extends OpenShopController {

    function __construct($config = array()) {
        parent::__construct($config);
    }

    public function deleteRowMedia() {
        $app = JFactory::getApplication()->input;
        $id = $app->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->clear();
            $query->select('media_image')
                    ->from($db->quoteName('#__openshop_mediadetails'))
                    ->where('id = ' . $id);
            $fileName = $db->setQuery($query)->loadResult();

            $query->clear();
            $query->delete('#__openshop_mediadetails')
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                JFile::delete(OPENSHOP_PATH_IMG_MEDIA . $fileName);
            }
            echo 'success';
        } catch (Exception $ex) {
            echo 'error';
        }
    }

}
