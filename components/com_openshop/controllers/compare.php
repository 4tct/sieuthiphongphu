<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerCompare extends JControllerLegacy
{
	/**
	 * Constructor function
	 *
	 * @param array $config
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	/**
	 * 
	 * Function to add a product into the compare
	 */
	function add()
	{
		$productId = JRequest::getInt('product_id');
		$model = $this->getModel('Compare');
		$json = $model->add($productId);
		echo json_encode($json);
		exit();
	}
	
	/**
	 *
	 * Function to remove a product from the compare
	 */
	function remove()
	{
		$session = JFactory::getSession();
		$compare = $session->get('compare');
		$productId = JRequest::getInt('product_id');
		$key = array_search($productId, $compare);
		if ($key !== false)
		{
			unset($compare[$key]);
		}
		$session->set('compare', $compare);
		$session->set('success', JText::_('OPENSHOP_COMPARE_REMOVED_MESSAGE'));
		$json = array();
		$json['redirect'] = JRoute::_(OpenShopRoute::getViewRoute('compare'));
		echo json_encode($json);
		exit();
	}
}