<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerCurrency extends JControllerLegacy
{
	/**
	 * Constructor function
	 *
	 * @param array $config
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	function change()
	{
		$session = JFactory::getSession();
		$currencyCode = JRequest::getVar('currency_code', null, 'POST');
		if (!$session->get('currency_code') || $session->get('currency_code') != $currencyCode)
		{
			$session->set('currency_code', $currencyCode);
		}
		if (!JRequest::getVar('currency_code', '', 'COOKIE') || JRequest::getVar('currency_code', '', 'COOKIE') != $currencyCode)
		{
			setcookie('currency_code', $currencyCode, time() + 60 * 60 * 24 * 30);
			JRequest::setVar('currency_code', $currencyCode, 'COOKIE');
		}
		$return = base64_decode(JRequest::getVar('return'));
		$mainframe = JFactory::getApplication();
		$mainframe->redirect($return);
	}
}