<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.application');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerProfile extends JControllerLegacy {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    public function editprofile() {
        try {
            JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));
            $session = JFactory::getSession();
            $user = $session->get('user');
            $app = JFactory::getApplication()->input;
            $edit = $app->getString('varedit');
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);

            $t = '';
            switch ($edit) {
                case 'editname':
                    $name = $app->getString('nameProfile');
                    $data = new stdClass();
                    $data->customer_id = $user->id;
                    $data->fullname = $name;
                    
                    $dataU = new stdClass();
                    $dataU->id = $user->id;
                    $dataU->name = $name;
                    if ($db->updateObject('#__openshop_customers', $data, 'customer_id') && $db->updateObject('#__users', $dataU, 'id')) {
                        $t = OpenShopHelper::convertBase64Encode('success');
                    } else {
                        $t = OpenShopHelper::convertBase64Encode('error');
                    }
                    break;
                case 'editpassword':
                    $password = array();
                    $password['password'] = $app->getString('passwordProfile');
                    $juser = new JUser();
                    $juser->bind($password); //convert pass
                    
                    $data = new stdClass();
                    $data->id = $user->id;
                    $data->password = $password['password'];
                    if ($db->updateObject('#__users', $data, 'id')) {
                        $t = OpenShopHelper::convertBase64Encode('success');
                    } else {
                        $t = OpenShopHelper::convertBase64Encode('error');
                    }
                    break;
                case 'editphone':
                    $phone = $app->getString('phoneProfile');
                    $data = new stdClass();
                    $data->customer_id = $user->id;
                    $data->telephone = $phone;
                    if ($db->updateObject('#__openshop_customers', $data, 'customer_id')) {
                        $t = OpenShopHelper::convertBase64Encode('success');
                    } else {
                        $t = OpenShopHelper::convertBase64Encode('error');
                    }
                    break;
                case 'editemail':
                    $email = $app->getString('emailProfile');
                    $data = new stdClass();
                    $data->customer_id = $user->id;
                    $data->email = $email;
                    
                    $dataU = new stdClass();
                    $dataU->id = $user->id;
                    $dataU->email = $email;
                    if ($db->updateObject('#__openshop_customers', $data, 'customer_id') && $db->updateObject('#__users', $dataU, 'id')) {
                        $t = OpenShopHelper::convertBase64Encode('success');
                    } else {
                        $t = OpenShopHelper::convertBase64Encode('error');
                    }
                    break;
                case 'editaddress':
                    $address = $app->getString('addressProfile');
                    $zone = $app->getInt('townProfile');
                    $district = $app->getInt('districtProfile');
                    $data = new stdClass();
                    $data->customer_id = $user->id;
                    $data->address = $address;
                    $data->zone_id = $zone;
                    $data->district_id = $district;
                    if ($db->updateObject('#__openshop_addresses', $data, 'customer_id')) {
                        $t = OpenShopHelper::convertBase64Encode('success');
                    } else {
                        $t = OpenShopHelper::convertBase64Encode('error');
                    }
                    break;
                default :
                    break;
            }
            
            $session->set('resEdit', $t);
            $this->setRedirect('/thong-tin-ca-nhan.html');
        } catch (Exception $ex) {
            $this->setRedirect('/thong-tin-ca-nhan.html');
        }
    }

    function clearSession(){
        $session = JFactory::getSession();
        $session->clear('resEdit');
    }
    
    function checkPhone(){
        $app = JFactory::getApplication()->input;
        $phone = $app->getString('p');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_customers'))
                ->where('telephone = "'. $phone .'"');
        if($db->setQuery($query)->loadResult()){
            echo '1';
        }
        else{
            echo '0';
        }
    }
    
}
