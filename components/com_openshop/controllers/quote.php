<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopControllerQuote extends JControllerLegacy {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * 
     * Function to add a product to the quote
     */
    function add() {
        $quote = new OpenShopQuote();
        $json = array();
        $productId = JRequest::getInt('id');
        $quantity = JRequest::getInt('quantity') > 0 ? JRequest::getInt('quantity') : 1;
        if (JRequest::getVar('options')) {
            $options = array_filter(JRequest::getVar('options'));
        } else {
            $options = array();
        }
        //Validate options first
        $productOptions = OpenShopHelper::getProductOptions($productId, JFactory::getLanguage()->getTag());
        for ($i = 0; $n = count($productOptions), $i < $n; $i++) {
            $productOption = $productOptions[$i];
            if ($productOption->required && empty($options[$productOption->product_option_id])) {
                $json['error']['option'][$productOption->product_option_id] = $productOption->option_name . ' ' . JText::_('OPENSHOP_REQUIRED');
            }
        }
        if (!$json) {
            $product = OpenShopHelper::getProduct($productId, JFactory::getLanguage()->getTag());
            $quote->add($productId, $quantity, $options);
            $viewProductLink = JRoute::_(OpenShopRoute::getProductRoute($productId, OpenShopHelper::getProductCategory($productId)));
            $viewQuoteLink = JRoute::_(OpenShopRoute::getViewRoute('quote'));
            if (OpenShopHelper::getConfigValue('active_https')) {
                $viewCheckoutLink = JRoute::_(OpenShopRoute::getViewRoute('checkout'), true, 1);
            } else {
                $viewCheckoutLink = JRoute::_(OpenShopRoute::getViewRoute('checkout'));
            }
            $message = '<div>' . sprintf(JText::_('OPENSHOP_ADD_TO_QUOTE_SUCCESS_MESSAGE'), $viewProductLink, $product->product_name, $viewQuoteLink) . '</div>';
            $json['success']['message'] = $message;
        } else {
            $json['redirect'] = JRoute::_(OpenShopRoute::getProductRoute($productId, OpenShopHelper::getProductCategory($productId)));
        }
        echo json_encode($json);
        JFactory::getApplication()->close();
    }

    /**
     * 
     * Function to update quantity of a product in the quote
     */
    function update() {
        $session = JFactory::getSession();
        $session->set('success', JText::_('OPENSHOP_QUOTE_UPDATE_MESSAGE'));
        $key = JRequest::getVar('key');
        $quantity = JRequest::getInt('quantity');
        $quote = new OpenShopQuote();
        $quote->update($key, $quantity);
    }

    /**
     *
     * Function to update quantity of all products in the quote
     */
    function updates() {
        $session = JFactory::getSession();
        $session->set('success', JText::_('OPENSHOP_QUOTE_UPDATE_MESSAGE'));
        $key = JRequest::getVar('key');
        $quantity = JRequest::getVar('quantity');
        $quote = new OpenShopQuote();
        $quote->updates($key, $quantity);
    }

    /**
     * 
     * Function to remove a product from the quote
     */
    function remove() {
        $session = JFactory::getSession();
        $key = JRequest::getVar('key');
        $quote = new OpenShopQuote();
        $quote->remove($key);
        {
            $session->set('success', JText::_('OPENSHOP_QUOTE_REMOVED_MESSAGE'));
        }
    }

    /**
     * Function to process quote
     */
    function processQuote() {
        $post = JRequest::get('post', JREQUEST_ALLOWHTML);
        $model = $this->getModel('Quote');
        $json = $model->processQuote($post);
        echo json_encode($json);
        exit();
    }

    /*
     * 
     */

    function getUrl() {

        $app = JFactory::getApplication()->input;
        $catID = $app->getInt('catID');

        $link = OpenShopRoute::getCategoryRoute($catID);

        $res = array();

        $res['url'] = JRoute::_($link);

        echo json_encode($res);
    }

    /*
     * 
     */

    function getShowProductDetail() {
        $app = JFactory::getApplication()->input;
        $productId = OpenShopHelper::convertBase64Decode($app->getString('i'));
        $token = $app->getString('token');
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);

        $query->select('a.*,b.product_name,b.product_desc')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id')
                ->where('a.id = ' . $productId);
        $product = $db->setQuery($query)->loadObject();

        $additionalImageSizeFunction = OpenShopHelper::getConfigValue('additional_image_size_function', 'resizeImage');
        $thumbImageSizeFunction = OpenShopHelper::getConfigValue('thumb_image_size_function', 'resizeImage');
        $popupImageSizeFunction = OpenShopHelper::getConfigValue('popup_image_size_function', 'resizeImage');
        $relatedImageSizeFunction = OpenShopHelper::getConfigValue('related_image_size_function', 'resizeImage');
        // Main image resize
        if ($product->product_image && JFile::exists(JPATH_ROOT . '/images/com_openshop/products/' . $product->product_image)) {
            $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array($product->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
            $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array($product->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
            $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array($product->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
        } else {
            $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
            $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
            $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
        }

        $product->small_thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $smallThumbImage;
        $product->thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $thumbImage;
        $product->thumb_image = JUri::base(true) . '/images/com_openshop/products/' . $product->product_image;
        $product->popup_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $popupImage;


        $productImages = OpenShopHelper::getProductImages($productId);
        // Additional images resize
        for ($i = 0; $n = count($productImages), $i < $n; $i++) {
            if ($productImages[$i]->image && JFile::exists(JPATH_ROOT . '/images/com_openshop/products/' . $productImages[$i]->image)) {
                $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array($productImages[$i]->image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
                $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array($productImages[$i]->image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
                $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array($productImages[$i]->image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
            } else {
                $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
                $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
                $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
            }
            $productImages[$i]->small_thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $smallThumbImage;
            $productImages[$i]->thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $thumbImage;
            $productImages[$i]->popup_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $popupImage;
        }


        $productRelations = OpenShopHelper::getProductRelations($productId, JFactory::getLanguage()->getTag());
        // Related products images resize
        for ($i = 0; $n = count($productRelations), $i < $n; $i++) {
            if ($productRelations[$i]->product_image && JFile::exists(JPATH_ROOT . '/images/com_openshop/products/' . $productRelations[$i]->product_image)) {
                $thumbImage = call_user_func_array(array('OpenShopHelper', $relatedImageSizeFunction), array($productRelations[$i]->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_related_width'), OpenShopHelper::getConfigValue('image_related_height')));
            } else {
                $thumbImage = call_user_func_array(array('OpenShopHelper', $relatedImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_related_width'), OpenShopHelper::getConfigValue('image_related_height')));
            }
            $productRelations[$i]->thumb_image = JUri::base(true) . '/images/com_openshop/products/' . $productRelations[$i]->product_image;
        }

        // Update hits for product
        OpenShopHelper::updateHits($productId, 'products');

        $this->productOptions = OpenShopHelper::getProductOptions($productId, JFactory::getLanguage()->getTag());
        $this->item = $product;
        $this->productImages = $productImages;
        $this->discountPrices = array();
        $this->productRelations = $productRelations;
        $this->token = $token;
        $this->urlHistory = $app->getString('urlHistory');
        $this->titleJS = $app->getString('title');
        $this->productColorDifferent = OpenShopHelper::getProductColorDifferent( $product->id );

        require_once OPENSHOP_COMPONENTS . 'common/product/showModalProduct.php';
    }

}
