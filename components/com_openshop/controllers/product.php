<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.application');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerProduct extends JControllerLegacy {

    function addCartProduct() {
        $app = JFactory::getApplication()->input;
        $cond = $app->getString('cond');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $session = JFactory::getSession();
        $user_session = $session->get('user');
        $id = $app->getInt('id');
        try {
            $sqs = explode(',', $app->getString('SizeQuantity'));

            foreach ($sqs as $sq) {
                $a_sq = explode('-', $sq);

                $query->clear();
                $query->select('*')
                        ->from($db->quoteName('#__openshop_cartproducts'))
                        ->where('product_id = ' . $id);

                if ($cond != 'notsize') {   //size
                    $query->where('size_id = ' . $a_sq[0]);
                } else {                    //not size
                    $query->where('color_id = ' . $a_sq[0]);
                }

                $row = $db->setQuery($query)->loadObject();

                $check = 1;

                if (count($row)) {
                    $data = new stdClass();
                    $quantity = (int) $row->quantity + (int) $a_sq[1];
                    $data->id = $row->id;
                    $data->quantity = $quantity;
                    $data->user_id = $user_session->id;

                    if ($db->updateObject('#__openshop_cartproducts', $data, 'id')) {
                        $res = 1;
                    } else {
                        $res = 0;
                    }
                    $check = 0;
                }

                if ($check) {
                    $data = new stdClass();
                    $data->product_id = $id;
                    $data->quantity = $a_sq[1];
                    $data->user_id = $user_session->id;
                    $data->ip = $_SERVER['REMOTE_ADDR'];

                    if ($cond != 'notsize') {
                        $data->size_id = $a_sq[0];
                        $data->color_id = 0;
                    } else {
                        $data->size_id = 0;
                        $data->color_id = $a_sq[0];
                    }


                    if ($db->insertObject('#__openshop_cartproducts', $data)) {
                        $res = 1;
                    } else {
                        $res = 0;
                    }
                }
            }

            if ($res) {
                echo 'success';
            } else {
                echo 'error';
            }
        } catch (Exception $ex) {
//            echo 'error';
            echo $ex;
        }
    }

    function removeCartProduct() {
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $id = OpenShopHelper::convertBase64Decode($app->getString('id'));
        try {
            $query->delete($db->quoteName('#__openshop_cartproducts'))
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                echo 'success';
            } else {
                echo 'error';
            }
        } catch (Exception $ex) {
            echo 'error';
//            echo $ex;
        }
    }

    /*
     * get size of product in cartproducts table
     */

    function getSizeCP() {
        try {
            $app = JFactory::getApplication()->input;
            $id = OpenShopHelper::convertBase64Decode($app->getString('id'));
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('b.optionvalue_id,b.value')
                    ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . ' ON a.option_value_id = b.optionvalue_id')
                    ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . ' ON c.option_id = b.option_id')
                    ->where('UPPER(c.option_name) = "SIZE"')
                    ->where('a.product_id = ' . $id);
            $sizes = $db->setQuery($query)->loadObjectList();
            $res_size = array();
            foreach ($sizes as $size) {
                $res_size[] = array(
                    'optionvalue_id' => OpenShopHelper::convertBase64Encode($size->optionvalue_id),
                    'value' => $size->value
                );
            }
            echo json_encode($res_size);
        } catch (Exception $ex) {
            echo json_encode(array('status' => 'error'));
        }
    }

    /*
     * add cartproducts table
     */

    function addCP() {
        try {
            $user = JFactory::getSession()->get('user');
            $app = JFactory::getApplication()->input;
            $size = OpenShopHelper::convertBase64Decode($app->getString('size'));
            $id_p = OpenShopHelper::convertBase64Decode($app->getString('id'));
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);

            $query->select('*')
                    ->from($db->quoteName('#__openshop_cartproducts'))
                    ->where('product_id = ' . $id_p)
                    ->where('size_id = ' . $size);
            $cp = $db->setQuery($query)->loadObject();

            if (count($cp)) {         //có dữ liệu
                $data = new stdClass();
                $data->id = $cp->id;
                $data->quantity = $cp->quantity + 1;

                if ($db->updateObject('#__openshop_cartproducts', $data, 'id')) {
                    echo json_encode(array('status' => 'success', 'message' => 'Thêm vào giỏ hàng thành công'));
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ'));
                }
            } else {           //không có dữ liệu
                $data = new stdClass();
                $data->product_id = $id_p;
                $data->size_id = $size;
                $data->color_id = '0';
                $data->quantity = '1';
                $data->user_id = $user->id;
                $data->ip = $_SERVER['REMOTE_ADDR'];

                if ($db->insertObject('#__openshop_cartproducts', $data)) {
                    echo json_encode(array('status' => 'success', 'message' => 'Thêm vào giỏ hàng thành công'));
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ'));
                }
            }
        } catch (Exception $ex) {
            echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ'));
        }
    }

    function wishProduct() {
        try {
            $user = JFactory::getSession()->get('user');
            $app = JFactory::getApplication()->input;
            $id = OpenShopHelper::convertBase64Decode($app->getString('id'));
            $db = JFactory::getDbo();

            $d = new stdClass();
            $d->customer_id = $user->id;
            $d->product_id = $id;
            if ($db->insertObject('#__openshop_wishlists', $d)) {
                echo json_encode(array('status' => 'success', 'message' => 'Thêm sản phẩm yêu thích thành công'));
            } else {
                echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ'));
            }
        } catch (Exception $ex) {
            echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ'));
        }
    }

    function nonwishProduct() {
        try {
            $user = JFactory::getSession()->get('user');
            $app = JFactory::getApplication()->input;
            $id = OpenShopHelper::convertBase64Decode($app->getString('id'));
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->delete($db->quoteName('#__openshop_wishlists'))
                    ->where('customer_id = ' . $user->id)
                    ->where('product_id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                echo json_encode(array('status' => 'success', 'message' => 'Bỏ sản phẩm yêu thích thành công'));
            } else {
                echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ'));
            }
        } catch (Exception $ex) {
            echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ'));
        }
    }

    /*
     * SEARCH 
     */

    function searchQuick() {
        $app = JFactory::getApplication()->input;
        $value = $app->getString('v');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id')
                ->where('b.product_name LIKE "%' . $value . '%"');
        $rows = $db->setQuery($query, 0, 5)->loadObjectList();

        $h = '<ul>';
        if (count($rows)) {
            foreach ($rows as $row) {
                $link = JRoute::_(OpenShopRoute::getProductRoute($row->id, OpenShopHelper::getProductCategory($row->id)));
                $h .= '<li onclick="location.href=\'' . $link . '\'">';
                $h .= ' <img src="' . OPENSHOP_PATH_IMG_PRODUCT_HTTP . $row->product_image . '" width="70px"/>';
                $h .= ' <div class="infoProSearch">';
                $h .= ' <div class="name_pro">' . $row->product_name . '</div>';
                $h .= ' <div class="all_size_pro">Size: <b>M</b> - <b>L</b></div>';
                $h .= ' <div class="price_pro">';
                $h .= '     ' . number_format($row->product_price, 0, ',', '.') . '<sup>đ</sup>';
                $h .= ' </div>';

                if ($row->product_price_r) {
                    $h .= ' <div class="price_pro_not">';
                    $h .= '     ' . number_format($row->product_price_r, 0, ',', '.') . '<sup>đ</sup>';
                    $h .= ' </div>';
                }
                $h .= '</div>';
                $h .= '</li>';
            }
        } else {
            echo '<li style="padding: 10px;list-style:none;">Không có sản phẩm</li>';
        }
        $h .= '</ul>';
        echo $h;
    }

    public function getProductInHomeSiteT2() {
        try {
            $app = JFactory::getApplication()->input;
            $id = OpenShopHelper::convertBase64Decode($app->getString('i'));
            $type = $app->getString('t');
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('a.*,b.product_name')
                    ->from($db->quoteName('#__openshop_products', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                    ->join('INNER', $db->quoteName('#__openshop_productcategories', 'c') . 'ON a.id = c.product_id')
                    ->where('a.delete_status = 0')
                    ->where('a.published = 1')
                    ->where('c.category_id = ' . $id);
            switch ($type) {
                case '2':       //sản phẩm hot
                    $query->where('a.product_hot = 1');
                    break;
                case '3': //sản phẩm khuyến mãi
                    $query->where('a.product_hot = 1');
                    break;
                default: //sản phẩm mới
                    $query->where('a.product_new = 1');
                    break;
            }

            $rows = $db->setQuery($query, 0, 8)->loadObjectList();

            $html = '';
            if (count($rows)) {
                foreach ($rows as $ct2) {
                    $link = JRoute::_(OpenShopRoute::getProductRoute($ct2->id, OpenShopHelper::getProductCategory($ct2->id)));

                    $html .= '<div class="col-sm-4 col-md-3 b_t_2 b_t_t_2">';
                    $html .= '<div class="infoImgT2" onclick="location.href = \'' . $link . '\'">';
                    $html .= '<img src="' . OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ct2->product_image . '" class="img-responsive"/>';
                    $html .= '<div class="infoChildProT2">';
                    $html .= '<div class="pricePro1">';
                    $html .= number_format($ct2->product_price, 0, ',', '.');
                    $html .= '<sup>đ</sup>';
                    if ($ct2->product_price_r != 0) {
                        $html .= '<span class="pricenotbuy">';
                        $html .= number_format($ct2->product_price_r, 0, ',', '.');
                        $html .= '<sup>đ</sup>';
                        $html .= '</span>';
                    }
                    $html .= '</div>';
                    $html .= '<div>';
                    $html .= 'Kích thước: ';
                    $sizes = OpenShopHelper::getProductOptionValueDetails($ct2->id, 'size');
                    foreach ($sizes as $k => $s) {
                        $html .= $s->value;
                        if ($k != count($sizes) - 1) {
                            $html .= ' - ';
                        }
                    }
                    $html .= '</div>';
                    $html .= '<!--<div>Màu sắc: Xanh</div>-->';
                    $html .= '</div>';
                    $html .= '</div>';

                    $html .= '<div class="namePro">';
                    $html .= '<a href="<?php echo $link ?>">';
                    $html .= $ct2->product_name;
                    $html .= '</a>';
                    $html .= '</div>';
                    $html .= '<div class="codePro">';
                    $html .= 'Mã: <strong>' . $ct2->product_sku . '</strong>';
                    $html .= '</div>';
                    $html .= '<div class="pricePro">';
                    $html .= number_format($ct2->product_price, 0, ',', '.');
                    $html .= '<sup>đ</sup>';
                    if ($ct2->product_price_r != 0) {
                        $html .= '<span class="pricenotbuy">';
                        $html .= number_format($ct2->product_price_r, 0, ',', '.');
                        $html .= '<sup>đ</sup>';
                        $html .= '</span>';
                    }
                    $html .= '</div>';
                    $html .= '<div class="btn-buy">';
                    $html .= '<a href="' . $link . '">';
                    $html .= 'Mua ngay';
                    $html .= '</a>';
                    $html .= '</div>';
                    $html .= '</div>';
                }
            } else {
                $html .= '<div class="ct2_not_pro">Hiện không có sản phẩm</div>';
            }
            $res = array();
            $res['html'] = $html;

            echo json_encode($res);
        } catch (Exception $ex) {
            echo json_encode(array('status' => 'error', 'message' => 'Trang hiện quá tải. Vui lòng refresh lại'));
        }
    }

    function loadProductShowHomeSite() {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $app = JFactory::getApplication()->input;
        $id = OpenShopHelper::convertBase64Decode($app->getString('i'));
        $style = $app->getString('style');
        $res = array();
        $html = '';
        try {
            $query->select('DISTINCT b.*,c.product_name')
                    ->from($db->quoteName('#__openshop_productcategories', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_products', 'b') . ' ON b.id = a.product_id')
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . ' ON b.id = c.product_id')
                    ->where('published = 1')
                    ->where('delete_status = 0')
                    ->order('b.id DESC, b.product_hot DESC');

            $idArrCat = OpenShopHelper::getAllChildCategories($id);
            if(!empty($idArrCat)){
                $query->where('a.category_id IN (' . implode(',', $idArrCat) . ')');
            }
            else{
                $query->where('a.category_id = ' . $id);
            }
            
            $rows = $db->setQuery($query, 0, 10)->loadObjectList();
            if (count($rows)) {
                foreach ($rows as $pm) {
                    $link = JRoute::_(OpenShopRoute::getProductRoute($pm->id, OpenShopHelper::getProductCategory($pm->id)));

                    //hình ảnh + tên sản phẩm
                    $html .= '<div class="swiper-slide" style="' . $style . '">';
                    $html .= '<div class="marginImg imgProShow3">';
                    $html .= '<a onclick="getTestModalDetail(\'' . OpenShopHelper::convertBase64Encode($pm->id) . '\', \'' . $link . '\')">';
                    $html .= '<img src="' . OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pm->product_image . '" class="img-responsive swiper-lazy" alt="' . $pm->product_name . '" title="' . $pm->product_name . '"/>';
                    $html .= '</a>';
                    $html .= '<div class="ProSKU">';
                    $html .= $pm->product_sku;
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="info_pro_t4">';
                    $html .= '<div class="name_pro_t4">';
                    $html .= '<a onclick="getTestModalDetail(\'' . OpenShopHelper::convertBase64Encode($pm->id) . '\', \'' . $link . '\')">';
                    $html .= '<h5 title="' . $pm->product_name . '">';
                    $html .= $pm->product_name;
                    $html .= '</h5>';
                    $html .= '</a>';
                    $html .= '</div>';
                    //giá
                    $html .= '<div class="info_price_pro_t4">';
                    if ($pm->product_price_r > 0) {
                        $html .= '<div class="not_price_pro_t4">';

                        $html .= number_format((int) $pm->product_price_r, 0, ',', '.');

                        $html .= '<sup>đ</sup>';
                        $html .= '<sub>' . OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) .'</sub>';
                        $html .= '</div>';
                    }
                    $html .= '<div class="price_pro_t4">';
                    $html .= number_format((int) $pm->product_price, 0, ',', '.');
                    $html .= '<sup>đ</sup>';
                    $html .= '<sub>' . OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) .'</sub>';
                    $html .= '</div>';
                    $html .= '</div>';
                    //Các nút
                    $html .= '<div class="act_pro">';
                    $html .= '<div class="col-md-12" style="padding:0">';
                    $html .= '<div class="col-md-6 cart_t4" onclick="getTestModalDetail(\'' . OpenShopHelper::convertBase64Encode($pm->id) . '\', \'' . $link . '\')">';
                    $html .= '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
                    $html .= '<span style="font-size: 14px;font-weight: bold;">Đặt mua</span>';
                    $html .= '</div>';
                    $html .= '<div class="col-md-3 buyer_t4" data-placement="top" data-toggle="tooltip" title="Lượt mua">';
                    $html .= '  <div class="showbuyer">';
                    $html .= '      ' . $pm->buyer_virtual;
                    $html .= '  </div>';
                    $html .= '  <div class="showView">';
                    $html .= '      <i class="fa fa-users" aria-hidden="true"></i>';
                    $html .= '  </div>';
                    $html .= '</div>';
                    $html .= '<div class="col-md-3 view_t4" data-placement="top" data-toggle="tooltip" title="Lượt xem">';
                    $html .= '  <div class="showbuyer">';
                    $html .= '      ' . $pm->hits;
                    $html .= '  </div>';
                    $html .= '  <div class="showView">';
                    $html .= '      <i class="fa fa-eye" aria-hidden="true"></i>';
                    $html .= '  </div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';

                    if ($pm->product_price_r > 0) {
                        $percent = 100 - (int) (((int) $pm->product_price * 100) / $pm->product_price_r);
                        $html .= '<div class="percentSale rightPercent">';
                        $html .= $percent . '%';
                        $html .= '</div>';
                    }

                    $html .= '</div>';
                }

                $res['status'] = 'success';
                $res['message'] = 'Đã tải sản phẩm';
            } else {
                $res['status'] = 'warning';
                $res['message'] = 'Hiện danh mục đang cập nhật sản phẩm';
            }
            $res['html'] = $html;

            echo json_encode($res);
        } catch (Exception $ex) {
            $res = array();
            $res['status'] = 'error';
            $res['message'] = 'Hệ thống đang quá tải. Vui long tải lại trang';
            echo json_encode($res);
        }
    }

    function setBuyProduct() {
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {

            $id = OpenShopHelper::convertBase64Decode($app->getString('i'));

            $query->select('a.*,b.product_name')
                    ->from($db->quoteName('#__openshop_products', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id')
                    ->where('a.id = ' . $id);

            $product = $db->setQuery($query)->loadObject();

//            print_r($product);

            $res = array();
            $res['data'] = $product;
            $res['status'] = 'success';

            echo json_encode($res);
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    function getProductPagination() {
        $res = array();
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $session = JFactory::getSession();
        $limit = (int) $session->get('limitProList');
        $idcat = OpenShopHelper::convertBase64Decode($app->getString('i'));
        $limitLoadmore = (int) OpenShopHelper::getConfigValue('catalog_limit_load_more');
        $limitShowProduct = (int) OpenShopHelper::getConfigValue('catalog_limit');
        $limitstart = $limitShowProduct + ($limit * $limitLoadmore);
        try {
            //set ID category
            $categoryIds = array($idcat);
            if (OpenShopHelper::getConfigValue('show_products_in_all_levels')) {
                $categoryIds = array_merge(array(intval($idcat)), OpenShopHelper::getAllChildCategories($idcat));
            }

            $query->select('a.*,b.product_name')
                    ->from($db->quoteName('#__openshop_products', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                    ->where('a.delete_status = 0')
                    ->where('a.published = 1');

            if ($idcat != 'all') {
                $subQuery = $db->getQuery(true);
                $subQuery->select('pc.product_id FROM #__openshop_productcategories AS pc WHERE pc.category_id IN (' . implode(',', $categoryIds) . ')');
                $query->where('a.id IN (' . (string) $subQuery . ')');
            }

            $query->order('product_hot DESC, product_new DESC, created_date DESC');
            $products = $db->setQuery($query, $limitstart, $limitLoadmore)->loadObjectList();

            //mảng chưa các id sản phẩm dùng để lấy size
            $idProductArr = array();
            foreach ($products as $pro) {
                $idProductArr[] = $pro->id;
                $pro->link_image = OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pro->product_image;
                $pro->url = JRoute::_(OpenShopRoute::getProductRoute($pro->id, OpenShopHelper::getProductCategory($pro->id)));
                $pro->id_encode = OpenShopHelper::convertBase64Encode($pro->id);
                $pro->product_price = number_format($pro->product_price, 0, ',', '.');
                $pro->product_price_r = number_format($pro->product_price_r, 0, ',', '.');
                $pro->sku = $pro->product_sku;
                $pro->donvi = OpenShopHelper::getDonViProduct( $pro->donvi );
                $pro->id = md5($pro->id);
            }

            $querySize = $db->getQuery(true);
            $querySize->select('a.product_id,b.optionvalue_id,b.value')
                    ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . ' ON a.option_value_id = b.optionvalue_id ')
                    ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . ' ON c.option_id = b.option_id ')
                    ->where('a.product_id IN (' . implode(',', $idProductArr) . ') ')
                    ->where('UPPER(c.option_name) = "SIZE"')
                    ->group('a.id');
            $sizes = $db->setQuery($querySize)->loadObjectList();

            $sizeArr = array();
            $idPro_temp = '';
            $sizeText = '';
            foreach ($sizes as $key => $size) {
                if ($key == 0) {
                    $idPro_temp = $size->product_id;
                    $sizeText = $size->value;
                } else {
                    if ($idPro_temp != $size->product_id) {
                        $sizeArr[md5($idPro_temp)] = $sizeText;

                        $idPro_temp = $size->product_id;
                        $sizeText = $size->value;
                    } else {
                        $sizeText .= ' - ' . $size->value;
                    }
                }
                $sizeArr[md5($idPro_temp)] = $sizeText;
            }

            $res['status'] = 'success';
            $res['products'] = $products;
            $res['limit'] = $limit;
            $res['size'] = $sizeArr;
            //set session pagination +1
            $session->set('limitProList', ++$limit);


            echo json_encode($res);
        } catch (Exception $ex) {
            $res['status'] = 'error';
            $res['message'] = 'Hệ thống đang quá tải. Vui lòng tải lại trang';
            echo json_encode($res);
        }
    }

    function updateQuantityProduct() {
        $res = array();
        $app = JFactory::getApplication()->input;
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $id = intval(OpenShopHelper::convertBase64Decode($app->getString('i')));
        $quan = intval($app->getInt('q')) == 0 ? 1 : intval($app->getInt('q'));
        try {
            $query->update($db->quoteName('#__openshop_cartproducts'))
                    ->set('quantity = ' . $quan)
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                $res['status'] = 'success';
                $res['message'] = 'Cập nhật số lượng thành công';
            } else {
                $res['status'] = 'warning';
                $res['message'] = 'Cập nhật số lượng thất bại';
            }
            echo json_encode($res);
        } catch (Exception $ex) {
            $res['status'] = 'error';
            $res['message'] = 'Hệ thống hiện quá tải. Vui lòng tải lại trang';
            echo json_encode($res);
        }
    }

}
