<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.application');

/**
 * OpenShop controller
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopControllerCart extends JControllerLegacy {

    /**
     * Constructor function
     *
     * @param array $config
     */
    function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * 
     * Function to add a product to the cart
     */
    function add() {
        $cart = new OpenShopCart();
        $json = array();
        $productId = JRequest::getInt('id');
        $quantity = JRequest::getInt('quantity') > 0 ? JRequest::getInt('quantity') : 1;
        if (JRequest::getVar('options')) {
            $options = array_filter(JRequest::getVar('options'));
        } else {
            $options = array();
        }
        //Validate options first
        $productOptions = OpenShopHelper::getProductOptions($productId, JFactory::getLanguage()->getTag());
        for ($i = 0; $n = count($productOptions), $i < $n; $i++) {
            $productOption = $productOptions[$i];
            if ($productOption->required && empty($options[$productOption->product_option_id])) {
                $json['error']['option'][$productOption->product_option_id] = $productOption->option_name . ' ' . JText::_('OPENSHOP_REQUIRED');
            }
        }
        if (!$json) {
            $product = OpenShopHelper::getProduct($productId, JFactory::getLanguage()->getTag());
            $cart->add($productId, $quantity, $options);
            $viewProductLink = JRoute::_(OpenShopRoute::getProductRoute($productId, OpenShopHelper::getProductCategory($productId)));
            $viewCartLink = JRoute::_(OpenShopRoute::getViewRoute('cart'));
            if (OpenShopHelper::getConfigValue('active_https')) {
                $viewCheckoutLink = JRoute::_(OpenShopRoute::getViewRoute('checkout'), true, 1);
            } else {
                $viewCheckoutLink = JRoute::_(OpenShopRoute::getViewRoute('checkout'));
            }
            $json['success'] = true;
            //Clear shipping and payment methods
            $session = JFactory::getSession();
            $session->clear('shipping_method');
            $session->clear('shipping_methods');
            $session->clear('payment_method');
        } else {
            $json['redirect'] = JRoute::_(OpenShopRoute::getProductRoute($productId, OpenShopHelper::getProductCategory($productId)));
        }
        echo json_encode($json);
        JFactory::getApplication()->close();
    }

    /**
     *
     * Function to re-order
     */
    function reOrder() {
        $mainframe = JFactory::getApplication();
        $user = JFactory::getUser();
        if (!$user->id) {
            $mainframe->enqueueMessage(JText::_('OPENSHOP_RE_ORDER_LOGIN_PROMPT'), 'Notice');
            $mainframe->redirect('index.php?option=com_users&view=login');
        } else {
            $orderId = JRequest::getInt('order_id');
            // Validate order
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_orders')
                    ->where('id = ' . intval($orderId))
                    ->where('customer_id = ' . intval($user->id));
            $db->setQuery($query);
            if (!$db->loadResult()) {
                $mainframe->enqueueMessage(JText::_('OPENSHOP_RE_ORDER_NOT_ALLOW'), 'Error');
                $mainframe->redirect(OpenShopRoute::getViewRoute('customer') . '&layout=orders');
            } else {
                $cart = new OpenShopCart();
                // Clear cart first
                $cart->clear();
                // Then, clear shipping and payment methods
                $session = JFactory::getSession();
                $session->clear('shipping_method');
                $session->clear('shipping_methods');
                $session->clear('payment_method');
                // Re-add products and corresponding options to the cart
                $orderProducts = OpenShopHelper::getOrderProducts($orderId);
                if (!count($orderProducts)) {
                    $mainframe->enqueueMessage(JText::_('OPENSHOP_RE_ORDER_NOT_ALLOW'), 'Error');
                    $mainframe->redirect(OpenShopRoute::getViewRoute('customer') . '&layout=orders');
                } else {
                    for ($i = 0; $n = count($orderProducts), $i < $n; $i++) {
                        $orderProduct = $orderProducts[$i];
                        $options = array();
                        for ($j = 0; $m = count($orderProduct->orderOptions), $j < $m; $j++) {
                            $option = $orderProduct->orderOptions[$j];
                            $optionType = $option->option_type;
                            if ($optionType == 'Select' || $optionType == 'Radio') {
                                $options[$option->product_option_id] = $option->product_option_value_id;
                            } elseif ($optionType == 'Checkbox') {
                                if (is_array($options[$option->product_option_id])) {
                                    $options[$option->product_option_id][] = $option->product_option_value_id;
                                } else {
                                    $options[$option->product_option_id] = array();
                                    $options[$option->product_option_id][] = $option->product_option_value_id;
                                }
                            } else {
                                $options[$option->product_option_id] = $option->option_value;
                            }
                        }
                        $cart->add($orderProduct->product_id, $orderProduct->quantity, $options);
                    }
                    $mainframe->redirect(JRoute::_(OpenShopRoute::getViewRoute('cart')));
                }
            }
        }
    }

    /**
     * 
     * Function to update quantity of a product in the cart
     */
    function update() {
        $session = JFactory::getSession();
        $session->set('success', JText::_('OPENSHOP_CART_UPDATE_MESSAGE'));
        $key = JRequest::getVar('key');
        $quantity = JRequest::getInt('quantity');
        $cart = new OpenShopCart();
        $cart->update($key, $quantity);
        //Clear shipping and payment methods
        $session->clear('shipping_method');
        $session->clear('shipping_methods');
        $session->clear('payment_method');
    }

    /**
     * 
     * Function to update quantities of all products in the cart
     */
    function updates() {
        $session = JFactory::getSession();
        $session->set('success', JText::_('OPENSHOP_CART_UPDATE_MESSAGE'));
        $key = JRequest::getVar('key');
        $quantity = JRequest::getVar('quantity');
        $cart = new OpenShopCart();
        $cart->updates($key, $quantity);
        //Clear shipping and payment methods
        $session->clear('shipping_method');
        $session->clear('shipping_methods');
        $session->clear('payment_method');
    }

    /**
     * 
     * Function to remove a product from the cart
     */
    function remove() {
        $session = JFactory::getSession();
        $key = JRequest::getVar('key');
        $cart = new OpenShopCart();
        $cart->remove($key);
        //Clear shipping and payment methods
        $session = JFactory::getSession();
        $session->clear('shipping_method');
        $session->clear('shipping_methods');
        $session->clear('payment_method');
        if (JRequest::getInt('redirect')) {
            $session->set('success', JText::_('OPENSHOP_CART_REMOVED_MESSAGE'));
        }
    }

    /**
     * 
     * Function to apply coupon to the cart
     */
    function applyCoupon() {
        $session = JFactory::getSession();
        $couponCode = JRequest::getVar('coupon_code');
        $coupon = new OpenShopCoupon();
        $couponData = $coupon->getCouponData($couponCode);
        if (!count($couponData)) {
            $couponInfo = $coupon->getCouponInfo($couponCode);
            $user = JFactory::getUser();
            if (is_object($couponInfo) && $couponInfo->coupon_per_customer && !$user->get('id')) {
                $session->set('warning', JText::_('OPENSHOP_COUPON_IS_ONLY_FOR_REGISTERED_USER'));
            } else {
                $session->set('warning', JText::_('OPENSHOP_COUPON_APPLY_ERROR'));
            }
        } else {
            $session->set('coupon_code', $couponCode);
            $session->set('success', JText::_('OPENSHOP_COUPON_APPLY_SUCCESS'));
        }
    }

    /**
     *
     * Function to apply voucher to the cart
     */
    function applyVoucher() {
        $session = JFactory::getSession();
        $voucherCode = JRequest::getVar('voucher_code');
        $voucher = new OpenShopVoucher();
        $voucherData = $voucher->getVoucherData($voucherCode);
        if (!count($voucherData)) {
            $session->set('warning', JText::_('OPENSHOP_VOUCHER_APPLY_ERROR'));
        } else {
            $session->set('voucher_code', $voucherCode);
            $session->set('success', JText::_('OPENSHOP_VOUCHER_APPLY_SUCCESS'));
        }
    }

    /**
     * 
     * Function to apply shipping to the cart
     */
    function applyShipping() {
        $shippingMethod = explode('.', Jrequest::getVar('shipping_method'));
        $session = JFactory::getSession();
        $shippingMethods = $session->get('shipping_methods');
        if (isset($shippingMethods) && isset($shippingMethods[$shippingMethod[0]])) {
            $session->set('shipping_method', $shippingMethods[$shippingMethod[0]]['quote'][$shippingMethod[1]]);
            $session->set('success', JText::_('OPENSHOP_SHIPPING_APPLY_SUCCESS'));
        } else {
            $session->set('warning', JText::_('OPENSHOP_SHIPPING_APPLY_ERROR'));
        }
    }

    /**
     * 
     * Function to get Quote
     */
    function getQuote() {
        $json = array();
        $cart = new OpenShopCart();
        $countryId = JRequest::getInt('country_id');
        $zoneId = JRequest::getInt('zone_id');
        $postcode = JRequest::getVar('postcode');
        if (!$cart->hasProducts()) {
            $json['error']['warning'] = JText::_('OPENSHOP_ERROR_HAS_PRODUCTS');
        }

        if (!$cart->hasShipping()) {
            $json['error']['warning'] = JText::_('OPENSHOP_ERROR_HAS_SHIPPING');
        }
        if (!$countryId) {
            $json['error']['country'] = JText::_('OPENSHOP_ERROR_COUNTRY');
        }
        if (!$zoneId) {
            $json['error']['zone'] = JText::_('OPENSHOP_ERROR_ZONE');
        }
        $countryInfo = OpenShopHelper::getCountry($countryId);
        if (is_object($countryInfo) && $countryInfo->postcode_required && ((strlen($postcode) < 2) || (strlen($postcode) > 8))) {
            $json['error']['postcode'] = JText::_('OPENSHOP_ERROR_POSTCODE');
        }
        if (!$json) {
            $session = JFactory::getSession();
            $tax = new OpenShopTax(OpenShopHelper::getConfig());
            $tax->setShippingAddress($countryId, $zoneId);
            $session->set('shipping_country_id', $countryId);
            $session->set('shipping_zone_id', $zoneId);
            $session->set('shipping_postcode', $postcode);
            if (is_object($countryInfo)) {
                $countryName = $countryInfo->country_name;
                $isoCode2 = $countryInfo->iso_code_2;
                $isoCode3 = $countryInfo->iso_code_3;
            } else {
                $countryName = '';
                $isoCode2 = '';
                $isoCode3 = '';
            }
            $zoneInfo = OpenShopHelper::getZone($zoneId);
            if (is_object($zoneInfo)) {
                $zoneName = $zoneInfo->zone_name;
                $zoneCode = $zoneInfo->zone_code;
            } else {
                $zoneName = '';
                $zoneCode = '';
            }
            $addressData = array(
                'firstname' => '',
                'lastname' => '',
                'company' => '',
                'address_1' => '',
                'address_2' => '',
                'postcode' => $postcode,
                'city' => '',
                'zone_id' => $zoneId,
                'zone_name' => $zoneName,
                'zone_code' => $zoneCode,
                'country_id' => $countryId,
                'country_name' => $countryName,
                'iso_code_2' => $isoCode2,
                'iso_code_3' => $isoCode3
            );
            $quoteData = array();
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*')
                    ->from('#__openshop_shippings')
                    ->where('published = 1')
                    ->order('ordering');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            for ($i = 0; $n = count($rows), $i < $n; $i++) {
                $shippingName = $rows[$i]->name;
                $params = new JRegistry($rows[$i]->params);
                require_once JPATH_COMPONENT . '/plugins/shipping/' . $shippingName . '.php';
                $shippingClass = new $shippingName();
                $quote = $shippingClass->getQuote($addressData, $params);
                if ($quote) {
                    $quoteData[$shippingName] = array(
                        'title' => $quote['title'],
                        'quote' => $quote['quote'],
                        'ordering' => $quote['ordering'],
                        'error' => $quote['error']
                    );
                }
            }
            $session->set('shipping_methods', $quoteData);
            if ($session->get('shipping_methods')) {
                $json['shipping_methods'] = $session->get('shipping_methods');
            } else {
                $json['error']['warning'] = JText::_('OPENSHOP_NO_SHIPPING_METHODS');
            }
        }
        echo json_encode($json);
        JFactory::getApplication()->close();
    }

    /**
     * 
     * Function to get Zones for a specific Country
     */
    function getZones() {
        $json = array();
        $countryId = JRequest::getInt('country_id');
        $countryInfo = OpenShopHelper::getCountry($countryId);
        if (is_object($countryInfo)) {
            $json = array(
                'country_id' => $countryInfo->id,
                'country_name' => $countryInfo->country_name,
                'iso_code_2' => $countryInfo->iso_code_2,
                'iso_code_3' => $countryInfo->iso_code_3,
                'postcode_required' => $countryInfo->postcode_required,
                'zones' => OpenShopHelper::getCountryZones($countryId)
            );
        }
        echo json_encode($json);
        JFactory::getApplication()->close();
    }

    function buy() {
        $ses = JFactory::getSession();
        $app = JFactory::getApplication()->input;
        $quan = $app->getString('quantity');

        $id_pro = $app->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        foreach ($quan as $q) {
            $sizequantity = explode('-', $q);        //0:size - 1:quantity

            $query->clear();
            $query->select('id,quantity')
                    ->from($db->quoteName('#__openshop_cartproducts'))
                    ->where('product_id = ' . $id_pro)
                    ->where('size_id = ' . $sizequantity[0])
                    ->where('( user_id = ' . $ses->get('user')->id . ' OR ip = "' . $_SERVER['REMOTE_ADDR'] . '" )');
            $check = $db->setQuery($query)->loadObject();

            if (!count($check)) { //don't exists
                $query->clear();
                $data = new stdClass();
                $data->product_id = $id_pro;
                $data->size_id = $sizequantity[0];
                $data->color_id = 0;
                $data->quantity = $sizequantity[1];
                $data->user_id = $ses->get('user')->id;
                $data->ip = $_SERVER['REMOTE_ADDR'];
                $db->insertObject('#__openshop_cartproducts', $data);
            } else {    //exists
                $query->clear();
                $query->update($db->quoteName('#__openshop_cartproducts'))
                        ->set(array('quantity = ' . ((int) $check->quantity + (int) $sizequantity[1])))
                        ->where('id = ' . $check->id);
                $db->setQuery($query)->execute();
            }
        }
    }

    function changeQuantyCartProduct() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_CHANGE_FAILD'));
        $app = JFactory::getApplication()->input;
        $id = $app->getInt('id');
        $quantity = $app->getInt('quantity');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->update($db->quoteName('#__openshop_cartproducts'))
                    ->set(array('quantity = ' . $quantity))
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_CHANGE_SUCCESS'));
            }
        } catch (Exception $ex) {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_CHANGE_FAILD'));
        }

        echo json_encode($res);
    }

    function deleteCartProduct() {
        $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_DELETE_FAILD'));
        $app = JFactory::getApplication()->input;
        $id = $app->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->delete($db->quoteName('#__openshop_cartproducts'))
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                $res = array('status' => 'success', 'message' => JText::_('OPENSHOP_DELETE_SUCCESS'));
            }
        } catch (Exception $ex) {
            $res = array('status' => 'error', 'message' => JText::_('OPENSHOP_DELETE_FAILD'));
        }

        echo json_encode($res);
    }

    function changeZone() {
        $app = JFactory::getApplication()->input;
        $id = $app->getInt('id');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,district_name')
                ->from($db->quoteName('#__openshop_districts'))
                ->where('parent_id = ' . $id)
                ->where('published = 1');
        echo json_encode($db->setQuery($query)->loadObjectList());
    }

    function checkInput() {
        $app = JFactory::getApplication()->input;
        $act = $app->getString('act');
        $str = $app->getString('str');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_customers'))
                ->where($act . ' = "' . $str . '"');
        echo $db->setQuery($query)->loadResult();
    }

    function orderInCartProduct() {
        $data = JRequest::get('data');
        $arr_form = array();
        foreach ($data['data'] as $k => $v) {
            if ($v['name'] == 'cid[]') {
                $arr_form['cid'] = array($v['value']);
            } else {
                $arr_form[$v['name']] = $v['value'];
            }
        }
        $this->saveCartProduct($arr_form);
    }

    function saveCartProduct($data) {
        $session = JFactory::getSession();
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $check = 1;
        $getUserNew = '';
        try {
            if ($check) {
                if (empty(JFactory::getSession()->get('user')->id)) {
                    //insert user
                    $data['password'] = $data['customer_phone'];
                    $user = new JUser();
                    $user->bind($data); //convert pass

                    $data_user = new stdClass();
                    $data_user->name = $data['customer_name'];
                    $data_user->username = $data['customer_phone'];
                    $data_user->email = $data['customer_email'];
                    $data_user->password = $data['password'];
                    $data_user->block = '0';
                    $data_user->sendEmail = '0';
                    $data_user->registerDate = OpenShopHelper::getDateTimeCurrent();

                    if ($db->insertObject('#__users', $data_user)) {
                        $id_user_getInfo = $db->insertid();         //get id new user
                        $getUserNew = $id_user_getInfo;
                        $query->clear();
                        $query->select('max(id)')
                                ->from($db->quoteName('#__users'));
                        $user_id = $db->setQuery($query)->loadResult();
                    }

                    //add user_usergroup_map table
                    $data_usergroup_map = new stdClass();
                    $data_usergroup_map->group_id = '2';
                    $data_usergroup_map->user_id = $user_id;

                    $db->insertObject('#__user_usergroup_map', $data_usergroup_map);

                    $d_cus = new stdClass();
                    $d_cus->customer_id = $user_id;
                    $d_cus->customergroup_id = '1';
                    $d_cus->fullname = $data['customer_name'];
                    $d_cus->email = $data['customer_email'];
                    $d_cus->telephone = $data['customer_phone'];
                    $d_cus->published = '1';
                    $d_cus->created_date = OpenShopHelper::getDateTimeCurrent();
                    $d_cus->modified_date = OpenShopHelper::getDateTimeCurrent();

                    $db->insertObject('#__openshop_customers', $d_cus);

                    $d_address = new stdClass();
                    $d_address->customer_id = $user_id;
                    $d_address->address = $data['customer_address'];
                    $d_address->zone_id = $data['zone'];
                    $d_address->district_id = $data['district'];
                    $d_address->created_date = OpenShopHelper::getDateTimeCurrent();
                    $d_address->modified_date = OpenShopHelper::getDateTimeCurrent();

                    $db->insertObject('#__openshop_addresses', $d_address);

                    $query->clear();
                    $query->select('max(id)')->from($db->quoteName('#__openshop_addresses'));
                    $db->setQuery($query);
                    $fields = array('address_id' . ' = ' . $db->loadResult());
                    $query->update($db->quoteName('#__openshop_customers'))
                            ->set($fields)->where('customer_id = ' . $user_id);
                    $db->setQuery($query)->execute();
                    $returnRrl = JRoute::_('cam-on.html?t=' . OpenShopHelper::convertBase64Encode($id_user_getInfo) );
                } else {
                    $id_user_getInfo = $session->get('user')->id;
                    $returnRrl = JRoute::_('cam-on.html?t=' . OpenShopHelper::convertBase64Encode('0'));
                }
                

                if (!empty($getUserNew)) {
                    //Get info user
                    $query->clear();
                    $query->select('a.*,b.zone_id,b.address,b.district_id')
                            ->from($db->quoteName('#__openshop_customers', 'a'))
                            ->join('LEFT', $db->quoteName('#__openshop_addresses', 'b') . 'ON a.customer_id = b.customer_id')
                            ->where('a.customer_id = ' . $id_user_getInfo);
                    $userC = $db->setQuery($query)->loadObject();

                    if (!count($userC)) {
                        echo json_encode(array('status' => 'warning', 'message' => 'Chúng tôi không tìm thấy địa chỉ của bạn. Vui lòng cập nhật địa chỉ trước khi đặt hàng'));
                        return true;
                    } else {
                        $userC = new stdClass();
                        $userC->customer_id = $userC->id;
                        $userC->email = empty($data['user_email']) ? '' : $data['user_email'];
                        $userC->telephone = $data['user_phone'];
                        $userC->zone_id = $data['user_zone'];
                        $userC->fullname = $data['user_name'];
                        $userC->address = $data['user_address'];
                        $userC->district_id = $data['user_district'];
                    }
                } else {
                    $userC = new stdClass();
                    $userC->customer_id = $id_user_getInfo;
                    $userC->email = empty($data['user_email']) ? '' : $data['user_email'];
                    $userC->telephone = $data['user_phone'];
                    $userC->zone_id = $data['user_zone'];
                    $userC->fullname = $data['user_name'];
                    $userC->address = $data['user_address'];
                    $userC->district_id = $data['user_district'];
                }
//                print_r($data);
//                exit();

                //Save ORDER-------------------------------------------
                /*
                 * ORDER_NUMBER
                 */
                $query->clear();
                $query->select('max(invoice_number)')->from($db->quoteName('#__openshop_orders'));
                $db->setQuery($query);
                $num_invoice = $db->loadResult();

                $data['order_status_id'] = '8';  //pending

                $data_order = new stdClass();
                $data_order->customer_id = $userC->customer_id;
                $data_order->payment_email = $userC->email;
                $data_order->payment_telephone = $userC->telephone;
                $data_order->payment_zone_id = $userC->zone_id;
                $data_order->total = $data['total'];
                $data_order->payment_fullname = $userC->fullname;
                $data_order->payment_address = $userC->address;
                $data_order->payment_district_id = $userC->district_id;
                $data_order->buyfrom_id = $data['total'];
                $data_order->created_date = OpenShopHelper::getDateTimeCurrent();
                $data_order->order_status_id = '8';

                $query->clear();
                $query->select('a.*,b.product_sku,c.product_name,b.product_price,b.product_price_input')
                        ->from($db->quoteName('#__openshop_cartproducts', 'a'))
                        ->join('LEFT', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . ' ON b.id = c.product_id')
                        ->where('a.id IN (' . OpenShopHelper::convertBase64Decode($data['id_cartpro']) . ')');
                $rowsCP = $db->setQuery($query)->loadObjectList();

                //get id transporter
                $query->clear();
                $query->select('c.id')
                        ->from($db->quoteName('#__openshop_geozones', 'a'))
                        ->join('LEFT', $db->quoteName('#__openshop_geozonezones', 'b') . 'ON a.id = b.geozone_id')
                        ->join('LEFT', $db->quoteName('#__openshop_transporters', 'c') . 'ON a.transporter_id = c.id')
                        ->where('zone_id = ' . $userC->zone_id)
                        ->where('district_id = ' . $userC->district_id);
                $db->setQuery($query);

                $transporter_id = "";
                if ($db->loadResult()) {
                    $transporter_id = $db->loadResult();
                }
                foreach ($rowsCP as $kCP => $vCP) {

                    ++$num_invoice;
                    $data['invoice_number'] = $num_invoice;
                    $data['order_number'] = 'DH' . $num_invoice;
                    $data_order->order_number = $data['order_number'];
                    $data_order->invoice_number = $data['invoice_number'];

                    $db->insertObject('#__openshop_orders', $data_order);
                    $order_id = $db->insertid();

                    $data_op = new stdClass();
                    $data_op->order_id = $order_id;
                    $data_op->product_id = $vCP->product_id;
                    $data_op->product_name = $vCP->product_name;
                    $data_op->product_sku = $vCP->product_sku;
                    $data_op->quantity = $vCP->quantity;
                    $data_op->price = $vCP->product_price;
                    $data_op->total_price = (int) $vCP->quantity * (int) $vCP->product_price;
                    $data_op->product_size = $vCP->size_id;
                    $data_op->product_color = $vCP->color_id;
                    $data_op->optionheight_id = '0';
                    $data_op->optionweight_id = '0';
                    $data_op->order_product_desc = '';
                    $data_op->price_input = $vCP->product_price_input;

                    //insert into OrderProducts table
                    $db->insertObject('#__openshop_orderproducts', $data_op);
                    //delete record in cartproducts table
                    OpenShopHelper::deleteRecordTable('#__openshop_cartproducts', 'id', $vCP->id);
//                
                    //insert carts table
                    $data_cart = new stdClass();
                    $data_cart->order_id = $order_id;
                    $data_cart->transporter_id = $transporter_id;
                    $db->insertObject('#__openshop_carts', $data_cart);
                }

                echo json_encode(array('status' => 'success', 'message' => 'Đặt hàng thành công', 'url' => $returnRrl));
            } else {
                echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ', 'url' => ''));
            }
        } catch (Exception $ex) {
            echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được máy chủ', 'url' => ''));
        }
    }

    function changeQuantityCart() {
        try {
            $app = JFactory::getApplication()->input;
            $id = OpenShopHelper::convertBase64Decode($app->getString('i'));
            $quantity = OpenShopHelper::convertBase64Decode($app->getString('v'));
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->update('#__openshop_cartproducts')
                    ->set('quantity = ' . $quantity)
                    ->where('id = ' . $id);
            if ($db->setQuery($query)->execute()) {
                echo json_encode(array('status' => 'success', 'message' => 'Cập nhật số lượng thành công'));
            } else {
                echo json_encode(array('status' => 'error', 'message' => 'Cập nhật số lượng thất bại'));
            }
        } catch (Exception $ex) {
//            echo json_encode(array('status' => 'error', 'message' => 'Không kết nối được với máy chủ'));
        }
    }

}
