/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Author     : HUY HUYNH
 */

$ = jQuery.noConflict();
$(document).ready(function () {
    $('#myModalSignIn').on('hidden.bs.modal', function (e) {
        $('body').removeClass('modal-open');
    });
    $('.bg-menu-top').on('click', function () {
        $('.menuTopSmall').addClass('menuTop');
        $('.menuTop').removeClass('menuTopSmall');
        $('.bg-menu-top').hide();
    });
});
function showCart(v) {
    $check = 1;
    if ($('#quantityProduct').val() === '') {
        toastr['warning']('Chưa chọn số lượng');
        $check = 0;
        jQuery('.inputNumberProduct').addClass('animationValQuantity');
    }


    if ($check) {
        jQuery('#myTestModalDetail').modal('hide');
        jQuery('#myLoading').modal('show');
        $.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=product.addCartProduct',
            method: 'post',
            data: {
                id: $('#IDProduct').val(),
                SizeQuantity: $('#quantityProduct').val(),
                cond: v
            },
            success: function (dt) {
                if (dt === 'success') {
                    $('#formCart').submit();
                } else {
                    toastr['error']('Không kết nối được máy chủ!');
                }
            }
        });
    }

}

function changeSizeProduct(s, q) {
    var size = new Array();
    var checkT = 1;
    if ($('#quantityProduct').val() !== '') {
        var ss = $('#quantityProduct').val().split(',');
        for (var i = 0; i < ss.length; i++) {
            var t = ss[i].split('-');

            if (parseInt(t[0]) === parseInt(s)) {
                if (q !== 0) {
                    size.push(s + '-' + q);
                }
                checkT = 0;
            } else
            {
                size.push(t[0] + '-' + t[1]);
            }


        }
        if (checkT) {
            size.push(s + '-' + q);
        }
    } else
    {
        size.push(s + '-' + q);
    }
    $('#quantityProduct').val(size);
}

function removeCartProduct(id) {
    jQuery('#myLoading').modal('show');
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=product.removeCartProduct',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            jQuery('#myLoading').modal('hide');
            if (dt === 'success') {
                toastr['success']('Xóa thành công');
                location.reload();
            } else
            {
                toastr['error']('Xóa thất bại');
            }
        }
    });
}

function closeAction() {
    $('.question').css('display', 'none');
}

function showRemoveAction(id) {
    $('.question').css('display', 'block');
    $('#action').attr('onclick', 'removeCartProduct("' + id + '")');
}

function checkoutCart(c) {
    var temp = 1;
    if (c === 'quick') {

        var rules = new Array(
                {
                    id: 'customer_name_quick',
                    rule: 'required'
                },
                {
                    id: 'customer_phone_quick',
                    rule: 'number',
                    limit: '10-11'
                },
                {
                    id: 'customer_address_quick',
                    rule: 'required'
                },
                {
                    id: 'customer_town_quick',
                    rule: 'selected'
                },
                {
                    id: 'customer_district_1',
                    rule: 'selected'
                }
        );
        if (checkValidator(rules)) {
            var data = $('#formQuickOrder').serializeArray();

            jQuery('#myLoading').modal('show');

            $.ajax({
                url: 'index.php?option=com_openshop&format=ajax&task=profile.checkPhone',
                method: 'post',
                data: {
                    p: $('#customer_phone_quick').val()
                },
                success: function (dt) {
                    if (dt === '0') {
                        $.ajax({
                            url: 'index.php?option=com_openshop&format=ajax&task=cart.orderInCartProduct',
                            method: 'post',
                            data: {
                                data: data
                            },
                            success: function (dt) {
                                var d = $.parseJSON(dt);
                                toastr[d['status']](d['message']);
                                if (d['status'] === 'success') {
                                    location.href = d['url'];
                                }
                            }
                        });
                    } else {        //trùng số điện thoại
                        jQuery('#myLoading').modal('hide');
                        $('.error-customer_phone_quick').html('Số điện thoại đã được sử dụng');
                        $('#customer_phone_quick').css('border-color', 'red');
                    }
                }
            });

        }
    } else
    {
        var rules = new Array(
                {
                    id: 'user_name',
                    rule: 'required'
                },
                {
                    id: 'user_phone',
                    rule: 'number',
                    limit: '10-11'
                },
                {
                    id: 'user_address',
                    rule: 'required'
                },
                {
                    id: 'user_zone',
                    rule: 'selected'
                },
                {
                    id: 'user_district',
                    rule: 'selected'
                }
        );
        
        if (checkValidator(rules)) {
            var data = $('#formCart').serializeArray();

            jQuery('#myLoading').modal('show');
            $.ajax({
                url: 'index.php?option=com_openshop&format=ajax&task=cart.orderInCartProduct',
                method: 'post',
                data: {
                    data: data
                },
                success: function (dt) {
                    var d = $.parseJSON(dt);
                    toastr[d['status']](d['message']);
                    if (d['status'] === 'success') {
                        location.href = d['url'];
                    } else {
                        jQuery('#myLoading').modal('hide');
                    }
                }
            });
        }

    }
}

function showQuiclOrder() {
    $('#myModalQuiclOrder').modal();
}

function showlogin() {
    $('#modalFormLogin').modal();
}
function showSignIn() {
    $('body').addClass('modal-open');
    $('#myModalSignIn').modal();
}

function loginForm() {
    var rules = new Array(
            {
                id: 'username',
                rule: 'required'
            },
            {
                id: 'password',
                rule: 'required'
            }
    );
    if (checkValidator(rules)) {
        $('#formLogin').submit();
    }
}

function formLogout() {
    $('#formLogout').submit();
}

function changeZone(id) {
    var loadding = '<option value="0">Đang tải ...</option>';
    $('#customer_district').html(loadding);
    $('#customer_district_1').html(loadding);
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=cart.changeZone',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            var h = '<option value="0">- Quận/Huyện -</option>';
            $.each(d, function (index, value) {
                h += '<option value="' + value['id'] + '">' + value['district_name'] + '</option>';
            });
            $('#customer_district').html(h);
            $('#customer_district_1').html(h);
        }
    });
}

function changeZoneUser(id) {
    var loadding = '<option value="">Đang tải ...</option>';
    $('#user_district').html(loadding);
    $.ajax({
        url: encodeURI('index.php?option=com_openshop&format=ajax&task=cart.changeZone'),
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            var h = '<option value="0">- Quận/Huyện -</option>';
            $.each(d, function (index, value) {
                h += '<option value="' + value['id'] + '">' + value['district_name'] + '</option>';
            });
            $('#user_district').html(h);
        }
    });
}

function changeZoneUserDiff(id) {
    var loadding = '<option value="">Đang tải ...</option>';
    $('#user_district_diff').html(loadding);
    $.ajax({
        url: encodeURI('index.php?option=com_openshop&format=ajax&task=cart.changeZone'),
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            var h = '<option value="0">- Quận/Huyện -</option>';
            $.each(d, function (index, value) {
                h += '<option value="' + value['id'] + '">' + value['district_name'] + '</option>';
            });
            $('#user_district_diff').html(h);
        }
    });
}

function showChooseSize(v) {
    jQuery('#myLoading').modal('show');
    $.ajax({
        url: 'index.php?com_openshop&format=ajax&task=product.getSizeCP',
        method: 'post',
        data: {
            id: v
        },
        success: function (dt) {
            jQuery('#myLoading').modal('hide');
            var d = $.parseJSON(dt);
            var h = '';
            h += '<select name="slt_sizeCP" id="slt_sizeCP" class="form-control">';
            h += '   <option value="">- Chọn -</option>';
            $.each(d, function (i, v) {
                h += '<option value="' + v['optionvalue_id'] + '">' + v['value'] + '</option>';
            });
            h += '</select>';
            $('.contentSizeCP').html(h);
            $('.addCP').attr('onclick', 'addCP("' + v + '")');
            $('.chooseSizeCP').modal();
        }
    });
}

function addCP(v) {
    if ($('#slt_sizeCP').val() !== '') {
        $('.chooseSizeCP').modal('hide');
        jQuery('#myLoading').modal('show');
        $.ajax({
            url: 'index.php?com_openshop&format=ajax&task=product.addCP',
            method: 'post',
            data: {
                id: v,
                size: $('#slt_sizeCP').val()
            },
            success: function (dt) {
                jQuery('#myLoading').modal('hide');
                var d = $.parseJSON(dt);
                toastr[d['status']](d['message']);
            }
        });
    } else
    {
        toastr['warning']('Chọn size');
    }
}

function wishProduct(id) {
    jQuery('#myLoading').modal('show');
    $.ajax({
        url: 'index.php?com_openshop&format=ajax&task=product.wishProduct',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            jQuery('#myLoading').modal('hide');
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
        }
    });
}

function nonwishProduct(id) {
    jQuery('#myLoading').modal('show');
    $.ajax({
        url: 'index.php?com_openshop&format=ajax&task=product.nonwishProduct',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            jQuery('#myLoading').modal('hide');
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
        }
    });
}

function formRegister() {
    var rules = new Array(
            {
                id: 'usernameR',
                rule: 'required'
            },
            {
                id: 'passwordR',
                rule: 'required'
            },
            {
                id: 'password_again',
                rule: 'match',
                match: 'passwordR'
            },
            {
                id: 'customer_name',
                rule: 'required'
            },
            {
                id: 'customer_phone',
                rule: 'required'
            },
            {
                id: 'customer_address',
                rule: 'required'
            },
            {
                id: 'customer_town',
                rule: 'selected'
            },
            {
                id: 'customer_district',
                rule: 'selected'
            }
    );
    if (checkValidator(rules)) {
        $('#formRegister').submit();
    }
}



/*
 * Validator
 */

function checkValidator(rules) {
    var res = 1;
    var limit = '';
    $.each(rules, function (i, v) {
        switch (v['rule']) {
            case 'required':
                if ($('#' + v['id']).val() === '') {
                    $('#' + v['id']).css('border-color', 'red');
                    $('.error-' + v['id']).text(' * Bắt buộc nhập');
                    res = 0;
                } else {
                    $('#' + v['id']).css('border-color', 'rgb(221, 221, 221)');
                    $('.error-' + v['id']).text('');
                }
                break;
            case 'match':
                if ($('#' + v['id']).val() !== $('#' + v['match']).val()) {
                    $('#' + v['id']).css('border-color', 'red');
                    $('.error-' + v['id']).text(' * Không giống nhau');
                    res = 0;
                } else {
                    $('#' + v['id']).css('border-color', 'rgb(221, 221, 221)');
                    $('.error-' + v['id']).text('');
                }
                break;
            case 'selected':
                if ($('#' + v['id']).val() === '0' || $('#' + v['id']).val() === '') {
                    $('#' + v['id']).css('border-color', 'red');
                    $('.error-' + v['id']).text(' * Bắt buộc chọn');
                    res = 0;
                } else {
                    $('#' + v['id']).css('border-color', 'rgb(221, 221, 221)');
                    $('.error-' + v['id']).text('');
                }
                break;
            case 'number':
                if (isNaN($('#' + v['id']).val()) || $('#' + v['id']).val() === '')
                {
                    $('#' + v['id']).css('border-color', 'red');
                    $('.error-' + v['id']).text(' * Bắt buộc nhập số nguyên');
                    res = 0;
                } else
                {
                    limit = v['limit'].split('-');

                    if (parseInt($('#' + v['id']).val().length) < parseInt(limit[0]) || parseInt($('#' + v['id']).val().length) > parseInt(limit[1])) {
                        $('#' + v['id']).css('border-color', 'red');
                        $('.error-' + v['id']).text(' * Bắt buộc nhập số nguyên có từ ' + limit[0] + ' đến ' + limit[1]);
                        res = 0;
                    } else
                    {
                        $('#' + v['id']).css('border-color', 'rgb(221, 221, 221)');
                        $('.error-' + v['id']).text('');
                    }
                }
                break;
            default:
                break;
        }
    });
    return res;
}

function loadMoreProducts() {
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=product.loadMoreProducts',
        method: 'post',
        data: {
            limit: $('#limitH').val()
        },
        success: function (dt) {
            console.log(dt);
        }
    });
}

function getModelShowEditProfile(v) {
    $('.editNoneProfile').css('display', 'none');
    $('.' + v).css('display', 'block');
    $('#varedit').val(v);
    $('#myModalEditProfile').modal();
}

function checkFormProfile() {
    var edit = $('#varedit').val();
    switch (edit) {
        case 'editname':
            var rules = new Array(
                    {
                        id: 'nameProfile',
                        rule: 'required'
                    }
            );
            if (checkValidator(rules)) {
                return true;
            } else
            {
                return false;
            }
            break;
        case 'editpassword':
            var rules = new Array(
                    {
                        id: 'passwordProfile',
                        rule: 'required'
                    },
                    {
                        id: 'passwordagainProfile',
                        rule: 'match',
                        match: 'passwordProfile'
                    }
            );
            if (checkValidator(rules)) {
                return true;
            } else
            {
                return false;
            }
            break;
        case 'editphone':
            var rules = new Array(
                    {
                        id: 'phoneProfile',
                        rule: 'required'
                    }
            );
            if (checkValidator(rules)) {
                return true;
            } else
            {
                return false;
            }
            break;
        case 'editemail':
            var rules = new Array(
                    {
                        id: 'emailProfile',
                        rule: 'required'
                    }
            );
            if (checkValidator(rules)) {
                return true;
            } else
            {
                return false;
            }
            break;
        case 'editaddress':
            var rules = new Array(
                    {
                        id: 'addressProfile',
                        rule: 'required'
                    },
                    {
                        id: 'customer_town',
                        rule: 'selected'
                    },
                    {
                        id: 'customer_district',
                        rule: 'selected'
                    }
            );
            if (checkValidator(rules)) {
                return true;
            } else
            {
                return false;
            }
            break;
        default:
            break;
    }
}

function removeAttr(v) {
    $.ajax({
        url: 'index.php?com_openshop&format=ajax&task=profile.clearSession',
        method: 'post',
        success: function () {
            //
        }
    });
    $('.' + v).remove();
}

function changeQuantityCart(i, v) {
    jQuery('#myLoading').modal('show');
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=cart.changeQuantityCart',
        method: 'post',
        data: {
            i: i,
            v: v
        },
        success: function (dt) {
            jQuery('#myLoading').modal('hide');
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            location.reload();
        }
    });
}

function showMenuTop() {
    $('.menuTop').addClass('menuTopSmall');
    $('.menuTopSmall').removeClass('menuTop');
    $('.bg-menu-top').show();
}

/*
 * SEARCH TAG
 */
function searchTag(v) {
    $('#key_search').val(v);
    $('#search_mini_form').submit();
}

/*
 * ANSWER FAQ
 */
function showAnswerFAQ(v) {
    $('#keyFAQ').val(v);
    $('#formAnswerFAQ').submit();
}

function showAnswer(k) {
    $('.answer_' + k).toggle('100');
}

/*
 * Page Number
 */
function getPageN(v) {
    $('#pageN').val(v);
    $('#formParchaseH').submit();
}

/*
 * SHOW MENU CATEGORY
 */
function showMenuCategory() {
    $('.bg-menuCartResponsive').css('display', 'block');
    $('.categories').toggle();
}
/*
 * HIDE MENU CATEGORY
 */
function hideMenuCategory() {
    $('.bg-menuCartResponsive').css('display', 'none');
    $('.categories').toggle();
}
/*
 * SHOW MENU
 */
function showMenu3() {
    $('.menuResponsive').toggle();
    $('.bg-menu3').css('display', 'block');
    $('body').css('overflow', 'hidden');
}
function hideMenuResponsive() {
    $('.menuResponsive').toggle();
    $('.bg-menu3').css('display', 'none');
    $('body').css('overflow', 'auto');
}

/*
 * View Map
 */
function showViewMap(v) {
    $('.modalViewMap').modal();
    $('#googlemaplink').attr('src', v);
    s
}


function showSignInUp() {
    $('#modalFormLogin').modal();
}

function signUpForm() {
    $('body').addClass('modal-open');
    $('#modalFormLogin').modal('hide');
    $('#myModalSignIn').modal();
}