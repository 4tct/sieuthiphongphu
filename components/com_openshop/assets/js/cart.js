$ = jQuery.noConflict();

function check_cart(id) {
    var ids = id.split(',');
    var check_quantity = '0';
    var arr_quantity = new Array();
    $.each(ids, function (index, value) {
        if ($('#quantity_order_' + value).val() !== '0') {
            arr_quantity.push(value + '-' + $('#quantity_order_' + value).val());
            check_quantity = '1';
        }
    });

    if (check_quantity === '0') {
        toastr['warning']('Chọn số lượng trước khi đặt!');
    } else
    {
        $.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=cart.buy',
            method: 'post',
            data: {
                quantity: arr_quantity,
                id: $('#product_id').val()
            },
            success: function (dt) {
                console.log(dt);
                window.location.href = 'index.php?option=com_openshop&view=cart';
            }
        });
    }

}

function changeQuantityCartProduct(id_cartpro, price, quantity) {
    var money = price * quantity;
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=cart.changeQuantyCartProduct',
        method: 'post',
        data: {
            id: id_cartpro,
            quantity: quantity
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            if (d['status'] === 'success') {
                $('.amount_' + id_cartpro).text(convert_money(money));
                total_quantity();
                total_money();
            }
        }
    });
}

function deleteCartProduct(id) {
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=cart.deleteCartProduct',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            toastr[d['status']](d['message']);
            if (d['status'] === 'success') {
                $('.tr_' + id).remove();
                var ids = $('#id_cartpro').val().split(',');
                if (ids.indexOf(id.toString()) !== -1) {
                    ids.splice(ids.indexOf(id.toString()), 1);
                }

                $('#id_cartpro').val(ids.join(','));

                total_quantity();
                total_money();

            }
        }
    });
}

function deleteNotAmount(id) {
    $('.tr_' + id).remove();
    var ids = $('#id_cartpro').val().split(',');
    if (ids.indexOf(id.toString()) !== -1) {
        ids.splice(ids.indexOf(id.toString()), 1);
    }

    $('#id_cartpro').val(ids.join(','));

    total_quantity();
    total_money();
}

function changeZone(id) {
    var loadding = '<option value="0">Đang tải ...</option>';
    $('#customer_district').html(loadding);
    $('#customer_district_1').html(loadding);
    $.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=cart.changeZone',
        method: 'post',
        data: {
            id: id
        },
        success: function (dt) {
            var d = $.parseJSON(dt);
            var h = '<option value="0">- Quận/Huyện -</option>';
            $.each(d, function (index, value) {
                h += '<option value="' + value['id'] + '">' + value['district_name'] + '</option>';
            });

            $('#customer_district').html(h);
            $('#customer_district_1').html(h);
        }
    });
}


function checkInput(str, act) {
    switch (act) {
        case 'telephone':
            if (str === '') {
                toastr['warning']('Chưa nhập số điện thoại!');
                $('#buyer_phone').css('border', '1px solid red');
            } else if (str.length !== 10 && str.length !== 11) {
                toastr['warning']('Số điện thoại không đúng!');
                $('#buyer_phone').css('border', '1px solid red');
            } else
            {
                var check = 1;
                for (var i = 0; i < str.length; i++) {
                    var n = str.substr(i, 1);
                    if (isNaN(n)) {
                        check = 0;
                        break;
                    }
                }

                if (check === 1)
                {
                    $.ajax({
                        url: 'index.php?option=com_openshop&format=ajax&task=cart.checkInput',
                        method: 'post',
                        data: {
                            act: act,
                            str: str
                        },
                        success: function (dt) {
                            if (dt == '1') {
                                toastr['warning']('Số điện thoại đã sử dụng!');
                                $('#buyer_phone').css('border', '1px solid red');
                                $('#buyer_phone').focus();
                            } else
                            {
                                $('#buyer_phone').css('border', '1px solid #ccc');
                            }
                        }
                    });
                }
            }
            break;
        case 'email':
            if (str === '') {
                toastr['warning']('Chưa nhập email!');
                $('#buyer_email').css('border', '1px solid red');
            } else
            {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(str)) {
                    toastr['warning']('Email không hợp lệ. Ví dụ: Example@gmail.com');
                } else {
                    $.ajax({
                        url: 'index.php?option=com_openshop&format=ajax&task=cart.checkInput',
                        method: 'post',
                        data: {
                            act: act,
                            str: str
                        },
                        success: function (dt) {
                            if (dt == '1') {
                                toastr['warning']('Email đã sử dụng!');
                                $('#buyer_email').css('border', '1px solid red');
                                $('#buyer_email').focus();
                            } else
                            {
                                $('#buyer_email').css('border', '1px solid #ccc');
                            }
                        }
                    });
                }
            }
            break;
        default:
            break;
    }
}

function submitForm() {
    var check = 1;
    if ($('#id_cartpro').val() === '') {
        toastr['warning']('Không có sản phẩm!');
        check = 0;
    } else {
        if ($('#buyer_name').val() === '') {
            $('#buyer_name').css('border', '1px solid red');
            check = 0;
        } else
        {
            $('#buyer_name').css('border', '1px solid #ccc');
        }
        //check phone
        if ($('#buyer_phone').val() === '') {
            $('#buyer_phone').css('border', '1px solid red');
            check = 0;
        } else
        {
            $('#buyer_phone').css('border', '1px solid #ccc');
        }
        //check mail
        if ($('#buyer_email').val() === '') {
            $('#buyer_email').css('border', '1px solid red');
            check = 0;
        } else
        {
            $('#buyer_email').css('border', '1px solid #ccc');
        }
        //check address
        if ($('#buyer_address').val() === '') {
            $('#buyer_address').css('border', '1px solid red');
            check = 0;
        } else
        {
            $('#buyer_address').css('border', '1px solid #ccc');
        }

        if ($('#zone').val() === '0') {
            $('#zone').css('border', '1px solid red');
            check = 0;
        } else
        {
            $('#zone').css('border', '1px solid #ccc');
        }
        if ($('#district').val() === '0') {
            $('#district').css('border', '1px solid red');
            check = 0;
        } else
        {
            $('#district').css('border', '1px solid #ccc');
        }

        if (check == 0) {
            toastr['warning']('Không đủ thông tin');
        } else
        {
            $.ajax({
                url: 'index.php?option=com_openshop&format=ajax&task=cart.orderInCartProduct',
                method: 'post',
                data: {
                    data: $('#formCart').serializeArray()
                },
                beforeSend: function (xhr) {
                    $('.loading').css('display','block');
                },
                success: function (dt) {
                    $('.loading').css('display','none');
                    if(dt === 'success'){
                        window.location.href = 'index.php?option=com_openshop&view=cart&layout=thanks';
                    }
                    else
                    {
                        toastr['error']('Không kết nối được với máy chủ');
                    }
                }
            });
        }
    }
}

/*
 * CONVERT MONEY
 */
function convert_money(num)
{
    var str_num = num.toString();
    var arr_num = new Array();
    var temp = 1;
    for (var i = (str_num.length - 1); i >= 0; i--)
    {
        if (temp === 3 && i !== 0 && str_num.slice(i - 1, i) !== '-')
        {
            arr_num[i] = '.' + str_num.slice(i, i + 1);
            temp = 1;
        } else
        {
            arr_num[i] = str_num.slice(i, i + 1);
            temp += 1;
        }
    }
    return arr_num.join('');
}

/*
 * CONVERT MONEY -> INT
 * @param {type} str
 * @returns {String}
 */
function convert_money_num(str) {
    var arr = str.split('.');
    var num = '';
    for (var i = 0, l = arr.length; i < l; i++) {
        num += arr[i];
    }

    return num;
}

/*
 * TOTAL Quantity
 */

function total_quantity() {
    var ids = $('#id_cartpro').val().split(',');
    var total = 0;
    $.each(ids, function (index, value) {
        if (value !== '') {
            total += parseInt($('#quantity_cart_' + value).val());
        }
    });

    $('.total-quantity').text(total);
}

/*
 * TOTAL MONEY
 */

function total_money() {
    var ids = $('#id_cartpro').val().split(',');
    var total = 0;
    $.each(ids, function (index, value) {
        if (value !== '') {
            total += parseInt(convert_money_num($('.amount_' + value).text()));
        }
    });
    $('.total-money').text(convert_money(total));
    $('.total').text(convert_money(total + parseInt(convert_money_num($('.transfer-money').text()))));
    $('#total').val(total + parseInt(convert_money_num($('.transfer-money').text())));
}
