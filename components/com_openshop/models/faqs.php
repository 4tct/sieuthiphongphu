<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelFAQs extends OpenShopModelProducts {

    public function getFAQs() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $app = JFactory::getApplication()->input;
        $query->select('*')
                ->from($db->quoteName('#__openshop_faqs', 'a'))
                ->where('a.published = 1');
        return $db->setQuery($query)->loadObjectList();
    }

    public function getAnswerFAQ() {
        try {
            $app = JFactory::getApplication()->input;
            $id = OpenShopHelper::convertBase64Decode($app->getString('keyFAQ'));
            if (!empty($id)) {
                $db = JFactory::getDbo();
                $query = $db->getQuery(TRUE);
                $query->select('b.*')
                        ->from($db->quoteName('#__openshop_faqs', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_faqdetails', 'b') . ' ON a.id = b.faq_id')
                        ->where('a.id = ' . $id)
                        ->where('a.published = 1');
                return $db->setQuery($query)->loadObjectList();
            }
            else
            {
                return '';
            }
        } catch (Exception $ex) {
            return '';
        }
    }

}
