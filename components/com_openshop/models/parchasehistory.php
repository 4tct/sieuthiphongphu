<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelParchaseHistory extends JModelLegacy {

    public function getOrderCus() {
        try {
            $session = JFactory::getSession();
            $user = $session->get('user');
            $app = JFactory::getApplication()->input;
            $limit = $app->getInt('pageN') * OpenShopHelper::getConfigValue('catalog_limit');
            if (!empty($user->id)) {
                $db = JFactory::getDbo();
                $query = $db->getQuery(TRUE);
                $query->select('a.*,b.product_name,b.quantity,b.price,b.total_price,c.product_image')
                        ->from($db->quoteName('#__openshop_orders', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_orderproducts', 'b') . ' ON a.id = b.order_id ')
                        ->join('INNER', $db->quoteName('#__openshop_products','c') . ' ON c.id = b.product_id')
                        ->where('a.customer_id = ' . $user->id);
                return $db->setQuery($query, $limit , OpenShopHelper::getConfigValue('catalog_limit'))->loadObjectList();
            } else {
                return '';
            }
        } catch (Exception $ex) {
//            return '';
            echo $ex;;
        }
    }
    
    public function getOrderCusPagination(){
        try {
            $session = JFactory::getSession();
            $user = $session->get('user');
            if (!empty($user->id)) {
                $db = JFactory::getDbo();
                $query = $db->getQuery(TRUE);
                $query->select('count(a.id)')
                        ->from($db->quoteName('#__openshop_orders', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_orderproducts', 'b') . ' ON a.id = b.order_id ')
                        ->join('INNER', $db->quoteName('#__openshop_products','c') . ' ON c.id = b.product_id')
                        ->where('a.customer_id = ' . $user->id);
                $num = $db->setQuery($query)->loadResult();
                return (int)$num / OpenShopHelper::getConfigValue('catalog_limit');
            } else {
                return '0';
            }
        } catch (Exception $ex) {
            return '0';
        }
    }

}
