<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelSearch extends OpenShopModelProducts {

    protected function _buildQueryWhere(JDatabaseQuery $query) {
        parent::_buildQueryWhere($query);
        $state = $this->getState();
        $db = $this->getDbo();
        $subQuery = $db->getQuery(true);
        $subsubQuery = $db->getQuery(true);

        return $this;
    }

    public function setTags() {
        $state = $this->getState();

        if ($state->filter_search) {
            try {
                $db = JFactory::getDbo();
                $query = $db->getQuery(TRUE);
                $query->select('hits as hits,id')
                        ->from($db->quoteName('#__openshop_tags'))
                        ->where('tag_name = "' . $state->filter_search . '"');
                $Tag = $db->setQuery($query)->loadObject();

                //keyword is exists. Update
                if (count($Tag)) {
                    $query->clear();
                    $query->update($db->quoteName('#__openshop_tags'))
                            ->set('hits = ' . ((int) $Tag->hits + 1))
                            ->where('id = ' . $Tag->id);
                    if ($db->setQuery($query)->execute()) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } else {
                    $data = new stdClass();
                    $data->tag_name = $state->filter_search;
                    $data->hits = '1';
                    $data->published = '1';

                    if ($db->insertObject('#__openshop_tags', $data)) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
//                return FALSE;
                echo $ex;
            }
        }
    }

}
