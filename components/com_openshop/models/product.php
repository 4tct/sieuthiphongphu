<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_openshop' . DS . 'models'.DS.'products.php' );
class OpenShopModelProduct extends OpenShopModel
{
	/**
	 * Entity ID
	 *
	 * @var int
	 */
	protected $id = null;

	/**
	 * Entity data
	 *
	 * @var array
	 */
	protected $data = null;
	
	/**
	 * Current active language
	 *
	 * @var string
	 */
	protected $language = null;
	
	/**
	 * 
	 * Constructor
	 * @since 1.5
	 */
	public function __construct($config = array())
	{
		parent::__construct();
		$this->id = JRequest::getInt('id');
		$this->data = null;
		$this->language = JFactory::getLanguage()->getTag();
	}
	
	/**
	 * 
	 * Function to get product data
	 * @see OpenShopModel::getData()
	 */
	function &getData()
	{
		if (empty($this->data))
		{
			$this->_loadData();
		}
		return $this->data;
	}
	
	/**
	 * 
	 * Function to load product data
	 * @see OpenShopModel::_loadData()
	 */
	function _loadData() {
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*, b.product_name, b.product_alias, b.product_desc, b.product_short_desc, b.product_page_title, b.product_page_heading, b.meta_key, b.meta_desc, b.tab1_title, b.tab1_content, b.tab2_title, b.tab2_content, b.tab3_title, b.tab3_content, b.tab4_title, b.tab4_content, b.tab5_title, b.tab5_content')
			->from('#__openshop_products AS a')
			->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
			->where('a.id = ' . intval($this->id))
			->where('a.published = 1')
			->where('b.language = "' . $this->language . '"');
		//Check viewable of customer groups
		$user = JFactory::getUser();
		if ($user->get('id'))
		{
			$customer = new OpenShopCustomer();
			$customerGroupId = $customer->getCustomerGroupId();
		}
		else
		{
			$customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
		}
		if (!$customerGroupId)
			$customerGroupId = 0;
		$query->where('((a.product_customergroups = "") OR (a.product_customergroups IS NULL) OR (a.product_customergroups = "' . $customerGroupId . '") OR (a.product_customergroups LIKE "' . $customerGroupId . ',%") OR (a.product_customergroups LIKE "%,' . $customerGroupId . ',%") OR (a.product_customergroups LIKE "%,' . $customerGroupId . '"))');
		//Check out of stock
		if (OpenShopHelper::getConfigValue('hide_out_of_stock_products'))
		{
			$query->where('a.product_quantity > 0');
		}
                
		$db->setQuery($query);
		$this->data = $db->loadObject();
	}
	
	/**
	 * 
	 * Function to write review
	 * @param array $data
	 * @return json array
	 */
	function writeReview($data)
	{
		$user = JFactory::getUser();
		$json = array();
		if (strlen($data['author']) < 3 || strlen($data['author']) > 25)
		{
			$json['error'] = JText::_('OPENSHOP_ERROR_YOUR_NAME');
			return $json;
		}
		if (strlen($data['review']) < 25 || strlen($data['review']) > 1000)
		{
			$json['error'] = JText::_('OPENSHOP_ERROR_YOUR_REVIEW');
			return $json;
		}
		if (!$data['rating'])
		{
			$json['error'] = JText::_('OPENSHOP_ERROR_RATING');
			return $json;
		}
		if (OpenShopHelper::getConfigValue('enable_reviews_captcha'))
		{
			$captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
			if ($captchaPlugin == 'recaptcha')
			{
				$input = JFactory::getApplication()->input;
				$res = JCaptcha::getInstance($captchaPlugin)->checkAnswer($input->post->get('recaptcha_response_field', '', 'string'));
				if (!$res)
				{
					$json['error'] = JText::_('OPENSHOP_INVALID_CAPTCHA');
					return $json;
				}	
			}
		}
		if (!$json)
		{
			$row = JTable::getInstance('OpenShop', 'Review');
			$row->bind($data);
			$row->id = '';
			$row->product_id = $data['product_id'];
			$row->customer_id = $user->get('id') ? $user->get('id') : 0;
			$row->published = 0;
			$row->created_date = JFactory::getDate()->toSql();
			$row->created_by = $user->get('id') ? $user->get('id') : 0;
			$row->modified_date = JFactory::getDate()->toSql();
			$row->modified_by = $user->get('id') ? $user->get('id') : 0;
			$row->checked_out = 0;
			$row->checked_out_time = '0000-00-00 00:00:00';
			if ($row->store())
			{
				$json['success'] = JText::_('OPENSHOP_REVIEW_SUBMITTED_SUCESSFULLY');
			}
			else 
			{
				$json['error'] = JText::_('OPENSHOP_REVIEW_SUBMITTED_FAILURED');
			}
			return $json;
		}
	}
	
	/**
	 * 
	 * Function to process ask question
	 * @param array $data
	 */
	function processAskQuestion($data)
	{
		$jconfig = new JConfig();
		$fromName = $jconfig->fromname;
		$fromEmail =  $jconfig->mailfrom;
		$product = OpenShopHelper::getProduct($data['product_id']);
		$subject 	= JText::_('OPENSHOP_ASK_QUESTION_SUBJECT');
		$body 		= OpenShopHelper::getAskQuestionEmailBody($data, $product);
		$adminEmail = OpenShopHelper::getConfigValue('email') ? trim(OpenShopHelper::getConfigValue('email')) : $fromEmail;
		$mailer = JFactory::getMailer();
		$mailer->ClearAllRecipients();
		$mailer->sendMail($fromEmail, $fromName, $adminEmail, $subject, $body, 1);
	}
	
	/*
	*
	*/
	function getListProducts(){
		JLoader::import('joomla.application.component.model');
		
		$a = RADModel::getInstance( 'Products', 'OpenShopModel' );
		// $products_model->setState( 'id', $myItemId );
		$products = $a->getData();
		return $products;	
	}
}