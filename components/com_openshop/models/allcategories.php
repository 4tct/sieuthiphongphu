<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelAllcategories extends OpenShopModelProducts {

    protected function _buildQueryWhere(JDatabaseQuery $query) {
        parent::_buildQueryWhere($query);
        return $this;
    }

    /**
     * Builds a generic ORDER BY clasue based on the model's state
     */
    protected function _buildQueryOrder(JDatabaseQuery $query) {
        $query->order('created_date DESC,product_hot DESC, product_new DESC');
    }

}
