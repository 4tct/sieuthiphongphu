<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelorderCTV extends OpenShopModel {

    /**
     * Entity data
     *
     * @var array
     */
    protected $cartData = null;

    /**
     * 
     * Total Data object array, each element is an price price in the cart 
     * @var object array
     */
    protected $totalData = null;

    /**
     * 
     * Final total price of the cart
     * @var float
     */
    protected $total = null;

    /**
     * 
     * Taxes of all elements in the cart
     * @var array
     */
    protected $taxes = null;

    public function __construct($config = array()) {
        parent::__construct();
        $this->cartData = null;
        $this->totalData = null;
        $this->total = null;
        $this->taxes = null;
    }

}
