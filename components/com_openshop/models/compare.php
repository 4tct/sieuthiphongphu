<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelCompare extends OpenShopModel
{

	/**
	 * 
	 * Constructor
	 * @since 1.5
	 */
	public function __construct($config = array())
	{
		parent::__construct();
	}
	
	function add($productId)
	{
		$json = array();
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$compare = $session->get('compare');
		if (!$compare)
		{
			$compare = array();
		}
		$productInfo = OpenShopHelper::getProduct($productId, JFactory::getLanguage()->getTag());
		if (is_object($productInfo))
		{
			if (!in_array($productId, $compare))
			{
				if (count($compare) >= 4) {
					array_shift($compare);
				}
				$compare[] = $productId;
				$session->set('compare', $compare);
			}
			$viewProductLink = JRoute::_(OpenShopRoute::getProductRoute($productId, OpenShopHelper::getProductCategory($productId)));
			$viewCompareLink = JRoute::_(OpenShopRoute::getViewRoute('compare'));
			$message = '<div class="compare-message">' . sprintf(JText::_('OPENSHOP_ADD_TO_COMPARE_SUCCESS_MESSAGE'), $viewProductLink, $productInfo->product_name, $viewCompareLink) . '</div>';
			$json['success']['message'] =  '<h1>' . JText::_('OPENSHOP_PRODUCT_COMPARE') . '</h1>' . $message;
		}
		return $json;
	}
}