<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelFrontpage extends OpenShopModelProducts
{
    public function getProducts_hit(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . 'ON a.id = b.product_id')
                ->order('hits DESC');
        return $db->setQuery($query,0,10)->loadObjectList();
    }
    
    public function getprintProduct(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . 'ON a.id = b.product_id')
                ->join('INNER', $db->quoteName('#__openshop_productcategories','c') . 'ON a.id = c.product_id')
                ->where('c.category_id = 30')
                ->order('a.created_date DESC');
        return $db->setQuery($query,0,10)->loadObjectList();
    }
}