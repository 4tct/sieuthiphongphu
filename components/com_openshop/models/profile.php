<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelProfile extends OpenShopModelProducts {

    public function getProfile() {
        try {
            $user = JFactory::getSession()->get('user');
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('a.username,a.password,b.fullname,b.telephone,b.email,c.address,d.zone_name,e.district_name')
                    ->from($db->quoteName('#__users', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_customers', 'b') . ' ON a.id = b.customer_id')
                    ->join('INNER', $db->quoteName('#__openshop_addresses', 'c') . ' ON a.id = c.customer_id')
                    ->join('LEFT', $db->quoteName('#__openshop_zones', 'd') . ' ON d.id = c.zone_id')
                    ->join('LEFT', $db->quoteName('#__openshop_districts', 'e') . ' ON e.id = c.district_id')
                    ->where('a.id = ' . $user->id)
                    ->where('b.published = 1');
            $profile = $db->setQuery($query)->loadObject();

            //tài khoản quản trị
            if (!count($profile)) {
                $query = $db->getQuery(TRUE);
                $query->select('a.id,a.username,a.password,a.name as fullname')
                        ->from($db->quoteName('#__users', 'a'))
                        ->where('a.id = ' . $user->id);
                $profile = $db->setQuery($query)->loadObject();

                //them vào dịa chi
                $query->clear();
                $query->select('count(id)')
                        ->from($db->quoteName('#__openshop_addresses'))
                        ->where('customer_id = ' . $profile->id);
                //không tồn tại mới thêm
                if (!$db->setQuery($query)->loadResult()) {
                    $d_address = new stdClass();
                    $d_address->customer_id = $profile->id;
                    $data->created_date = date('Y-m-d H:m:s');
                    $db->insertObject('#__openshop_addresses', $d_address);
                }

                //them vào customer
                $query->clear();
                $query->select('count(id)')
                        ->from($db->quoteName('#__openshop_customers'))
                        ->where('customer_id = ' . $profile->id);
                //không tồn tại mới thêm
                if (!$db->setQuery($query)->loadResult()) {
                    $data = new stdClass();
                    $data->fullname = $profile->fullname;
                    $data->customer_id = $profile->id;
                    $data->published = '1';
                    $data->created_date = date('Y-m-d H:m:s');
                    $data->modified_date = '0000-00-00 00:00:00';
                    $db->insertObject('#__openshop_customers', $data);
                }
            }
            return $profile;
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
