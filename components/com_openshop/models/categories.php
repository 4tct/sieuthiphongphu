<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelCategories extends RADModelList {

    public function __construct($config = array()) {
        $config['translatable'] = true;
        $config['translatable_fields'] = array(
            'category_name',
            'category_alias',
            'category_page_title',
            'category_page_heading',
            'category_desc',
            'meta_key',
            'meta_desc');
        parent::__construct($config);
        $app = JFactory::getApplication();
        $listLength = OpenShopHelper::getConfigValue('catalog_limit');
        if (!$listLength) {
            $listLength = $app->getCfg('list_limit');
        }
        $this->state->insert('id', 'int', 0)
                ->insert('limit', 'int', $listLength);
        $request = OpenShopHelper::getRequestData();
        $this->state->setData($request);
        if ($app->input->getCmd('view') == 'categories') {
            $app->setUserState('limit', $this->state->limit);
        }
    }

    /**
     * Method to get categories data
     *
     * @access public
     * @return array
     */
    public function getData() {
        // Lets load the content if it doesn't already exist
        if (empty($this->data)) {
            $rows = parent::getData();
            $this->data = $rows;
        }
        return $this->data;
    }

    /**
     * Override BuildQueryWhere method
     * @see RADModelList::_buildQueryWhere()
     */
    protected function _buildQueryWhere(JDatabaseQuery $query) {
        parent::_buildQueryWhere($query);
        $query->where('a.category_parent_id=' . $this->state->id);
        //Check viewable of customer groups
        $user = JFactory::getUser();
        if ($user->get('id')) {
            $customer = new OpenShopCustomer();
            $customerGroupId = $customer->getCustomerGroupId();
        } else {
            $customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
        }
        if (!$customerGroupId)
            $customerGroupId = 0;
        $query->where('((a.category_customergroups = "") OR (a.category_customergroups IS NULL) OR (a.category_customergroups = "' . $customerGroupId . '") OR (a.category_customergroups LIKE "' . $customerGroupId . ',%") OR (a.category_customergroups LIKE "%,' . $customerGroupId . ',%") OR (a.category_customergroups LIKE "%,' . $customerGroupId . '"))');
        return $this;
    }

}
