<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelBanner extends OpenShopModelProducts {

    function getBanner() {
        $app = JFactory::getApplication()->input;
        $id = $app->getInt('id');
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_banners'))
                ->where('id = ' . $id);
        $rows = $db->setQuery($query)->loadObject();
        return $rows;
    }

    function getProducts($arr_product) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('a.*,b.product_name')
                    ->from($db->quoteName('#__openshop_products', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id')
                    ->where('a.id IN (' . $arr_product . ')');
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }

}
