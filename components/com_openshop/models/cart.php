<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelCart extends OpenShopModel
{
	/**
	 * Entity data
	 *
	 * @var array
	 */
	protected $cartData = null;
	
	/**
	 * 
	 * Total Data object array, each element is an price price in the cart 
	 * @var object array
	 */
	protected $totalData = null;
	
	/**
	 * 
	 * Final total price of the cart
	 * @var float
	 */
	protected $total = null;
	
	/**
	 * 
	 * Taxes of all elements in the cart
	 * @var array
	 */
	protected $taxes = null;
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->cartData		= null;
		$this->totalData	= null;
		$this->total		= null;
		$this->taxes		= null;
	}

	/**
	 * 
	 * Function to get Cart Data
	 */
	function getCartData()
	{
		$cart = new OpenShopCart();
		if (!$this->cartData)
		{
			$this->cartData = $cart->getCartData();
		}
		return $this->cartData;
	}
	
	/**
	 * 
	 * Function to get Costs
	 */
	function getCosts()
	{
		$totalData = array();
		$total = 0;
		$taxes = array();
		$this->getSubTotalCosts($totalData, $total, $taxes);
		$this->getVoucherCosts($totalData, $total, $taxes);
		$this->getShippingCosts($totalData, $total, $taxes);
		$this->getCouponCosts($totalData, $total, $taxes);
		$this->getTaxesCosts($totalData, $total, $taxes);
		$this->getTotalCosts($totalData, $total, $taxes);
		$this->totalData	= $totalData;
		$this->total		= $total;
		$this->taxes		= $taxes;
	}
	
	/**
	 * 
	 * Function to get Sub Total Costs
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	function getSubTotalCosts(&$totalData, &$total, &$taxes)
	{
		$cart = new OpenShopCart();
		$currency = new OpenShopCurrency();
		$total = $cart->getSubTotal();
		$totalData[] = array(
			'name'		=> 'sub_total',
			'title'		=> JText::_('OPENSHOP_SUB_TOTAL'),
			'text'		=> $currency->format(max(0, $total)),
			'value'		=> max(0, $total)
		);
		$taxes = $cart->getTaxes();
	}
	
	/**
	 * 
	 * Function to get Coupon Costs
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	function getCouponCosts(&$totalData, &$total, &$taxes)
	{
		$coupon = new OpenShopCoupon();
		$coupon->getCosts($totalData, $total, $taxes);
	}
	
	/**
	 *
	 * Function to get Voucher Costs
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	function getVoucherCosts(&$totalData, &$total, &$taxes)
	{
		$voucher = new OpenShopVoucher();
		$voucher->getCosts($totalData, $total, $taxes);
	}
	
	/**
	 * 
	 * Function to get Shipping Costs
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	function getShippingCosts(&$totalData, &$total, &$taxes)
	{
		$shipping = new OpenShopShipping();
		$shipping->getCosts($totalData, $total, $taxes);
	}
	
	/**
	 * 
	 * Function to get Taxes Costs
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	function getTaxesCosts(&$totalData, &$total, &$taxes)
	{
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		$tax->getCosts($totalData, $total, $taxes);
	}
	
	/**
	 * 
	 * Function to get Total Costs
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	function getTotalCosts(&$totalData, &$total, &$taxes)
	{
		$currency = new OpenShopCurrency();
		$totalData[] = array(
			'name'		=> 'total',
			'title'		=> JText::_('OPENSHOP_TOTAL'),
			'text'		=> $currency->format(max(0, $total)),
			'value'		=> max(0, $total)
		);
	}
	
	/**
	 * 
	 * Function to get Total Data
	 */
	public function getTotalData()
	{
		return $this->totalData;
	}
	
	/**
	 * 
	 * Function to get Total
	 */
	function getTotal()
	{
		return $this->total;
	}
	
	/**
	 * 
	 * Function to get Taxes
	 */
	function getTaxes()
	{
		return $this->taxes;
	}
}