<?php

/**
 * @version        1.4.2
 * @package        Joomla
 * @subpackage     OpenShop
 * @author         Giang Dinh Truong
 * @copyright      Copyright (C) 2012 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die();
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelCategory extends OpenShopModelProducts {

    protected function _buildQueryJoins(JDatabaseQuery $query) {
        parent::_buildQueryJoins($query);
        return $this;
    }

    protected function _buildQueryWhere(JDatabaseQuery $query) {
        parent::_buildQueryWhere($query);

        if (OpenShopHelper::getConfigValue('show_products_in_all_levels')) {
            $categoryIds = array_merge(array($this->state->id), OpenShopHelper::getAllChildCategories($this->state->id));
        } else {
            $categoryIds = array($this->state->id);
        }

        $db = $this->getDbo();
        $subQuery = $db->getQuery(true);
        $subQuery->select('pc.product_id FROM #__openshop_productcategories AS pc WHERE pc.category_id IN (' . implode(',', $categoryIds) . ')');
        $query->where('a.id IN (' . (string) $subQuery . ')');

        $query->where('a.delete_status = 0');
        $query->where('a.published = 1');
        
        return $this;
    }
    
    /**
     * Builds a generic ORDER BY clasue based on the model's state
     */
    protected function _buildQueryOrder(JDatabaseQuery $query) {
        $query->order('product_hot DESC, product_new DESC, created_date DESC');
    }

}
