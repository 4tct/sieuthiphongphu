<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelProducts extends RADModelList {

    public function __construct($config = array()) {
        $config['table'] = '#__openshop_products';
        $config['translatable'] = FALSE;
        $config['translatable_fields'] = array(
            'b.product_name',
            'b.product_alias',
            'b.product_desc',
            'b.product_short_desc',
            'b.product_page_title',
            'b.product_page_heading',
            'b.meta_key',
            'b.meta_desc');
        $config['search_fields'] = array(
            'b.product_name',
            'a.product_sku'
        );
        parent::__construct($config);
        $app = JFactory::getApplication();
        $request = OpenShopHelper::getRequestData();
        $name = $this->getName();
        $listLength = 0;
        if ($name == 'category') {
            $category = OpenShopHelper::getCategory((int) $request['id'], false);
            $listLength = (int) $category->products_per_page;
        }
        if (!$listLength) {
            $listLength = OpenShopHelper::getConfigValue('catalog_limit');
        }
        if (!$listLength) {
            $listLength = $app->getCfg('list_limit');
        }
        $limit = $app->getUserStateFromRequest('com_openshop.' . $name . '.limit', 'limit', $listLength, 'int');
        $this->state->insert('id', 'int', 0)
                ->insert('product_featured', 'int', 0)
                ->insert('limit', 'int', $limit)
                ->insert('sort_options', 'string', '');

        //Search filters
        if ($this->name == 'Search') {
//            $this->state->insert('min_price', 'string', '')
//                    ->insert('max_price', 'string', '')
//                    ->insert('min_weight', 'float', '')
//                    ->insert('max_weight', 'float', '')
//                    ->insert('same_weight_unit', 'int', '1')
//                    ->insert('min_length', 'float', '')
//                    ->insert('max_length', 'float', '')
//                    ->insert('min_width', 'float', '')
//                    ->insert('max_width', 'float', '')
//                    ->insert('min_height', 'float', '')
//                    ->insert('max_height', 'float', '')
//                    ->insert('same_length_unit', 'int', '1')
//                    ->insert('product_in_stock', 'int', 2)
//                    ->insert('category_ids', 'string', '')
//                    ->insert('manufacturer_ids', 'string', '')
//                    ->insert('brand_ids', 'string', '')
//                    ->insert('attribute_ids', 'string', '')
//                    ->insert('optionvalue_kichthuoc_ids', 'string', '')
//                    ->insert('optionvalue_ids', 'string', '')
//                    ->insert('optionvalue_kichthuoc_ids', 'string', '')
//                    ->insert('keyword', 'string', '');
        }
        if (!isset($request['sort_options'])) {
            $request['sort_options'] = OpenShopHelper::getConfigValue('default_sorting');
        }
        $this->state->setData($request);
        $app->setUserState('limit', $this->state->limit);
    }

    /**
     * Method to get categories data
     *
     * @access public
     * @return array
     */
    public function getData() {
        // Lets load the content if it doesn't already exist
        if (empty($this->data)) {
            $rows = parent::getData();
            $imageSizeFunction = OpenShopHelper::getConfigValue('list_image_size_function', 'resizeImage');
            if (JRequest::getVar('view') == 'search' && JRequest::getVar('layout') == 'ajax') {
                $imageListWidth = JRequest::getVar('image_width', 50);
                $imageListHeight = JRequest::getVar('image_height', 50);
            } else {
                $imageListWidth = OpenShopHelper::getConfigValue('image_list_width');
                $imageListHeight = OpenShopHelper::getConfigValue('image_list_height');
            }
            for ($i = 0; $n = count($rows), $i < $n; $i++) {
                $row = $rows[$i];
//                if ($row->product_image && JFile::exists(JPATH_ROOT . '/images/com_openshop/products/' . $row->product_image)) {
//                    $image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($row->product_image, JPATH_ROOT . '/images/com_openshop/products/', $imageListWidth, $imageListHeight));
//                } else {
//                    $image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', $imageListWidth, $imageListHeight));
//                }
//                $row->image = JUri::base(true) . '/images/com_openshop/products/resized/' . $image;
                $row->labels = OpenShopHelper::getProductLabels($row->id);
            }
            $this->data = $rows;
        }

        return $this->data;
    }

    /**
     * Builds LEFT JOINS clauses for the query
     */
    protected function _buildQueryJoins(JDatabaseQuery $query) {
        $query->innerJoin('#__openshop_productdetails AS b ON a.id = b.product_id');
        $sortOptions = $this->state->sort_options;
        if ($sortOptions == 'product_rates-ASC' || $sortOptions == 'product_rates-DESC' || $sortOptions == 'product_reviews-ASC' || $sortOptions == 'product_reviews-DESC') {
            $query->leftJoin('#__openshop_reviews AS r ON (a.id = r.product_id AND r.published = 1)');
        }

        return $this;
    }

    protected function _buildQueryWhere(JDatabaseQuery $query) {
        parent::_buildQueryWhere($query);
        if ($this->state->product_featured) {
            //$query->where('a.product_featured = 1');
        }
        //Check viewable of customer groups
        $user = JFactory::getUser();
        if ($user->get('id')) {
            $customer = new OpenShopCustomer();
            $customerGroupId = $customer->getCustomerGroupId();
        } else {
            $customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
        }
        if (!$customerGroupId)
            $customerGroupId = 0;
        //Check out of stock
        if (OpenShopHelper::getConfigValue('hide_out_of_stock_products')) {
            $query->where('a.product_quantity > 0');
        }
        return $this;
    }

    protected function _buildQueryOrder(JDatabaseQuery $query) {
        $allowedSortArr = array('a.ordering', 'b.product_name', 'a.product_sku', 'a.product_price', 'a.product_length', 'a.product_width', 'a.product_height', 'a.product_weight', 'a.product_quantity', 'b.product_short_desc', 'b.product_desc', 'product_rates', 'product_reviews');
        $allowedDirectArr = array('ASC', 'DESC');
        $sortOptions = $this->state->sort_options;
        if ($sortOptions != '') {
            $sortOptions = explode('-', $sortOptions);
            if (isset($sortOptions[0]) && in_array($sortOptions[0], $allowedSortArr)) {
                $sort = $sortOptions[0];
            } else {
                $sort = 'a.ordering';
            }
            if (isset($sortOptions[1]) && in_array($sortOptions[1], $allowedDirectArr)) {
                $direct = $sortOptions[1];
            } else {
                $direct = 'ASC';
            }

            $query->order($sort . ' ' . $direct)
                    ->order('a.ordering');
            return $this;
        } else {
            return parent::_buildQueryOrder($query);
        }
    }

    protected function _buildQueryColumns(JDatabaseQuery $query) {
        $query->select(array('a.*','b.product_name'));
        if ($this->translatable) {
            $query->select($this->translatableFields);
        }
        $sortOptions = $this->state->sort_options;
        if ($sortOptions == 'product_rates-ASC' || $sortOptions == 'product_rates-DESC') {
            $query->select('AVG(r.rating) AS product_rates');
        } elseif ($sortOptions == 'product_reviews-ASC' || $sortOptions == 'product_reviews-DESC') {
            $query->select('COUNT(r.id) AS product_reviews');
        }

        return $this;
    }

    protected function _buildQueryGroup(JDatabaseQuery $query) {
        $sortOptions = $this->state->sort_options;
        if ($sortOptions == 'product_rates-ASC' || $sortOptions == 'product_rates-DESC' || $sortOptions == 'product_reviews-ASC' || $sortOptions == 'product_reviews-DESC') {
            $query->group('a.id');
        }

        return $this;
    }

}
