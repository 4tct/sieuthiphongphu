<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelQuote extends OpenShopModel
{
	/**
	 * Entity data
	 *
	 * @var array
	 */
	protected $quoteData = null;
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->quoteData = null;
	}

	/**
	 * 
	 * Function to get Quote Data
	 */
	function getQuoteData()
	{
		$quote = new OpenShopQuote();
		if (!$this->quoteData)
		{
			$this->quoteData = $quote->getQuoteData();
		}
		return $this->quoteData;
	}
	
	/**
	 *
	 * Function to process quote
	 * @param array $data
	 * @return json array
	 */
	function processQuote($data)
	{
		$quote = new OpenShopQuote();
		$user = JFactory::getUser();
		$currency = new OpenShopCurrency();
		$data['currency_id'] = $currency->getCurrencyId();
		$data['currency_code'] = $currency->getCurrencyCode();
		$data['currency_exchanged_value'] = $currency->getExchangedValue();
		$session = JFactory::getSession();
		$json = array();
		// Validate products in the quote
		if (!$quote->hasProducts())
		{
			$json['return'] = JRoute::_(OpenShopRoute::getViewRoute('quote'));
		}
		if (!$json)
		{
			// Name validate
			if (strlen($data['name']) < 3 || strlen($data['name']) > 25)
			{
				$json['error']['name'] = JText::_('OPENSHOP_QUOTE_NAME_REQUIRED');
			}
			// Email validate
			if ((strlen($data['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $data['email']))
			{
				$json['error']['email'] = JText::_('OPENSHOP_QUOTE_EMAIL_REQUIRED');
			}
			// Message validate
			if (strlen($data['message']) < 25 || strlen($data['message']) > 1000)
			{
				$json['error']['message'] = JText::_('OPENSHOP_QUOTE_MESSAGE_REQUIRED');
			}
			if (OpenShopHelper::getConfigValue('enable_quote_captcha'))
			{
				$captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
				if ($captchaPlugin == 'recaptcha')
				{
					$input = JFactory::getApplication()->input;
					$res = JCaptcha::getInstance($captchaPlugin)->checkAnswer($input->post->get('recaptcha_response_field', '', 'string'));
					if (!$res)
					{
						$json['error']['captcha'] = JText::_('OPENSHOP_INVALID_CAPTCHA');
					}
				}
			}
			if (!$json)
			{
				// Store Quote
				$row = JTable::getInstance('OpenShop', 'Quote');
				$row->bind($data);
				$total = 0;
				foreach ($quote->getQuoteData() as $product)
				{
					$total += $product['total_price'];
				}
				$row->total = $total;
				$row->customer_id = $user->get('id');
				$row->created_date = JFactory::getDate()->toSql();
				$row->modified_date = JFactory::getDate()->toSql();
				$row->modified_by = 0;
				$row->checked_out = 0;
				$row->checked_out_time = '0000-00-00 00:00:00';
				$row->store();
				$quoteRow = $row;
				$quoteId = $row->id;
				$quote = new OpenShopQuote();
				// Store Quote Products, Quote Options
				foreach ($quote->getQuoteData() as $product)
				{
					// Quote Products
					$row = JTable::getInstance('OpenShop', 'Quoteproducts');
					$row->id = '';
					$row->quote_id = $quoteId;
					$row->product_id = $product['product_id'];
					$row->product_name = $product['product_name'];
					$row->product_sku = $product['product_sku'];
					$row->quantity = $product['quantity'];
					$row->price = $product['price'];
					$row->total_price = $product['total_price'];
					$row->store();
					$quoteProductId = $row->id;
					// Quote Options
					foreach ($product['option_data'] as $option)
					{
						$row = JTable::getInstance('OpenShop', 'Quoteoptions');
						$row->id = '';
						$row->quote_id = $quoteId;
						$row->quote_product_id = $quoteProductId;
						$row->product_option_id = $option['product_option_id'];
						$row->product_option_value_id = $option['product_option_value_id'];
						$row->option_name = $option['option_name'];
						$row->option_value = $option['option_value'];
						$row->option_type = $option['option_type'];
						$row->sku = $option['sku'];
						$row->store();
					}
				}
				//Send confirmation email here
				if (OpenShopHelper::getConfigValue('order_alert_mail'))
				{
					OpenShopHelper::sendQuoteEmails($quoteRow);
				}
				$json['success'] = JRoute::_(OpenShopRoute::getViewRoute('quote') . '&layout=complete');
				$quote->clear();
			}
		}
		return $json;
	}
}