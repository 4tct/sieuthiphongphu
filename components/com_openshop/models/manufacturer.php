<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__) . '/products.php';

class OpenShopModelManufacturer extends OpenShopModelProducts
{
	protected function _buildQueryJoins(JDatabaseQuery $query)
	{
		parent::_buildQueryJoins($query);
		$query->innerJoin('#__openshop_manufacturers AS pc ON (a.manufacturer_id = pc.id)');

		return $this;
	}

	protected function _buildQueryWhere(JDatabaseQuery $query)
	{
		parent::_buildQueryWhere($query);
		$query->where('pc.id = ' . $this->state->id);
		return $this;
	}
}