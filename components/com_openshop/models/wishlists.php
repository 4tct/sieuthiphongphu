<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopModelWishlists extends OpenShopModel {

    /**
     * 
     * Constructor
     * @since 1.5
     */
    public function __construct($config = array()) {
        parent::__construct();
    }

    public function getWishLists() {
        try {
            $session = JFactory::getSession();
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('b.*,c.product_name')
                    ->from($db->quoteName('#__openshop_wishlists', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_products', 'b') . ' ON a.product_id = b.id ')
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . ' ON c.product_id = b.id ')
                    ->where('customer_id = ' . $session->get('user')->id);
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }

}
