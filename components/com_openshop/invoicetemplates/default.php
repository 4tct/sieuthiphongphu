<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

$orderProducts = $this->order_products;
$orderTotals   = $this->order_total;
?>
<table width="100%">
	<tr>
		<td style="background-color: #CDDDDD; text-align: left">
			<?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
		</td>
		<td style="background-color: #CDDDDD; text-align: left">
			<?php echo JText::_('OPENSHOP_MODEL'); ?>
		</td>
		<td style="background-color: #CDDDDD; text-align: left">
			<?php echo JText::_('OPENSHOP_QUANTITY'); ?>
		</td>
		<td style="background-color: #CDDDDD; text-align: left">
			<?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?>
		</td>
		<td style="background-color: #CDDDDD; text-align: left">
			<?php echo JText::_('OPENSHOP_TOTAL'); ?>
		</td>
	</tr>
	<?php 
	foreach ($orderProducts as $product)
	{
		$options = $product->options;
		?>
		<tr>
			<td>
				<?php
				echo '<b>' . $product->product_name . '</b>';
				for ($i = 0; $n = count($options), $i < $n; $i++)
				{
					echo '<br />- ' . $options[$i]->option_name . ': ' . $options[$i]->option_value . (isset($options[$i]->sku) && $options[$i]->sku != '' ? ' (' . $options[$i]->sku . ')' : '');
				}
				?>
			</td>
			<td>
				<?php echo $product->product_sku; ?>
			</td>
			<td>
				<?php echo $product->quantity; ?>
			</td>
			<td>
				<?php echo $product->price; ?>
			</td>
			<td>
				<?php echo $product->total_price; ?>
			</td>
		</tr>
		<?php 
	}
	foreach ($orderTotals as $orderTotal)
	{
		?>
		<tr>
			<td colspan="4" style="text-align: right;">
				<?php echo $orderTotal->title; ?>:
			</td>
			<td>
				<strong><?php echo $orderTotal->text; ?></strong>
			</td>
		</tr>
		<?php
	}
	?>
</table>