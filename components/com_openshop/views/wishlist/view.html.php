<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewWishlist extends OpenShopView
{		
	function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		$currency = new OpenShopCurrency();
		$user = JFactory::getUser();
		if (!$user->get('id'))
		{
			$mainframe->enqueueMessage(JText::_('OPENSHOP_YOU_MUST_LOGIN_TO_VIEW_WISHLIST'), 'Notice');
			$mainframe->redirect('index.php?option=com_users&view=login&return=' . base64_encode('index.php?option=com_openshop&view=wishlist'));
		}
		else
		{
			$document = JFactory::getDocument();
			$app		= JFactory::getApplication();
			$title = JText::_('OPENSHOP_WISHLIST');
			// Set title of the page
			$siteNamePosition = $app->getCfg('sitename_pagetitles');
			if($siteNamePosition == 1)
			{
				$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
			}
			elseif ($siteNamePosition == 2)
			{
				$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
			}
			$document->setTitle($title);
			$document->addStyleSheet(JUri::base(true).'/components/com_openshop/assets/colorbox/colorbox.css');
			$session = JFactory::getSession();
			$wishlist = ($session->get('wishlist') ? $session->get('wishlist') : array());
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			if (count($wishlist))
			{
				//Save wishlist from session
				foreach ($wishlist as $productId)
				{
					$query->clear();
					$query->select('COUNT(*)')
						->from('#__openshop_wishlists')
						->where('customer_id = ' . intval($user->get('id')))
						->where('product_id = ' . intval($productId));
					$db->setQuery($query);
					if (!$db->loadResult())
					{
						$row = JTable::getInstance('OpenShop', 'Wishlist');
						$row->customer_id = $user->get('id');
						$row->product_id = $productId;
						$row->store();
					}
				}
				$session->clear('wishlist');
			}
			$query->clear();
			$query->select('a.*, b.product_name')
				->from('#__openshop_products AS a')
				->innerJoin('#__openshop_wishlists AS w ON (a.id = w.product_id)')
				->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
				->where('a.published = 1')
				->where('w.customer_id = ' . intval($user->get('id')))
				->where('b.language = "' . JFactory::getLanguage()->getTag() . '"');
			$db->setQuery($query);
			$products = $db->loadObjectList();
			for ($i = 0; $n = count($products), $i < $n; $i++)
			{
				// Resize wishlist images
				$imageSizeFunction = OpenShopHelper::getConfigValue('wishlist_image_size_function', 'resizeImage');
				if ($products[$i]->product_image && JFile::exists(JPATH_ROOT.'/images/com_openshop/products/'.$products[$i]->product_image))
				{
					$image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($products[$i]->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_wishlist_width'), OpenShopHelper::getConfigValue('image_wishlist_height')));
				}
				else
				{
					$image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_wishlist_width'), OpenShopHelper::getConfigValue('image_wishlist_height')));
				}
				$products[$i]->image = JUri::base(true) . '/images/com_openshop/products/resized/' . $image;
				// Product availability
				if ($products[$i]->product_quantity <= 0)
				{
					$availability = OpenShopHelper::getStockStatusName($products[$i]->product_stock_status_id ? $products[$i]->product_stock_status_id : OpenShopHelper::getConfigValue('stock_status_id'), JFactory::getLanguage()->getTag());
				}
				elseif (OpenShopHelper::getConfigValue('stock_display'))
				{
					$availability = $products[$i]->product_quantity;
				}
				else
				{
					$availability = JText::_('OPENSHOP_IN_STOCK');
				}
				$products[$i]->availability = $availability;
				// Price
				$productPriceArray = OpenShopHelper::getProductPriceArray($products[$i]->id, $products[$i]->product_price);
				if ($productPriceArray['salePrice'])
				{
					$basePrice = $currency->format($tax->calculate($productPriceArray['basePrice'], $productInfo->product_taxclass_id, OpenShopHelper::getConfigValue('tax')));
					$salePrice = $currency->format($tax->calculate($productPriceArray['salePrice'], $productInfo->product_taxclass_id, OpenShopHelper::getConfigValue('tax')));
				}
				else
				{
					$basePrice = $currency->format($tax->calculate($productPriceArray['basePrice'], $productInfo->product_taxclass_id, OpenShopHelper::getConfigValue('tax')));
					$salePrice = 0;
				}
				$products[$i]->base_price = $basePrice;
				$products[$i]->sale_price = $salePrice;
			}
			if ($session->get('success'))
			{
			$this->success = $session->get('success');
			$session->clear('success');
			}
			$this->products = $products;
			$this->tax = $tax;
			$this->currency = $currency;
		}
		parent::display($tpl);
	}
}