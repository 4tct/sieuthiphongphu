<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCompare extends OpenShopView
{		
	function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		$currency = new OpenShopCurrency();
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::base(true).'/components/com_openshop/assets/colorbox/colorbox.css');
		$session = JFactory::getSession();
		$compare = $session->get('compare');
		$products = array();
		$attributeGroups = OpenShopHelper::getAttributeGroups(JFactory::getLanguage()->getTag());
		$visibleAttributeGroups = array();
		$app		= JFactory::getApplication();
		$title = JText::_('OPENSHOP_COMPARE');
		// Set title of the page
		$siteNamePosition = $app->getCfg('sitename_pagetitles');
		if($siteNamePosition == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($siteNamePosition == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$document->setTitle($title);
		if (count($compare))
		{
			foreach ($compare as $productId)
			{
				$productInfo = OpenShopHelper::getProduct($productId);
				if (is_object($productInfo))
				{
					// Image
					$imageSizeFunction = OpenShopHelper::getConfigValue('compare_image_size_function', 'resizeImage');
					if ($productInfo->product_image && JFile::exists(JPATH_ROOT.'/media/com_openshop/products/' . $productInfo->product_image))
					{
						$image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($productInfo->product_image, JPATH_ROOT . '/media/com_openshop/products/', OpenShopHelper::getConfigValue('image_compare_width'), OpenShopHelper::getConfigValue('image_compare_height')));
					}
					else
					{
						$image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array('no-image.png', JPATH_ROOT . '/media/com_openshop/products/', OpenShopHelper::getConfigValue('image_compare_width'), OpenShopHelper::getConfigValue('image_compare_height')));
					}
					$image = JUri::base(true) . '/media/com_openshop/products/resized/' . $image;
					// Availability
					if ($productInfo->product_quantity <= 0)
					{
						$availability = OpenShopHelper::getStockStatusName($productInfo->product_stock_status_id ? $productInfo->product_stock_status_id : OpenShopHelper::getConfigValue('stock_status_id'), JFactory::getLanguage()->getTag());
					}
					elseif (OpenShopHelper::getConfigValue('stock_display'))
					{
						$availability = $productInfo->product_quantity;
					}
					else
					{
						$availability = JText::_('OPENSHOP_IN_STOCK');
					}
					// Manufacturer
					$manufacturer = OpenShopHelper::getProductManufacturer($productId, JFactory::getLanguage()->getTag());
					// Price
					$productPriceArray = OpenShopHelper::getProductPriceArray($productId, $productInfo->product_price);
					if ($productPriceArray['salePrice'])
					{
						$basePrice = $currency->format($tax->calculate($productPriceArray['basePrice'], $productInfo->product_taxclass_id, OpenShopHelper::getConfigValue('tax')));
						$salePrice = $currency->format($tax->calculate($productPriceArray['salePrice'], $productInfo->product_taxclass_id, OpenShopHelper::getConfigValue('tax')));
					}
					else
					{
						$basePrice = $currency->format($tax->calculate($productPriceArray['basePrice'], $productInfo->product_taxclass_id, OpenShopHelper::getConfigValue('tax')));
						$salePrice = 0;
					}
					// Atrributes
					$productAttributes = array();
					for ($j = 0; $m = count($attributeGroups), $j < $m; $j++)
					{
						$attributes = OpenShopHelper::getAttributes($productId, $attributeGroups[$j]->id, JFactory::getLanguage()->getTag());
						if (count($attributes))
						{
							$visibleAttributeGroups[$attributeGroups[$j]->id]['id'] = $attributeGroups[$j]->id;
							$visibleAttributeGroups[$attributeGroups[$j]->id]['attributegroup_name'] = $attributeGroups[$j]->attributegroup_name;
							foreach ($attributes as $attribute)
							{
								if (isset($visibleAttributeGroups[$attributeGroups[$j]->id]['attribute_name']))
								{
									if (!in_array($attribute->attribute_name, $visibleAttributeGroups[$attributeGroups[$j]->id]['attribute_name']))
									{
										$visibleAttributeGroups[$attributeGroups[$j]->id]['attribute_name'][] = $attribute->attribute_name;
									}
								}
								else
								{
									$visibleAttributeGroups[$attributeGroups[$j]->id]['attribute_name'][] = $attribute->attribute_name;
								}
								$productAttributes[$attributeGroups[$j]->id]['value'][$attribute->attribute_name] = $attribute->value;
							}
						}
					}
					$products[$productId] = array(
						'product_id'			=> $productId,
						'product_sku'			=> $productInfo->product_sku,
						'product_name'			=> $productInfo->product_name,
						'product_short_desc'	=> $productInfo->product_short_desc,
						'image'					=> $image,
						'product_desc'			=> substr(strip_tags(html_entity_decode($productInfo->product_desc, ENT_QUOTES, 'UTF-8')), 0, 200) . '...',
						'base_price'			=> $basePrice,
						'sale_price'			=> $salePrice,
						'product_call_for_price'=> $productInfo->product_call_for_price,
						'availability'			=> $availability,
						'rating'				=> OpenShopHelper::getProductRating($productId),
						'num_reviews'			=> count(OpenShopHelper::getProductReviews($productId)),
						'weight'				=> number_format($productInfo->product_weight, 2).OpenShopHelper::getWeightUnit($productInfo->product_weight_id, JFactory::getLanguage()->getTag()),
						'length'				=> number_format($productInfo->product_length, 2).OpenShopHelper::getLengthUnit($productInfo->product_length_id, JFactory::getLanguage()->getTag()),
						'width'					=> number_format($productInfo->product_width, 2).OpenShopHelper::getLengthUnit($productInfo->product_length_id, JFactory::getLanguage()->getTag()),
						'height'				=> number_format($productInfo->product_height, 2).OpenShopHelper::getLengthUnit($productInfo->product_length_id, JFactory::getLanguage()->getTag()),
						'manufacturer'			=> isset($manufacturer->manufacturer_name) ?  $manufacturer->manufacturer_name : '',
						'attributes'			=> $productAttributes
					);
				}
			}
		}
		if ($session->get('success'))
		{
			$this->success = $session->get('success');
			$session->clear('success');
		}
		$this->visibleAttributeGroups = $visibleAttributeGroups;
		$this->products = $products;
		parent::display($tpl);
	}
}