<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCheckout extends OpenShopView
{		
	function display($tpl = null)
	{
		JHtml::_('behavior.calendar');
		JHtml::_('behavior.tooltip');
		$mainframe = JFactory::getApplication();
		$session = JFactory::getSession();
		if (OpenShopHelper::getConfigValue('catalog_mode'))
		{
			$session = JFactory::getSession();
			$session->set('warning', JText::_('OPENSHOP_CATALOG_MODE_ON'));
			$mainframe->redirect(JRoute::_(OpenShopRoute::getViewRoute('categories')));
		}
		else
		{
			$app		= JFactory::getApplication();
			$menu		= $app->getMenu();
			$menuItem = $menu->getActive();
			if ($menuItem)
			{
				if (isset($menuItem->query['view']) && ($menuItem->query['view']== 'frontpage'))
				{
					$pathway = $app->getPathway();
					$pathUrl = OpenShopRoute::getViewRoute('frontpage');
					$pathway->addItem(JText::_('OPENSHOP_CHECKOUT'), $pathUrl);
				}
			}
			if ($this->getLayout() == 'complete')
			{
				$this->_displayComplete($tpl);
			}
			elseif ($this->getLayout() == 'cancel')
			{
				$this->_displayCancel($tpl);
			}
			else
			{
				$cart = new OpenShopCart();
				// Check if cart has products or not
				if (!$cart->hasProducts() || $cart->getStockWarning() != '' || $cart->getMinSubTotalWarning() != '' || $cart->getMinQuantityWarning() != '' || $cart->getMinProductQuantityWarning() != '' || $cart->getMaxProductQuantityWarning() != '')
				{
					$mainframe->redirect(JRoute::_(OpenShopRoute::getViewRoute('cart')));
				}
				$document = JFactory::getDocument();
				$document->addStyleSheet(JUri::base(true).'/components/com_openshop/assets/colorbox/colorbox.css');
				$title = JText::_('OPENSHOP_CHECKOUT');
				// Set title of the page
				$siteNamePosition = $app->getCfg('sitename_pagetitles');
				if($siteNamePosition == 1)
				{
					$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
				}
				elseif ($siteNamePosition == 2)
				{
					$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
				}
				$document->setTitle($title);
				$user = JFactory::getUser();
				$this->user = $user;
				$this->shipping_required = $cart->hasShipping();
				if (OpenShopHelper::getConfigValue('enable_checkout_captcha'))
				{
					$captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
					if ($captchaPlugin == 'recaptcha')
					{
						JCaptcha::getInstance($captchaPlugin)->initialise('dynamic_recaptcha_1');
					}
				}
				parent::display($tpl);
			}	
		}
	}
	
	/**
	 * 
	 * Function to display complete layout
	 * @param string $tpl
	 */
	function _displayComplete($tpl)
	{
		$cart = new OpenShopCart();
		$session = JFactory::getSession();
		$orderId = $session->get('order_id');
		$orderInfor = OpenShopHelper::getOrder($orderId);
		if (is_object($orderInfor))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$tax = new OpenShopTax(OpenShopHelper::getConfig());
			$currency = new OpenShopCurrency();
			
			$orderProducts = OpenShopHelper::getOrderProducts($orderId);
			for ($i = 0; $n = count($orderProducts), $i < $n; $i++)
			{
				$query->clear();
				$query->select('*')
					->from('#__openshop_orderoptions')
					->where('order_product_id = ' . intval($orderProducts[$i]->id));
				$db->setQuery($query);
				$orderProducts[$i]->options = $db->loadObjectList();
			}
			$orderTotals   = OpenShopHelper::getOrderTotals($orderId);
			//Payment custom fields here
			$form = new RADForm(OpenShopHelper::getFormFields('B'));
			$this->paymentFields = $form->getFields();
			//Shipping custom fields here
			$form = new RADForm(OpenShopHelper::getFormFields('S'));
			$this->shippingFields = $form->getFields();
			$this->orderProducts = $orderProducts;
			$this->orderTotals   = $orderTotals;
			$this->tax		  = $tax;
			$this->currency     = $currency;
			// Clear cart and session
			if ($session->get('order_id'))
			{
				$cart->clear();
				$session->clear('shipping_method');
				$session->clear('shipping_methods');
				$session->clear('payment_method');
				$session->clear('guest');
				$session->clear('comment');
				$session->clear('order_id');
				$session->clear('coupon_code');
				$session->clear('voucher_code');
			}
		}
		$this->orderInfor   = $orderInfor;
		if (OpenShopHelper::getConfigValue('completed_url') != '')
		{
			JFactory::getApplication()->redirect(OpenShopHelper::getConfigValue('completed_url'));
		}
		else 
		{
			parent::display($tpl);
		}
	}
	
	
	/**
	 *
	 * Function to display cancel layout
	 * @param string $tpl
	 */
	function _displayCancel($tpl)
	{
		parent::display($tpl);
	}
}