<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCategory extends OpenShopView {

    function display($tpl = null) {
        $app = JFactory::getApplication();
        $session = JFactory::getSession();
        $model = $this->getModel();
        $state = $model->getState();
        $category = OpenShopHelper::getCategory($state->id, true, true);
        if (!is_object($category)) {
            // Requested category does not existed.			
            $session->set('warning', JText::_('OPENSHOP_CATEGORY_DOES_NOT_EXIST'));
            $app->redirect(JRoute::_(OpenShopRoute::getViewRoute('categories')));
        } else {
            $document = JFactory::getDocument();
            //Handle breadcrump			
            $menu = $app->getMenu();
            $menuItem = $menu->getActive();
            if ($menuItem) {
                if (isset($menuItem->query['view']) && ($menuItem->query['view'] == 'frontpage' || $menuItem->query['view'] == 'categories' || $menuItem->query['view'] == 'category')) {
                    $parentId = isset($menuItem->query['id']) ? (int) $menuItem->query['id'] : '0';
                    if ($category->id) {
                        $pathway = $app->getPathway();
                        $paths = OpenShopHelper::getCategoriesBreadcrumb($category->id, $parentId);
                        for ($i = count($paths) - 1; $i >= 0; $i--) {
                            $path = $paths[$i];
                            $pathUrl = OpenShopRoute::getCategoryRoute($path->id);
                            $pathway->addItem($path->category_name, $pathUrl);
                        }
                    }
                }
            }
            // Update hits for category
            OpenShopHelper::updateHits($category->id, 'categories');
            // Set title of the page
            $siteNamePosition = $app->getCfg('sitename_pagetitles');
            $categoryPageTitle = $category->category_page_title != '' ? $category->category_page_title : $category->category_name;
            if ($siteNamePosition == 0) {
                $title = $categoryPageTitle;
            } elseif ($siteNamePosition == 1) {
                $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $categoryPageTitle);
            } else {
                $title = JText::sprintf('JPAGETITLE', $categoryPageTitle, $app->getCfg('sitename'));
            }
            $document->setTitle($title);
            // Set metakey and metadesc
            $metaKey = $category->meta_key;
            $metaDesc = $category->meta_desc;
            if ($metaKey) {
                $document->setMetaData('keywords', $metaKey);
            }
            if ($metaDesc) {
                $document->setMetaData('description', $metaDesc);
            }
            $products = $model->getData();
            $pagination = $model->getPagination();
            //Get subcategories
            JLoader::register('OpenShopModelCategories', JPATH_ROOT . '/components/com_openshop/models/categories.php');
            $subCategories = RADModel::getInstance('Categories', 'OpenShopModel', array('remember_states' => false))
                    ->limitstart(0)
                    ->limit(0)
                    ->filter_order('a.ordering')
                    ->id($state->id)
                    ->getData();
            //Sort options
            $sortOptions = OpenShopHelper::getConfigValue('sort_options');
            $sortOptions = explode(',', $sortOptions);
            $sortValues = array(
                'a.ordering-ASC',
                'a.ordering-DESC',
                'b.product_name-ASC',
                'b.product_name-DESC',
                'a.product_sku-ASC',
                'a.product_sku-DESC',
                'a.product_price-ASC',
                'a.product_price-DESC',
                'a.product_length-ASC',
                'a.product_length-DESC',
                'a.product_width-ASC',
                'a.product_width-DESC',
                'a.product_height-ASC',
                'a.product_height-DESC',
                'a.product_weight-ASC',
                'a.product_weight-DESC',
                'a.product_quantity-ASC',
                'a.product_quantity-DESC',
                'b.product_short_desc-ASC',
                'b.product_short_desc-DESC',
                'b.product_desc-ASC',
                'b.product_desc-DESC',
                'product_rates-ASC',
                'product_rates-DESC',
                'product_reviews-ASC',
                'product_reviews-DESC'
            );
            $sortTexts = array(
                JText::_('OPENSHOP_SORTING_PRODUCT_ORDERING_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_ORDERING_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_NAME_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_NAME_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_SKU_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_SKU_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_PRICE_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_PRICE_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_LENGTH_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_LENGTH_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_WIDTH_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_WIDTH_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_HEIGHT_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_HEIGHT_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_WEIGHT_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_WEIGHT_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_QUANTITY_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_QUANTITY_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_SHORT_DESC_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_SHORT_DESC_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_DESC_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_DESC_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_RATES_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_RATES_DESC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_REVIEWS_ASC'),
                JText::_('OPENSHOP_SORTING_PRODUCT_REVIEWS_DESC')
            );
            $options = array();
            for ($i = 0; $i < count($sortValues); $i++) {
                if (in_array($sortValues[$i], $sortOptions)) {
                    $options[] = JHtml::_('select.option', $sortValues[$i], $sortTexts[$i]);
                }
            }
            if (count($options) > 1) {
                $this->sort_options = JHtml::_('select.genericlist', $options, 'sort_options', 'class="inputbox input-xlarge" onchange="this.form.submit();" ', 'value', 'text', $state->sort_options ? $state->sort_options : OpenShopHelper::getConfigValue('default_sorting'));
            } else {
                $this->sort_options = '';
            }
            $app->setUserState('sort_options', $state->sort_options ? $state->sort_options : OpenShopHelper::getConfigValue('default_sorting'));
            $app->setUserState('from_view', 'category');
            // Store session for Continue Shopping Url				
            $session->set('continue_shopping_url', JUri::getInstance()->toString());
            if ($state->sort_options) {
//                $pagination->setAdditionalUrlParam('sort_options', $state->sort_options);
            }
            $tax = new OpenShopTax(OpenShopHelper::getConfig());
            $currency = new OpenShopCurrency();
            $this->category = $category;
            $this->subCategories = $subCategories;
            $this->subCategoriesPerRow = OpenShopHelper::getConfigValue('items_per_row', 3);
            $this->products = $products;
            $this->pagination = $pagination;
            $this->tax = $tax;
            $this->currency = $currency;
            //Added by tuanpn, to use share common layout
            $productsPerRow = $category->products_per_row;
            if (!$productsPerRow) {
                $productsPerRow = OpenShopHelper::getConfigValue('items_per_row', 3);
            }
            if ($pagination->limitstart) {
                $this->actionUrl = JRoute::_(OpenShopRoute::getCategoryRoute($category->id) . '&limitstart=' . $pagination->limitstart);
            } else {
                $this->actionUrl = JRoute::_(OpenShopRoute::getCategoryRoute($category->id));
            }
            $this->productsPerRow = $productsPerRow;
            $this->categoriesNavigation = OpenShopHelper::getCategoriesNavigation($category->id);
            parent::display($tpl);
        }
    }

}
