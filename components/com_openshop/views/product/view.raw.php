<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.0
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 *
 * @package Joomla
 * @subpackage OpenShop
 * @since 1.5
 */
class OpenShopViewProduct extends OpenShopView
{

	function display($tpl = null)
	{
		$item = $this->get('Data');
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		if (JRequest::getVar('options'))
		{
			$options = array_filter(JRequest::getVar('options'));
		}
		else
		{
			$options = array();
		}
		$optionPrice = 0;
		foreach ($options as $productOptionId => $optionValue)
		{
			$query->clear();
			$query->select('po.id, po.option_id, o.option_type')
				->from('#__openshop_productoptions AS po')
				->innerJoin('#__openshop_options AS o ON (po.option_id = o.id)')
				->where('po.id = ' . intval($productOptionId))
				->where('po.product_id = ' . intval($item->id));
			$db->setQuery($query);
			$optionRow = $db->loadObject();
			if (is_object($optionRow))
			{
				if ($optionRow->option_type == 'Select' || $optionRow->option_type == 'Radio')
				{
					$query->clear();
					$query->select('price, price_sign')
						->from('#__openshop_productoptionvalues')
						->where('id = ' . intval($optionValue))
						->where('product_option_id = ' . intval($productOptionId));
					$db->setQuery($query);
					$optionValueRow = $db->loadObject();
					if (is_object($optionValueRow))
					{
						//Calculate option price
						if ($optionValueRow->price_sign == '+')
						{
							$optionPrice += $optionValueRow->price;
						}
						elseif ($optionValueRow->price_sign == '-')
						{
							$optionPrice -= $optionValueRow->price;
						}
					}
				}
				elseif ($optionRow->option_type == 'Checkbox')
				{
					foreach ($optionValue as $productOptionValueId)
					{
						$query->clear();
						$query->select('price, price_sign')
							->from('#__openshop_productoptionvalues')
							->where('product_option_id = ' . intval($productOptionId))
							->where('id = ' . intval($productOptionValueId));
						$db->setQuery($query);
						$optionValueRow = $db->loadObject();
						if (is_object($optionValueRow))
						{
							//Calculate option price
							if ($optionValueRow->price_sign == '+')
							{
								$optionPrice += $optionValueRow->price;
							}
							elseif ($optionValueRow->price_sign == '-')
							{
								$optionPrice -= $optionValueRow->price;
							}
						}
					}
				}elseif ($optionRow->option_type == 'Text' || $optionRow->option_type == 'Textarea'){
				    $query->clear()
    				    ->select('price, price_sign')
    				    ->from('#__openshop_productoptionvalues')
    				    ->where('option_id = '. intval($optionRow->option_id))    				    
    				    ->where('product_option_id = ' . intval($productOptionId))
    				    ->where('product_id = '.intval($item->id));
				    $db->setQuery($query);
				    $optionValueRow = $db->loadObject();
				    if (is_object($optionValueRow))
				    {
				        //Calculate option price
				        if ($optionValueRow->price_sign == '+')
				        {
				            $optionPrice += $optionValueRow->price * strlen($optionValue);
				            
				        }
				        elseif ($optionValueRow->price_sign == '-')
				        {
				            $optionPrice -= $optionValueRow->price * strlen($optionValue);
				        }
				    }
				}
			}
		}
		$item->product_price = $item->product_price + $optionPrice;
		
		$this->tax = $tax;
		$this->item = $item;
		$this->option_price = $optionPrice;
		$this->currency = new OpenShopCurrency();
		
		parent::display($tpl);
	}
}