<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewProduct extends OpenShopView {

    /**
     * Display function
     * @see JView::display()
     */
    function display($tpl = null) {
        $mainframe = JFactory::getApplication();
        $session = JFactory::getSession();
        $document = JFactory::getDocument();
        $item = $this->get('Data');
        $this->currency = new OpenShopCurrency();
        if (!is_object($item)) {
            // Requested product does not existed.
            $session->set('warning', JText::_('OPENSHOP_PRODUCT_DOES_NOT_EXIST'));
            $mainframe->redirect(JRoute::_(OpenShopRoute::getViewRoute('categories')));
        } else {
            $document->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/colorbox/colorbox.css');
            if (OpenShopHelper::getConfigValue('view_image') == 'zoom') {
                $document->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/jquery.jqzoom.css');
            }
            $document->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/labels.css');
            $productId = JRequest::getInt('id');
            //Set session for viewed products
            $viewedProductIds = $session->get('viewed_product_ids');
            if (!count($viewedProductIds)) {
                $viewedProductIds = array();
            }
            if (!in_array($productId, $viewedProductIds)) {
                $viewedProductIds[] = $productId;
            }
            $session->set('viewed_product_ids', $viewedProductIds);
            // Handle breadcrumb
            $db = JFactory::getDbo();
            $categoryId = JRequest::getVar('catid');
            if (!$categoryId) {
                $query = $db->getQuery(true);
                $query->select('a.id')
                        ->from('#__openshop_categories AS a')
                        ->innerJoin('#__openshop_productcategories AS b ON a.id = b.category_id')
                        ->where('b.product_id = ' . (int) $productId);
                $db->setQuery($query);
                $categoryId = (int) $db->loadResult();
            }
            $app = JFactory::getApplication();
            $menu = $app->getMenu();
            $menuItem = $menu->getActive();
            if ($menuItem) {
                if (isset($menuItem->query['view']) && ($menuItem->query['view'] == 'frontpage' || $menuItem->query['view'] == 'categories' || $menuItem->query['view'] == 'category')) {
                    $parentId = isset($menuItem->query['id']) ? (int) $menuItem->query['id'] : '0';
                    if ($categoryId) {
                        $pathway = $app->getPathway();
                        $paths = OpenShopHelper::getCategoriesBreadcrumb($categoryId, $parentId);
                        for ($i = count($paths) - 1; $i >= 0; $i--) {
                            $category = $paths[$i];
                            $pathUrl = OpenShopRoute::getCategoryRoute($category->id);
                            $pathway->addItem($category->category_name, $pathUrl);
                        }
                        $pathway->addItem($item->product_name);
                    }
                }
            }
            // Update hits for product
            OpenShopHelper::updateHits($productId, 'products');

            // Set title of the page
            $siteNamePosition = $app->getCfg('sitename_pagetitles');
            $productPageTitle = $item->product_page_title != '' ? $item->product_page_title : $item->product_name;
            if ($siteNamePosition == 0) {
                $title = $productPageTitle;
            } elseif ($siteNamePosition == 1) {
                $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $productPageTitle);
            } else {
                $title = JText::sprintf('JPAGETITLE', $productPageTitle, $app->getCfg('sitename'));
            }
            $document->setTitle($title);
            $additionalImageSizeFunction = OpenShopHelper::getConfigValue('additional_image_size_function', 'resizeImage');
            $thumbImageSizeFunction = OpenShopHelper::getConfigValue('thumb_image_size_function', 'resizeImage');
            $popupImageSizeFunction = OpenShopHelper::getConfigValue('popup_image_size_function', 'resizeImage');
            $relatedImageSizeFunction = OpenShopHelper::getConfigValue('related_image_size_function', 'resizeImage');
            // Main image resize
            if ($item->product_image && JFile::exists(JPATH_ROOT . '/images/com_openshop/products/' . $item->product_image)) {
                $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array($item->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
                $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array($item->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
                $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array($item->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
            } else {
                $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
                $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
                $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
            }

            $item->small_thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $smallThumbImage;
            $item->thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $thumbImage;
            $item->thumb_image = JUri::base(true) . '/images/com_openshop/products/' . $item->product_image;
            $item->popup_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $popupImage;
            // Set metakey and metadesc
            $metaKey = $item->meta_key;
            $metaDesc = $item->meta_desc;
            if ($metaKey) {
                $document->setMetaData('keywords', $metaKey);
            }
            if ($metaDesc) {
                $document->setMetaData('description', $metaDesc);
            }
            // Product availability
            if ($item->product_quantity <= 0) {
                $nullDate = $db->getNullDate();
                if ($item->product_available_date != $nullDate) {
                    $this->product_available_date = JHtml::date($item->product_available_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y'));
                }
                $availability = OpenShopHelper::getStockStatusName($item->product_stock_status_id ? $item->product_stock_status_id : OpenShopHelper::getConfigValue('stock_status_id'), JFactory::getLanguage()->getTag());
            } elseif (OpenShopHelper::getConfigValue('stock_display')) {
                $availability = $item->product_quantity;
            } else {
                $availability = JText::_('OPENSHOP_IN_STOCK');
            }
            $item->availability = $availability;
            // Product tags
            $productTags = OpenShopHelper::getProductTags($item->id);
            $this->productTags = $productTags;
            $item->product_desc = JHtml::_('content.prepare', $item->product_desc);
            if ($item->tab1_title != '' && $item->tab1_content != '')
                $item->tab1_content = JHtml::_('content.prepare', $item->tab1_content);
            if ($item->tab2_title != '' && $item->tab2_content != '')
                $item->tab2_content = JHtml::_('content.prepare', $item->tab2_content);
            if ($item->tab3_title != '' && $item->tab3_content != '')
                $item->tab3_content = JHtml::_('content.prepare', $item->tab3_content);
            if ($item->tab4_title != '' && $item->tab4_content != '')
                $item->tab4_content = JHtml::_('content.prepare', $item->tab4_content);
            if ($item->tab5_title != '' && $item->tab5_content != '')
                $item->tab5_content = JHtml::_('content.prepare', $item->tab5_content);
            // Get information related to this current product
            $productImages = OpenShopHelper::getProductImages($productId);
            // Additional images resize
            for ($i = 0; $n = count($productImages), $i < $n; $i++) {
                if ($productImages[$i]->image && JFile::exists(JPATH_ROOT . '/images/com_openshop/products/' . $productImages[$i]->image)) {
                    $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array($productImages[$i]->image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
                    $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array($productImages[$i]->image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
                    $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array($productImages[$i]->image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
                } else {
                    $smallThumbImage = call_user_func_array(array('OpenShopHelper', $additionalImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_additional_width'), OpenShopHelper::getConfigValue('image_additional_height')));
                    $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
                    $popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
                }
                $productImages[$i]->small_thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $smallThumbImage;
                $productImages[$i]->thumb_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $thumbImage;
                $productImages[$i]->popup_image = JUri::base(true) . '/images/com_openshop/products/resized/' . $popupImage;
            }
            $discountPrices = OpenShopHelper::getDiscountPrices($productId);
            $productOptions = OpenShopHelper::getProductOptions($productId, JFactory::getLanguage()->getTag());
            $hasSpecification = false;
            $attributeGroups = OpenShopHelper::getAttributeGroups(JFactory::getLanguage()->getTag());
            $productAttributes = array();
            for ($i = 0; $n = count($attributeGroups), $i < $n; $i++) {
                $productAttributes[] = OpenShopHelper::getAttributes($productId, $attributeGroups[$i]->id, JFactory::getLanguage()->getTag());
                if (count($productAttributes[$i]))
                    $hasSpecification = true;
            }
            $productRelations = OpenShopHelper::getProductRelations($productId, JFactory::getLanguage()->getTag());
            // Related products images resize
            for ($i = 0; $n = count($productRelations), $i < $n; $i++) {
                if ($productRelations[$i]->product_image && JFile::exists(JPATH_ROOT . '/images/com_openshop/products/' . $productRelations[$i]->product_image)) {
                    $thumbImage = call_user_func_array(array('OpenShopHelper', $relatedImageSizeFunction), array($productRelations[$i]->product_image, JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_related_width'), OpenShopHelper::getConfigValue('image_related_height')));
                } else {
                    $thumbImage = call_user_func_array(array('OpenShopHelper', $relatedImageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/products/', OpenShopHelper::getConfigValue('image_related_width'), OpenShopHelper::getConfigValue('image_related_height')));
                }
                $productRelations[$i]->thumb_image = JUri::base(true) . '/images/com_openshop/products/' . $productRelations[$i]->product_image;
            }
            if (OpenShopHelper::getConfigValue('allow_reviews')) {
                $productReviews = OpenShopHelper::getProductReviews($productId);
                $this->productReviews = $productReviews;
            }
            $tax = new OpenShopTax(OpenShopHelper::getConfig());
            if (OpenShopHelper::getConfigValue('social_enable')) {
                OpenShopHelper::loadShareScripts($item);
            }
            $this->item = $item;
            $this->productImages = $productImages;
            $this->discountPrices = $discountPrices;
            $this->productOptions = $productOptions;
            $this->hasSpecification = $hasSpecification;
            $this->attributeGroups = $attributeGroups;
            $this->productAttributes = $productAttributes;
            $this->productRelations = $productRelations;
            $this->manufacturer = OpenShopHelper::getProductBrand($productId, JFactory::getLanguage()->getTag());
            $this->tax = $tax;
            // Preparing rating html
            $ratingHtml = '<b>' . JText::_('OPENSHOP_BAD') . '</b>';
            for ($i = 1; $i <= 5; $i++) {
                $ratingHtml .= '<input type="radio" name="rating" value="' . $i . '" style="width: 25px;" />';
            }
            $ratingHtml .= '<b>' . JText::_('OPENSHOP_EXCELLENT') . '</b>';
            $this->ratingHtml = $ratingHtml;
            $this->productsNavigation = OpenShopHelper::getProductsNavigation($item->id);
            $this->labels = OpenShopHelper::getProductLabels($item->id);
            //Captcha
            $showCaptcha = 0;
            if (OpenShopHelper::getConfigValue('enable_reviews_captcha')) {
                $captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
                if ($captchaPlugin == 'recaptcha') {
                    $showCaptcha = 1;
                    $this->captcha = JCaptcha::getInstance($captchaPlugin)->display('dynamic_recaptcha_1', 'dynamic_recaptcha_1', 'required');
                }
            }
            $this->showCaptcha = $showCaptcha;
            $this->productColorDifferent = OpenShopHelper::getProductColorDifferent($item->id);

            $this->products = $this->get('ListProducts');
            $this->showSortOptions = true;
            parent::display($tpl);
        }
    }

    function getProductReplation($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('related_product_id')
                ->from($db->quoteName('#__openshop_productrelations', 'a'))
                ->where('a.product_id = ' . $id);
        $row_pr = $db->setQuery($query)->loadObjectList();

        $id_pr = '';
        $t = 1;
        foreach ($row_pr as $value) {
            if ($t == 1) {
                $id_pr .= $value->related_product_id;
                $t = 0;
            } else {
                $id_pr .= ',' . $value->related_product_id;
            }
        }


        $query->clear();
        $query->select('a.id,b.product_name,a.product_sku,a.product_image,a.product_price,a.product_price_r,a.buyer_virtual')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id');
        if (!empty($id_pr)) {
            $query->where('a.id IN (' . $id_pr . ')');
        } else {
            $query->where('a.id IN (0)');
        }

        return $db->setQuery($query)->loadObjectList();
    }

}
