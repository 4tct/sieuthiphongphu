<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 *
 * @package Joomla
 * @subpackage OpenShop
 * @since 1.5
 */
class OpenShopViewCart extends OpenShopView
{

	function display($tpl = null)
	{
		switch ($this->getLayout())
		{
			case 'mini':
				$this->_displayMini($tpl);
				break;
			case 'popout':
				$this->_displayPopout($tpl);
				break;
			default:
				break;
		}
	}

	/**
	 *
	 * @param string $tpl        	
	 */
	function _displayMini($tpl = null)
	{
		//Get cart data
		$cart = new OpenShopCart();
		$items = $cart->getCartData();
		$countProducts = $cart->countProducts();
		$currency = new OpenShopCurrency();
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		
		$model = $this->getModel();
		$model->getCosts();
		$totalData = $model->getTotalData();
		$totalPrice = $currency->format($model->getTotal());
		
		$this->items = $items;
		$this->countProducts = $countProducts;
		$this->totalData = $totalData;
		$this->totalPrice = $totalPrice;
		$this->currency = $currency;
		$this->tax = $tax;
		parent::display($tpl);
	}
	
	/**
	 *
	 * @param string $tpl
	 */
	function _displayPopout($tpl = null)
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::base(true).'/components/com_openshop/assets/colorbox/colorbox.css');
		$session = JFactory::getSession();
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		$cart = new OpenShopCart();
		$currency = new OpenShopCurrency();
		$cartData = $this->get('CartData');
		$model = $this->getModel();
		$model->getCosts();
		$totalData = $model->getTotalData();
		$total = $model->getTotal();
		$taxes = $model->getTaxes();
		$this->cartData = $cartData;
		$this->totalData = $totalData;
		$this->total = $total;
		$this->taxes = $taxes;
		$this->tax = $tax;
		$this->currency = $currency;
		if (OpenShopHelper::getConfigValue('cart_weight') && $cart->hasShipping())
		{
			$openshopWeight = new OpenShopWeight();
			$this->weight = $openshopWeight->format($cart->getWeight(), OpenShopHelper::getConfigValue('weight_id'));
		}
		else
		{
			$this->weight = 0;
		}
		// Success message
		if ($session->get('success'))
		{
			$this->success = $session->get('success');
			$session->clear('success');
		}
		if ($cart->getStockWarning() != '')
		{
			$this->warning = $cart->getStockWarning();
		}
		elseif ($cart->getMinSubTotalWarning() != '')
		{
			$this->warning = $cart->getMinSubTotalWarning();
		}
		elseif ($cart->getMinQuantityWarning() != '')
		{
			$this->warning = $cart->getMinQuantityWarning();
		}
		elseif ($cart->getMinProductQuantityWarning() != '')
		{
			$this->warning = $cart->getMinProductQuantityWarning();
		}
		elseif ($cart->getMaxProductQuantityWarning() != '')
		{
			$this->warning = $cart->getMaxProductQuantityWarning();
		}
		if ($session->get('warning'))
		{
			$this->warning = $session->get('warning');
			$session->clear('warning');
		}
		parent::display($tpl);
	}
}