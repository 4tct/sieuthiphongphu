<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCart extends OpenShopView {

    function display($tpl = null) {
        $mainframe = JFactory::getApplication();
        if (OpenShopHelper::getConfigValue('catalog_mode')) {
            $session = JFactory::getSession();
            $session->set('warning', JText::_('OPENSHOP_CATALOG_MODE_ON'));
            $mainframe->redirect(JRoute::_(OpenShopRoute::getViewRoute('categories')));
        } else {
            $app = JFactory::getApplication();
            $menu = $app->getMenu();
            $menuItem = $menu->getActive();
            if ($menuItem) {
                if (isset($menuItem->query['view']) && ($menuItem->query['view'] == 'frontpage')) {
                    $pathway = $app->getPathway();
                    $pathUrl = OpenShopRoute::getViewRoute('frontpage');
                    $pathway->addItem(JText::_('OPENSHOP_SHOPPING_CART'), $pathUrl);
                }
            }
            $document = JFactory::getDocument();
            $document->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/colorbox/colorbox.css');
            $title = JText::_('OPENSHOP_SHOPPING_CART');
            // Set title of the page
            $siteNamePosition = $app->getCfg('sitename_pagetitles');
            if ($siteNamePosition == 1) {
                $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
            } elseif ($siteNamePosition == 2) {
                $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
            }
            $document->setTitle($title);
            $session = JFactory::getSession();
            $tax = new OpenShopTax(OpenShopHelper::getConfig());
            $cart = new OpenShopCart();
            $currency = new OpenShopCurrency();
            $cartData = $this->get('CartData');
            $model = $this->getModel();
            $model->getCosts();
            $totalData = $model->getTotalData();
            $total = $model->getTotal();
            $taxes = $model->getTaxes();
            $this->cartData = $cartData;
            $this->totalData = $totalData;
            $this->total = $total;
            $this->taxes = $taxes;
            $this->tax = $tax;
            $this->currency = $currency;
            $this->coupon_code = $session->get('coupon_code');
            $this->voucher_code = $session->get('voucher_code');
            $this->postcode = $session->get('shipping_postcode');
            $this->shipping_required = $cart->hasShipping();
            if (OpenShopHelper::getConfigValue('cart_weight') && $cart->hasShipping()) {
                $openshopWeight = new OpenShopWeight();
                $this->weight = $openshopWeight->format($cart->getWeight(), OpenShopHelper::getConfigValue('weight_id'));
            } else {
                $this->weight = 0;
            }
            if ($this->shipping_required) {
                $shippingMethod = $session->get('shipping_method');
                if (is_array($shippingMethod)) {
                    $this->shipping_method = $shippingMethod['name'];
                } else {
                    $this->shipping_method = '';
                }
                $document->addScriptDeclaration(OpenShopHtmlHelper::getZonesArrayJs());
                //Country list
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->select('id, country_name AS name')
                        ->from('#__openshop_countries')
                        ->where('published=1')
                        ->order('country_name');
                $db->setQuery($query);
                $options = array();
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_PLEASE_SELECT'), 'id', 'name');
                $options = array_merge($options, $db->loadObjectList());
                $lists['country_id'] = JHtml::_('select.genericlist', $options, 'country_id', ' class="inputbox" ', 'id', 'name', $session->get('shipping_country_id'));
                //Zone list
                $query->clear();
                $query->select('id, zone_name')
                        ->from('#__openshop_zones')
                        ->where('country_id=' . (int) $session->get('shipping_country_id'))
                        ->where('published=1');

                $db->setQuery($query);
                $options = array();
                $options[] = JHtml::_('select.option', 0, JText::_('OPENSHOP_PLEASE_SELECT'), 'id', 'zone_name');
                $options = array_merge($options, $db->loadObjectList());
                $lists['zone_id'] = JHtml::_('select.genericlist', $options, 'zone_id', ' class="inputbox" ', 'id', 'zone_name', $session->get('shipping_zone_id'));
                $this->lists = $lists;
                $this->shipping_zone_id = $session->get('shipping_zone_id');
            }
            // Success message
            if ($session->get('success')) {
                $this->success = $session->get('success');
                $session->clear('success');
            }
            if ($cart->getStockWarning() != '') {
                $this->warning = $cart->getStockWarning();
            } elseif ($cart->getMinSubTotalWarning() != '') {
                $this->warning = $cart->getMinSubTotalWarning();
            } elseif ($cart->getMinQuantityWarning() != '') {
                $this->warning = $cart->getMinQuantityWarning();
            } elseif ($cart->getMinProductQuantityWarning() != '') {
                $this->warning = $cart->getMinProductQuantityWarning();
            } elseif ($cart->getMaxProductQuantityWarning() != '') {
                $this->warning = $cart->getMaxProductQuantityWarning();
            }
            if ($session->get('warning')) {
                $this->warning = $session->get('warning');
                $session->clear('warning');
            }
            $this->user = JFactory::getUser();
            parent::display($tpl);
        }
    }

    function getCartProducts($ip, $user_id = null) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_sku,c.product_name,d.value as size,b.product_image,b.product_price,b.id as product_id,d.optionvalue_id as size_id,b.product_price_r')
                ->from($db->quoteName('#__openshop_cartproducts', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . 'ON a.product_id = c.product_id')
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'd') . 'ON d.optionvalue_id = a.size_id')
                ->where('a.ip = "' . $ip . '"', 'OR');
        if (isset($user_id) && !empty($user_id) && $user_id != '0') {
            $query->where('a.user_id = ' . $user_id);
        }
        $productS = $db->setQuery($query)->loadObjectList();
        
        
        $query->clear();
        $query->select('a.*,b.product_sku,c.product_name,d.value as size,b.product_image,b.product_price,b.id as product_id,d.optionvalue_id as color_id,b.product_price_r')
                ->from($db->quoteName('#__openshop_cartproducts', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . 'ON a.product_id = c.product_id')
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'd') . 'ON d.optionvalue_id = a.color_id')
                ->where('a.ip = "' . $ip . '"', 'OR');
        if (isset($user_id) && !empty($user_id) && $user_id != '0') {
            $query->where('a.user_id = ' . $user_id);
        }
        $productC = $db->setQuery($query)->loadObjectList();
        
        return array_merge($productC, $productS);
        
    }

    function getProduct($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.product_name,a.*')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id')
                ->where('a.id = ' . $id);
        return $db->setQuery($query)->loadObject();
    }

    function getZone() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('id,zone_name')
                    ->from($db->quoteName('#__openshop_zones'))
                    ->where('published = 1');
        } catch (Exception $ex) {
            //error
        }
        return $db->setQuery($query)->loadObjectList();
    }

    function getDistrict() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('id,district_name')
                    ->from($db->quoteName('#__openshop_districts'))
                    ->where('published = 1');
        } catch (Exception $ex) {
            //error
        }
        return $db->setQuery($query)->loadObjectList();
    }

    function getUserInfo($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('a.fullname,a.telephone,a.email,b.address,b.district_id,b.zone_id,c.username')
                    ->from($db->quoteName('#__openshop_customers', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_addresses', 'b') . 'ON a.customer_id = b.customer_id')
                    ->join('INNER', $db->quoteName('#__users', 'c') . 'ON c.id = a.customer_id')
                    ->where('a.customer_id = ' . $id);
        } catch (Exception $ex) {
            
        }
        return $db->setQuery($query)->loadObject();
    }

    function getAllSize() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.optionvalue_id,a.value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'b') . ' ON a.option_id = b.option_id ')
                ->where('b.option_name = "SIZE"');
        return $db->setQuery($query)->loadObjectList();
    }
    
    function checkNewUser($value){
        $v = OpenShopHelper::convertBase64Decode($value);
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__users'))
                ->where('id = ' . $v);
        return $db->setQuery($query)->loadObject();
    }

}
