<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 *
 * @package Joomla
 * @subpackage OpenShop
 * @since 1.5
 */
class OpenShopViewQuote extends OpenShopView
{

	function display($tpl = null)
	{
		switch ($this->getLayout())
		{
			case 'mini':
				$this->_displayMini($tpl);
				break;
			case 'popout':
				$this->_displayPopout($tpl);
				break;
			default:
				break;
		}
	}

	/**
	 *
	 * @param string $tpl        	
	 */
	function _displayMini($tpl = null)
	{
		//Get quote data
		$quote = new OpenShopQuote();
		$items = $quote->getQuoteData();
		$countProducts = $quote->countProducts();
		$this->items = $items;
		$this->countProducts = $countProducts;
		parent::display($tpl);
	}
	
	function _displayPopout($tpl = null)
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::base(true).'/components/com_openshop/assets/colorbox/colorbox.css');
		$session = JFactory::getSession();
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		$quote = new OpenShopQuote();
		$currency = new OpenShopCurrency();
		$quoteData = $this->get('QuoteData');
		$this->quoteData = $quoteData;
		$this->tax = $tax;
		$this->currency = $currency;
		// Success message
		if ($session->get('success'))
		{
			$this->success = $session->get('success');
			$session->clear('success');
		}
		parent::display($tpl);
	}
}