<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewQuote extends OpenShopView
{		
	function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		if (!OpenShopHelper::getConfigValue('quote_cart_mode'))
		{
			$session = JFactory::getSession();
			$session->set('warning', JText::_('OPENSHOP_QUOTE_CART_MODE_OFF'));
			$mainframe->redirect(JRoute::_(OpenShopRoute::getViewRoute('categories')));
		}
		else
		{
			$app		= JFactory::getApplication();
			$menu		= $app->getMenu();
			$menuItem = $menu->getActive();
			if ($menuItem)
			{
				if (isset($menuItem->query['view']) && ($menuItem->query['view']== 'frontpage'))
				{
					$pathway = $app->getPathway();
					$pathUrl = OpenShopRoute::getViewRoute('frontpage');
					$pathway->addItem(JText::_('OPENSHOP_QUOTE_CART'), $pathUrl);
				}
			}
			$document = JFactory::getDocument();
			$document->addStyleSheet(JUri::base(true).'/components/com_openshop/assets/colorbox/colorbox.css');
			$title = JText::_('OPENSHOP_QUOTE_CART');
			// Set title of the page
			$siteNamePosition = $app->getCfg('sitename_pagetitles');
			if($siteNamePosition == 1)
			{
				$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
			}
			elseif ($siteNamePosition == 2)
			{
				$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
			}
			$document->setTitle($title);
			$session = JFactory::getSession();
			$tax = new OpenShopTax(OpenShopHelper::getConfig());
			$quote = new OpenShopQuote();
			$currency = new OpenShopCurrency();
			$quoteData = $this->get('QuoteData');
			$this->quoteData = $quoteData;
			$this->tax = $tax;
			$this->currency = $currency;
			// Success message
			if ($session->get('success'))
			{
				$this->success = $session->get('success');
				$session->clear('success');
			}
			//Captcha
			$showCaptcha = 0;
			if (OpenShopHelper::getConfigValue('enable_quote_captcha'))
			{
				$captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
				if ($captchaPlugin == 'recaptcha')
				{
					$showCaptcha = 1;
					$this->captcha = JCaptcha::getInstance($captchaPlugin)->display('dynamic_recaptcha_1', 'dynamic_recaptcha_1', 'required');
				}
			}
			$this->showCaptcha = $showCaptcha;
			parent::display($tpl);
		}
	}
}