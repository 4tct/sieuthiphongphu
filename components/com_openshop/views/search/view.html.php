<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewSearch extends OpenShopView {

    function display($tpl = null) {
        $keyword = JRequest::getString('keyword');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__openshop_tags')
                ->where('tag_name = ' . $db->quote($keyword));
        $db->setQuery($query);
        $tagId = $db->loadResult();
        if ($tagId)
            OpenShopHelper::updateHits($tagId, 'tags');
        $app = JFactory::getApplication();
        $model = $this->getModel();
        $document = JFactory::getDocument();
        $document->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/colorbox/colorbox.css');
        $document->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/css/labels.css');
        $params = $app->getParams();
        $title = $params->get('page_title', '');
        if ($title == '') {
            $title = JText::_('OPENSHOP_SEARCH_RESULT');
        }
        // Set title of the page
        $siteNamePosition = $app->getCfg('sitename_pagetitles');
        if ($siteNamePosition == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($siteNamePosition == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $document->setTitle($title);
        //Sort options
        $sortOptions = OpenShopHelper::getConfigValue('sort_options');
        $sortOptions = explode(',', $sortOptions);
        $sortValues = array(
            'b.product_name-ASC',
            'b.product_name-DESC',
            'a.product_sku-ASC',
            'a.product_sku-DESC',
            'a.product_price-ASC',
            'a.product_price-DESC',
            'a.product_length-ASC',
            'a.product_length-DESC',
            'a.product_width-ASC',
            'a.product_width-DESC',
            'a.product_height-ASC',
            'a.product_height-DESC',
            'a.product_weight-ASC',
            'a.product_weight-DESC',
            'a.product_quantity-ASC',
            'a.product_quantity-DESC',
            'b.product_short_desc-ASC',
            'b.product_short_desc-DESC',
            'b.product_desc-ASC',
            'b.product_desc-DESC'
        );
        $sortTexts = array(
            JText::_('OPENSHOP_SORTING_PRODUCT_NAME_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_NAME_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_SKU_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_SKU_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_PRICE_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_PRICE_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_LENGTH_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_LENGTH_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_WIDTH_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_WIDTH_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_HEIGHT_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_HEIGHT_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_WEIGHT_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_WEIGHT_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_QUANTITY_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_QUANTITY_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_SHORT_DESC_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_SHORT_DESC_DESC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_DESC_ASC'),
            JText::_('OPENSHOP_SORTING_PRODUCT_DESC_DESC')
        );
        $options = array();
        $options[] = JHtml::_('select.option', 'a.id-DESC', JText::_('OPENSHOP_SORTING_DEFAULT'));
        for ($i = 0; $i < count($sortValues); $i++) {
            if (in_array($sortValues[$i], $sortOptions)) {
                $options[] = JHtml::_('select.option', $sortValues[$i], $sortTexts[$i]);
            }
        }
        if (count($options) > 1) {
            $this->sort_options = JHtml::_('select.genericlist', $options, 'sort_options', 'class="input-large" onchange="this.form.submit();" ', 'value', 'text', JRequest::getVar('sort_options', ''));
        } else {
            $this->sort_options = '';
        }
        // Store session for Continue Shopping Url
        JFactory::getSession()->set('continue_shopping_url', JUri::getInstance()->toString());
        $this->products = $model->getData();
        $this->pagination = $model->getPagination();
        $this->tags = $model->setTags();
        $this->tax = new OpenShopTax(OpenShopHelper::getConfig());
        $this->currency = new OpenShopCurrency();
        $this->productsPerRow = OpenShopHelper::getConfigValue('items_per_row', 3);
        $this->actionUrl = '';

        parent::display($tpl);
    }
}
