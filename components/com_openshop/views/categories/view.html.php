<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 1.5
 */
class OpenShopViewCategories extends OpenShopView
{

	function display($tpl = null)
	{
		$app = JFactory::getApplication();																						
		$params = $app->getParams();
		$model = $this->getModel();
		$title = $params->get('page_title', '');
		$session = JFactory::getSession();
		// Set title of the page
		$siteNamePosition = $app->getCfg('sitename_pagetitles');
		if ($siteNamePosition == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($siteNamePosition == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		JFactory::getDocument()->setTitle($title);					
		JFactory::getSession()->set('continue_shopping_url', JUri::getInstance()->toString());
		if ($session->get('warning'))
		{
			$this->warning = $session->get('warning');
			$session->clear('warning');
		}
		$this->config = OpenShopHelper::getConfig();
		$this->items = $model->getData();
		$this->categoriesPerRow = OpenShopHelper::getConfigValue('items_per_row', 3);
		$this->pagination = $model->getPagination();
		$this->params = $params;
		parent::display($tpl);
	}
}