<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewWishlists extends OpenShopView {

    function display($tpl = null) {
        $model = $this->getModel();
        $this->wishlists = $model->getWishLists();
        parent::display($tpl);
    }
    
    function getSizeProduct($id_pro) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.optionvalue_id,a.value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productoptionvalues', 'b') . 'ON a.optionvalue_id = b.option_value_id')
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . 'ON a.option_id = c.option_id')
                ->where('UPPER(c.option_name) = "SIZE"')
                ->where('b.product_id = ' . $id_pro);
        return $db->setQuery($query)->loadObjectList();
    }

}
