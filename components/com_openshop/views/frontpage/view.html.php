<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewFrontpage extends OpenShopView {

    function display($tpl = null) {
        $document = JFactory::getDocument();
        $app = JFactory::getApplication('site');
        $params = $app->getParams();
        $title = $params->get('page_title', '');
        if ($title == '') {
            $title = JText::_('OPENSHOP_FRONT_PAGE');
        }
        // Set title of the page
        $siteNamePosition = $app->getCfg('sitename_pagetitles');
        if ($siteNamePosition == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($siteNamePosition == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $document->setTitle($title);
        // Set metakey, metadesc and robots
        if ($params->get('menu-meta_keywords')) {
            $document->setMetaData('keywords', $params->get('menu-meta_keywords'));
        }
        if ($params->get('menu-meta_description')) {
            $document->setMetaData('description', $params->get('menu-meta_description'));
        }
        if ($params->get('robots')) {
            $document->setMetadata('robots', $params->get('robots'));
        }
        $numberCategories = (int) $params->get('num_categories', 9);
        $numberProducts = (int) $params->get('num_products', 9);
        JLoader::register('OpenShopModelCategories', JPATH_ROOT . '/components/com_openshop/models/categories.php');
        JLoader::register('OpenShopModelProducts', JPATH_ROOT . '/components/com_openshop/models/products.php');
        if ($numberCategories > 0) {
            $categories = RADModel::getInstance('Categories', 'OpenShopModel', array('remember_states' => false))
                    ->limitstart(0)
                    ->limit($numberCategories)
                    ->filter_order('a.ordering')
                    ->getData();
        } else {
            $categories = array();
        }
        if ($numberProducts > 0) {
            $products = RADModel::getInstance('Products', 'OpenShopModel', array('remember_states' => false))
                    ->limitstart(0)
                    ->limit($numberProducts)
                    ->product_featured(1)
                    ->sort_options('a.ordering-ASC')
                    ->getData();
        } else {
            $products = array();
        }



        // Store session for Continue Shopping Url		
        JFactory::getSession()->set('continue_shopping_url', JUri::getInstance()->toString());
        $tax = new OpenShopTax(OpenShopHelper::getConfig());
        $currency = new OpenShopCurrency();
        $this->categories = $categories;
        $this->products = $products;
        $this->products_hit = $this->get('products_hit');
        $this->printProduct = $this->get('printProduct');
        $this->tax = $tax;
        $this->currency = $currency;
        $this->productsPerRow = OpenShopHelper::getConfigValue('items_per_row', 3);
        $this->categoriesPerRow = OpenShopHelper::getConfigValue('items_per_row', 3);
        $this->show_categories = $params->get('show_categories');

        parent::display($tpl);
    }

    
    
}
