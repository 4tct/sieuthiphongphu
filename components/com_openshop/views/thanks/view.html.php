<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for OpenShop component
 *
 * @static
 * @package		Joomla
 * @subpackage          OpenShop
 * @since 1.5
 */
class OpenShopViewThanks extends OpenShopView {

    function display($tpl = null) {
        parent::display($tpl);
    }
    
    function checkNewUser($value){
        $v = OpenShopHelper::convertBase64Decode($value);
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__users'))
                ->where('id = ' . $v);
        return $db->setQuery($query)->loadObject();
    }

}
