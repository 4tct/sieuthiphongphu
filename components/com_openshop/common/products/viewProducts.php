<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$user = JFactory::getSession()->get('user');
JFactory::getDocument(true)->addStyleSheet(JUri::base() . 'components/com_openshop/assets/css/list.css');
$session = JFactory::getSession();
$session->set('limitProList', '0');
if (empty($idCat)) {
    $idCat = 'all';
}
?>

<style>
    .openshop-products-list{
        background-image: url(<?php echo JUri::base() ?>images/patern/background-content1.png);
        box-shadow: 0px 0px 10px 0px #b3b3b3;
        border-radius: 0px 0px 5px 5px;
    }
    .search-categories{
        width: 100%;
        background-image: url(<?php echo JUri::base() ?>images/patern/background-left1.png);
        border: 1px solid #dedede;
        box-shadow: 1px 1px 5px 0px #dedede;
    }
</style>
<div>
    <div id="products-list-container" class="products-list-container block list">
        <div id="products-list" class="products-list">
            <!--//Search advance-->
            <?php
            $cls_col_wapper = 'col-md-12';
            $cls_col_wapper_pro = 'col-md-4';
            $modules = JModuleHelper::getModules('search-advance-left');
            if (count($modules)) {
                ?>
                <div class="col-md-2">
                    <?php
                    foreach ($modules as $module) {
                        echo JModuleHelper::renderModule($module);
                    }
                    ?>
                </div>
                <?php
                $cls_col_wapper = 'col-md-10';
            } else {
                $cls_col_wapper_pro = 'col-md-2';
            }
            ?>

            <!--//show products-->
            <div 
                class="<?php
                echo $cls_col_wapper;
                echo ' ';
                echo OpenShopHelper::getConfigValue('default_products_layout');
                ?> colShowPro showTabContentT2" id="contentShowProduct">
                <div class="content_show_product_tabT2 tab1_T2">
                    <div class="col-md-12 padding0">
                        <div class="clr"></div>
                        <div class="breadcrumd_detail">
                            <div class="breadcrumb_new">
                                <div class="bg-breadcrumb_new">
                                    <ul class="ul_breadcrumb_new">
                                        <li class="home">
                                            <a href="#">
                                                <i class="fa fa-home" aria-hidden="true"></i>
                                            </a>
                                            <div class="arrow-right"></div>
                                        </li>
                                        <?php
                                        $list = OpenShopHelper::getParentCategories($idCat);
                                        $totalL = count($list) - 1;
                                        for ($i = $totalL; $i >= 0; $i--) {
                                            $l = $list[$i];
                                            $cls = 'brc';
                                            $link = JRoute::_(OpenShopRoute::getCategoryRoute($l->id));
                                            if($totalL == 0){
                                                $cls = 'brc_active';
                                            }
                                            ?>
                                            <li class="<?php echo $cls ?>">
                                                <a href="<?php echo $link; ?>">
                                                    <?php echo $l->category_name ?>
                                                </a>
                                                <div class="bg-arrow-right"></div>
                                                <div class="arrow-right"></div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="contentLoadT2">
                        <?php
                        if (count($products)) {
                            foreach ($products as $ct2) {
                                $link = JRoute::_(OpenShopRoute::getProductRoute($ct2->id, OpenShopHelper::getProductCategory($ct2->id)));
                                ?>
                                <div class="wow fadeInUp b_t_2 b_t_t_2 col-sm-4 <?php echo $cls_col_wapper_pro; ?>">
                                    <div class="borderContentProduct">
                                        <div class="infoImgT2" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ct2->id) ?>', '<?php echo $link ?>')" >
                                            <img style="display: inline;" data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ct2->product_image ?>" class="img-responsive lazy" title="<?php echo $ct2->product_name ?>" alt="<?php echo $ct2->product_name ?>"/>
                                            <div class="infoChildProT2">
                                                <div class="pricePro1">
                                                    <?php
                                                    echo number_format($ct2->product_price, 0, ',', '.');
                                                    echo '<sup>đ</sup>';
                                                    if ($ct2->product_price_r != 0) {
                                                        echo '<span class="pricenotbuy">';
                                                        echo number_format($ct2->product_price_r, 0, ',', '.');
                                                        echo '<sup>đ</sup>';
                                                        echo '</span>';
                                                    }
                                                    ?>
                                                </div>
                                                <div>
                                                    Kích thước: 
                                                    <?php
                                                    $sizes = OpenShopHelper::getProductOptionValueDetails($ct2->id, 'size');
                                                    foreach ($sizes as $k => $s) {
                                                        echo $s->value;
                                                        if ($k != count($sizes) - 1) {
                                                            echo ' - ';
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <!--<div>Màu sắc: Xanh</div>-->
                                            </div>

                                            <?php
                                            if ($ct2->product_price_r > 0) {
                                                $percent = 100 - (int) (((int) $ct2->product_price * 100) / $ct2->product_price_r)
                                                ?>
                                                <div class="percentSale rightPercent2"> 
                                                    <?php echo $percent ?>%
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="namePro">
                                            <!--<a href="<?php echo $link ?>">-->
                                            <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ct2->id) ?>', '<?php echo $link ?>')" >
                                                <?php echo $ct2->product_name ?>
                                            </a>
                                        </div> 
                                        <div class="codePro">
                                            Mã: <strong><?php echo $ct2->product_sku ?></strong>
                                        </div>
                                        <div class="pricePro">
                                            <?php
                                            echo number_format($ct2->product_price, 0, ',', '.');
                                            echo '<sup>đ</sup>';
                                            if ($ct2->product_price_r != 0) {
                                                echo '<span class="pricenotbuy">';
                                                echo number_format($ct2->product_price_r, 0, ',', '.');
                                                echo '<sup>đ</sup>';
                                                echo '</span>';
                                            }
                                            ?>
                                        </div>
                                        <div class="btn-buy">
                                            <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ct2->id) ?>', '<?php echo $link ?>')" >
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                <span>Mua ngay</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="ct2_not_pro">
                                Hiện chưa có sản phẩm
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!--Load More-->
        <div class="col-md-12 padding0">
            <div class="loadmoreProduct">
                <span class="textLoadMore" onclick="getProductPagination('<?php echo OpenShopHelper::convertBase64Encode($idCat); ?>', '<?php echo $cls_col_wapper_pro; ?>')">
                    Xem thêm
                    <span class="loadingLoadMore"></span>
                </span>
            </div>
        </div>
    </div>
</div>


<div class = "modal fade chooseSizeCP form-horizontal" tabindex = "-1" role = "dialog" aria-labelledby = "mySmallModalLabel">
    <div class = "modal-dialog modal-sm" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
                <h4 class = "modal-title" id = "myModalLabel">Chọn Size</h4>
            </div>
            <div class = "modal-body">
                <label>Chọn size</label>
                <div class="contentSizeCP">
                    <!--//size-->
                </div>
            </div>
            <div class = "modal-footer">
                <button type = "button" class = "btn btn-primary addCP" onclick="">Thêm vào giỏ hàng</button>
            </div>
        </div>
    </div>
</div>
<div class="clr"></div>