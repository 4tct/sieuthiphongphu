<?php
defined('_JEXEC') or die('Restricted access');
$uri = JUri::getInstance();
$proOption = $this->productOptions;

$onClickCart = 'notsize';
if (count($this->productOptions)) {
    foreach ($proOption as $proO) {
        if ($proO->option_name == "SIZE") {
            $onClickCart = '';
        }
    }
}

if (isset($this->token) && !empty($this->token)) {
    $onclickClose = 'closeModal(\'' . $this->titleJS . '\')';
} else {
    $catObject = OpenShopHelper::getCategory(JRequest::getVar('catid'));
    $linkCatObject = JRoute::_(OpenShopRoute::getCategoryRoute($catObject->id));
    $onclickClose = 'closeModalCat(\'' . $catObject->category_name . '\',\'' . $linkCatObject . '\')';
}

$urlHistory = '';
if (isset($this->urlHistory) && !empty($this->urlHistory)) {
    $urlHistory = OpenShopHelper::convertBase64Decode($this->urlHistory);
    ?>
    <script>
        document.title = "<?php echo $this->item->product_name ?>";
    </script>
    <?php
}
?>

<?php
$style = 'style="width: 90%;"';
$paddingImg = 'paddingL0';
$marginBtnBuy = '';
if (OpenShopHelper::isMobile()) {
    $style = '';
    $paddingImg = 'padding0';
    $marginBtnBuy = 'style="margin-top: 10px;"';
}
?>

<div class="showModalContentProductDetail">
    <div class="modal-header widthFixed">
        <button type="button" class="close" onclick="<?php echo $onclickClose; ?>">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="display_inline_block">
            <h2 class="modal-title" id="myDetailProductLabel">
                <?php echo $this->item->product_name ?>
            </h2>
        </div>
        <div class="btn_buy_Fixed display_inline_block">
            <div class="btn-buy-now display_inline_block" onclick="showCart('<?php echo $onClickCart ?>')" <?php echo $marginBtnBuy ?>>
                <i class="fa fa-check" aria-hidden="true"></i>
                Mua ngay
            </div>
        </div>
    </div>

    <div class="modal fade" id="myTestModalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document" <?php echo $style ?>>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close btnCloseJ" onclick="<?php echo $onclickClose ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="myDetailProductLabel">
                        <?php echo $this->item->product_name ?>
                    </h2>
                </div>
                <div class="modal-body" id="myTestModal">
                    <div class="product-info">
                        <div class="row showProductDetail">
                            <div class="col-md-9">
                                <div class="col-sm-4 col-md-4 <?php echo $paddingImg ?>">
                                    <div class="borderMainImg">
                                        <?php
                                        if (OpenShopHelper::getConfigValue('view_image') == 'zoom') {
                                            ?>
                                            <img id="zoomZoom" class="img-responsive imgdisplaynone" src="<?php echo $this->item->thumb_image; ?>" data-zoom-image="<?php echo $this->item->popup_image; ?>" title="<?php echo $this->item->product_name; ?>" alt="<?php echo $this->item->product_name ?>"/>
                                            <?php
                                            if (count($this->productImages)) {
                                                ?>
                                                <div id="showSubImgZoom">
                                                    <a href="#" data-image="<?php echo $this->item->thumb_image; ?>" data-zoom-image="<?php echo $this->item->popup_image; ?>">
                                                        <img id="zoomZoom" src="<?php echo $this->item->small_thumb_image; ?>" />
                                                    </a>

                                                    <?php
                                                    for ($i = 0; $n = count($this->productImages), $i < $n; $i++) {
                                                        ?>
                                                        <a href="#" data-image="<?php echo $this->productImages[$i]->thumb_image; ?>" data-zoom-image="<?php echo $this->productImages[$i]->popup_image; ?>">
                                                            <img id="zoomZoom" src="<?php echo $this->productImages[$i]->small_thumb_image; ?>" title="<?php echo $this->item->product_name . '-' . $i; ?>" alt="<?php echo $this->item->product_name . '-' . $i ?>"/>
                                                        </a>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>

                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <div class="image img-polaroid" id="main-image-area">
                                                <a class="product-image" href="<?php echo $this->item->popup_image; ?>">
                                                    <?php
                                                    if (count($this->labels)) {
                                                        for ($i = 0; $n = count($this->labels), $i < $n; $i++) {
                                                            $label = $this->labels[$i];
                                                            if ($label->label_style == 'rotated' && !($label->enable_image && $label->label_image)) {
                                                                ?>
                                                                <div class="cut-rotated">
                                                                    <?php
                                                                }
                                                                if ($label->enable_image && $label->label_image) {
                                                                    $imageWidth = $label->label_image_width > 0 ? $label->label_image_width : OpenShopHelper::getConfigValue('label_image_width');
                                                                    if (!$imageWidth)
                                                                        $imageWidth = 50;
                                                                    $imageHeight = $label->label_image_height > 0 ? $label->label_image_height : OpenShopHelper::getConfigValue('label_image_height');
                                                                    if (!$imageHeight)
                                                                        $imageHeight = 50;
                                                                    ?>
                                                                    <span class="horizontal <?php echo $label->label_position; ?> small-db" style="opacity: <?php echo $label->label_opacity; ?>;<?php echo 'background-image: url(' . $label->label_image . ')'; ?>; background-repeat: no-repeat; width: <?php echo $imageWidth; ?>px; height: <?php echo $imageHeight; ?>px; box-shadow: none;"></span>
                                                                    <?php
                                                                }
                                                                else {
                                                                    ?>
                                                                    <span class="<?php echo $label->label_style; ?> <?php echo $label->label_position; ?> small-db" style="background-color: <?php echo '#' . $label->label_background_color; ?>; color: <?php echo '#' . $label->label_foreground_color; ?>; opacity: <?php echo $label->label_opacity; ?>;<?php if ($label->label_bold) echo 'font-weight: bold;'; ?>">
                                                                        <?php echo $label->label_name; ?>
                                                                    </span>
                                                                    <?php
                                                                }
                                                                if ($label->label_style == 'rotated' && !($label->enable_image && $label->label_image)) {
                                                                    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <img src="<?php echo $this->item->thumb_image; ?>" title="<?php echo $this->item->product_page_title != '' ? $this->item->product_page_title : $this->item->product_name; ?>" alt="<?php echo $this->item->product_page_title != '' ? $this->item->product_page_title : $this->item->product_name; ?>" />
                                                </a>
                                            </div>
                                            <?php
                                            if (count($this->productImages) > 0) {
                                                ?>
                                                <div class="image-additional">
                                                    <?php
                                                    for ($i = 0; $n = count($this->productImages), $i < $n; $i++) {
                                                        ?>
                                                        <div>
                                                            <a class="product-image" href="<?php echo $this->productImages[$i]->popup_image; ?>">
                                                                <img src="<?php echo $this->productImages[$i]->small_thumb_image; ?>" />
                                                            </a>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>

                                </div>
                                <!-- /Hình ảnh chính-->


                                <div class="col-sm-8 col-md-8 paddingR0">
                                    <div class="clr"></div>
                                    <?php
                                    if (!OpenShopHelper::isMobile()) {
                                        ?>
                                        <div class="breadcrumd_detail">
                                            <div class="breadcrumb_new">
                                                <div class="bg-breadcrumb_new">
                                                    <ul class="ul_breadcrumb_new">
                                                        <li class="home">
                                                            <a href="#">
                                                                <i class="fa fa-home" aria-hidden="true"></i>
                                                            </a>
                                                            <div class="arrow-right"></div>
                                                        </li>
                                                        <?php
                                                        $list = OpenShopHelper::getParentCategories(OpenShopHelper::getProductCategory($this->item->id));
                                                        for ($i = (count($list) - 1); $i >= 0; $i--) {
                                                            $l = $list[$i];
                                                            $cls = 'brc';
                                                            $link = JRoute::_(OpenShopRoute::getCategoryRoute($l->id));
                                                            ?>
                                                            <li class="<?php echo $cls ?>">
                                                                <a href="<?php echo $link; ?>">
                                                                    <?php echo $l->category_name ?>
                                                                </a>
                                                                <div class="bg-arrow-right"></div>
                                                                <div class="arrow-right"></div>
                                                            </li>
                                                            <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="infoshortP">
                                        <div class="short-desc"> 
                                            <?php
                                            if (empty($this->item->product_short_desc)) {
                                                echo $this->item->product_name;
                                            } else {
                                                echo $this->item->product_short_desc;
                                            }
                                            ?> 
                                        </div>
                                        <?php
                                        if (OpenShopHelper::getConfigValue('show_manufacturer') || OpenShopHelper::getConfigValue('show_sku') || OpenShopHelper::getConfigValue('show_availability')) {
                                            ?>
                                            <div>
                                                <div class="product-desc">
                                                    <?php
                                                    if (OpenShopHelper::getConfigValue('show_manufacturer')) {
                                                        ?>
                                                        <div class="product-manufacturer">
                                                            <strong><?php echo JText::_('OPENSHOP_BRAND'); ?>:</strong>
                                                            <span><?php echo isset($this->manufacturer->manufacturer_name) ? $this->manufacturer->manufacturer_name : ''; ?></span>
                                                        </div>
                                                        <?php
                                                    }
                                                    if (OpenShopHelper::getConfigValue('show_sku')) {
                                                        ?>
                                                        <div class="product-sku">
                                                            <strong><?php echo JText::_('OPENSHOP_PRODUCT_CODE'); ?>:</strong>
                                                            <span><?php echo $this->item->product_sku; ?></span>
                                                        </div>	
                                                        <?php
                                                    }
                                                    if (OpenShopHelper::getConfigValue('show_availability')) {
                                                        ?>
                                                        <div class="product-availability">
                                                            <strong><?php echo JText::_('OPENSHOP_AVAILABILITY'); ?>:</strong>
                                                            <span>
                                                                <?php
                                                                echo $this->item->availability;
                                                                if (isset($this->product_available_date)) {
                                                                    echo ' (' . JText::_('OPENSHOP_PRODUCT_AVAILABLE_DATE') . ': ' . $this->product_available_date . ')';
                                                                }
                                                                ?>
                                                            </span>
                                                        </div>
                                                        <?php
                                                    }
                                                    if (OpenShopHelper::getConfigValue('show_product_weight')) {
                                                        ?>
                                                        <div class="product-weight">
                                                            <strong><?php echo JText::_('OPENSHOP_PRODUCT_WEIGHT'); ?>:</strong>
                                                            <span><?php echo number_format($this->item->product_weight, 2) . OpenShopHelper::getWeightUnit($this->item->product_weight_id, JFactory::getLanguage()->getTag()); ?></span>
                                                        </div>
                                                        <?php
                                                    }
                                                    if (OpenShopHelper::getConfigValue('show_product_dimensions')) {
                                                        ?>
                                                        <div class="product-dimensions">
                                                            <strong><?php echo JText::_('OPENSHOP_PRODUCT_DIMENSIONS'); ?>:</strong>
                                                            <span><?php echo number_format($this->item->product_length, 2) . OpenShopHelper::getLengthUnit($this->item->product_length_id, JFactory::getLanguage()->getTag()) . ' x ' . number_format($this->item->product_width, 2) . OpenShopHelper::getLengthUnit($this->item->product_length_id, JFactory::getLanguage()->getTag()) . ' x ' . number_format($this->item->product_height, 2) . OpenShopHelper::getLengthUnit($this->item->product_length_id, JFactory::getLanguage()->getTag()); ?></span>
                                                        </div>
                                                        <?php
                                                    }
                                                    if (OpenShopHelper::getConfigValue('show_product_tags') && count($this->productTags)) {
                                                        ?>
                                                        <div class="product-tags">
                                                            <strong><?php echo JText::_('OPENSHOP_PRODUCT_TAGS'); ?>:</strong>
                                                            <span>
                                                                <?php
                                                                for ($i = 0; $n = count($this->productTags), $i < $n; $i++) {
                                                                    $tagName = trim($this->productTags[$i]->tag_name);
                                                                    $searchTagLink = JRoute::_('index.php?option=com_openshop&view=search&keyword=' . $tagName);
                                                                    ?>
                                                                    <a href="<?php echo $searchTagLink; ?>" title="<?php echo $tagName; ?>"><?php echo $tagName; ?></a>
                                                                    <?php
                                                                    if ($i < ($n - 1))
                                                                        echo ", ";
                                                                }
                                                                ?>
                                                            </span>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>	
                                            <?php
                                        }
                                        if (OpenShopHelper::showPrice() && !$this->item->product_call_for_price) {
                                            ?>
                                            <div style="padding: 0 0 10px 0;">
                                                <div class="product-price" id="product-price">
                                                    <div class="col-md-6 paddingL0">
                                                        <?php
                                                        if (!empty($this->item->product_price_r)) {
                                                            ?>
                                                            <div class="priceBuy col-md-6 paddingL0">
                                                                <?php echo number_format((int) $this->item->product_price, 0, ',', '.') ?><sup>đ</sup>
                                                            </div>
                                                            <div class="priceBuyBo col-md-6 padding0 marginT10">
                                                                <span>
                                                                    <?php echo number_format((int) $this->item->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                                </span>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="priceJ1">
                                                                <?php echo number_format((int) $this->item->product_price, 0, ',', '.') ?><sup>đ</sup>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-6" style="margin-top: 30px;">
                                                        <?php
                                                        echo '<div class="display_inline_block">';
                                                        echo ' <span class="viewDetail" data-toggle="tooltip" title="Lượt xem">' . $this->item->hits . '<i class="fa fa-eye" aria-hidden="true"></i></span>';
                                                        echo ' <span class="buyerDetail" data-toggle="tooltip" title="Lượt mua">' . $this->item->buyer_virtual . '<i class="fa fa-users" aria-hidden="true"></i></span>';
                                                        echo '</div>';
                                                        ?>
                                                        <div class="showSKU display_inline_block">
                                                            Mã SP: <?php echo $this->item->product_sku ?>
                                                        </div>
                                                    </div>
                                                    <h2>
                                                        <?php
                                                        if (OpenShopHelper::getConfigValue('tax') && OpenShopHelper::getConfigValue('display_ex_tax')) {
                                                            ?>
                                                            <small>
                                                                <?php echo JText::_('OPENSHOP_EX_TAX'); ?>:
                                                                <?php
                                                                if ($productPriceArray['salePrice']) {
                                                                    echo $this->currency->format($productPriceArray['salePrice']);
                                                                } else {
                                                                    echo $this->currency->format($productPriceArray['basePrice']);
                                                                }
                                                                ?>
                                                            </small>
                                                            <?php
                                                        }
                                                        ?>
                                                    </h2>
                                                </div>
                                            </div>
                                            <?php
                                            if (count($this->discountPrices)) {
                                                ?>
                                                <div>
                                                    <div class="product-discount-price">
                                                        <?php
                                                        for ($i = 0; $n = count($this->discountPrices), $i < $n; $i++) {
                                                            $discountPrices = $this->discountPrices[$i];
                                                            echo $discountPrices->quantity . ' ' . JText::_('OPENSHOP_OR_MORE') . ' ' . $this->currency->format($this->tax->calculate($discountPrices->price, $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))) . '<br />';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }

                                        if ($this->item->product_call_for_price) {
                                            ?>
                                            <div>
                                                <div class="product-price">
                                                    <?php echo JText::_('OPENSHOP_CALL_FOR_PRICE'); ?>: <?php echo OpenShopHelper::getConfigValue('telephone'); ?>
                                                </div>
                                            </div>
                                            <?php
                                        }

                                        if (count($this->productOptions)) {
                                            ?>
                                            <div class="col-md-12 shortDescription">
                                                <div class="col-md-6 padding0">
                                                    <?php
                                                    //Lượt xem - lượt mua
                                                    echo '<div>';
                                                    echo ' <span class="viewDetail" data-toggle="tooltip" title="Lượt xem">' . $this->item->hits . '<i class="fa fa-eye" aria-hidden="true"></i></span>';
                                                    echo ' <span class="buyerDetail" data-toggle="tooltip" title="Lượt mua">' . $this->item->buyer_virtual . '<i class="fa fa-users" aria-hidden="true"></i></span>';
                                                    echo '</div>';

                                                    //color
                                                    $color = OpenShopHelper::getColorProduct($this->item->id);
                                                    if (count($color)) {
                                                        ?>
                                                        <div class="colorProduct">
                                                            Màu sắc: <?php echo $color->value ?>
                                                            <?php
                                                            if (!empty($color->img_color)) {
                                                                ?>
                                                                <i>
                                                                    <img src="<?php echo OPENSHOP_PATH_IMG_COLOR_HTTP . $color->img_color ?>" alt="<?php echo $color->value ?>" title="<?php echo $color->value ?>" width="20px" height="20px"/>
                                                                </i>
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                        <?php
                                                    }

                                                    //thuộc tích
                                                    $attrPros = OpenShopHelper::getProductAttributes($this->item->id);
                                                    foreach ($attrPros as $attrPro) {
                                                        $attrDetail = OpenShopHelper::getProductAttributeDetail($attrPro->id);

                                                        echo '<div class="attrProDetail">';
                                                        echo $attrDetail->attribute_name . ': ' . $attrPro->value;
                                                        echo '</div>';
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-md-6 padding0 maxHeightSrollZise">
                                                    <table class="table table-bordered table-hover shortDescription">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2">Chọn số lượng</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $sizes = OpenShopHelper::getSizeColor($this->item->id);
                                                            if (count($sizes)) {
                                                                foreach ($sizes as $k_size => $size) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            Sise: <?php echo $size->value ?>
                                                                        </td>
                                                                        <td>
                                                                            <div class="numricLR">
                                                                                <div class="sub-quantity" onclick="subQuantity(<?php echo $size->optionvalue_id ?>,<?php echo $k_size ?>)">
                                                                                    <span>-</span>
                                                                                </div>
                                                                                <input type="number" id="inputQ_<?php echo $k_size; ?>" class="inputNumberProduct inputQ_<?php echo $k_size; ?>" value="0" onchange="checkQuantity(<?php echo $k_size ?>, this.value)"/>
                                                                                <div class="add-quantity" onclick="addQuantity(<?php echo $size->optionvalue_id ?>,<?php echo $k_size ?>)">
                                                                                    <span>+</span>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            } else {
                                                                $k_size = 0;
                                                                ?>
                                                                <tr>
                                                                    <td colspan="2" class="text_center">
                                                                        <div class="numricLR">
                                                                            <div class="sub-quantity" onclick="subQuantity(<?php echo $color->optionvalue_id ?>,<?php echo $k_size ?>)">
                                                                                <span>-</span>
                                                                            </div>
                                                                            <input type="number" id="inputQ_<?php echo $k_size; ?>" class="inputNumberProduct inputQ_<?php echo $k_size; ?>" value="0" onchange="checkQuantity(<?php echo $k_size ?>, this.value)"/>
                                                                            <div class="add-quantity" onclick="addQuantity(<?php echo $color->optionvalue_id ?>,<?php echo $k_size ?>)">
                                                                                <span>+</span>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php
                                        }

//                                        Nút đặt hàng
                                        ?>
                                        <div class="clr"></div>
                                        <div class="col-md-12 padding0 marginT10">
                                            <!--                                            <div class="btn-order display_inline_block"> onclick="showCart()" 
                                                                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                            <?php echo JText::_('OPENSHOP_ADD_TO_CART'); ?>
                                                                                        </div>-->
                                            <div class="btn-buy-now display_inline_block" onclick="showCart('<?php echo $onClickCart ?>')">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                Mua ngay
                                            </div>
                                        </div>
                                        <?php
//                                        /Nút đặt hàng
//                                        sản phẩm khác màu
                                        $idProColorDiff = $this->productColorDifferent;
                                        if (count($idProColorDiff)) {
                                            ?>
                                            <div class="clr"></div>
                                            <div class="productDetail marginT10">
                                                <div class="contentProductColorDiff">
                                                    <?php
                                                    $productColorDiff = OpenShopHelper::getProductInArray($idProColorDiff);
                                                    if (count($productColorDiff)) {
                                                        ?>
                                                        <div class="swiper-container sildeProductColorDiff">
                                                            <div class="loadingProductColorDiff">
                                                                <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="swiper-wrapper productColorDiffNone">
                                                                <?php
                                                                foreach ($productColorDiff as $pcd) {
                                                                    $linkColorDiff = JRoute::_(OpenShopRoute::getProductRoute($pcd->id, OpenShopHelper::getProductCategory($pcd->id)));
                                                                    ?>
                                                                    <div class="swiper-slide productCD">
                                                                        <div class="imgProductColorDiff">
                                                                            <a href="<?php echo $linkColorDiff ?>" target="_blank">
                                                                                <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pcd->product_image ?>" alt="<?php echo $pcd->product_name ?>"  title="<?php echo $pcd->product_name ?>" />
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <!-- Navigation -->
                                                            <div class="jd-nav-button-next">
                                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="jd-nav-button-prev">
                                                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="clr"></div>
                                            <?php
                                        }
//                                        Sản phẩm khác màu
                                        ?>
                                        <div class="row-fluid">
                                            <div class="product-cart clearfix">
                                                <div class="col-md-12 no_margin_left" style="padding: 15px 0px;">
                                                    <input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
                                                    <?php
                                                    if (OpenShopHelper::getConfigValue('show_quantity_box_in_product_page')) {
                                                        ?>
                                                        <div class="input-append input-prepend">
                                                            <label class="btn"><?php echo JText::_('OPENSHOP_QTY'); ?>:</label>
                                                            <span class="openshop-quantity">
                                                                <a class="btn btn-default button-minus spin-down" id="<?php echo $this->item->id; ?>" data="down">-</a>
                                                                <input type="text" class="openshop-quantity-value" id="quantity_<?php echo $this->item->id; ?>" name="quantity" value="1" />
                                                                <a class="btn btn-default button-plus spin-up" id="<?php echo $this->item->id; ?>" data="up">+</a>
                                                            </span>
                                                        </div>
                                                        <?php
                                                    }

                                                    if (OpenShopHelper::isQuoteMode($this->item)) {
                                                        ?>
                                                        <button id="add-to-quote" class="btn btn-primary" type="button"><?php echo JText::_('OPENSHOP_ADD_TO_QUOTE'); ?></button>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <?php
                                                if (OpenShopHelper::getConfigValue('allow_wishlist') || OpenShopHelper::getConfigValue('allow_compare') || OpenShopHelper::getConfigValue('allow_ask_question')) {
                                                    ?>
                                                    <div class="span6">
                                                        <?php
                                                        if (OpenShopHelper::getConfigValue('allow_wishlist')) {
                                                            ?>
                                                            <p><a style="cursor: pointer;" onclick="addToWishList(<?php echo $this->item->id; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>')"><?php echo JText::_('OPENSHOP_ADD_TO_WISH_LIST'); ?></a></p>
                                                            <?php
                                                        }
                                                        if (OpenShopHelper::getConfigValue('allow_compare')) {
                                                            ?>
                                                            <p><a style="cursor: pointer;" onclick="addToCompare(<?php echo $this->item->id; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>')"><?php echo JText::_('OPENSHOP_ADD_TO_COMPARE'); ?></a></p>
                                                            <?php
                                                        }
                                                        if (OpenShopHelper::getConfigValue('allow_ask_question')) {
                                                            ?>
                                                            <p><a style="cursor: pointer;" onclick="askQuestion(<?php echo $this->item->id; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>')"><?php echo JText::_('OPENSHOP_ASK_QUESTION'); ?></a></p>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                        if (OpenShopHelper::getConfigValue('allow_reviews')) {
                                            ?>
                                            <div>
                                                <div class="product-review">
                                                    <p>
                                                        <img src="components/com_openshop/assets/images/stars-<?php echo round(OpenShopHelper::getProductRating($this->item->id)); ?>.png" />
                                                        <a onclick="activeReviewsTab();" style="cursor: pointer;"><?php echo count($this->productReviews) . ' ' . JText::_('OPENSHOP_REVIEWS'); ?></a> | <a onclick="activeReviewsTab();" style="cursor: pointer;"><?php echo JText::_('OPENSHOP_WRITE_A_REVIEW'); ?></a>
                                                    </p>
                                                </div>
                                            </div>	
                                            <?php
                                        }
                                        if (OpenShopHelper::getConfigValue('social_enable')) {
                                            ?>
                                            <div>
                                                <div class="product-share">
                                                    <div class="ps_area clearfix">
                                                        <?php
                                                        if (OpenShopHelper::getConfigValue('show_facebook_button')) {
                                                            ?>
                                                            <div class="ps_facebook_like display_inline_block" style="position: relative;top: -5px;">
                                                                <div class="fb-like" data-send="true" 
                                                                     data-width="<?php echo OpenShopHelper::getConfigValue('button_width', 450); ?>" 
                                                                     data-show-faces="<?php echo OpenShopHelper::getConfigValue('show_faces', 1); ?>" 
                                                                     vdata-font="<?php echo OpenShopHelper::getConfigValue('button_font', 'arial'); ?>" 
                                                                     data-colorscheme="<?php echo OpenShopHelper::getConfigValue('button_theme', 'light'); ?>" 
                                                                     layout="<?php echo OpenShopHelper::getConfigValue('button_layout', 'button_count'); ?>">
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                        if (OpenShopHelper::getConfigValue('show_twitter_button')) {
                                                            ?>
                                                            <div class="ps_twitter display_inline_block">
                                                                <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $uri->toString(); ?>" tw:via="ontwiik" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="horizontal">Tweet</a>
                                                            </div>
                                                            <?php
                                                        }
                                                        if (OpenShopHelper::getConfigValue('show_pinit_button')) {
                                                            ?>
                                                            <div class="ps_pinit display_inline_block">
                                                                <a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode($uri->toString()); ?>&images=<?php echo urlencode(OpenShopHelper::getSiteUrl() . $this->item->thumb_image); ?>&description=<?php echo $this->item->product_name; ?>" count-layout="horizontal" class="pin-it-button">Pin It</a>
                                                            </div>
                                                            <?php
                                                        }
                                                        if (OpenShopHelper::getConfigValue('show_linkedin_button')) {
                                                            ?>
                                                            <div class="ps_linkedin display_inline_block">
                                                                <?php
                                                                if (OpenShopHelper::getConfigValue('linkedin_layout', 'right') == 'no-count') {
                                                                    ?>
                                                                    <script type="IN/Share"></script>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <script type="IN/Share" data-counter="<?php echo OpenShopHelper::getConfigValue('linkedin_layout', 'right'); ?>"></script>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }
                                                        if (OpenShopHelper::getConfigValue('show_google_button')) {
                                                            ?>
                                                            <div class="ps_google display_inline_block">
                                                                <div class="g-plusone"></div>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="clr"></div>
                                <!--Description-->
                                <div class="productDetail">
                                    <div class="titleProDetail">
                                        <div class="titlePD">
                                            Thông tin chi tiết
                                        </div>
                                    </div>
                                    <div class="contentProductDetail">

                                        <p>
                                        <h2 style="text-align: center; font-weight: bold;"><?php echo $this->item->product_name ?></h2>
                                        <div class="descSize" style="padding: 0 20px;">
                                            Kích thước: 
                                            <?php
                                            $db = JFactory::getDBO();
                                            $query = $db->getQuery(TRUE);
                                            $query->select('*')
                                                    ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                                                    ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . ' ON a.option_value_id = b.optionvalue_id')
                                                    ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . ' ON c.option_id = b.option_id')
                                                    ->where('UPPER(c.option_name) = "SIZE"')
                                                    ->where('a.product_id = ' . $this->item->id);
                                            
                                            $proOptionValue = $db->setQuery($query)->loadObjectList();
                                            foreach ($proOptionValue as $k => $sp) {
                                                if ($k == 0) {
                                                    echo $sp->value;
                                                } else {
                                                    echo ' - ' . $sp->value;
                                                }
                                            }
                                            ?>
                                            <div style="padding: 5px 20px;">
                                                <?php
                                                foreach ($proOptionValue as $k => $sp) {
                                                    echo '<strong>' . $sp->value . '</strong>: ' . $sp->desc_size . '<br/>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php echo $this->item->product_desc; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <?php
                                $modules = JModuleHelper::getModules('product_detail_left_top');
                                foreach ($modules as $module) {
                                    echo JModuleHelper::renderModule($module);
                                }
                                ?>

                                <!--sản phẩm liên quan-->
                                <div class="proRelateDetail">
                                    <div class="titleProductRelate">
                                        Sản phẩm liên quan
                                    </div>
                                    <div class="contentProductRelate">
                                        <?php
                                        if (count($this->productRelations)) {
                                            foreach ($this->productRelations as $proReplace) {
                                                $link = JRoute::_(OpenShopRoute::getProductRoute($proReplace->id, OpenShopHelper::getProductCategory($proReplace->id)));
                                                ?>
                                                <div class="proDetailRelate">
                                                    <div class="imgProductDetailRelate">
                                                        <a class="pointer" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($proReplace->id) ?>', '<?php echo $link ?>')">
                                                            <img class="img-responsive lazy" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $proReplace->product_image ?>" alt="<?php echo $proReplace->product_name ?>" title="<?php echo $proReplace->product_name ?>" />
                                                        </a>
                                                    </div>
                                                    <div class="productDeatilRelate">
                                                        <div class="nameProductReplace">
                                                            <a class="pointer" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($proReplace->id) ?>', '<?php echo $link ?>')">
                                                                <h5><?php echo $proReplace->product_name ?></h5>
                                                            </a>
                                                        </div>
                                                        <div class="priceProductReplace">
                                                            <div class="priceBuyProReplace display_inline_block">
                                                                <?php echo number_format((int) $proReplace->product_price, 0, ',', '.') ?><sup>đ</sup>
                                                            </div>
                                                            <?php
                                                            if ($proReplace->product_price_r > 0) {
                                                                ?>
                                                                <div class="priceNotBuyProReplace display_inline_block">
                                                                    <?php echo number_format((int) $proReplace->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        } else {

                                            $productCats = OpenShopHelper::getProductsOfCategory(OpenShopHelper::getProductCategory($this->item->id), '10');
                                            foreach ($productCats as $prcat) {
                                                if ($prcat->id != $this->item->id) {
                                                    $link = JRoute::_(OpenShopRoute::getProductRoute($prcat->id, OpenShopHelper::getProductCategory($prcat->id)));
                                                    ?>
                                                    <div class="proDetailRelate">
                                                        <div class="imgProductDetailRelate">
                                                            <a class="pointer" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($prcat->id) ?>', '<?php echo $link ?>')">
                                                                <img class="img-responsive lazy" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $prcat->product_image ?>" alt="<?php echo $prcat->product_name ?>" title="<?php echo $prcat->product_name ?>" />
                                                            </a>
                                                        </div>
                                                        <div class="productDeatilRelate">
                                                            <div class="nameProductReplace">
                                                                <a class="pointer" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($prcat->id) ?>', '<?php echo $link ?>')">
                                                                    <h5><?php echo $prcat->product_name ?></h5>
                                                                </a>
                                                            </div>
                                                            <div class="priceProductReplace">
                                                                <div class="priceBuyProReplace display_inline_block">
                                                                    <?php echo number_format((int) $prcat->product_price, 0, ',', '.') ?><sup>đ</sup>
                                                                </div>
                                                                <?php
                                                                if ($prcat->product_price_r > 0) {
                                                                    ?>
                                                                    <div class="priceNotBuyProReplace display_inline_block">
                                                                        <?php echo number_format((int) $prcat->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" value="<?php echo JRequest::getVar('catid') ?>" id="catID" />
                </div>
            </div>
        </div>
    </div>



    <form action="/gio-hang.html" method="post" id="formCart">
        <input type="hidden" id="IDProduct" name="IDProduct" value="<?php echo $this->item->id ?>"/>
        <input type="hidden" id="quantityProduct" name="quantityProduct" value=""/>
    </form>

    <input type="hidden" name="review-tab" id="review-tab" value="0" />
</div>

<script>
    jQuery(document).ready(function () {
        jQuery('.colDetailPro img').addClass('img-responsive');

        jQuery('#myTestModalDetail').modal();
    });

    jQuery('#myTestModalDetail').scroll(function () {
        var w = jQuery(this).scrollTop();
        var scroll_cur = 450;
        if (w > scroll_cur) {
            jQuery('.widthFixed').css('display', 'block');
        } else {
            jQuery('.widthFixed').css('display', 'none');
        }
    })


    $("#zoomZoom").elevateZoom({
        gallery: 'showSubImgZoom',
        cursor: 'pointer',
        galleryActiveClass: 'active',
        imageCrossfade: false,
        loadingIcon: '',
        scrollZoom: true
    });

    //pass the images to Fancybox
    $("#zoomZoom").bind("click", function (e) {
        var ez = $('#zoomZoom').data('elevateZoom');
        $.fancybox(ez.getGalleryList());
        return false;
    });

    jQuery('#myTestModalDetail').on('show.bs.modal', function () {
        jQuery('body').addClass('modal-open');
    });
    jQuery('#myTestModalDetail').on('hide.bs.modal', function () {
        jQuery('.zoomContainer').remove();
        jQuery('body').removeClass('modal-open');
        history.pushState({}, "", "<?php echo $urlHistory ?>");
    });

<?php
if (isset($this->token) && !empty($this->token)) {
    ?>
        //load SP
        jQuery('.productColorDiffNone').removeClass('productColorDiffNone');
        jQuery('.loadingProductColorDiff').remove();

        new Swiper('.sildeProductColorDiff', {
            slidesPerView: 6,
            spaceBetween: 5,
            autoplay: 3000,
            //            nextButton: '.jd-nav-button-next',
            //            prevButton: '.jd-nav-button-prev',
            breakpoints: {
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 40
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        });
    <?php
} else {
    ?>
        jQuery(window).bind('load', function () {

            //load SP
            jQuery('.productColorDiffNone').removeClass('productColorDiffNone');
            jQuery('.loadingProductColorDiff').remove();

            new Swiper('.sildeProductColorDiff', {
                slidesPerView: 6,
                spaceBetween: 5,
                autoplay: 3000,
                //            nextButton: '.jd-nav-button-next',
                //            prevButton: '.jd-nav-button-prev',
                breakpoints: {
                    1024: {
                        slidesPerView: 4,
                        spaceBetween: 40
                    },
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    },
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    }
                }
            });

        });
    <?php
}
?>

//    jQuery('.sildeProductColorDiff').mouseover(function () {
//        $('.jd-nav-button-next').css('opacity', '1');
//        $('.jd-nav-button-prev').css('opacity', '1');
//    });
//    jQuery('.sildeProductColorDiff').mouseout(function () {
//        $('.jd-nav-button-next').css('opacity', '0');
//        $('.jd-nav-button-prev').css('opacity', '0');
//    });


</script>
