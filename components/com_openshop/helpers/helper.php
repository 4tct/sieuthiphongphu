<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopHelper {

    /**
     *
     * Function to get configuration object
     */
    public static function getConfig() {
        static $config;
        if (is_null($config)) {
            $config = new stdClass();
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('config_key, config_value')
                    ->from('#__openshop_configs');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            foreach ($rows as $row) {
                $config->{$row->config_key} = $row->config_value;
            }
        }
        return $config;
    }

    /**
     * 
     * Function to get weight ids
     * @return array
     */
    public static function getWeightIds() {
        static $weightIds;
        if (is_null($weightIds)) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_weights');
            $db->setQuery($query);
            $weightIds = $db->loadColumn();
        }
        return $weightIds;
    }

    /**
     *
     * Function to get length ids
     * @return array
     */
    public static function getLengthIds() {
        static $lengthIds;
        if (is_null($lengthIds)) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__openshop_lengths');
            $db->setQuery($query);
            $lengthIds = $db->loadColumn();
        }
        return $lengthIds;
    }

    /**
     * Function to check if joomla is version 3 or not
     * @param number $minor
     * @return boolean
     */
    public static function isJ3($minor = 0) {
        static $status;
        if (!isset($status)) {
            if (version_compare(JVERSION, '3.' . $minor . '.0', 'ge')) {
                $status = true;
            } else {
                $status = false;
            }
        }
        return $status;
    }

    /**
     * 
     * Function to check if is mobile or not
     * @return boolean
     */
    public static function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    /**
     *
     * Function to get value of configuration variable
     * @param string $configKey
     * @param string $default
     * @return string
     */
    public static function getConfigValue($configKey, $default = null) {
        static $configValues;
        if (!isset($configValues["$configKey"])) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('config_value')
                    ->from('#__openshop_configs')
                    ->where('config_key = "' . $configKey . '"');
            $db->setQuery($query);
            $configValues[$configKey] = $db->loadResult();
        }
        return $configValues[$configKey] ? $configValues[$configKey] : $default;
    }

    public static function getNameImagesColor($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('img_color')
                ->from('#__openshop_optionvaluedetails')
                ->where('optionvalue_id = ' . $id);
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * Get the invoice number for an order
     */
    public static function getInvoiceNumber() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('MAX(invoice_number)')
                ->from('#__openshop_orders');
        if (self::getConfigValue('reset_invoice_number')) {
            $query->where('YEAR(created_date) = YEAR(CURDATE())');
        }
        $db->setQuery($query);
        $invoiceNumber = intval($db->loadResult());
        if (!$invoiceNumber) {
            $invoiceNumber = intval(self::getConfigValue('invoice_start_number'));
            if (!$invoiceNumber)
                $invoiceNumber = 1;
        }
        else {
            $invoiceNumber++;
        }
        return $invoiceNumber;
    }

    /**
     * Format invoice number
     * @param string $invoiceNumber
     * @param Object $config
     */
    public static function formatInvoiceNumber($invoiceNumber) {
        return str_replace('[YEAR]', date('Y'), self::getConfigValue('invoice_prefix')) . str_pad($invoiceNumber, self::getConfigValue('invoice_number_length') ? self::getConfigValue('invoice_number_length') : 5, '0', STR_PAD_LEFT);
    }

    /**
     * Get request data, used for RADList model
     *
     */
    public static function getRequestData() {
        $request = $_REQUEST;
        //Remove cookie vars from request
        $cookieVars = array_keys($_COOKIE);
        if (count($cookieVars)) {
            foreach ($cookieVars as $key) {
                if (!isset($_POST[$key]) && !isset($_GET[$key])) {
                    unset($request[$key]);
                }
            }
        }
        if (isset($request['start']) && !isset($request['limitstart'])) {
            $request['limitstart'] = $request['start'];
        }
        if (!isset($request['limitstart'])) {
            $request['limitstart'] = 0;
        }
        return $request;
    }

    public static function getCategory($categoryId, $processImage = true, $checkPermission = false) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*, b.category_name, b.category_alias, b.category_desc, b.category_page_title, b.category_page_heading, b.meta_key, b.meta_desc')
                ->from('#__openshop_categories AS a')
                ->innerJoin('#__openshop_categorydetails AS b ON a.id = b.category_id')
                ->where('a.id = ' . intval($categoryId))
                ->where('a.published = 1')
                ->where('b.language = "' . JFactory::getLanguage()->getTag() . '"');
        if ($checkPermission) {
            //Check viewable of customer groups
            $user = JFactory::getUser();
            if ($user->get('id')) {
                $customer = new OpenShopCustomer();
                $customerGroupId = $customer->getCustomerGroupId();
            } else {
                $customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
            }
            if (!$customerGroupId)
                $customerGroupId = 0;
            $query->where('((a.category_customergroups = "") OR (a.category_customergroups IS NULL) OR (a.category_customergroups = "' . $customerGroupId . '") OR (a.category_customergroups LIKE "' . $customerGroupId . ',%") OR (a.category_customergroups LIKE "%,' . $customerGroupId . ',%") OR (a.category_customergroups LIKE "%,' . $customerGroupId . '"))');
        }
        $db->setQuery($query);
        $category = $db->loadObject();
        if (is_object($category) && $processImage) {
//            $imageSizeFunction = OpenShopHelper::getConfigValue('category_image_size_function', 'resizeImage');
//            if ($category->category_image && JFile::exists(JPATH_ROOT . '/images/com_openshop/categories/' . $category->category_image)) {
//                $image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($category->category_image, JPATH_ROOT . '/images/com_openshop/categories/', OpenShopHelper::getConfigValue('image_category_width'), OpenShopHelper::getConfigValue('image_category_height')));
//            } else {
//                $image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/categories/', OpenShopHelper::getConfigValue('image_category_width'), OpenShopHelper::getConfigValue('image_category_height')));
//            }
//            $category->image = JUri::base(true) . '/images/com_openshop/categories/resized/' . $image;
        }
        return $category;
    }

    public static function getBrand($id, $processImage = true, $checkPermission = false) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*, b.brand_name, b.brand_alias, b.brand_desc, b.brand_page_title, b.brand_page_heading')
                ->from('#__openshop_brands AS a')
                ->innerJoin('#__openshop_branddetails AS b ON (a.id = b.brand_id)')
                ->where('a.id = ' . (int) $id)
                ->where('b.language = ' . $db->quote(JFactory::getLanguage()->getTag()));
        if ($checkPermission) {
            //Check viewable of customer groups
            $user = JFactory::getUser();
            if ($user->get('id')) {
                $customer = new OpenShopCustomer();
                $customerGroupId = $customer->getCustomerGroupId();
            } else {
                $customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
            }
            if (!$customerGroupId)
                $customerGroupId = 0;
            $query->where('((a.brand_customergroups = "") OR (a.brand_customergroups IS NULL) OR (a.brand_customergroups = "' . $customerGroupId . '") OR (a.brand_customergroups LIKE "' . $customerGroupId . ',%") OR (a.brand_customergroups LIKE "%,' . $customerGroupId . ',%") OR (a.brand_customergroups LIKE "%,' . $customerGroupId . '"))');
        }
        $db->setQuery($query);
        $brand = $db->loadObject();
        if ($brand && $processImage) {
            $imageSizeFunction = OpenShopHelper::getConfigValue('brand_image_size_function', 'resizeImage');
            if ($brand->brand_image && JFile::exists(JPATH_ROOT . '/images/com_openshop/brands/' . $brand->brand_image)) {
                $image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($brand->brand_image, JPATH_ROOT . '/images/com_openshop/brands/', OpenShopHelper::getConfigValue('image_brand_width'), OpenShopHelper::getConfigValue('image_brand_height')));
            } else {
                $image = call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array('no-image.png', JPATH_ROOT . '/images/com_openshop/brands/', OpenShopHelper::getConfigValue('image_brand_width'), OpenShopHelper::getConfigValue('image_brand_height')));
            }
            $brand->image = JUri::base(true) . '/images/com_openshop/brands/resized/' . $image;
        }
        return $brand;
    }

    /**
     * Get the associations.
     *
     */
    public static function getAssociations($id, $view = 'product') {
        $langCode = JFactory::getLanguage()->getTag();
        $associations = array();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($view . '_id, language')
                ->from('#__openshop_' . $view . 'details')
                ->where($view . '_id = ' . intval($id))
                ->where('language != "' . $langCode . '"');
        $db->setQuery($query);

        try {
            $items = $db->loadObjectList('language');
        } catch (RuntimeException $e) {
            throw new Exception($e->getMessage(), 500);
        }

        if ($items) {
            foreach ($items as $tag => $item) {
                $associations[$tag] = $item;
            }
        }
        return $associations;
    }

    /**
     * 
     * Function to update currencies
     * @param boolean $force
     * @param int $timePeriod
     * @param string $timeUnit
     */
    public static function updateCurrencies($force = false, $timePeriod = 1, $timeUnit = 'day') {
        if (extension_loaded('curl')) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            if ($force) {
                $query->select('*')
                        ->from('#__openshop_currencies')
                        ->where('currency_code != ' . $db->quote(self::getConfigValue('default_currency_code')));
            } else {
                $query->select('*')
                        ->from('#__openshop_currencies')
                        ->where('currency_code != ' . $db->quote(self::getConfigValue('default_currency_code')))
                        ->where('modified_date <= ' . $db->quote(date('Y-m-d H:i:s', strtotime('-' . (int) $timePeriod . ' ' . $timeUnit))));
            }
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            if (count($rows)) {
                $data = array();
                foreach ($rows as $row) {
                    $data[] = self::getConfigValue('default_currency_code') . $row->currency_code . '=X';
                }
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, 'http://download.finance.yahoo.com/d/quotes.csv?s=' . implode(',', $data) . '&f=sl1&e=.csv');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $content = curl_exec($curl);
                curl_close($curl);
                $lines = explode("\n", trim($content));
                foreach ($lines as $line) {
                    $currency = substr($line, 4, 3);
                    $value = substr($line, 11, 6);
                    if ((float) $value) {
                        $query->clear();
                        $query->update('#__openshop_currencies')
                                ->set('exchanged_value = ' . (float) $value)
                                ->set('modified_date = ' . $db->quote(date('Y-m-d H:i:s')))
                                ->where('currency_code = ' . $db->quote($currency));
                        $db->setQuery($query);
                        $db->query();
                    }
                }
            }
            $query->clear();
            $query->update('#__openshop_currencies')
                    ->set('exchanged_value = 1.00000')
                    ->set('modified_date = ' . $db->quote(date('Y-m-d H:i:s')))
                    ->where('currency_code = ' . $db->quote(self::getConfigValue('default_currency_code')));
            $db->setQuery($query);
            $db->query();
        }
    }

    /**
     * 
     * Function to update hits for category/brand/product
     * @param int $id
     * @param string $element
     */
    public static function updateHits($id, $element) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->update('#__openshop_' . $element)
                ->set('hits = hits + 1')
                ->where('id = ' . intval($id));
        $db->setQuery($query);
        $db->query();
    }

    /*
     * Function to update buyer for products when order
     */

    public static function updateBuyerProduct($idPro) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__openshop_products'))
                ->set('buyer_virtual = buyer_virtual + 1')
                ->where('id = ' . intval($idPro));
        $db->setQuery($query)->execute();
    }

    /**
     * 
     * Function to get name of a specific stock status
     * @param int $stockStatusId
     * @param string $langCode
     * @return string
     */
    public static function getStockStatusName($stockStatusId, $langCode) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('stockstatus_name')
                ->from('#__openshop_stockstatusdetails')
                ->where('stockstatus_id = ' . intval($stockStatusId))
                ->where('language = "' . $langCode . '"');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     *
     * Function to get name of a specific order status
     * @param int $orderStatusId
     * @param string $langCode
     * @return string
     */
    public static function getOrderStatusName($orderStatusId, $langCode) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('orderstatus_name')
                ->from('#__openshop_orderstatusdetails')
                ->where('orderstatus_id = ' . intval($orderStatusId))
                ->where('language = "' . $langCode . '"');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     *
     * Function to get unit of a specific length
     * @param int $lengthId
     * @param string $langCode
     * @return string
     */
    public static function getLengthUnit($lengthId, $langCode) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('length_unit')
                ->from('#__openshop_lengthdetails')
                ->where('length_id = ' . intval($lengthId))
                ->where('language = "' . $langCode . '"');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     *
     * Function to get unit of a specific weight
     * @param int $weightId
     * @param string $langCode
     * @return string
     */
    public static function getWeightUnit($weightId, $langCode) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('weight_unit')
                ->from('#__openshop_weightdetails')
                ->where('weight_id = ' . intval($weightId))
                ->where('language = "' . $langCode . '"');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * 
     * Function to get payment title
     * @param string $paymentName
     * @return string
     */
    public static function getPaymentTitle($paymentName) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('title')
                ->from('#__openshop_payments')
                ->where('name = "' . $paymentName . '"');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     *
     * Function to get shipping title
     * @param string $shippingName
     * @return string
     */
    public static function getShippingTitle($shippingName) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('title')
                ->from('#__openshop_shippings')
                ->where('name = "' . $shippingName . '"');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * 
     * Function to get all available languages
     * @return languages object list
     */
    public static function getLanguages() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('lang_id, lang_code, title')
                ->from('#__languages')
                ->where('published = 1')
                ->order('ordering');
        $db->setQuery($query);
        $languages = $db->loadObjectList();
        return $languages;
    }

    /**
     *
     * Function to get flags for languages
     */
    public static function getLanguageData() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $languageData = array();
        $query->select('image, lang_code, title')
                ->from('#__languages')
                ->where('published = 1')
                ->order('ordering');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        for ($i = 0; $n = count($rows), $i < $n; $i++) {
            $languageData['flag'][$rows[$i]->lang_code] = $rows[$i]->image . '.png';
            $languageData['title'][$rows[$i]->lang_code] = $rows[$i]->title;
        }
        return $languageData;
    }

    /**
     * 
     * Function to get active language
     */
    public static function getActiveLanguage() {
        $langCode = JFactory::getLanguage()->getTag();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__languages')
                ->where('lang_code = ' . $db->quote($langCode));
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     * 
     * Function to get attached lang link
     * @return string
     */
    public static function getAttachedLangLink() {
        $attachedLangLink = '';
        if (JLanguageMultilang::isEnabled()) {
            $activeLanguage = self::getActiveLanguage();
            $attachedLangLink = '&lang=' . $activeLanguage->sef;
        }
        return $attachedLangLink;
    }

    /**
     *
     * Function to get attribute groups
     * @return attribute groups object list
     */
    public static function getAttributeGroups($langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, b.attributegroup_name')
                ->from('#__openshop_attributegroups AS a')
                ->innerJoin('#__openshop_attributegroupdetails AS b ON (a.id = b.attributegroup_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . $langCode . '"')
                ->order('a.ordering');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     *
     * Function to get attributes for a specific products
     * @param int $productId
     * @param int $attributeGroupId
     * @return attribute object list
     */
    public static function getAttributes($productId, $attributeGroupId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('ad.attribute_name, pad.value')
                ->from('#__openshop_attributes AS a')
                ->innerJoin('#__openshop_attributedetails AS ad ON (a.id = ad.attribute_id)')
                ->innerJoin('#__openshop_productattributes AS pa ON (a.id = pa.attribute_id)')
                ->innerJoin('#__openshop_productattributedetails AS pad ON (pa.id = pad.productattribute_id)')
                ->where('a.attributegroup_id = ' . intval($attributeGroupId))
                ->where('a.published = 1')
                ->where('pa.published = 1')
                ->where('pa.product_id = ' . intval($productId))
                ->where('ad.language = "' . $langCode . '"')
                ->where('pad.language = "' . $langCode . '"')
                ->order('a.ordering, pad.value');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     *
     * Function to get attribute group for a specific attribute
     * @param unknown $attributeId
     * @return Ambigous <mixed, NULL>
     */
    public static function getAttributeAttributeGroup($attributeId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.attributegroup_id, b.attributegroup_name')
                ->from('#__openshop_attributes AS a')
                ->innerJoin('#__openshop_attributegroupdetails AS b ON (a.attributegroup_id = b.attributegroup_id)')
                ->where('a.id = ' . intval($attributeId))
                ->where('b.language = "' . $langCode . '"');
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     * 
     * Function to get Categories
     * @param int $categoryId
     * @return categories object list
     */
    public static function getCategories($categoryId = 0, $langCode = '', $checkPermission = false) {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.category_parent_id, a.category_image, b.category_name, b.category_desc')
                ->from('#__openshop_categories AS a')
                ->innerJoin('#__openshop_categorydetails AS b ON (a.id = b.category_id)')
                ->where('a.category_parent_id = ' . intval($categoryId))
                ->where('a.published = 1')
                ->where('b.language = "' . $langCode . '"')
                ->order('a.ordering');
        if ($checkPermission) {
            //Check viewable of customer groups
            $user = JFactory::getUser();
            if ($user->get('id')) {
                $customer = new OpenShopCustomer();
                $customerGroupId = $customer->getCustomerGroupId();
            } else {
                $customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
            }
            if (!$customerGroupId)
                $customerGroupId = 0;
            $query->where('((a.category_customergroups = "") OR (a.category_customergroups IS NULL) OR (a.category_customergroups = "' . $customerGroupId . '") OR (a.category_customergroups LIKE "' . $customerGroupId . ',%") OR (a.category_customergroups LIKE "%,' . $customerGroupId . ',%") OR (a.category_customergroups LIKE "%,' . $customerGroupId . '"))');
        }
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     * 
     * Function to get all child categories levels of a category
     * @param int $id
     * @return array
     */
    public static function getAllChildCategories($id) {
        $data = array();
        if ($results = self::getCategories($id, '', true)) {
            foreach ($results as $result) {
                $data[] = $result->id;
                $subCategories = self::getAllChildCategories($result->id);
                if ($subCategories) {
                    $data = array_merge($data, $subCategories);
                }
            }
        }
        return $data;
    }

    /**
     * 
     * Function to get number products for a specific category
     * @param int $categoryId
     * @return int
     */
    public static function getNumCategoryProducts($categoryId, $allLevels = false) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        if ($allLevels) {
            $categoryIds = array_merge(array($categoryId), OpenShopHelper::getAllChildCategories($categoryId));
        } else {
            $categoryIds = array($categoryId);
        }
        $query->select('COUNT(DISTINCT(a.id))')
                ->from('#__openshop_products AS a')
                ->innerJoin('#__openshop_productcategories AS b ON (a.id = b.product_id)')
                ->where('a.published = 1')
                ->where('b.category_id IN (' . implode(',', $categoryIds) . ')');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     *
     * Function to get list of parent categories
     * @param int $categoryId
     * @return array of object
     */
    public static function getParentCategories($categoryId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $parentCategories = array();
        while (true) {
            $query->clear();
            $query->select('a.id, a.category_parent_id, b.category_name')
                    ->from('#__openshop_categories AS a')
                    ->innerJoin('#__openshop_categorydetails AS b ON (a.id = b.category_id)')
                    ->where('a.id = ' . intval($categoryId))
                    ->where('a.published = 1')
                    ->where('b.language = "' . $langCode . '"');
            $db->setQuery($query);
            $row = $db->loadObject();
            if ($row) {
                $parentCategories[] = $row;
                $categoryId = $row->category_parent_id;
            } else {
                break;
            }
        }
        return $parentCategories;
    }

    /**
     *
     * Function to get values for a specific option
     * @param int $optionId
     */
    public static function getOptionValues($optionId, $langCode = '', $multipleLanguage = 'true') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $languages = self::getLanguages();
        if (JLanguageMultilang::isEnabled() && count($languages) > 1 && $multipleLanguage) {
            $query->select('*')
                    ->from('#__openshop_optionvalues')
                    ->where('option_id = ' . intval($optionId))
                    ->order('ordering');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            if (count($rows)) {
                for ($i = 0; $n = count($rows), $i < $n; $i++) {
                    $query->clear();
                    $query->select('*')
                            ->from('#__openshop_optionvaluedetails')
                            ->where('option_id = ' . intval($optionId))
                            ->where('optionvalue_id = ' . intval($rows[$i]->id));
                    $db->setQuery($query);
                    $detailsRows = $db->loadObjectList('language');
                    if (count($detailsRows)) {
                        foreach ($detailsRows as $language => $detailsRow) {
                            $rows[$i]->{'optionvaluedetails_id_' . $language} = $detailsRow->id;
                            $rows[$i]->{'value_' . $language} = $detailsRow->value;
                        }
                    }
                }
            }
        } else {
            $query->select('ov.*, ovd.id AS optionvaluedetails_id, ovd.value, ovd.language, ovd.img_color')
                    ->from('#__openshop_optionvalues AS ov')
                    ->innerJoin('#__openshop_optionvaluedetails AS ovd ON (ov.id = ovd.optionvalue_id)')
                    ->where('ov.option_id = ' . intval($optionId))
                    ->where('ovd.language = "' . $langCode . '"')
                    ->order('ov.ordering');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
        }
        return $rows;
    }

    /**
     *
     * Function to get information for a specific product
     * @param int $productId
     */
    public static function getProduct($productId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*, b.product_name, b.product_alias, b.product_desc, b.product_short_desc, b.meta_key, b.meta_desc')
                ->from('#__openshop_products AS a')
                ->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
                ->where('b.language = "' . $langCode . '"')
                ->where('a.id = ' . intval($productId));
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     * 
     * Function to get categories for a specific product
     * @param int $productId        	
     */
    public static function getProductCategories($productId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('c.id, cd.category_name')
                ->from('#__openshop_categories AS c')
                ->innerJoin('#__openshop_categorydetails AS cd ON (c.id = cd.category_id)')
                ->innerJoin('#__openshop_productcategories AS pc ON (c.id = pc.category_id)')
                ->where('pc.product_id = ' . intval($productId))
                ->where('cd.language = "' . $langCode . '"');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     * 
     * Function to get category id for a specific product
     * @param int $productId
     * @return int
     */
    public static function getProductCategory($productId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.category_id')
                ->from('#__openshop_productcategories AS a')
                ->innerJoin('#__openshop_categories AS b ON (a.category_id = b.id)')
                ->where('a.product_id = ' . intval($productId))
                ->where('b.published = 1');
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     *
     * Function to get brand for a specific product
     * @param int $productId
     * @return brand object
     */
    public static function getProductBrand($productId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('m.id, m.brand_email, md.brand_name')
                ->from('#__openshop_products AS p')
                ->innerJoin('#__openshop_brands AS m ON (p.brand_id = m.id)')
                ->innerJoin('#__openshop_branddetails AS md ON (m.id = md.brand_id)')
                ->where('p.id = ' . intval($productId));
        $db->setQuery($query);
        $row = $db->loadObject();
        return $row;
    }

    /**
     *
     * Function to get related products for a specific product
     * @param int $productId
     */
    public static function getProductRelations($productId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('p.*, pd.product_name, pd.product_alias, pd.product_desc, pd.product_short_desc, pd.meta_key, pd.meta_desc')
                ->from('#__openshop_products AS p')
                ->innerJoin('#__openshop_productdetails AS pd ON (p.id = pd.product_id)')
                ->innerJoin('#__openshop_productrelations AS pr ON (p.id = pr.related_product_id)')
                ->where('pr.product_id = ' . intval($productId))
                ->where('pd.language = "' . $langCode . '"')
                ->order('p.ordering');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     *
     * Function to get product downloads for a specific product
     * @param int $productId
     */
    public static function getProductDownloads($productId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.filename, a.total_downloads_allowed, b.download_name')
                ->from('#__openshop_downloads AS a')
                ->innerJoin('#__openshop_downloaddetails AS b ON (a.id = b.download_id)')
                ->innerJoin('#__openshop_productdownloads AS c ON (a.id = c.download_id)')
                ->where('c.product_id = ' . intval($productId))
                ->where('b.language = "' . $langCode . '"');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     * 
     * Function to reviews for a specific product
     * @param int $productId
     * @return reviews object list
     */
    public static function getProductReviews($productId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_reviews')
                ->where('product_id = ' . intval($productId))
                ->where('published = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     * 
     * Function to get average rating for a specific product 
     * @param int $productId
     * @return average rating
     */
    public static function getProductRating($productId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('AVG(rating) as rating')
                ->from('#__openshop_reviews')
                ->where('product_id = ' . intval($productId))
                ->where('published = 1');
        $db->setQuery($query);
        $rating = $db->loadResult();
        return $rating;
    }

    /**
     *
     * Function to get attributes for a specific product
     * @param int $productId
     */
    public static function getProductAttributes($productId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $languages = self::getLanguages();
        if (JLanguageMultilang::isEnabled() && count($languages) > 1) {
            $query->select('a.id, pa.id AS productattribute_id, pa.published')
                    ->from('#__openshop_attributes AS a')
                    ->innerJoin('#__openshop_productattributes AS pa ON (a.id = pa.attribute_id)')
                    ->where('pa.product_id = ' . intval($productId))
                    ->order('a.ordering');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            if (count($rows)) {
                for ($i = 0; $n = count($rows), $i < $n; $i++) {
                    $query->clear();
                    $query->select('*')
                            ->from('#__openshop_productattributedetails')
                            ->where('productattribute_id = ' . intval($rows[$i]->productattribute_id));
                    $db->setQuery($query);
                    $detailsRows = $db->loadObjectList('language');
                    if (count($detailsRows)) {
                        foreach ($detailsRows as $language => $detailsRow) {
                            $rows[$i]->{'productattributedetails_id_' . $language} = $detailsRow->id;
                            $rows[$i]->{'value_' . $language} = $detailsRow->value;
                        }
                    }
                }
            }
        } else {
            $query->select('a.id, pa.id AS productattribute_id, pa.published, pad.id AS productattributedetails_id ,pad.value')
                    ->from('#__openshop_attributes AS a')
                    ->innerJoin('#__openshop_productattributes AS pa ON (a.id = pa.attribute_id)')
                    ->innerJoin('#__openshop_productattributedetails AS pad ON (pa.id = pad.productattribute_id)')
                    ->where('pa.product_id = ' . intval($productId))
                    ->where('pad.language = "' . $langCode . '"')
                    ->order('a.ordering');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
        }
        return $rows;
    }

    public static function getProductAttributeDetail($idAttribute) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_attributedetails'))
                ->where('attribute_id = ' . $idAttribute);
        return $db->setQuery($query)->loadObject();
    }

    /**
     *
     * Function to get options for a specific product
     * @param int $productId
     */
    public static function getProductOptions($productId, $langCode = '') {
        if ($langCode == '') {
            $langCode = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('o.id, o.option_type, o.option_image, od.option_name, od.option_desc, po.required, po.id AS product_option_id')
                ->from('#__openshop_options AS o')
                ->innerJoin('#__openshop_optiondetails AS od ON (o.id = od.option_id)')
                ->innerJoin('#__openshop_productoptions AS po ON (o.id = po.option_id)')
                ->where('po.product_id = ' . intval($productId))
                ->where('od.language = "' . $langCode . '"')
                ->order('o.ordering');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     * 
     * Function to get option values
     * @param int $productId
     * @param int $optionId
     * @return option value object list
     */
    public static function getProductOptionValues($productId, $optionId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('pov.*')
                ->from('#__openshop_productoptionvalues AS pov')
                ->where('product_id = ' . intval($productId))
                ->where('option_id = ' . intval($optionId))
                ->order('id');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        //Resize option image
        $imagePath = JPATH_ROOT . '/images/com_openshop/options/';
        $imageSizeFunction = OpenShopHelper::getConfigValue('option_image_size_function', 'resizeImage');
        return $rows;
    }

    public static function getProductOptionValueDetails($productId, $optionName) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,b.optionvalue_id,b.value')
                ->from($db->quoteName('#__openshop_productoptionvalues', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . ' ON a.option_value_id = b.optionvalue_id ')
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . ' ON c.option_id = b.option_id ')
                ->where('a.product_id = ' . $productId)
                ->where('UPPER(c.option_name) = "' . strtoupper($optionName) . '"');
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     *
     * Function to get images for a specific product
     * @param int $productId
     */
    public static function getProductImages($productId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('pi.*')
                ->from('#__openshop_productimages AS pi')
                ->where('product_id = ' . intval($productId))
                ->order('pi.ordering');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     * 
     * Function to get tags for a specific product
     * @param int $productId
     */
    public static function getProductTags($productId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*')
                ->from('#__openshop_tags AS a')
                ->innerJoin('#__openshop_producttags AS b ON (a.id = b.tag_id)')
                ->where('a.published = 1')
                ->where('b.product_id = ' . intval($productId));
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     *  
     * Function to resize image 
     * @param string $filename
     * @param string $imagePath
     * @param int $width
     * @param int $height
     * @return void|string
     */
    public static function resizeImage($filename, $imagePath, $width, $height) {
        if (!file_exists($imagePath . $filename) || !is_file($imagePath . $filename)) {
            return;
        }
        $info = pathinfo($filename);
        $extension = $info['extension'];        //check type file
        $oldImage = $filename;
        $newImage = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
        if (!file_exists($imagePath . '/resized/' . $newImage) || (filemtime($imagePath . $oldImage) > filemtime($imagePath . '/resized/' . $newImage))) {

            list($width_orig, $height_orig) = getimagesize($imagePath . $oldImage);
            if ($width_orig != $width || $height_orig != $height) {
                $image = new OpenShopImage($imagePath . $oldImage);
                $image->resize($width, $height);
                $image->save($imagePath . '/resized/' . $newImage);
            } else {
                copy($imagePath . $oldImage, $imagePath . '/resized/' . $newImage);
            }
        }
        return $newImage;
    }

    /**
     *  
     * Function to resize image 
     * @param string $filename
     * @param string $imagePath
     * @param int $width
     * @param int $height
     * @return void|string
     */
    public static function resizeImageURL($filename, $imagePath, $width, $height) {
        if (!file_exists($imagePath . $filename) || !is_file($imagePath . $filename)) {
            return;
        }
        $info = pathinfo($filename);
        $extension = $info['extension'];        //check type file
        $oldImage = $filename;
        $newImage = substr($filename, 0, strrpos($filename, '.')) . '.' . $extension;
        if (!file_exists($imagePath . $newImage) || (filemtime($imagePath . $oldImage) > filemtime($imagePath . $newImage))) {

            list($width_orig, $height_orig) = getimagesize($imagePath . $oldImage);
            if ($width_orig != $width || $height_orig != $height) {
                $image = new OpenShopImage($imagePath . $oldImage);
                $image->resize($width, $height);
                $image->save($imagePath . $newImage);
            } else {
                copy($imagePath . $oldImage, $imagePath . $newImage);
            }
        }
        return $newImage;
    }

    /**
     *
     * Function to cropsize image
     * @param string $filename
     * @param string $imagePath
     * @param int $width
     * @param int $height
     * @return void|string
     */
    public static function cropsizeImage($filename, $imagePath, $width, $height) {
        if (!file_exists($imagePath . $filename) || !is_file($imagePath . $filename)) {
            return;
        }
        $info = pathinfo($filename);
        $extension = $info['extension'];
        $oldImage = $filename;
        $newImage = substr($filename, 0, strrpos($filename, '.')) . '-cr-' . $width . 'x' . $height . '.' . $extension;
        if (!file_exists($imagePath . '/resized/' . $newImage) || (filemtime($imagePath . $oldImage) > filemtime($imagePath . '/resized/' . $newImage))) {
            list($width_orig, $height_orig) = getimagesize($imagePath . $oldImage);
            if ($width_orig != $width || $height_orig != $height) {
                $image = new OpenShopImage($imagePath . $oldImage);
                $image->cropsize($width, $height);
                $image->save($imagePath . '/resized/' . $newImage);
            } else {
                copy($imagePath . $oldImage, $imagePath . '/resized/' . $newImage);
            }
        }
        return $newImage;
    }

    /**
     *
     * Function to max size image
     * @param string $filename
     * @param string $imagePath
     * @param int $width
     * @param int $height
     * @return void|string
     */
    public static function maxsizeImage($filename, $imagePath, $width, $height) {
        $maxsize = ($width > $height) ? $width : $height;
        if (!file_exists($imagePath . $filename) || !is_file($imagePath . $filename)) {
            return;
        }
        $info = pathinfo($filename);
        $extension = $info['extension'];
        $oldImage = $filename;
        $newImage = substr($filename, 0, strrpos($filename, '.')) . '-max-' . $width . 'x' . $height . '.' . $extension;
        if (!file_exists($imagePath . '/resized/' . $newImage) || (filemtime($imagePath . $oldImage) > filemtime($imagePath . '/resized/' . $newImage))) {
            $image = new OpenShopImage($imagePath . $oldImage);
            $image->maxsize($maxsize);
            $image->save($imagePath . '/resized/' . $newImage);
        }
        return $newImage;
    }

    /**
     *
     * Function to get discount for a specific product
     * @param int $productId        	
     */
    public static function getProductDiscounts($productId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('pd.*')
                ->from('#__openshop_productdiscounts AS pd')
                ->innerJoin('#__openshop_customergroups AS cg ON (pd.customergroup_id = cg.id)')
                ->where('pd.product_id = ' . intval($productId))
                ->order('pd.priority');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     *
     * Function to get special for a specific product
     * @param int $productId        	
     */
    public static function getProductSpecials($productId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('ps.*')
                ->from('#__openshop_productspecials AS ps')
                ->innerJoin('#__openshop_customergroups AS cg ON (ps.customergroup_id = cg.id)')
                ->where('ps.product_id = ' . intval($productId))
                ->order('ps.priority');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    /**
     * 
     * Function to get discount price for a specific product
     * @param int $productId
     * @return price
     */
    public static function getDiscountPrice($productId) {
        $user = JFactory::getUser();
        if ($user->get('id')) {
            $customer = new OpenShopCustomer();
            $customerGroupId = $customer->getCustomerGroupId();
        } else {
            $customerGroupId = self::getConfigValue('customergroup_id');
        }
        if (!$customerGroupId)
            $customerGroupId = 0;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('price')
                ->from('#__openshop_productdiscounts')
                ->where('product_id = ' . intval($productId))
                ->where('published = 1')
                ->where('customergroup_id = ' . intval($customerGroupId))
                ->where('date_start <= "' . date('Y-m-d H:i:s') . '"')
                ->where('date_end >= "' . date('Y-m-d H:i:s') . '"')
                ->where('quantity = 1')
                ->order('priority');
        $db->setQuery($query);
        $discountPrice = $db->loadResult();
        return $discountPrice;
    }

    /**
     *
     * Function to get discount prices for a specific product
     * @param int $productId
     * @return prices
     */
    public static function getDiscountPrices($productId) {
        $user = JFactory::getUser();
        if ($user->get('id')) {
            $customer = new OpenShopCustomer();
            $customerGroupId = $customer->getCustomerGroupId();
        } else {
            $customerGroupId = self::getConfigValue('customergroup_id');
        }
        if (!$customerGroupId)
            $customerGroupId = 0;

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('quantity, price')
                ->from('#__openshop_productdiscounts')
                ->where('product_id = ' . intval($productId))
                ->where('published = 1')
                ->where('customergroup_id = ' . intval($customerGroupId))
                ->where('(date_start <= "' . date('Y-m-d H:i:s') . '" OR date_start = "0000-00-00 00:00:00")')
                ->where('(date_end >= "' . date('Y-m-d H:i:s') . '" OR date_end = "0000-00-00 00:00:00")')
                ->where('quantity > 1')
                ->order('priority');
        $db->setQuery($query);
        $discountPrices = $db->loadObjectList();
        return $discountPrices;
    }

    /**
     * 
     * Function to get special price
     * @param int $productId
     * @return price
     */
    public static function getSpecialPrice($productId) {
        $user = JFactory::getUser();
        if ($user->get('id')) {
            $customer = new OpenShopCustomer();
            $customerGroupId = $customer->getCustomerGroupId();
        } else {
            $customerGroupId = self::getConfigValue('customergroup_id');
        }
        if (!$customerGroupId)
            $customerGroupId = 0;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('price')
                ->from('#__openshop_productspecials')
                ->where('product_id = ' . intval($productId))
                ->where('published = 1')
                ->where('customergroup_id = ' . intval($customerGroupId))
                ->where('(date_start <= "' . date('Y-m-d H:i:s') . '" OR date_start = "0000-00-00 00:00:00")')
                ->where('(date_end >= "' . date('Y-m-d H:i:s') . '" OR date_end = "0000-00-00 00:00:00")')
                ->order('priority');
        $db->setQuery($query, 0, 1);
        $specialPrice = $db->loadResult();
        if (!$specialPrice)
            $specialPrice = 0;
        return $specialPrice;
    }

    /**
     * 
     * Function to get product price array
     * @param int $productId
     * @param float $productPrice
     * @return array of price
     */
    public static function getProductPriceArray($productId, $productPrice) {
        $specialPrice = self::getSpecialPrice($productId);
        $discountPrice = self::getDiscountPrice($productId);
        if ($specialPrice) {
            $salePrice = $specialPrice;
            if ($discountPrice) {
                $basePrice = $discountPrice;
            } else {
                $basePrice = $productPrice;
            }
        } else {
            $basePrice = $productPrice;
            $salePrice = $discountPrice;
        }
        $productPriceArray = array("basePrice" => $basePrice, "salePrice" => $salePrice);
        return $productPriceArray;
    }

    /**
     *
     * Function to get currency format for a specific number
     * @param float $number        	
     * @param int $currencyId        	
     */
    public static function getCurrencyFormat($number, $currencyId = '') {
        if (!$currencyId) {
            // Use default currency
            $currencyId = 4;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_currencies')
                ->where('id = ' . intval($currencyId));
        $db->setQuery($query);
        $row = $db->loadObject();
        $currencyFormat = '';
        $sign = '';
        if ($number < 0) {
            $sign = '-';
            $number = abs($number);
        }
        if (is_object($row)) {
            $currencyFormat = $sign . $row->left_symbol . number_format($number, $row->decimal_place, $row->decimal_symbol, $row->thousands_separator) .
                    $row->right_symbol;
        }
        return $currencyFormat;
    }

    /**
     * 
     * Function to round out a number
     * @param float $number
     * @param int $places
     * @return float
     */
    public static function roundOut($number, $places = 0) {
        if ($places < 0)
            $places = 0;
        $mult = pow(10, $places);
        return ($number >= 0 ? ceil($number * $mult) : floor($number * $mult)) / $mult;
    }

    /**
     * 
     * Function to round up a number 
     * @param float $number
     * @param int $places
     * @return float
     */
    public static function roundUp($number, $places = 0) {
        if ($places < 0)
            $places = 0;
        $mult = pow(10, $places);
        return ceil($number * $mult) / $mult;
    }

    /**
     *
     * Function to get information for a specific address
     * @param int $addressId
     */
    public static function getAddress($addressId) {
        $user = JFactory::getUser();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*, z.zone_name, z.zone_code, c.country_name, c.iso_code_2, c.iso_code_3')
                ->from('#__openshop_addresses AS a')
                ->leftJoin('#__openshop_zones AS z ON (a.zone_id = z.id)')
                ->leftJoin('#__openshop_countries AS c ON (a.country_id = c.id)')
                ->where('a.id = ' . intval($addressId))
                ->where('a.customer_id = ' . intval($user->get('id')));
        $db->setQuery($query);
        return $db->loadAssoc();
    }

    /**
     *
     * Function to get district of zone
     * @param int $addressId
     */
    public static function getDistrictOfZone($idZone) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        try {
            $query->select('*')
                    ->from($db->quoteName('#__openshop_districts', 'a'))
                    ->where('parent_id = ' . $idZone);
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }

    /**
     *
     * Function to get information for a specific customer
     * @param int $customerId
     * @return customer object
     */
    public static function getCustomer($customerId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_customers')
                ->where('customer_id = ' . intval($customerId));
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     *
     * Function to get information for a specific country
     * @param int $countryId
     */
    public static function getCountry($countryId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_countries')
                ->where('id = ' . intval($countryId))
                ->where('published = 1');
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     *
     * Function to get Zones for a specific Country
     * @param int $countryId
     */
    public static function getCountryZones($countryId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id, zone_name')
                ->from('#__openshop_zones')
                ->where('country_id = ' . intval($countryId))
                ->where('published = 1')
                ->order('zone_name');
        $db->setQuery($query);
        return $db->loadAssocList();
    }

    /**
     *
     * Function to get information for a specific zone
     * @param int $zoneId
     */
    public static function getZone($zoneId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_zones')
                ->where('id = ' . intval($zoneId))
                ->where('published = 1');
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     *
     * Function to get all for a specific zone
     * @param int $zoneId
     */
    public static function getZones() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_zones')
                ->where('published = 1');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     *
     * Function to get information for a specific zone
     * @param int $zoneId
     */
    public static function getDistrict($Id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_districts')
                ->where('id = ' . intval($Id))
                ->where('published = 1');
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     *
     * Function to get information for a specific geozone
     * @param int $geozoneId
     */
    public static function getGeozone($geozoneId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_geozones')
                ->where('id = ' . intval($geozoneId))
                ->where('published = 1');
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     *
     * Function to complete an order
     * @param order object $row
     */
    public static function completeOrder($row) {
        $orderId = intval($row->id);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_orderproducts')
                ->where('order_id = ' . intval($orderId));
        $db->setQuery($query);
        $orderProducts = $db->loadObjectList();
        foreach ($orderProducts as $orderProduct) {
            //Update product quantity
            $query->clear();
            $query->update('#__openshop_products')
                    ->set('product_quantity = product_quantity - ' . intval($orderProduct->quantity))
                    ->where('id = ' . intval($orderProduct->product_id));
            $db->setQuery($query);
            $db->query();
            //Update product options
            $query->clear();
            $query->select('*')
                    ->from('#__openshop_orderoptions')
                    ->where('order_id = ' . intval($orderId))
                    ->where('order_product_id = ' . intval($orderProduct->id));
            $db->setQuery($query);
            $orderOptions = $db->loadObjectList();
            foreach ($orderOptions as $orderOption) {
                if ($orderOption->option_type == 'Select' || $orderOption->option_type == 'Radio' || $orderOption->option_type == 'Checkbox') {
                    $query->clear();
                    $query->update('#__openshop_productoptionvalues')
                            ->set('quantity = quantity - ' . intval($orderProduct->quantity))
                            ->where('id = ' . intval($orderOption->product_option_value_id));
                    $db->setQuery($query);
                    $db->query();
                }
            }
        }
        //Add coupon history and voucher history
        self::addCouponHistory($row);
        self::addVoucherHistory($row);
    }

    /**
     * 
     * Function to add coupon history
     * @param order object $row
     */
    public static function addCouponHistory($row) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('value')
                ->from('#__openshop_ordertotals')
                ->where('order_id = ' . intval($row->id))
                ->where('name = "coupon"');
        $db->setQuery($query);
        $amount = $db->loadResult();
        if ($amount) {
            $couponId = $row->coupon_id;
            if ($couponId) {
                $coupon = new OpenShopCoupon();
                $coupon->addCouponHistory($couponId, $row->id, $row->customer_id, $amount);
            }
        }
    }

    /**
     *
     * Function to add voucher history
     * @param order object $row
     */
    public static function addVoucherHistory($row) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('value')
                ->from('#__openshop_ordertotals')
                ->where('order_id = ' . intval($row->id))
                ->where('name = "voucher"');
        $db->setQuery($query);
        $amount = $db->loadResult();
        if ($amount) {
            $voucherId = $row->voucher_id;
            if ($voucherId) {
                $voucher = new OpenShopVoucher();
                $voucher->addVoucherHistory($voucherId, $row->id, $row->customer_id, $amount);
            }
        }
    }

    /**
     *
     * Function to send quote emails
     * @param order object $row
     */
    public static function sendQuoteEmails($row) {
        $jconfig = new JConfig();
        $mailer = JFactory::getMailer();
        $fromName = $jconfig->fromname;
        $fromEmail = $jconfig->mailfrom;

        //Send notification email to admin
        $adminSubject = sprintf(JText::_('OPENSHOP_ADMIN_QUOTE_EMAIL_SUBJECT'), $row->name);
        $adminBody = self::getAdminQuoteEmailBody($row);
        $adminBody = self::convertImgTags($adminBody);
        $adminEmail = self::getConfigValue('email') ? trim(self::getConfigValue('email')) : $fromEmail;
        $mailer->sendMail($fromEmail, $fromName, $adminEmail, $adminSubject, $adminBody, 1);

        //Send notification email to additional emails
        $alertEmails = self::getConfigValue('alert_emails');
        $alertEmails = str_replace(' ', '', $alertEmails);
        $alertEmails = explode(',', $alertEmails);
        for ($i = 0; $n = count($alertEmails), $i < $n; $i++) {
            if ($alertEmails[$i] != '') {
                $mailer->ClearAllRecipients();
                $mailer->sendMail($fromEmail, $fromName, $alertEmails[$i], $adminSubject, $adminBody, 1);
            }
        }

        //Send email to customer
        $customerSubject = sprintf(JText::_('OPENSHOP_CUSTOMER_QUOTE_EMAIL_SUBJECT'));
        $customerBody = self::getCustomerQuoteEmailBody($row);
        $customerBody = self::convertImgTags($customerBody);
        $mailer->ClearAllRecipients();
        $mailer->sendMail($fromEmail, $fromName, $row->email, $customerSubject, $customerBody, 1);
    }

    /**
     *
     * Function to get admin quote email body
     * @param quote object $row
     * @return string
     */
    public static function getAdminQuoteEmailBody($row) {
        $currency = new OpenShopCurrency();
        $row->total_price = $currency->format($row->total, $row->currency_code, $row->currency_exchanged_value);
        $adminEmailBody = self::getMessageValue('admin_quote_email');
        // Quote information
        $replaces = array();
        $replaces['name'] = $row->name;
        $replaces['email'] = $row->email;
        $replaces['company'] = $row->company;
        $replaces['telephone'] = $row->telephone;
        $replaces['message'] = $row->message;
        // Products list
        $viewConfig['name'] = 'email';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['layout'] = 'quoteproducts';
        $view = new JViewLegacy($viewConfig);
        $quoteProducts = self::getQuoteProducts($row->id);
        $view->assignRef('quoteProducts', $quoteProducts);
        $view->assignRef('row', $row);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        $replaces['products_list'] = $text;

        foreach ($replaces as $key => $value) {
            $key = strtoupper($key);
            $adminEmailBody = str_replace("[$key]", $value, $adminEmailBody);
        }
        return $adminEmailBody;
    }

    /**
     *
     * Function to get customer quote email body
     * @param quote object $row
     * @return string
     */
    public static function getCustomerQuoteEmailBody($row) {
        $currency = new OpenShopCurrency();
        $row->total_price = $currency->format($row->total, $row->currency_code, $row->currency_exchanged_value);
        $customerEmailBody = self::getMessageValue('customer_quote_email');
        // Quote information
        $replaces = array();
        // Products list
        $viewConfig['name'] = 'email';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['layout'] = 'quoteproducts';
        $view = new JViewLegacy($viewConfig);
        $quoteProducts = self::getQuoteProducts($row->id);
        $view->assignRef('quoteProducts', $quoteProducts);
        $view->assignRef('row', $row);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        $replaces['name'] = $row->name;
        $replaces['products_list'] = $text;

        foreach ($replaces as $key => $value) {
            $key = strtoupper($key);
            $customerEmailBody = str_replace("[$key]", $value, $customerEmailBody);
        }
        return $customerEmailBody;
    }

    /**
     *
     * Function to send email
     * @param order object $row
     */
    public static function sendEmails($row) {
        $jconfig = new JConfig();
        $mailer = JFactory::getMailer();
        $fromName = $jconfig->fromname;
        $fromEmail = $jconfig->mailfrom;

        //Send notification email to admin
        $adminSubject = sprintf(JText::_('OPENSHOP_ADMIN_EMAIL_SUBJECT'), self::getConfigValue('store_name'), $row->id);
        $adminBody = self::getAdminEmailBody($row);
        $adminBody = self::convertImgTags($adminBody);
        $adminEmail = self::getConfigValue('email') ? trim(self::getConfigValue('email')) : $fromEmail;
        $mailer->sendMail($fromEmail, $fromName, $adminEmail, $adminSubject, $adminBody, 1);

        //Send notification email to additional emails
        $alertEmails = self::getConfigValue('alert_emails');
        $alertEmails = str_replace(' ', '', $alertEmails);
        $alertEmails = explode(',', $alertEmails);
        for ($i = 0; $n = count($alertEmails), $i < $n; $i++) {
            if ($alertEmails[$i] != '') {
                $mailer->ClearAllRecipients();
                $mailer->sendMail($fromEmail, $fromName, $alertEmails[$i], $adminSubject, $adminBody, 1);
            }
        }

        //Send notification email to brand
        $brands = array();
        $orderProducts = self::getOrderProducts($row->id);
        for ($i = 0; $n = count($orderProducts), $i < $n; $i++) {
            $product = $orderProducts[$i];
            $brand = self::getProductBrand($product->product_id, JFactory::getLanguage()->getTag());
            if (is_object($brand)) {
                $brand->product = $orderProducts[$i];
                if (!isset($brands[$brand->id])) {
                    $brands[$brand->id] = array();
                }
                $brands[$brand->id][] = $brand;
            }
        }
        $brandSubject = JText::_('OPENSHOP_MANUFACTURER_EMAIL_SUBJECT');
        foreach ($brands as $brandId => $brand) {
            if ($brand[0]->brand_email != '') {
                $brandBody = self::getBrandEmailBody($brand, $row);
                $brandBody = self::convertImgTags($brandBody);
                $mailer->ClearAllRecipients();
                $mailer->sendMail($fromEmail, $fromName, $brand[0]->brand_email, $brandSubject, $brandBody);
            }
        }

        //Send email to customer
        $customerSubject = sprintf(JText::_('OPENSHOP_CUSTOMER_EMAIL_SUBJECT'), self::getConfigValue('store_name'), $row->id);
        $customerBody = self::getCustomerEmailBody($row);
        $customerBody = self::convertImgTags($customerBody);
        $mailer->ClearAllRecipients();
        $attachment = null;
        if (self::getConfigValue('invoice_enable') && self::getConfigValue('send_invoice_to_customer')) {
            if (!$row->invoice_number) {
                $row->invoice_number = self::getInvoiceNumber();
                $row->store();
            }
            self::generateInvoicePDF(array($row->id));
            $attachment = JPATH_ROOT . '/images/com_openshop/invoices/' . self::formatInvoiceNumber($row->invoice_number) . '.pdf';
        }
        $mailer->sendMail($fromEmail, $fromName, $row->email, $customerSubject, $customerBody, 1, null, null, $attachment);
    }

    /**
     *
     * Function to get admin email body
     * @param order object $row
     * @return string
     */
    public static function getAdminEmailBody($row) {
        $adminEmailBody = self::getMessageValue('admin_notification_email', $row->language);
        // Order information
        $replaces = array();
        $replaces['order_id'] = $row->id;
        $replaces['order_number'] = $row->order_number;
        $replaces['order_status'] = self::getOrderStatusName($row->order_status_id, $row->language);
        $replaces['date_added'] = JHtml::date($row->created_date, self::getConfigValue('date_format', 'm-d-Y'));
        if ($row->payment_method == 'os_creditcard') {
            $cardNumber = JRequest::getVar('card_number', '');
            if ($cardNumber) {
                $last4Digits = substr($cardNumber, strlen($cardNumber) - 4);
                $replaces['payment_method'] = JText::_($row->payment_method_title) . ' (' . JText::_('OPENSHOP_LAST_4DIGITS_CREDIT_CARD_NUMBER') . ': ' . $last4Digits . ')';
            } else {
                $replaces['payment_method'] = JText::_($row->payment_method_title);
            }
        } else {
            $replaces['payment_method'] = JText::_($row->payment_method_title);
        }
        $replaces['shipping_method'] = $row->shipping_method_title;
        $replaces['customer_email'] = $row->email;
        $replaces['customer_telephone'] = $row->telephone;
        // Comment
        $replaces['comment'] = $row->comment;
        // Delivery Date
        $replaces['delivery_date'] = JHtml::date($row->delivery_date, self::getConfigValue('date_format', 'm-d-Y'));
        // Payment information
        $replaces['payment_address'] = self::getPaymentAddress($row);
        //Payment custom fields here
        $excludedFields = array('firstname', 'lastname', 'email', 'telephone', 'fax', 'company', 'company_id', 'address_1', 'address_2', 'city', 'postcode', 'country_id', 'zone_id');
        $form = new RADForm(self::getFormFields('B'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'payment_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['payment_' . $fieldName] = $fieldValue;
            }
        }
        // Shipping information
        $replaces['shipping_address'] = self::getShippingAddress($row);
        //Shipping custom fields here
        $form = new RADForm(self::getFormFields('S'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'shipping_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['shipping_' . $fieldName] = $fieldValue;
            }
        }
        // Products list
        $viewConfig['name'] = 'email';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['layout'] = 'admin';
        $view = new JViewLegacy($viewConfig);
        $orderProducts = self::getOrderProducts($row->id);
        $view->assignRef('orderProducts', $orderProducts);
        $orderTotals = self::getOrderTotals($row->id);
        $view->assignRef('orderTotals', $orderTotals);
        $view->assignRef('row', $row);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        $replaces['products_list'] = $text;

        foreach ($replaces as $key => $value) {
            $key = strtoupper($key);
            $adminEmailBody = str_replace("[$key]", $value, $adminEmailBody);
        }
        return $adminEmailBody;
    }

    /**
     * 
     * Function to get brand email body
     * @param array $brand
     */
    public static function getBrandEmailBody($brand, $row) {
        $brandEmailBody = self::getMessageValue('brand_notification_email', $row->language);
        $replaces = array();
        $replaces['brand_name'] = $brand[0]->brand_name;
        $replaces['store_name'] = self::getConfigValue('store_name');
        $replaces['order_id'] = $row->id;
        $replaces['order_number'] = $row->order_number;
        $replaces['date_added'] = JHtml::date($row->created_date, self::getConfigValue('date_format', 'm-d-Y'));
        $replaces['payment_method'] = JText::_($row->payment_method_title);
        $replaces['shipping_method'] = $row->shipping_method_title;
        $replaces['customer_email'] = $row->email;
        $replaces['customer_telephone'] = $row->telephone;
        // Products list
        $viewConfig['name'] = 'email';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['layout'] = 'brand';
        $view = new JViewLegacy($viewConfig);
        $view->assignRef('brand', $brand);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        $replaces['products_list'] = $text;
        // Comment
        $replaces['comment'] = $row->comment;
        // Delivery Date
        $replaces['delivery_date'] = JHtml::date($row->delivery_date, self::getConfigValue('date_format', 'm-d-Y'));
        // Payment information
        $replaces['payment_address'] = self::getPaymentAddress($row);
        //Payment custom fields here
        $excludedFields = array('firstname', 'lastname', 'email', 'telephone', 'fax', 'company', 'company_id', 'address_1', 'address_2', 'city', 'postcode', 'country_id', 'zone_id');
        $form = new RADForm(self::getFormFields('B'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'payment_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['payment_' . $fieldName] = $fieldValue;
            }
        }
        // Shipping information
        $replaces['shipping_address'] = self::getShippingAddress($row);
        //Shipping custom fields here
        $form = new RADForm(self::getFormFields('S'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'shipping_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['shipping_' . $fieldName] = $fieldValue;
            }
        }
        foreach ($replaces as $key => $value) {
            $key = strtoupper($key);
            $brandEmailBody = str_replace("[$key]", $value, $brandEmailBody);
        }
        return $brandEmailBody;
    }

    /**
     *
     * Function to get customer email body
     * @param order object $row
     * @return html string
     */
    public static function getCustomerEmailBody($row) {
        $hasDownload = false;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(*)')
                ->from('#__openshop_orderdownloads')
                ->where('order_id = ' . intval($row->id));
        $db->setQuery($query);
        if ($db->loadResult())
            $hasDownload = true;
        if ($row->customer_id) {
            if (strpos($row->payment_method, 'os_offline') !== false) {
                if ($hasDownload) {
                    $customerEmailBody = self::getMessageValue('offline_payment_customer_notification_email_with_download', $row->language);
                } else {
                    $customerEmailBody = self::getMessageValue('offline_payment_customer_notification_email', $row->language);
                }
            } else {
                if ($hasDownload) {
                    $customerEmailBody = self::getMessageValue('customer_notification_email_with_download', $row->language);
                } else {
                    $customerEmailBody = self::getMessageValue('customer_notification_email', $row->language);
                }
            }
        } else {
            if (strpos($row->payment_method, 'os_offline') !== false) {
                if ($hasDownload) {
                    $customerEmailBody = self::getMessageValue('offline_payment_guest_notification_email_with_download', $row->language);
                } else {
                    $customerEmailBody = self::getMessageValue('offline_payment_guest_notification_email', $row->language);
                }
            } else {
                if ($hasDownload) {
                    $customerEmailBody = self::getMessageValue('guest_notification_email_with_download', $row->language);
                } else {
                    $customerEmailBody = self::getMessageValue('guest_notification_email', $row->language);
                }
            }
        }
        // Order information
        $replaces = array();
        $replaces['customer_name'] = $row->firstname . ' ' . $row->lastname;
        $replaces['payment_firstname'] = $row->payment_firstname;
        $replaces['store_name'] = self::getConfigValue('store_name');
        $replaces['order_link'] = JRoute::_(JUri::root() . 'index.php?option=com_openshop&view=customer&layout=order&order_id=' . $row->id);
        $replaces['download_link'] = JRoute::_(JUri::root() . 'index.php?option=com_openshop&view=customer&layout=downloads');
        $replaces['order_id'] = $row->id;
        $replaces['order_number'] = $row->order_number;
        $replaces['order_status'] = self::getOrderStatusName($row->order_status_id, $row->language);
        $replaces['date_added'] = JHtml::date($row->created_date, self::getConfigValue('date_format', 'm-d-Y'));
        $replaces['payment_method'] = JText::_($row->payment_method_title);
        $replaces['shipping_method'] = $row->shipping_method_title;
        $replaces['customer_email'] = $row->email;
        $replaces['customer_telephone'] = $row->telephone;
        // Comment
        $replaces['comment'] = $row->comment;
        // Delivery Date
        $replaces['delivery_date'] = JHtml::date($row->delivery_date, self::getConfigValue('date_format', 'm-d-Y'));
        // Payment information
        $replaces['payment_address'] = self::getPaymentAddress($row);
        //Payment custom fields here
        $excludedFields = array('firstname', 'lastname', 'email', 'telephone', 'fax', 'company', 'company_id', 'address_1', 'address_2', 'city', 'postcode', 'country_id', 'zone_id');
        $form = new RADForm(self::getFormFields('B'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'payment_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['payment_' . $fieldName] = $fieldValue;
            }
        }
        // Shipping information
        $replaces['shipping_address'] = self::getShippingAddress($row);
        //Shipping custom fields here
        $form = new RADForm(self::getFormFields('S'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'shipping_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['shipping_' . $fieldName] = $fieldValue;
            }
        }
        // Products list
        $viewConfig['name'] = 'email';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['layout'] = 'customer';
        $view = new JViewLegacy($viewConfig);
        $orderProducts = self::getOrderProducts($row->id);
        $view->assignRef('orderProducts', $orderProducts);
        $orderTotals = self::getOrderTotals($row->id);
        $view->assignRef('orderTotals', $orderTotals);
        $view->assignRef('row', $row);
        if ($hasDownload && $row->order_status_id == OpenShopHelper::getConfigValue('complete_status_id')) {
            $showDownloadLink = true;
        } else {
            $showDownloadLink = false;
        }
        $view->assignRef('showDownloadLink', $showDownloadLink);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        $replaces['products_list'] = $text;

        foreach ($replaces as $key => $value) {
            $key = strtoupper($key);
            $customerEmailBody = str_replace("[$key]", $value, $customerEmailBody);
        }
        return $customerEmailBody;
    }

    /**
     *
     * Function to get notification email body
     * @param order object $row
     * @return html string
     */
    public static function getNotificationEmailBody($row, $orderStatusFrom, $orderStatusTo) {
        if ($row->customer_id) {
            $notificationEmailBody = self::getMessageValue('order_status_change_customer', $row->language);
        } else {
            $notificationEmailBody = self::getMessageValue('order_status_change_guest', $row->language);
        }
        // Order information
        $replaces = array();
        $replaces['customer_name'] = $row->firstname . ' ' . $row->lastname;
        $replaces['order_status_from'] = self::getOrderStatusName($orderStatusFrom, $row->language);
        $replaces['order_status_to'] = self::getOrderStatusName($orderStatusTo, $row->language);
        $replaces['payment_firstname'] = $row->payment_firstname;
        $replaces['store_name'] = self::getConfigValue('store_name');
        $replaces['order_link'] = JRoute::_(JUri::root() . 'index.php?option=com_openshop&view=customer&layout=order&order_id=' . $row->id);
        $replaces['order_id'] = $row->id;
        $replaces['order_number'] = $row->order_number;
        $replaces['date_added'] = JHtml::date($row->created_date, self::getConfigValue('date_format', 'm-d-Y'));
        $replaces['payment_method'] = JText::_($row->payment_method_title);
        $replaces['shipping_method'] = $row->shipping_method_title;
        $replaces['customer_email'] = $row->email;
        $replaces['customer_telephone'] = $row->telephone;
        // Comment
        $replaces['comment'] = $row->comment;
        // Delivery Date
        $replaces['delivery_date'] = JHtml::date($row->delivery_date, self::getConfigValue('date_format', 'm-d-Y'));
        // Payment information
        $replaces['payment_address'] = self::getPaymentAddress($row);
        //Payment custom fields here
        $excludedFields = array('firstname', 'lastname', 'email', 'telephone', 'fax', 'company', 'company_id', 'address_1', 'address_2', 'city', 'postcode', 'country_id', 'zone_id');
        $form = new RADForm(self::getFormFields('B'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'payment_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['payment_' . $fieldName] = $fieldValue;
            }
        }
        // Shipping information
        $replaces['shipping_address'] = self::getShippingAddress($row);
        //Shipping custom fields here
        $form = new RADForm(self::getFormFields('S'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'shipping_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['shipping_' . $fieldName] = $fieldValue;
            }
        }
        // Products list
        $viewConfig['name'] = 'email';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['layout'] = 'customer';
        $view = new JViewLegacy($viewConfig);
        $orderProducts = self::getOrderProducts($row->id);
        $view->assignRef('orderProducts', $orderProducts);
        $view->assignRef('orderTotals', self::getOrderTotals($row->id));
        $view->assignRef('row', $row);
        $hasDownload = false;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(*)')
                ->from('#__openshop_orderdownloads')
                ->where('order_id = ' . intval($row->id));
        $db->setQuery($query);
        if ($db->loadResult())
            $hasDownload = true;
        if ($hasDownload && $row->order_status_id == OpenShopHelper::getConfigValue('complete_status_id')) {
            $showDownloadLink = true;
        } else {
            $showDownloadLink = false;
        }
        $view->assignRef('showDownloadLink', $showDownloadLink);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        $replaces['products_list'] = $text;

        foreach ($replaces as $key => $value) {
            $key = strtoupper($key);
            $notificationEmailBody = str_replace("[$key]", $value, $notificationEmailBody);
        }
        return $notificationEmailBody;
    }

    /**
     *
     * Function to get shipping notification email body
     * @param order object $row
     * @return html string
     */
    public static function getShippingNotificationEmailBody($row) {
        $shippingNotificationEmailBody = self::getMessageValue('shipping_notification_email', $row->language);
        // Order information
        $replaces = array();
        $replaces['customer_name'] = $row->firstname . ' ' . $row->lastname;
        $replaces['order_id'] = $row->id;
        $replaces['order_number'] = $row->order_number;
        $replaces['shipping_tracking_number'] = $row->shipping_tracking_number;
        $replaces['shipping_tracking_url'] = $row->shipping_tracking_url;
        $replaces['comment'] = $row->comment;
        // Shipping information
        $replaces['shipping_address'] = self::getShippingAddress($row);
        //Shipping custom fields here
        $excludedFields = array('firstname', 'lastname', 'email', 'telephone', 'fax', 'company', 'company_id', 'address_1', 'address_2', 'city', 'postcode', 'country_id', 'zone_id');
        $form = new RADForm(self::getFormFields('S'));
        $fields = $form->getFields();
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if (!in_array($fieldName, $excludedFields)) {
                $fieldValue = $row->{'shipping_' . $fieldName};
                if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                    $fieldValue = implode(', ', json_decode($fieldValue));
                }
                $replaces['shipping_' . $fieldName] = $fieldValue;
            }
        }
        foreach ($replaces as $key => $value) {
            $key = strtoupper($key);
            $shippingNotificationEmailBody = str_replace("[$key]", $value, $shippingNotificationEmailBody);
        }
        return $shippingNotificationEmailBody;
    }

    /**
     *
     * Function to get ask question email body
     * @param object product
     */
    public static function getAskQuestionEmailBody($data, $product) {
        // Products list
        $viewConfig['name'] = 'email';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/emailtemplates';
        $viewConfig['layout'] = 'askquestion';
        $view = new JViewLegacy($viewConfig);
        $view->assignRef('data', $data);
        $view->assignRef('product', $product);
        ob_start();
        $view->display();
        $askQuestionEmailBody = ob_get_contents();
        ob_end_clean();
        return $askQuestionEmailBody;
    }

    /**
     * 
     * Function to get invoice ouput for a specific order
     * @param int $orderId
     * @return string
     */
    public static function getInvoiceHtml($orderId) {
        $viewConfig['name'] = 'invoice';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/invoicetemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/invoicetemplates';
        $viewConfig['layout'] = 'default';
        $view = new JViewLegacy($viewConfig);
        $view->assignRef('order_id', $orderId);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        return $text;
    }

    /**
     *
     * Function to load jQuery chosen plugin
     */
    public static function chosen() {
        static $chosenLoaded;
        if (!$chosenLoaded) {
            $document = JFactory::getDocument();
            if (version_compare(JVERSION, '3.0', 'ge')) {
                JHtml::_('formbehavior.chosen', '.chosen');
            } else {
                $document->addScript(JUri::base(true) . '/components/com_openshop/assets/chosen/chosen.jquery.js');
                $document->addStyleSheet(JUri::base(true) . '/components/com_openshop/assets/chosen/chosen.css');
            }
            $document->addScriptDeclaration(
                    "jQuery(document).ready(function(){
	                    jQuery(\".chosen\").chosen();
	                });");
            $chosenLoaded = true;
        }
    }

    /**
     *
     * Function to load bootstrap library
     */
    public static function loadBootstrap($loadJs = true) {
        $document = JFactory::getDocument();
        if ($loadJs) {
            $document->addScript(JUri::root(true) . '/components/com_openshop/assets/bootstrap/js/jquery.min.js');
            $document->addScript(JUri::root(true) . '/components/com_openshop/assets/bootstrap/js/jquery-noconflict.js');
            $document->addScript(JUri::root(true) . '/components/com_openshop/assets/bootstrap/js/bootstrap.min.js');
        }
        $document->addStyleSheet(JUri::root(true) . '/components/com_openshop/assets/bootstrap/css/bootstrap.css');
        $document->addStyleSheet(JUri::root(true) . '/components/com_openshop/assets/bootstrap/css/bootstrap.min.css');
    }

    /**
     *
     * Function to load bootstrap css
     */
    public static function loadBootstrapCss() {
        $document = JFactory::getDocument();
//        $document->addStyleSheet(JUri::root(true) . '/components/com_openshop/assets/bootstrap/css/bootstrap.css');
        $document->addStyleSheet(JUri::root(true) . '/components/com_openshop/assets/bootstrap/css/bootstrap.min.css');
    }

    /**
     *
     * Function to load bootstrap javascript
     */
    public static function loadBootstrapJs() {
        $document = JFactory::getDocument();
        $document->addScript(JUri::root(true) . '/components/com_openshop/assets/bootstrap/js/jquery.min.js');
        $document->addScript(JUri::root(true) . '/components/com_openshop/assets/bootstrap/js/jquery-noconflict.js');
        $document->addScript(JUri::root(true) . '/components/com_openshop/assets/bootstrap/js/bootstrap.min.js');
    }

    /**
     * 
     * Function to load scripts for share product
     */
    public static function loadShareScripts($product) {
        $document = JFactory::getDocument();

        //Add script for Twitter
        if (self::getConfigValue('show_twitter_button')) {
            $script = '!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");';
            $document->addScriptDeclaration($script);
        }

        //Add script for PinIt
        if (self::getConfigValue('show_pinit_button')) {
            $script = '(function() {
				window.PinIt = window.PinIt || { loaded:false };
				if (window.PinIt.loaded) return;
				window.PinIt.loaded = true;
				function async_load(){
					var s = document.createElement("script");
					s.type = "text/javascript";
					s.async = true;
					s.src = "http://assets.pinterest.com/js/pinit.js";
					var x = document.getElementsByTagName("script")[0];
					x.parentNode.insertBefore(s, x);
				}
				if (window.attachEvent)
					window.attachEvent("onload", async_load);
				else
					window.addEventListener("load", async_load, false);
			})();';
            $document->addScriptDeclaration($script);
        }

        // Add script for LinkedIn
        if (self::getConfigValue('show_linkedin_button'))
            $document->addScript('//platform.linkedin.com/in.js');

        // Add script for Google
        if (self::getConfigValue('show_google_button')) {
            $script = '(function() {
				var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
				po.src = "https://apis.google.com/js/plusone.js";
				var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
			})();';
            $document->addScriptDeclaration($script);
        }

        // Add script for Facebook
        if (self::getConfigValue('show_facebook_button') || self::getConfigValue('show_facebook_comment')) {
            $script = '(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/' . self::getConfigValue('button_language', 'en_US') . '/all.js#xfbml=1&appId=' . self::getConfigValue('app_id', '372958799407679') . '";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, "script","facebook-jssdk"));';
            $document->addScriptDeclaration($script);
            $uri = JUri::getInstance();
            $conf = JFactory::getConfig();
            $document->addCustomTag('<meta property="og:title" content="' . $product->product_name . '"/>');
            $document->addCustomTag('<meta property="og:type" content="product"/>');
            $document->addCustomTag('<meta property="og:image" content="' . $product->thumb_image . '"/>');
            $document->addCustomTag('<meta property="og:url" content="' . $uri->toString() . '"/>');
            $document->addCustomTag('<meta property="og:description" content="' . $product->product_name . '"/>');
            $document->addCustomTag('<meta property="og:site_name" content="' . $conf->get('sitename') . '"/>');
            $document->addCustomTag('<meta property="fb:admins" content="' . self::getConfigValue('app_id', '372958799407679') . '"/>');
        }
    }

    /**
     * 
     * Function to get Itemid of OpenShop component
     * @return int
     */
    public static function getItemid() {
        $user = JFactory::getUser();
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__menu')
                ->where('link LIKE "%index.php?option=com_openshop%"')
                ->where('published = 1')
                ->where('`access` IN ("' . implode(',', $user->getAuthorisedViewLevels()) . '")')
                ->order('access');
        $db->setQuery($query);
        $itemId = $db->loadResult();
        if (!$itemId) {
            $Itemid = JRequest::getInt('Itemid');
            if ($Itemid == 1)
                $itemId = 999999;
            else
                $itemId = $Itemid;
        }
        return $itemId;
    }

    /**
     *
     * Function to get a list of the actions that can be performed.
     * @return JObject
     * @since 1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject();
        $actions = array('core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete');
        $assetName = 'com_openshop';
        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }

    /**
     * 
     * Function to display copy right information
     */
    public static function displayCopyRight() {
        echo '<div class="copyright" style="text-align:center;margin-top: 5px;"><strong>OpenShop Manager Version 2.0.1.6, Developed by LMNX</strong></a></div>';
    }

    /**
     *
     * Function to add dropdown menu
     * @param string $vName        	
     */
    public static function renderSubmenuPer($vName = 'dashboard') {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_menus')
                ->where('published = 1')
                ->where('menu_parent_id = 0')
                ->where('access = 1')
                ->order('ordering');
        $db->setQuery($query);
        $menus = $db->loadObjectList();

        //get user group
        $query->clear();
        $query->select('group_id')
                ->from($db->quoteName('#__user_usergroup_map'))
                ->where('user_id = ' . JFactory::getUser()->id);
        $id_usergroup = $db->setQuery($query)->loadObjectList();

        $html = '';
        $html .= '<ul class="nav nav-tabs">';
        for ($i = 0; $n = count($menus), $i < $n; $i++) {
            $menu = $menus[$i];
            $showCondition = true;
            if ($showCondition) {
                $query->clear();
                $query->select('*')
                        ->from('#__openshop_menus')
                        ->where('published = 1')
                        ->where('access = 1')
                        ->where('menu_parent_id = ' . intval($menu->id))
                        ->order('ordering');
                $db->setQuery($query);
                $subMenus = $db->loadObjectList();
                if (!count($subMenus)) {
                    if (OpenShopHelper::getPermissionAction($menu->menu_view, 'view')) {
                        $class = '';
                        if ($menu->menu_view == $vName) {
                            $class = ' class="active"';
                        }
                        $html .= '<li' . $class . '><a href="index.php?option=com_openshop&view=' . $menu->menu_view . '"><span class="' . $menu->menu_class . '"></span> ' . JText::_($menu->menu_name) . '</a></li>';
                    }
                } else {
                    $checkPerInUserGroup = 0;
                    foreach ($id_usergroup as $id_ug) {
                        $checkPerInUserGroup = OpenShopHelper::checkCountChildPerView($subMenus, $id_ug->group_id);
                        if ($checkPerInUserGroup) {
                            break;
                        }
                    }
                    if ($checkPerInUserGroup) {
                        $class = ' class="dropdown"';
                        for ($j = 0; $m = count($subMenus), $j < $m; $j++) {
                            $subMenu = $subMenus[$j];
                            $lName = JRequest::getVar('layout');
                            if ((!$subMenu->menu_layout && $vName == $subMenu->menu_view ) || ($lName != '' && $lName == $subMenu->menu_layout)) {
                                $class = ' class="dropdown active"';
                                break;
                            }
                        }
                        $html .= '<li' . $class . '>';
                        $html .= '<a id="drop_' . $menu->id . '" href="#" data-toggle="dropdown" role="button" class="dropdown-toggle"><span class="' . $menu->menu_class . '"></span> ' .
                                JText::_($menu->menu_name) . ' <b class="caret"></b></a>';
                        $html .= '<ul aria-labelledby="drop_' . $menu->id . '" role="menu" class="dropdown-menu" id="menu_' . $menu->id . '">';
                        foreach ($id_usergroup as $id_ug) {
                            for ($j = 0; $m = count($subMenus), $j < $m; $j++) {
                                $subMenu = $subMenus[$j];
                                $per_sub = json_decode($subMenu->permission, TRUE);
                                //check view 1=>show, 0=>hide
                                if (isset($per_sub[$id_ug->group_id])) {
                                    if ($per_sub[$id_ug->group_id]['view']) {
                                        $showSubCondition = true;

                                        if ($showSubCondition) {
                                            $layoutLink = '';
                                            if ($subMenu->menu_layout) {
                                                $layoutLink = '&layout=' . $subMenu->menu_layout;
                                            }
                                            $class = '';
                                            $lName = JRequest::getVar('layout');
                                            if ((!$subMenu->menu_layout && $vName == $subMenu->menu_view ) || ($lName != '' && $lName == $subMenu->menu_layout)) {
                                                $class = ' class="active"';
                                            }
                                            if ($subMenu->menu_link != '')
                                                $href = $subMenu->menu_link;
                                            else
                                                $href = 'index.php?option=com_openshop&view=' . $subMenu->menu_view . $layoutLink;

                                            $html .= '<li' . $class . '><a href="' . $href . '" tabindex="-1"><span class="icon-' . $subMenu->menu_class . '"></span> ' . JText::_($subMenu->menu_name) . '</a></li>';
                                        }
                                    }
                                }
                            }
                        }
                        $html .= '</ul>';
                        $html .= '</li>';
                    }
                }
            }
        }
        $html .= '</ul>';
        if (version_compare(JVERSION, '3.0', 'le')) {
            JFactory::getDocument()->setBuffer($html, array('type' => 'modules', 'name' => 'submenu'));
        } else {
            echo $html;
        }
    }

    public static function loadLeftMenusBootstrap($vName = "dashboard") {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_menus')
                ->where('published = 1')
                ->where('menu_parent_id = 0')
                ->order('ordering');
        $db->setQuery($query);
        $menus = $db->loadObjectList();
        $html = '';
        $html .= '<ul class="nav nav-sidebar">';
        for ($i = 0; $n = count($menus), $i < $n; $i++) {

            $menu = $menus[$i];
            $showCondition = true;
//            if ($menu->menu_name == 'OPENSHOP_PLUGINS') {
//                if (!JFactory::getUser()->authorise('openshop.payments', 'com_openshop') && !JFactory::getUser()->authorise('openshop.shippings', 'com_openshop') && !JFactory::getUser()->authorise('openshop.themes', 'com_openshop'))
//                    $showCondition = false;
//            }
//            elseif ($menu->menu_name == 'OPENSHOP_SALES') {
//                if (!JFactory::getUser()->authorise('openshop.orders', 'com_openshop') && !JFactory::getUser()->authorise('openshop.customers', 'com_openshop') && !JFactory::getUser()->authorise('openshop.customergroups', 'com_openshop') && !JFactory::getUser()->authorise('openshop.coupons', 'com_openshop') && !JFactory::getUser()->authorise('openshop.vouchers', 'com_openshop'))
//                    $showCondition = false;
//            }
//            elseif ($menu->menu_name == 'OPENSHOP_REPORTS') {
//                if (!JFactory::getUser()->authorise('openshop.reports', 'com_openshop'))
//                    $showCondition = false;
//            }

            if ($showCondition) {
                $query->clear();
                $query->select('*')
                        ->from('#__openshop_menus')
                        ->where('published = 1')
                        ->where('menu_parent_id = ' . intval($menu->id))
                        ->order('ordering');
                $db->setQuery($query);
                $subMenus = $db->loadObjectList();
                //  echo count($subMenus);
                if (!count($subMenus)) {
                    $class = '';
                    if ($menu->menu_view == $vName) {
                        $class = ' class=""';
                    }


                    $html .= '<li><a href="index.php"><i class="' . $menu->menu_class . '"></i><span class="text">' . JText::_($menu->menu_name) . '</span></a></li>';
                } else {
                    $class = ' class="active"';
                    for ($j = 0; $m = count($subMenus), $j < $m; $j++) {
                        $subMenu = $subMenus[$j];
                        $lName = JRequest::getVar('layout');
                        if ((!$subMenu->menu_layout && $vName == $subMenu->menu_view ) || ($lName != '' && $lName == $subMenu->menu_layout)) {
                            $class = ' class="active"';
                            break;
                        }
                    }



                    $html .= '<li' . $class . '>';
                    $html .= '<a href="#"><i class="' . $menu->menu_class . '"></i><span class="text">' . JText::_($menu->menu_name) . '<span class="indicator"></span>' . '            <span class="fa fa-angle-double-down"> </span></a>';
                    $html .= '<ul class="sub_ul">';
                    for ($j = 0; $m = count($subMenus), $j < $m; $j++) {
                        $subMenu = $subMenus[$j];
                        $showSubCondition = true;
                        if ($subMenu->menu_view == 'reviews' && !JFactory::getUser()->authorise('openshop.reviews', 'com_openshop'))
                            $showSubCondition = false;
                        elseif ($subMenu->menu_view == 'taxclasses' && !JFactory::getUser()->authorise('openshop.taxclasses', 'com_openshop'))
                            $showSubCondition = false;
                        elseif ($subMenu->menu_view == 'taxrates' && !JFactory::getUser()->authorise('openshop.taxrates', 'com_openshop'))
                            $showSubCondition = false;
                        elseif ($subMenu->menu_view == 'configuration' && !JFactory::getUser()->authorise('openshop.configuration', 'com_openshop'))
                            $showSubCondition = false;
                        elseif ($subMenu->menu_view == 'tools' && !JFactory::getUser()->authorise('openshop.tools', 'com_openshop'))
                            $showSubCondition = false;
                        if ($showSubCondition) {
                            $layoutLink = '';
                            if ($subMenu->menu_layout) {
                                $layoutLink = '&layout=' . $subMenu->menu_layout;
                            }
                            $class = '';
                            $lName = JRequest::getVar('layout');
                            if ((!$subMenu->menu_layout && $vName == $subMenu->menu_view ) || ($lName != '' && $lName == $subMenu->menu_layout)) {
                                $class = ' class="active"';
                            }
                            $html .= '<li' . $class . '><a href="index.php?option=com_openshop&view=' .
                                    $subMenu->menu_view . $layoutLink . '" tabindex="-1"><span class="icon-' . $subMenu->menu_class . '"></span> ' . JText::_($subMenu->menu_name) . '</a></li>';
                        }
                    }
                    $html .= '</ul>';
                    $html .= '</li>';
                }
            }
        }
        $html .= '</ul>';

        echo $html;
    }

    /**
     * 
     * Function to get value for a message
     * @param string $messageName
     * @return string
     */
    public static function getMessageValue($messageName, $langCode = '') {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $language = JFactory::getLanguage();
        if (!$langCode)
            $langCode = $language->getTag();
        if (!$langCode)
            $langCode = 'en-GB';
        $language->load('com_openshop', JPATH_ROOT, $langCode);
        $query->select('a.message_value')
                ->from('#__openshop_messagedetails AS a')
                ->innerJoin('#__openshop_messages AS b ON a.message_id = b.id')
                ->where('a.language = ' . $db->quote($langCode))
                ->where('b.message_name = ' . $db->quote($messageName));
        $db->setQuery($query);
        $messageValue = $db->loadResult();
        if (!$messageValue) {
            $query->clear();
            $query->select('a.message_value')
                    ->from('#__openshop_messagedetails AS a')
                    ->innerJoin('#__openshop_messages AS b ON a.message_id = b.id')
                    ->where('a.language = "en-GB"')
                    ->where('b.message_name = ' . $db->quote($messageName));
            $db->setQuery($query);
            $messageValue = $db->loadResult();
        }
        return $messageValue;
    }

    /**
     *
     * Function to get information for a specific order
     * @param int $orderId
     * @return order Object
     */
    public static function getOrder($orderId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_orders')
                ->where('id = ' . (int) $orderId);
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     * 
     * Function to get products for a specific order
     * @param int $orderId
     */
    public static function getOrderProducts($orderId) {
        $order = self::getOrder($orderId);
        $currency = new OpenShopCurrency();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_orderproducts')
                ->where('order_id = ' . intval($orderId));
        $db->setQuery($query);
        $orderProducts = $db->loadObjectList();
        for ($i = 0; $n = count($orderProducts), $i < $n; $i++) {
            $orderProducts[$i]->orderOptions = self::getOrderOptions($orderProducts[$i]->id);
            $orderProducts[$i]->price = $currency->format($orderProducts[$i]->price, $order->currency_code, $order->currency_exchanged_value);
            $orderProducts[$i]->total_price = $currency->format($orderProducts[$i]->total_price, $order->currency_code, $order->currency_exchanged_value);
            //Get downloads for each order product
            $query->clear();
            $query->select('*')
                    ->from('#__openshop_orderdownloads')
                    ->where('order_id = ' . intval($orderId))
                    ->where('order_product_id = ' . $orderProducts[$i]->id);
            $db->setQuery($query);
            $orderProducts[$i]->downloads = $db->loadObjectList();
        }
        return $orderProducts;
    }

    /**
     * 
     * Function to get totals for a specific order
     * @param int $orderId
     * @return total object list
     */
    public static function getOrderTotals($orderId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_ordertotals')
                ->where('order_id = ' . intval($orderId))
                ->order('id');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     * 
     * Function to get options for a specific order product
     * @param unknown $orderProductId
     */
    public static function getOrderOptions($orderProductId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_orderoptions')
                ->where('order_product_id = ' . (int) $orderProductId);
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     *
     * Function to get information for a specific quote
     * @param int $quoteId
     * @return quote Object
     */
    public static function getQuote($quoteId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_quotes')
                ->where('id = ' . (int) $quoteId);
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     *
     * Function to get products for a specific quote
     * @param int $quoteId
     */
    public static function getQuoteProducts($quoteId) {
        $quote = self::getQuote($quoteId);
        $currency = new OpenShopCurrency();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_quoteproducts')
                ->where('quote_id = ' . (int) $quoteId);
        $db->setQuery($query);
        $quoteProducts = $db->loadObjectList();
        for ($i = 0; $n = count($quoteProducts), $i < $n; $i++) {
            $quoteProducts[$i]->quoteOptions = self::getQuoteOptions($quoteProducts[$i]->id);
            $quoteProducts[$i]->price = $currency->format($quoteProducts[$i]->price, $quote->currency_code, $quote->currency_exchanged_value);
            $quoteProducts[$i]->total_price = $currency->format($quoteProducts[$i]->total_price, $quote->currency_code, $quote->currency_exchanged_value);
        }
        return $quoteProducts;
    }

    /**
     *
     * Function to get options for a specific quote product
     * @param unknown $quoteProductId
     */
    public static function getQuoteOptions($quoteProductId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_quoteoptions')
                ->where('quote_product_id = ' . (int) $quoteProductId);
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     * 
     * Function to get invoice output for products
     * @param int $orderId
     * @return string
     */
    public static function getInvoiceProducts($orderId) {
        $viewConfig['name'] = 'invoice';
        $viewConfig['base_path'] = JPATH_ROOT . '/components/com_openshop/invoicetemplates';
        $viewConfig['template_path'] = JPATH_ROOT . '/components/com_openshop/invoicetemplates';
        $viewConfig['layout'] = 'default';
        $view = new JViewLegacy($viewConfig);
        $orderProducts = self::getOrderProducts($orderId);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        for ($i = 0; $n = count($orderProducts), $i < $n; $i++) {
            $query->clear();
            $query->select('*')
                    ->from('#__openshop_orderoptions')
                    ->where('order_product_id = ' . intval($orderProducts[$i]->id));
            $db->setQuery($query);
            $orderProducts[$i]->options = $db->loadObjectList();
        }
        $orderTotals = self::getOrderTotals($orderId);
        $view->assignRef('order_id', $orderId);
        $view->assignRef('order_products', $orderProducts);
        $view->assignRef('order_total', $orderTotals);
        ob_start();
        $view->display();
        $text = ob_get_contents();
        ob_end_clean();
        return $text;
    }

    /**
     * Generate invoice PDF
     * @param array $cid
     */
    public static function generateInvoicePDF($cid) {
        $mainframe = JFactory::getApplication();
        $sitename = $mainframe->getCfg("sitename");
        require_once JPATH_ROOT . "/components/com_openshop/tcpdf/tcpdf.php";
        require_once JPATH_ROOT . "/components/com_openshop/tcpdf/config/lang/eng.php";
        JTable::addIncludePath(JPATH_ROOT . '/administrator/components/com_openshop/tables');
        $invoiceOutputs = '';
        $filename = '';
        for ($i = 0; $n = count($cid), $i < $n; $i++) {
            $id = $cid[$i];
            $row = JTable::getInstance('OpenShop', 'Order');
            $row->load($id);
            // Initial pdf object
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor($sitename);
            $pdf->SetTitle('Invoice');
            $pdf->SetSubject('Invoice');
            $pdf->SetKeywords('Invoice');
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);
            $pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // Set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            // Set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetFont('times', '', 8);
            $pdf->AddPage();
            $invoiceOutput = self::getMessageValue('invoice_layout');

            // Store information
            $replaces = array();
            $replaces['customer_name'] = $row->firstname . ' ' . $row->lastname;
            $replaces['invoice_number'] = self::formatInvoiceNumber($row->invoice_number);
            $replaces['store_owner'] = self::getConfigValue('store_owner');
            $replaces['store_name'] = self::getConfigValue('store_name');
            $replaces['store_address'] = str_replace("\r\n", "<br />", self::getConfigValue('address'));
            $replaces['store_telephone'] = self::getConfigValue('telephone');
            $replaces['store_fax'] = self::getConfigValue('fax');
            $replaces['store_email'] = self::getConfigValue('email');
            $replaces['store_url'] = JUri::root();
            $replaces['date_added'] = JHtml::date($row->created_date, self::getConfigValue('date_format', 'm-d-Y'));
            $replaces['order_id'] = $row->id;
            $replaces['order_number'] = $row->order_number;
            $replaces['order_status'] = self::getOrderStatusName($row->order_status_id, $row->language);
            $replaces['payment_method'] = JText::_($row->payment_method_title);
            $replaces['shipping_method'] = $row->shipping_method_title;

            // Payment information
            $replaces['payment_address'] = self::getPaymentAddress($row);
            //Payment custom fields here
            $excludedFields = array('firstname', 'lastname', 'email', 'telephone', 'fax', 'company', 'company_id', 'address_1', 'address_2', 'city', 'postcode', 'country_id', 'zone_id');
            $form = new RADForm(self::getFormFields('B'));
            $fields = $form->getFields();
            foreach ($fields as $field) {
                $fieldName = $field->name;
                if (!in_array($fieldName, $excludedFields)) {
                    $fieldValue = $row->{'payment_' . $fieldName};
                    if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                        $fieldValue = implode(', ', json_decode($fieldValue));
                    }
                    $replaces['payment_' . $fieldName] = $fieldValue;
                }
            }
            // Shipping information
            $replaces['shipping_address'] = self::getShippingAddress($row);
            //Shipping custom fields here
            $form = new RADForm(self::getFormFields('S'));
            $fields = $form->getFields();
            foreach ($fields as $field) {
                $fieldName = $field->name;
                if (!in_array($fieldName, $excludedFields)) {
                    $fieldValue = $row->{'shipping_' . $fieldName};
                    if (is_string($fieldValue) && is_array(json_decode($fieldValue))) {
                        $fieldValue = implode(', ', json_decode($fieldValue));
                    }
                    $replaces['shipping_' . $fieldName] = $fieldValue;
                }
            }
            // Products list
            $replaces['products_list'] = self::getInvoiceProducts($row->id);
            // Comment
            $replaces['comment'] = $row->comment;
            // Delivery Date
            $replaces['delivery_date'] = JHtml::date($row->delivery_date, self::getConfigValue('date_format', 'm-d-Y'));
            foreach ($replaces as $key => $value) {
                $key = strtoupper($key);
                $invoiceOutput = str_replace("[$key]", $value, $invoiceOutput);
            }
            $invoiceOutput = self::convertImgTags($invoiceOutput);
            if ($n > 1 && $i < ($n - 1))
                $invoiceOutput = '<div style="page-break-after: always;">' . $invoiceOutput . '</div>';
            $invoiceOutputs .= $invoiceOutput;
            $filename .= self::formatInvoiceNumber($row->invoice_number);
            if ($i < ($n - 1))
                $filename .= '-';
        }
        $v = $pdf->writeHTML($invoiceOutputs, true, false, false, false, '');

        $filePath = JPATH_ROOT . '/images/com_openshop/invoices/' . $filename . '.pdf';
        $pdf->Output($filePath, 'F');
    }

    /**
     * 
     * Function to download invoice
     * @param array $cid
     */
    public static function downloadInvoice($cid) {
        JTable::addIncludePath(JPATH_ROOT . '/administrator/components/com_openshop/tables');
        $invoiceStorePath = JPATH_ROOT . '/images/com_openshop/invoices/';
        $filename = '';
        for ($i = 0; $n = count($cid), $i < $n; $i++) {
            $id = $cid[$i];
            $row = JTable::getInstance('OpenShop', 'Order');
            $row->load($id);
            $filename .= self::formatInvoiceNumber($row->invoice_number);
            if ($i < ($n - 1))
                $filename .= '-';
        }
        $filename .= '.pdf';
        self::generateInvoicePDF($cid);
        $invoicePath = $invoiceStorePath . $filename;
        while (@ob_end_clean());
        self::processDownload($invoicePath, $filename, true);
    }

    /**
     * 
     * Function to process download
     * @param string $filePath
     * @param string $filename
     * @param boolean $download
     */
    public static function processDownload($filePath, $filename, $download = false) {
        jimport('joomla.filesystem.file');
        $fsize = @filesize($filePath);
        $mod_date = date('r', filemtime($filePath));
        if ($download) {
            $cont_dis = 'attachment';
        } else {
            $cont_dis = 'inline';
        }
        $ext = JFile::getExt($filename);
        $mime = self::getMimeType($ext);
        // required for IE, otherwise Content-disposition is ignored
        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 'Off');
        }
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Expires: 0");
        header("Content-Transfer-Encoding: binary");
        header('Content-Disposition:' . $cont_dis . ';'
                . ' filename="' . JFile::getName($filename) . '";'
                . ' modification-date="' . $mod_date . '";'
                . ' size=' . $fsize . ';'
        ); //RFC2183
        header("Content-Type: " . $mime);   // MIME type
        header("Content-Length: " . $fsize);

        if (!ini_get('safe_mode')) { // set_time_limit doesn't work in safe mode
            @set_time_limit(0);
        }
        self::readfile_chunked($filePath);
    }

    /**
     * 
     * Function to get mimetype of file
     * @param string $ext
     * @return string
     */
    public static function getMimeType($ext) {
        require_once JPATH_ROOT . "/components/com_openshop/helpers/mime.mapping.php";
        foreach ($mime_extension_map as $key => $value) {
            if ($key == $ext) {
                return $value;
            }
        }
        return "";
    }

    /**
     * 
     * Function to read file
     * @param string $filename
     * @param boolean $retbytes
     * @return boolean|number
     */
    public static function readfile_chunked($filename, $retbytes = true) {
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        $buffer = '';
        $cnt = 0;
        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            echo $buffer;
            @ob_flush();
            flush();
            if ($retbytes) {
                $cnt += strlen($buffer);
            }
        }
        $status = fclose($handle);
        if ($retbytes && $status) {
            return $cnt; // return num. bytes delivered like readfile() does.
        }
        return $status;
    }

    /**
     * Convert all img tags to use absolute URL
     * @param string $html_content
     */
    public static function convertImgTags($html_content) {
        $patterns = array();
        $replacements = array();
        $i = 0;
        $src_exp = "/src=\"(.*?)\"/";
        $link_exp = "[^http:\/\/www\.|^www\.|^https:\/\/|^http:\/\/]";
        $siteURL = JUri::root();
        preg_match_all($src_exp, $html_content, $out, PREG_SET_ORDER);
        foreach ($out as $val) {
            $links = preg_match($link_exp, $val[1], $match, PREG_OFFSET_CAPTURE);
            if ($links == '0') {
                $patterns[$i] = $val[1];
                $patterns[$i] = "\"$val[1]";
                $replacements[$i] = $siteURL . $val[1];
                $replacements[$i] = "\"$replacements[$i]";
            }
            $i++;
        }
        $mod_html_content = str_replace($patterns, $replacements, $html_content);

        return $mod_html_content;
    }

    /**
     *
     * Function to get order number product
     * @param int $orderId
     */
    public static function getNumberProduct($orderId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(id)')
                ->from('#__openshop_orderproducts')
                ->where('order_id=' . intval($orderId));
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * 
     * Function to get substring
     * @param string $text
     * @param int $length
     * @param string $replacer
     * @param boolean $isAutoStripsTag
     * @return string
     */
    public static function substring($text, $length = 100, $replacer = '...', $isAutoStripsTag = true) {
        $string = $isAutoStripsTag ? strip_tags($text) : $text;
        return JString::strlen($string) > $length ? JHtml::_('string.truncate', $string, $length) : $string;
    }

    /**
     * 
     * Function to get alement alias
     * @param int $id
     * @param string $element
     * @param string $langCode
     * @return string
     */
    public static function getElementAlias($id, $element, $langCode = '') {
        if (!$langCode)
            $langCode = JFactory::getLanguage()->getTag();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($element . '_name, ' . $element . '_alias')
                ->from('#__openshop_' . $element . 'details')
                ->where($element . '_id = ' . (int) $id)
                ->where('language = "' . $langCode . '"');
        $db->setQuery($query);
        $row = $db->loadObject();
        if ($row->{$element . '_alias'} != '')
            return $row->{$element . '_alias'};
        else
            return $row->{$element . '_name'};
    }

    public static function stringAlias($string) {
        $trans = array(
            "đ" => "d", "ă" => "a", "â" => "a", "á" => "a", "à" => "a",
            "ả" => "a", "ã" => "a", "ạ" => "a",
            "ấ" => "a", "ầ" => "a", "ẩ" => "a", "ẫ" => "a", "ậ" => "a",
            "ắ" => "a", "ằ" => "a", "ẳ" => "a", "ẵ" => "a", "ặ" => "a",
            "é" => "e", "è" => "e", "ẻ" => "e", "ẽ" => "e", "ẹ" => "e",
            "ế" => "e", "ề" => "e", "ể" => "e", "ễ" => "e", "ệ" => "e",
            "í" => "i", "ì" => "i", "ỉ" => "i", "ĩ" => "i", "ị" => "i",
            "ư" => "u", "ô" => "o", "ơ" => "o", "ê" => "e",
            "Ư" => "u", "Ô" => "o", "Ơ" => "o", "Ê" => "e",
            "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u",
            "ứ" => "u", "ừ" => "u", "ử" => "u", "ữ" => "u", "ự" => "u",
            "ó" => "o", "ò" => "o", "ỏ" => "o", "õ" => "o", "ọ" => "o",
            "ớ" => "o", "ờ" => "o", "ở" => "o", "ỡ" => "o", "ợ" => "o",
            "ố" => "o", "ồ" => "o", "ổ" => "o", "ỗ" => "o", "ộ" => "o",
            "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u",
            "ứ" => "u", "ừ" => "u", "ử" => "u", "ữ" => "u", "ự" => "u",
            "ý" => "y", "ỳ" => "y", "ỷ" => "y", "ỹ" => "y", "ỵ" => "y",
            "Ý" => "Y", "Ỳ" => "Y", "Ỷ" => "Y", "Ỹ" => "Y", "Ỵ" => "Y",
            "Đ" => "D", "Ă" => "A", "Â" => "A", "Á" => "A", "À" => "A",
            "Ả" => "A", "Ã" => "A", "Ạ" => "A",
            "Ấ" => "A", "Ầ" => "A", "Ẩ" => "A", "Ẫ" => "A", "Ậ" => "A",
            "Ắ" => "A", "Ằ" => "A", "Ẳ" => "A", "Ẵ" => "A", "Ặ" => "A",
            "É" => "E", "È" => "E", "Ẻ" => "E", "Ẽ" => "E", "Ẹ" => "E",
            "Ế" => "E", "Ề" => "E", "Ể" => "E", "Ễ" => "E", "Ệ" => "E",
            "Í" => "I", "Ì" => "I", "Ỉ" => "I", "Ĩ" => "I", "Ị" => "I",
            "Ư" => "U", "Ô" => "O", "Ơ" => "O", "Ê" => "E",
            "Ư" => "U", "Ô" => "O", "Ơ" => "O", "Ê" => "E",
            "Ú" => "U", "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U",
            "Ứ" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U", "Ự" => "U",
            "Ó" => "O", "Ò" => "O", "Ỏ" => "O", "Õ" => "O", "Ọ" => "O",
            "Ớ" => "O", "Ờ" => "O", "Ở" => "O", "Ỡ" => "O", "Ợ" => "O",
            "Ố" => "O", "Ồ" => "O", "Ổ" => "O", "Ỗ" => "O", "Ộ" => "O",
            "Ú" => "U", "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U",
            "Ứ" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U", "Ự" => "U",);

        //remove any '-' from the string they will be used as concatonater
        $str = str_replace('_', ' ', $string);

        $str = strtr($str, $trans);
        $lang = JFactory::getLanguage();
        $str = $lang->transliterate($str);
//
//        // remove any duplicate whitespace, and ensure all characters are alphanumeric
        $str = preg_replace(array('/\s+/', '/[^A-Za-z0-9\-]/'), array('_', '_'), $str);

        // lowercase and trim
        $str = trim(strtolower($str));
        return $str;
    }

    /**
     * 
     * Function to get categories navigation 
     * @param int $id
     */
    public static function getCategoriesNavigation($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        //Find previous/next categories
        $query->select('a.id, b.category_id, b.category_name, b.category_page_title')
                ->from('#__openshop_categories AS a')
                ->innerJoin('#__openshop_categorydetails AS b ON (a.id = b.category_id)')
                ->where('a.published = 1')
                ->where('a.category_parent_id = (SELECT category_parent_id FROM #__openshop_categories WHERE id = ' . intval($id) . ')')
                ->where('b.language = "' . JFactory::getLanguage()->getTag() . '"')
                ->order('a.ordering');
        $db->setQuery($query);
        $categories = $db->loadObjectList();
        for ($i = 0; $n = count($categories), $i < $n; $i++) {
            if ($categories[$i]->id == $id)
                break;
        }
        $categoriesNavigation = array(isset($categories[$i - 1]) ? $categories[$i - 1] : '', isset($categories[$i + 1]) ? $categories[$i + 1] : '');
        return $categoriesNavigation;
    }

    /**
     *
     * Function to get products navigation
     * @param int $id
     */
    public static function getProductsNavigation($id) {
        $mainframe = JFactory::getApplication();
        $fromView = $mainframe->getUserState('from_view');
        $sortOptions = $mainframe->getUserState('sort_options');
        $allowedSortArr = array('a.ordering', 'b.product_name', 'a.product_sku', 'a.product_price', 'a.product_length', 'a.product_width', 'a.product_height', 'a.product_weight', 'a.product_quantity', 'b.product_short_desc', 'b.product_desc', 'product_rates', 'product_reviews');
        $allowedDirectArr = array('ASC', 'DESC');
        $sort = 'a.ordering';
        $direct = 'ASC';
        if ($sortOptions != '') {
            $sortOptions = explode('-', $sortOptions);
            if (isset($sortOptions[0]) && in_array($sortOptions[0], $allowedSortArr)) {
                $sort = $sortOptions[0];
            }
            if (isset($sortOptions[1]) && in_array($sortOptions[1], $allowedDirectArr)) {
                $direct = $sortOptions[1];
            }
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        if ($fromView == 'brand') {
            //Find previous/next products
            $query->select('a.id, b.product_id, b.product_name, b.product_page_title')
                    ->from('#__openshop_products AS a')
                    ->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
                    ->where('a.published = 1')
                    ->where('a.brand_id = (SELECT brand_id FROM #__openshop_products WHERE id = ' . intval($id) . ')')
                    ->where('b.language = "' . JFactory::getLanguage()->getTag() . '"')
                    ->order($sort . ' ' . $direct)
                    ->order('a.ordering');
        } else {
            $categoryId = JRequest::getInt('catid');
            if (!$categoryId)
                $categoryId = 0;
            //Find previous/next products
            $query->select('a.id, b.product_id, b.product_name, b.product_page_title, pc.category_id')
                    ->from('#__openshop_products AS a')
                    ->innerJoin('#__openshop_productdetails AS b ON (a.id = b.product_id)')
                    ->innerJoin('#__openshop_productcategories AS pc ON (a.id = pc.product_id)')
                    ->where('a.published = 1')
                    ->where('pc.category_id = ' . intval($categoryId))
                    ->where('b.language = "' . JFactory::getLanguage()->getTag() . '"')
                    ->order($sort . ' ' . $direct)
                    ->order('a.ordering');
        }
        $db->setQuery($query);
        $products = $db->loadObjectList();
        for ($i = 0; $n = count($products), $i < $n; $i++) {
            if ($products[$i]->id == $id)
                break;
        }
        $productsNavigation = array(isset($products[$i - 1]) ? $products[$i - 1] : '', isset($products[$i + 1]) ? $products[$i + 1] : '');
        return $productsNavigation;
    }

    /**
     * 
     * Function to get category id/alias path
     * @param int $id
     * @param string $type
     * @param string $langCode
     * @param int $parentId
     * @return array
     */
    public static function getCategoryPath($id, $type, $langCode = '', $parentId = 0) {
        static $categories;
        if (!$langCode)
            $langCode = JFactory::getLanguage()->getTag();
        $db = JFactory::getDbo();
        if (empty($categories)) {
            $query = $db->getQuery(true);
            $query->select('a.id, a.category_parent_id, b.category_alias')
                    ->from('#__openshop_categories AS a')
                    ->innerJoin('#__openshop_categorydetails AS b ON (a.id = b.category_id)')
                    ->where('b.language = "' . $langCode . '"');
            $db->setQuery($query);
            $categories = $db->loadObjectList('id');
        } $ids = array();

        $alias = array();
        do {
            if (!isset($categories[$id])) {
                break;
            }
            $alias[] = $categories[$id]->category_alias;
            $ids[] = $categories[$id]->id;
            $id = $categories[$id]->category_parent_id;
        } while ($id != $parentId);
        if ($type == 'id')
            return array_reverse($ids);
        else
            return array_reverse($alias);
    }

    /**
     * 
     * Function to get category id/alias path
     * @param int $id
     * @param string $type
     * @param string $langCode
     * @param int $parentId
     * @return array
     */
    public static function getBannerPath($id, $type, $langCode = '', $parentId = 0) {
        $res = array();
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_banners', 'a'))
                ->where('id = ' . $id);
        $row = $db->setQuery($query)->loadObject();
        $res[] = $row->alias;
        return array_reverse($res);
    }

    /**
     * 
     * Function to get categories bread crumb
     * @param int $id
     * @param int $parentId
     * @param string $langCode
     * @return array
     */
    public static function getCategoriesBreadcrumb($id, $parentId, $langCode = '') {
        $db = JFactory::getDbo();
        if (!$langCode)
            $langCode = JFactory::getLanguage()->getTag();
        $query = $db->getQuery(true);
        $query->select('a.id, a.category_parent_id, b.category_name')
                ->from('#__openshop_categories AS a')
                ->innerJoin('#__openshop_categorydetails AS b ON (a.id = b.category_id)')
                ->where('a.published = 1')
                ->where('b.language = "' . $langCode . '"');
        $db->setQuery($query);
        $categories = $db->loadObjectList('id');
        $paths = array();
        while ($id != $parentId) {
            if (isset($categories[$id])) {
                $paths[] = $categories[$id];
                $id = $categories[$id]->category_parent_id;
            } else {
                break;
            }
        }
        return $paths;
    }

    /**
     * 
     * Function to get category name path
     * @param int $id
     * @param string $langCode
     * @return string
     */
    public static function getCategoryNamePath($id, $langCode = '') {
        if (!$langCode)
            $langCode = JFactory::getLanguage()->getTag();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.category_parent_id, b.category_name')
                ->from('#__openshop_categories AS a')
                ->innerJoin('#__openshop_categorydetails AS b ON (a.id = b.category_id)')
                ->where('b.language = "' . $langCode . '"');
        $db->setQuery($query);
        $categories = $db->loadObjectList('id');
        $names = array();
        do {
            $names[] = $categories[$id]->category_name;
            $id = $categories[$id]->category_parent_id;
        } while ($id != 0);
        return array_reverse($names);
    }

    /**
     * 
     * Function to identify if price will be showed or not
     * @return boolean
     */
    public static function showPrice() {
        $displayPrice = OpenShopHelper::getConfigValue('display_price', 'public');
        if ($displayPrice == 'public') {
            $showPrice = true;
        } elseif ($displayPrice == 'hide') {
            $showPrice = false;
        } else {
            $user = JFactory::getUser();
            if ($user->get('id'))
                $showPrice = true;
            else
                $showPrice = false;
        }
        return $showPrice;
    }

    /**
     * 
     * Function to get default address id
     * @param int $id
     * @return int
     */
    public static function getDefaultAddressId($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('address_id')
                ->from('#__openshop_customers')
                ->where('customer_id = ' . (int) $id);
        $db->setQuery($query);
        return $db->loadResult();
    }
    
    /*
     * get all manufacture
     */
    public static function getManufactures(){
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('*')
                    ->from($db->quoteName('#__openshop_manufacturers'))
                    ->where('published = 1');
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }

     /*
     * get Manufacture Name
     */
    public static function getManufactureName($man_id){
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('manufacturer_name')
                    ->from($db->quoteName('#__openshop_manufacturers'))
                    ->where('id = '.(int)$man_id);
            $man_name = $db->setQuery($query)->loadResult();
            return $man_name;
        } catch (Exception $ex) {
            return '';
        }
    }
    

    /**
     * 
     * Function to count address for current user
     * @return int
     */
    public static function countAddress() {
        $user = JFactory::getUser();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(id)')
                ->from('#__openshop_addresses')
                ->where('customer_id=' . (int) $user->get('id'));
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * 
     * Function to get continue shopping url
     * @return string
     */
    public static function getContinueShopingUrl() {
        $session = JFactory::getSession();
        if ($session->get('continue_shopping_url')) {
            $url = $session->get('continue_shopping_url');
        } else {
            $url = JUri::root();
        }
        return $url;
    }

    /**
     * 
     * Function to get coupon
     * @param string $couponCode
     * @return object
     */
    public static function getCoupon($couponCode) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_coupons')
                ->where('coupon_code = "' . $couponCode . '"');
        $db->setQuery($query);
        return $db->loadObject();
    }

    /**
     *
     * Function to get voucher
     * @param string $voucherCode
     * @return object
     */
    public static function getVoucher($voucherCode) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_vouchers')
                ->where('voucher_code = "' . $voucherCode . '"');
        $db->setQuery($query);
        return $db->loadObject();
    }

    public static function getProductLabels($productId, $langCode = '') {
        if (!$langCode)
            $langCode = JFactory::getLanguage()->getTag();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('DISTINCT(label_id)')
                ->from('#__openshop_labelelements')
                ->where('(element_type = "product" AND element_id = ' . intval($productId) . ') OR (element_type = "brand" AND element_id = (SELECT brand_id FROM #__openshop_products WHERE id = ' . intval($productId) . ')) OR (element_type = "category" AND element_id IN (SELECT category_id FROM #__openshop_productcategories WHERE product_id = ' . intval($productId) . '))');
        $db->setQuery($query);
        $labelIds = $db->loadColumn();
        if (count($labelIds)) {
            $query->clear();
            $query->select('a.*, b.label_name')
                    ->from('#__openshop_labels AS a')
                    ->innerJoin('#__openshop_labeldetails AS b ON (a.id = b.label_id)')
                    ->where('a.id IN (' . implode(',', $labelIds) . ')')
                    ->where('a.published = 1')
                    ->where('(label_start_date = "0000-00-00 00:00:00" OR label_start_date < NOW())')
                    ->where('(label_end_date = "0000-00-00 00:00:00" OR label_end_date > NOW())')
                    ->where('b.language = "' . $langCode . '"')
                    ->order('a.ordering');
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            $imagePath = JPATH_ROOT . '/images/com_openshop/labels/';
            $imageSizeFunction = OpenShopHelper::getConfigValue('label_image_size_function', 'resizeImage');
            for ($i = 0; $n = count($rows), $i < $n; $i++) {
                $row = $rows[$i];
                if ($row->label_image) {
                    //Do the resize
                    $imageWidth = $row->label_image_width > 0 ? $row->label_image_width : OpenShopHelper::getConfigValue('image_label_width');
                    if (!$imageWidth)
                        $imageWidth = 50;
                    $imageHeight = $row->label_image_height > 0 ? $row->label_image_height : OpenShopHelper::getConfigValue('image_label_height');
                    if (!$imageHeight)
                        $imageHeight = 50;
                    if (!JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->label_image) . '-' . $imageWidth . 'x' . $imageHeight . '.' . JFile::getExt($row->label_image))) {
                        $rows[$i]->label_image = JUri::base(true) . '/images/com_openshop/labels/resized/' . call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($row->label_image, $imagePath, $imageWidth, $imageHeight));
                    } else {
                        $rows[$i]->label_image = JUri::base(true) . '/images/com_openshop/labels/resized/' . JFile::stripExt($row->label_image) . '-' . $imageWidth . 'x' . $imageHeight . '.' . JFile::getExt($row->label_image);
                    }
                }
            }
            return $rows;
        } else {
            return array();
        }
    }

    /**
     * Get URL of the site, using for Ajax request
     */
    public static function getSiteUrl() {
        $uri = JUri::getInstance();
        $base = $uri->toString(array('scheme', 'host', 'port'));
        if (strpos(php_sapi_name(), 'cgi') !== false && !ini_get('cgi.fix_pathinfo') && !empty($_SERVER['REQUEST_URI'])) {
            $script_name = $_SERVER['PHP_SELF'];
        } else {
            $script_name = $_SERVER['SCRIPT_NAME'];
        }
        $path = rtrim(dirname($script_name), '/\\');
        if ($path) {
            $siteUrl = $base . $path . '/';
        } else {
            $siteUrl = $base . '/';
        }
        if (JFactory::getApplication()->isAdmin()) {
            $adminPos = strrpos($siteUrl, 'administrator/');
            $siteUrl = substr_replace($siteUrl, '', $adminPos, 14);
        }
        return $siteUrl;
    }

    /**
     * 
     * Function to get checkout type
     * @return string
     */
    public static function getCheckoutType() {
        $cart = new OpenShopCart();
        if (OpenShopHelper::getConfigValue('display_price') == 'registered') {
            //Only registered
            $checkoutType = 'registered_only';
        } else {
            $checkoutType = OpenShopHelper::getConfigValue('checkout_type');
        }
        return $checkoutType;
    }

    /**
     * Get form billing or shopping form fields
     * @param string $addressType
     * @return array
     */
    public static function getFormFields($addressType, $excludedFields = array()) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__openshop_fields AS a')
                ->innerJoin('#__openshop_fielddetails AS b ON a.id=b.field_id')
                ->where('a.published = 1')
                ->where('(address_type=' . $db->quote($addressType) . ' OR address_type="A")')
                ->where('b.language = "' . JFactory::getLanguage()->getTag() . '"')
                ->order('a.ordering');
        if (count($excludedFields) > 0) {
            foreach ($excludedFields as $fieldName) {
                $query->where('name != "' . $fieldName . '"');
            }
        }
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /**
     * Check if the country has zones or not
     * @param int $countryId
     * @return boolean
     */
    public static function hasZone($countryId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(*)')
                ->from('#__openshop_zones')
                ->where('country_id=' . (int) $countryId)
                ->where('published=1');
        $db->setQuery($query);
        $total = (int) $db->loadResult();
        if ($total) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * 
     * Function to get Shipping Address Format
     * @param order object $row
     * @return string
     */
    public static function getShippingAddress($row) {
        $shippingAddress = self::getConfigValue('shipping_address_format', '[SHIPPING_FIRSTNAME] [SHIPPING_LASTNAME]<br /> [SHIPPING_ADDRESS_1], [SHIPPING_ADDRESS_2]<br /> [SHIPPING_CITY], [SHIPPING_POSTCODE] [SHIPPING_ZONE_NAME]<br /> [SHIPPING_EMAIL]<br /> [SHIPPING_TELEPHONE]<br /> [SHIPPING_FAX]');
        $shippingAddress = str_replace('[SHIPPING_FIRSTNAME]', $row->shipping_firstname, $shippingAddress);
        if (OpenShopHelper::isFieldPublished('lastname') && $row->shipping_lastname != '') {
            $shippingAddress = str_replace('[SHIPPING_LASTNAME]', $row->shipping_lastname, $shippingAddress);
        } else {
            $shippingAddress = str_replace('[SHIPPING_LASTNAME]', '', $shippingAddress);
        }
        $shippingAddress = str_replace('[SHIPPING_ADDRESS_1]', $row->shipping_address_1, $shippingAddress);
        if (OpenShopHelper::isFieldPublished('address_2') && $row->shipping_address_2 != '') {
            $shippingAddress = str_replace(', [SHIPPING_ADDRESS_2]', ', ' . $row->shipping_address_2, $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_ADDRESS_2]', $row->shipping_address_2, $shippingAddress);
        } else {
            $shippingAddress = str_replace(', [SHIPPING_ADDRESS_2]', '', $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_ADDRESS_2]', '', $shippingAddress);
        }
        if (OpenShopHelper::isFieldPublished('city')) {
            $shippingAddress = str_replace('<br /> [SHIPPING_CITY]', '<br />' . $row->shipping_city, $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_CITY]', $row->shipping_city, $shippingAddress);
        } else {
            $shippingAddress = str_replace('<br /> [SHIPPING_CITY]', '', $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_CITY]', '', $shippingAddress);
        }
        if (OpenShopHelper::isFieldPublished('postcode') && $row->shipping_postcode != '') {
            $shippingAddress = str_replace(', [SHIPPING_POSTCODE]', ', ' . $row->shipping_postcode, $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_POSTCODE]', $row->shipping_postcode, $shippingAddress);
        } else {
            $shippingAddress = str_replace(', [SHIPPING_POSTCODE]', '', $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_POSTCODE]', '', $shippingAddress);
        }
        $shippingAddress = str_replace('[SHIPPING_EMAIL]', $row->shipping_email, $shippingAddress);
        if (OpenShopHelper::isFieldPublished('telephone') && $row->shipping_telephone != '') {
            $shippingAddress = str_replace('<br /> [SHIPPING_TELEPHONE]', '<br /> ' . $row->shipping_telephone, $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_TELEPHONE]', $row->shipping_telephone, $shippingAddress);
        } else {
            $shippingAddress = str_replace('<br /> [SHIPPING_TELEPHONE]', '', $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_TELEPHONE]', '', $shippingAddress);
        }
        if (OpenShopHelper::isFieldPublished('fax') && $row->shipping_fax != '') {
            $shippingAddress = str_replace('<br /> [SHIPPING_FAX]', '<br /> ' . $row->shipping_fax, $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_FAX]', $row->shipping_fax, $shippingAddress);
        } else {
            $shippingAddress = str_replace('<br /> [SHIPPING_FAX]', '', $shippingAddress);
            $shippingAddress = str_replace('[SHIPPING_FAX]', '', $shippingAddress);
        }
        if (OpenShopHelper::isFieldPublished('company') && $row->shipping_company != '') {
            $shippingAddress = str_replace('[SHIPPING_COMPANY]', $row->shipping_company, $shippingAddress);
        } else {
            $shippingAddress = str_replace('[SHIPPING_COMPANY]', '', $shippingAddress);
        }
        if (OpenShopHelper::isFieldPublished('company_id') && $row->shipping_company_id != '') {
            $shippingAddress = str_replace('[SHIPPING_COMPANY_ID]', $row->shipping_company_id, $shippingAddress);
        } else {
            $shippingAddress = str_replace('[SHIPPING_COMPANY_ID]', '', $shippingAddress);
        }
        if (OpenShopHelper::isFieldPublished('zone_id') && $row->shipping_zone_name != '') {
            $shippingAddress = str_replace('[SHIPPING_ZONE_NAME]', $row->shipping_zone_name, $shippingAddress);
        } else {
            $shippingAddress = str_replace('[SHIPPING_ZONE_NAME]', '', $shippingAddress);
        }
        if (OpenShopHelper::isFieldPublished('country_id') && $row->shipping_country_name != '') {
            $shippingAddress = str_replace('[SHIPPING_COUNTRY_NAME]', $row->shipping_country_name, $shippingAddress);
        } else {
            $shippingAddress = str_replace('[SHIPPING_COUNTRY_NAME]', '', $shippingAddress);
        }
        return $shippingAddress;
    }

    /**
     *
     * Function to get Payment Address Format
     * @param order object $row
     * @return string
     */
    public static function getPaymentAddress($row) {
        $paymentAddress = self::getConfigValue('payment_address_format', '[PAYMENT_FIRSTNAME] [PAYMENT_LASTNAME]<br /> [PAYMENT_ADDRESS_1], [PAYMENT_ADDRESS_2]<br /> [PAYMENT_CITY], [PAYMENT_POSTCODE] [PAYMENT_ZONE_NAME]<br /> [PAYMENT_EMAIL]<br /> [PAYMENT_TELEPHONE]<br /> [PAYMENT_FAX]');
        $paymentAddress = str_replace('[PAYMENT_FIRSTNAME]', $row->payment_firstname, $paymentAddress);
        if (OpenShopHelper::isFieldPublished('lastname') && $row->payment_lastname != '') {
            $paymentAddress = str_replace('[PAYMENT_LASTNAME]', $row->payment_lastname, $paymentAddress);
        } else {
            $paymentAddress = str_replace('[PAYMENT_LASTNAME]', '', $paymentAddress);
        }
        $paymentAddress = str_replace('[PAYMENT_ADDRESS_1]', $row->payment_address_1, $paymentAddress);
        if (OpenShopHelper::isFieldPublished('address_2') && $row->payment_address_2 != '') {
            $paymentAddress = str_replace(', [PAYMENT_ADDRESS_2]', ', ' . $row->payment_address_2, $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_ADDRESS_2]', $row->payment_address_2, $paymentAddress);
        } else {
            $paymentAddress = str_replace(', [PAYMENT_ADDRESS_2]', '', $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_ADDRESS_2]', '', $paymentAddress);
        }
        if (OpenShopHelper::isFieldPublished('city')) {
            $paymentAddress = str_replace('<br /> [PAYMENT_CITY]', '<br /> ' . $row->payment_city, $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_CITY]', $row->payment_city, $paymentAddress);
        } else {
            $paymentAddress = str_replace('<br /> [PAYMENT_CITY]', '', $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_CITY]', '', $paymentAddress);
        }
        if (OpenShopHelper::isFieldPublished('postcode') && $row->payment_postcode != '') {
            $paymentAddress = str_replace(', [PAYMENT_POSTCODE]', ', ' . $row->payment_postcode, $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_POSTCODE]', $row->payment_postcode, $paymentAddress);
        } else {
            $paymentAddress = str_replace(', [PAYMENT_POSTCODE]', '', $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_POSTCODE]', '', $paymentAddress);
        }
        $paymentAddress = str_replace('[PAYMENT_EMAIL]', $row->payment_email, $paymentAddress);
        if (OpenShopHelper::isFieldPublished('telephone') && $row->payment_telephone != '') {
            $paymentAddress = str_replace('<br /> [PAYMENT_TELEPHONE]', '<br /> ' . $row->payment_telephone, $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_TELEPHONE]', $row->payment_telephone, $paymentAddress);
        } else {
            $paymentAddress = str_replace('<br /> [PAYMENT_TELEPHONE]', '', $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_TELEPHONE]', '', $paymentAddress);
        }
        if (OpenShopHelper::isFieldPublished('fax') && $row->payment_fax != '') {
            $paymentAddress = str_replace('<br /> [PAYMENT_FAX]', '<br /> ' . $row->payment_fax, $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_FAX]', $row->payment_fax, $paymentAddress);
        } else {
            $paymentAddress = str_replace('<br /> [PAYMENT_FAX]', '', $paymentAddress);
            $paymentAddress = str_replace('[PAYMENT_FAX]', '', $paymentAddress);
        }
        if (OpenShopHelper::isFieldPublished('company') && $row->payment_company != '') {
            $paymentAddress = str_replace('[PAYMENT_COMPANY]', $row->payment_company, $paymentAddress);
        } else {
            $paymentAddress = str_replace('[PAYMENT_COMPANY]', '', $paymentAddress);
        }
        if (OpenShopHelper::isFieldPublished('company_id') && $row->payment_company_id != '') {
            $paymentAddress = str_replace('[PAYMENT_COMPANY_ID]', $row->payment_company_id, $paymentAddress);
        } else {
            $paymentAddress = str_replace('[PAYMENT_COMPANY_ID]', '', $paymentAddress);
        }
        if (OpenShopHelper::isFieldPublished('zone_id') && $row->payment_zone_name != '') {
            $paymentAddress = str_replace('[PAYMENT_ZONE_NAME]', $row->payment_zone_name, $paymentAddress);
        } else {
            $paymentAddress = str_replace('[PAYMENT_ZONE_NAME]', '', $paymentAddress);
        }
        if (OpenShopHelper::isFieldPublished('country_id') && $row->payment_country_name != '') {
            $paymentAddress = str_replace('[PAYMENT_COUNTRY_NAME]', $row->payment_country_name, $paymentAddress);
        } else {
            $paymentAddress = str_replace('[PAYMENT_COUNTRY_NAME]', '', $paymentAddress);
        }
        return $paymentAddress;
    }

    /**
     * 
     * Function to identify if cart mode is available for a specific product or not
     * @param object $product
     * @return boolean
     */
    public static function isCartMode($product) {
        $isCartMode = true;
        if (OpenShopHelper::getConfigValue('catalog_mode')) {
            $isCartMode = false;
        } else {
            if (!OpenShopHelper::showPrice() || $product->product_call_for_price || ($product->product_quantity <= 0 && !OpenShopHelper::getConfigValue('stock_checkout'))) {
                $isCartMode = false;
            }
        }
        return $isCartMode;
    }

    /**
     *
     * Function to identify if quote mode is available for a specific product or not
     * @param object $product
     * @return boolean
     */
    public static function isQuoteMode($product) {
        if (OpenShopHelper::getConfigValue('quote_cart_mode') && $product->product_quote_mode) {
            $isQuoteMode = true;
        } else {
            $isQuoteMode = false;
        }
        return $isQuoteMode;
    }

    /**
     * 
     * Function to integrate with iDevAffiliate
     * @param order object $order
     */
    public static function iDevAffiliate($order) {
        $orderNumber = $order->order_number;
        $orderTotal = $order->total;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::getSiteUrl() . self::getConfigValue('idevaffiliate_path') . "/sale.php?profile=72198&idev_saleamt=" . $orderTotal . "&idev_ordernum=" . $orderNumber . "&ip_address=" . $ipAddress);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     * 
     * Function to check if a field is published or not
     * @param string $fieldName
     * @return boolean
     */
    public static function isFieldPublished($fieldName) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__openshop_fields')
                ->where('name = "' . $fieldName . '"')
                ->where('published = 1');
        $db->setQuery($query);
        if ($db->loadResult()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * Function to get installed version
     * @return string
     */
    public static function getInstalledVersion() {
        return '1.4.2';
    }

    /*
     * UPDATE STATE
     */

    public static function update_state($table, $col_u, $content_u, $col_w, $content_w) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->update($db->quoteName('#__openshop_' . $table))
                ->set($col_u . ' = ' . $content_u)
                ->where($col_w . ' = ' . $content_w);

        $db->setQuery($query);
        if ($db->execute())
            return true;
        else
            return false;
    }

    public static function deleteRecordTable($table, $id_where, $id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->delete($db->quoteName($table))
                ->where(array($id_where . ' = ' . $id));
        $db->setQuery($query);
        $db->execute();
    }

    public static function getStatusOrder($id_order) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $query->select('b.orderstatus_name')
                ->from($db->quoteName('#__openshop_orders', 'a'))
                ->join('LEFT', $db->quoteName('#__openshop_orderstatusdetails', 'b') . 'ON a.order_status_id = b.id')
                ->where('a.id = ' . $id_order);
        $db->setQuery($query);

        return $db->loadResult();
    }

    public static function getIdWarehouseInUserGroupArray() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);

        $ids = array();
        foreach (JFactory::getUser()->groups as $value) {
            $ids[] = $value;
        }

        $id_warehouse = array();

        $query->clear();
        $query->select('id,user_group_manage')
                ->from($db->quoteName('#__openshop_warehouses'));
        $rows = $db->setQuery($query)->loadObjectList();                        //get user group manage of warehouse

        foreach ($rows as $row) {                                               //in warehouse
            if (!empty($row->user_group_manage)) {
                $row_arr = explode(',', $row->user_group_manage);
                foreach ($ids as $id) {                                           //in user group
                    if (in_array($id, $row_arr)) {
                        $id_warehouse[] = $row->id;
                        break;
                    }
                }
            }
        }

        return $id_warehouse;
    }

    public static function chuyenChuoiKhongDau($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        return $str;
    }

    public static function getPermissionAction($viewName, $action) {
        $menu_name = 'OPENSHOP_' . strtoupper($viewName);

        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('permission')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('UPPER(menu_name) = "' . $menu_name . '"');
        $pers = json_decode($db->setQuery($query)->loadResult(), TRUE);

        $query->clear();
        $query->select('group_id')
                ->from($db->quoteName('#__user_usergroup_map'))
                ->where('user_id = ' . JFactory::getUser()->id);
        $id_usergroup = $db->setQuery($query)->loadObjectList();

        $check = 0;
        foreach ($id_usergroup as $id_ug) {
            if (isset($pers[$id_ug->group_id])) {
                $check = $pers[$id_ug->group_id][$action];
                if ($check) {
                    break;
                }
            }
        }

        return $check;
    }

    public static function getPermissionsDB() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('menu_name')
                ->from($db->quoteName('#__openshop_menus'));
        return $db->setQuery($query)->loadObjectList();
    }

    public static function checkCountChildPerView($subMenus, $id_usergroup) {
        $res = 0;
        foreach ($subMenus as $subMenu) {
            $per_sub = json_decode($subMenu->permission, TRUE);
            //check view 1=>show, 0=>hide
            if (isset($per_sub[$id_usergroup])) {
                if ($per_sub[$id_usergroup]['view'] == '1') {
                    $res = 1;
                    break;
                }
            }
        }

        return $res;
    }

    public static function get1RecordTable($table, $id, $column) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select($column)
                ->from($db->quoteName($table))
                ->where('id = ' . $id);
        return $db->setQuery($query)->loadResult();
    }

    public static function getUsergroupId($id_user) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('group_id')
                ->from($db->quoteName('#__user_usergroup_map'));
        return $db->setQuery($query)->loadResult();
    }

    /*
     * get information of user
     */

    public static function getUserInfo($idUser) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('*')
                    ->from($db->quoteName('#__openshop_customers', 'a'))
                    ->join('LEFT', $db->quoteName('#__openshop_addresses', 'b') . 'ON a.customer_id = b.customer_id')
                    ->where('a.customer_id = ' . $idUser);
            return $db->setQuery($query)->loadObject();
        } catch (Exception $ex) {
            return '';
        }
    }

    public static function getIdWarehouseUserF() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->clear();
        $query->select('id,user_group_manage')
                ->from($db->quoteName('#__openshop_warehouses'));
        $rows_warehouse = $db->setQuery($query)->loadObjectList();

        $usergroup_id = OpenShopHelper::getUsergroupId(JFactory::getUser()->id);
        $res_warehouse = array();
        foreach ($rows_warehouse as $v_id) {
            $arr = explode(',', $v_id->user_group_manage);
            foreach ($arr as $v_arr) {
                if ($v_arr == $usergroup_id) {
                    $res_warehouse[] = $v_id->id;
                    break;
                }
            }
        }

        return $res_warehouse[0];
    }

    public static function getActiveMenuLeft($view) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,menu_parent_id,menu_view')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('menu_view = "' . $view . '"');
        $menu_cur = $db->setQuery($query)->loadObject();
        if (isset($menu_cur)) {
            if (!$menu_cur->menu_parent_id) {
                return $menu_cur->menu_view;
            } else {
                return OpenShopHelper::getLoopMenuParent($menu_cur->menu_parent_id);
            }
        } else {
            return '';
        }
    }

    public static function getLoopMenuParent($parent_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->clear();
        $query->select('id,menu_parent_id,menu_view')
                ->from($db->quoteName('#__openshop_menus'))
                ->where('id = ' . $parent_id);
        $res = $db->setQuery($query)->loadObject();
        if (!$res->menu_parent_id) {
            return $res->menu_view;
        } else {
            self::getLoopMenuParent($res->id);
        }
    }

    public static function getDateTimeCurrent() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $now = getdate();
        $day = $now['mday'] < 10 ? '0' . $now['mday'] : $now['mday'];
        $month = $now['mon'] < 10 ? '0' . $now['mon'] : $now['mon'];
        $year = $now['year'];
        $hour = $now['hours'] < 10 ? '0' . $now['hours'] : $now['hours'];
        $minute = $now['minutes'] < 10 ? '0' . $now['minutes'] : $now['minutes'];
        $second = $now['seconds'] < 10 ? '0' . $now['seconds'] : $now['seconds'];

        return $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':' . $second;
    }

    public static function randomString() {
        $str = '';
        for ($i = 0; $i < 10; $i++) {
            $t = rand(1, 3);
            if ($t % 2 == 0) {
                $str .= rand(0, 9);
            } else {
                $s = 'mnbvcxzasdfghjklpoiuytrewq';
                $str .= substr($s, rand(1, 25), 1);
            }
        }

        return $str;
    }

    public static function convertBase64Encode($valueEncode) {
        $str_encode = self::randomString() . base64_encode($valueEncode);

        return $str_encode;
    }

    public static function convertBase64Decode($valueDecode) {
        $str_decode = substr($valueDecode, 10, strlen($valueDecode));

        return base64_decode($str_decode);
    }

    /*
     * Save information of VISITOR
     */

    public static function saveInfoVisitor() {
        try {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $user = JFactory::getSession()->get('user');
            $user_id = 0;
            if (!empty($user->id)) {
                $user_id = $user->id;
            }

            $data = new stdClass();
            $data->ip = $_SERVER['REMOTE_ADDR'];
            $data->user_id = $user_id;
            $data->date = self::getDateTimeCurrent();
            $data->browser = $_SERVER['HTTP_USER_AGENT'];
            $data->location = JFactory::getDocument()->getTitle();
            $ipnoaccess = array("66.249.79.12", "66.249.79.13", "66.249.79.11");
            if (!in_array($data->ip, $ipnoaccess)) {
                $db->insertObject('#__openshop_develops', $data);
            }
        } catch (Exception $ex) {
//            print_r($ex);
        }
    }

    public static function getValueSize() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_optiondetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'b') . 'ON a.option_id = b.option_id')
                ->where('UPPER(a.option_name) = "SIZE"');
        return $db->setQuery($query)->loadObjectList();
    }

    public static function getOpenshopTemplate($user_id, $col = null, $moduleWhere = null) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        if (isset($col)) {
            $query->select($col);
        } else {
            $query->select('*');
        }

        $query->from($db->quoteName('#__openshop_templates'))
                ->where('user_id = ' . $user_id);
        if (isset($moduleWhere)) {
            $query->where('module = "' . $moduleWhere . '"');
        }
        return $db->setQuery($query)->loadObject();
    }

    public static function getSizeColor($id_pro) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.optionvalue_id,a.value')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productoptionvalues', 'b') . 'ON a.optionvalue_id = b.option_value_id')
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . 'ON a.option_id = c.option_id')
                ->where('UPPER(c.option_name) = "SIZE"')
                ->where('b.product_id = ' . $id_pro);
        return $db->setQuery($query)->loadObjectList();
    }

    public static function getColorProduct($id_pro) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.optionvalue_id,a.value,a.img_color')
                ->from($db->quoteName('#__openshop_optionvaluedetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productoptionvalues', 'b') . 'ON a.optionvalue_id = b.option_value_id')
                ->join('INNER', $db->quoteName('#__openshop_optiondetails', 'c') . 'ON a.option_id = c.option_id')
                ->where('UPPER(c.option_name) = "COLOR"')
                ->where('b.product_id = ' . $id_pro);
        return $db->setQuery($query)->loadObject();
    }

    /*
     * get Warehouse
     */
    public static function getWareHouse($id_warehouse){
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_warehouses'))
                ->where('id = ' . $id_warehouse);
        return $db->setQuery($query)->loadObject();
    }
    
    /*
     * return array id product
     */

    public static function getProductColorDifferent($idProduct) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $idProArr = array();
        try {
            $query->select('*')
                    ->from($db->quoteName('#__openshop_product_different_colors'))
                    ->where('product_id = ' . $idProduct, 'OR')
                    ->where('product_id_diff = ' . $idProduct);
            $rows = $db->setQuery($query)->loadObjectList();
            if (count($rows)) {
                $id_product = '';
                foreach ($rows as $row) {
                    $id_product = $row->product_id;
                    if ($id_product == $idProduct) {
                        $id_product = $row->product_id_diff;
                    }
                    $idProArr[] = $id_product;
                }
            }

            return $idProArr;
        } catch (Exception $ex) {
            return $idProArr;
        }
    }

    /*
     * get product detail in array
     */

    public static function getProductInArray($idArr, $langCode = '') {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $idProArr = implode(',', $idArr);
        try {
            $query->select('a.*,b.product_name')
                    ->from($db->quoteName('#__openshop_products', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id')
                    ->where('a.delete_status = 0')
                    ->where('a.published = 1')
                    ->where('a.id IN (' . $idProArr . ')');
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return array();
        }
    }

    /*
     * lấy sản phẩm theo id category
     */

    public static function getProductsOfCategory($idCategory, $limit = '5') {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                ->join('INNER', $db->quoteName('#__openshop_productcategories', 'c') . 'ON a.id = c.product_id')
                ->where('c.category_id = ' . $idCategory)
                ->where('a.published = 1')
                ->where('a.delete_status = 0');
        return $db->setQuery($query, 0, $limit)->loadObjectList();
    }

    public static function addCustoms() {
        JFactory::getDocument()->addStyleSheet(JURI::base() . '/components/com_openshop/assets/css/toastr.min.css');

        JFactory::getDocument()->addScript(JURI::base() . 'components/com_openshop/assets/js/jquery.min.js');
        JFactory::getDocument()->addScript(JURI::base() . 'components/com_openshop/assets/js/script_admin.js');
        JFactory::getDocument()->addScript(JURI::base() . '/components/com_openshop/assets/js/toastr.min.js');
        JFactory::getDocument()->addScript(JURI::base() . 'components/com_openshop/assets/js/openshop.js');
    }
    
    public static function getDonViProduct($dv){
        switch ($dv){
            case '1':
                return 'cái';
                break;
            case '0':
                return 'cặp';
                break;
            case '2':
                return 'set';
                break;
            default :
                return 'cái';
                break;
        }
    }

}
