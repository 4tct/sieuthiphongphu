<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * Options helper class
 *
 */
class OpenShopOption
{
	/**
	 * 
	 * Function to render an option input for a product
	 * @param int $productId
	 * @param int $optionId
	 * @param int $optionType
	 * @param int $taxClassId
	 * @return html code
	 */
	public static function renderOption($productId, $optionId, $optionType, $taxClassId)
	{
		$currency = new OpenShopCurrency();
		$tax = new OpenShopTax(OpenShopHelper::getConfig());
		$product = OpenShopHelper::getProduct($productId);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id')
			->from('#__openshop_productoptions')
			->where('product_id = ' . intval($productId))
			->where('option_id = ' . intval($optionId));
		$db->setQuery($query);
		$productOptionId = $db->loadResult();
		switch ($optionType)
		{
		    case 'Text':
		    case 'Textarea':
		        $query->clear()
    		        ->select('price, price_sign')
    		        ->from('#__openshop_productoptionvalues')
    		        ->where('product_option_id = ' . intval($productOptionId));
		        break;
		    default:
		        $query->clear()
					->select('ovd.value, pov.id, pov.price, pov.price_sign, pov.image')
					->from('#__openshop_optionvalues AS ov')
					->innerJoin('#__openshop_optionvaluedetails AS ovd ON (ov.id = ovd.optionvalue_id)')
					->innerJoin('#__openshop_productoptionvalues AS pov ON (ovd.optionvalue_id = pov.option_value_id)')
					->where('pov.product_option_id = ' . intval($productOptionId))
					->where('ovd.language = "' . JFactory::getLanguage()->getTag() . '"')
					->order('pov.id');
				if (OpenShopHelper::getConfigValue('hide_out_of_stock_products'))
				{
					$query->where('pov.quantity > 0');
				}
		        break;
		}
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$optionHtml = '';
        $optionImage = '';
		$imagePath = JPATH_ROOT . '/images/com_openshop/options/';
		$imageSizeFunction = OpenShopHelper::getConfigValue('option_image_size_function', 'resizeImage');
        $thumbImageSizeFunction = OpenShopHelper::getConfigValue('thumb_image_size_function', 'resizeImage');
		$popupImageSizeFunction = OpenShopHelper::getConfigValue('popup_image_size_function', 'resizeImage');
		for ($i = 0; $n = count($rows), $i < $n; $i++)
		{
			$row = $rows[$i];
			if (OpenShopHelper::showPrice() && $row->price > 0 && !$product->product_call_for_price)
			{
				if ($optionType == 'Text' || $optionType == 'Textarea')
				{
					$rows[$i]->text = '('.$row->price_sign.$currency->format($tax->calculate($row->price, $taxClassId, OpenShopHelper::getConfigValue('tax'))).' ' . JText::_('OPENSHOP_PER_CHAR') . ')';
				}
				else 
				{
					$rows[$i]->text = $row->value.' ('.$row->price_sign.$currency->format($tax->calculate($row->price, $taxClassId, OpenShopHelper::getConfigValue('tax'))).')';
				}
			}
			else
				$rows[$i]->text = isset($row->value) ? $row->value : '';
			if ($optionType != 'Text' && $optionType != 'Textarea')
			{
				$rows[$i]->value = $row->id;
			}
			//Resize option image
			if (isset($row->image) && $row->image != '')
			{
                $thumbImage = call_user_func_array(array('OpenShopHelper', $thumbImageSizeFunction), array($row->image, $imagePath, OpenShopHelper::getConfigValue('image_thumb_width'), OpenShopHelper::getConfigValue('image_thumb_height')));
				$popupImage = call_user_func_array(array('OpenShopHelper', $popupImageSizeFunction), array($row->image, $imagePath, OpenShopHelper::getConfigValue('image_popup_width'), OpenShopHelper::getConfigValue('image_popup_height')));
                $rows[$i]->thumb_image = JUri::base(true) . '/images/com_openshop/options/resized/' . $thumbImage;
				$rows[$i]->popup_image = JUri::base(true) . '/images/com_openshop/options/resized/' . $popupImage;
                
				$imageWidth = OpenShopHelper::getConfigValue('image_option_width');
				if (!$imageWidth)
					$imageWidth = 100;
				$imageHeight = OpenShopHelper::getConfigValue('image_option_height');
				if (!$imageHeight)
					$imageHeight = 100;
				if (!JFile::exists($imagePath . 'resized/' . JFile::stripExt($row->image).'-'.$imageWidth.'x'.$imageHeight.'.'.JFile::getExt($row->image)))
				{
					$rows[$i]->image = JUri::base(true) . '/images/com_openshop/options/resized/' . call_user_func_array(array('OpenShopHelper', $imageSizeFunction), array($row->image, $imagePath, $imageWidth, $imageHeight));
				}
				else
				{
					$rows[$i]->image = JUri::base(true) . '/images/com_openshop/options/resized/' . JFile::stripExt($row->image).'-'.$imageWidth.'x'.$imageHeight.'.'.JFile::getExt($row->image);
				}
				if (OpenShopHelper::getConfigValue('view_image') == 'zoom')
				{
					$optionImage .= '<a id="option-image-'.$rows[$i]->id.'" class="option-image-zoom" href="javascript:void(0);" rel="{gallery: \'product-thumbnails\', smallimage: \''.$rows[$i]->thumb_image.'\',largeimage: \''.$rows[$i]->popup_image.'\'}">
										<img src="'.$rows[$i]->image.'">
									</a>';
				}
				else
				{
					$optionImage .= '<a id="option-image-'.$rows[$i]->id.'" class="product-image" href="'.$rows[$i]->popup_image.'">
										<img src="' . $rows[$i]->thumb_image . '" title="'.$rows[$i]->text.'" alt="'.$rows[$i]->text.'" />
									</a>';
				}
			}
		}
        if ($optionImage != '')
        	$optionHtml .= '<span style="display:none;" class="option-image">'.$optionImage.'</span>';
		if (OpenShopHelper::isCartMode($product) || OpenShopHelper::isQuoteMode($product))
		{
			$updatePrice = '';
			if (OpenShopHelper::getConfigValue('dynamic_price') && OpenShopHelper::showPrice() && !$product->product_call_for_price)
			    switch ($optionType)
			    {
			        case 'Text':
			        case 'Textarea':
			            $updatePrice = ' onkeyup="updatePrice();"';
			            break;
			        default:
			            $updatePrice = ' onchange="updatePrice();"';
			            break;
			    }
				
			switch ($optionType)
			{
				case 'Select':
					$options[] = JHtml::_('select.option', '', JText::_('OPENSHOP_PLEASE_SELECT'), 'value', 'text');
					$optionHtml .= JHtml::_('select.genericlist', array_merge($options, $rows), 'options['.$productOptionId.']',
						array(
							'option.text.toHtml' => false,
							'option.value' => 'value',
							'option.text' => 'text',
							'list.attr' => ' class="inputbox"' . $updatePrice));
					break;
				case 'Checkbox':                    
					for ($i = 0; $n = count($rows), $i < $n; $i++)
					{
						$optionHtml .= '<label class="checkbox">';
						$optionHtml .= '<input type="checkbox" name="options['.$productOptionId.'][]" value="'.$rows[$i]->id.'"' . $updatePrice . '> '.$rows[$i]->text;
						$optionHtml .= '</label>';						
					}                                                              
					break;
				case 'Radio':                    
					for ($i = 0; $n = count($rows), $i < $n; $i++)
					{
						$optionHtml .= '<label class="radio">';
						$optionHtml .= '<input type="radio" name="options['.$productOptionId.']" value="'.$rows[$i]->id.'"' .$updatePrice . '> '.$rows[$i]->text;
						$optionHtml .= '</label>';						
					}
					break;
				case 'Text':				   
					$optionHtml .= '<input type="text" name="options['.$productOptionId.']" value=""' .$updatePrice.' />'.$rows[0]->text;									
					break;
				case 'Textarea':
					$optionHtml .= '<textarea name="options['.$productOptionId.']" cols="40" rows="5"' .$updatePrice.' ></textarea>'.$rows[0]->text;					
					break;
				case 'File':
					$optionHtml .= '<input type="button" value="'.JText::_('OPENSHOP_UPLOAD_FILE').'" id="button-option-'.$productOptionId.'" class="btn btn-primary">';
					$optionHtml .= '<input type="hidden" name="options['.$productOptionId.']" value="" />';
					break;	
				case 'Date':
					$optionHtml .= JHtml::_('calendar', '', 'options['.$productOptionId.']', 'options['.$productOptionId.']', '%Y-%m-%d');
					break;
				case 'Datetime':
					$optionHtml .= JHtml::_('calendar', '', 'options['.$productOptionId.']', 'options['.$productOptionId.']', '%Y-%m-%d 00:00:00');
					break;
				default:
					break;
			}
		}
		else 
		{
			for ($i = 0; $n = count($rows), $i < $n; $i++)
			{
				echo $rows[$i]->text . '<br />';
			}
		}
		return $optionHtml;
	}
}
?>