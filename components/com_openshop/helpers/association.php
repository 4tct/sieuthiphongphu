<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

JLoader::register('CategoryHelperAssociation', JPATH_ADMINISTRATOR . '/components/com_categories/helpers/association.php');
/**
 * OpenShop Component Association Helper
 *
 * @package		Joomla
 * @subpackage	OpenShop
 * @since 3.0
 */
abstract class OpenShopHelperAssociation extends CategoryHelperAssociation
{
	/**
	 * Method to get the associations for a given item
	 *
	 * @param   integer  $id    Id of the item
	 * @param   string   $view  Name of the view
	 *
	 * @return  array   Array of associations for the item
	 *
	 * @since  3.0
	 */

	public static function getAssociations($id = 0, $view = null)
	{
		jimport('helper.route', JPATH_COMPONENT_SITE);

		$app = JFactory::getApplication();
		$jinput = $app->input;
		$view = is_null($view) ? $jinput->get('view') : $view;
		$id = empty($id) ? $jinput->getInt('id') : $id;
		$return = array();
		if ($view == 'product' || $view == 'category' || $view == 'manufacturer')
		{
			if ($id)
			{
				$associations = OpenShopHelper::getAssociations($id, $view);
				foreach ($associations as $tag => $item)
				{
					if ($view == 'product')
					{
						$return[$tag] = OpenShopRoute::getProductRoute($item->product_id, OpenShopHelper::getProductCategory($item->product_id), $item->language);
					}
					if ($view == 'category')
					{
						$return[$tag] = OpenShopRoute::getCategoryRoute($item->category_id, $item->language);
					}
				}
			}
		}
		elseif ($view == 'cart' || $view == 'checkout' || $view == 'wishlist' || $view == 'compare' || $view == 'customer')
		{
			$languages = OpenShopHelper::getLanguages();
			foreach ($languages as $language)
			{
				if ($language->lang_code != JFactory::getLanguage()->getTag())
				{
					$return[$language->lang_code] = OpenShopRoute::getViewRoute($view, $language->lang_code);
				}
			}
		}
		return $return;
	}
}