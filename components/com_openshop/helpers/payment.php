<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');


class OpenShopPayment
{

	/**
	 * 
	 * Function to get Costs, passed by reference to update
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	public function getCosts(&$totalData, &$total, &$taxes)
	{
		$session = JFactory::getSession();
		$paymentMethod = $session->get('payment_method');
		$currency = new OpenShopCurrency();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('params')
			->from('#__openshop_payments')
			->where('name = "' . $paymentMethod . '"');
		$db->setQuery($query);
		$paymentPlugin = $db->loadObject();
		if (is_object($paymentPlugin))
		{
			$params = new JRegistry($paymentPlugin->params);
			$paymentFee = $params->get('payment_fee');
			$minSubTotal = $params->get('min_sub_total');
			if ($minSubTotal > 0)
			{
				if ($total >= $minSubTotal)
				{
					$paymentFee = 0;
				}
			}
			
			$percentage = false;
			if (strpos($paymentFee, '%'))
			{
				$percentage = true;
				$paymentFee = str_replace('%', '', $paymentFee);
			}
			
			if ($paymentFee > 0)
			{
				if ($percentage)
				{
					$paymentFee = $paymentFee * $total / 100;
				}
				$totalData[] = array(
					'name'		=> 'payment_fee',
					'title'		=> JText::_('OPENSHOP_PAYMENT_FEE'),
					'text'		=> $currency->format($paymentFee),
					'value'		=> $paymentFee);
				if ($params->get('taxclass_id'))
				{
					$tax = new OpenShopTax(OpenShopHelper::getConfig());
					$taxRates = $tax->getTaxRates($paymentFee, $params->get('taxclass_id'));
					foreach ($taxRates as $taxRate)
					{
						if (!isset($taxes[$taxRate['tax_rate_id']]))
						{
							$taxes[$taxRate['tax_rate_id']] = $taxRate['amount'];
						}
						else
						{
							$taxes[$taxRate['tax_rate_id']] += $taxRate['amount'];
						}
					}
				}
				$total += $paymentFee;
			}
		}	
	}
}