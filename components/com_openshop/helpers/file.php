<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class OpenShopFile
{
	
	/**
	 * 
	 * Function to check file before uploading
	 * @param file $file
	 */
	public static function checkFileUpload($file)
	{
		$error = array();
		if (is_array($file['name']))
		{
			for ($i = 0; $n = count($file['name']), $i < $n; $i++)
			{
				if ($file['name'][$i] != '')
				{
					$fileName = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($file['name'][$i], ENT_QUOTES, 'UTF-8')));
					if ((strlen($fileName) < 3) || (strlen($fileName) > 64))
					{
						$error[] = JText::_('OPENSHOP_UPLOAD_ERROR_FILENAME');
					}
					//Allowed file extension types
					$allowed = array();
					$fileTypes = explode("\n", OpenShopHelper::getConfigValue('file_extensions_allowed'));
					foreach ($filpeeTypes as $fileType)
					{
						$allowed[] = strtolower(trim($fileType));
					}
					if (!in_array(strtolower(substr(strrchr($fileName, '.'), 1)), $allowed))
					{
						$error[] = JText::_('OPENSHOP_UPLOAD_ERROR_FILETYPE');
					}
					// Allowed file mime types
					$allowed = array();
					$fileTypes = explode("\n", OpenShopHelper::getConfigValue('file_mime_types_allowed'));
					foreach ($fileTypes as $fileType)
					{
						$allowed[] = strtolower(trim($fileType));
					}
					if (!in_array(strtolower($file['type'][$i]), $allowed))
					{
						$error[] = JText::_('OPENSHOP_UPLOAD_ERROR_FILE_MIME_TYPE');
					}
					if ($file['error'][$i] != UPLOAD_ERR_OK)
					{
						$error[] = JText::_('OPENSHOP_ERROR_UPLOAD_' . $file['error'][$i]);
					}
					if (count($error))
						break;
				}
			}
		}
		else 
		{
			$fileName = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($file['name'], ENT_QUOTES, 'UTF-8')));
			if ((strlen($fileName) < 3) || (strlen($fileName) > 64))
			{
				$error[] = JText::_('OPENSHOP_UPLOAD_ERROR_FILENAME');
			}
			//Allowed file extension types
			$allowed = array();
			$fileTypes = explode("\n", OpenShopHelper::getConfigValue('file_extensions_allowed'));
			foreach ($fileTypes as $fileType)
			{
				$allowed[] = strtolower(trim($fileType));
			}
			if (!in_array(strtolower(substr(strrchr($fileName, '.'), 1)), $allowed))
			{
				$error[] = JText::_('OPENSHOP_UPLOAD_ERROR_FILETYPE');
			}
			// Allowed file mime types
			$allowed = array();
			$fileTypes = explode("\n", OpenShopHelper::getConfigValue('file_mime_types_allowed'));
			foreach ($fileTypes as $fileType)
			{
				$allowed[] = strtolower(trim($fileType));
			}
			if (!in_array(strtolower($file['type']), $allowed))
			{
				$error[] = JText::_('OPENSHOP_UPLOAD_ERROR_FILE_MIME_TYPE');
			}
			if ($file['error'] != UPLOAD_ERR_OK)
			{
				$error[] = JText::_('OPENSHOP_ERROR_UPLOAD_' . $file['error']);
			}	
		}
		if (count($error) > 0)
		{
			return $error;
		}
		else
		{
			return true;
		}
	}
}