<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');


class OpenShopCustomer
{

	/**
	 * 
	 * Function to get customer group id
	 */
	public function getCustomerGroupId()
	{
		$user = Jfactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('customergroup_id')
			->from('#__openshop_customers')
			->where('customer_id = ' . intval($user->get('id')));
		$db->setQuery($query);
		if ($db->loadResult())
			$customerGroupId = $db->loadResult();
		else
			$customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
		return $customerGroupId;
	}
}