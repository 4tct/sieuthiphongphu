<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');


class OpenShopDonate
{

	/**
	 * 
	 * Function to get Costs, passed by reference to update
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	public function getCosts(&$totalData, &$total, &$taxes)
	{
		$currency = new OpenShopCurrency();
		$session = JFactory::getSession();
		$donateAmount = $session->get('donate_amount');
		if ($donateAmount > 0)
		{
			$totalData[] = array(
				'name'		=> 'donate_amount',
				'title'		=> JText::_('OPENSHOP_DONATE_AMOUNT'),
				'text'		=> $currency->format($donateAmount),
				'value'		=> $donateAmount);
			$total += $donateAmount;
		}
	}
}