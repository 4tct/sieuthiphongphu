<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');


class OpenShopShipping
{

	/**
	 * 
	 * Function to get Costs, passed by reference to update
	 * @param  array $totalData
	 * @param  float $total
	 * @param  array $taxes
	 */
	public function getCosts(&$totalData, &$total, &$taxes)
	{
		$session = JFactory::getSession();
		$currency = new OpenShopCurrency();
		$shippingMethod = $session->get('shipping_method');
		if (is_array($shippingMethod))
		{
			$totalData[] = array(
				'name'		=> 'shipping',
				'title'		=> $shippingMethod['title'],
				'text'		=> $currency->format($shippingMethod['cost']),
				'value'		=> $shippingMethod['cost']);
			
			if (!empty($shippingMethod['taxclass_id']))
			{
				$tax = new OpenShopTax(OpenShopHelper::getConfig());
				$taxRates = $tax->getTaxRates($shippingMethod['cost'], $shippingMethod['taxclass_id']);
				foreach ($taxRates as $taxRate)
				{
					if (!isset($taxes[$taxRate['tax_rate_id']]))
					{
						$taxes[$taxRate['tax_rate_id']] = $taxRate['amount'];
					}
					else
					{
						$taxes[$taxRate['tax_rate_id']] += $taxRate['amount'];
					}
				}
			}
			$total += $shippingMethod['cost'];
		}
	}
}