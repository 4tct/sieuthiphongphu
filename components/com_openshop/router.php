<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

/**
 * 
 * Build the route for the com_openshop component
 * @param	array	An array of URL arguments
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 * @since	1.5
 */
function OpenShopBuildRoute(&$query) {
    
    
    $segments = array();
    require_once JPATH_ROOT . '/components/com_openshop/helpers/helper.php';
    require_once JPATH_ROOT . '/components/com_openshop/helpers/' . ((version_compare(JVERSION, '3.0', 'ge') && JLanguageMultilang::isEnabled() && count(OpenShopHelper::getLanguages()) > 1) ? 'routev3.php' : 'route.php');
    $db = JFactory::getDbo();
    $queryArr = $query;
    if (isset($queryArr['option']))
        unset($queryArr['option']);
    if (isset($queryArr['Itemid']))
        unset($queryArr['Itemid']);
    //Store the query string to use in the parseRouter method
    $queryString = http_build_query($queryArr);

    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    
    //We need a menu item.  Either the one specified in the query, or the current active one if none specified
    if (empty($query['Itemid']))
        $menuItem = $menu->getActive();
    else
        $menuItem = $menu->getItem($query['Itemid']);

    if (empty($menuItem->query['view'])) {
        $menuItem->query['view'] = '';
    }
    //Are we dealing with an product or category that is attached to a menu item?
    if (($menuItem instanceof stdClass) && isset($query['view']) && isset($query['id']) && $menuItem->query['view'] == $query['view'] && isset($query['id']) && $menuItem->query['id'] == intval($query['id'])) {
        unset($query['view']);
        if (isset($query['catid']))
            unset($query['catid']);
        unset($query['id']);
    }

    if (($menuItem instanceof stdClass) && $menuItem->query['view'] == 'category' && isset($query['catid']) && $menuItem->query['id'] == intval($query['catid'])) {
        if (isset($query['catid']))
            unset($query['catid']);
    }

    $parentId = 0;
    if (($menuItem instanceof stdClass)) {
        if (isset($menuItem->query['view']) && $menuItem->query['view'] == 'category') {
            $parentId = (int) $menuItem->query['id'];
        }
    }

    $view = isset($query['view']) ? $query['view'] : '';
    $id = isset($query['id']) ? (int) $query['id'] : 0;
    $catid = isset($query['catid']) ? (int) $query['catid'] : 0;

    if ($view == 'cart' || $view == 'quote' || $view == 'checkout' || $view == 'wishlist' || $view == 'compare' || $view == 'customer') {
        if (isset($query['Itemid']) && !OpenShopRoute::findView($view, isset($query['l']) ? $query['l'] : '')) {
            unset($query['Itemid']);
        }
    }
  
//    exit();
    switch ($view) {
        case 'search':
            if (!isset($query['Itemid']) || (isset($query['Itemid']) && $query['Itemid'] == OpenShopRoute::getDefaultItemId(isset($query['l']) ? $query['l'] : ''))) {
                if (isset($query['l'])) {
                    $language = JLanguage::getInstance($query['l'], 0);
                    $language->load('com_openshop', JPATH_ROOT, $query['l']);
                    $segments[] = $language->_('OPENSHOP_SEARCH_RESULT');
                } else {
                    $segments[] = JText::_('OPENSHOP_SEARCH_RESULT');
                }
            }
            break;
        case 'category' :
            if ($id)
                $segments = array_merge($segments, OpenShopHelper::getCategoryPath($id, 'alias', isset($query['l']) ? $query['l'] : '', $parentId));
            break;
        case 'product' :
            if ($id) {
                $segments[] = OpenShopHelper::getElementAlias($id, 'product', isset($query['l']) ? $query['l'] : '');
            }
            if ($catid) {
                $segments = array_merge(OpenShopHelper::getCategoryPath($catid, 'alias', isset($query['l']) ? $query['l'] : '', $parentId), $segments);
            }
            break;
        case 'brand':
            if ($id) {
                $segments[] = OpenShopHelper::getElementAlias($id, 'brand', isset($query['l']) ? $query['l'] : '');
            }
            break;
        case 'banner':
            if ($id) {
                $segments = array_merge($segments, OpenShopHelper::getBannerPath($id, 'alias', isset($query['l']) ? $query['l'] : '', $parentId));
            }
            break;
        case 'checkout':
            $layout = isset($query['layout']) ? $query['layout'] : '';
            if (!isset($query['Itemid']) || (isset($query['Itemid']) && $query['Itemid'] == OpenShopRoute::getDefaultItemId(isset($query['l']) ? $query['l'] : ''))) {
                if (isset($query['l'])) {
                    $language = JLanguage::getInstance($query['l'], 0);
                    $language->load('com_openshop', JPATH_ROOT, $query['l']);
                    $segments[] = $language->_('OPENSHOP_CHECKOUT');
                } else {
                    $segments[] = JText::_('OPENSHOP_CHECKOUT');
                }
            }
            if (isset($query['l'])) {
                $language = JLanguage::getInstance($query['l'], 0);
                $language->load('com_openshop', JPATH_ROOT, $query['l']);
                switch ($layout) {
                    case 'cancel':
                        $segments[] = $language->_('OPENSHOP_CHECKOUT_CANCEL');
                        break;
                    case 'complete':
                        $segments[] = $language->_('OPENSHOP_CHECKOUT_COMPLETE');
                        break;
                    case 'failure':
                        $segments[] = $language->_('OPENSHOP_CHECKOUT_FAILURE');
                        break;
                    default:
                        break;
                }
            } else {
                switch ($layout) {
                    case 'cancel':
                        $segments[] = JText::_('OPENSHOP_CHECKOUT_CANCEL');
                        break;
                    case 'complete':
                        $segments[] = JText::_('OPENSHOP_CHECKOUT_COMPLETE');
                        break;
                    case 'failure':
                        $segments[] = JText::_('OPENSHOP_CHECKOUT_FAILURE');
                        break;
                    default:
                        break;
                }
            }
            break;
        case 'cart':
            if (!isset($query['Itemid']) || (isset($query['Itemid']) && $query['Itemid'] == OpenShopRoute::getDefaultItemId(isset($query['l']) ? $query['l'] : ''))) {
                if (isset($query['l'])) {
                    $language = JLanguage::getInstance($query['l'], 0);
                    $language->load('com_openshop', JPATH_ROOT, $query['l']);
                    $segments[] = $language->_('OPENSHOP_SHOPPING_CART');
                } else {
                    $segments[] = JText::_('OPENSHOP_SHOPPING_CART');
                }
            }
            break;
        case 'quote':
            $layout = isset($query['layout']) ? $query['layout'] : '';
            if (!isset($query['Itemid']) || (isset($query['Itemid']) && $query['Itemid'] == OpenShopRoute::getDefaultItemId(isset($query['l']) ? $query['l'] : ''))) {
                if (isset($query['l'])) {
                    $language = JLanguage::getInstance($query['l'], 0);
                    $language->load('com_openshop', JPATH_ROOT, $query['l']);
                    $segments[] = $language->_('OPENSHOP_QUOTE_CART');
                } else {
                    $segments[] = JText::_('OPENSHOP_QUOTE_CART');
                }
            }
            if (isset($query['l'])) {
                $language = JLanguage::getInstance($query['l'], 0);
                $language->load('com_openshop', JPATH_ROOT, $query['l']);
                switch ($layout) {
                    case 'complete':
                        $segments[] = $language->_('OPENSHOP_QUOTE_COMPLETE');
                        break;
                    default:
                        break;
                }
            } else {
                switch ($layout) {
                    case 'complete':
                        $segments[] = JText::_('OPENSHOP_QUOTE_COMPLETE');
                        break;
                    default:
                        break;
                }
            }
            break;
        case 'wishlist':
            if (!isset($query['Itemid']) || (isset($query['Itemid']) && $query['Itemid'] == OpenShopRoute::getDefaultItemId(isset($query['l']) ? $query['l'] : ''))) {
                if (isset($query['l'])) {
                    $language = JLanguage::getInstance($query['l'], 0);
                    $language->load('com_openshop', JPATH_ROOT, $query['l']);
                    $segments[] = $language->_('OPENSHOP_WISHLIST');
                } else {
                    $segments[] = JText::_('OPENSHOP_WISHLIST');
                }
            }
            break;
        case 'compare':
            if (!isset($query['Itemid']) || (isset($query['Itemid']) && $query['Itemid'] == OpenShopRoute::getDefaultItemId(isset($query['l']) ? $query['l'] : ''))) {
                if (isset($query['l'])) {
                    $language = JLanguage::getInstance($query['l'], 0);
                    $language->load('com_openshop', JPATH_ROOT, $query['l']);
                    $segments[] = $language->_('OPENSHOP_COMPARE');
                } else {
                    $segments[] = JText::_('OPENSHOP_COMPARE');
                }
            }
            break;
        case 'customer':
            $layout = isset($query['layout']) ? $query['layout'] : '';
            if (!isset($query['Itemid']) || (isset($query['Itemid']) && $query['Itemid'] == OpenShopRoute::getDefaultItemId(isset($query['l']) ? $query['l'] : ''))) {
                if (isset($query['l'])) {
                    $language = JLanguage::getInstance($query['l'], 0);
                    $language->load('com_openshop', JPATH_ROOT, $query['l']);
                    $segments[] = $language->_('OPENSHOP_CUSTOMER');
                } else {
                    $segments[] = JText::_('OPENSHOP_CUSTOMER');
                }
            }
            if (isset($query['l'])) {
                $language = JLanguage::getInstance($query['l'], 0);
                $language->load('com_openshop', JPATH_ROOT, $query['l']);
                switch ($layout) {
                    case 'account':
                        $segments[] = $language->_('OPENSHOP_EDIT_ACCOUNT');
                        break;
                    case 'orders':
                        $segments[] = $language->_('OPENSHOP_ORDER_HISTORY');
                        break;
                    case 'downloads':
                        $segments[] = $language->_('OPENSHOP_DOWNLOADS');
                        break;
                    case 'addresses':
                        $segments[] = $language->_('OPENSHOP_ADDRESSES');
                        break;
                    case 'order':
                        $segments[] = $language->_('OPENSHOP_ORDER_DETAILS');
                        break;
                    case 'address':
                        $segments[] = $language->_('OPENSHOP_ADDRESS_EDIT');
                        break;
                    default:
                        break;
                }
            } else {
                switch ($layout) {
                    case 'account':
                        $segments[] = JText::_('OPENSHOP_EDIT_ACCOUNT');
                        break;
                    case 'orders':
                        $segments[] = JText::_('OPENSHOP_ORDER_HISTORY');
                        break;
                    case 'downloads':
                        $segments[] = JText::_('OPENSHOP_DOWNLOADS');
                        break;
                    case 'addresses':
                        $segments[] = JText::_('OPENSHOP_ADDRESSES');
                        break;
                    case 'order':
                        $segments[] = JText::_('OPENSHOP_ORDER_DETAILS');
                        break;
                    case 'address':
                        $segments[] = JText::_('OPENSHOP_ADDRESS_EDIT');
                        break;
                    default:
                        break;
                }
            }
            break;
    }

    if (isset($query['start']) || isset($query['limitstart'])) {
        $limit = $app->getUserState('limit');
        if (!$limit)
            $limit = OpenShopHelper::getConfigValue('catalog_limit');
        $limitStart = isset($query['limitstart']) ? (int) $query['limitstart'] : (int) $query['start'];
        $page = ceil(($limitStart + 1) / $limit);
        $segments[] = JText::_('OPENSHOP_PAGE') . '-' . $page . '-' . $limit;
    }

    if (isset($query['task']) && $query['task'] == 'customer.downloadInvoice')
        $segments[] = JText::_('OPENSHOP_DOWNLOAD_INVOICE');
    if (isset($query['task']) && $query['task'] == 'cart.reOrder')
        $segments[] = JText::_('OPENSHOP_RE_ORDER');
    if (isset($query['task']) && $query['task'] == 'customer.downloadFile')
        $segments[] = JText::_('OPENSHOP_DOWNLOAD');
    if (isset($query['task']) && $query['task'] == 'search')
        $segments[] = JText::_('OPENSHOP_SEARCH');


    if (isset($query['task']))
        unset($query['task']);

    if (isset($query['view']))
        unset($query['view']);

    if (isset($query['id']))
        unset($query['id']);

    if (isset($query['catid']))
        unset($query['catid']);

    if (isset($query['key']))
        unset($query['key']);

    if (isset($query['redirect']))
        unset($query['redirect']);

    if (isset($query['start']))
        unset($query['start']);

    if (isset($query['limitstart']))
        unset($query['limitstart']);

    if (isset($query['l']))
        unset($query['l']);

    if (isset($query['layout']))
        unset($query['layout']);

    if (count($segments)) {
        $segments = array_map('JApplication::stringURLSafe', $segments);
        $key = md5(implode('/', $segments));
        $q = $db->getQuery(true);
        $q->select('id')
                ->from('#__openshop_urls')
                ->where('md5_key="' . $key . '"');
        $db->setQuery($q);
        $urlId = $db->loadResult();
        if (!$urlId) {
            $q->clear();
            $q->insert('#__openshop_urls')
                    ->columns('md5_key, `query`')
                    ->values("'$key', '$queryString'");
            $db->setQuery($q);
            $db->query();
        } else {
            $q->clear();
            $q->update('#__openshop_urls')
                    ->set('query="' . $queryString . '"')
                    ->where('md5_key="' . $key . '"');
            $db->setQuery($q);
            $db->query();
        }
    }

    return $segments;
}

/**
 * 
 * Parse the segments of a URL.
 * @param	array	The segments of the URL to parse.
 * @return	array	The URL attributes to be used by the application.
 * @since	1.5
 */
function OpenShopParseRoute($segments) {
    $vars = array();
    if (count($segments)) {
        $db = JFactory::getDbo();
        $key = md5(str_replace(':', '-', implode('/', $segments)));
        $query = $db->getQuery(true);
        $query->select('`query`')
                ->from('#__openshop_urls')
                ->where('md5_key = "' . $key . '"');
        $db->setQuery($query);
        $queryString = $db->loadResult();
        if ($queryString)
            parse_str($queryString, $vars);
    }

    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    if ($item = $menu->getActive()) {
        foreach ($item->query as $key => $value) {
            if ($key != 'option' && $key != 'Itemid' && !isset($vars[$key]))
                $vars[$key] = $value;
        }
    }
    return $vars;
}
