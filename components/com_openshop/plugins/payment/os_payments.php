<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class os_payments
{
    public static $methods;
	/**
	 * Get list of payment methods
	 *
	 * @return array
	 */
	public static function getPaymentMethods()
	{
		if (self::$methods == null)
		{
			$session = JFactory::getSession();
			$user = JFactory::getUser();
			if ($user->get('id') && $session->get('payment_address_id'))
			{
				$paymentAddress = OpenShopHelper::getAddress($session->get('payment_address_id'));
			}
			else
			{
				$guest = $session->get('guest');
				$paymentAddress = isset($guest['payment']) ? $guest['payment'] : '';
			}
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')
				->from('#__openshop_payments')
				->where('published = 1')
                ->order('ordering');
			$db->setQuery($query);
			$rows = $db->loadObjectList();
			foreach ($rows as $row)
			{
				if (file_exists(JPATH_ROOT . '/components/com_openshop/plugins/payment/' . $row->name . '.php'))
				{
                    require_once JPATH_ROOT . '/components/com_openshop/plugins/payment/' . $row->name . '.php';
                    $params = new JRegistry($row->params);
                    $status = true;
                    if ($params->get('geozone_id', '0'))
                    {
                    	$query->clear();
                    	$query->select('COUNT(*)')
                    		->from('#__openshop_geozonezones')
                    		->where('geozone_id = ' . intval($params->get('geozone_id')))
                    		->where('country_id = ' . intval($paymentAddress['country_id']))
                    		->where('(zone_id = 0 OR zone_id = ' . intval($paymentAddress['zone_id']) . ')');
                    	$db->setQuery($query);
                    	if (!$db->loadResult())
                    	{
                    		$status = false;
                    	}
                    }
                    if ($status)
                    {
                    	$method = new $row->name($params);
                    	$method->title = JText::_($row->title);
                    	self::$methods[] = $method;
                    }
				}
			}
		}

		return self::$methods;
	}

	/**
	 * Load information about the payment method
	 *
	 * @param string $name
	 * Name of the payment method
	 */
	public static function loadPaymentMethod($name)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__openshop_payments')
			->where('name = "' . $name . '"');
		$db->setQuery($query);
		return $db->loadObject();
	}

	/**
	 * Get default payment gateway
	 *
	 * @return string
	 */
	public static function getDefautPaymentMethod()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('name')
			->from('#__openshop_payments')
			->where('published = 1')
			->order('ordering');
		$db->setQuery($query, 0, 1);
		return $db->loadResult();
	}

	/**
	 * Get the payment method object based on it's name
	 *
	 * @param string $name        	
	 * @return object
	 */
	public static function getPaymentMethod($name)
	{
		$methods = self::getPaymentMethods();
		foreach ($methods as $method)
		{
			if ($method->getName() == $name)
			{
				return $method;
			}
		}
		return null;
	}
}
?>