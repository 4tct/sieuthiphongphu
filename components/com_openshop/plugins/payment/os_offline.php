<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class os_offline extends os_payment
{
	/**
	 * Constructor functions, init some parameter
	 *
	 * @param object $params
	 */
	public function __construct($params)
	{
        $config = array(
            'type' => 0,
            'show_card_type' => false,
            'show_card_holder_name' => false
        );

        parent::__construct($params, $config);
	}
	
	/**
	 * Process payment 
	 *
	 */
	public function processPayment($data)
	{
		$row = JTable::getInstance('OpenShop', 'Order');
		$id = $data['order_id'];
		$row->load($id);
		OpenShopHelper::completeOrder($row);
		//Send confirmation email here
		if (OpenShopHelper::getConfigValue('order_alert_mail'))
		{
			OpenShopHelper::sendEmails($row);
		}
        JFactory::getApplication()->redirect(JRoute::_(OpenShopRoute::getViewRoute('checkout').'&layout=complete'));
	}
}