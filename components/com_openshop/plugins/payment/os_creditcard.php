<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class os_creditcard extends os_payment {	
    /**
     * Constructor function
     *
     * @param object $config
     */    
    function __construct($params)
    {
    	$config = array(
    		'type' => 1,
    		'show_card_type' => false,
    		'show_card_holder_name' => false
    	);
    	parent::__construct($params, $config);
    }
    /**
     * Process payment with the posted data
     *
     * @param array $data array
     * @return void
     */
    function processPayment($data)
    {    	    	
    	$row = JTable::getInstance('OpenShop', 'Order');    	
    	$row->load($data['order_id']);    	
    	$expDate = str_pad($data['exp_month'], 2, '0', STR_PAD_LEFT) .'/'.substr($data['exp_year'], 2, 2) ;
    	$cvv = $data['cvv_code'];
    	$params = new JRegistry($row->params);
    	$params->set('cvv', $cvv);
    	$params->set('exp_date', $expDate);
    	$params->set('card_number', substr($data['card_number'], 0, strlen($data['card_number']) - 4));
    	$row->params = $params->toString();
    	$row->store();
    	OpenShopHelper::completeOrder($row);
    	JPluginHelper::importPlugin('openshop');
    	$dispatcher = JDispatcher::getInstance();
    	$dispatcher->trigger('onAfterCompleteOrder', array($row));
    	//Send confirmation email here
    	if (OpenShopHelper::getConfigValue('order_alert_mail'))
    	{
    		OpenShopHelper::sendEmails($row);
    	}
    	JFactory::getApplication()->redirect(JRoute::_(OpenShopRoute::getViewRoute('checkout').'&layout=complete'));
    }    
}