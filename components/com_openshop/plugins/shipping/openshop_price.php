<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class openshop_price extends openshop_shipping
{

	/**
	 *
	 * Constructor function
	 */
	function openshop_price()
	{
		parent::setName('openshop_price');
		parent::openshop_shipping();
	}
	
	/**
	 * 
	 * Function tet get quote for price shipping
	 * @param array $addressData
	 * @param object $params
	 */
	function getQuote($addressData, $params)
	{
		//Check geozone condition
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		if (!$params->get('geozone_id'))
		{
			$status = true;
		}
		else
		{
			$query->select('COUNT(*)')
				->from('#__openshop_geozonezones')
				->where('geozone_id = ' . intval($params->get('geozone_id')))
				->where('country_id = ' . intval($addressData['country_id']))
				->where('(zone_id = 0 OR zone_id = ' . intval($addressData['zone_id']) . ')');
			$db->setQuery($query);
			if ($db->loadResult())
			{
				$status = true;
			}
			else
			{
				$status = false;
			}
		}
		//Check min sub total for free condition
		$cart = new OpenShopCart();
		$subTotal = $cart->getSubTotal(1);
		$minTotalForFree = $params->get('min_sub_total_for_free', 0);
		if ($minTotalForFree > 0 && $minTotalForFree <= $subTotal)
		{
			$status = false;
		}
		//Check input data condition
		$subTotalRange = $params->get('sub_total_range');
		$subTotalRangeArr = explode(';', $subTotalRange);
		$costRange = $params->get('cost_range');
		$costRangeArr = explode(';', $costRange);
		if (!count($subTotalRangeArr) || !count($costRangeArr))
		{
			$status = false;
		}
		$methodData = array();
		if ($status)
		{
			$tax = new OpenShopTax(OpenShopHelper::getConfig());
			$currency = new OpenShopCurrency();
			$quoteData = array();
			$packageFee = $params->get('package_fee', 0);
			$cost = 0;
			for ($i = 0; $n = count($subTotalRangeArr), $i < ($n - 1); $i++)
			{
				if ($subTotal >= $subTotalRangeArr[$i] && $subTotal <= $subTotalRangeArr[$i + 1])
				{
					if (strpos($costRangeArr[$i], '%'))
					{
						$percentageCost = str_replace('%', '', $costRangeArr[$i]);
						$cost = round($subTotal * $percentageCost / 100, 2);
					}
					else
					{
						$cost = $costRangeArr[$i];
					}
					break;
				}
				else
				{
					continue;
				}
			}
			if ($i == ($n - 1))
			{
				if (strpos($costRangeArr[$i], '%'))
				{
					$percentageCost = str_replace('%', '', $costRangeArr[$i]);
					$cost = round($subTotal * $percentageCost / 100, 2);
				}
				else
				{
					$cost = $costRangeArr[$i];
				}
			}
			$cost = $cost + $packageFee;
			
			$query->clear();
			$query->select('*')
				->from('#__openshop_shippings')
				->where('name = "openshop_price"');
			$db->setQuery($query);
			$row = $db->loadObject();
			$quoteData['price'] = array(
				'name'			=> 'openshop_price.price', 
				'title'			=> JText::_('PLG_OPENSHOP_PRICE_DESC'), 
				'cost'			=> $cost, 
				'taxclass_id' 	=> $params->get('taxclass_id'), 
				'text'			=> $currency->format($tax->calculate($cost, $params->get('taxclass_id'), OpenShopHelper::getConfigValue('tax'))));
			
			$methodData = array(
				'name'		=> 'openshop_price',
				'title'		=> JText::_('PLG_OPENSHOP_PRICE_TITLE'),
				'quote'		=> $quoteData,
				'ordering'	=> $row->ordering,
				'error'		=> false);
		}
		return $methodData;
	}
}