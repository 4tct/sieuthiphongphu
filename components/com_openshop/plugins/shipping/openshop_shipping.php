<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class openshop_shipping
{

	/**
	 * Name of shipping plugin
	 *
	 * @var string
	 */
	var $_name = null;
	
	/**
	 * Method to get shipping name
	 *
	 * @return string
	 */
	function getName()
	{
		return $this->_name;
	}
	
	/**
	 * Method to set shipping name
	 *
	 * @param string $value
	 */
	function setName($value)
	{
		$this->_name = $value;
	}
	
	/**
	 * 
	 * Constructor function
	 */
	function openshop_shipping()
	{
		$this->loadLanguage();
	}
	
	/**
	 * Load language file for this payment plugin
	 */
	function loadLanguage()
	{
		$pluginName = $this->getName();
		$lang = JFactory::getLanguage();
		$tag = $lang->getTag();
		if (!$tag)
			$tag = 'en-GB';
		$lang->load('plg_'.$pluginName, JPATH_ROOT, $tag);
	}
}
?>