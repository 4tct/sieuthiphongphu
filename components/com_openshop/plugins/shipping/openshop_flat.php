<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class openshop_flat extends openshop_shipping
{
	
	/**
	 * 
	 * Constructor function
	 */
	function openshop_flat()
	{
		parent::setName('openshop_flat');
		parent::openshop_shipping();
	}

	/**
	 * 
	 * Function tet get quote for flat shipping
	 * @param array $addressData
	 * @param object $params
	 */
	function getQuote($addressData, $params)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		if (!$params->get('geozone_id'))
		{
			$status = true;
		}
		else
		{
			$query->select('COUNT(*)')
				->from('#__openshop_geozonezones')
				->where('geozone_id = ' . intval($params->get('geozone_id')))
				->where('country_id = ' . intval($addressData['country_id']))
				->where('(zone_id = 0 OR zone_id = ' . intval($addressData['zone_id']) . ')');
			$db->setQuery($query);
			if ($db->loadResult())
			{
				$status = true;
			}
			else
			{
				$status = false;
			}
		}
		$methodData = array();
		if ($status)
		{
			$tax = new OpenShopTax(OpenShopHelper::getConfig());
			$currency = new OpenShopCurrency();
			$quoteData = array();
			$query->clear();
			$query->select('*')
				->from('#__openshop_shippings')
				->where('name = "openshop_flat"');
			$db->setQuery($query);
			$row = $db->loadObject();
			$cost = $params->get('cost');
			$packageFee = $params->get('package_fee', 0);
			$cost = $cost + $packageFee;
			$quoteData['flat'] = array (
				'name'			=> 'openshop_flat.flat', 
				'title'			=> JText::_('PLG_OPENSHOP_FLAT_DESC'), 
				'cost'			=> $cost, 
				'taxclass_id' 	=> $params->get('taxclass_id'), 
				'text'			=> $currency->format($tax->calculate($cost, $params->get('taxclass_id'), OpenShopHelper::getConfigValue('tax'))));
			
			$methodData = array (
				'name'		=> 'openshop_flat',
				'title'		=> JText::_('PLG_OPENSHOP_FLAT_TITLE'),
				'quote'		=> $quoteData,
				'ordering'	=> $row->ordering,
				'error'		=> false);
		}
		return $methodData;
	}
}