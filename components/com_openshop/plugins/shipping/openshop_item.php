<?php
/**
 * @version		1.4.2
 * @package		Joomla
 * @subpackage	EShop - Item Shipping
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die();

class openshop_item extends openshop_shipping
{

	/**
	 *
	 * Constructor function
	 */
	function openshop_item()
	{
		parent::setName('openshop_item');
		parent::openshop_shipping();
	}
	
	/**
	 * 
	 * Function tet get quote for item shipping
	 * @param array $addressData
	 * @param object $params
	 */
	function getQuote($addressData, $params)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		if (!$params->get('geozone_id'))
		{
			$status = true;
		}
		else
		{
			$query->select('COUNT(*)')
				->from('#__openshop_geozonezones')
				->where('geozone_id = ' . intval($params->get('geozone_id')))
				->where('country_id = ' . intval($addressData['country_id']))
				->where('(zone_id = 0 OR zone_id = ' . intval($addressData['zone_id']) . ')');
			$db->setQuery($query);
			if ($db->loadResult())
			{
				$status = true;
			}
			else
			{
				$status = false;
			}
		}
		$methodData = array();
		if ($status)
		{
			$cart = new OpenShopCart();
			$packageFee = $params->get('package_fee', 0);
			$cost = 0;
			foreach ($cart->getCartData() as $product)
			{
				if ($product['product_shipping'])
				{
					$cost += $product['product_shipping_cost'] * $product['quantity'];
				}
			}
			$cost = $cost + $packageFee;
			$tax = new OpenShopTax(OpenShopHelper::getConfig());
			$currency = new OpenShopCurrency();
			$quoteData = array();
			$query->clear();
			$query->select('*')
				->from('#__openshop_shippings')
				->where('name = "openshop_item"');
			$db->setQuery($query);
			$row = $db->loadObject();
			$quoteData['item'] = array (
				'name'			=> 'openshop_item.item', 
				'title'			=> JText::_('PLG_OPENSHOP_ITEM_DESC'), 
				'cost'			=> $cost, 
				'taxclass_id' 	=> $params->get('taxclass_id'), 
				'text'			=> $currency->format($tax->calculate($cost, $params->get('taxclass_id'), OpenShopHelper::getConfigValue('tax'))));
			
			$methodData = array (
				'name'		=> 'openshop_item',
				'title'		=> JText::_('PLG_OPENSHOP_ITEM_TITLE'),
				'quote'		=> $quoteData,
				'ordering'	=> $row->ordering,
				'error'		=> false);
		}
		return $methodData;
	}
}