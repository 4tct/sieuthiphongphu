<?php
/**
 * @version		1.4.2
 * @package		Joomla
 * @subpackage	EShop - Free Shipping
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die();

class openshop_free extends openshop_shipping
{
	
	/**
	 *
	 * Constructor function
	 */
	function openshop_free()
	{
		parent::setName('openshop_free');
		parent::openshop_shipping();
	}

	/**
	 * 
	 * Function tet get quote for free shipping
	 * @param array $addressData
	 * @param object $params
	 */
	function getQuote($addressData, $params)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		if (!$params->get('geozone_id'))
		{
			$status = true;
		}
		else
		{
			$query->select('COUNT(*)')
				->from('#__openshop_geozonezones')
				->where('geozone_id = ' . intval($params->get('geozone_id')))
				->where('country_id = ' . intval($addressData['country_id']))
				->where('(zone_id = 0 OR zone_id = ' . intval($addressData['zone_id']) . ')');
			$db->setQuery($query);
			if ($db->loadResult())
			{
				$status = true;
			}
			else
			{
				$status = false;
			}
		}
		//Check customer groups
		$customerGroups = $params->get('customer_groups');
		if (count($customerGroups))
		{
			$user = JFactory::getUser();
			if ($user->get('id'))
			{
				$customer = new OpenShopCustomer();
				$customerGroupId = $customer->getCustomerGroupId();
			}
			else
			{
				$customerGroupId = OpenShopHelper::getConfigValue('customergroup_id');
			}
			if (!$customerGroupId)
				$customerGroupId = 0;
			if (!in_array($customerGroupId, $customerGroups))
			{
				$status = false;
			}
		}
		//Check min total
		$cart = new OpenShopCart();
		$total = $cart->getTotal();
		$minTotal = $params->get('min_total', 0);
		if ($total < $minTotal)
		{
			$status = false;
		}
		$methodData = array();
		if ($status)
		{
			$currency = new OpenShopCurrency();
			$quoteData = array();
			$query->clear();
			$query->select('*')
				->from('#__openshop_shippings')
				->where('name = "openshop_free"');
			$db->setQuery($query);
			$row = $db->loadObject();
			$quoteData['free'] = array (
				'name'			=> 'openshop_free.free', 
				'title'			=> JText::_('PLG_OPENSHOP_FREE_DESC'), 
				'cost'			=> 0.00, 
				'taxclass_id' 	=> $params->get('taxclass_id'),
				'text'			=> $currency->format(0.00));
			
			$methodData = array (
				'name'		=> 'openshop_free',
				'title'		=> JText::_('PLG_OPENSHOP_FREE_TITLE'),
				'quote'		=> $quoteData,
				'ordering'	=> $row->ordering,
				'error'		=> false);
		}
		return $methodData;
	}
}