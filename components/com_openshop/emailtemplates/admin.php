<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
        <tr>
            <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
                <?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
            </td>
            <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
                <?php echo JText::_('OPENSHOP_MODEL'); ?>
            </td>
            <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">
                <?php echo JText::_('OPENSHOP_QUANTITY'); ?>
            </td>
            <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">
                <?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?>
            </td>
            <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">
                <?php echo JText::_('OPENSHOP_TOTAL'); ?>
            </td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($this->orderProducts as $product) {
            ?>
            <tr>
                <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
    <?php echo $product->product_name; ?>
                    <?php
                    foreach ($product->orderOptions as $option) {
                        ?>
                        <br />
                        &nbsp;<small> - <?php echo $option->option_name; ?>: <?php echo $option->option_value . (isset($option->sku) && $option->sku != '' ? ' (' . $option->sku . ')' : ''); ?></small>
                        <?php
                    }
                    ?>
                </td>
                <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
                    <?php echo $product->product_sku; ?>
                </td>
                <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
                    <?php echo $product->quantity; ?>
                </td>
                <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
                    <?php echo $product->price; ?>
                </td>
                <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
                    <?php echo $product->total_price; ?>
                </td>
            </tr>
                    <?php
                }
                ?>
    </tbody>
    <tfoot>
        <?php
        foreach ($this->orderTotals as $total) {
            ?>
            <tr>
                <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="4">
                    <b><?php echo $total->title; ?></b>
                </td>
                <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
    <?php echo $total->text; ?>
                </td>
            </tr>
                    <?php
                }
                ?>
    </tfoot>
</table>