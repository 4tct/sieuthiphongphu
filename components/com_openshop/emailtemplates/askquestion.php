<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

?>
<div style="width: 680px;">
	<p style="margin-top: 0px; margin-bottom: 20px;">You have received a question about a product.</p>
	<table style="width: 100%; margin-bottom: 20px;">
		<tr>
			<td width="30%" style="font-size: 12px; text-align: right; padding: 7px;" valign="top"><strong><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>:</strong></td>
			<td width="70%"><?php echo $this->product->product_name; ?></td>
		</tr>
		<tr>		
			<td width="30%" style="font-size: 12px; text-align: right; padding: 7px;" valign="top"><strong><?php echo JText::_('OPENSHOP_MODEL'); ?>:</strong></td>
			<td width="70%"><?php echo $this->product->product_sku; ?></td>
		</tr>
		<tr>
			<td width="30%" style="font-size: 12px; text-align: right; padding: 7px;" valign="top"><strong><?php echo JText::_('OPENSHOP_NAME'); ?>:</strong></td>
			<td width="70%"><?php echo $this->data['name']; ?></td>
		</tr>
		<tr>
			<td width="30%" style="font-size: 12px; text-align: right; padding: 7px;" valign="top"><strong><?php echo JText::_('OPENSHOP_COMPANY'); ?>:</strong></td>
			<td width="70%"><?php echo $this->data['company']; ?></td>
		</tr>
		<tr>
			<td width="30%" style="font-size: 12px; text-align: right; padding: 7px;" valign="top"><strong><?php echo JText::_('OPENSHOP_EMAIL'); ?>:</strong></td>
			<td width="70%"><?php echo $this->data['email']; ?></td>
		</tr>
		<tr>
			<td width="30%" style="font-size: 12px; text-align: right; padding: 7px;" valign="top"><strong><?php echo JText::_('OPENSHOP_PHONE'); ?>:</strong></td>
			<td width="70%"><?php echo $this->data['phone']; ?></td>
		</tr>
		<tr>
			<td width="30%" style="font-size: 12px; text-align: right; padding: 7px;" valign="top"><strong><?php echo JText::_('OPENSHOP_MESSAGE'); ?>:</strong></td>
			<td width="70%"><?php echo nl2br($this->data['message']); ?></td>
		</tr>
	</table>
</div>