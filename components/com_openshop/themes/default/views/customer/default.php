<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
if (isset($this->success))
{
	?>
	<div class="success"><?php echo $this->success; ?></div>
	<?php
}
?>
<h1><?php echo JText::_('OPENSHOP_MY_ACCOUNT'); ?></h1>
<div class="row-fluid">
	<ul>
		<li>
			<a href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer').'&layout=account'); ?>">
				<?php echo JText::_('OPENSHOP_EDIT_ACCOUNT'); ?>
			</a>
		</li>
		<li>
			<a href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer').'&layout=orders'); ?>">
				<?php echo JText::_('OPENSHOP_ORDER_HISTORY'); ?>
			</a>
		</li>
		<li>
			<a href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer').'&layout=downloads'); ?>">
				<?php echo JText::_('OPENSHOP_DOWNLOADS'); ?>
			</a>
		</li>
		<li>
			<a href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer').'&layout=addresses'); ?>">
				<?php echo JText::_('OPENSHOP_MODIFY_ADDRESS'); ?>
			</a>
		</li>
	</ul>
</div>