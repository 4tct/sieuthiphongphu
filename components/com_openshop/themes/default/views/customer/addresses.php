<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

$language = JFactory::getLanguage();
$tag = $language->getTag();
if (!$tag)
	$tag = 'en-GB';
if (isset($this->success))
{
	?>
	<div class="success"><?php echo $this->success; ?></div>
	<?php
}
if (isset($this->warning))
{
	?>
	<div class="warning"><?php echo $this->warning; ?></div>
	<?php
}
?>
<h1><?php echo JText::_('OPENSHOP_ADDRESS_HISTORY'); ?></h1>
<?php
if (!count($this->addresses))
{
	?>
	<div class="no-content"><?php echo JText::_('OPENSHOP_NO_ADDRESS'); ?></div>
	<?php
}
else
{
	?>
	<div class="row-fluid">
		<form id="adminForm" class="order-list">
			<?php
			foreach ($this->addresses as $address)
			{
				?>
				<div class="content">
					<table class="list">
						<tr>
							<td class="left" width="80%">
								<?php
								$addressText = $address->firstname;
								if (OpenShopHelper::isFieldPublished('lastname') && $address->lastname != '')
								{
									$addressText .= " " . $address->lastname;
								}
								$addressText .= "<br />" . $address->address_1;
								if (OpenShopHelper::isFieldPublished('address_2') && $address->address_2 != '')
								{
									$addressText .= ", " . $address->address_2;
								}
								if (OpenShopHelper::isFieldPublished('city') && $address->city != '')
								{
									$addressText .= "<br />" . $address->city;
								}
								if (OpenShopHelper::isFieldPublished('postcode') && $address->postcode != '')
								{
									$addressText .= ", " . $address->postcode;
								}
								$addressText .= "<br />" . $address->email;
								if (OpenShopHelper::isFieldPublished('zone_id') && $address->zone_name != '')
								{
									$addressText .= "<br />" . $address->zone_name;
								}
								if (OpenShopHelper::isFieldPublished('country_id') && $address->country_name != '')
								{
									$addressText .= "<br />" . $address->country_name;
								}
								if (OpenShopHelper::isFieldPublished('telephone') && $address->telephone != '')
								{
									$addressText .= "<br />" . $address->telephone;
								}
								if (OpenShopHelper::isFieldPublished('fax') && $address->fax != '')
								{
									$addressText .= "<br />" . $address->fax;
								}
								if (OpenShopHelper::isFieldPublished('company_id') && $address->company_id != '')
								{
									$addressText .= "<br />" . $address->company_id;
								}
								if (OpenShopHelper::isFieldPublished('company') && $address->company != '')
								{
									$addressText .= "<br />" . $address->company;
								}
								echo $addressText;
								?>
							</td>
							<td class="right" width="20%">
								<input type="button" value="<?php echo JText::_('OPENSHOP_EDIT'); ?>" id="button-edit-address" class="btn btn-primary" onclick="window.location.assign('<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer').'&layout=address&aid='.$address->id); ?>');" />&nbsp; 
								<input type="button" value="<?php echo JText::_('OPENSHOP_DELETE'); ?>" id="<?php echo $address->id; ?>" class="button-delete-address btn btn-primary pull-right" />
							</td>
						</tr>
					</table>
				</div>	
				<?php
			}
			?>
		</form>
	</div>
	<?php
}
?>
<div class="row-fluid">
	<input type="button" value="<?php echo JText::_('OPENSHOP_BACK'); ?>" id="button-back-address" class="btn btn-primary pull-left" />
	<input type="button" value="<?php echo JText::_('OPENSHOP_ADD_ADDRESS'); ?>" id="button-new-address" class="btn btn-primary pull-right" />
</div>
<script type="text/javascript">
	OpenShop.jQuery(function($){
		$(document).ready(function(){
			$('#button-back-address').click(function() {
				var url = '<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer')); ?>';
				$(location).attr('href', url);
			});

			$('#button-new-address').click(function() {
				var url = '<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer').'&layout=address'); ?>';
				$(location).attr('href', url);
			});

			//process user
			$('.button-delete-address').live('click', function() {
				var id = $(this).attr('id');
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
				$.ajax({
					url: siteUrl + 'index.php?option=com_openshop&task=customer.deleteAddress<?php echo OpenShopHelper::getAttachedLangLink(); ?>&aid=' + id,
					type: 'post',
					data: $("#adminForm").serialize(),
					dataType: 'json',
					success: function(json) {
							$('.warning, .error').remove();
							if (json['return']) {
								window.location.href = json['return'];
							} else {
							$('.error').remove();
							$('.warning, .error').remove();
							
						}	  
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});	
			});
		})
	});
</script>