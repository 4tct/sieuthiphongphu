<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

$language = JFactory::getLanguage();
$tag = $language->getTag();
if (!$tag)
	$tag = 'en-GB';
if (isset($this->warning))
{
	?>
	<div class="warning"><?php echo $this->warning; ?></div>
	<?php
}
?>
<h1><?php echo JText::_('OPENSHOP_ORDER_HISTORY'); ?></h1><br />
<?php
if (!count($this->orders))
{
	?>
	<div class="no-content"><?php echo JText::_('OPENSHOP_NO_ORDERS'); ?></div>
	<?php
}
else
{
	?>
	<div class="row-fluid">
		<form id="adminForm" class="order-list">
			<?php
			foreach ($this->orders as $order)
			{
				?>
				<div class="order-id"><b><?php echo JText::_('OPENSHOP_ORDER_ID'); ?>: </b>#<?php echo $order->id; ?></div>
				<div class="order-status"><b><?php echo JText::_('OPENSHOP_STATUS'); ?>: </b><?php echo OpenShopHelper::getOrderStatusName($order->order_status_id, $tag); ?></div>
				<div class="order-content">
					<div>
						<b><?php echo JText::_('OPENSHOP_DATE_ADDED'); ?>: </b><?php echo JHtml::date($order->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y')); ?><br />
						<b><?php echo JText::_('OPENSHOP_PRODUCT'); ?>: </b><?php echo OpenShopHelper::getNumberProduct($order->id); ?>
					</div>
					<div>
						<b><?php echo JText::_('OPENSHOP_CUSTOMER'); ?>: </b><?php echo $order->firstname . ' ' . $order->lastname; ?><br />
						<b><?php echo JText::_('OPENSHOP_TOTAL'); ?>: </b> <?php echo $order->total; ?>
					</div>
					<div class="order-info" align="right">
						<a href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer').'&layout=order&order_id='.(int)$order->id); ?>"><?php echo JText::_('OPENSHOP_VIEW'); ?></a>
						<?php
						if (OpenShopHelper::getConfigValue('allow_re_order'))
						{
							?>
							&nbsp;|&nbsp;
							<a href="<?php echo JRoute::_('index.php?option=com_openshop&task=cart.reOrder&order_id='.(int)$order->id); ?>"><?php echo JText::_('OPENSHOP_RE_ORDER'); ?></a>
							<?php
						}
						if (OpenShopHelper::getConfigValue('invoice_enable'))
						{
							?>
							&nbsp;|&nbsp;
							<a href="<?php echo JRoute::_('index.php?option=com_openshop&task=customer.downloadInvoice&order_id='.(int)$order->id); ?>"><?php echo JText::_('OPENSHOP_DOWNLOAD_INVOICE'); ?></a>
							<?php
						}
						?>
					</div>
				</div>
				<?php
			}
			?>
		</form>
	</div>
	<?php
}
?>
<div class="row-fluid">
	<div class="span2">
		<input type="button" value="<?php echo JText::_('OPENSHOP_BACK'); ?>" id="button-back-order" class="btn btn-primary pull-left" />
	</div>
</div>
<script type="text/javascript">
	OpenShop.jQuery(function($){
		$(document).ready(function(){
			$('#button-back-order').click(function() {
				var url = '<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer')); ?>';
				$(location).attr('href', url);
			});
		})
	});
</script>