<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
 
$user = JFactory::getUser();
$language = JFactory::getLanguage();
$tag = $language->getTag();
if (!$tag)
	$tag = 'en-GB';
if(!$this->orderProducts)
{
	?>
	<div class="warning"><?php echo JText::_('OPENSHOP_ORDER_DOES_NOT_EXITS'); ?></div>
	<?php
}
else
{
	$hasShipping = $this->orderInfor->shipping_method;
	?>
	<form id="adminForm">
		<table cellpadding="0" cellspacing="0" class="list">
			<thead>
				<tr>
					<td colspan="2" class="left">
						<?php echo JText::_('OPENSHOP_ORDER_DETAILS'); ?>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="width: 50%;" class="left">
						 <b><?php echo JText::_('OPENSHOP_ORDER_ID'); ?>: </b>#<?php echo $this->orderInfor->id; ?><br />
	         			 <b><?php echo JText::_('OPENSHOP_DATE_ADDED'); ?>: </b> <?php echo JHtml::date($this->orderInfor->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y')); ?>
	         		</td>
					<td style="width: 50%;" class="left">
					    <b><?php echo JText::_('OPENSHOP_PAYMENT_METHOD'); ?>: </b> <?php echo JText::_($this->orderInfor->payment_method_title); ?><br />
					    <b><?php echo JText::_('OPENSHOP_SHIPPING_METHOD'); ?>: </b> <?php echo $this->orderInfor->shipping_method_title; ?><br />
	                </td>
				</tr>
			</tbody>
		</table>
		<table cellpadding="0" cellspacing="0" class="list">
			<thead>
				<tr>
					<td class="left">
						<?php echo JText::_('OPENSHOP_PAYMENT_ADDRESS'); ?>
					</td>
					<?php
					if ($hasShipping)
					{
						?>
						<td class="left">
							<?php echo JText::_('OPENSHOP_SHIPPING_ADDRESS'); ?>
						</td>
						<?php
					}
					?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="left">
						<?php
						echo OpenShopHelper::getPaymentAddress($this->orderInfor);
						$excludedFields = array('firstname', 'lastname', 'email', 'telephone', 'fax', 'company', 'company_id', 'address_1', 'address_2', 'city', 'postcode', 'country_id', 'zone_id');
						foreach ($this->paymentFields as $field)
						{
							$fieldName = $field->name;
							if (!in_array($fieldName, $excludedFields))
							{
								$fieldValue = $this->orderInfor->{'payment_'.$fieldName};
								if (is_string($fieldValue) && is_array(json_decode($fieldValue)))
								{
									$fieldValue = implode(', ', json_decode($fieldValue));
								}
								if ($fieldValue != '')
								{
									echo '<br />' . JText::_($field->title) . ': ' . $fieldValue;
								}
							}
						}
						?>
					</td>
					<?php
					if ($hasShipping)
					{
						?>
						<td class="left">
							<?php
							echo OpenShopHelper::getShippingAddress($this->orderInfor);
							foreach ($this->shippingFields as $field)
							{
								$fieldName = $field->name;
								if (!in_array($fieldName, $excludedFields))
								{
									$fieldValue = $this->orderInfor->{'shipping_'.$fieldName};
									if (is_string($fieldValue) && is_array(json_decode($fieldValue)))
									{
										$fieldValue = implode(', ', json_decode($fieldValue));
									}
									if ($fieldValue != '')
									{
										echo '<br />' . JText::_($field->title) . ': ' . $fieldValue;
									}
								}
							}
							?>
						</td>
						<?php
					}
					?>
				</tr>
			</tbody>
		</table>
		<table cellpadding="0" cellspacing="0" class="list">
			<thead>
				<tr>
					<td class="left">
						<?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?>
					</td>
					<td class="left">
						<?php echo JText::_('OPENSHOP_MODEL'); ?>
					</td>
					<td class="left">
						<?php echo JText::_('OPENSHOP_QUANTITY'); ?>
					</td>
					<td class="left">
						<?php echo JText::_('OPENSHOP_PRICE'); ?>
					</td>
					<td class="left">
						<?php echo JText::_('OPENSHOP_TOTAL'); ?>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($this->orderProducts as $product)
				{
					$options = $product->options;
					?>
					<tr>
						<td class="left">
							<?php
							echo '<b>' . $product->product_name . '</b>';
							for ($i = 0; $n = count($options), $i < $n; $i++)
							{
								echo '<br />- ' . $options[$i]->option_name . ': ' . $options[$i]->option_value . (isset($options[$i]->sku) && $options[$i]->sku != '' ? ' (' . $options[$i]->sku . ')' : '');
							}
							?>
						</td>
						<td class="left"><?php echo $product->product_sku; ?></td>
						<td class="left"><?php echo $product->quantity; ?></td>
						<td class="right"><?php echo $product->price; ?></td>
						<td class="right"><?php echo $product->total_price; ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				<?php
					foreach ($this->orderTotals as $ordertotal)
					{ 
				?>
				<tr>
					<td colspan="3"></td>
					<td class="right">
						<b><?php echo $ordertotal->title?>: </b>
					</td>
					<td class="right">
						<?php echo $ordertotal->text?>
					</td>
				</tr>
				<?php
					} 
				?>
			</tfoot>
		</table>
		
		<h2><?php echo JText::_('OPENSHOP_ORDER_HISTORY'); ?></h2>
		<table cellpadding="0" cellspacing="0" class="list">
			<thead>
				<tr>
					<td class="left">
						<?php echo JText::_('OPENSHOP_DATE_ADDED'); ?>
					</td>
					<td class="left">
						<?php echo JText::_('OPENSHOP_STATUS'); ?>
					</td>
					<td class="left">
						<?php echo JText::_('OPENSHOP_COMMENT'); ?>
					</td>
					<?php
					if (OpenShopHelper::getConfigValue('delivery_date'))
					{
						?>
						<td class="left">
							<?php echo JText::_('OPENSHOP_DELIVERY_DATE'); ?>
						</td>
						<?php
					}
					?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="left">
						<?php echo JHtml::date($this->orderInfor->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y'));?>
					</td>
					<td class="left">
						<?php echo OpenShopHelper::getOrderStatusName($this->orderInfor->order_status_id, $tag); ?>
					</td>
					<td class="left">
						<?php echo nl2br($this->orderInfor->comment); ?>
					</td>
					<?php
					if (OpenShopHelper::getConfigValue('delivery_date'))
					{
						?>
						<td class="left">
							<?php echo JHtml::date($this->orderInfor->delivery_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y')); ?>
						</td>
						<?php
					}
					?>
				</tr>
			</tbody>
		</table>
	</form>
	<div class="no_margin_left">
		<input type="button" value="<?php echo JText::_('OPENSHOP_BACK'); ?>" id="button-user-orderinfor" class="btn btn-primary pull-right">
	</div>
	<?php
}
?>
<script type="text/javascript">
	OpenShop.jQuery(function($){
		$(document).ready(function(){
			$('#button-user-orderinfor').click(function(){
				var url = '<?php echo JRoute::_(OpenShopRoute::getViewRoute('customer') . '&layout=orders'); ?>';
				$(location).attr('href',url);
			});
		})
	});
</script>