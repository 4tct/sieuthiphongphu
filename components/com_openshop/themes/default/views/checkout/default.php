<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<h1><?php echo JText::_('OPENSHOP_CHECKOUT'); ?></h1><br />
<div class="row-fluid">
	<div id="checkout-options">
		<div class="checkout-heading"><?php echo JText::_('OPENSHOP_CHECKOUT_STEP_1'); ?></div>
		<div class="checkout-content"></div>
	</div>
	<div id="payment-address">
		<div class="checkout-heading">
			<?php
			if (OpenShopHelper::getCheckoutType() == 'guest_only')
			{
				echo JText::_('OPENSHOP_CHECKOUT_STEP_2_GUEST');
			}
			else 
			{
				echo JText::_('OPENSHOP_CHECKOUT_STEP_2_REGISTER');
			}
			?>
		</div>
		<div class="checkout-content"></div>
	</div>
	<?php
	if ($this->shipping_required)
	{
		?>
		<div id="shipping-address">
			<div class="checkout-heading"><?php echo JText::_('OPENSHOP_CHECKOUT_STEP_3'); ?></div>
			<div class="checkout-content"></div>
		</div>
		<div id="shipping-method">
			<div class="checkout-heading"><?php echo JText::_('OPENSHOP_CHECKOUT_STEP_4'); ?></div>
			<div class="checkout-content"></div>
		</div>
		<?php
	}
	?>
	<div id="payment-method">
		<div class="checkout-heading"><?php echo JText::_('OPENSHOP_CHECKOUT_STEP_5'); ?></div>
		<div class="checkout-content"></div>
	</div>
	<div id="confirm">
		<div class="checkout-heading"><?php echo JText::_('OPENSHOP_CHECKOUT_STEP_6'); ?></div>
		<div class="checkout-content"></div>
	</div>
</div>
<script type="text/javascript">
	OpenShop.jQuery(function($){
		//Script to allow Edit step
		$('.checkout-heading a').live('click', function() {
			$('.checkout-content').slideUp('slow');
			$(this).parent().parent().find('.checkout-content').slideDown('slow');
		});
		//If user is not logged in, then show login layout
		<?php
		if (!$this->user->get('id'))
		{
			if (OpenShopHelper::getConfigValue('checkout_type') == 'guest_only')
			{
				?>
				$(document).ready(function() {
					var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
					$.ajax({
						url: siteUrl + 'index.php?option=com_openshop&view=checkout&layout=guest&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
						dataType: 'html',
						success: function(html) {
							$('#payment-address .checkout-content').html(html);
							$('#payment-address .checkout-content').slideDown('slow');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				});
				<?php
			}
			else 
			{
				?>
				$(document).ready(function() {
					var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
					$.ajax({
						url: siteUrl + 'index.php?option=com_openshop&view=checkout&layout=login&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
						dataType: 'html',
						success: function(html) {
							$('#checkout-options .checkout-content').html(html);
							$('#checkout-options .checkout-content').slideDown('slow');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				});
				<?php
			}
		}
		//Else, show payment address layout
		else
		{
			?>
			$('#payment-address .checkout-heading').html('<?php echo JText::_('OPENSHOP_CHECKOUT_STEP_2_GUEST'); ?>');
			$(document).ready(function() {
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
				$.ajax({
					url: siteUrl + 'index.php?option=com_openshop&view=checkout&layout=payment_address&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
					dataType: 'html',
					success: function(html) {
						$('#payment-address .checkout-content').html(html);
						$('#payment-address .checkout-content').slideDown('slow');
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			});
			<?php
		}
		?>
	});
</script>