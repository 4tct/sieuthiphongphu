<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<h1><?php echo JText::_('OPENSHOP_PAYMENT_FAILURE_TITLE'); ?></h1>
<p>
	<?php
	$session = JFactory::getSession();
	echo $session->get('openshop_payment_error_reason');
	?>
</p>