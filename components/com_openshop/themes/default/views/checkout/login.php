<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="span6 no_margin_left">
	<?php
	if (OpenShopHelper::getCheckoutType() != 'guest_only')
	{
		?>
		<h4><?php echo JText::_('OPENSHOP_CHECKOUT_NEW_CUSTOMER'); ?></h4>
		<p><?php echo JText::_('OPENSHOP_CHECKOUT_NEW_CUSTOMER_INTRO'); ?></p>
		<label class="radio"><input type="radio" value="register" name="account" checked="checked" /> <?php echo JText::_('OPENSHOP_REGISTER_ACCOUNT'); ?></label>
		<?php
	}
	if (OpenShopHelper::getCheckoutType() != 'registered_only')
	{
		?>
		<label class="radio"><input type="radio" value="guest" name="account" <?php if (OpenShopHelper::getCheckoutType() == 'guest_only') echo 'checked="checked"'; ?> /> <?php echo JText::_('OPENSHOP_GUEST_CHECKOUT'); ?></label>
		<?php
	}
	?>
	<input type="button" class="btn btn-primary pull-left" id="button-account" value="<?php echo JText::_('OPENSHOP_CONTINUE'); ?>" />
</div>
<?php
if (OpenShopHelper::getCheckoutType() != 'guest_only')
{
	?>
	<div id="login" class="span5">
		<h4><?php echo JText::_('OPENSHOP_REGISTERED_CUSTOMER'); ?></h4>
		<p><?php echo JText::_('OPENSHOP_REGISTERED_CUSTOMER_INTRO'); ?></p>
		<fieldset>
			<div class="control-group">
				<label for="username" class="control-label"><?php echo JText::_('OPENSHOP_USERNAME'); ?></label>
				<div class="controls">
					<input type="text" placeholder="<?php echo JText::_('OPENSHOP_USERNAME_INTRO'); ?>" id="username" name="username" class="input-xlarge focused" />
				</div>
			</div>
			<div class="control-group">
				<label for="password" class="control-label"><?php echo JText::_('OPENSHOP_PASSWORD'); ?></label>
				<div class="controls">
					<input type="password" placeholder="<?php echo JText::_('OPENSHOP_PASSWORD_INTRO'); ?>" id="password" name="password" class="input-xlarge" />
				</div>
			</div>
			<label class="checkbox" for="remember">
				<input type="checkbox" alt="<?php echo JText::_('OPENSHOP_REMEMBER_ME'); ?>" value="yes" class="inputbox" name="remember" id="remember" /><?php echo JText::_('OPENSHOP_REMEMBER_ME'); ?>
			</label>
			<ul>
				<li>
					<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
					<?php echo JText::_('OPENSHOP_FORGOT_YOUR_PASSWORD'); ?></a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
					<?php echo JText::_('OPENSHOP_FORGOT_YOUR_USERNAME'); ?></a>
				</li>
			</ul>
			<input type="button" class="btn btn-primary pull-left" id="button-login" value="<?php echo JText::_('OPENSHOP_CONTINUE'); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
	</div>
	<?php
}
?>
<script type="text/javascript">
	//Script to change Payment Address heading when changing checkout options between Register and Guest
	OpenShop.jQuery(document).ready(function($){
		$('#checkout-options .checkout-content input[name=\'account\']').click(function(){
			if ($(this).val() == 'register') {
				$('#payment-address .checkout-heading').html('<?php echo JText::_('OPENSHOP_CHECKOUT_STEP_2_REGISTER'); ?>');
			} else {
				$('#payment-address .checkout-heading').html('<?php echo JText::_('OPENSHOP_CHECKOUT_STEP_2_GUEST'); ?>');
			}
		});

		//Checkout options - will run if user choose Register Account or Guest Checkout
		$('#button-account').click(function(){
			var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
			$.ajax({
				url: siteUrl + 'index.php?option=com_openshop&view=checkout&layout=' + $('input[name=\'account\']:checked').attr('value') + '&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
				dataType: 'html',
				beforeSend: function() {
					$('#button-account').attr('disabled', true);
					$('#button-account').after('<span class="wait">&nbsp;<img src="components/com_openshop/assets/images/loading.gif" alt="" /></span>');
				},
				complete: function() {
					$('#button-account').attr('disabled', false);
					$('.wait').remove();
				},
				success: function(html) {
					$('#payment-address .checkout-content').html(html);
					$('#checkout-options .checkout-content').slideUp('slow');
					$('#payment-address .checkout-content').slideDown('slow');
					$('.checkout-heading a').remove();
					$('#checkout-options .checkout-heading').append('<a><?php echo Jtext::_('OPENSHOP_EDIT'); ?></a>');
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});

		
		//Login - will run if user choose login with an existed account
		$('#button-login').click(function(){
			var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
			$.ajax({
				url: siteUrl + 'index.php?option=com_openshop&task=checkout.login<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
				type: 'post',
				data: $('#checkout-options #login :input'),
				dataType: 'json',
				beforeSend: function() {
					$('#button-login').attr('disabled', true);
					$('#button-login').after('<span class="wait">&nbsp;<img src="components/com_openshop/assets/images/loading.gif" alt="" /></span>');
				},	
				complete: function() {
					$('#button-login').attr('disabled', false);
					$('.wait').remove();
				},				
				success: function(json) {
					$('.warning, .error').remove();
					if (json['return']) {
						window.location.href = json['return'];
					} else if (json['error']) {
						$('#checkout-options .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
						$('.warning').fadeIn('slow');
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	});
</script>