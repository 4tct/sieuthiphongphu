<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/colorbox/jquery.colorbox.js" type="text/javascript"></script>
<script type="text/javascript">
	OpenShop.jQuery(document).ready(function($){			
		$(".colorbox").colorbox({
			overlayClose: true,
			opacity: 0.5,
		});
	});
</script>
<?php
if (count($this->methods))
{
	?>
	<div class="control-group">
		<p><?php echo JText::_('OPENSHOP_PAYMENT_METHOD_TITLE'); ?></p>
		<?php
		for ($i = 0 , $n = count($this->methods); $i < $n; $i++)
		{
			$paymentMethod = $this->methods[$i];
			if ($paymentMethod->getName() == $this->paymentMethod)
			{
				$checked = ' checked="checked" ';
			}
			else
				$checked = '';
			?>
			<label class="radio">
				<input type="radio" name="payment_method" value="<?php echo $paymentMethod->getName(); ?>" <?php echo $checked; ?> /> <?php echo JText::_($paymentMethod->title); ?> <br />
			</label>
			<?php
		}
		?>
	</div>
	<?php
}
if (OpenShopHelper::getConfigValue('enable_checkout_donate'))
{
	?>
	<br />
	<div class="control-group">
		<p><?php echo JText::_('OPENSHOP_CHECKOUT_DONATE_INTRO'); ?></p>
		<?php
		if (OpenShopHelper::getConfigValue('donate_amounts') != '')
		{
			$donateAmounts = explode("\n", OpenShopHelper::getConfigValue('donate_amounts'));
			$donateExplanations = explode("\n", OpenShopHelper::getConfigValue('donate_explanations'));
			for ($i = 0 , $n = count($donateAmounts); $i < $n; $i++)
			{
				?>
				<label class="radio">
					<?php
					if ($donateAmounts[$i] > 0)
					{
						?>
						<input type="radio" name="donate_amount" value="<?php echo trim($donateAmounts[$i]); ?>" /> <?php echo $this->currency->format(trim($donateAmounts[$i])) . (isset($donateExplanations[$i]) && $donateExplanations[$i] != '' ? ' (' . trim($donateExplanations[$i]) . ')' : ''); ?><br />
						<?php
					}
					else 
					{
						?>
						<input type="radio" checked="checked" name="donate_amount" value="<?php echo trim($donateAmounts[$i]); ?>" /> <?php echo (isset($donateExplanations[$i]) && $donateExplanations[$i] != '' ? trim($donateExplanations[$i]) : ''); ?><br />
						<?php
					}
					?>
				</label>
				<?php
			}
			?>
				<label class="radio">
					<input type="radio" name="donate_amount" value="other_amount" /><?php echo JText::_('OPENSHOP_DONATE_OTHER_AMOUNT'); ?><br />
				</label>
				<input type="text" name="other_amount" id="other_amount" class="input-small" />
			<?php
		}
		else 
		{
			?>
			<label for="other_amount" class="control-label"><?php echo JText::_('OPENSHOP_DONATE_AMOUNT'); ?></label>
			<input type="text" name="other_amount" id="other_amount" class="input-small" />
			<?php
		}
		?>
	</div>
	<?php
}
if (OpenShopHelper::getConfigValue('allow_coupon'))
{
	?>
	<br />
	<div class="control-group">
		<label for="coupon_code" class="control-label"><?php echo JText::_('OPENSHOP_COUPON_TEXT'); ?></label>
		<div class="controls">
			<input type="text" id="coupon_code" name="coupon_code" class="input-large" value="<?php echo htmlspecialchars($this->coupon_code, ENT_COMPAT, 'UTF-8'); ?>">
		</div>
	</div>
	<?php
}
if (OpenShopHelper::getConfigValue('allow_voucher') && $this->user->get('id'))
{
	?>
	<div class="control-group">
		<label for="voucher_code" class="control-label"><?php echo JText::_('OPENSHOP_VOUCHER_TEXT'); ?></label>
		<div class="controls">
			<input type="text" id="voucher_code" name="voucher_code" class="input-large" value="<?php echo htmlspecialchars($this->voucher_code, ENT_COMPAT, 'UTF-8'); ?>">
		</div>
	</div>
	<?php
}
?>
<br />
<div class="control-group">
	<label for="textarea" class="control-label"><?php echo JText::_('OPENSHOP_COMMENT_ORDER'); ?></label>
	<div class="controls">
		<textarea rows="8" id="textarea" class="input-xlarge span12" name="comment"><?php echo $this->comment; ?></textarea>
	</div>
</div>
<div class="no_margin_left">
	<?php
	if (isset($this->checkoutTermsLink) && $this->checkoutTermsLink != '')
	{
		?>
		<span class="privacy">
			<input type="checkbox" value="1" name="checkout_terms_agree" <?php echo ($this->checkout_terms_agree) ? $this->checkout_terms_agree : ''; ?>/>
			&nbsp;<?php echo JText::_('OPENSHOP_CHECKOUT_TERMS_AGREE'); ?>&nbsp;<a class="colorbox cboxElement" href="<?php echo $this->checkoutTermsLink; ?>"><?php echo JText::_('OPENSHOP_CHECKOUT_TERMS_AGREE_TITLE'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</span>	
		<?php
	}
	?>
	<input type="button" class="btn btn-primary pull-right" id="button-payment-method" value="<?php echo JText::_('OPENSHOP_CONTINUE'); ?>" />
</div>
<script type="text/javascript">
	OpenShop.jQuery(function($){
		// Payment Method
		$('#button-payment-method').click(function(){
			var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
			$.ajax({
				url: siteUrl + 'index.php?option=com_openshop&task=checkout.processPaymentMethod<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
				type: 'post',
				data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'text\'],  #payment-method textarea'),
				dataType: 'json',
				beforeSend: function() {
					$('#button-payment-method').attr('disabled', true);
					$('#button-payment-method').after('<span class="wait">&nbsp;<img src="components/com_openshop/assets/images/loading.gif" alt="" /></span>');
				},	
				complete: function() {
					$('#button-payment-method').attr('disabled', false);
					$('.wait').remove();
				},			
				success: function(json) {
					$('.warning, .error').remove();
					
					if (json['return']) {
						window.location.href = json['return'];
					} else if (json['error']) {
						if (json['error']['warning']) {
							$('#payment-method .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
							$('.warning').fadeIn('slow');
						}
					} else {
						var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
						$.ajax({
							url: siteUrl + 'index.php?option=com_openshop&view=checkout&layout=confirm&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
							dataType: 'html',
							success: function(html) {
								$('#confirm .checkout-content').html(html);
								$('#payment-method .checkout-content').slideUp('slow');
								$('#confirm .checkout-content').slideDown('slow');
								$('#payment-method .checkout-heading a').remove();
								$('#payment-method .checkout-heading').append('<a><?php echo JText::_('OPENSHOP_EDIT'); ?></a>');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	})
</script>