<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
if (isset($this->success))
{
	?>
	<div class="success"><?php echo $this->success; ?></div>
	<?php
}
if (!OpenShopHelper::isMobile())
{
	?>
	<div class="cart-info">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
					<th><?php echo JText::_('OPENSHOP_MODEL'); ?></th>
					<th><?php echo JText::_('OPENSHOP_QUANTITY'); ?></th>
					<th><?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?></th>
					<th><?php echo JText::_('OPENSHOP_TOTAL'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($this->cartData as $key => $product)
				{
					$optionData = $product['option_data'];
					$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
					?>
					<tr>
						<td>
							<a href="<?php echo $viewProductUrl; ?>">
								<?php echo $product['product_name']; ?>
							</a><br />	
							<?php
							for ($i = 0; $n = count($optionData), $i < $n; $i++)
							{
								echo '- ' . $optionData[$i]['option_name'] . ': ' . $optionData[$i]['option_value'] . (isset($optionData[$i]['sku']) && $optionData[$i]['sku'] != '' ? ' (' . $optionData[$i]['sku'] . ')' : '') . '<br />';
							}
							?>
						</td>
						<td><?php echo $product['product_sku']; ?></td>
						<td>
							<?php echo $product['quantity']; ?>
						</td>
						<td><?php echo $this->currency->format($this->tax->calculate($product['price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax'))); ?></td>
						<td><?php echo $this->currency->format($this->tax->calculate($product['total_price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax'))); ?></td>
					</tr>
					<?php
				}
				foreach ($this->totalData as $data)
				{
					?>
					<tr>
						<td colspan="4" style="text-align: right;"><?php echo $data['title']; ?>:</td>
						<td><strong><?php echo $data['text']; ?></strong></td>
					</tr>
					<?php	
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
}
else
{
	?>
	<div class="cart-info">
		<div class="row-fluid">
			<?php
			foreach ($this->cartData as $key => $product)
			{
				$optionData = $product['option_data'];
				$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
				?>
				<div class="well clearfix">
					<div class="row-fluid">
						<div class="span4">							
							<h5 class="openshop-center-text">
								<a href="<?php echo $viewProductUrl; ?>">
									<?php echo $product['product_name']; ?>
								</a>
							</h5>
							<?php
							for ($i = 0; $n = count($optionData), $i < $n; $i++)
							{
								echo '- ' . $optionData[$i]['option_name'] . ': ' . $optionData[$i]['option_value'] . '<br />';
							}
							?>
						</div>
						<div class="span2">
							<strong><?php echo JText::_('OPENSHOP_MODEL'); ?>: </strong>
							<?php echo $product['product_sku']; ?>
						</div>
						<div class="span2">
							<strong><?php echo JText::_('OPENSHOP_QUANTITY'); ?>: </strong>
							<?php echo $product['quantity']; ?>
						</div>
						<div class="span2">
							<strong><?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?> : </strong>
							<?php echo $this->currency->format($this->tax->calculate($product['price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax'))); ?>
						</div>
						<div class="span2">
							<strong><?php echo JText::_('OPENSHOP_TOTAL'); ?>: </strong>
							<?php echo $this->currency->format($this->tax->calculate($product['total_price'], $product['product_taxclass_id'], OpenShopHelper::getConfigValue('tax'))); ?>
						</div>
					</div>
				</div>
				<?php
			}
			if (OpenShopHelper::showPrice())
			{
				?>
				<div class="well clearfix">
					<?php
					foreach ($this->totalData as $data)
					{
						echo $data['title']; ?>:<strong><?php echo $data['text']; ?></strong><br />
						<?php
					}
					?>
				</div>
				<?php
			}
			?>
		</div>
	</div>
<?php
}
if ($this->total > 0)
{
	?>
	<div class="openshop-payment-information">
		<?php echo $this->paymentClass->renderPaymentInformation(); ?>
	</div>
	<?php
}
else 
{
	?>
	<form action="<?php echo OpenShopHelper::getSiteUrl(); ?>index.php?option=com_openshop&task=checkout.processOrder" method="post" name="payment_method_form" id="payment_method_form" class="form form-horizontal">
		<div class="no_margin_left">
			<div class="no_margin_left">
				<input id="button-confirm" type="submit" class="btn btn-primary pull-right" value="<?php echo JText::_('OPENSHOP_CONFIRM_ORDER'); ?>" />
			</div>
		</div>
	</form>
	<?php	
}