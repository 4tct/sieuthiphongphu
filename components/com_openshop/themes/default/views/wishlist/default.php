<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/colorbox/jquery.colorbox.js" type="text/javascript"></script>
<?php
if (isset($this->success))
{
	?>
	<div class="success"><?php echo $this->success; ?></div>
	<?php
}
?>
<h1><?php echo JText::_('OPENSHOP_MY_WISHLIST'); ?></h1><br />
<?php
if (!count($this->products))
{
	?>
	<div class="no-content"><?php echo JText::_('OPENSHOP_WISHLIST_EMPTY'); ?></div>
	<?php
}
else
{
	?>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th><?php echo JText::_('OPENSHOP_IMAGE'); ?></th>
				<th><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
				<th><?php echo JText::_('OPENSHOP_MODEL'); ?></th>
				<th><?php echo JText::_('OPENSHOP_AVAILABILITY'); ?></th>
				<th><?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?></th>
				<th><?php echo JText::_('OPENSHOP_ACTION'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($this->products as $product)
			{
				$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product->id, OpenShopHelper::getProductCategory($product->id)));
				?>
				<tr>
					<td class="muted openshop-center-text">
						<a href="<?php echo $viewProductUrl; ?>">
							<img class="img-polaroid" src="<?php echo $product->image; ?>" />
						</a>
					</td>
					<td>
						<a href="<?php echo $viewProductUrl; ?>">
							<?php echo $product->product_name; ?>
						</a>
					</td>
					<td><?php echo $product->product_sku; ?></td>
					<td>
						<?php echo $product->availability; ?>
					</td>
					<td>
						<?php
						if (OpenShopHelper::showPrice())
						{
							if (!$product->product_call_for_price)
							{
								if ($product->sale_price)
								{
									?>
									<span class="openshop-base-price"><?php echo $product->base_price; ?></span>&nbsp;
									<span class="openshop-sale-price"><?php echo $product->sale_price; ?></span>
									<?php
								}
								else 
								{
									?>
									<span class="price"><?php echo $product->base_price; ?></span>
									<?php
								}
							}
							else
							{
								?>
								<span class="call-for-price"><?php echo JText::_('OPENSHOP_CALL_FOR_PRICE'); ?>: <?php echo OpenShopHelper::getConfigValue('telephone'); ?></span>
								<?php
							}
						}
						?>
					</td>
					<td>
						<?php
						if (!OpenShopHelper::getConfigValue('catalog_mode') && OpenShopHelper::showPrice() && !$product->product_call_for_price)
						{
							?>
							<input id="add-to-cart-<?php echo $product->id; ?>" type="button" class="btn btn-primary" onclick="addToCart(<?php echo $product->id; ?>, 1, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>');" value="<?php echo JText::_('OPENSHOP_ADD_TO_CART'); ?>" />
							<?php
						}
						?>
						<input type="button" class="btn btn-primary" onclick="removeFromWishlist(<?php echo $product->id; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>');" value="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" />
					</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>	
	<?php
}