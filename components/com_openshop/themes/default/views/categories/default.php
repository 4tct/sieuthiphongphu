<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

if (isset($this->warning))
{
?>
	<div class="warning"><?php echo $this->warning; ?></div>
<?php
}
//Display Shop Instroduction
if (OpenShopHelper::getMessageValue('shop_introduction') != '' && OpenShopHelper::getConfigValue('introduction_display_on', 'front_page') == 'categories_page')
{
	?>
	<div class="eshop-shop-introduction"><?php echo OpenShopHelper::getMessageValue('shop_introduction'); ?></div>
	<?php
}
if (count($this->items)) 
{
	if ($this->params->get('show_page_heading'))
	{
	?>
		<h1 class="eshop-categories-heading"><?php echo JText::_('OPENSHOP_CATEGORIES'); ?></h1>
	<?php
	}
	?>
	<div class="eshop-categories-list"><?php echo OpenShopHtmlHelper::loadCommonLayout('common/categories.php', array ('categories' => $this->items, 'categoriesPerRow' => $this->categoriesPerRow)); ?></div>
	<?php
	if ($this->pagination->total > $this->pagination->limit) 
	{
	?>
		<div class="pagination">
			<?php echo $this->pagination->getPagesLinks(); ?>
		</div>
	<?php
	}
}