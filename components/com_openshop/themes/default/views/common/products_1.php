<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$user = JFactory::getSession()->get('user');
?>

<div id="products-list-container" class="products-list-container block list">
    <div class="sortPagiBar row-fluid">
        <div class="span3" style="text-align: right;">
            <div class="btn-group hidden-phone">
                <?php
                $active_grid = '';
                $active_list = '';
                if (OpenShopHelper::getConfigValue('default_products_layout') == 'grid') {
                    $active_grid = "btn-chose-active";
                } else {
                    $active_list = "btn-chose-active";
                }
                ?>
                <a rel="grid" href="#" class="btn-chose <?php echo $active_grid ?>" style="padding: 10px;"><i class="fa fa-th-large"></i></a>
                <a rel="list" href="#" class="btn-chose <?php echo $active_list ?>"><i class="fa fa-list"></i></a>

            </div>
        </div>
        <?php
        if ($showSortOptions) {
            ?>
            <div class="span9">
                <form method="post" name="adminForm" id="adminForm" action="<?php echo $actionUrl; ?>">
                    <div class="clearfix">
                        <div class="openshop-product-show">
                            <b><?php echo JText::_('OPENSHOP_SHOW'); ?>: </b>
                            <?php echo $pagination->getLimitBox(); ?>
                        </div>
                        <?php
                        if ($sort_options) {
                            ?>
                            <div class="openshop-product-sorting">
                                <b><?php echo JText::_('OPENSHOP_SORTING_BY'); ?>: </b>
                                <?php echo $sort_options; ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </form> 
            </div>
            <?php
        }
        ?>
    </div>
    <div id="products-list" class="products-list">
        <!--//menu categories-->
        <div class="col-md-2 colMenuC" style="padding-right: 15px;">
            <div class="search-categories">
                <div class="title-categories">Danh mục</div>
                <div class="allcategories">
                    <ul>
                        <?php
                        $db = JFactory::getDbo();
                        $query = $db->getQuery(TRUE);
                        $query->select('a.id, b.category_name AS title, a.category_parent_id AS parent_id')
                                ->from($db->quoteName('#__openshop_categories', 'a'))
                                ->join('INNER', $db->quoteName('#__openshop_categorydetails', 'b') . 'ON a.id = b.category_id')
                                ->where('a.published = 1');
                        $rowsC = $db->setQuery($query)->loadObjectList();
                        $children = array();
                        if ($rowsC) {
                            // first pass - collect children
                            foreach ($rowsC as $v) {
                                $pt = $v->parent_id;
                                $list = @$children[$pt] ? $children[$pt] : array();
                                array_push($list, $v);
                                $children[$pt] = $list;
                            }
                        }
                        $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
                        $options = array();
                        foreach ($list as $listItem) {
                            $options[] = JHtml::_('select.option', $listItem->id, $listItem->treename);
                        }

                        foreach ($options as $c) {
                            $categoryUrl = JRoute::_(OpenShopRoute::getCategoryRoute($c->value));
                            ?>
                            <li>
                                <a href="<?php echo $categoryUrl ?>"><?php echo $c->text ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>

        <!--//show products-->
        <div class="col-md-10 <?php echo OpenShopHelper::getConfigValue('default_products_layout') ?> colShowPro" id="contentShowProduct">
            <?php
            $col = 1;
            foreach ($products as $product) {
                $productUrl = JRoute::_(OpenShopRoute::getProductRoute($product->id, $catId ? $catId : OpenShopHelper::getProductCategory($product->id)));
                $id = OpenShopHelper::convertBase64Encode($product->id);
                if ($col == 1) {
                    echo '<div class="col-md-12">';
                }
                ?>
                <div class="productD col-xs-6 col-sm-3 col-md-3">
                    <?php
                    if ($product->product_new) {
                        ?>
                        <div class="new-item bg-item"><span>New</span></div>
                        <?php
                    }

                    $sizes = OpenShopHelper::getProductOptionValueDetails($product->id, 'size');
                    foreach ($sizes as $k => $s) {
                        ?>
                        <div class="showSizeP posS-<?php echo $k ?>"><?php echo $s->value ?></div>
                        <?php
                    }
                    ?>
                    <img onclick="location.href = '<?php echo $productUrl ?>'" class="lazy img-responsive" data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $product->product_image ?>" width="100%" alt="<?php echo $product->product_name ?>"/>
                    <!--<div class="bg-featurePH"></div>-->
                    <div class="featurePH">
                        <?php
                        if (!empty($user->id)) {
                            ?>
                            <span class="addcart" onclick="showChooseSize('<?php echo $id ?>')">
                                <i class="fa fa-cart-plus" style="font-size: 18px;"></i> Thêm vào giỏ hàng
                            </span>
                            <?php
                            $db = JFactory::getDbo();
                            $query = $db->getQuery(TRUE);
                            $query->select('count(id)')
                                    ->from($db->quoteName('#__openshop_wishlists'))
                                    ->where('product_id = ' . $product->id)
                                    ->where('customer_id = ' . $user->id);
                            $cw = $db->setQuery($query)->loadResult();
                            if (!$cw) {
                                $cls = 'addlove';
                                $tlt = 'Chưa thích sản phẩm';
                                $onclick = 'wishProduct(\'' . $id . '\')';
                            } else {
                                $cls = 'loved';
                                $tlt = 'Đã thích sản phẩm';
                                $onclick = 'nonwishProduct(\'' . $id . '\')';
                            }
                            ?>
                            <span class="<?php echo $cls; ?>" onclick="<?php echo $onclick; ?>" title="<?php echo $tlt ?>">
                                <i class="fa fa-heart" style="font-size: 18px;"></i>
                            </span>
                            <?php
                        } else {
                            ?>
                            <span class="addcart" onclick="showChooseSize('<?php echo $id ?>')" style="border: 0px;">
                                <i class="fa fa-cart-plus" style="font-size: 18px;"></i> Thêm vào giỏ hàng
                            </span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="infoPH">
                        <div class="proname">
                            <a href="<?php echo $productUrl; ?>" title="<?php echo $product->product_name ?>"><?php echo $product->product_name ?></a>
                        </div>
                        <?php
                        if ($product->product_price_r) {
                            ?>
                            <div class="pricePHnotSaleR">
                                <div class="priceold2">
                                    <?php echo number_format((int) $product->product_price_r, '0', ',', '.') ?><sup>đ</sup>
                                </div>
                                <div class="pricebuy2"><?php echo number_format((int) $product->product_price, '0', ',', '.') ?><sup>đ</sup></div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="pricePHnotSale">
                                <div class="pricebuy2"><?php echo number_format((int) $product->product_price, '0', ',', '.') ?><sup>đ</sup></div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
                if ($col == 4) {
                    echo '</div>';
                    $col = 1;
                } else {
                    ++$col;
                }
            }
            ?>
        </div>
        </div>

        <div class="clr"></div>

        <!--//pagination default-->
        <?php
        if (isset($pagination) && ($pagination->total > $pagination->limit)) {
            ?>
            <div class="row-fluid">
                <div class="pagination">
                    <?php echo $pagination->getPagesLinks(); ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>


<div class = "modal fade chooseSizeCP form-horizontal" tabindex = "-1" role = "dialog" aria-labelledby = "mySmallModalLabel">
    <div class = "modal-dialog modal-sm" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
                <h4 class = "modal-title" id = "myModalLabel">Chọn Size</h4>
            </div>
            <div class = "modal-body">
                <label>Chọn size</label>
                <div class="contentSizeCP">
                    <!--//size-->
                </div>
            </div>
            <div class = "modal-footer">
                <button type = "button" class = "btn btn-primary addCP" onclick="">Thêm vào giỏ hàng</button>
            </div>
        </div>
    </div>
</div>

<script>
    if (jQuery(document).width() <= 540) {
        jQuery('.productD').removeClass('col-xs-6');
    }

    if (jQuery(document).width() < 1024) {
        jQuery('.colMenuC').css('display', 'none');
        jQuery('.colShowPro').addClass('col-md-12');
        jQuery('.colShowPro').removeClass('col-md-10');
    } else
    {
        jQuery('.colMenuC').css('display', 'block');
        jQuery('.colShowPro').addClass('col-md-10');
        jQuery('.colShowPro').removeClass('col-md-12');
    }


    jQuery(window).resize(function () {
        if (jQuery(this).width() <= 450) {
            jQuery('.productD').removeClass('col-xs-6');
        } else
        {
            jQuery('.productD').addClass('col-xs-6');
        }
    });

</script>

<div class="clr"></div>