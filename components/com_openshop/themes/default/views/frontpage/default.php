<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
//Display Shop Instroduction
?>

<div class="col-md-12 padding0">
    <?php
    if (OpenShopHelper::getMessageValue('shop_introduction') != '' && OpenShopHelper::getConfigValue('introduction_display_on', 'front_page') == 'front_page') {
        ?>
        <div class="openshop-shop-introduction"><?php echo OpenShopHelper::getMessageValue('shop_introduction'); ?></div>
        <?php
    }
    //Product hits
    echo OpenShopHtmlHelper::loadCommonLayout('common/showintro.php', array(
        'products_hit' => $this->products_hit,
        'printProduct' => $this->printProduct,
        'tax' => $this->tax,
        'currency' => $this->currency,
        'productsPerRow' => $this->productsPerRow,
        'catId' => 0,
        'showSortOptions' => false
    ));

    /**     Display categories * */
    $t = 0;
    if ($t) {
        if (count($this->categories) && $this->show_categories) {
            ?>
            <div class="openshop-categories-list">
                <?php
                echo OpenShopHtmlHelper::loadCommonLayout('common/categories.php', array('categories' => $this->categories,
                    'categoriesPerRow' => $this->categoriesPerRow
                        )
                );
                ?>
            </div>	
            <?php
        }
    }
    ?>
</div>

