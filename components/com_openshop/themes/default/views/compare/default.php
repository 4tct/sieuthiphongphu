<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/colorbox/jquery.colorbox.js" type="text/javascript"></script>
<?php
if (isset($this->success))
{
	?>
	<div class="success"><?php echo $this->success; ?></div>
	<?php
}
?>
<h1><?php echo JText::_('OPENSHOP_PRODUCT_COMPARE'); ?></h1><br />
<?php
if (!count($this->products))
{
	?>
	<div class="no-content"><?php echo JText::_('OPENSHOP_COMPARE_EMPTY'); ?></div>
	<?php
}
else
{
	?>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th colspan="<?php echo count($this->products) + 1; ?>"><?php echo JText::_('OPENSHOP_COMPARE_PRODUCT_DETAILS'); ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="20%" style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_PRODUCT'); ?></b></td>
				<?php
				foreach ($this->products as $product)
				{
					$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
					?>
					<td>
						<a href="<?php echo $viewProductUrl; ?>">
							<?php echo $product['product_name']; ?>
						</a>
					</td>
					<?php
				}
				?>
			</tr>
			<?php
			if (OpenShopHelper::getConfigValue('compare_image'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_IMAGE'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td style="text-align:center;">
							<img class="img-polaroid" src="<?php echo $product['image']; ?>" />
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::showPrice() && OpenShopHelper::getConfigValue('compare_price'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_PRICE'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php
							if (!$product['product_call_for_price'])
							{
								if ($product['sale_price'])
								{
									?>
									<span class="openshop-base-price"><?php echo $product['base_price']; ?></span>&nbsp;
									<span class="openshop-sale-price"><?php echo $product['sale_price']; ?></span>
									<?php
								}
								else 
								{
									?>
									<span class="price"><?php echo $product['base_price']; ?></span>
									<?php
								}
							}
							else
							{
								?>
								<span class="call-for-price"><?php echo JText::_('OPENSHOP_CALL_FOR_PRICE'); ?>: <?php echo OpenShopHelper::getConfigValue('telephone'); ?></span>
								<?php
							}
							?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_sku'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_MODEL'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php echo $product['product_sku']; ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_manufacturer'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_BRAND'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php echo $product['manufacturer']; ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_availability'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_AVAILABILITY'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php echo $product['availability']; ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_rating'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_RATING'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<img src="components/com_openshop/assets/images/stars-<?php echo round($product['rating']); ?>.png" /><br />
							<?php echo sprintf(JText::_('OPENSHOP_COMPARE_NUM_REVIEWS'), $product['num_reviews']); ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_short_desc'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_SHORT_DESCRIPTION'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php echo $product['product_short_desc']; ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_desc'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_DESCRIPTION'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php echo $product['product_desc']; ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_weight'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_WEIGHT'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php echo $product['weight']; ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (OpenShopHelper::getConfigValue('compare_dimensions'))
			{
				?>
				<tr>
					<td style="text-align:right;"><b><?php echo JText::_('OPENSHOP_COMPARE_DIMENSIONS'); ?></b></td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td>
							<?php echo $product['length'] . ' x ' . $product['width'] . ' x ' . $product['height']; ?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			if (count($this->visibleAttributeGroups) && OpenShopHelper::getConfigValue('compare_attributes'))
			{
				foreach ($this->visibleAttributeGroups as $key => $visibleAttributeGroup)
				{
					?>
					<tr>
						<th style="text-align: left;" colspan="<?php echo count($this->products) + 1; ?>"><?php echo $visibleAttributeGroup['attributegroup_name']; ?></th>
					</tr>
					<?php
					foreach ($visibleAttributeGroup['attribute_name'] as $attributeName)
					{
						?>
						<tr>
							<td style="text-align:right;"><?php echo $attributeName; ?></td>
							<?php
							foreach ($this->products as $product)
							{
								?>
								<td>
									<?php
										if (isset($product['attributes'][$visibleAttributeGroup['id']]['value'][$attributeName]))
										{
											echo $product['attributes'][$visibleAttributeGroup['id']]['value'][$attributeName];
										}
									?>
								</td>
								<?php
							}
							?>
						</tr>
						<?php
					}
				}
			}
			$showAddToCart = false;
			foreach ($this->products as $product)
			{
				if (!OpenShopHelper::getConfigValue('catalog_mode') && OpenShopHelper::showPrice() && !$product['product_call_for_price'])
				{
					$showAddToCart = true;
					break;
				}
			}
			if ($showAddToCart)
			{
				?>
				<tr>
					<td>&nbsp;</td>
					<?php
					foreach ($this->products as $product)
					{
						?>
						<td style="text-align:center;">
							<?php
							if (!OpenShopHelper::getConfigValue('catalog_mode') && OpenShopHelper::showPrice() && !$product['product_call_for_price'])
							{
								?>
								<input id="add-to-cart-<?php echo $product['product_id']; ?>" type="button" class="btn btn-primary" onclick="addToCart(<?php echo $product['product_id']; ?>, 1, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>');" value="<?php echo JText::_('OPENSHOP_ADD_TO_CART'); ?>" />
								<?php
							}
							?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
			?>
			<tr>
				<td>&nbsp;</td>
				<?php
				foreach ($this->products as $product)
				{
					?>
					<td style="text-align:center;">
						<input type="button" class="btn btn-primary" onclick="removeFromCompare(<?php echo $product['product_id']; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>');" value="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" />
					</td>
					<?php
				}
				?>
			</tr>
		</tbody>
	</table>	
	<?php
}