<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

//get all status of order
$db = JFactory::getDbo();
$query = $db->getQuery(TRUE);
$query->select('a.orderstatus_id as id,a.orderstatus_name as value')
        ->from($db->quoteName('#__openshop_orderstatusdetails', 'a'))
        ->join('INNER', $db->quoteName('#__openshop_orderstatuses', 'b') . ' ON b.id = a.orderstatus_id')
        ->where('b.published = 1');
$statuses = $db->setQuery($query)->loadObjectList();
$sts = array();
if (!empty($statuses)) {
    foreach ($statuses as $status) {
        $sts[$status->id] = $status->value;
    }
}
?>
<div class="parchaseHistory">
    <h1>Lịch sử mua hàng</h1>
    <div class="contentparchaseH">
        <table class="table table-hover table-responsive table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tên sản phẩm</th>
                    <th>Hình ảnh</th>
                    <th>Số lượng</th>
                    <th>Giá bán</th>
                    <th>Thành tiền</th>
                    <th>Trạng thái</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (empty($this->orders)) {
                    ?>
                    <tr>
                        <td colspan="7"><i>Bạn chưa mua hàng</i></td>
                    </tr>
                    <?php
                } else {
                    foreach ($this->orders as $key => $order) {
                        ?>
                        <tr>
                            <td align="center"><?php echo $key + 1 ?></td>
                            <td><?php echo $order->product_name ?></td>
                            <td align="center"><img class="lazy" width="60px" data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $order->product_image ?>" title="<?php echo $order->product_name ?>" alt="<?php echo $order->product_name ?>" /></td>
                            <td align="center"><?php echo $order->quantity ?></td>
                            <td align="center"><?php echo number_format((int) $order->price, 0, ',', '.'); ?></td>
                            <td align="center"><?php echo number_format((int) $order->total_price, 0, ',', '.'); ?></td>
                            <td align="center">
                                <span class="border_background_<?php echo strtolower($sts[$order->order_status_id]) ?>"><?php echo JText::_('OPENSHOP_STATUS_ORDER_' . strtoupper($sts[$order->order_status_id])) ?></span>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>

        <div class="parchaseHPagination">
            <nav aria-label="Page navigation" class="pageH">
                <ul class="pagination">
                    <li>
                        <a aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <?php
                    $app = JFactory::getApplication()->input;
                    $limit = $app->getInt('pageN');
                    for ($i = 0; $i < $this->paginations; $i++) {
                        $actPageH = '';
                        if ($i == $limit) {
                            $actPageH = 'active';
                        }
                        ?>
                        <li class="<?php echo $actPageH ?>">
                            <a class="c" onclick="getPageN(<?php echo $i ?>)"><?php echo $i + 1 ?></a>
                        </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<form action="/lich-su-mua-hang.html" method="post" id="formParchaseH">
    <?php
    if (isset($limit)) {
        $limit = $app->getInt('pageN');
    } else {
        $limit = OpenShopHelper::getConfigValue('catalog_limit');
    }
    ?>
    <input type="hidden" name="pageN" id="pageN" value="<?php echo $limit ?>" />
    <?php JHtml::_('form.token') ?>
</form>

