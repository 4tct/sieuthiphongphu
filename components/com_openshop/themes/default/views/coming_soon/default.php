<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>

<div class="comming-soon" style="text-align: center;padding: 42px;">
    <img class="lazy" src="<?php echo 'components/com_openshop/assets/images/comingsoon.png' ?>" />
</div>