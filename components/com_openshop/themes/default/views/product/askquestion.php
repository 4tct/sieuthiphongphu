<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="row-fluid">
	<h1 id="ask-question-title"><?php echo JText::_('OPENSHOP_ASK_QUESTION'); ?></h1>
	<div class="ask-question-intro"><?php echo sprintf(JText::_('OPENSHOP_ASK_QUESTION_INTRO'), $this->item->product_name); ?></div>
	<div id="ask-question-area">
		<form method="post" name="adminForm" id="adminForm" action="index.php" class="form form-horizontal">
			<div class="control-group">
				<label class="control-label" for="name"><span class="required">*</span><?php echo JText::_('OPENSHOP_NAME'); ?>:</label>
				<div class="controls docs-input-sizes">
					<input type="text" class="input-large" name="name" id="name" value="" />
					<span style="display: none;" class="error name-required"><?php echo JText::_('OPENSHOP_NAME_REQUIRED'); ?></span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="email"><span class="required">*</span><?php echo JText::_('OPENSHOP_EMAIL'); ?>:</label>
				<div class="controls docs-input-sizes">
					<input type="text" class="input-large" name="email" id="email" value="" />
					<span style="display: none;" class="error email-required"><?php echo JText::_('OPENSHOP_EMAIL_REQUIRED'); ?></span>
					<span style="display: none;" class="error email-invalid"><?php echo JText::_('OPENSHOP_EMAIL_INVALID'); ?></span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="company"><?php echo JText::_('OPENSHOP_COMPANY'); ?>:</label>
				<div class="controls docs-input-sizes">
					<input type="text" class="input-large" name="company" id="company" value="" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="phone"><?php echo JText::_('OPENSHOP_PHONE'); ?>:</label>
				<div class="controls docs-input-sizes">
					<input type="text" class="input-large" name="phone" id="phone" value="" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="message"><span class="required">*</span><?php echo JText::_('OPENSHOP_MESSAGE'); ?>:</label>
				<div class="controls docs-input-sizes">
					<textarea rows="5" cols="5" name="message" id="message"></textarea>
					<span style="display: none;" class="error message-required"><?php echo JText::_('OPENSHOP_MESSAGE_REQUIRED'); ?></span>
				</div>
			</div>
			<input type="hidden" name="product_id" id="product_id" value="<?php echo JRequest::getInt('id'); ?>" />
			<input type="button" class="btn btn-primary pull-left" id="button-ask-question" value="<?php echo JText::_('OPENSHOP_SUBMIT'); ?>" />
			<span class="wait"></span>
		</form>
	</div>
</div>
<script type="text/javascript">
	function isValidEmail(emailAddress)
	{
	    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	    return pattern.test(emailAddress);
	}
	OpenShop.jQuery(function($){
		$('#button-ask-question').click(function(){
			$('#success').hide();
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var contactName = $('#name').val();
			var contactEmail = $('#email').val();
			var contactMessage = $('#message').val();
			var validated = true;
			if(contactName == '')
			{
				validated = false;
				$('.name-required').show();
			}
			else
			{
				$('.name-required').hide();
			}
			
			if(contactEmail == '')
			{
				validated = false;
				$('.email-required').show();
			}
			else if (!isValidEmail(contactEmail))
			{
				validated = false;
				$('.email-required').hide();
				$('.email-invalid').show();
			}
			else
			{
				$('.email-required').hide();
				$('.email-invalid').hide();
			}
			
			if (contactMessage == '')
			{
				validated = false;
				$('.message-required').show();
			}
			else
			{
				$('.message-required').hide();
			}
	
			if (validated)
			{
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
				$.ajax({
					type :'POST',
					url: siteUrl + 'index.php?option=com_openshop&task=product.processAskQuestion<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
					data: $('#ask-question-area input[type=\'text\'], #ask-question-area input[type=\'hidden\'], #ask-question-area input[type=\'radio\']:checked, #ask-question-area input[type=\'checkbox\']:checked, #ask-question-area select, #ask-question-area textarea'),
					beforeSend: function() {
						$('.wait').html('<img src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/images/loading.gif" alt="" />');
					},
					success : function(html) {
						$('#ask-question-area').html('<div class="success"><?php echo JText::_('OPENSHOP_ASK_QUESTION_SUCCESSFULLY')?></div>');
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			}
		});
	});
</script>