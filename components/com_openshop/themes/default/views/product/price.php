<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$uri = JUri::getInstance();
?>
<h2>
	<strong>
		<?php echo JText::_('OPENSHOP_PRICE'); ?>:
		<?php
		$productPriceArray = OpenShopHelper::getProductPriceArray($this->item->id, $this->item->product_price);
		if ($productPriceArray['salePrice'])
		{
			?>
			<span class="openshop-base-price"><?php echo $this->currency->format($this->tax->calculate($productPriceArray['basePrice'], $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))); ?></span>&nbsp;
			<span class="openshop-sale-price"><?php echo $this->currency->format($this->tax->calculate($productPriceArray['salePrice'] + $this->option_price, $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))); ?></span>
			<?php
		}
		else
		{
			?>
			<span class="price"><?php echo $this->currency->format($this->tax->calculate($productPriceArray['basePrice'], $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))); ?></span>
			<?php
		}
		?>
	</strong><br />
	<?php
	if (OpenShopHelper::getConfigValue('tax') && OpenShopHelper::getConfigValue('display_ex_tax'))
	{
		?>
		<small>
			<?php echo JText::_('OPENSHOP_EX_TAX'); ?>:
		<?php
		if ($productPriceArray['salePrice'])
		{
			echo $this->currency->format($productPriceArray['salePrice'] + $this->option_price);
		}
		else
		{
			echo $this->currency->format($productPriceArray['basePrice']);
		}
		?>
		</small>
		<?php
	}
	?>
</h2>