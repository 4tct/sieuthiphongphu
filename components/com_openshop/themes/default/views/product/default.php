<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$uri = JUri::getInstance();

?>
<div class="product-info">
    <div class="row showProductDetail">
        <div class="col-sm-4 col-md-6">
            <?php
            if (OpenShopHelper::getConfigValue('view_image') == 'zoom') {
                ?>
                <div class="image img-polaroid" id="main-image-area" style="text-align: center;">
                    <a class="product-image-zoom" <?php if (count($this->productImages)) echo 'rel="product-thumbnails"'; ?> title="<?php echo $this->item->product_name; ?>">
                        <?php
                        if (count($this->labels)) {
                            for ($i = 0; $n = count($this->labels), $i < $n; $i++) {
                                $label = $this->labels[$i];
                                if ($label->label_style == 'rotated' && !($label->enable_image && $label->label_image)) {
                                    ?>
                                    <div class="cut-rotated">
                                        <?php
                                    }
                                    if ($label->enable_image && $label->label_image) {
                                        $imageWidth = $label->label_image_width > 0 ? $label->label_image_width : OpenShopHelper::getConfigValue('label_image_width');
                                        if (!$imageWidth)
                                            $imageWidth = 50;
                                        $imageHeight = $label->label_image_height > 0 ? $label->label_image_height : OpenShopHelper::getConfigValue('label_image_height');
                                        if (!$imageHeight)
                                            $imageHeight = 50;
                                        ?>
                                        <span class="horizontal <?php echo $label->label_position; ?> small-db" style="opacity: <?php echo $label->label_opacity; ?>;<?php echo 'background-image: url(' . $label->label_image . ')'; ?>; background-repeat: no-repeat; width: <?php echo $imageWidth; ?>px; height: <?php echo $imageHeight; ?>px; box-shadow: none;"></span>
                                        <?php
                                    }
                                    else {
                                        ?>
                                        <span class="<?php echo $label->label_style; ?> <?php echo $label->label_position; ?> small-db" style="background-color: <?php echo '#' . $label->label_background_color; ?>; color: <?php echo '#' . $label->label_foreground_color; ?>; opacity: <?php echo $label->label_opacity; ?>;<?php if ($label->label_bold) echo 'font-weight: bold;'; ?>">
                                            <?php echo $label->label_name; ?>
                                        </span>
                                        <?php
                                    }
                                    if ($label->label_style == 'rotated' && !($label->enable_image && $label->label_image)) {
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <img class="img-responsive imgdisplaynone" src="<?php echo $this->item->thumb_image; ?>" title="<?php echo $this->item->product_page_title != '' ? $this->item->product_page_title : $this->item->product_name; ?>" alt="<?php echo $this->item->product_page_title != '' ? $this->item->product_page_title : $this->item->product_name; ?>" />
                    </a>
                </div>	
                <?php
                if (count($this->productImages)) {
                    ?>
                    <div class="image-additional">
                        <div>
                            <a class="zoomThumbActive" href="javascript:void(0);" rel="{gallery: 'product-thumbnails', smallimage: '<?php echo $this->item->thumb_image; ?>',largeimage: '<?php echo $this->item->popup_image; ?>'}">
                                <img src="<?php echo $this->item->small_thumb_image; ?>">
                            </a>
                        </div>
                        <?php
                        for ($i = 0; $n = count($this->productImages), $i < $n; $i++) {
                            ?>
                            <div>
                                <a href="javascript:void(0);" rel="{gallery: 'product-thumbnails', smallimage: '<?php echo $this->productImages[$i]->thumb_image; ?>',largeimage: '<?php echo $this->productImages[$i]->popup_image; ?>'}">
                                    <img src="<?php echo $this->productImages[$i]->small_thumb_image; ?>">
                                </a>
                            </div>	
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="image img-polaroid" id="main-image-area">
                    <a class="product-image" href="<?php echo $this->item->popup_image; ?>">
                        <?php
                        if (count($this->labels)) {
                            for ($i = 0; $n = count($this->labels), $i < $n; $i++) {
                                $label = $this->labels[$i];
                                if ($label->label_style == 'rotated' && !($label->enable_image && $label->label_image)) {
                                    ?>
                                    <div class="cut-rotated">
                                        <?php
                                    }
                                    if ($label->enable_image && $label->label_image) {
                                        $imageWidth = $label->label_image_width > 0 ? $label->label_image_width : OpenShopHelper::getConfigValue('label_image_width');
                                        if (!$imageWidth)
                                            $imageWidth = 50;
                                        $imageHeight = $label->label_image_height > 0 ? $label->label_image_height : OpenShopHelper::getConfigValue('label_image_height');
                                        if (!$imageHeight)
                                            $imageHeight = 50;
                                        ?>
                                        <span class="horizontal <?php echo $label->label_position; ?> small-db" style="opacity: <?php echo $label->label_opacity; ?>;<?php echo 'background-image: url(' . $label->label_image . ')'; ?>; background-repeat: no-repeat; width: <?php echo $imageWidth; ?>px; height: <?php echo $imageHeight; ?>px; box-shadow: none;"></span>
                                        <?php
                                    }
                                    else {
                                        ?>
                                        <span class="<?php echo $label->label_style; ?> <?php echo $label->label_position; ?> small-db" style="background-color: <?php echo '#' . $label->label_background_color; ?>; color: <?php echo '#' . $label->label_foreground_color; ?>; opacity: <?php echo $label->label_opacity; ?>;<?php if ($label->label_bold) echo 'font-weight: bold;'; ?>">
                                            <?php echo $label->label_name; ?>
                                        </span>
                                        <?php
                                    }
                                    if ($label->label_style == 'rotated' && !($label->enable_image && $label->label_image)) {
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <img src="<?php echo $this->item->thumb_image; ?>" title="<?php echo $this->item->product_page_title != '' ? $this->item->product_page_title : $this->item->product_name; ?>" alt="<?php echo $this->item->product_page_title != '' ? $this->item->product_page_title : $this->item->product_name; ?>" />
                    </a>
                </div>
                <?php
                if (count($this->productImages) > 0) {
                    ?>
                    <div class="image-additional">
                        <?php
                        for ($i = 0; $n = count($this->productImages), $i < $n; $i++) {
                            ?>
                            <div>
                                <a class="product-image" href="<?php echo $this->productImages[$i]->popup_image; ?>">
                                    <img src="<?php echo $this->productImages[$i]->small_thumb_image; ?>" />
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-sm-8 col-md-6">
            <div class="infoshortP">
                <h2><?php echo $this->item->product_name ?></h2>
                <div class="short-desc"> 
                    <?php
                    if (empty($this->item->product_short_desc)) {
                        echo 'Đang cập nhật...';
                    } else {
                        echo $this->item->product_short_desc;
                    }
                    ?> 
                </div>
                <?php
                if (OpenShopHelper::getConfigValue('show_manufacturer') || OpenShopHelper::getConfigValue('show_sku') || OpenShopHelper::getConfigValue('show_availability')) {
                    ?>
                    <div>
                        <div class="product-desc">
                            <?php
                            if (OpenShopHelper::getConfigValue('show_manufacturer')) {
                                ?>
                                <div class="product-manufacturer">
                                    <strong><?php echo JText::_('OPENSHOP_BRAND'); ?>:</strong>
                                    <span><?php echo isset($this->manufacturer->manufacturer_name) ? $this->manufacturer->manufacturer_name : ''; ?></span>
                                </div>
                                <?php
                            }
                            if (OpenShopHelper::getConfigValue('show_sku')) {
                                ?>
                                <div class="product-sku">
                                    <strong><?php echo JText::_('OPENSHOP_PRODUCT_CODE'); ?>:</strong>
                                    <span><?php echo $this->item->product_sku; ?></span>
                                </div>	
                                <?php
                            }
                            if (OpenShopHelper::getConfigValue('show_availability')) {
                                ?>
                                <div class="product-availability">
                                    <strong><?php echo JText::_('OPENSHOP_AVAILABILITY'); ?>:</strong>
                                    <span>
                                        <?php
                                        echo $this->item->availability;
                                        if (isset($this->product_available_date)) {
                                            echo ' (' . JText::_('OPENSHOP_PRODUCT_AVAILABLE_DATE') . ': ' . $this->product_available_date . ')';
                                        }
                                        ?>
                                    </span>
                                </div>
                                <?php
                            }
                            if (OpenShopHelper::getConfigValue('show_product_weight')) {
                                ?>
                                <div class="product-weight">
                                    <strong><?php echo JText::_('OPENSHOP_PRODUCT_WEIGHT'); ?>:</strong>
                                    <span><?php echo number_format($this->item->product_weight, 2) . OpenShopHelper::getWeightUnit($this->item->product_weight_id, JFactory::getLanguage()->getTag()); ?></span>
                                </div>
                                <?php
                            }
                            if (OpenShopHelper::getConfigValue('show_product_dimensions')) {
                                ?>
                                <div class="product-dimensions">
                                    <strong><?php echo JText::_('OPENSHOP_PRODUCT_DIMENSIONS'); ?>:</strong>
                                    <span><?php echo number_format($this->item->product_length, 2) . OpenShopHelper::getLengthUnit($this->item->product_length_id, JFactory::getLanguage()->getTag()) . ' x ' . number_format($this->item->product_width, 2) . OpenShopHelper::getLengthUnit($this->item->product_length_id, JFactory::getLanguage()->getTag()) . ' x ' . number_format($this->item->product_height, 2) . OpenShopHelper::getLengthUnit($this->item->product_length_id, JFactory::getLanguage()->getTag()); ?></span>
                                </div>
                                <?php
                            }
                            if (OpenShopHelper::getConfigValue('show_product_tags') && count($this->productTags)) {
                                ?>
                                <div class="product-tags">
                                    <strong><?php echo JText::_('OPENSHOP_PRODUCT_TAGS'); ?>:</strong>
                                    <span>
                                        <?php
                                        for ($i = 0; $n = count($this->productTags), $i < $n; $i++) {
                                            $tagName = trim($this->productTags[$i]->tag_name);
                                            $searchTagLink = JRoute::_('index.php?option=com_openshop&view=search&keyword=' . $tagName);
                                            ?>
                                            <a href="<?php echo $searchTagLink; ?>" title="<?php echo $tagName; ?>"><?php echo $tagName; ?></a>
                                            <?php
                                            if ($i < ($n - 1))
                                                echo ", ";
                                        }
                                        ?>
                                    </span>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>	
                    <?php
                }
                if (OpenShopHelper::showPrice() && !$this->item->product_call_for_price) {
                    ?>
                    <div style="padding: 0 0 10px 0;">
                        <div class="product-price" id="product-price">
                            <div class="col-md-6">
                                <?php
                                if (!empty($this->item->product_price_r)) {
                                    ?>
                                    <div class="percentGiam">
                                        <?php echo 100 - (int) ((int) ($this->item->product_price * 100) / (int) $this->item->product_price_r) . '%' ?>
                                    </div>
                                    <div class="priceBuy">
                                        <?php echo number_format((int) $this->item->product_price, 0, ',', '.') ?><sup>đ</sup>
                                    </div>
                                    <div class="priceBuyBo">
                                        <?php echo number_format((int) $this->item->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="priceJ1">
                                        <?php echo number_format((int) $this->item->product_price, 0, ',', '.') ?>
                                        <sup>đ</sup>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <div class="showSKU">
                                    Mã SP: <?php echo $this->item->product_sku ?>
                                </div>
                            </div>
                            <h2>
    <!--                                <strong>
                                <?php echo JText::_('OPENSHOP_PRICE'); ?>:
                                <?php
                                $productPriceArray = OpenShopHelper::getProductPriceArray($this->item->id, $this->item->product_price);
                                if ($productPriceArray['salePrice']) {
                                    ?>
                                                                                                                            <span class="openshop-base-price"><?php echo $this->currency->format($this->tax->calculate($productPriceArray['basePrice'], $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))); ?></span>&nbsp;
                                                                                                                            <span class="openshop-sale-price"><?php echo $this->currency->format($this->tax->calculate($productPriceArray['salePrice'], $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))); ?></span>
                                    <?php
                                } else {
                                    ?>
                                                                                                                            <span class="price"><?php echo $this->currency->format($this->tax->calculate($productPriceArray['basePrice'], $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))); ?></span>
                                    <?php
                                }
                                ?>
                                </strong>-->
                                <br />
                                <?php
                                if (OpenShopHelper::getConfigValue('tax') && OpenShopHelper::getConfigValue('display_ex_tax')) {
                                    ?>
                                    <small>
                                        <?php echo JText::_('OPENSHOP_EX_TAX'); ?>:
                                        <?php
                                        if ($productPriceArray['salePrice']) {
                                            echo $this->currency->format($productPriceArray['salePrice']);
                                        } else {
                                            echo $this->currency->format($productPriceArray['basePrice']);
                                        }
                                        ?>
                                    </small>
                                    <?php
                                }
                                ?>
                            </h2>
                        </div>
                    </div>
                    <?php
                    if (count($this->discountPrices)) {
                        ?>
                        <div>
                            <div class="product-discount-price">
                                <?php
                                for ($i = 0; $n = count($this->discountPrices), $i < $n; $i++) {
                                    $discountPrices = $this->discountPrices[$i];
                                    echo $discountPrices->quantity . ' ' . JText::_('OPENSHOP_OR_MORE') . ' ' . $this->currency->format($this->tax->calculate($discountPrices->price, $this->item->product_taxclass_id, OpenShopHelper::getConfigValue('tax'))) . '<br />';
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                if ($this->item->product_call_for_price) {
                    ?>
                    <div>
                        <div class="product-price">
                            <?php echo JText::_('OPENSHOP_CALL_FOR_PRICE'); ?>: <?php echo OpenShopHelper::getConfigValue('telephone'); ?>
                        </div>
                    </div>
                    <?php
                }
                if (count($this->productOptions)) {
                    ?>
                    <div style="padding: 30px 0 0 0;">
                        <div class="product-options table-responsive">
                            <span style="font-size: 20px;">Chọn số lượng </span> <i class="descMobile"> - Kéo qua bên trái để chọn số lượng</i>
                            <table class="tbl-order-number table-hover table">
                                <?php
                                foreach ($this->getSizeColor($this->item->id) as $size) {
                                    ?>
                                    <tr>
                                        <td>
                                            <img class="lazy" data-original="<?php echo $this->item->thumb_image; ?>" width="45px" title="<?php echo $this->item->product_name ?>" alt="<?php echo $this->item->product_name ?>"/>
                                        </td>
                                        <td>
                                            <h5><?php echo $this->item->product_name ?></h5>
                                            <div><?php echo 'Size: ' . $size->value ?></div>
                                        </td>
                                        <td>
                                            <select style="width: 50px;" name="slt_quantity" id="slt_quantity" onchange="changeSizeProduct(<?php echo $size->optionvalue_id ?>, this.value)">
                                                <?php
                                                for ($i = 0; $i <= 5; $i++) {
                                                    ?>
                                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="row-fluid">
                    <div class="product-cart clearfix">
                        <?php
                        if (OpenShopHelper::isCartMode($this->item) || OpenShopHelper::isQuoteMode($this->item)) {
                            ?>
                            <div class="col-md-6 no_margin_left" style="padding: 15px 0px;">
                                <input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
                                <?php
                                if (OpenShopHelper::getConfigValue('show_quantity_box_in_product_page')) {
                                    ?>
                                    <div class="input-append input-prepend">
                                        <label class="btn"><?php echo JText::_('OPENSHOP_QTY'); ?>:</label>
                                        <span class="openshop-quantity">
                                            <a class="btn btn-default button-minus spin-down" id="<?php echo $this->item->id; ?>" data="down">-</a>
                                            <input type="text" class="openshop-quantity-value" id="quantity_<?php echo $this->item->id; ?>" name="quantity" value="1" />
                                            <a class="btn btn-default button-plus spin-up" id="<?php echo $this->item->id; ?>" data="up">+</a>
                                        </span>
                                    </div>
                                    <?php
                                }
                                //Nút đặt hang
                                if (OpenShopHelper::isCartMode($this->item)) {
                                    ?>
                                    <div class="btn-order-Nelo" onclick="showCart()">
                                        <?php echo JText::_('OPENSHOP_ADD_TO_CART'); ?>
                                    </div>
                                    <?php
                                }
                                if (OpenShopHelper::isQuoteMode($this->item)) {
                                    ?>
                                    <button id="add-to-quote" class="btn btn-primary" type="button"><?php echo JText::_('OPENSHOP_ADD_TO_QUOTE'); ?></button>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        if (OpenShopHelper::getConfigValue('allow_wishlist') || OpenShopHelper::getConfigValue('allow_compare') || OpenShopHelper::getConfigValue('allow_ask_question')) {
                            ?>
                            <div class="span6">
                                <?php
                                if (OpenShopHelper::getConfigValue('allow_wishlist')) {
                                    ?>
                                    <p><a style="cursor: pointer;" onclick="addToWishList(<?php echo $this->item->id; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>')"><?php echo JText::_('OPENSHOP_ADD_TO_WISH_LIST'); ?></a></p>
                                    <?php
                                }
                                if (OpenShopHelper::getConfigValue('allow_compare')) {
                                    ?>
                                    <p><a style="cursor: pointer;" onclick="addToCompare(<?php echo $this->item->id; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>')"><?php echo JText::_('OPENSHOP_ADD_TO_COMPARE'); ?></a></p>
                                    <?php
                                }
                                if (OpenShopHelper::getConfigValue('allow_ask_question')) {
                                    ?>
                                    <p><a style="cursor: pointer;" onclick="askQuestion(<?php echo $this->item->id; ?>, '<?php echo OpenShopHelper::getSiteUrl(); ?>', '<?php echo OpenShopHelper::getAttachedLangLink(); ?>')"><?php echo JText::_('OPENSHOP_ASK_QUESTION'); ?></a></p>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
                if (OpenShopHelper::getConfigValue('allow_reviews')) {
                    ?>
                    <div>
                        <div class="product-review">
                            <p>
                                <img src="components/com_openshop/assets/images/stars-<?php echo round(OpenShopHelper::getProductRating($this->item->id)); ?>.png" />
                                <a onclick="activeReviewsTab();" style="cursor: pointer;"><?php echo count($this->productReviews) . ' ' . JText::_('OPENSHOP_REVIEWS'); ?></a> | <a onclick="activeReviewsTab();" style="cursor: pointer;"><?php echo JText::_('OPENSHOP_WRITE_A_REVIEW'); ?></a>
                            </p>
                        </div>
                    </div>	
                    <?php
                }
                if (OpenShopHelper::getConfigValue('social_enable')) {
                    ?>
                    <div>
                        <div class="product-share">
                            <div class="ps_area clearfix">
                                <?php
                                if (OpenShopHelper::getConfigValue('show_facebook_button')) {
                                    ?>
                                    <div class="ps_facebook_like">
                                        <div class="fb-like" data-send="true" data-width="<?php echo OpenShopHelper::getConfigValue('button_width', 450); ?>" data-show-faces="<?php echo OpenShopHelper::getConfigValue('show_faces', 1); ?>" vdata-font="<?php echo OpenShopHelper::getConfigValue('button_font', 'arial'); ?>" data-colorscheme="<?php echo OpenShopHelper::getConfigValue('button_theme', 'light'); ?>" layout="<?php echo OpenShopHelper::getConfigValue('button_layout', 'button_count'); ?>"></div>
                                    </div>
                                    <?php
                                }
                                if (OpenShopHelper::getConfigValue('show_twitter_button')) {
                                    ?>
                                    <div class="ps_twitter">
                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $uri->toString(); ?>" tw:via="ontwiik" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="horizontal">Tweet</a>
                                    </div>
                                    <?php
                                }
                                if (OpenShopHelper::getConfigValue('show_pinit_button')) {
                                    ?>
                                    <div class="ps_pinit">
                                        <a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode($uri->toString()); ?>&images=<?php echo urlencode(OpenShopHelper::getSiteUrl() . $this->item->thumb_image); ?>&description=<?php echo $this->item->product_name; ?>" count-layout="horizontal" class="pin-it-button">Pin It</a>
                                    </div>
                                    <?php
                                }
                                if (OpenShopHelper::getConfigValue('show_linkedin_button')) {
                                    ?>
                                    <div class="ps_linkedin">
                                        <?php
                                        if (OpenShopHelper::getConfigValue('linkedin_layout', 'right') == 'no-count') {
                                            ?>
                                            <script type="IN/Share"></script>
                                            <?php
                                        } else {
                                            ?>
                                            <script type="IN/Share" data-counter="<?php echo OpenShopHelper::getConfigValue('linkedin_layout', 'right'); ?>"></script>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                                if (OpenShopHelper::getConfigValue('show_google_button')) {
                                    ?>
                                    <div class="ps_google">
                                        <div class="g-plusone"></div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row detailBorder">
        <div class="col-sm-12 col-md-9 colDetailPro">
            <div class="titleDetail">Thông tin chi tiết</div>
            <p><?php echo $this->item->product_desc; ?></p>
            <div class="tabbable">
                <!--                <ul class="nav nav-tabs" id="productTab">
                                    <li class="active"><a href="#description" data-toggle="tab"><?php echo JText::_('OPENSHOP_DESCRIPTION'); ?></a></li>
                <?php
                if (OpenShopHelper::getConfigValue('show_specification') && $this->hasSpecification) {
                    ?>
                                                                            <li><a href="#specification" data-toggle="tab"><?php echo JText::_('OPENSHOP_SPECIFICATION'); ?></a></li>
                    <?php
                }
                if (OpenShopHelper::getConfigValue('allow_reviews')) {
                    ?>
                                                                            <li><a href="#reviews" data-toggle="tab"><?php echo JText::_('OPENSHOP_REVIEWS') . ' (' . count($this->productReviews) . ')'; ?></a></li>
                    <?php
                }
                if (OpenShopHelper::getConfigValue('show_related_products') && count($this->productRelations)) {
                    ?>
                                                                            <li><a href="#related-products" data-toggle="tab"><?php echo JText::_('OPENSHOP_RELATED_PRODUCTS'); ?></a></li>	
                    <?php
                }
                ?>
                                </ul>-->
                <div class="tab-content">
                    <?php
                    if (OpenShopHelper::getConfigValue('show_specification') && $this->hasSpecification) {
                        ?>
                        <div class="tab-pane" id="specification">
                            <table class="table table-bordered">
                                <?php
                                for ($i = 0; $n = count($this->attributeGroups), $i < $n; $i++) {
                                    if (count($this->productAttributes[$i])) {
                                        ?>
                                        <thead>
                                            <tr>
                                                <th colspan="2"><?php echo $this->attributeGroups[$i]->attributegroup_name; ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($j = 0; $m = count($this->productAttributes[$i]), $j < $m; $j++) {
                                                ?>
                                                <tr>
                                                    <td width="30%"><?php echo $this->productAttributes[$i][$j]->attribute_name; ?></td>
                                                    <td width="70%"><?php echo $this->productAttributes[$i][$j]->value; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <?php
                    }
                    if (OpenShopHelper::getConfigValue('allow_reviews')) {
                        ?>
                        <div class="tab-pane" id="reviews">
                            <?php
                            if (count($this->productReviews) > 5) {
                                ?>
                                <div class="span12 pagination pagination-toolbar" style="text-align: right; margin-top: 20px;"> 
                                    <ul class="review-pagination-list"></ul>
                                </div>
                                <?php
                            }
                            ?>
                            <div id="wrap-review">
                                <?php
                                if (count($this->productReviews)) {
                                    foreach ($this->productReviews as $review) {
                                        ?>
                                        <div class="review-list">
                                            <div class="author"><b><?php echo $review->author; ?></b> <?php echo JText::_('OPENSHOP_REVIEW_ON'); ?> <?php echo JHtml::date($review->created_date, OpenShopHelper::getConfigValue('date_format', 'm-d-Y') . ' h:i A'); ?></div>
                                            <div class="rating"><img src="components/com_openshop/assets/images/stars-<?php echo $review->rating . '.png'; ?>" alt="" /></div>
                                            <div class="text"><?php echo nl2br($review->review); ?></div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="no-content"><?php echo JText::_('OPENSHOP_NO_PRODUCT_REVIEWS'); ?></div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row-fluid form-horizontal" id="review-form">
                                <legend id="review-title"><?php echo JText::_('OPENSHOP_WRITE_A_REVIEW'); ?></legend>
                                <div class="control-group">
                                    <label class="control-label" for="author"><span class="required">*</span><?php echo JText::_('OPENSHOP_YOUR_NAME'); ?>:</label>
                                    <div class="controls docs-input-sizes">
                                        <input type="text" class="input-large" name="author" id="author" value="" />
                                    </div>
                                </div>
                                <div class="control-group">	
                                    <label class="control-label" for="author"><span class="required">*</span><?php echo JText::_('OPENSHOP_YOUR_REVIEW'); ?>:</label>
                                    <div class="controls docs-input-sizes">
                                        <textarea rows="5" cols="40" name="review"></textarea>
                                    </div>
                                </div>
                                <div class="control-group">	
                                    <label class="control-label" for="author"><span class="required">*</span><?php echo JText::_('OPENSHOP_RATING'); ?>:</label>
                                    <div class="controls docs-input-sizes">
                                        <?php echo $this->ratingHtml; ?>
                                    </div>
                                </div>	
                                <?php
                                if ($this->showCaptcha) {
                                    ?>
                                    <div class="control-group">
                                        <label class="control-label" for="recaptcha_response_field">
                                            <?php echo JText::_('OPENSHOP_CAPTCHA'); ?><span class="required">*</span>
                                        </label>
                                        <div class="controls docs-input-sizes">
                                            <?php echo $this->captcha; ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <input type="button" class="btn btn-primary pull-left" id="button-review" value="<?php echo JText::_('OPENSHOP_SUBMIT'); ?>" />
                                <input type="hidden" name="product_id" value="<?php echo $this->item->id; ?>" />
                            </div>
                            <?php
                            if (OpenShopHelper::getConfigValue('show_facebook_comment')) {
                                ?>
                                <div class="row-fluild">
                                    <legend id="review-title"><?php echo JText::_('OPENSHOP_FACEBOOK_COMMENT'); ?></legend>
                                    <div class="fb-comments" data-num-posts="<?php echo OpenShopHelper::getConfigValue('num_posts', 10); ?>" data-width="<?php echo OpenShopHelper::getConfigValue('comment_width', 400); ?>" data-href="<?php echo $uri->toString(); ?>"></div>
                                </div>	
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 relateP">
            <div class="titleDetail">Sản phẩm liên quan</div>
            <?php
            if (OpenShopHelper::getConfigValue('show_related_products') && count($this->productRelations)) {
                ?>
                <div class="tab-pane" id="related-products">
                    <div class="related_products row-fluid">
                        <?php
                        for ($i = 0; $n = count($this->productRelations), $i < $n; $i++) {
                            $productRelation = $this->productRelations[$i];
                            ?>
                            <div class="relatePDetail">
                                <div class="image img-polaroid">
                                    <a href="<?php echo JRoute::_(OpenShopRoute::getProductRoute($productRelation->id, OpenShopHelper::getProductCategory($productRelation->id))); ?>">
                                        <img class="lazy img-responsive" data-original="<?php echo $productRelation->thumb_image; ?>" width="100%" alt="<?php echo $productRelation->product_name; ?>" title="<?php echo $productRelation->product_name; ?>"/>
                                    </a>
                                </div>
                                <div class="name">
                                    <a href="<?php echo JRoute::_(OpenShopRoute::getProductRoute($productRelation->id, OpenShopHelper::getProductCategory($productRelation->id))); ?>">
                                        <h5><?php echo $productRelation->product_name; ?></h5>
                                    </a>
                                    <?php
                                    if (OpenShopHelper::showPrice() && !$productRelation->product_call_for_price) {
                                        ?>
                                        <?php
                                        if ($productRelation->product_price_r) {
                                            ?>
                                            <div class="pricePHnotSaleR">
                                                <div class="priceold2">
                                                    <?php echo number_format((int) $productRelation->product_price_r, '0', ',', '.') ?><sup>đ</sup>
                                                </div>
                                                <div class="pricebuy2"><?php echo number_format((int) $productRelation->product_price, '0', ',', '.') ?><sup>đ</sup></div>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="pricePHnotSale">
                                                <div class="pricebuy2"><?php echo number_format((int) $productRelation->product_price, '0', ',', '.') ?><sup>đ</sup></div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            if ($i > 0 && ($i + 1) % 3 == 0) {
                                ?>
                            </div><div class="related_products row-fluid">
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<form action="/gio-hang.html" method="post" id="formCart">
    <input type="hidden" id="IDProduct" name="IDProduct" value="<?php echo $this->item->id ?>"/>
    <input type="hidden" id="quantityProduct" name="quantityProduct" value=""/>
</form>

<input type="hidden" name="review-tab" id="review-tab" value="0" />

<script>
    if (jQuery(window).width() < 1024) {
        jQuery('.colDetailPro').addClass('col-md-12');
        jQuery('.colDetailPro').removeClass('col-md-9');

        jQuery('.relateP').addClass('col-md-12');
        jQuery('.relateP').removeClass('col-md-3');

        jQuery('.relatePDetail').addClass('col-sm-4 col-md-4');
        jQuery('.relatePDetail').css('background', 'none');
        jQuery('.relatePDetail .name').css('background', 'white');
        jQuery('.relatePDetail .name').css('height', '125px');
    } else
    {
        jQuery('.colDetailPro').addClass('col-md-9');
        jQuery('.colDetailPro').removeClass('col-md-12');

        jQuery('.relateP').addClass('col-md-3');
        jQuery('.relateP').removeClass('col-md-12');
    }

    jQuery(document).ready(function () {
        jQuery('.colDetailPro img').addClass('img-responsive');
    });
</script>