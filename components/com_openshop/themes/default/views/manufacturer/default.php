<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<h1><?php echo $this->manufacturer->manufacturer_page_heading != '' ? $this->manufacturer->manufacturer_page_heading : $this->manufacturer->manufacturer_name; ?></h1>
<div class="row-fluid">
	<div class="span4">
		<img src="<?php echo $this->manufacturer->image; ?>" title="<?php echo $this->manufacturer->manufacturer_page_title != '' ? $this->manufacturer->manufacturer_page_title : $this->manufacturer->manufacturer_name; ?>" alt="<?php echo $this->manufacturer->manufacturer_page_title != '' ? $this->manufacturer->manufacturer_page_title : $this->manufacturer->manufacturer_name; ?>" />
	</div>
	<div class="span8"><?php echo $this->manufacturer->manufacturer_desc; ?></div>		
</div>
<?php
if (count($this->products))
{
	?>
	<div class="openshop-products-list">
		<?php
		echo OpenShopHtmlHelper::loadCommonLayout ('common/products.php', array (
			'products' => $this->products,
			'pagination' => $this->pagination,
			'sort_options' => $this->sort_options,
			'tax' => $this->tax,
			'currency' => $this->currency,
			'productsPerRow' => $this->productsPerRow,
			'catId' => 0,
			'actionUrl' => $this->actionUrl,
			'showSortOptions' => true
		));
		?>
	</div>
	<?php
}