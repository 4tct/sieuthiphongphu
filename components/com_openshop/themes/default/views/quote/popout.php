<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/colorbox/jquery.colorbox.js" type="text/javascript"></script>
<script src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/js/openshop.js" type="text/javascript"></script>
<h1><?php echo JText::_('OPENSHOP_QUOTE_CART'); ?></h1>
<?php
if (isset($this->success))
{
	?>
	<div class="success"><?php echo $this->success; ?></div>
	<?php
}
?>
<?php
if (!count($this->quoteData))
{
	?>
	<div class="no-content"><?php echo JText::_('OPENSHOP_QUOTE_EMPTY'); ?></div>
	<?php
}
else
{
	?>
	<div class="quote-info">
		<?php
		if(!OpenShopHelper::isMobile())
		{
			?>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style="text-align: center;"><?php echo JText::_('OPENSHOP_REMOVE'); ?></th>
							<th style="text-align: center;"><?php echo JText::_('OPENSHOP_IMAGE'); ?></th>
						<th><?php echo JText::_('OPENSHOP_PRODUCT_NAME'); ?></th>
						<th><?php echo JText::_('OPENSHOP_QUANTITY'); ?></th>
						<?php
						if (OpenShopHelper::showPrice())
						{
							?>
							<th nowrap="nowrap"><?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?></th>
							<th><?php echo JText::_('OPENSHOP_TOTAL'); ?></th>
							<?php
						}
						?>
					</tr>
				</thead>
				<tbody>
					<?php
					$totalPrice = 0;
					$countProducts = 0;
					foreach ($this->quoteData as $key => $product)
					{
						$countProducts++;
						$optionData = $product['option_data'];
						$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
						if (OpenShopHelper::showPrice() && !$product['product_call_for_price'])
						{
							$totalPrice += $product['total_price'];
						}
						?>
						<tr>
							<td class="openshop-center-text" style="vertical-align: middle;">
								<a class="openshop-remove-item-quote" id="<?php echo $key; ?>" style="cursor: pointer;">
									<img alt="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" title="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/images/remove.png" />
								</a>
							</td>
							<td class="muted openshop-center-text" style="vertical-align: middle;">
								<a href="<?php echo $viewProductUrl; ?>">
									<img class="img-polaroid" src="<?php echo $product['image']; ?>" />
								</a>
							</td>
							<td style="vertical-align: middle;">
								<a href="<?php echo $viewProductUrl; ?>">
									<?php echo $product['product_name']; ?>
								</a>
								<br />	
								<?php
								for ($i = 0; $n = count($optionData), $i < $n; $i++)
								{
									echo '- ' . $optionData[$i]['option_name'] . ': ' . $optionData[$i]['option_value'] . '<br />';
								}
								?>
							</td>
							<td style="vertical-align: middle;">
								<div class="input-append input-prepend">
									<span class="openshop-quantity">
										<input type="hidden" name="key[]" value="<?php echo $key; ?>" />
										<a class="btn btn-default button-plus" id="popout_<?php echo $countProducts; ?>" data="up">+</a>
											<input type="text" class="openshop-quantity-value" value="<?php echo htmlspecialchars($product['quantity'], ENT_COMPAT, 'UTF-8'); ?>" name="quantity[]" id="quantity_popout_<?php echo $countProducts; ?>" />
										<a class="btn btn-default button-minus" id="popout_<?php echo $countProducts; ?>" data="down">-</a>
									</span>
								</div>
							</td>
							<?php
							if (OpenShopHelper::showPrice())
							{
								?>
								<td style="vertical-align: middle;">
									<?php
									if (!$product['product_call_for_price'])
									{
										echo $this->currency->format($product['price']);
									}
									?>
								</td>
								<td style="vertical-align: middle;">
									<?php
									if (!$product['product_call_for_price'])
									{
										echo $this->currency->format($product['total_price']);
									}	
									?>
								</td>
								<?php
							}
							?>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
			<?php
			if (OpenShopHelper::showPrice())
			{
				?>
				<div class="totals text-center" style="text-align: center">
					<div>
						<?php echo JText::_('OPENSHOP_TOTAL'); ?>:
						<strong><?php echo $this->currency->format($totalPrice); ?></strong>
					</div>
				</div>
				<?php
			}
		}
		else
		{
			?>
			<div class="row-fluid">
				<?php
				$totalPrice = 0;
				foreach ($this->quoteData as $key => $product)
				{
					if (OpenShopHelper::showPrice() && !$product['product_call_for_price'])
					{
						$totalPrice += $product['total_price'];
					}
					$optionData = $product['option_data'];
					$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
					?>
					<div class="well clearfix">
						<div class="row-fluid">
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_REMOVE'); ?>: </strong>
								<a class="openshop-remove-item-quote" id="<?php echo $key; ?>" style="cursor: pointer;">
									<img alt="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" title="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/images/remove.png" />
								</a>
							</div>
							<div class="span2 openshop-center-text">
								<a href="<?php echo $viewProductUrl; ?>">
									<img class="img-polaroid" src="<?php echo $product['image']; ?>" />
								</a>
							</div>
							<div class="span2">
								<h5 class="openshop-center-text">
									<a href="<?php echo $viewProductUrl; ?>">
										<?php echo $product['product_name']; ?>
									</a>
								</h5>
								<?php
								if (count($optionData))
								{
									?>
									<br />
									<?php
								}
								for ($i = 0; $n = count($optionData), $i < $n; $i++)
								{
									echo '- ' . $optionData[$i]['option_name'] . ': ' . $optionData[$i]['option_value'] . '<br />';
								}
								?>
							</div>
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_MODEL'); ?>: </strong>
								<?php echo $product['product_sku']; ?>
							</div>
							<div class="span2">
								<strong><?php echo JText::_('OPENSHOP_QUANTITY'); ?></strong>
								<div class="input-append input-prepend">
									<span class="openshop-quantity">
										<input type="hidden" name="key[]" value="<?php echo $key; ?>" />
										<a class="btn btn-default button-plus" id="popout_<?php echo $countProducts; ?>" data="up">+</a>
											<input type="text" class="openshop-quantity-value" value="<?php echo htmlspecialchars($product['quantity'], ENT_COMPAT, 'UTF-8'); ?>" name="quantity[]" id="quantity_popout_<?php echo $countProducts; ?>" />
										<a class="btn btn-default button-minus" id="popout_<?php echo $countProducts; ?>" data="down">-</a>
									</span>
								</div>
							</div>
							<?php
							if (OpenShopHelper::showPrice() && !$product['product_call_for_price'])
							{
								?>
								<div class="span2">
									<strong><?php echo JText::_('OPENSHOP_UNIT_PRICE'); ?>: </strong>
									<?php echo $this->currency->format($product['price']); ?>
								</div>
								<div class="span2">
									<strong><?php echo JText::_('OPENSHOP_TOTAL'); ?>: </strong>
									<?php echo $this->currency->format($product['total_price']); ?>
								</div>
								<?php
							}
							?>
						</div>
					</div>
					<?php
				}
				if (OpenShopHelper::showPrice())
				{
					?>
					<div class="well clearfix">
						<?php echo JText::_('OPENSHOP_TOTAL'); ?>: <strong><?php echo $this->currency->format($totalPrice); ?></strong>
					</div>
					<?php
				}
				?>
			</div>
		<?php
	    }
		?>
	</div>
	<div class="control-group" style="text-align: center;">
		<div class="controls">
			<a class="btn btn-danger" href="<?php echo JRoute::_(OpenShopHelper::getContinueShopingUrl()); ?>"><?php echo JText::_('OPENSHOP_CONTINUE_SHOPPING'); ?></a>
			<button type="button" class="btn btn-info" onclick="updateQuote();" id="update-quote"><?php echo JText::_('OPENSHOP_UPDATE_QUOTE'); ?></button>
			<a class="btn btn-success" href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('quote')); ?>"><?php echo JText::_('OPENSHOP_QUOTE_FORM'); ?></a>
		</div>
	</div>
	<script type="text/javascript">
		//Function to update quote
		function updateQuote(key)
		{
			OpenShop.jQuery(function($){
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
				$.ajax({
					type: 'POST',
					url: siteUrl + 'index.php?option=com_openshop&task=quote.updates<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
					data: $('.quote-info input[type=\'text\'], .quote-info input[type=\'hidden\']'),
					beforeSend: function() {
						$('#update-quote').attr('disabled', true);
						$('#update-quote').after('<span class="wait">&nbsp;<img src="components/com_openshop/assets/images/loading.gif" alt="" /></span>');
					},
					complete: function() {
						$('#update-quote').attr('disabled', false);
						$('.wait').remove();
					},
					success: function() {
						$.ajax({
							url: siteUrl + 'index.php?option=com_openshop&view=quote&layout=popout&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
							dataType: 'html',
							success: function(html) {
								$.colorbox({
									overlayClose: true,
									opacity: 0.5,
									href: false,
									html: html
								});
								$.ajax({
									url: siteUrl + 'index.php?option=com_openshop&view=quote&layout=mini&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
									dataType: 'html',
									success: function(html) {
										jQuery('#openshop-quote').html(html);
										jQuery('.openshop-content').hide();
									},
									error: function(xhr, ajaxOptions, thrownError) {
										alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
									}
								});
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
				  	}
				});
			})
		}

		OpenShop.jQuery(function($) {
			//Ajax remove quote item
			$('.openshop-remove-item-quote').bind('click', function() {
				var aTag = $(this);
				var id = aTag.attr('id');
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';
				$.ajax({
					type :'POST',
					url: siteUrl + 'index.php?option=com_openshop&task=quote.remove&key=' +  id + '&redirect=1<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
					beforeSend: function() {
						aTag.attr('disabled', true);
						aTag.after('<span class="wait">&nbsp;<img src="components/com_openshop/assets/images/loading.gif" alt="" /></span>');
					},
					complete: function() {
						aTag.attr('disabled', false);
						$('.wait').remove();
					},
					success : function() {
						$.ajax({
							url: siteUrl + 'index.php?option=com_openshop&view=quote&layout=popout&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
							dataType: 'html',
							success: function(html) {
								$.colorbox({
									overlayClose: true,
									opacity: 0.5,
									href: false,
									html: html
								});
								$.ajax({
									url: siteUrl + 'index.php?option=com_openshop&view=quote&layout=mini&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
									dataType: 'html',
									success: function(html) {
										jQuery('#openshop-quote').html(html);
										jQuery('.openshop-content').hide();
									},
									error: function(xhr, ajaxOptions, thrownError) {
										alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
									}
								});
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			});
		});
	</script>
	<?php
}