<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="openshop-items">
	<h4><?php echo JText::_('OPENSHOP_QUOTE_CART'); ?></h4>
	<a>
		<span id="openshop-quote-total">
			<?php echo $this->countProducts; ?>&nbsp;<?php echo JText::_('OPENSHOP_ITEMS'); ?>
		</span>
	</a>
</div>
<div class="openshop-content">
<?php
	if ($this->countProducts == 0)
	{
		echo JText::_('OPENSHOP_QUOTE_EMPTY');
	}
	else
	{
	?>
	<div class="openshop-mini-quote-info">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="5" style="border: 0px;"><span class="wait"></span></td>
			</tr>
			<?php
			foreach ($this->items as $key => $product)
			{
				$optionData = $product['option_data'];
				$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product['product_id'], OpenShopHelper::getProductCategory($product['product_id'])));
				?>
				<tr>
					<td class="openshop-image">
						<a href="<?php echo $viewProductUrl; ?>">
							<img src="<?php echo $product['image']; ?>" />
						</a>
					</td>
					<td class="openshop-name">
						<a href="<?php echo $viewProductUrl; ?>">
							<?php echo $product['product_name']; ?>
						</a>
						<div>
						<?php
						for ($i = 0; $n = count($optionData), $i < $n; $i++)
						{
							echo '<small>- ' . $optionData[$i]['option_name'] . ': ' . $optionData[$i]['option_value'] . '</small><br />';
						}
						?>
						</div>
					</td>
					<td class="openshop-quantity">
						x&nbsp;<?php echo $product['quantity']; ?>
					</td>
					<td class="openshop-remove">
						<a class="openshop-remove-item" href="#" id="<?php echo $key; ?>">
							<img alt="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" title="<?php echo JText::_('OPENSHOP_REMOVE'); ?>" src="<?php echo JUri::base(true); ?>/components/com_openshop/assets/images/remove.png" />
						</a>
					</td>
				</tr>
			<?php
			}
			?>
		</table>
	</div>
	<div class="checkout">
		<a href="<?php echo JRoute::_(OpenShopRoute::getViewRoute('quote')); ?>"><?php echo JText::_('OPENSHOP_VIEW_QUOTE'); ?></a>
	</div>
	<?php
	}
	?>
</div>
<script type="text/javascript">
	OpenShop.jQuery(function($) {
		$(document).ready(function() {
			$('.openshop-items a').click(function() {
				$('.openshop-content').slideToggle('fast');
			});
			$('.openshop-content').mouseleave(function() {
				$('.openshop-content').hide();
			});
			//Ajax remove quote item
			$('.openshop-remove-item').bind('click', function() {
				var id = $(this).attr('id');
				var siteUrl = '<?php echo OpenShopHelper::getSiteUrl(); ?>';				
				$.ajax({
					type :'POST',
					url: siteUrl + 'index.php?option=com_openshop&task=quote.remove&key=' +  id + '&redirect=0<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
					beforeSend: function() {
						$('.wait').html('<img src="components/com_openshop/assets/images/loading.gif" alt="" />');
					},
					success : function() {
						$.ajax({
							url: siteUrl + 'index.php?option=com_openshop&view=quote&layout=mini&format=raw<?php echo OpenShopHelper::getAttachedLangLink(); ?>',
							dataType: 'html',
							success: function(html) {
								$('#openshop-quote').html(html);
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			});
		});
	});
</script>