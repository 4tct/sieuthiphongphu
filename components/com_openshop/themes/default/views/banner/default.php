<?php
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript($this->baseurl . '/components/com_openshop/assets/js/jquery.countdown.min.js');

$app = JFactory::getApplication()->input;
$id = $app->getInt('id');
//print_r($this->banner);
date_default_timezone_set('Asia/Ho_Chi_Minh');
?>
<div class="row padding0">
    <div class="col-md-12 padding0 productBanner">
        <?php
        if (empty($this->products)) {
            ?>
            <div class="messageNotRun">
                <i>Hiện chương trình chưa mở.</i>
                <br>
                <b>Vui lòng quay lại sau</b>
            </div>
            <?php
        } else {
            $timefinish = $this->banner->time_finish . ' 23:59:59';
            $timestart = $this->banner->time_start . ' 00:00:00';

            $timecur = date('Y-m-d H:m:s');

//            echo $timecur;
            //ngày hiện tại < ngày bắt đầu chương trình
            if (strtotime($timestart) > strtotime($timecur)) {
                ?>
                <div class="messageNotRun">
                    <div class="countdown">
                        <i>Hiện chương trình sẽ mở sau <span id="clock" style="font-size: 30px;"></span>.</i>

                    </div>
                </div>

                <script>
                    //                    $('#clock').countdown('<?php echo date('Y/m/d H:m:s', strtotime($timestart)) ?>')
                    //                            .on('update.countdown', function (event) {
                    //                                var format = '%H:%M:%S';
                    //                                if (event.offset.totalDays > 0) {
                    //                                    format = '%-d ngày%!d ' + format;
                    //                                }
                    //                                if (event.offset.weeks > 0) {
                    //                                    format = '%-w tuần%!w ' + format;
                    //                                }
                    //                                $(this).html(event.strftime(format));
                    //                            })
                    //                            .on('finish.countdown', function (event) {
                    //                                window.location.reload();
                    //                            });
                </script>
                <?php
            }

            //ngày hiện tại > ngày kết thúc chương trình => chương trình kết thúc
            else if (strtotime($timefinish) < strtotime($timecur)) {
                ?>
                <div class="messageNotRun">
                    <i>Hiện chương trình đã kết thúc.</i>
                </div>
                <?php
            }

            //đang chạy chương trình
            else {
                ?>
                <div class="messageRun">
                    <div class="img_banner positionRelative">
                        <!--<img class="img-responsive" src="<?php echo OPENSHOP_PATH_BANNERS_HTTP . $this->banner->banner_image ?>" alt="<?php echo $this->banner->title ?>" title="<?php echo $this->banner->title ?>" />-->
                    </div>

                    <div class="col-md-12 titleBanner">
                        <h2>
                            <?php echo $this->banner->title ?>
                        </h2>
                        <div>
                            <?php echo $this->banner->description ?>
                        </div>
                    </div>

                    <div class="contentProductBanner col-md-12 padding0">
                        <?php
                        foreach ($this->products as $pro) {
                            $link = JRoute::_(OpenShopRoute::getProductRoute($pro->id, OpenShopHelper::getProductCategory($pro->id)));
                            ?>
                            <div class="col-md-3">
                                <div class="borderImgBanner">
                                    <div class="imgPro positionRelative">
                                        <a href="<?php echo $link ?>">
                                            <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pro->product_image ?>" alt="<?php echo $pro->product_name ?>" title="<?php echo $pro->product_name ?>" data-toggle="tooltip" data-placement="top"/>
                                        </a>
                                        <?php
                                        if ($pro->product_price_r > 0) {
                                            $percent = 100 - (int) (((int) $pro->product_price * 100) / $pro->product_price_r)
                                            ?>
                                            <div class="percentSale top20"> 
                                                <?php echo $percent ?>%
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="infoPro">
                                        <div class="namePro">
                                            <a href="<?php echo $link ?>">
                                                <?php echo $pro->product_name ?>
                                            </a>
                                        </div>
                                        <div class="price">
                                            <span class="priceProBuy">
                                                <?php echo number_format($pro->product_price, 0, ',', '.') ?><sup>đ</sup>
                                            </span>
                                            <?php
                                            if ($pro->product_price_r > 0) {
                                                ?>
                                                <span class="priceProNotBuy">
                                                    <?php echo number_format($pro->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                </span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="buttonBuy">
                                            <div class="col-md-12" onclick="getTestModalDetail(<?php echo $pro->id ?>)">
                                                <span>Mua ngay</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>


<!-- Detail -->
<div class="modal fade" id="myDetailProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 1030px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myDetailProductLabel">
                    <!--Tên sản phẩm-->
                </h4>
            </div>
            <div class="modal-body" id="myContentDetailProduct">
                <!--Detail-->

<!--                <div class="col-md-12 padding0">
                    <div class="col-md-9">
                        <div class="col-md-12 padding0">
                            <div class="row padding0">
                                <div class="col-md-5">
                                    <div class="imgProductDetail positionRelative">
                                        <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title=""/>
                                        <div class="percentSale"> 
                                            40%
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="subImgProduct">
                                            <ul>
                                                <li>
                                                    <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title="" width="50px"/>
                                                </li>
                                                <li>
                                                    <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title="" width="50px"/>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 padding0">
                                    <div class="nameProductDetail col-md-12 padding0">
                                        áo đẹp áo đẹp áo đẹp áo đẹp áo đẹp áo đẹp áo đẹp áo đẹp áo đẹp
                                    </div>
                                    <div class="infoViewBuyerDetail col-md-12">
                                        <div class="infoView" data-toggle="tooltip" title="Lượt xem">
                                            100 <i class="fa fa-eye"></i>
                                        </div>
                                        <div class="infobuyer" data-toggle="tooltip" title="Lượt mua">
                                            100 <i class="fa fa-group"></i>
                                        </div>
                                    </div>

                                    <div class="priceProductdetail col-md-12">
                                        <div class="priceBuyProductDetail">
                                            100.000<sup>đ</sup>
                                        </div>
                                        <div class="priceNotBuyProductDetail">
                                            200.000<sup>đ</sup>
                                        </div>
                                    </div>
                                    <div class="chooseAttrProductDetail">
                                        <table class="table table-bordered table-hover" style="border-radius: 5px;"> 
                                            <thead>
                                                <tr>
                                                    <td colspan="3">Chọn số lượng</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="width: 18%">
                                                        <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title="" width="50px"/>
                                                    </td>
                                                    <td>
                                                        <div>Áo đệp Áo đệp Áo đệp Áo đệp Áo đệp Áo đệp Áo đệp</div>
                                                        <div>Size: M</div>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <select>
                                                            <option value="">- Chọn -</option>
                                                            <?php
                                                            for ($i = 1; $i <= 3; $i++) {
                                                                ?>
                                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 18%">
                                                        <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title="" width="50px"/>
                                                    </td>
                                                    <td>
                                                        <div>Áo đệp Áo đệp Áo đệp Áo đệp Áo đệp Áo đệp Áo đệp</div>
                                                        <div>Size: L</div>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <select>
                                                            <option value="">- Chọn -</option>
                                                            <?php
                                                            for ($i = 1; $i <= 3; $i++) {
                                                                ?>
                                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="buttonBuyNow">
                                        <div class="display_inline_block addCartProductDetail">
                                            <a href="#">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                Thêm vào giỏ hàng
                                            </a>
                                        </div>
                                        <div class="display_inline_block buyNowProductDetail">
                                            <a href="#">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                Mua ngay
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        sản phẩm liên quan
                        <div class="col-md-12 padding0 proRelate">
                            <div class="titleProductRelate">
                                <div class="titleRelate">
                                    Sản phẩm liên quan
                                </div>
                            </div>
                            <div class="contentProductRelate">
                                <div class="col-md-12 padding0 showProductRelate">
                                    <div class="col-md-3">
                                        <div class="c_productRelate">
                                            <div class="positionRelative" data-toggle="tooltip" title="Tên sản phẩm">
                                                <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title=""/>
                                                <div class="percentSale"> 
                                                    40%
                                                </div>
                                            </div>
                                            <div class="nameProRelate">
                                                <a href="#">
                                                    Áo đẹp wa Áo đẹp wa Áo đẹp wa 
                                                </a>
                                            </div>
                                            <div class="priceProductRelate">
                                                <div class="pricebuyRelate">
                                                    100.000<sup>đ</sup>
                                                </div>
                                                <div class="prceNotBuyRelate">
                                                    200.000<sup>đ</sup>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="c_productRelate">
                                            <div class="positionRelative" data-toggle="tooltip" title="Tên sản phẩm">
                                                <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title=""/>
                                                <div class="percentSale"> 
                                                    40%
                                                </div>
                                            </div>
                                            <div class="nameProRelate">
                                                <a href="#">
                                                    Áo đẹp wa Áo đẹp wa Áo đẹp wa 
                                                </a>
                                            </div>
                                            <div class="priceProductRelate">
                                                <div class="pricebuyRelate">
                                                    100.000<sup>đ</sup>
                                                </div>
                                                <div class="prceNotBuyRelate">
                                                    200.000<sup>đ</sup>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="c_productRelate">
                                            <div class="positionRelative" data-toggle="tooltip" title="Tên sản phẩm">
                                                <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title=""/>
                                                <div class="percentSale"> 
                                                    40%
                                                </div>
                                            </div>
                                            <div class="nameProRelate">
                                                <a href="#">
                                                    Áo đẹp wa Áo đẹp wa Áo đẹp wa 
                                                </a>
                                            </div>
                                            <div class="priceProductRelate">
                                                <div class="pricebuyRelate">
                                                    100.000<sup>đ</sup>
                                                </div>
                                                <div class="prceNotBuyRelate">
                                                    200.000<sup>đ</sup>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="c_productRelate">
                                            <div class="positionRelative" data-toggle="tooltip" title="Tên sản phẩm">
                                                <img src="components/com_openshop/assets/images/image.jpg" class="img-responsive" alt="" title=""/>
                                                <div class="percentSale"> 
                                                    40%
                                                </div>
                                            </div>
                                            <div class="nameProRelate">
                                                <a href="#">
                                                    Áo đẹp wa Áo đẹp wa Áo đẹp wa 
                                                </a>
                                            </div>
                                            <div class="priceProductRelate">
                                                <div class="pricebuyRelate">
                                                    100.000<sup>đ</sup>
                                                </div>
                                                <div class="prceNotBuyRelate">
                                                    200.000<sup>đ</sup>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        Chi tiết sản phẩm
                        <div class="col-md-12 padding0 proRelate proDetail">
                            <div class="titleProductRelate">
                                <div class="titleRelate">
                                    Chi tiết sản phẩm
                                </div>
                            </div>
                            <div class="contentProductRelate">
                                <div class="col-md-12 padding0 showProductRelate">
                                    Chi tiết sản phẩm
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3">
                        a
                    </div>
                </div>

                <div class="clr"></div>

                Có thể bạn quan tâm
                <div class="yourCare">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="titleCare">
                                Có thể bạn quan tâm
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contentProductCare">
                                aa
                            </div>
                        </div>
                    </div>
                </div>-->



                <div class="clr"></div>


            </div>
        </div>
    </div>
</div>

<!--Test modal-->
<div class="modal fade" id="myTestModalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 1030px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myDetailProductLabel">
                    <!--Tên sản phẩm-->
                </h4>
            </div>
            <div class="modal-body" id="myTestModal">
                <iframe class="formDetail"></iframe>
            </div>
        </div>
    </div>
</div>
