<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

if(count($this->products))
{
	$descriptionMaxChars = JRequest::getInt('description_max_chars');
        
	foreach ($this->products as $product)
	{
		$viewProductUrl = JRoute::_(OpenShopRoute::getProductRoute($product->id, OpenShopHelper::getProductCategory($product->id)));
		?>
		<li>
			<a href="<?php echo $viewProductUrl; ?>">
				<img alt="<?php echo $product->product_name; ?>" src="<?php echo $product->image; ?>" />
			</a>
			<div>
				<a href="<?php echo $viewProductUrl; ?>"><?php echo $product->product_name; ?></a><br />
				<span><?php echo OpenShopHelper::substring($product->product_short_desc, $descriptionMaxChars, '...'); ?></span>
			</div>
		</li>
		<?php
	}
}
else
{
	?>
	<li><?php echo JText::_('OPENSHOP_NO_PRODUCTS'); ?></li>
	<?php 
}