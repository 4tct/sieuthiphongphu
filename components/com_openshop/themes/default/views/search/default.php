<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<?php
if (count($this->products)) {
    ?>
    <div class="openshop-products-list">
        <?php
        echo OpenShopHtmlHelper::loadCommonLayout('common/products.php', array(
            'products' => $this->products,
            'pagination' => $this->pagination,
            'sort_options' => $this->sort_options,
            'tax' => $this->tax,
            'currency' => $this->currency,
            'productsPerRow' => $this->productsPerRow,
            'catId' => 0,
            'actionUrl' => $this->actionUrl,
            'showSortOptions' => FALSE
        ));
        ?>
    </div>
    <?php
} else {
    echo '<div class="noProduct"><i>Hiện không có sản phẩm</i></div>';
}