<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$app = JFactory::getApplication()->input;
?>
<div class="contentFAQs">
    <div class="col-md-3">
        <ul>
            <li class="titleFAQ">Vấn để</li>
            <?php
            if (empty($this->faqs)) {
                ?>
                <i>Đang cập nhật...</i>
                <?php
            } else {
                foreach ($this->faqs as $k => $f) {
                    if(OpenShopHelper::convertBase64Decode($app->getString('keyFAQ')) == $f->id){
                        $active = 'active';
                    }
                    else
                    {
                        $active = '';
                    }
                    ?>
                    <li>
                        <a class="<?php echo $active ?>" onclick="showAnswerFAQ('<?php echo OpenShopHelper::convertBase64Encode($f->id); ?>')"><?php echo ($k + 1) . '. ' . $f->faq_name ?></a>
                    </li>
                    <?php
                }
            }
            ?>

        </ul>
    </div>
    <div class="col-md-9">
        <div class="titleA">Giải đáp</div>
        <div class="contentA">
            <?php
            if (empty($this->answer)) {
                ?>
                <i>Đang cập nhật câu trả lời.</i>
                <?php
            } else {
                foreach ($this->answer as $k => $a) {
                    ?>
                    <div class="contentQA">
                        <div class="contentquestion">
                            <h5 onclick="showAnswer('<?php echo $k ?>')"><?php echo ($k+1) .'. '. $a->faq_question ?></h5>
                        </div>
                        <div class="contentAnswer answer_<?php echo $k ?>">
                            <?php echo $a->faq_answer ?>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<form action="/cau-hoi-thuong-gap.html" method="post" id="formAnswerFAQ">
    <input type="hidden" name="keyFAQ" value="<?php echo $app->getString('keyFAQ') ?>" id="keyFAQ"/>
    <?php echo JHtml::_('form.token') ?>
</form>