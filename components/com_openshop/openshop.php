<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');

//Define DS
if(!defined('DS')){
    define('DS', DIRECTORY_SEPARATOR);
}
//Require the controller
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/defines.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/inflector.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/autoload.php';
require_once JPATH_ADMINISTRATOR . '/components/com_openshop/libraries/rad/bootstrap.php';
//Encode - Decode by HuyHuynh
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/endeCodeJ.php';

$command = JRequest::getVar('task', 'display');

// Check for a controller.task command.
if (strpos($command, '.') !== false)
{
	list ($controller, $task) = explode('.', $command);
	$path = JPATH_COMPONENT . '/controllers/' . $controller . '.php';
	if (file_exists($path))
	{
		require_once $path;
		$className = 'OpenShopController' . ucfirst($controller);
		$controller = new $className();
	}
	else
	{
		//Fallback to default controller
		$controller = new OpenShopController(array('entity_name' => $controller, 'name' => 'OpenShop'));
	}
	JRequest::setVar('task', $task);
}
else
{
	$path = JPATH_COMPONENT . '/controller.php';
	require_once $path;
	$controller = new OpenShopController();
}
// Load Bootstrap CSS and JS
if (OpenShopHelper::getConfigValue('load_bootstrap_css'))
{
	OpenShopHelper::loadBootstrapCss();
}
if (OpenShopHelper::getConfigValue('load_bootstrap_js'))
{
	OpenShopHelper::loadBootstrapJs();
}

// Load CSS of corresponding theme
$document = JFactory::getDocument();

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();