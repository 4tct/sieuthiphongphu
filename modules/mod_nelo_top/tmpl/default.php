<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .top{
        width: 100%;
        height: <?php echo $params->get('heightTop') . 'px' ?>;
        background: <?php echo $params->get('colorTop') ?>;
    }
    .content-top{
        width: <?php echo $params->get('widthAuto') . $params->get('selectP') ?>;
        margin: 0 auto;
        padding-top: <?php echo $params->get('paddingTop') . 'px' ?>;
    }
    @media(max-width: 680px){
        .content-top{
            width: 90%;
        }
    }
</style>

<div class="top">
    <div class="content-top">
        <?php
        $modules = JModuleHelper::getModules('nelo-top-social');
        foreach ($modules as $module) {
            echo JModuleHelper::renderModule($module);
        }
        ?>
        <?php
        $modules = JModuleHelper::getModules('nelo-top-login-logout');
        foreach ($modules as $module) {
            echo JModuleHelper::renderModule($module);
        }
        ?>
        <?php
        $modules = JModuleHelper::getModules('nelo-top-menu');
        foreach ($modules as $module) {
            echo JModuleHelper::renderModule($module);
        }
        ?>
    </div>
</div>