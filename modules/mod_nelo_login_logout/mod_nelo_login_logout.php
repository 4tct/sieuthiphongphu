<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$town = Modnelo_login_logoutHelper::getTown();
$district = Modnelo_login_logoutHelper::getDistrict();
$link = Modnelo_login_logoutHelper::getLinkURL($params);
require JModuleHelper::getLayoutPath('mod_nelo_login_logout');