<?php
// No direct access
defined('_JEXEC') or die;

$session = JFactory::getSession();
$user = $session->get('user');
?>

<style>
    .login-logout{
        display: inline;
        float: <?php echo $params->get('alignAuto') ?>
    }
    .login-logout span{
        color: white;
    }
/*    .login-logout span:hover{
        color: black;
        cursor:pointer;
    }*/
/*    .login-logout > a{
        text-decoration: none;
        color: <?php echo $params->get('colorL') ?>;
        cursor: pointer;
    }
    .login-logout a:hover{
        color: <?php echo $params->get('colorH') ?>;
    }*/
    .m-loginout{
        display: none;
        color: white;
        cursor: pointer;
    }
    @media(max-width: 360px){
        .l-loginout{
            display: none;
        }
        .m-loginout{
            display: block;
        }
    }
    .top-link-account {
        margin-left: 25px;
        padding: 30px 0 0 0;
    }
    .top-link-account > ul > li {
        float: left;
    }
    .block-action-headers .acc-link {
        padding-left: 12px;
        min-width: 115px;
        min-height: 43px;
        margin-left: 10px;
    }
    .login-logout ul, ol {
        padding: 0px;
        margin: 0px;
        list-style: none;
    }
    .top-link-account > ul > li > a {
        font-size: 116%;
        font-weight: 600;
        display: block;
        cursor: pointer;
        line-height: 16px;
        font-family: tahoma;
    }
    .my-account-link > ul > li .dropdown-link-account {
        box-shadow: rgba(0, 0, 0, 0.45098) 0px 7px 5px;
        width: 220px;
        background-color: rgb(255, 255, 255);
        z-index: 88;
        transform-origin: 35% top 0px;
        border-top: none;
        display: none;
    }
    /*    .block-action-headers .acc-link:hover {
            background-color: rgb(255, 255, 255);
            box-shadow: rgba(0, 0, 0, 0.45098) 0px 7px 5px;
            border-radius: 3px 3px 0px 0px;
            border-bottom: none;
        }*/
    .my-account-link > ul > li:hover .dropdown-link-account {
        display: block;
        position: absolute;
        padding: 10px 0 0 0;
    }
    .my-account-link a{
        color: #444444 !important;
    }
    .my-account-link a:hover{
        color: #6CC550 !important;
    }
    .my-account-link > ul > li .dropdown-link-account .link-lg1 {
        margin-top: 15px;
    }
    .my-account-link > ul > li .dropdown-link-account .links {
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
        text-align: center;
        text-transform: uppercase;
        padding: 10px;
        width: 200px;
        line-height: 17px;
        font-weight: bold;
        margin-left: 10px;
        margin-top: 5px;
        border-radius: 3px;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(221, 221, 221);
        background: rgb(255, 255, 255);
    }
    .my-account-link > ul > li .dropdown-link-account .links:hover {
        background: rgb(108, 197, 80);
        border-color: rgb(108, 197, 80);
    }
    .my-account-link > ul > li .dropdown-link-account .links:hover a {
        color: white !important;
    }
    .my-account-link > ul > li .dropdown-link-account .link-ac {
        padding: 0 10px 5px 10px;
    }
    .my-account-link > ul > li .dropdown-link-account .link-logout {
        padding: 10px;
        background: #C3C3C3;
        text-align: center;
        font-weight: bold;
    }
    .my-account-link > ul > li .dropdown-link-account .link-logout a:hover {
        color: white !important;
    }
    .links-res{
        margin: 10px;
    }
</style>

<div class="login-logout">
    <?php
    switch ($params->get('selectL')) {
        case 'l1':
            ?>
            <div class="m-loginout">
                <i class="fa fa-bars"></i>
                <div>

                </div>
            </div>
            <div class="l-loginout">
                <a>Đăng nhập</a> <span>-</span> <a>Đăng ký</a>
            </div>
            <?php
            break;
        case 'l2':
            ?>
            <div class="block-action-headers top-link-account my-account-link">
                <ul>
                    <li class="acc-link">
                        Xin chào,
                        <a class="account-toplink dropdown-ict" title="My Account" href="#">
                            <?php
                            if (empty($user->id)) {
                                echo 'Tài khoản của tôi';
                            } else {
                                echo $user->name;
                            }
                            ?>
                        </a>
                        <ul class="dropdown-link-account">
                            <?php
                            if (empty($user->id)) {
                                ?>
                                <li class="links link-lg1">
                                    <a title="Logout" href="#" class="logout" onclick="showlogin()">Đăng nhập</a>
                                </li>
                                <li class="links links-res">
                                    <a title="Register" href="#" class="registers" onclick="showSignIn()">Đăng ký</a>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li class="link-ac"><a title="My History" href="/cong-tac-vien.html">Công tác viên</a></li>
                                <li class="link-ac"><a title="Account Information" href="/thong-tin-ca-nhan.html">Thông tin tài khoản </a></li>
                                <li class="link-ac"><a title="My Checkout" href="/gio-hang.html">Giỏ hàng</a></li>
                                <li class="link-ac"><a title="My History" href="/lich-su-mua-hang.html">Lịch sử mua hàng</a></li>
                                <li class="link-logout">
                                    <form action="<?php echo JRoute::_('index.php?option=com_users&task=user.logout'); ?>" method="post" id="formLogout">
                                        <a title="My History" href="#" onclick="formLogout()">Đăng xuất</a>
                                        <?php
                                        $return = substr($_SERVER['REQUEST_URI'], 1, strlen($_SERVER['REQUEST_URI']));
                                        ?>
                                        <input type="hidden" name="return" id="btl-return" value="<?php echo base64_encode($return); ?>">
                                        <?php echo JHtml::_('form.token'); ?>
                                    </form>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>								
                    </li>
                </ul>	
            </div>
            <?php
            break;
        case 'l3':
            require 'type_3.php';
            break;
        default:
            break;
    }
    ?>

</div>


<?php
if (empty($user->id)) {
    ?>
    <!--//LogIn-->
    <form action="index.php?option=com_users&task=user.login" method="post" id="formLogin" class="form-horizontal">
        <div class="modal fade" id="modalFormLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Đăng nhập</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="username" id="username" value="" placeholder="Tài khoản" class="form-control"/>
                        <div class="error error-username"></div>
                        <br>
                        <input type="password" name="password" id="password" value="" placeholder="Mật khẩu" class="form-control"/>
                        <div class="error error-password"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="loginForm()">Đăng nhập</button>
                        <?php
                        $return = JRoute::_($_SERVER['REQUEST_URI']);
                        ?>
                        <input type="hidden" name="return" id="btl-return" value="<?php echo base64_encode($return) ?>">
                        <?php echo JHtml::_('form.token'); ?>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--//SignIN-->
    <form action="index.php?option=com_users&task=user.registerJ" method="post" id="formRegister" class="form-horizontal">
        <div class="modal fade" id="myModalSignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Đặt ký nhanh</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-3">Tên tài khoản <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <input type="text" value="" class="form-control" placeholder="Nhập tên tài khoản của bạn" name="username" id="usernameR"/>
                                <div class="error error-usernameR"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Mật khẩu <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <input type="password" value="" class="form-control" placeholder="Nhập mật khẩu của bạn" name="password" id="passwordR"/>
                                <div class="error error-passwordR"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Nhập lại mật khẩu <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <input type="password" value="" class="form-control" placeholder="Nhập mật khẩu của bạn" name="password_again" id="password_again"/>
                                <div class="error error-password_again"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Họ và tên <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <input type="text" value="" class="form-control" placeholder="Nhập họ và tên của bạn" name="customer_name" id="customer_name"/>
                                <div class="error error-customer_name"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Số điện thoại <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <input type="text" value="" class="form-control" placeholder="Nhập số điện thoại của bạn" name="customer_phone" id="customer_phone"/>
                                <div class="error error-customer_phone"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Email </label>
                            <div class="col-md-9">
                                <input type="text" value="" class="form-control" placeholder="Nhập enail của bạn" name="customer_email" id="customer_email"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Địa chỉ <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <input type="text" value="" class="form-control" placeholder="Nhập địa chỉ của bạn bao gồm số nhà, đường,..." name="customer_address" id="customer_address"/>
                                <div class="error error-customer_address"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Tỉnh/Thành Phố <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <select name="zone" id="customer_town" class="form-control" onchange="changeZone(this.value)">
                                    <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_ZONE') ?></option>
                                    <?php
                                    foreach ($town as $v_z) {
                                        $cls = '';
                                        echo '<option value="' . $v_z->id . '" ' . $cls . '>' . $v_z->zone_name . '</option>';
                                    }
                                    ?>
                                </select>
                                <div class="error error-customer_town"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Quận/Huyện <i style="color:red;">(*)</i></label>
                            <div class="col-md-9">
                                <select name="district" id="customer_district" class="form-control">
                                    <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_DISTRICT') ?></option>
                                    <?php
                                    foreach ($district as $v_d) {
                                        $cls = '';
                                        echo '<option value="' . $v_d->id . '" ' . $cls . '>' . $v_d->district_name . '</option>';
                                    }
                                    ?>
                                </select>
                                <div class="error error-customer_district"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3"></label>
                            <div class="col-md-9">
                                <i style="color:red;">(*)</i> Yêu cầu nhập
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="formRegister()">Đăng ký</button>
                        <?php echo JHtml::_('form.token'); ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php
}
?>