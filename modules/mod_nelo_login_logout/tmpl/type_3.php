<?php
defined('_JEXEC') or die;
?>

<style>
    .login-logout{
    }
    .logiout_t3{
    }
    .logiout_t3 .wap_loginout > a{
        text-decoration: none;
        color: <?php echo $params->get('colorL') ?>
    }
    .logiout_t3 i{
        font-size: 30px;
    }
    .logiout_t3 span{
        color: <?php echo $params->get('colorL') ?>
    }
    /*    .logiout_t3 a:hover,.logiout_t3 span:hover{
            color: <?php echo $params->get('colorH') ?>
    }*/
    .logiout_t3:hover .info_user_log{
        z-index: 1000;
        display: block;
        transform: translate(0px, 0px);
        -webkit-transform: translate(0px, 0px);
        -moz-transform: translate(0px, 0px);
        transition: all .3s ease;
        -webkit-transition: all .3s ease;
        -moz-transition: all .3s ease;
    }
    .info_user_log{
        position: absolute;
        display: none;
        background: white;
        box-shadow: 0px 0px 5px 0px #c5c5c5;
        z-index: -10;
        transform: translate(0px, 20px);
        -webkit-transform: translate(0px, 20px);
        -moz-transform: translate(0px, 20px);
        transition: all .3s ease;
        -webkit-transition: all .3s ease;
        -moz-transition: all .3s ease;
    }
    .logiout_t3 .info_user_log ul{
        width: 200px;
    }
    .logiout_t3 .info_user_log ul li{
        padding: 10px 10px 5px 10px;
    }
    .logiout_t3 .wap_loginout{
        margin-top: 18px;
    }
    .info_user_log ul li a{
        text-decoration: none;
    }
    .logout_t3{
        color: white;
    }
    .login_t3{
        background: #05b2e9;
        padding: 10px;
        text-align: center;
        cursor: pointer;
        border: 1px solid #05b2e9;
    }
    .register_t3{
        background: #ff5e00;
        padding: 10px;
        text-align: center;
        cursor: pointer;
        border: 1px solid #ff5e00;
    }
    .logiout_t3 .wap_loginout:hover{
    }
    .info_user_log .login_t3:hover{
        background: white;
        color: #05b2e9;
    }
    .info_user_log .register_t3:hover{
        background: white;
        color: #ff5e00;
    }
    .logiout_t3 .colorLG .dn{
        color: #5d5d5d;
        font-weight: bold;
    }
    .logiout_t3 .colorLG .tkdh{
        color: #5d5d5d;
    }
    .logiout_t3 .nameUser{
        color: #5d5d5d;
    }
    .info_user_log ul li a .logout_t3{
        color: #ffffff;
        background: red;
        padding: 10px;
        text-align: center;
        border: 1px solid red;
    }
    .info_user_log ul li a .logout_t3:hover{
        color: red;
        background: white;
        border: 1px solid red;
    }
</style>

<div class="logiout_t3">
    <div class="wap_loginout">
        <a href="<?php echo JRoute::_($link) ?>">
            <div class="display_inline_block">
                <i class="fa fa-user icon_black" aria-hidden="true"></i>
            </div>
            <div class="display_inline_block colorLG">
                <?php
                if ($user->id == '') {
                    ?>
                    <div class="dn">
                        Đăng nhập
                    </div>
                    <div class="tkdh">
                        Tài khoản & đơn hàng
                    </div>
                    <?php
                } else {
                    echo '<div class="nameUser">';
                    echo $user->name;
                    echo '</div>';
                }
                ?>
            </div>
        </a>
    </div>

    <div class="info_user_log">
        <ul>
            <?php
            if ($user->id == '') {
                ?>
                <li>
                    <a href="/dang-nhap.html">
                        <div class="login_t3">
                            Đăng nhập
                        </div>
                    </a>
                </li>
                <li>
                    <a href="dang-ky.html">
                        <div class="register_t3">
                            Đăng ký
                        </div>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li>
                    <a href="/thong-tin-ca-nhan.html">Thông tin cá nhân</a>
                </li>
                <li>
                    <a href="/gio-hang.html">Giỏ hàng</a>
                </li>
                <li>
                    <a href="/lich-su-mua-hang.html">Lịch sử mua hàng</a>
                </li>
                <li>
                    <form action="<?php echo JRoute::_('index.php?option=com_users&task=user.logout'); ?>" method="post" id="formLogout">
                        <a title="My History" href="#" onclick="formLogout()">
                            <div class="logout_t3">
                                Đăng xuất
                            </div>
                        </a>
                        <?php
                        $return = substr($_SERVER['REQUEST_URI'], 1, strlen($_SERVER['REQUEST_URI']));
                        ?>
                        <input type="hidden" name="return" id="btl-return" value="<?php echo base64_encode($return); ?>">
                        <?php echo JHtml::_('form.token'); ?>
                    </form>
                </li>
                <?php
            }
            ?>

        </ul>
    </div>
</div>

