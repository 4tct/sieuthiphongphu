<?php

class Modnelo_login_logoutHelper {
    public static function getTown() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('id,zone_name')
                    ->from($db->quoteName('#__openshop_zones'))
                    ->where('published = 1');
        } catch (Exception $ex) {
            //error
        }
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getDistrict() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('id,district_name')
                    ->from($db->quoteName('#__openshop_districts'))
                    ->where('published = 1');
        } catch (Exception $ex) {
            //error
        }
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getLinkURL($params){
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('link')
                    ->from($db->quoteName('#__menu'))
                    ->where('id = ' . $params->get('linkLogInOut'));
            $link = $db->setQuery($query)->loadResult();
            
            return $link . '&Itemid=' . $params->get('linkLogInOut');
        } catch (Exception $ex) {
            return '#';
        }
    }

}
