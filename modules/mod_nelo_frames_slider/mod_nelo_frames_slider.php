<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$framSlider = Modnelo_frames_sliderHelper::getnelo_frames_slider($params);
require JModuleHelper::getLayoutPath('mod_nelo_frames_slider');