<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .frame-slider{
        width: <?php echo $params->get('contentF') . $params->get('selectP') ?>;
        margin: 0 auto;
    }
    .nelo-breadcrumb{
        width: <?php echo $params->get('contentF') . $params->get('selectP') ?>;
        margin: 0 auto;
    }
    .categories{
        height: 400px;
        background: #444;
    }
    .main-slider{
        height: 400px;
        /*background: #CCCCCC;*/
    }
    .info1{
        height: 100%;
        /*background: #FF9D00;*/
    }
    .frame-slider .categories, .frame-slider .main-slider, .frame-slider .info1, .frame-slider .info2{
        margin: 0;
        padding: 0;
    }
    @media(max-width: 991px){
        .frame-slider .categories{
            display: none;
        }
        .frame-slider .info1{
            display: none;
        }
    }
</style>

<div class="frame-slider">
    <?php
    if ($params->get('selectF') == 'l1') {
        ?>
        <div>
            <div class="menuCatResponsive" onclick="showMenuCategory()">
                Danh mục sản phẩm
            </div>
            <div class="bg-menuCartResponsive" onclick="hideMenuCategory()"></div>
            <div class="categories col-md-3">
                <?php
                $modulesMC = JModuleHelper::getModules('nelo-menu-categories');
                foreach ($modulesMC as $module) {
                    echo JModuleHelper::renderModule($module);
                }
                ?>
            </div>
        </div>
        <div class="main-slider col-md-6">
            <?php
            $modulesS = JModuleHelper::getModules('nelo-slider');
            foreach ($modulesS as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <div class="info1 col-md-3">
            <?php
            $modulesOP = JModuleHelper::getModules('nelo-one-product-1');
            foreach ($modulesOP as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <?php
    } else if ($params->get('selectF') == 'l2') {
        $modulesS = JModuleHelper::getModules('nelo-slider');
        foreach ($modulesS as $module) {
            echo JModuleHelper::renderModule($module);
        }
    }
    ?>
</div>