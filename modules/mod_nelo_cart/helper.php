<?php

class Modnelo_cartHelper {
    public static function getnelo_cart($ip) {
        $user = JFactory::getSession()->get('user');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_sku,c.product_name,d.value as size,b.product_image,b.product_price,b.id as product_id,d.optionvalue_id as size_id,b.product_price_r')
                ->from($db->quoteName('#__openshop_cartproducts', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . 'ON a.product_id = c.product_id')
                ->join('INNER', $db->quoteName('#__openshop_optionvaluedetails', 'd') . 'ON d.optionvalue_id = a.size_id')
                ->where('a.ip = "' . $ip . '"', 'OR');
        if (!empty($user->id) && $user->id != '0') {
            $query->where('a.user_id = ' . $user->id);
        }

        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getNumberCart($ip) {
        $user = JFactory::getSession()->get('user');
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(a.id)')
                ->from($db->quoteName('#__openshop_cartproducts', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . 'ON a.product_id = b.id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . 'ON a.product_id = c.product_id')
                ->where('a.ip = "' . $ip . '"', 'OR');
        if (!empty($user->id) && $user->id != '0') {
            $query->where('a.user_id = ' . $user->id);
        }

        return $db->setQuery($query)->loadResult();
    }
    
    public static function getURLCart($params){
        try {
            $db = JFactory::getDBO();
            $query = $db->getQuery(TRUE);
            $query->select('link')
                    ->from($db->quoteName('#__menu'))
                    ->where('id = ' . $params->get('linkMenuId'));
            $link = $db->setQuery($query)->loadResult();
            
            return $link . '&Itemid=' . $params->get('linkMenuId');
        } catch (Exception $ex) {
            return '#';
            
        }
    }

}
