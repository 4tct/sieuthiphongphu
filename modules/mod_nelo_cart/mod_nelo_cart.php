<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$carts = Modnelo_cartHelper::getnelo_cart($_SERVER['REMOTE_ADDR']);
$link = Modnelo_cartHelper::getURLCart($params);
$numcart = Modnelo_cartHelper::getNumberCart($_SERVER['REMOTE_ADDR']);
require JModuleHelper::getLayoutPath('mod_nelo_cart');