<style>
    .cart_t1{
        padding: 0 0 0 30px;
        position: relative;
    }
    .cart_t1 a{
        text-decoration: none;
    }
    .cart_t1 i{
        font-size: 30px;
        color: #000;
    }
    .cart_t1 span{
        color: #000;
    }
    .cart_t1 .num_cart{
        position: absolute;
        top: -10px;
        left: 50px;
    }
    .cart_t1 .num_cart span{
        padding: 5px 10px;
        background: red;
        border-radius: 50%;
        color: white;
    }
</style>
<div class="cart_t1">
    <a href="<?php echo JRoute::_($link) ?>">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        <span>Giỏ hàng</span>
    </a>

    <div class="num_cart">
        <span>0</span>
    </div>
</div>
