<?php
// No direct access
defined('_JEXEC') or die;

switch ($params->get('chooseType')){
    case 't1':
        require 'type_1.php';
        break;
    case 't2':
        require 'type_2.php';
        break;
    default :
        break;
}
?>
