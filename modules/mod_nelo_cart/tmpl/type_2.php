<style>
    .cart_t1{
        float: right;
        position: relative;
            margin-top: 5px;
    }
    .cart_t1 a{
        text-decoration: none;
        margin-right: 10px;
    }
    .cart_t1 i{
        font-size: 30px;
        color: #000;
    }
    .cart_t1 a span{
        color: #707070;
        font-size: 18px;
    }
    .cart_t1 .num_cart{
        position: absolute;
        top: 0px;
        right: 0;
    }
    .cart_t1 .num_cart span{
        padding: 5px 10px;
        border-radius: 50%;
        color: #05b2e9;
        background: white;
        border: 1px solid #05b2e9;
    }
    .wap_cart_icon{
        display: inline-block;
            padding: 15px 17px;
        background: #05b2e9;
        color: white;
        border-radius: 50%;
    }

</style>
<div class="cart_t1">
    <a href="<?php echo JRoute::_($link) ?>">
        <span><!-- Giỏ hàng --></span>
        <div class="wap_cart_icon">
            <i class="fa fa-shopping-cart icon_white" aria-hidden="true"></i>
        </div>
    </a>
    <div class="num_cart">
        <span><?php echo $numcart; ?></span>
    </div>
</div>
