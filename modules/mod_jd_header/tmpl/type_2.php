<?php
$view = $app->input->getCmd('view', '');
?>

<style>
    .head-top{
        border-bottom: 1px solid #e2e2e2;
    }
    .head-logo{
        padding: 10px 0 15px 0;
    }
    .head-loginout{
        padding-top: 15px;
    }
    .head-cart{
        padding-top: 20px;
        float: right;
    }
</style>

<div class="wapper-head-t2">
    <div class="head-top">
        <?php
        $modules = JModuleHelper::getModules('nelo-top');
        foreach ($modules as $module) {
            echo JModuleHelper::renderModule($module);
        }
        ?>
    </div>
    <div class="head-logo-cart width-body">
        <div class="head-logo col-md-3">
            <?php
            $modules = JModuleHelper::getModules('nelo-logo');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <div class="head-seach col-md-6">
            <?php
            $modules = JModuleHelper::getModules('nelo-search');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <div class="head-loginout col-md-3">
            <?php
            $modules = JModuleHelper::getModules('nelo-loginout');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }

            $modules = JModuleHelper::getModules('nelo-cart');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
    </div>

    <div class="clr"></div>

    <div class="head-menu">
        <div class="main-menu">
            <?php
            $modules = JModuleHelper::getModules('nelo-main-menu');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
    </div>

    <div class="head-silde width-body">
        <div class="col-md-3 menu-caterogy padding0">
            <?php
            $modules = JModuleHelper::getModules('nelo-menu-categories');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>

        <?php
        if ($view == 'frontpage') {
            $modules = JModuleHelper::getModules('nelo-slider');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
        }
        ?>
    </div>

</div>
