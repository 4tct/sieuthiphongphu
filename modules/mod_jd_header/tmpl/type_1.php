<style>
    .wapper-head-t1{
        width: <?php echo $params->get('width') . '%' ?>;
        margin-left: <?php echo (int) (100 - (int) $params->get('width')) / 2 . '%' ?>;
    }
</style>

<div class="wapper-head-t1">
    <!--LOGO - SEARCH - LOGIN - CART-->
    <div class="head-logo-search">
        <!--logo-->
        <div class="display_inline_block head-logo">
            <?php
            $modules = JModuleHelper::getModules('nelo-logo');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <div class="display_inline_block head-search">
            <?php
            $modules = JModuleHelper::getModules('nelo-search');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <div class="display_inline_block head-login">
            <?php
            $modules = JModuleHelper::getModules('nelo-loginout');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <div class="display_inline_block head-cart">
            <?php
            $modules = JModuleHelper::getModules('nelo-cart');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
    </div>
    <!--MENU-->
    <div class="head-menu">
        <div class="display_inline_block menu-caterogy">
            <?php
            $modules = JModuleHelper::getModules('nelo-menu-categories');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
        <div class="display_inline_block main-menu">
            <?php
            $modules = JModuleHelper::getModules('nelo-main-menu');
            foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
            }
            ?>
        </div>
    </div>
</div>