<?php
class Modnelo_tagsHelper
{
    public static function getnelo_tags($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_tags'))
                ->order('hits DESC');
        return $db->setQuery($query, 0, 10)->loadObjectList();
    }
}
