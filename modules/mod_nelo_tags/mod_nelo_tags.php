<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$tags = Modnelo_tagsHelper::getnelo_tags($params);
require JModuleHelper::getLayoutPath('mod_nelo_tags');