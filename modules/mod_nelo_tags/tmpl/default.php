<?php
// No direct access
defined('_JEXEC') or die;
?>

<div class="nelo-tags">
    <div class="titleC titlePH">
        <a class="tPH">TOP từ khóa</a>
    </div>
    <div class="nelo-content-tags">
        <?php
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                ?>
                <h5 onclick="searchTag('<?php echo $tag->tag_name ?>')"><?php echo $tag->tag_name ?></h5>
                <?php
            }
        } else {
            ?>
            <h5>Thời trang nam cao cấp</h5>
            <h5>Áo thun</h5>
            <h5>Thời trang nữ cao cấp</h5>
            <h5>Áo đẹp</h5>
            <?php
        }
        ?>
    </div>
</div>