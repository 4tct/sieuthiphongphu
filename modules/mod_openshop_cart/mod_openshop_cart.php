<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

require_once (dirname(__FILE__).'/helper.php');
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/defines.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/inflector.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/autoload.php';
require_once JPATH_ADMINISTRATOR . '/components/com_openshop/libraries/rad/bootstrap.php';

//Load com_openshop language file
$language = JFactory::getLanguage();
$template = JFactory::getApplication()->getTemplate();

$tag = $language->getTag();

if (!$tag)
	$tag = 'en-GB';

$language->load('com_openshop', JPATH_ROOT, $tag);

//Load css module openshop cart
$document = JFactory::getDocument();

// add extra css for selected type

if (is_file(JPATH_SITE .  '/templates/'. $template .  '/css/'  . $module->module . '.css')) {
	$document->addStyleSheet(JURI::base().'templates/' . $template . '/css/' . $module->module . '.css');
} else {
	$document->addStyleSheet(JURI::base().'modules/' . $module->module . '/asset/css/style.css');
}

// Load Bootstrap CSS and JS
if (OpenShopHelper::getConfigValue('load_bootstrap_css'))
{
	OpenShopHelper::loadBootstrapCss();
}
if (OpenShopHelper::getConfigValue('load_bootstrap_js'))
{
	OpenShopHelper::loadBootstrapJs();
}
JHtml::_('script', OpenShopHelper::getSiteUrl(). 'components/com_openshop/assets/js/noconflict.js', false, false);
//Get cart data
$cart = new OpenShopCart();

/* ------------ Author:Huy --------------- */

$total = $cart->getTotalCartProduct();

/* ------------ Author:Huy --------------- */

$items = $cart->getCartData();
$countProducts = $cart->countProducts();

$currency = new OpenShopCurrency();

$tax = new OpenShopTax(OpenShopHelper::getConfig());

$openshopModel = new OpenShopModel();

$cartModel = $openshopModel->getModel('cart');
$cartModel->getCosts();
$totalData = $cartModel->getTotalData();
//$totalPrice = $currency->format($cartModel->getTotal());
$totalPrice = $cartModel->getTotal();
$view = JRequest::getVar('view');

require JModuleHelper::getLayoutPath('mod_openshop_cart', $params->get('layout', 'default'));
?>