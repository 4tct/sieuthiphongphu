<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$hello = Modnelo_google_mapsHelper::getnelo_google_maps($params);
require JModuleHelper::getLayoutPath('mod_nelo_google_maps');