<?php
$clsP = 'paddingR0';
$height = '456px';
$heightWapperSilder = '456px';
$clsTop = '';
if (OpenShopHelper::isMobile()) {
    $clsP = 'padding0';
    $height = 'auto';
    $heightWapperSilder = ' 183px';
    $clsTop = 'dtop35px';
}
?>

<div class="col-md-9 <?php echo $clsP ?> <?php echo $clsTop ?>" style="height: <?php echo $heightWapperSilder ?>;position: relative;">
    <div class="swiper-container swiper-silde-menu">
        <div class="swiper-wrapper">
            <?php
            $banners = Modslider_huyHelper::getBanner();
            foreach ($banners['banner'] as $k => $banner) {
                ?>
                <div class="swiper-slide slide_border_img s_<?php echo $k ?>">
                    <img src="<?php echo OPENSHOP_PATH_BANNERS_HTTP . $banner->banner_image ?>" width="100%" height="<?php echo $height ?>"/>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <?php
    if ($params->get('showSubBanner')) {
        if (!OpenShopHelper::isMobile()) {
            ?>
            <div class="page_j_silde">
                <?php
                foreach ($banners['banner'] as $k => $banner) {
                    $cls = 'cls_border';
                    $cls_bT = '';
                    $cls_bB = '';

                    if ($k == 0) {
                        $cls_bT = 'border5500';
                    }
                    if ($k == count($banners['banner']) - 1) {
                        $cls = '';
                        $cls_bB = 'border0055';
                    }

                    $link = OpenShopRoute::getBannerRoute($banner->id);
                    ?>
                    <div class="page_j <?php echo $cls ?> <?php echo $cls_bT ?> <?php echo $cls_bB ?> check_active_<?php echo $k + 1 ?>" data-active="<?php echo $k + 1 ?>" onclick="location.href = '<?php echo JRoute::_($link) ?>'">
                        <div class="banner_img" style="display: inline-block;">
                            <img src="<?php echo OPENSHOP_PATH_BANNERS_HTTP . $banner->banner_image ?>" width="60px" height="40px"/>
                        </div>
                        <div class="banner_title">
                            <strong><?php echo $banner->title ?></strong>
                            <div class="dsc">
                                <?php echo $banner->description ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <input type="hidden" value="<?php echo count($banners['banner']) ?>" id="totleBanner"/>
            </div>
            <?php
        }
    }
    ?>
</div>

<script>
    var swiper_silde_menu = new Swiper('.swiper-silde-menu', {
        paginationClickable: true,
        direction: 'vertical',
        autoplay: 3000,
        loop: true,
        onSlideChangeStart: function (swiper) {
            var index = parseInt(swiper.activeIndex);
            if (parseInt($('#totleBanner').val()) < parseInt(swiper.activeIndex)) {
                index = 1;
            }
            $('.page_j').removeClass('backgroundWhite');
            $('.page_j').removeClass('page_before');
            $('.check_active_' + index).addClass('backgroundWhite');
            $('.check_active_' + index).addClass('page_before');
        }
    });

    $('.page_j').hover(function () {
        var num = $(this).attr('data-active');
        swiper_silde_menu.slideTo(num, 300, true);
        swiper_silde_menu.startAutoplay();

        if (num == '0') {
            $('.swiper-silde-menu .swiper-wrapper').removeAttr('style');
            $('.swiper-silde-menu .swiper-wrapper').attr('style', 'transition-duration: 300ms');
        }
        $('.page_j').removeClass('backgroundWhite');
        $('.page_j').removeClass('page_before');
        $(this).addClass('backgroundWhite');
        $(this).addClass('page_before');
    });
</script>
