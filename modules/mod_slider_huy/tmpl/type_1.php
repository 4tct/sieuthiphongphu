<style>
    .my-slider ul li img{
        width: 100%;
        max-height: <?php echo $params->get('heightSlider') . 'px' ?>;
    }
</style>

<div class="my-slider">

    <div class="swiper-container slide_main_home" style="padding-top: 0px;">
        <div class="swiper-wrapper">
            <?php
            foreach ($images as $key => $image) {
                ?>
                <div class="swiper-slide">
                    <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $image->media_image ?>" alt="Slider" style="max-height: <?php echo $params->get('heightSlider') . 'px' ?>; padding: 0;" width="100%"/>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<script>
    var swiper = new Swiper('.slide_main_home', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.jd-nav-button-next',
        prevButton: '.jd-nav-button-prev',
        autoplay: <?php echo $params->get('delaySlider') ?>,
        effect: '<?php echo $params->get('stypeSlide') ?>'
    });
</script>

