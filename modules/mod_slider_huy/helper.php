<?php

class Modslider_huyHelper {

    public static function getslider_huy($params) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_mediadetails'))
                ->where('media_id = ' . $params->get('slider_id'));
        return $db->setQuery($query)->loadObjectList();
    }

    public static function getBanner($limit = 8) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__openshop_banners'))
                ->where('published = 1')
                ->order('id DESC');

        $banner = array();

        $banner['banner'] = $db->setQuery($query, 0, $limit)->loadObjectList();

        return $banner;
    }

    public static function getBannerModule($params) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('*')
                    ->from($db->quoteName('#__menu', 'a'))
                    ->where('a.menutype = "' . $params->get('menu_id') . '"');
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }

    public static function getLinkBannerMenu($id) {
        try {
            $db = JFactory::getDBO();
            $query = $db->getQuery(TRUE);
            $query->select('b.id,b.link')
                    ->from($db->quoteName('#__openshop_banners', 'a'))
                    ->join('INNER', $db->quoteName('#__menu', 'b') . ' ON a.link_id = b.id')
                    ->where('a.id = ' . $id);
            $res = $db->setQuery($query)->loadObject();

            if (count($res)) {
                return $res->link . '&Itemid=' . $res->id;
            } else {
                return '';
            }
        } catch (Exception $ex) {
            return '';
        }
    }

}
