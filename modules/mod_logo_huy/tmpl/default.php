<?php
// No direct access
defined('_JEXEC') or die;
$app = JFactory::getApplication();
?>

<style>
    .logo{
        position: relative;
        padding-top: 5px;
    }
    .logo a img{
        padding-top: <?php echo $params->get('paddingLogo') . 'px'; ?>;
        padding-bottom: <?php echo $params->get('paddingLogo') . 'px'; ?>;
        padding-left: <?php echo $params->get('paddingLRLogo') . 'px'; ?>;
        padding-right: <?php echo $params->get('paddingLRLogo') . 'px'; ?>;
    }
    .logo a{
        text-decoration: none
    }
    .logo a img{
        height: 80px;
    }
</style>

<div class="logo">
    <a href="/" title="Logo" class="logo">
        <?php
        foreach ($logo as $k => $v) {
            ?>
            <img class="img-responsive" src="<?php echo JUri::root() . 'images/com_openshop/medias/' . $v->media_image ?>" alt="Logo"/>
            <?php
        }
        ?>
    </a>
</div>