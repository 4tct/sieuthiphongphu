<?php
class ModLogo_huyHelper
{
    public static function getLogo_huy($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.media_image')
                ->from($db->quoteName('#__openshop_medias','a'))
                ->join('INNER', $db->quoteName('#__openshop_mediadetails','b') . ' ON a.id = b.media_id')
                ->where('a.id = ' . $params->get('id'));
        $name_img = $db->setQuery($query)->loadObjectList();
        
        //set session
        $session = JFactory::getSession();
        $session->set('logo_img', $name_img);
        
        return $name_img;
    }
}
