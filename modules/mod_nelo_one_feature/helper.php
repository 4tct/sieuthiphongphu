<?php
class Modnelo_one_featureHelper
{
    public static function getProductOP($params)
    {
        $ids = implode(',', $params->get('productOP'));
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . ' ON a.id = b.product_id')
                ->where('a.id IN ('. $ids .')');
        return $db->setQuery($query)->loadObjectList();
    }
}
