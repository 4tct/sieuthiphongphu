<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$products = Modnelo_one_featureHelper::getProductOP($params);
require JModuleHelper::getLayoutPath('mod_nelo_one_feature');