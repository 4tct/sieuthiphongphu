<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .bg_title{
        padding: 10px;
        background: black;
        z-index: 10000;
        opacity: 0.3;
        position: absolute;
        width: 100%;
        bottom: 0;
        height: 40px;
    }
    .title_P{
        position: absolute;
        bottom: 7px;
        text-align: center;
        width: 100%;
        color: white;
        z-index: 10000;
    }
    .showTitle<?php echo $params->get('class_sfx') ?>{
        opacity: 1;
    }
    .showContent<?php echo $params->get('class_sfx') ?>:hover .showTitle<?php echo $params->get('class_sfx') ?>{
        opacity: 1;
        transition: 1s all;
    }
    .f-container{
        margin: 0 auto;
        position: relative;
        overflow: hidden;
        z-index: 1;
        height: <?php echo $params->get('heightOP') . 'px' ?>;
    }
</style>

<div class="showContent<?php echo $params->get('class_sfx') ?>">
    <div class="f-container">
        <div class="swiper-wrapper">
            <?php
            foreach ($products as $product) {
                $productUrl = JRoute::_(OpenShopRoute::getProductRoute($product->id, OpenShopHelper::getProductCategory($product->id)));
                ?>
                <div class="swiper-slide" onclick="location.href = '<?php echo $productUrl ?>'" style="cursor:pointer;">
                    <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $product->product_image ?>" width="100%" height="<?php echo $params->get('heightOP')?>" title="<?php echo $product->product_name; ?>" alt="<?php echo $product->product_name; ?>"/> 
                    <div class="showTitle<?php echo $params->get('class_sfx') ?>">
                        <div class="bg_title"></div>
                        <h5 class="title_P"><?php echo $product->product_name; ?></h5>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<script>
    var swiper = new Swiper('.f-container', {
        paginationClickable: true,
        direction: 'vertical',
        autoplay: 7000
    });
</script>