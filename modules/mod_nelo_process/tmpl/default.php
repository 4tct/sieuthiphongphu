<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .nelo_process{
        float: <?php echo $params->get('chooseFloat'); ?>;
    }
    .nelo_process ul{
        display: inline-block;
        list-style: none;
        margin: 32px 60px 0 0;
        padding: 0 0 0 50px;
        position: relative;
    }
    .nelo_process ul li{
        font-weight: bold;
    }
    .nelo_process .cls-icon{
        position: absolute;
        left: 0;
        font-size: 30px;
        top: 5px;
    }
    .text_red{
        color:red;
    }
</style>

<div class="nelo_process">
    <ul>
        <li><i class="fa fa-truck cls-icon" aria-hidden="true"></i></li>
        <li>GIAO HÀNG</li>
        <li>TRÊN TOÀN QUỐC</li>
    </ul>
    <ul>
        <li><i class="fa fa-credit-card-alt cls-icon" aria-hidden="true"></i></li>
        <li>THANH TOÁN</li>
        <li>KHI NHẬN HÀNG</li>
    </ul>
    <ul>
        <li><i class="fa fa-clock-o cls-icon" aria-hidden="true"></i></li>
        <li>HỖ TRỢ</li>
        <li>ĐẶT HÀNG</li>
    </ul>
    <ul>
        <li><i class="fa fa-phone cls-icon" aria-hidden="true"></i></li>
        <li>HOTLINE: <b class="text_red">1900 1090</b></li>
        <li>HOTLINE: <b class="text_red">1900 1090</b></li>
    </ul>
</div>