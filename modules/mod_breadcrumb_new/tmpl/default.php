<?php
// No direct access
defined('_JEXEC') or die;
?>

<div class="breadcrumb_new">
    <div class="bg-breadcrumb_new">
        <ul class="ul_breadcrumb_new">
            <?php
            foreach ($list as $k => $l) {
                if ($k == 0) {
                    ?>
                    <li class="home">
                        <a href="<?php echo $l->link ?>">
                            <i class="fa fa-home" aria-hidden="true"></i>
                        </a>
                        <div class="arrow-right"></div>
                    </li>
                    <?php
                } else {
                    $cls = 'brc';
                    $link = $l->link;

                    if ($k == count($list) - 1) {
                        $cls = 'brc_active';
                        $link = '#';
                    }

                    if ($k == count($list) - 1) {
                        if ($params->get('showLast')) {
                            ?>
                            <li class="<?php echo $cls ?>">
                                <a href="<?php echo $link; ?>">
                                    <?php echo $l->name ?>
                                </a>
                                <div class="bg-arrow-right"></div>
                                <div class="arrow-right"></div>
                            </li>
                            <?php
                        }
                    } else {
                        ?>
                        <li class="<?php echo $cls ?>">
                            <a href="<?php echo $link; ?>">
                                <?php echo $l->name ?>
                            </a>
                            <div class="bg-arrow-right"></div>
                            <div class="arrow-right"></div>
                        </li>
                        <?php
                    }
                }
            }
            ?>
        </ul>
    </div>
</div>