<?php

// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

// Get the breadcrumbs
$list = Modbreadcrumb_newHelper::getList($params);
$count = count($list);

// Set the default separator
$separator = Modbreadcrumb_newHelper::setSeparator($params->get('separator'));
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_breadcrumb_new');
