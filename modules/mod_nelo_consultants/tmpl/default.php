<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .consultants ul li{
        text-align: center;
        padding: 40px 40px 70px 40px;
    }
    .consultants ul li div img{
        width: 150px;
        height: 150px;
        border-radius: 150px;
    }
    .title-consultants{
        text-align: center;
        padding: 40px 0 0 0;
        font-size: 40px;
        font-weight: bold;
        font-family: serif;
    }
</style>

<div class="consultants">
    <div class="title-consultants wow fadeInDown">
        <span><?php echo $params->get('title') ?></span>
    </div>
    <ul>
        <?php
        $t = substr($params->get('selectUser'), 1, 1);
        for ($i = 1; $i <= $t; $i++) {
            if ($i % 2 == 0) {
                $eff = 'bounceInRight';
            } else {
                $eff = "bounceInLeft";
            }
            ?>
            <li class="col-md-<?php echo 12 / $t ?> wow <?php echo $eff ?>">
                <div>
                    <img class="lazy" data-original="<?php echo $params->get('image' . $i) ?>" />
                </div>
                <h4><?php echo $params->get('name' . $i) ?></h4>
                <div style="font-weight: bold;"><?php echo $params->get('position' . $i) ?></div>
                <div><?php echo $params->get('phone' . $i) ?></div>
                <div><?php echo $params->get('email' . $i) ?></div>
            </li>
    <?php
}
?>
    </ul>
</div>

<script>
    var wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });

    wow.init();
</script>