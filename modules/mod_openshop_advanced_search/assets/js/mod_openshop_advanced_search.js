$ = jQuery.noConflict();
function search(id) {
    var danhmuc = id.slice(0, id.lastIndexOf("_"));
    var index = id.substr(id.lastIndexOf("_"));
    switch (danhmuc) {
        case "kichthuoc":
        {
            var nameclass = jQuery('#' + id).attr('class');
            if (nameclass == "icon-checkbox-unchecked") {
                jQuery('#' + id).removeClass(nameclass);
                jQuery('#' + id).addClass('icon-checkin');
                jQuery('#'+danhmuc+"input"+index).attr('checked', true);
                var val = jQuery('#'+danhmuc+"input"+index).val();
                jQuery('#'+danhmuc+"input"+index).val(val+"_1");
            } else {
                jQuery('#' + id).removeClass(nameclass);
                jQuery('#' + id).addClass('icon-checkbox-unchecked');
                jQuery('#'+danhmuc+"input"+index).attr('checked', false);
                var val2 = jQuery('#'+danhmuc+"input"+index).val();
                jQuery('#'+danhmuc+"input"+index).val(val2.slice(0, val2.lastIndexOf("_")));//value index_1 là có checked
            }
            break;
        }
        case "gia":
        {
            var nameclass = jQuery('#' + id).attr('class');
            if (nameclass == "icon-checkbox-unchecked") {
                jQuery('#' + id).removeClass(nameclass);
                jQuery('#' + id).addClass('icon-checkin');
                
//                alert('#'+danhmuc+"input"+index);
                jQuery('#'+danhmuc+"input"+index).attr('checked', true);
                var val = jQuery('#'+danhmuc+"input"+index).val();
                jQuery('#'+danhmuc+"input"+index).val(val+"_1");
                var index2 = index.substr(index.lastIndexOf("_")-1);
                for (var i = 0;i<5;i++){
                    if(i!=index2){
                        jQuery('#gia_' + i).removeClass(nameclass);
                        jQuery('#gia_' + i).addClass('icon-cancel');
                    }
                }
            } else if(nameclass == "icon-checkin") {
                jQuery('#' + id).removeClass(nameclass);
                jQuery('#' + id).addClass('icon-checkbox-unchecked');
                
                jQuery('#'+danhmuc+"input"+index).attr('checked', false);
                var val2 = jQuery('#'+danhmuc+"input"+index).val();
                jQuery('#'+danhmuc+"input"+index).val(val2.slice(0, val2.lastIndexOf("_")));
                
                var index2 = index.substr(index.lastIndexOf("_")-1);
                for (var i = 0;i<5;i++){
                    if(i!=index2){
                        jQuery('#gia_' + i).removeClass("icon-cancel");
                        jQuery('#gia_' + i).addClass('icon-checkbox-unchecked');
                    }
                }
            }
            break;
        }
        case "categories":
        {
            var nameclass = jQuery('#' + id).attr('class');
            if (nameclass == "icon-checkbox-unchecked") {
                jQuery('#' + id).removeClass(nameclass);
                jQuery('#' + id).addClass('icon-checkin');
                jQuery('#'+danhmuc+"input"+index).attr('checked', true);
                var val = jQuery('#'+danhmuc+"input"+index).val();
                jQuery('#'+danhmuc+"input"+index).val(val+"_1");
            } else {
                jQuery('#' + id).removeClass(nameclass);
                jQuery('#' + id).addClass('icon-checkbox-unchecked');
                jQuery('#'+danhmuc+"input"+index).attr('checked', false);
                var val2 = jQuery('#'+danhmuc+"input"+index).val();
                jQuery('#'+danhmuc+"input"+index).val(val2.slice(0, val2.lastIndexOf("_")));//value index_1 là có checked
            }


            break;
        }
    }

}
function setimg(id_iconthuonghieu, id_imgthuonghieu) {
    //icon-delete
    var nameicon = id_iconthuonghieu.slice(0, id_iconthuonghieu.lastIndexOf("_"));
    var index = id_iconthuonghieu.substr(id_iconthuonghieu.lastIndexOf("_"));
    var nameimg = id_imgthuonghieu.slice(0, id_imgthuonghieu.lastIndexOf("_"));
    switch (nameicon) {
        case "iconthuonghieu":
        {
            var icon = jQuery('#' + id_iconthuonghieu).attr('class');
            if (icon == "") {
                jQuery('#' + id_iconthuonghieu).addClass('icon-delete');
                
                jQuery('#'+nameicon+"input"+index).attr('checked', true);
                var val = jQuery('#'+nameicon+"input"+index).val();
                jQuery('#'+nameicon+"input"+index).val(val+"_1");
            } else {
                jQuery('#' + id_iconthuonghieu).removeClass(icon);
                
                jQuery('#'+nameicon+"input"+index).attr('checked', false);
                var val2 = jQuery('#'+nameicon+"input"+index).val();
                jQuery('#'+nameicon+"input"+index).val(val2.slice(0, val2.lastIndexOf("_")));
            }
            break;
        }
    }
}

function setimg2(id_icon, id_img) {
    var nameicon = id_icon.slice(0, id_icon.lastIndexOf("_"));
    var index = id_icon.substr(id_icon.lastIndexOf("_"));
    var nameimg = id_img.slice(0, id_img.lastIndexOf("_"));
    switch (nameicon) {
        case "iconcolor":
        {
            var icon = jQuery('#' + id_icon).attr('class');
            if (icon == "") {
                jQuery('#' + id_icon).addClass('icon-cancel-2');
                jQuery('#' + id_img).removeClass('img_attr_brand_img');
                jQuery('#' + id_img).addClass('img_attr_brand_img_red');
                
                jQuery('#'+nameicon+"input"+index).attr('checked', true);
                var val = jQuery('#'+nameicon+"input"+index).val();
                jQuery('#'+nameicon+"input"+index).val(val+"_1");
            } else {
                jQuery('#' + id_icon).removeClass(icon);
                jQuery('#' + id_img).removeClass('img_attr_brand_img_red');
                jQuery('#' + id_img).addClass('img_attr_brand_img');
                
                jQuery('#'+nameicon+"input"+index).attr('checked', false);
                var val2 = jQuery('#'+nameicon+"input"+index).val();
                jQuery('#'+nameicon+"input"+index).val(val2.slice(0, val2.lastIndexOf("_")));
            }

            break;
        }
    }
}