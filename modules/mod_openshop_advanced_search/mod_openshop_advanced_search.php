<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

error_reporting(E_ALL);
// no direct access
defined('_JEXEC') or die('Restricted access');
require_once (dirname(__FILE__).'/helper.php');
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/defines.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/inflector.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/autoload.php';
$categories = modOpenShopAdvancedSearchHelper::getCategories_parent();//modOpenShopAdvancedSearchHelper::getCategories($params->get('child_categories_level', 9999));
$brands = modOpenShopAdvancedSearchHelper::getBrands();
$attributeGroups = modOpenShopAdvancedSearchHelper::getAttributeGroups();
$options = modOpenShopAdvancedSearchHelper::getOptions();
$template = JFactory::getApplication()->getTemplate();

//Get currency symbol
$currency = new OpenShopCurrency();
$currencyCode = $currency->getCurrencyCode();
$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select('left_symbol, right_symbol')
	->from('#__openshop_currencies')
	->where('currency_code = ' . $db->quote($currencyCode));
$db->setQuery($query);
$row = $db->loadObject();
($row->left_symbol) ? $symbol = $row->left_symbol : $symbol = $row->right_symbol;

//Get weight unit
$weight = new OpenShopWeight();
$weightId = OpenShopHelper::getConfigValue('weight_id');
$weightUnit = $weight->getUnit($weightId);

//Get length unit
$length = new OpenShopLength();
$lengthId = OpenShopHelper::getConfigValue('length_id');
$lengthUnit = $length->getUnit($lengthId);

//Get submitted values
//$minPrice = str_replace($symbol, '', htmlspecialchars(JRequest::getVar('min_price'), ENT_COMPAT, 'UTF-8'));
//$maxPrice = str_replace($symbol, '', htmlspecialchars(JRequest::getVar('max_price'), ENT_COMPAT, 'UTF-8'));
$minWeight = str_replace($weightUnit, '', htmlspecialchars(JRequest::getVar('min_weight'), ENT_COMPAT, 'UTF-8'));
$maxWeight = str_replace($weightUnit, '', htmlspecialchars(JRequest::getVar('max_weight'), ENT_COMPAT, 'UTF-8'));
$minLength = str_replace($lengthUnit, '', htmlspecialchars(JRequest::getVar('min_length'), ENT_COMPAT, 'UTF-8'));
$maxLength = str_replace($lengthUnit, '', htmlspecialchars(JRequest::getVar('max_length'), ENT_COMPAT, 'UTF-8'));
$minWidth = str_replace($lengthUnit, '', htmlspecialchars(JRequest::getVar('min_width'), ENT_COMPAT, 'UTF-8'));
$maxWidth = str_replace($lengthUnit, '', htmlspecialchars(JRequest::getVar('max_width'), ENT_COMPAT, 'UTF-8'));
$minHeight = str_replace($lengthUnit, '', htmlspecialchars(JRequest::getVar('min_height'), ENT_COMPAT, 'UTF-8'));
$maxHeight = str_replace($lengthUnit, '', htmlspecialchars(JRequest::getVar('max_height'), ENT_COMPAT, 'UTF-8'));
$productInStock = JRequest::getVar('product_in_stock');
$categoryIds = JRequest::getVar('category_ids');
$minpriceIds  = JRequest::getVar('min_price');
$minpriceIds = JRequest::getVar('min_price');
if (!$minpriceIds) {
    $minpriceIds = array();
}
if (!$categoryIds) {
    $categoryIds = array();
} else 
{
	$categoryIds = explode(',', $categoryIds);
}
//exit();
$manufacturerIds = JRequest::getVar('manufacturer_ids');

if (!$manufacturerIds)
{
	$manufacturerIds = array();
}
else 
{
	$manufacturerIds = explode(',', $manufacturerIds);
}
$attributeIds = JRequest::getVar('attribute_ids');
if (!$attributeIds)
{
	$attributeIds = array();
}
else 
{
	$attributeIds = explode(',', $attributeIds);
}
$optionValueIds = JRequest::getVar('optionvalue_ids');
if (!$optionValueIds)
{
	$optionValueIds = array();
}
else 
{
	$optionValueIds = explode(',', $optionValueIds);
}
$keyword = JRequest::getString('keyword');
if (!empty($keyword))
{
	$keyword = htmlspecialchars($keyword, ENT_COMPAT, 'UTF-8');
}
$itemId = $params->get('item_id');
if (!$itemId)
{
	$itemId = OpenShopRoute::getDefaultItemId();
}
// Load Bootstrap CSS and JS
if (OpenShopHelper::getConfigValue('load_bootstrap_css'))
{
	OpenShopHelper::loadBootstrapCss();
}
if (OpenShopHelper::getConfigValue('load_bootstrap_js'))
{
	OpenShopHelper::loadBootstrapJs();
}
$document = JFactory::getDocument();
$document->addScript(OpenShopHelper::getSiteUrl().'components/com_openshop/assets/js/noconflict.js');
if (JFile::exists(JPATH_ROOT.'/templates/'. $template .  '/css/'  . $module->module . '.css'))
{
	$document->addStyleSheet(JURI::base().'templates/' . $template . '/css/' . $module->module . '.css');
}
else 
{
	$document->addStyleSheet(JURI::base().'modules/' . $module->module . '/assets/css/style.css');
}
$document->addStyleSheet(OpenShopHelper::getSiteUrl().'modules/mod_openshop_advanced_search/assets/css/jquery.nouislider.css');
$document->addScript(OpenShopHelper::getSiteUrl().'modules/mod_openshop_advanced_search/assets/js/jquery.nouislider.min.js');

require(JModuleHelper::getLayoutPath('mod_openshop_advanced_search'));
