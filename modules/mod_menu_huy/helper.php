<?php
class Modmenu_huyHelper
{
    public static function getmenu_huy($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,b.category_name')
                ->from($db->quoteName('#__openshop_categories','a'))
                ->join('INNER', $db->quoteName('#__openshop_categorydetails','b') . 'ON a.id = b.category_id')
                ->where('category_parent_id = 0');
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getMenu_Child($id_parent){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,b.category_name')
                ->from($db->quoteName('#__openshop_categories','a'))
                ->join('INNER', $db->quoteName('#__openshop_categorydetails','b') . 'ON a.id = b.category_id')
                ->where('category_parent_id = ' . $id_parent);
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getNumInCart(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('count(id)')
                ->from($db->quoteName('#__openshop_cartproducts'));
        if(JFactory::getSession()->get('user')->id){
            $query->where('user_id = ' . JFactory::getSession()->get('user')->id , 'OR');
        }
        
        $query->where('ip = "'. $_SERVER['REMOTE_ADDR'] .'"');
        
        return $db->setQuery($query)->loadResult();
    }
    
    public static function getmenus5($params,$paren_id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__menu'))
                ->where('menutype = "'. $params->get('menu_id') .'"')
                ->where('parent_id = '. $paren_id)
                ->Order('lft');
        return $db->setQuery($query)->loadObjectList();
    }
    
}
