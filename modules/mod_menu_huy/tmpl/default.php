<?php
// No direct access
defined('_JEXEC') or die;

$db = JFactory::getDbo();
$query = $db->getQuery(TRUE);
$session = JFactory::getSession();
?>

<style>
    .main-menu{
        background: <?php echo $params->get('colorMenu') ?>;
        color: #000;
    }
    .content-main-menu{
        margin: 0 auto;
        width: <?php echo $params->get('contentMenu') . 'px' ?>;
    }
    ul.m li{
        list-style: none;
    }
    .content-main-menu > ul > li{
        display: inline-table;
        cursor:pointer;
    }
    .content-main-menu > ul > li{
        padding: <?php echo $params->get('paddingMenu') . 'px' ?> 10px;
    }
    .menu-child{
        display: none;
        margin: 10px 0 0 -10px;
    }
    .content-main-menu > ul > li:hover{
        background: #2BB700;
    }
    .content-main-menu > ul > li:hover .menu-child{
        display: block;
    }
    .menu-child{
        position: absolute;
        box-shadow: 0 3px 10px 2px #969696;
        background: white;
        color: #444444;
        width: 200px;
        z-index: 10000;
    }
    .menu-child ul li{
        padding: 10px 15px;
        border-bottom: 1px solid #D4D4D4;
    }
    .menu-child ul li:hover{
        background: #2D940E;
        color: white;
    }
    .menu-child-1{
        display: none;
        position: absolute;
        width: 200px;
        left: 200px;
        background: #FFFFFF;
        margin-top: -20px;
        color: #292929;
        box-shadow: 0px 4px 7px -1px #797979;
    }
    .menu-child ul li:hover .menu-child-1{
        display: block;
    }
    ol, ul{
        margin-bottom: 0px;
    }
    .menuInLogo{
        float: right;
        padding-top: <?php echo $params->get('paddingMenu') . 'px' ?>;
        padding-bottom: <?php echo $params->get('paddingMenu') . 'px' ?>;
    }

    .menuInLogo ul li{
        z-index: 10000;
    }
    .cf:before, .cf:after {
        content:" ";
        display: table;
    }
    .cf:after {
        clear: both;
    }
    .cf {
        *zoom: 1;
    }
    .menu {
        list-style:none;
        width: -moz-fit-content;
        width: -webkit-fit-content;
        width: fit-content;
    }
    .menu > li {
        background: <?php echo $params->get('colorMenu') ?>;
        float: left;
        position: relative;
        -webkit-transform: skewX(25deg);
    }
    .menu a {
        display: block;
        color: #fff;
        text-transform: uppercase;
        text-decoration: none;
        font-family: Arial, Helvetica;
        font-size: 14px;
    }
    .menu li:hover {
        background: <?php echo $params->get('colorHover') ?>;
    }
    .menu > li > a {
        -webkit-transform: skewX(-25deg);
        padding: 1em 2em;
    }
    /* Dropdown */
    .submenu {
        position: absolute;
        width: 200px;
        left: 50%;
        margin-left: -100px;
        -webkit-transform: skewX(-25deg);
        -webkit-transform-origin: left top;
    }
    .submenu li {
        background-color: <?php echo $params->get('colorMenu') ?>;
        position: relative;
        overflow: hidden;
    }
    .submenu > li > a {
        padding: 1em 2em;
    }
    .submenu > li::after {
        content:'';
        position: absolute;
        top: -125%;
        height: 100%;
        width: 100%;
        box-shadow: 0 0 50px rgba(0, 0, 0, .9);
    }
    /* Odd stuff */
    .submenu > li:nth-child(odd) {
        -webkit-transform: skewX(-25deg) translateX(0);
    }
    .submenu > li:nth-child(odd) > a {
        -webkit-transform: skewX(25deg);
    }
    .submenu > li:nth-child(odd)::after {
        right: -50%;
        -webkit-transform: skewX(-25deg) rotate(3deg);
    }
    /* Even stuff */
    .submenu > li:nth-child(even) {
        -webkit-transform: skewX(25deg) translateX(0);
    }
    .submenu > li:nth-child(even) > a {
        -webkit-transform: skewX(-25deg);
    }
    .submenu > li:nth-child(even)::after {
        left: -50%;
        -webkit-transform: skewX(25deg) rotate(3deg);
    }
    /* Show dropdown */
    .submenu, .submenu li {
        opacity: 0;
        visibility: hidden;
    }
    .submenu li {
        transition: .2s ease -webkit-transform;
    }
    .menu > li:hover .submenu, .menu > li:hover .submenu li {
        opacity: 1;
        visibility: visible;
    }
    .menu > li:hover .submenu li:nth-child(even) {
        -webkit-transform: skewX(25deg) translateX(15px);
    }
    .menu > li:hover .submenu li:nth-child(odd) {
        -webkit-transform: skewX(-25deg) translateX(-15px);
    }
    .reponsive-menu{
        display: none;
    }

    @media(max-width: 980px){
        .cf  li  a{
            font-size: 12px;
        }
    }
    @media(max-width: 880px){
        .cf  li  a{
            font-size: 10px;
        }
    }
    @media(max-width: 770px){
        .cf{
            display: none;
        }
        .reponsive-menu{
            display: block;
        }
        .reponsive-menu i{
            font-size: 35px;
            cursor: pointer;
        }
        .reponsive-menu-content{
            position: fixed;
            top: 0;
            right: 0;
            background: rgba(82, 82, 82, 0.95);
            width: 200px;
            height: 100%;
            z-index: 1000000;
            overflow: auto;
            padding: 0;
            display: none;
        }
        .reponsive-menu-content a{
            text-decoration: none;
            color: white;
            cursor:pointer;
        }
        .reponsive-menu-content .back-menu{
            text-align: center;
        }
        .reponsive-menu-content .back-menu i{
            color:white;
        }

        .reponsive-menu-content > li{
            padding: 10px;
            border-bottom: 1px solid rgba(255, 255, 255, 0.16);
            cursor: pointer;
        }

        .reponsive-menu-content  li:hover{
            background: rgba(184, 54, 216, 0.48);
        }

        .reponsive-content-sub{
            padding: 10px 10px 10px 25px !important;
        }
        .icon-show{
            font-size: 20px !important;
            color: white;
            float: right;
        }
    }


    /*//MENU 3*/
    ul.menu3{
        background: <?php echo $params->get('colorMenu') ?>;
        padding: <?php echo $params->get('paddingMenu') . 'px' ?> 0;
        width: <?php echo $params->get('contentMenu') . $params->get('MenuP') ?>;
        margin: 0 auto;

    }
    .menu3 > li{
        display: inline;
        padding: <?php echo $params->get('paddingMenu') + 2 . 'px' ?>;
        position: relative;
        cursor:pointer;
    }
    .menu3 > li:hover{
        /*background: <?php echo $params->get('colorHover') ?>;*/
        background: <?php echo OpenShopHelper::getConfigValue('t_background_color'); ?>;
    }
    .menu3 > li:hover .sub_menu3{
        opacity: 1;
        transform:translate(0,0);
        transition: 0.5s all;
        z-index: 100000;
    }
    .menu3 li a{
        text-decoration: none;
        color: #fff;
    }
    .footmenu3{
        padding: 2px;
        /*background: <?php echo $params->get('colorHover') ?>;*/
        background: <?php echo OpenShopHelper::getConfigValue('t_background_color'); ?>;
        width: <?php echo $params->get('contentMenu') . $params->get('MenuP') ?>;
        margin: 0 auto;
    }
    .active_menu3{
        /*background: <?php echo $params->get('colorHover') ?>;*/
        background: <?php echo OpenShopHelper::getConfigValue('t_background_color'); ?>;
    }
    .sub_menu3{
        position: absolute;
        width: 630px;
        background: white;
        padding: 10px;
        margin: 10px 0 0 0;
        transform: rotateX(90deg);
        opacity: 0;
        z-index: 0;
        left: 0;
    }
    .sub_menu3 li{
        list-style: none;
        display: inline-table;
        padding: 10px 10px 10px 12px;
        width: 190px;
    }
    .sub_menu3 li a{
        color:black;
    }
    .sub_menu3 li a:hover{
        /*color: <?php echo $params->get('colorHover') ?>;*/
        color: <?php echo OpenShopHelper::getConfigValue('t_background_color'); ?>;
        /*color: <?php echo $session->get('background_color') ?>;*/
    }
    .sub2_menu3 li{
        margin: 0 0 0 -10px;
    }
</style>

<?php
if ($params->get('selectMenu') == "m1") {
    require 'menu_1.php';
} else if ($params->get('selectMenu') == "m2") {
    require 'menu_2.php';
} else if ($params->get('selectMenu') == "m3") {
    ?>
    <ul class="menu3">
        <li class="iconMainMenu" onclick="showMenu3()">
            <a>
                <i class="fa fa-bars" aria-hidden="true"></i> Danh mục
            </a>
        </li>
        <div class="bg-menu3" onclick="hideMenuResponsive()"></div>
        <div class="menu3 menuResponsive">
            <?php
            $id_parent = '';
            $Itemid = JFactory::getApplication()->input->get('Itemid');
            if (isset($Itemid)) {
                $id_parent = Modmenu_huyHelper::getIdParentMenu(JFactory::getApplication()->input->get('Itemid'));
            }
            foreach ($menus as $value) {
                $active_menu3 = '';
                if ($id_parent == $value->id) {
                    $active_menu3 = 'active_menu3';
                }
                ?>
                <li class="<?php echo $active_menu3 ?>">
                    <?php
                    $nameMenu = explode('|', $value->title);
                    echo '<a href="' . $value->link . '&Itemid=' . $value->id . '">' . $nameMenu[0] . '</a>';

                    //menu children 1
                    $query->clear();
                    $query->select('*')
                            ->from($db->quoteName('#__menu'))
                            ->where('menutype = "' . $params->get('menu_id') . '"')
                            ->where('parent_id = ' . $value->id);
                    $rowsMC = $db->setQuery($query)->loadObjectList();

                    if (count($rowsMC)) {
                        ?>
                        <ul class="sub_menu3 subM31">
                            <?php
                            foreach ($rowsMC as $key => $rowMC) {
                                echo ' <li>';
                                echo '  <a href="' . $rowMC->link . '&Itemid=' . $rowMC->id . '"><b>' . $rowMC->title . '</b></a>';
                                $query->clear();
                                $query->select('*')
                                        ->from($db->quoteName('#__menu'))
                                        ->where('menutype = "' . $params->get('menu_id') . '"')
                                        ->where('parent_id = ' . $rowMC->id);
                                $rowsMC1 = $db->setQuery($query)->loadObjectList();
                                if (count($rowsMC1)) {
                                    echo '  <ul class="sub2_menu3">';
                                    foreach ($rowsMC1 as $chd) {
                                        echo '<li><a href="' . $chd->link . '&Itemid=' . $chd->id . '">' . $chd->title . '</a></li>';
                                    }
                                    echo '  </ul>';
                                    echo '</li>';
                                }
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                </li>
                <?php
            }
            ?>
        </div>
    </ul>
    <div class="footmenu3"></div>
    <?php
} else if ($params->get('selectMenu') == "m4") {
    require 'menu_4.php';
}
else if($params->get('selectMenu') == "m5"){
    require 'menu_5.php';
}
?>

<script>
    function showMenu() {
        jQuery('.reponsive-menu-content').show(200);
    }
    function hideMenu() {
        jQuery('.reponsive-menu-content').hide(200);
    }

    if (jQuery(window).width() < 1024) {
        jQuery('.menuResponsive').removeClass('menu3');
        jQuery('.subM31').removeClass('sub_menu3');
        jQuery('.subM31').addClass('sub_menu3Rp');
    }
</script>