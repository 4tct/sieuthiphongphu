<style>
    .m_4{
        background: #da1400;
    }
    .m_4 > ul{
        padding: 0 30px;
    }
    .m_4 > ul > li{
        padding: 15px;
        cursor: pointer;
        display: inline-block;
        color: white;
        font-weight: bold;
        margin: 0 0 0 -4px;
    }
    .m_4 > ul > li > a{
        color: white;
        text-decoration: none;
    }
    .m_4 > ul > li:hover{
        background: #961b0e;
    }
    .m_con{
        position: absolute;
        z-index: 100;
        background: white;
        width: 250px;
        /*display: none;*/
        opacity: 0;
        transform: translate(0px,20px);
    }
    .m_con ul{
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .m_con ul li{
        padding: 13px;
        border-bottom: 1px solid #ababab;
        cursor: pointer;
    }
    .m_con ul li a{
        text-decoration: none;
        color: black;
    }
    .float_right{
        float: right !important;
    }
    .active_m_4{
        background: #961b0e;
    }
    .form_search_m_4{
        display: inline-block;
        padding: 0 0 0 20px;
    }
    .form_search_m_4 input[type=text]{
        border: 0;
        padding: 7px;
        outline: 0;
        width: 410px;
    }
    .form_search_m_4 input[type=submit]{
        border: 0;
        padding: 7px;
        outline: 0;
        color: #da1400;
    }
    .cart_m_4{
        display: inline-block;
        border: 1px solid white;
        padding: 7px 15px;
        border-radius: 50px;
        background: white;
        font-weight: bold;    
        margin-left: 15px;
    }
    .cart_m_4 .num_cart_m_4{
        padding: 5px 9px;
        background: red;
        border-radius: 100px;
        color: white;
    }
    .m_4_child{
        position: absolute;
        width: 250px;
        left: 250px;
        background: #ffffff;
        /*top: 0;*/
        border-left: 1px solid #adadad;
        margin-top: -33px;
        display: none;
    }
    .m_con ul li:hover{
        background: #961b0e;
        color: white;
    }
    .m_con > ul li:hover > a{
        color: white;
    }
    .m_con ul li:hover ul.m_4_child{
        display: block;
        margin-top: -33px;
    }
    .m_4_child li a{
        color: black;
        text-decoration: none;
    }
    .m_4_child li:hover a{
        color: white;
    }
    .cart_m_4 a{
        text-decoration: none;
        color: black;
    }
    .resultQuickSearch{
        position: absolute;
        z-index: 100000;
        background: white;
        width: 410px;
        border: 1px solid #e6e6e6;
        display: none;
    }
    .resultQuickSearch ul{
        list-style: none;
        box-shadow: 0px 3px 10px red;
    }
    .resultQuickSearch ul li{
        border-bottom: 1px solid #bfbfbf;
        padding: 10px;
    }
    .resultQuickSearch ul li a{
        text-decoration: none;
        color: black;
    }
    .resultQuickSearch .infoProSearch{
        display: inline-block;
        position: absolute;
        margin: 0 0 0 10px;
    }
    .resultQuickSearch .all_size_pro{
        padding: 5px 0 0 0;
    }
    .resultQuickSearch .price_pro{
        color: red;
        font-size: 20px;
        padding: 5px 0 0 0;
        display: inline-block;
    }
    .resultQuickSearch .price_pro_not{
        display: inline-block;
        margin-left: 10px;
        text-decoration: line-through;
        color: #b3b3b3;
        display: inline-block;
    }
    .resultQuickSearch ul li:hover{
        background: #f7f7f7;
        cursor: pointer;
    }
</style>

<div class="m_4">
    <ul>
        <li class="active_m_4 menu_m4" style="width: 250px;margin-left: 0px;">
            <i class="fa fa-bars" aria-hidden="true"></i> DANH MỤC SẢN PHẨM
        </li>
        <?php 
        $view = $app->input->getCmd('view', '');
        $cls = 'menu_m4_ef_hide';
        if($view == 'frontpage'){
            $cls = 'menu_m4_ef_show';
        }
        ?>
        <div class="m_con <?php echo $cls ?>">
            <ul>
                <?php
                foreach ($menus as $menu) {
                    $link = JRoute::_(OpenShopRoute::getCategoryRoute($menu->id));
                    ?>
                    <li onclick="location.href = '<?php echo $link ?>'"> 
                        <a href="<?php echo $link ?>"><?php echo $menu->category_name ?></a>
                        <?php
                        $menu_child = Modmenu_huyHelper::getMenu_Child($menu->id);
                        if (count($menu_child)) {
                            ?>
                            <i class="fa fa-angle-right float_right" aria-hidden="true"></i>

                            <ul class="m_4_child">
                                <?php
                                foreach ($menu_child as $m_c) {
                                    $link_sub = JRoute::_(OpenShopRoute::getCategoryRoute($m_c->id));
                                    ?>
                                    <li onclick="location.href = '<?php echo $link_sub ?>'"> 
                                        <a href="<?php echo $link_sub ?>"><?php echo $m_c->category_name ?></a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <li>
            <i class="fa fa-flag" aria-hidden="true"></i> 
            <a href="#"> KHUYẾN MÃI</a>
        </li>
        <li>
            <i class="fa fa-fire" aria-hidden="true"></i> 
            <a href="#"> XU HƯỚNG</a>
        </li>
        <div class="form_search_m_4">
            <form action="/tim-kiem.html" method="post" id="searchProductM4">
                <input value="" type="text" name="filter_search" id="search" placeholder="Tìm kiếm..." onkeyup="searchQuick(this.value)" value=""/>
                <input type="submit" value="Tìm kiếm" id="btn-seach" />
            </form>
            <div class="resultQuickSearch">
                <!--//search-->
            </div>
        </div>
        <div class="cart_m_4">
            <a href="/gio-hang.html">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i> GIỎ HÀNG
                <span class="num_cart_m_4"> <?php echo $num_in_cart ?> </span>
            </a>
        </div>
    </ul>
</div>

<script>
    function searchQuick(v) {
        if (v.length >= 4) {
            jQuery.ajax({
                url: 'index.php?option=com_openshop&format=ajax&task=product.searchQuick',
                method: 'post',
                data: {
                    v: v
                },
                success: function (dt) {
                    jQuery('.resultQuickSearch').css('display', 'block');
                    jQuery('.resultQuickSearch').html(dt);
                }
            });
        }
    }

    jQuery(function () {
        jQuery('#search').blur(function () {
//           jQuery('.resultQuickSearch').css('display','none');
        });

        jQuery('.menu_m4').on('click', function () {
            if (jQuery('.m_4 > ul > div').hasClass('menu_m4_ef_show')) {
                jQuery('.m_con').addClass('menu_m4_ef_hide');
                jQuery('.m_con').removeClass('menu_m4_ef_show');
            } else {
                jQuery('.m_con').addClass('menu_m4_ef_show');
                jQuery('.m_con').removeClass('menu_m4_ef_hide');
            }

        });
    });
</script>