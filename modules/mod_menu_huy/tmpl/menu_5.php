<style>
    .menu_5{
        padding: 0 25px;
        background: #1e2c42;
    }
    .menu_5 > ul{
        list-style: none;
    }
    .menu_5 > ul > li{
        display: inline-block;
        padding: 25px 10px;
        position: relative;
        cursor: pointer;
        border-top: 3px solid #1e2c42;
    }
    .menu_5 > ul > li, .menu_5 > ul > li > a{
        color: white;
        text-decoration: none;
        text-transform: uppercase;
        font-size: 12px;
    }
    ul.menu5_child_1{
        position: absolute;
        list-style: none;
        background: white;
        top: 67px;
        z-index: -1;
        width: 160px;
        opacity: 0;
        left: 0;
        transform: translate(10px, 0px);
        box-shadow: 0px 0px 5px 0px #c5c5c5;
    }
    ul.menu5_child_1 > li{
        padding: 10px 15px;
        position: relative;
    }
    ul.menu5_child_1 > li > a{
        color: black;
        text-decoration: none;
        text-transform: none;
    }

    /*hover*/
    .menu_5 ul.menu5_main > li:hover{
        background: #23344f;
        border-top: 3px solid orange;
    }
    .menu_5 ul.menu5_main > li:hover > a{
        color: orange;
    }
    .menu_5 ul.menu5_main > li:hover ul.menu5_child_1{
        opacity: 1;
        z-index: 1000;
        transform: translate(0px, 0px);
        transition: all 0.4s ease;
        -webkit-transition: all 0.4s ease;
    }
    ul.menu5_child_2{
        position: absolute;
        left: 160px;
        top: 0;
        width: 160px;
        background: #ffffff;
        text-transform: none;
        list-style: none;
        opacity: 0;
        z-index: -1;
        transform: translate(0px, 20px);
        -webkit-box-shadow: 0px 0px 5px 0px #c5c5c5;
        box-shadow: 0px 0px 5px 0px #c5c5c5;
    }
    ul.menu5_child_2 > li{
        padding: 10px;
    }
    ul.menu5_child_2 > li > a{
        color: black;
        text-decoration: none;
    }

    /*hover menu child 2*/
    ul.menu5_child_1 > li:hover i, ul.menu5_child_1 > li:hover > a, ul.menu5_child_2 li:hover > a{
        color: orange;
    }
    ul.menu5_child_1 > li:hover ul.menu5_child_2{
        opacity: 1;
        -webkit-transition: all 0.4s ease;
        transition: all 0.4s ease;
        transform: translate(0px, 0px);
    }
    .menu_5 .searchM5{
        float: right;
        margin-top: -50px;
    }
    .menu_5 .searchM5 #search{
        padding: 5px;
        outline: 0;
        border: 0;
    }
    .menu_5 .searchM5 .btn-searchM5{
        margin-left: -5px;
        border: 0;
        padding: 5px 14px;
        background: orange;
        color: white;
    }
    .menu5_fixed_top{
        position: fixed;
        z-index: 100000;
        width: 100%;
        top: 0;
    }
</style>

<div class="menu_5">
    <ul class="menu5_main">
        <?php
        foreach ($menus5 as $m5) {
            ?>
            <li onclick="location.href = '<?php echo JRoute::_($m5->link . '&Itemid=' . $m5->id) ?>'">
                <a href="<?php echo JRoute::_($m5->link . '&Itemid=' . $m5->id) ?>">
                    <?php echo $m5->title ?>
                    <?php
                    //menu con 1
                    $m5_c1 = Modmenu_huyHelper::getmenus5($params, $m5->id);
                    if (count($m5_c1)) {
                        echo '  <i class="fa fa-angle-down"></i>';
                        echo '</a>';
                        echo '<ul class="menu5_child_1">';
                        foreach ($m5_c1 as $c1) {
                            ?>
                            <li onclick="location.href = '<?php echo JRoute::_($c1->link . '&Itemid=' . $c1->id) ?>'">
                                <a href="<?php echo JRoute::_($c1->link . '&Itemid=' . $c1->id) ?>">
                                    <?php echo $c1->title ?>
                                    <!--</a>-->
                                    <?php
                                    //menu con 2
                                    $m5_c2 = Modmenu_huyHelper::getmenus5($params, $c1->id);
                                    if (count($m5_c2)) {
                                        echo '  <i class="fa fa-angle-right float_right text_black"></i>';
                                        echo '</a>';
                                        echo '<ul class="menu5_child_2">';
                                        foreach ($m5_c2 as $c2) {
                                            ?>
                                            <li onclick="location.href = '<?php echo JRoute::_($c2->link . '&Itemid=' . $c2->id) ?>'">
                                                <a href="<?php echo JRoute::_($c2->link . '&Itemid=' . $c2->id) ?>">
                                                    <?php echo $c2->title ?>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        echo '</ul>';
                                    } else {
                                        echo '</a>';
                                    }
                                    ?>

                            </li>
                            <?php
                        }
                        echo '</ul>';
                    } else {
                        echo '</a>';
                    }
                    ?>  
            </li>
            <?php
        }
        ?>
    </ul>
    <!--//search-->
    <div class="searchM5">
        <form action="/tim-kiem.html" method="post" id="searchProductM4">
            <input value="" type="text" name="filter_search" id="search" placeholder="Tìm kiếm..." value=""/>
            <button class="btn-searchM5">
                <i class="fa fa-search" aria-hidden="true"></i>
            </button>
        </form>
    </div>
</div>

<script>
//    jQuery(window).scroll(function(){
//       var s = jQuery(this).scrollTop();
//       if(s >= 43){
//           jQuery('.menu_5').addClass('menu5_fixed_top');
//       }
//       else{
//           jQuery('.menu_5').removeClass('menu5_fixed_top');
//       }
//       console.log(s);
//    });
</script>

<div class="clr"></div>

