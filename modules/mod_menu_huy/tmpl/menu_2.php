<div class="menu_5">
    <div class="width-100">
    
        <div class="col-sm-6 col-md-12 paddingR0">
            <ul class="hidden-desktop-1">
                <li class="zindex101">
                    <a onclick="showMainMenu()">
                        <span>MENU</span>
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
            <ul class="menu5_main">
                <?php
                foreach ($menus5 as $m5) {
                    ?>
                    <li onclick="location.href = '<?php echo JRoute::_($m5->link . '&Itemid=' . $m5->id) ?>'">
                        <a href="<?php echo JRoute::_($m5->link . '&Itemid=' . $m5->id) ?>">
                            <?php echo $m5->title ?>
                            <?php
                            //menu con 1
                            $m5_c1 = Modmenu_huyHelper::getmenus5($params, $m5->id);
                            if (count($m5_c1)) {
                                echo '  <i class="fa fa-angle-down"></i>';
                                echo '</a>';
                                echo '<ul class="menu5_child_1">';
                                foreach ($m5_c1 as $c1) {
                                    ?>
                                    <li onclick="location.href = '<?php echo JRoute::_($c1->link . '&Itemid=' . $c1->id) ?>'">
                                        <a href="<?php echo JRoute::_($c1->link . '&Itemid=' . $c1->id) ?>">
                                            <?php echo $c1->title ?>
                                            <!--</a>-->
                                            <?php
                                            //menu con 2
                                            $m5_c2 = Modmenu_huyHelper::getmenus5($params, $c1->id);
                                            if (count($m5_c2)) {
                                                echo '  <i class="fa fa-angle-right float_right text_black"></i>';
                                                echo '</a>';
                                                echo '<ul class="menu5_child_2">';
                                                foreach ($m5_c2 as $c2) {
                                                    ?>
                                                    <li onclick="location.href = '<?php echo JRoute::_($c2->link . '&Itemid=' . $c2->id) ?>'">
                                                        <a href="<?php echo JRoute::_($c2->link . '&Itemid=' . $c2->id) ?>">
                                                            <?php echo $c2->title ?>
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                                echo '</ul>';
                                            } else {
                                                echo '</a>';
                                            }
                                            ?>

                                    </li>
                                    <?php
                                }
                                echo '</ul>';
                            } else {
                                echo '</a>';
                            }
                            ?>  
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<?php
if ($params->get('showResponsive')) {
    ?>

    <div class="menu-responsive">
        <ul>
            <li class="liBackMenuResponsive" onclick="hideMenuResponsive()">
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
            </li>
            <?php
            foreach ($menus5 as $mres) {
                $link = JRoute::_($mres->link . '&Itemid=' . $mres->id);
                ?>
                <li>
                    <a href="<?php echo $link ?>">
                        <?php
                        echo $mres->title;
                        ?>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
    <div class="bg-menu-responsive" onclick="hideMenuResponsive()"></div>
    <?php
}
?>
<div class="clr"></div>

