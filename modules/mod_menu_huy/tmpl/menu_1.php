<style>
    .main-menu ul li{
        padding: 10px 0;
    }
    .main-menu ul li a{
        text-decoration: none;
        color: #000;
        padding: 3px 20px;
    }
    .main-menu ul li .borderRight{
        border-right: 1px solid #cac7c7;
    }
</style>

<ul>
    <?php
    foreach ($menus5 as $s => $menu) {
        $borderRight = '';
        if( $s != count($menus5) - 1 ){
            $borderRight = 'borderRight';
        }
        ?>
        <li class="display_inline_block text_uppercase">
            <a href="<?php echo JRoute::_( $menu->link . '&Itemid=' . $menu->id ) ?>" class="<?php echo $borderRight ?>">
                <?php echo $menu->title ?>
            </a>
        </li>
        <?php
    }
    ?>
</ul>

