<?php
class Modnelo_product_hotHelper
{
    public static function getnelo_product_hot($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . 'ON a.id = b.product_id')
                ->where('a.published = 1')
                ->where('product_hot = 1');
        return $db->setQuery($query)->loadObjectList();
    }
}
