<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$products_hot = Modnelo_product_hotHelper::getnelo_product_hot($params);
require JModuleHelper::getLayoutPath('mod_nelo_product_hot');