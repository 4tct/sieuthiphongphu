<?php
// No direct access
defined('_JEXEC') or die;
$user = JFactory::getSession()->get('user');
?>

<style>
    .product-hot{
        background: white;
    }
    .swiper-container-hot {
        width: 100%;
        height: 470px;
        padding: 10px 0 0 0;
    }
    @media(max-width: 1320px){
        .product-hot img,.product-hot .bg-featurePH, .product-hot .featurePH, .product-hot .infoPH{
            width: 280px;
        }
    }
    @media(max-width: 1220px){
        .product-hot img,.product-hot .bg-featurePH, .product-hot .featurePH, .product-hot .infoPH{
            width: 300px;
        }
    }
</style>

<div class="clr"></div>

<div class="product-hot">
    <div class="titlePH">
        <a class="tPH">Sản phẩm hot</a>
    </div>
    <div class="contentPH">
        <div class="swiper-container swiper-container-hot">
            <div class="swiper-wrapper">
                <?php
                foreach ($products_hot as $ph) {
                    $cartID = OpenShopHelper::getProductCategory($ph->id);
                    $urlP = JRoute::_(OpenShopRoute::getProductRoute($ph->id, $cartID));
                    $id = OpenShopHelper::convertBase64Encode($ph->id);
                    ?>
                    <div class="swiper-slide">
                        <?php
                        if ($ph->product_new) {
                            ?>
                            <div class="new-item bg-item"><span>New</span></div>
                            <?php
                        }

                        $sizes = OpenShopHelper::getProductOptionValueDetails($ph->id, 'size');
                        foreach ($sizes as $k => $s) {
                            ?>
                            <div class="showSizeP posS-<?php echo $k ?>"><?php echo $s->value ?></div>
                            <?php
                        }
                        ?>
                        <img onclick="location.href = '<?php echo $urlP ?>'" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ph->product_image ?>" width="300px" height="100%" alt="<?php echo $ph->product_name ?>"/>
                        <div class="bg-featurePH"></div>
                        <div class="featurePH">
                            <?php
                            if (!empty($user->id)) {
                                ?>
                                <span class="addcart" onclick="showChooseSize('<?php echo $id ?>')">
                                    <i class="fa fa-cart-plus" style="font-size: 18px;"></i> Thêm vào giỏ hàng
                                </span>
                                <?php
                                $onclick = '';
                                $db = JFactory::getDbo();
                                $query = $db->getQuery(TRUE);
                                $query->select('count(id)')
                                        ->from($db->quoteName('#__openshop_wishlists'))
                                        ->where('product_id = ' . $ph->id)
                                        ->where('customer_id = ' . $user->id);
                                $cw = $db->setQuery($query)->loadResult();
                                if (!$cw) {
                                    $cls = 'addlove';
                                    $tlt = 'Chưa thích sản phẩm';
                                    $onclick = 'wishProduct(\'' . $id . '\')';
                                } else {
                                    $cls = 'loved';
                                    $tlt = 'Đã thích sản phẩm';
                                    $onclick = 'nonwishProduct(\'' . $id . '\')';
                                }
                                ?>
                                <span class="<?php echo $cls; ?>" onclick="<?php echo $onclick; ?>" title="<?php echo $tlt ?>">
                                    <i class="fa fa-heart" style="font-size: 18px;"></i>
                                </span>
                                <?php
                            } else {
                                ?>
                                <span class="addcart" onclick="showChooseSize('<?php echo $id ?>')">
                                    <i class="fa fa-cart-plus" style="font-size: 18px;"></i> Thêm vào giỏ hàng
                                </span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="infoPH">
                            <div class="proname">
                                <a href="<?php echo $urlP; ?>" title="<?php echo $ph->product_name ?>"><?php echo $ph->product_name ?></a>
                            </div>
                            <div class="pricePH">
                                <?php
                                if ($ph->product_price_r) {
                                    ?>
                                    <div class="percent"><?php echo 100 - (int) ((int) ($ph->product_price * 100) / (int) $ph->product_price_r) . '%' ?></div>
                                    <div class="pricebuy"><?php echo number_format((int) $ph->product_price, '0', ',', '.') ?><sup>đ</sup></div>
                                    <div class="priceold"><?php echo number_format((int) $ph->product_price_r, '0', ',', '.') ?><sup>đ</sup></div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="pricebuy2"><?php echo number_format((int) $ph->product_price, '0', ',', '.') ?><sup>đ</sup></div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <!-- Add Pagination -->
            <!--<div class="swiper-pagination"></div>-->
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>

        <script>

            var n;
            var w = jQuery(window).width();
            if (w > 500 && w < 1115)
            {
                n = 2;
            } else {
                n = 1;
            }

            var swiper = new Swiper('.swiper-container-hot', {
                //        pagination: '.swiper-pagination',
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                slidesPerView: n,
                paginationClickable: true,
                spaceBetween: 30,
                autoplay: 5000
            });

//            jQuery(window).resize(function () {
//                if (jQuery(this).width() < 1115)
//                {
//                    nhot = 2;
//                } else
//                {
//                    nhot = 1;
//                }
//
//                var swiper = new Swiper('.swiper-container-hot', {
//                    //        pagination: '.swiper-pagination',
//                    nextButton: '.swiper-button-next',
//                    prevButton: '.swiper-button-prev',
//                    slidesPerView: nhot,
//                    paginationClickable: true,
//                    spaceBetween: 30,
//                    autoplay: 5000
//                });
//            });
        </script>
    </div>
</div>