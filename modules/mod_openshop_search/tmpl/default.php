<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
	<div class="openshop-search<?php echo $params->get( 'moduleclass_sfx' ) ?>">
        <div class="input-prepend">
            <span class="add-on"><i class="icon-search"></i></span>
            <input class="inputbox product_search" type="text" name="keyword" id="prependedInput" value="" placeholder="<?php echo JText::_('OPENSHOP_FIND_A_PRODUCT'); ;?>">
        </div>
		<ul id="openshop_result"></ul>
		<input type="hidden" name="live_site" id="live_site" value="<?php echo JURI::root(); ?>">
		<input type="hidden" name="image_width" id="image_width" value="<?php echo $params->get('image_width')?>">
		<input type="hidden" name="image_height" id="image_height" value="<?php echo $params->get('image_height')?>">
		<input type="hidden" name="category_ids" id="category_ids" value="<?php echo $params->get('category_ids') ? implode(',', $params->get('category_ids')) : ''; ?>">
		<input type="hidden" name="description_max_chars" id="description_max_chars" value="<?php echo $params->get('description_max_chars',50); ?>">
	</div>
<script type="text/javascript">
(function($){
	$(document).ready(function(){
		$('#openshop_result').hide();
		$('input.product_search').val('');
		$(window).click(function(){
			$('#openshop_result').hide();
		})
		function search() {
			var query_value = $('input.product_search').val();
			$('b#search-string').html(query_value);
			if(query_value !== ''){
				$('.product_search').addClass('openshop-loadding');
				$.ajax({
					type: "POST",
					url: $('#live_site').val() + "index.php?option=com_openshop&view=search&format=raw&layout=ajax<?php echo OpenShopHelper::getAttachedLangLink(); ?>",
					data: '&keyword=' + query_value + '&image_width=' + $('#image_width').val() + '&image_height=' + $('#image_height').val() + '&category_ids=' + $('#category_ids').val() + '&description_max_chars=' + $('#description_max_chars').val(),
					cache: false,
					success: function(html){
						$("ul#openshop_result").html(html);
						$('.product_search').removeClass('openshop-loadding');
					}
				});
			}return false;    
		}
		
		$("input.product_search").live("keyup", function(e) {
			//Set Timeout
			clearTimeout($.data(this, 'timer'));
			// Set Search String
			var search_string = $(this).val();
			// Do Search
			if (search_string == '') {
				$('.product_search').removeClass('openshop-loadding');
				$("ul#openshop_result").slideUp();
			}else{
				$("ul#openshop_result").slideDown('slow');
				$(this).data('timer', setTimeout(search, 100));
			};
		});
			
	});
})(jQuery);
</script>

<style>
#openshop_result
{
	 background-color:#ffffff;
	 width:<?php echo $params->get('width_result',270)?>px;
	 position:absolute;
	 z-index:9999;
}
</style>