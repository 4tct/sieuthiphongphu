window.addEvent("domready",function(){
	$$('.openshop_switch').each(function(el){
		el.setStyle('display','none');
		var style = (el.value == 1) ? 'on' : 'off';
		var openshop = new Element('div',{'class' : 'openshop-'+style});
		openshop.inject(el, 'after');
		openshop.addEvent("click", function(){
			if(el.value == 1){
				openshop.setProperty('class','openshop-off');
				el.value = 0;
			} else {
				openshop.setProperty('class','openshop-on');
				el.value = 1;
			}
		});
	});
});
