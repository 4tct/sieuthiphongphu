<?php

/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class modOpenShopSearchHelper {

    /**
     * 
     * Function to get Categories
     * @return categories list
     */
    public static function getCategories() {
        $categories = OpenShopHelper::getCategories(0, JFactory::getLanguage()->getTag(), true);
        for ($i = 0; $n = count($categories), $i < $n; $i++) {
            $categories[$i]->childCategories = OpenShopHelper::getCategories($categories[$i]->id, JFactory::getLanguage()->getTag(), true);
        }
        return $categories;
    }

    /**
     * 
     * Function to get id of parent category
     * @param int $categoryId
     * @return int id of parent category
     */
    public static function getParentCategoryId($categoryId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query = $query->select('category_parent_id')
                ->from('#__openshop_categories')
                ->where('id = ' . $categoryId);
        $db->setQuery($query);
        return $db->loadResult() ? $db->loadResult() : $categoryId;
    }

}
