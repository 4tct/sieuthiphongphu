<?php
class Modnelo_letterHelper
{
    public static function getnelo_letter($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,link')
                ->from($db->quoteName('#__menu'))
                ->where('id = ' . $params->get('link'));
        $res = $db->setQuery($query)->loadObject();
        return $res->link . '&Itemid=' . $res->id;
    }
}
