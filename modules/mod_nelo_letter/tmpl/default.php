<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .letter{
        padding: 20px;
        border: 1px solid #D0D0D0;
        background: white;
        margin: 20px 0 0 0;
    }
    .letter h2{
        margin-top: 0px;
        margin-bottom: 20px;
        font-weight: bold;
    }
    .linkC{
        text-align: center;
        padding: 30px 0;
    }
    .linkC a{
        padding: 10px 20px;
        background: #6CC550;
        color: white;
        cursor: pointer;
        text-decoration: none;
    }
    .linkC a:hover{
        background: #ccc;
        transition: 0.5s all;
    }
</style>

<div class="letter">
    <h2><?php echo $params->get('title') ?></h2>
    <p><?php echo $params->get('content') ?></p>
    <div class="linkC">
        <a href="<?php echo $link ?>">Liên hệ với chúng tôi</a>
    </div>
</div>