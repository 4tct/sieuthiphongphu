<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$link = Modnelo_letterHelper::getnelo_letter($params);
require JModuleHelper::getLayoutPath('mod_nelo_letter');