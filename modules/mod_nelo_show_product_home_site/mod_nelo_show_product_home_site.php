<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$category = Modnelo_show_product_home_siteHelper::getNameCategory($params);
$contentT2 = Modnelo_show_product_home_siteHelper::getContentT2($params);

$limit = (int)$params->get('numPro3') * (int)$params->get('numRow3');
$product_new = Modnelo_show_product_home_siteHelper::getProductNew($limit);

//$product_child = Modnelo_show_product_home_siteHelper::getProductSC($type,'12');

require JModuleHelper::getLayoutPath('mod_nelo_show_product_home_site');