<?php

class Modnelo_show_product_home_siteHelper {

    public static function getNameCategory($params, $limit = 4) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,a.category_name,b.category_image')
                ->from($db->quoteName('#__openshop_categorydetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_categories', 'b') . ' ON b.id = a.category_id')
                ->where('a.category_id = ' . $params->get('chooseCategory'));
        $name = $db->setQuery($query)->loadObject();

        $cat_sub = '';
        $catSub = $params->get('chooseCategorySub');
        if (!empty($catSub)) {
            $query->clear();
            $query->select('b.id,a.category_name')
                    ->from($db->quoteName('#__openshop_categorydetails', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_categories', 'b') . ' ON b.id = a.category_id')
                    ->where('b.id IN (' . implode(',', $params->get('chooseCategorySub')) . ')');
            $cat_sub = $db->setQuery($query)->loadObjectList();
        }
        //load product
        $limit = 10;
        $query->clear();
        $query->select('b.id,b.product_image,b.product_sku,b.product_price_r,b.product_price,c.product_name,b.product_price_r')
                ->from($db->quoteName('#__openshop_productcategories', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . ' ON b.id = a.product_id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . ' ON b.id = c.product_id')
                ->where('a.category_id = ' . $params->get('chooseCategory'))
                ->where('published = 1')
                ->where('delete_status = 0')
                ->order('b.product_hot, b.product_new');
        $products = $db->setQuery($query, 0, $limit)->loadObjectList();

        $res = array();
        $res['link_cat'] = JRoute::_(OpenShopRoute::getCategoryRoute($name->id));
        $res['name'] = $name->category_name;
        $res['id'] = $name->id;
        $res['cat_sub'] = $cat_sub;
        $res['products'] = $products;
        $res['img_cat'] = $name->category_image;

        return $res;
    }

    public static function getContentT2($params) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                ->join('INNER', $db->quoteName('#__openshop_productcategories', 'c') . 'ON a.id = c.product_id')
                ->where('a.delete_status = 0')
                ->where('a.published = 1')
                ->where('c.category_id = ' . $params->get('contentMenu1'));
        switch ($params->get('chooseWherePro1')) {
            case '2':
                $query->where('a.product_hot = 1');
                break;
            case '3':
                $query->where('a.product_hot = 1');
                break;
            default :
                $query->where('a.product_new = 1');
                break;
        }

        return $db->setQuery($query, 0, 8)->loadObjectList();
    }

    public static function getProductNew($limit) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                ->where('published = 1')
                ->where('delete_status = 0')
                ->order('a.id DESC');
        return $db->setQuery($query, 0, $limit)->loadObjectList();
    }

    public static function getProductSC($type, $limit) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);

        switch ($type) {
            case '1':
                $query->select('a.id,a.product_image,b.product_name,a.product_price, a.product_price_r,a.hits,a.buyer_virtual')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.product_price_r > 0')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1');
                break;
            case '2':
                $query->select('a.id,a.product_image,b.product_name,a.product_price, a.product_price_r,a.hits,a.buyer_virtual')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.product_hot > 0')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1');
                break;
            case '3':
                $query->select('a.id,a.product_image,b.product_name,a.product_price, a.product_price_r,a.hits,a.buyer_virtual')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1')
                        ->order('a.buyer_virtual DESC');
                break;
            case '4':
                $query->select('a.id,a.product_image,b.product_name,a.product_price, a.product_price_r,a.hits,a.buyer_virtual')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.product_featured = 1')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1')
                        ->order('rand()');
                break;
            default :
                $query->select('a.id,a.product_image,b.product_name,a.product_price, a.product_price_r,a.hits')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1');
                break;
        }
        return $db->setQuery($query, 0, $limit)->loadObjectList();
    }

    public static function getBannerImg($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('a.id,b.media_image')
                    ->from($db->quoteName('#__openshop_medias', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_mediadetails', 'b') . ' ON a.id = b.media_id')
                    ->where('a.published = 1')
                    ->where('a.id = ' . $id);
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }

}
