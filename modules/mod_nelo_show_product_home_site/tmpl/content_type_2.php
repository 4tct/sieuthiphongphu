<style>
    .showTabContentT2{
        padding-top: 30px;
    }
    .showTabContentT2 .tabCatT2{
        text-align: center;
    }
    .showTabContentT2 .tabCatT2 ul{
        position: relative;
        display: inline;
    }
    .showTabContentT2 .tabCatT2 ul li.active{
        font-weight: bold;
    }
    .showTabContentT2 .tabCatT2 ul:before, .showTabContentT2 .tabCatT2 ul:after{
        content: "";
        position: absolute;
        top: 50%;
        width: 25%;
        background: #cacaca;
        height: 1px;
    }
    .showTabContentT2 .tabCatT2 ul:before{
        right: 100%;
    }
    .showTabContentT2 .tabCatT2 ul:after{
        left: 100%;
    }
    .showTabContentT2 .tabCatT2 ul li{
        display: inline-block;
        padding: 0 20px;
        text-transform: uppercase;
        font-size: 20px;
        cursor: pointer;
    }
    .showTabContentT2 .tabCatT2 ul li:hover{
        text-shadow: #c3c3c3 1px 1px;
    }
    .showTabContentT2 .tabCatT2 ul .boder_right{
        border-right: 1px solid #7b7b7b;
    }
    .showTabContentT2 .content_show_product_tabT2{
        padding: 30px 0 0 0;
    }
    .showTabContentT2 .content_show_product_tabT2 .namePro{
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        padding: 10px 10px 0 10px;
    }
    .showTabContentT2 .content_show_product_tabT2 .namePro a{
        text-decoration: none;
        color: black;
    }
    .showTabContentT2 .content_show_product_tabT2 .codePro{
        padding: 5px 10px 0 10px;
    }
    .showTabContentT2 .content_show_product_tabT2 .pricePro{
        color: red;
        font-size: 18px;
        padding: 5px 10px 0px 10px;
        display: inline-block;
        transition: all 0.3s ease;
        position: absolute;
    }
    .showTabContentT2 .content_show_product_tabT2 .b_t_2{
        border: 1px solid #dedede;
        margin-left: -1px;
        padding: 15px;
    }
    .showTabContentT2 .content_show_product_tabT2 .b_t_t_2{
        margin-top: -1px;
    }
    .showTabContentT2 .content_show_product_tabT2 .infoImgT2{
        position: relative;
        cursor: pointer;
    }
    .showTabContentT2 .content_show_product_tabT2 .infoChildProT2{
        position: absolute;
        bottom: 5px;
        background: white;
        width: 90%;
        transform: translate(0px,10px);
        opacity: -1;
        margin-left: 5%;
        transition: all 0.4s ease;
    }
    .showTabContentT2 .content_show_product_tabT2 .infoChildProT2{
        padding: 10px;
    }
    .showTabContentT2 .content_show_product_tabT2 .b_t_2:hover .infoChildProT2{
        opacity: 1;
        transform: translate(0px, 0px);
        transition: all 0.4s ease;
    }
    .showTabContentT2 .content_show_product_tabT2 .b_t_2:hover .pricePro{
        opacity: -1;
        transform: translate(100px, 0px);
        transition: all 0.3s ease;
    }
    .showTabContentT2 .content_show_product_tabT2 .b_t_2:hover .btn-buy{
        opacity: 1;
        transform: translate(0px, 0px);
        transition: all 0.4s ease;
    }
    .showTabContentT2 .content_show_product_tabT2 .pricenotbuy{
        padding-left: 10px;
        color: #a0a0a0;
        text-decoration: line-through;
        font-size: 14px;
    }
    .showTabContentT2 .content_show_product_tabT2 .btn-buy{
        text-align: center;
        padding: 10px 0 0 0;
        transform: translate(-100px,0);
        opacity: 0;
        z-index: -1;
        transition: all 0.4s ease;
    }
    .showTabContentT2 .content_show_product_tabT2 .btn-buy a{
        padding: 10px;
        background: orange;
        color: white;
        text-decoration: none;
    }
    .showTabContentT2 .content_show_product_tabT2 .pricePro1{
        font-size: 18px;
        color: red;
        padding: 0 0 5px 0;
    }
    .ct2_not_pro{
        text-align: center;
        font-style: italic;
    }
</style>

<div class="showTabContentT2">
    <div class="tabCatT2">
        <ul>
            <?php
            $active = '0';
            if ($params->get('nameMenu1') != '') {
                $active = '1';
                $id = OpenShopHelper::convertBase64Encode($params->get('contentMenu1'));
                ?>
                <li class="boder_right tabClick1 active" onclick="getProductInHomeSiteT2<?php echo $params->get('classSpecial') ?>('<?php echo $id ?>','<?php echo $params->get('chooseWherePro1') ?>')">
                    <span>
                        <?php echo $params->get('nameMenu1') ?>
                    </span>
                </li>
                <?php
            }

            if ($params->get('nameMenu2') != '') {
                $id = OpenShopHelper::convertBase64Encode($params->get('contentMenu2'));
                if ($active == '0') {
                    echo '<li class="boder_right tabClick1 active" onclick="getProductInHomeSiteT2'. $params->get('classSpecial') .'(\'' . $id . '\', \''. $params->get('chooseWherePro2') .'\')">';
                    $active = '1';
                } else {
                    echo '<li class="boder_right tabClick1" onclick="getProductInHomeSiteT2'. $params->get('classSpecial') .'(\'' . $id . '\' , \''. $params->get('chooseWherePro2') .'\')">';
                }
                ?>
                <!--<li class="boder_right tabClick1">-->
                <span>
                    <?php echo $params->get('nameMenu2') ?>
                </span>
                </li>
                <?php
            }

            if ($params->get('nameMenu3') != '') {
                $id = OpenShopHelper::convertBase64Encode($params->get('contentMenu3'));
                if ($active == '0') {
                    echo '<li class="tabClick1 active" onclick="getProductInHomeSiteT2'. $params->get('classSpecial') .'(\'' . $id . '\', \''. $params->get('chooseWherePro3') .'\')">';
                    $active = '1';
                } else {
                    echo '<li class="tabClick1" onclick="getProductInHomeSiteT2'. $params->get('classSpecial') .'(\'' . $id . '\', \''. $params->get('chooseWherePro3') .'\')">';
                }
                ?>
                <!--<li class="">-->
                <span>
                    <?php echo $params->get('nameMenu3') ?>
                </span>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>

    <div class="content_show_product_tabT2 tab1_T2">
        <div class="col-md-12" id="contentLoadT2_<?php echo $params->get('classSpecial') ?>">
            <?php
            if (count($contentT2)) {
                foreach ($contentT2 as $ct2) {
                    $link = JRoute::_(OpenShopRoute::getProductRoute($ct2->id, OpenShopHelper::getProductCategory($ct2->id)));
                    ?>
                    <div class="col-sm-4 col-md-3 b_t_2 b_t_t_2">
                        <div class="infoImgT2" onclick="location.href = '<?php echo $link ?>'">
                            <img data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ct2->product_image ?>" class="img-responsive lazy" title="<?php echo $ct2->product_name ?>" alt="<?php echo $ct2->product_name ?>"/>
                            <div class="infoChildProT2">
                                <div class="pricePro1">
                                    <?php
                                    echo number_format($ct2->product_price, 0, ',', '.');
                                    echo '<sup>đ</sup>';
                                    if ($ct2->product_price_r != 0) {
                                        echo '<span class="pricenotbuy">';
                                        echo number_format($ct2->product_price_r, 0, ',', '.');
                                        echo '<sup>đ</sup>';
                                        echo '</span>';
                                    }
                                    ?>
                                </div>
                                <div>
                                    Kích thước: 
                                    <?php
                                    $sizes = OpenShopHelper::getProductOptionValueDetails($ct2->id, 'size');
                                    foreach ($sizes as $k => $s) {
                                        echo $s->value;
                                        if ($k != count($sizes) - 1) {
                                            echo ' - ';
                                        }
                                    }
                                    ?>
                                </div>
                                <!--<div>Màu sắc: Xanh</div>-->
                            </div>
                        </div>

                        <div class="namePro">
                            <a href="<?php echo $link ?>">
                                <?php echo $ct2->product_name ?>
                            </a>
                        </div> 
                        <div class="codePro">
                            Mã: <strong><?php echo $ct2->product_sku ?></strong>
                        </div>
                        <div class="pricePro">
                            <?php
                            echo number_format($ct2->product_price, 0, ',', '.');
                            echo '<sup>đ</sup>';
                            if ($ct2->product_price_r != 0) {
                                echo '<span class="pricenotbuy">';
                                echo number_format($ct2->product_price_r, 0, ',', '.');
                                echo '<sup>đ</sup>';
                                echo '</span>';
                            }
                            ?>
                        </div>
                        <div class="btn-buy">
                            <a href="<?php echo $link ?>">
                                Mua ngay
                            </a>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="ct2_not_pro">
                    Hiện chưa có sản phẩm
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<script>
    jQuery(function () {
        jQuery('.tabClick1').on('click', function () {
            //bỏ class active
            jQuery('.showTabContentT2 .tabCatT2 ul li').removeClass('active');
            //them class active
            jQuery(this).addClass('active');
        });
    });

    /*
     * i => id
     * t => type
     */
    function getProductInHomeSiteT2<?php echo $params->get('classSpecial') ?>(i,t) {
        toastr['info']('Đang tải...');
        $.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=product.getProductInHomeSiteT2',
            method: 'post',
            data: {
                i: i,
                t: t
            },
            success: function (dt) {
                var d = $.parseJSON(dt);
                $('#contentLoadT2_<?php echo $params->get('classSpecial') ?>').html(d.html);
            }
        });
    }
</script>

<div class="clr"></div>
