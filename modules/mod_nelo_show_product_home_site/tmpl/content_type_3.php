<style>
    .show-pro-home-t3{
        margin-top: 20px;
        border: 3px solid <?php echo $params->get('colorWap3') ?>;
        border-radius: 5px;
    }
    .show-pro-home-t3 .title_show_home_t3{
        background: <?php echo $params->get('colorWap3') ?>;
    }
    .show-pro-home-t3 .title_show_home_t3 i{
        padding: 15px;
        color: white;
    }
    .show-pro-home-t3 .title_show_home_t3 span{
        color: white;
        text-transform: uppercase;
        font-weight: bold;
        padding-left: 10px;
    }
    .show-pro-home-t3 .content_show_product_home_t3{
        background: white;
        padding: 5px;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .wap-pro_t3{
        padding: 0 10px;
        margin-top: 15px;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .wap-pro_t3 .wap-border-pro-t3{
        border: 1px solid #b3b3b3;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .wap-pro_t3 .info_pro_t3{
        background: #f3f3f3;
        height: 107px;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .name_pro_t3{
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        padding: 10px 10px 0 10px;
        font-size: 15px;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .price_pro_t3{
        padding: 10px;
        font-size: 16px;
        color: #fd0000;
        font-weight: bold;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 ul{
        padding: 8px 0;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 ul li{
        display: inline;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 .cart_t3{
        padding: 8px 0;
        background: #57b4c5;
        color: white;
        cursor: pointer;
        text-align: center;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 .cart_t3:hover{
        background: #0a68b3;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 .view_t3{
        padding: 3px 8% 4px 8%;
        background: #e27b4c;
        color: white;
        cursor: pointer;
        border-left: 1px solid white;
        font-size: 12px;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 .view_t3:hover{
        background:#f90000;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 .buyer_t3{
        padding: 3px 0 4px 0;
        background: #e27b4c;
        color: white;
        cursor: pointer;
        text-align: center;
        border-left: 1px solid white;
        font-size: 12px;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .act_pro_t3 .buyer_t3:hover{
        background: #ffbd03;
    }
    .show-pro-home-t3 .content_show_product_home_t3 .name_pro_t3 a{
        color: #383838;
        text-decoration: none;
    }


    /*--------------*/
    .show-pro-home-t3 .wap_pro_t3{
        display: inline-block;
        background: red;
        width: 216px;
    }

    .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        display: -webkit-block;
        display: -ms-block;
        display: -webkit-block;
        display: block;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }

</style>

<?php
jimport('joomla.application.module.helper');
$modules = JModuleHelper::getModules('nelo-show-product-home-site');
?>

<div class="show-pro-home-t3 wow fadeIn">
    <div class="title_show_home_t3">
        <i class="fa fa-tags" aria-hidden="true"></i>
        <span><?php echo $module->title ?></span>
    </div>
    <?php
    for ($i = 0; $i < $params->get('numRow3'); $i++) {
        $start = $i * (int) $params->get('numPro3');
        ?>
        <div class="content_show_product_home_t3">
            <div class="swiper-container slider-product-new">
                <div class="swiper-wrapper">
                    <?php
                    for ($p = $start; $p < count($product_new); $p++) {
                        $pm = $product_new[$p];
                        $link = JRoute::_(OpenShopRoute::getProductRoute($pm->id, OpenShopHelper::getProductCategory($pm->id)));
                        ?>
                        <div class="swiper-slide paddingImg">
                            <div class="imgProShow3">
                                <!--<a href="<?php echo $link ?>">-->
                                <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pm->id) ?>', '<?php echo $link ?>')">
                                    <img data-src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pm->product_image ?>" class="img-responsive swiper-lazy" alt="<?php echo $pm->product_name; ?>" title="<?php echo $pm->product_name; ?>"/>
                                    <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>
                                </a>
                                
                                <div class="ProSKU">
                                    <?php
                                    echo $pm->product_sku;
                                    ?>
                                </div>
                            </div>
                            <div class="info_pro_t3">
                                <div class="name_pro_t3">
                                    <!--<a href="<?php echo $link ?>">-->
                                    <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pm->id) ?>', '<?php echo $link ?>')" >
                                        <h5 title="<?php echo $pm->product_name; ?>">
                                            <?php
                                            echo $pm->product_name;
                                            ?>
                                        </h5>
                                    </a>
                                </div>
                                <div class="price_pro_t3">
                                    <?php
                                    if ($pm->product_price_r > 0) {
                                        echo '<div class="buyPrice">';
                                    }
                                    else{
                                        echo '<div>';
                                    }
                                    ?>
                                        <?php
                                        echo number_format((int) $pm->product_price, 0, ',', '.');
                                        ?>
                                        <sup>đ</sup>
                                        <sub><?php echo OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) ?></sub>
                                    </div>
                                    <?php
                                    if ($pm->product_price_r > 0) {
                                        ?>
                                        <div class="notbuyPrice">
                                            <?php
                                            echo number_format((int) $pm->product_price, 0, ',', '.');
                                            ?>
                                            <sup>đ</sup>
                                            <sub><?php echo OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) ?></sub>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="act_pro_t3">
                                    <div class="col-md-12" style="padding:0">
                                        <!--<div class="col-md-6 cart_t3" onclick="location.href = '<?php echo $link ?>'">-->
                                        <div class="col-xs-12 col-sm-12 col-md-6 cart_t3" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pm->id) ?>', '<?php echo $link ?>')">    
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                            <span style="font-size: 14px;font-weight: bold;">Đặt mua</span>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-3 buyer_t3 borderL0" data-toggle="tooltip" title="Lượt mua">
                                            <div>
                                                <?php echo $pm->buyer_virtual; ?>
                                            </div>
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-3 view_t3" data-toggle="tooltip" title="Lượt xem">
                                            <div>
                                                <?php echo $pm->hits; ?>
                                            </div>
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php
                            //show phần trăm giảm giá
                            if ($pm->product_price_r > 0) {
                                $percent = 100 - (int) (((int) $pm->product_price * 100) / $pm->product_price_r)
                                ?>
                                <div class="percentSale rightPercent"> 
                                    <?php echo $percent ?>%
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                        <?php
                        if ($p == ((int) $params->get('numPro3') - 1)) {
                            break;
                        }
                    }
                    ?>
                </div>

                <!-- Navigation -->
                <div class="jd-nav-button-next">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
                <div class="jd-nav-button-prev">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<script>
    var swiper = new Swiper('.slider-product-new', {
        slidesPerView: <?php echo $params->get('numCol3') ?>,
        spaceBetween: 15,
        height: 3000,
        preloadImages: false,
        lazyLoading: true,
        nextButton: '.jd-nav-button-next',
        prevButton: '.jd-nav-button-prev',
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    jQuery('.slider-product-new').mouseover(function () {
        $('.jd-nav-button-next').css('opacity', '1');
        $('.jd-nav-button-prev').css('opacity', '1');
    });
    jQuery('.slider-product-new').mouseout(function () {
        $('.jd-nav-button-next').css('opacity', '0');
        $('.jd-nav-button-prev').css('opacity', '0');
    });
</script>

