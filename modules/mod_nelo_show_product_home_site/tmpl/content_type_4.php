<style>
    .show-pro-home-t4{
        margin-top: 20px;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .wap-pro_t3{
        padding: 0 10px;
        margin-top: 15px;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .wap-border-pro-t4{
        border: 1px solid #b3b3b3;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .info_pro_t4{
        background: #f3f3f3;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .name_pro_3_t4{
        /*        overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;*/
        padding: 6px 0 6px 6px;
        font-size: 15px;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .price_pro_t4{
        padding: 10px;
        font-size: 16px;
        color: #fd0000;
        font-weight: bold;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .name_pro_3_t4 a{
        color: #383838;
        text-decoration: none;
        font-size: 15px;
    }
    .show-pro-home-t4 .title_show_t4{
        color: white;
        text-transform: uppercase;
    }
    .show-pro-home-t4 .title_show_t4 i{
        padding: 15px;
        color: white;
        text-transform: uppercase;
        font-weight: bold;
    }
    .show-pro-home-t4 .title_show_t4 span{
        color: white;
        text-transform: uppercase;
        font-weight: bold;
        padding-left: 10px;
    }
    .show-pro-home-t4 .img_pro_3_t4{
        float: left;
    }
    .show-pro-home-t4 .name_pro_3_t4 a{
        font-size: 16px;
        color: #5f5f5f;
        text-decoration: none;
    }
    .show-pro-home-t4 .price_pro_3_t4 .price_pro_sale_3_t4{
        font-size: 14px;
        text-decoration: line-through;
        color: #a5a5a5;
    }
    .show-pro-home-t4 .price_pro_3_t4 .price_pro_3_t4{
        font-size: 18px;
        color: red;
        font-weight: bold;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 ul{
        padding: 8px 0;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 ul li{
        display: inline;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 .cart_t4{
        padding: 8px 0;
        background: #57b4c5;
        color: white;
        cursor: pointer;
        text-align: center;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 .cart_t4:hover{
        background: #0404ff;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 .love_t4{
        padding: 8px 8%;
        background: #e27b4c;
        color: white;
        cursor: pointer;
        border-left: 1px solid white;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 .love_t4:hover{
        background:#f90000;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 .detail_t4{
        padding: 8px 0;
        background: #e27b4c;
        color: white;
        cursor: pointer;
        text-align: center;
        border-left: 1px solid white;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .act_pro_t4 .detail_t4:hover{
        background: #ffbd03;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .name_pro_t4 {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        padding: 10px 10px 0 10px;
        font-size: 15px;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .name_pro_t4 a{
        color: #383838;
        text-decoration: none;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .price_pro_t4 {
        padding: 10px;
        font-size: 16px;
        color: #fd0000;
        font-weight: bold;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .slider_3_t4_j{
        padding: 5px;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .hit_pro_3_t4{
        padding-top: 10px;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .hit_pro_3_t4 .vb{
        display: inline-block;
        font-size: 14px;
        padding: 0 10px;
        color: #2f5271;
    }


    /*--------------*/
    .show-pro-home-t4 .wap_pro_t3{
        display: inline-block;
        background: red;
    }

    .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: block;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .title_pro_main_t4 ul div{
        padding: 10px 0 10px 0;
        overflow: hidden;
    }
    .show-pro-home-t4 div.title_cat_<?php echo $params->get('header_class') ?>, .show-pro-home-t4 .title_cat_<?php echo $params->get('header_class') ?> li{
        background: <?php echo $params->get('colorWap4') ?>;
    }
    .title_pro_main_t4 ul li{
        padding: 12px 0;
        display: inline;
        cursor: pointer;
        position: relative;
    }
    .title_pro_main_t4 ul li a{
        padding: 0px 12px;
    }
    .title_pro_main_t4 ul li a.border_right{
        border-right: 1px solid white;
    }
    .title_pro_main_t4 ul li.last_active_title_t4{
        border-radius: 0 0 10px 0;
    }
    .title_pro_main_t4 ul li.active_title_t4{
        padding: 12px;
        background: white;
        border-radius: 10px 10px 0 0;
        padding-left: 20px;
        padding-right: 20px;
        margin-left: -1px;
    }
    .show-pro-home-t4 .content_show_product_home_t4 .title_pro_main_t4 ul li a{
        text-decoration: none;
        color: white;
    }
    .show-pro-home-t4 .title_pro_main_t4 ul li a span{
        position: relative;
        z-index: 20;
    }
    .title_pro_main_t4 ul li.active_title_t4 a span{
        color: #2f5271;
    }
    .title_pro_main_t4 .viewdetailcat li.all_right_active_title_t4 {

    }
    .btn_datmua_subcat{
        margin-top: 10px;
    }
    .btn_datmua_subcat a{
        padding: 10px 15px;
        background: #57b4c5;
        font-size: 14px;
        color: white;
        cursor: pointer;
        text-decoration: none;
    }
    .btn_datmua_subcat a:hover{
        background: #0404ff;
    }
    .show-pro-home-t4 .moccuoi{
        border-radius: 5px 0 0 0;
        background: white;    
        padding-left: 10px;
    }
    .show-pro-home-t4 .mocdau{
        border-radius: 0px 5px 0 0;
        background: white;    
        padding-left: 10px;
    }
    .show-pro-home-t4 .showProT4{
        padding: 9px;
    }
    .show-pro-home-t4 .viewdetailcat{
        position: absolute;
        top: 0;
        right: 0;
    }
    .show-pro-home-t4 .loadingProCat{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: #868686;
        opacity: 0.5;
        z-index: 1000;
    }
    .show-pro-home-t4 .loadingProCat i{
        font-size: 140px;
        color: white;
        position: absolute;
        top: 35%;
        left: 40%;
    }
    .show-pro-home-t4 .active_title_t4:before{
        content: '';
        width: 20px;
        height: 20px;
        background: white;
        position: absolute;
        left: -20px;
        bottom: 0;
        z-index: 4;
    }
    .show-pro-home-t4 .active_title_t4:after{
        content: '';
        width: 20px;
        height: 20px;
        background: white;
        position: absolute;
        right: -20px;
        bottom: 0;
        z-index: 4;
    }
    .show-pro-home-t4 .wapContetPro_<?php echo $params->get('header_class') ?>{
        position: relative;
        border: 3px solid <?php echo $params->get('colorWap4') ?>;
        border-radius: 5px;
        background: white;
    }
    .show-pro-home-t4 .title_cat_<?php echo $params->get('header_class') ?> .active_title_t4 a::after{
        content: '';
        width: 33px;
        height: 33px;
        background: <?php echo $params->get('colorWap4') ?>;
        position: absolute;
        bottom: 0px;
        right: -33px;
        z-index: 10;
        border-radius: 20px;
    }
    .show-pro-home-t4 .title_cat_<?php echo $params->get('header_class') ?> .active_title_t4 a::before{
        content: '';
        width: 33px;
        height: 33px;
        background: <?php echo $params->get('colorWap4') ?>;
        position: absolute;
        bottom: 0px;
        left: -33px;
        z-index: 10;
        border-radius: 20px;
    }
    .show-pro-home-t4 .slideLR{
        display: inline;
        float: right;
    }
    .show-pro-home-t4 .slideLR div{
        display: inline-block;
    }
    .show-pro-home-t4 .slideLR div i{
        padding: 15px 10px;
        cursor: pointer;
    }
    .show-pro-home-t4 .wapsub_<?php echo $params->get('header_class') ?>{
        padding-left: 0;
        padding-right: 0px;
        border: 3px solid <?php echo $params->get('colorWapSub4') ?>;
        border-radius: 5px;
    }
    .show-pro-home-t4 .title_show_t4_<?php echo $params->get('header_class') ?> {
        background: <?php echo $params->get('colorWapSub4') ?>;
    }
</style>

<div class="show-pro-home-t4">
    <div class="content_show_product_home_t4">
        <?php
        $product_child = Modnelo_show_product_home_siteHelper::getProductSC($params->get('chooseTypeInProSub'), '12');
        if ($params->get('choosePos4') == 'r') {            //sản phẩm hiển thị bên phải - sản phẩm phụ hiển thị bên trái
            ?>
            <div class="col-md-3 wow fadeInLeft wapsub_<?php echo $params->get('header_class') ?>">
                <div class="title_show_t4 title_show_t4_<?php echo $params->get('header_class') ?>">
                    <i class="fa fa-flag" aria-hidden="true"></i>
                    <span><?php echo $params->get('nameProSub') ?></span>

                    <div class="slideLR">
                        <div class="slideL">
                            <i class="fa fa-caret-left" aria-hidden="true"></i>
                        </div>
                        <div class="slideL">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="content_show_t4">
                    <div class="swiper-container top_t4">
                        <div class="swiper-wrapper">
                            <?php
                            foreach ($product_child as $ps) {
                                $link = JRoute::_(OpenShopRoute::getProductRoute($ps->id, OpenShopHelper::getProductCategory($ps->id)))
                                ?>
                                <div class="swiper-slide slider_3_t4_j">
                                    <div class="wap_pro_3_t4">
                                        <div class="img_pro_3_t4">
                                            <a href="<?php echo $link ?>">
                                                <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ps->product_image ?>" alt="<?php echo $ps->product_name ?>" title="<?php echo $ps->product_name ?>" width="148"/>
                                            </a>
                                        </div>
                                        <div class="info_pro_3_t4">
                                            <div class="name_pro_3_t4">
                                                <a href="<?php echo $link ?>">
                                                    <h5 title="<?php echo $ps->product_name; ?>">
                                                        <?php
                                                        echo $ps->product_name;
                                                        ?>
                                                    </h5>
                                                </a>
                                            </div>
                                            <div class="price_pro_3_t4">
                                                <?php
                                                if ($ps->product_price_r > 0) {
                                                    $clsP = 'style="padding-top:10px;"';
                                                    ?>
                                                    <div class="price_pro_sale_3_t4">
                                                        <?php echo number_format((int) $ps->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                    </div>
                                                    <?php
                                                    $clsP = '';
                                                }
                                                ?>
                                                <div class="price_pro_3_t4" <?php echo $clsP ?>>
                                                    <?php echo number_format((int) $ps->product_price, 0, ',', '.') ?><sup>đ</sup>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="btn_datmua_subcat">
                                            <a href="<?php echo $link ?>">
                                                Đặt mua
                                            </a>
                                        </div>

                                        <div class="hit_pro_3_t4">
                                            <div class="vb" data-toggle="tooltip" title="Lượt xem">
                                                <?php echo $ps->hits; ?>
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </div>
                                            <div data-toggle="tooltip" title="Lượt mua" class="vb">
                                                <?php echo $ps->buyer_virtual; ?>
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 wow fadeInRight" style="padding-right: 0;">
                <div class="wapContetPro_<?php echo $params->get('header_class') ?>">
                    <div class="title_pro_main_t4 title_pro_main_t4_<?php echo $params->get('header_class') ?>">
                        <ul>
                            <div class="title_cat_<?php echo $params->get('header_class') ?>">
                                <?php
                                $cls_cat = $params->get('header_class') . '_100';
                                ?>
                                <li class="active_title_t4 addLoadingFa<?php echo $cls_cat ?> title_t4_<?php echo $params->get('header_class') ?>" onclick="loadProductShowHomeSite('<?php echo $cls_cat ?>', '<?php echo OpenShopHelper::convertBase64Encode($category['id']) ?>')">
                                    <!--<i class="<?php echo $params->get('iconTitle') ?>" aria-hidden="true"></i>-->
                                    <a data-href="<?php echo $category['link_cat'] ?>" class="border_title_t4 border_right">
                                        <span class="catnameactive"><?php echo $category['name'] ?></span>
                                    </a>
                                </li>
                                <?php
                                if (!empty($category['cat_sub'])) {
                                    foreach ($category['cat_sub'] as $k => $cs) {
                                        $next_act = '';
                                        $last_act = '';
                                        $cls_border = 'border_right';

                                        if ($k == (count($category['cat_sub']) - 1)) {
                                            $last_act = 'last_active_title_t4';
                                            $cls_border = '';
                                        }
                                        $link_cat = JRoute::_(OpenShopRoute::getCategoryRoute($cs->id));
                                        $cls_cat = $params->get('header_class') . '_' . $k;
                                        ?>
                                        <li class="default_title_t4 addLoadingFa<?php echo $cls_cat ?> title_t4_<?php echo $params->get('header_class') ?> <?php echo $last_act ?>" onclick="loadProductShowHomeSite('<?php echo $cls_cat ?>', '<?php echo OpenShopHelper::convertBase64Encode($cs->id) ?>')">
                                            <a data-href="<?php echo $link_cat ?>" class="border_title_t4 <?php echo $cls_border ?>">
                                                <span><?php echo $cs->category_name ?></span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="viewdetailcat">
                                    <li class="all_right_active_title_t4 load_all_right_active_title_t4_<?php echo $params->get('header_class') ?>">
                                        <a href="<?php echo $category['link_cat'] ?>">
                                            Xem tất cả
                                        </a>
                                    </li>
                                </div>
                            </div>
                        </ul>
                    </div>
                    <div class="showProT4 appendLoading_<?php echo $params->get('header_class') ?>">
                        <div class="swiper-container slider-product-show-next-prev">
                            <div class="swiper-wrapper load_addLoadingFa<?php echo $params->get('header_class') ?>">
                                <?php
                                foreach ($category['products'] as $pm) {
                                    $link = JRoute::_(OpenShopRoute::getProductRoute($pm->id, OpenShopHelper::getProductCategory($pm->id)));
                                    ?>
                                    <div class="swiper-slide">
                                        <div style="min-height:200px;" class="paddingImg">
                                            <a href="<?php echo $link ?>">
                                                <img data-src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pm->product_image ?>" class="img-responsive swiper-lazy" alt="<?php echo $pm->product_name; ?>" title="<?php echo $pm->product_name; ?>"/>
                                                <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>
                                            </a>
                                        </div>
                                        <div class="info_pro_t4">
                                            <div class="name_pro_t4">
                                                <a href="<?php echo $link ?>">
                                                    <h5 title="<?php echo $pm->product_name; ?>">
                                                        <?php
                                                        echo $ps->product_name;
                                                        ?>
                                                    </h5>
                                                </a>
                                            </div>
                                            <div class="info_price_pro_t4">
                                                <?php
                                                if ($pm->product_price_r > 0) {
                                                    ?>
                                                    <div class="not_price_pro_t4">
                                                        <?php
                                                        echo number_format((int) $pm->product_price_r, 0, ',', '.');
                                                        ?>
                                                        <sup>đ</sup>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="price_pro_t4">
                                                    <?php
                                                    echo number_format((int) $pm->product_price, 0, ',', '.');
                                                    ?>
                                                    <sup>đ</sup>
                                                </div>
                                            </div>
                                            <div class="act_pro_t4">
                                                <div class="col-md-12" style="padding:0">
                                                    <div class="col-md-6 cart_t4" onclick="location.href = '<?php echo $link ?>'">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        <span style="font-size: 14px;font-weight: bold;">Đặt mua</span>
                                                    </div>
                                                    <div class="col-md-3 love_t4">
                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-3 detail_t4">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <!-- Navigation -->
                            <div class="jd-nav-button-next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div class="jd-nav-button-prev">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            //
        } else {//sản phẩm hiển thị bên trái - sản phẩm phụ hiển thị bên phải
            //
            ?>
            <div class="col-md-9 wow fadeInRight" style="padding-left: 0;">
                <div class="wapContetPro_<?php echo $params->get('header_class') ?>">
                    <div class="title_pro_main_t4 title_pro_main_t4_<?php echo $params->get('header_class') ?>">
                        <ul>
                            <div class="title_cat_<?php echo $params->get('header_class') ?>">
                                <?php
                                $cls_cat = $params->get('header_class') . '_100';
                                ?>
                                <li class="active_title_t4 addLoadingFa<?php echo $cls_cat ?> title_t4_<?php echo $params->get('header_class') ?>" onclick="loadProductShowHomeSite('<?php echo $cls_cat ?>', '<?php echo OpenShopHelper::convertBase64Encode($category['id']) ?>')">
                                    <!--<i class="<?php echo $params->get('iconTitle') ?>" aria-hidden="true"></i>-->
                                    <a data-href="<?php echo $category['link_cat'] ?>" class="border_title_t4 border_right">
                                        <span class="catnameactive"><?php echo $category['name'] ?></span>
                                    </a>
                                </li>
                                <?php
                                if (!empty($category['cat_sub'])) {
                                    foreach ($category['cat_sub'] as $k => $cs) {
                                        $next_act = '';
                                        $last_act = '';
                                        $cls_border = 'border_right';

                                        if ($k == (count($category['cat_sub']) - 1)) {
                                            $last_act = 'last_active_title_t4';
                                            $cls_border = '';
                                        }
                                        $link_cat = JRoute::_(OpenShopRoute::getCategoryRoute($cs->id));
                                        $cls_cat = $params->get('header_class') . '_' . $k;
                                        ?>
                                        <li class="default_title_t4 addLoadingFa<?php echo $cls_cat ?> title_t4_<?php echo $params->get('header_class') ?> <?php echo $last_act ?>" onclick="loadProductShowHomeSite('<?php echo $cls_cat ?>', '<?php echo OpenShopHelper::convertBase64Encode($cs->id) ?>')">
                                            <a data-href="<?php echo $link_cat ?>" class="border_title_t4 <?php echo $cls_border ?>">
                                                <span><?php echo $cs->category_name ?></span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="viewdetailcat">
                                    <li class="all_right_active_title_t4 load_all_right_active_title_t4_<?php echo $params->get('header_class') ?>">
                                        <a href="<?php echo $category['link_cat'] ?>">
                                            Xem tất cả
                                        </a>
                                    </li>
                                </div>
                            </div>
                        </ul>
                    </div>
                    <div class="showProT4 appendLoading_<?php echo $params->get('header_class') ?>">
                        <div class="swiper-container slider-product-show-next-prev">
                            <div class="swiper-wrapper load_addLoadingFa<?php echo $params->get('header_class') ?>">
                                <?php
                                foreach ($category['products'] as $pm) {
                                    $link = JRoute::_(OpenShopRoute::getProductRoute($pm->id, OpenShopHelper::getProductCategory($pm->id)));
                                    ?>
                                    <div class="swiper-slide">
                                        <div style="min-height:200px;" class="paddingImg">
                                            <a href="<?php echo $link ?>">
                                                <img data-src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pm->product_image ?>" class="img-responsive swiper-lazy" alt="<?php echo $pm->product_name; ?>" title="<?php echo $pm->product_name; ?>"/>
                                                <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>
                                            </a>
                                        </div>
                                        <div class="info_pro_t4">
                                            <div class="name_pro_t4">
                                                <a href="<?php echo $link ?>">
                                                    <h5 title="<?php echo $pm->product_name; ?>">
                                                        <?php
                                                        echo $pm->product_name;
                                                        ?>
                                                    </h5>
                                                </a>
                                            </div>
                                            <div class="info_price_pro_t4">
                                                <?php
                                                if ($pm->product_price_r > 0) {
                                                    ?>
                                                    <div class="not_price_pro_t4">
                                                        <?php
                                                        echo number_format((int) $pm->product_price_r, 0, ',', '.');
                                                        ?>
                                                        <sup>đ</sup>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="price_pro_t4">
                                                    <?php
                                                    echo number_format((int) $pm->product_price, 0, ',', '.');
                                                    ?>
                                                    <sup>đ</sup>
                                                </div>
                                            </div>
                                            <div class="act_pro_t4">
                                                <div class="col-md-12" style="padding:0">
                                                    <div class="col-md-6 cart_t4" onclick="location.href = '<?php echo $link ?>'">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        <span style="font-size: 14px;font-weight: bold;">Đặt mua</span>
                                                    </div>
                                                    <div class="col-md-3 love_t4">
                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-3 detail_t4">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <!-- Navigation -->
                            <div class="jd-nav-button-next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div class="jd-nav-button-prev">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 wow fadeInLeft wapsub_<?php echo $params->get('header_class') ?>">
                <div class="title_show_t4 title_show_t4_<?php echo $params->get('header_class') ?>">
                    <i class="fa fa-flag" aria-hidden="true"></i>
                    <span><?php echo $params->get('nameProSub') ?></span>

                    <div class="slideLR">
                        <div class="slideL">
                            <i class="fa fa-caret-left" aria-hidden="true"></i>
                        </div>
                        <div class="slideL">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="content_show_t4">
                    <div class="swiper-container top_t4">
                        <div class="swiper-wrapper">
                            <?php
                            foreach ($product_child as $ps) {
                                $link = JRoute::_(OpenShopRoute::getProductRoute($ps->id, OpenShopHelper::getProductCategory($ps->id)))
                                ?>
                                <div class="swiper-slide slider_3_t4_j">
                                    <div class="wap_pro_3_t4">
                                        <div class="img_pro_3_t4">
                                            <a href="<?php echo $link ?>">
                                                <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ps->product_image ?>" alt="<?php echo $ps->product_name ?>" title="<?php echo $ps->product_name ?>" width="148"/>
                                            </a>
                                        </div>
                                        <div class="info_pro_3_t4">
                                            <div class="name_pro_3_t4">
                                                <a href="<?php echo $link ?>">
                                                    <h5 title="<?php echo $ps->product_name; ?>">
                                                        <?php
                                                        echo $ps->product_name;
                                                        ?>
                                                    </h5>
                                                </a>
                                            </div>
                                            <div class="price_pro_3_t4">
                                                <?php
                                                if ($ps->product_price_r > 0) {
                                                    $clsP = 'style="padding-top:10px;"';
                                                    ?>
                                                    <div class="price_pro_sale_3_t4">
                                                        <?php echo number_format((int) $ps->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                    </div>
                                                    <?php
                                                    $clsP = '';
                                                }
                                                ?>
                                                <div class="price_pro_3_t4" <?php echo $clsP ?>>
                                                    <?php echo number_format((int) $ps->product_price, 0, ',', '.') ?><sup>đ</sup>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="btn_datmua_subcat">
                                            <a href="<?php echo $link ?>">
                                                Đặt mua
                                            </a>
                                        </div>

                                        <div class="hit_pro_3_t4">
                                            <div class="vb" data-toggle="tooltip" title="Lượt xem">
                                                <?php echo $ps->hits; ?>
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </div>
                                            <div data-toggle="tooltip" title="Lượt mua" class="vb">
                                                <?php echo $ps->buyer_virtual; ?>
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<div class="clr"></div>

<script>
    var swiper = new Swiper('.slider-product-show-next-prev', {
        slidesPerView: 4,
        spaceBetween: 15,
        preloadImages: false,
        lazyLoading: true,
        nextButton: '.jd-nav-button-next',
        prevButton: '.jd-nav-button-prev',
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    jQuery('.slider-product-show-next-prev').mouseover(function () {
        $('.jd-nav-button-next').css('opacity', '1');
        $('.jd-nav-button-prev').css('opacity', '1');
    });
    jQuery('.slider-product-show-next-prev').mouseout(function () {
        $('.jd-nav-button-next').css('opacity', '0');
        $('.jd-nav-button-prev').css('opacity', '0');
    });

    var swiper = new Swiper('.top_t4', {
        pagination: '.swiper-pagination',
        slidesPerView: 1,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: -1,
        autoplay: 3000,
        effect: 'slide'
    });

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>