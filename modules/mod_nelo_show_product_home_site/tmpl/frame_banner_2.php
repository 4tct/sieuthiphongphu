<div class="banner_custom">
    <?php
    $bannerImg = Modnelo_show_product_home_siteHelper::getBannerImg($params->get('chooseBannerImg'));
    if (count($bannerImg)) {
        ?>
        <div class="paddingT20">
            <img class="img-responsive lazy" data-original="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $bannerImg[0]->media_image ?>" alt="Quảng cáo"  width="100%"/>
        </div>
        <?php
    }
    ?>
</div>