<style>
    .banner_home_site{
        padding: 30px 30px 0 30px;
    }
    .img-banner-h{
        max-height: 120px;
        width: 100%;
    }
    .show_product_home{
        background: #ffffff;
        height: 450px;
    }
    .m_home_site{
        margin-top: 30px;
    }
    .show_product_home .bg_top{
        width: 100%;
        background: #e40000;
        height: 3px;
    }
    .title_name_category .main_title_name_category{
        font-weight: bold;
        padding: 10px 0;
        background: #e40000;
        color: white;
        display: inline-block;
        width: 250px;
        text-align: center;
        text-transform: uppercase;
    }
    .title_name_category .child_title_name_category{
        float: right;
    }
    .title_name_category .child_title_name_category a{
        display: inline-block;
        padding: 10px;
        border-right: 2px solid red;
        color: black;
        text-decoration: none;
        text-transform: uppercase;
    }
    .content_show_product_home hr{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .content_show_product_home .price_and_btn{
        position: relative;
    }
    .content_show_product_home .price_and_btn .price_show, .content_show_product_home .price_and_btn .btn_buy{
        display: inline-block;
    }
    .content_show_product_home .btn_buy{
        position: absolute;
        padding: 10px;
        background: red;
        cursor: pointer;
        right: 0;
        text-transform: uppercase;
        font-weight: bold;
    }
    .content_show_product_home .btn_buy a{
        color: white;
        text-decoration: none;
    }
    .content_show_product_home .price_not_buy{
        text-decoration: line-through;
        color: #ababab;
    }
    .content_show_product_home .price_buy{
        font-size: 18px;
        font-weight: bold;
        color: red;
    }
    .content_show_product_home img{
        width: 100%;
        max-height: 260px;
        cursor: pointer;
    }
    .content_show_product_home .name_pro{
        padding: 10px 0 0 0;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        cursor: pointer;
    }
    .content_show_product_home .code_pro{
        text-align: center;
        padding: 0 0 10px 0;
    }
    .content_show_product_home .col-md-3{
        padding: 10px;
        border: 1px solid white;
    }
    .content_show_product_home .col-md-3:hover{
        border: 1px solid red;
    }
    .content_show_product_home .col-md-3:hover .name_pro{
        color: red;
    }
    .content_show_product_home .only_price{
        padding: 10px 0;
    }
    .content_show_product_home .linkP a.showlinkP{
        text-decoration: none;
        color: black;
    }
</style>

<div class="m_home_site">
    <div class="bg_top"></div>
    <div class="title_name_category">
        <div class="main_title_name_category"><?php echo $category['name'] ?></div>
        <div class="child_title_name_category">
            <?php
            if ($category['cat_sub'] != '') {
                foreach ($category['cat_sub'] as $k => $cat_sub) {
                    $link = JRoute::_(OpenShopRoute::getCategoryRoute($cat_sub->id));
                    $style = '';
                    if (count($category['cat_sub']) == ($k + 1)) {
                        $style = 'style="border-right:0;"';
                    }
                    ?>
                    <a href="<?php echo $link ?>" <?php echo $style ?>>
                        <?php echo $cat_sub->category_name ?>
                    </a>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="content_show_product_home">
        <?php
        foreach ($category['products'] as $product) {
            $link = JRoute::_(OpenShopRoute::getProductRoute($product->id, OpenShopHelper::getProductCategory($product->id)));
            ?>
            <div class="col-md-3 linkP">
                <a href="<?php echo $link ?>" class="showlinkP">
                    <img class="lazy" data-original="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $product->product_image ?>" class="img-responsive" title="<?php echo $product->product_name ?>" alt="<?php echo $product->product_name ?>"/>
                    <div class="name_pro" title="<?php echo $product->product_name ?>">
                        <?php echo $product->product_name ?>
                    </div>
                </a>
                <hr>
                <div class="code_pro">Mã SP: <strong><?php echo $product->product_sku ?></strong></div>
                <div class="price_and_btn">
                    <div class="price_show">
                        <?php
                        if ($product->product_price_r) {
                            ?>
                            <div class="price_not_buy">
                                <?php
                                echo number_format((int) $product->product_price_r, 0, ',', '.');
                                ?>
                                <sup>đ</sup>
                            </div>
                            <div class="price_buy">
                                <?php
                                echo number_format((int) $product->product_price, 0, ',', '.');
                                ?>
                                <sup>đ</sup>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="price_buy only_price">
                                <?php
                                echo number_format((int) $product->product_price, 0, ',', '.');
                                ?>
                                <sup>đ</sup>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="btn_buy">
                        <a href="<?php echo $link ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Mua ngay</a>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
