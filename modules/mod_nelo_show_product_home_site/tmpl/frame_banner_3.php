<div class="banner_custom">
    <div class="banner3">
        <?php
        $bannerImg = Modnelo_show_product_home_siteHelper::getBannerImg($params->get('chooseBannerImg'));
        if (count($bannerImg)) {
            ?>
            <div class="col-md-6 paddingL0 col_banner3">
                <img class="img-responsive lazy" data-original="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $bannerImg[0]->media_image ?>" alt="Quảng cáo" />
            </div>
            <div class="col-md-6 paddingR0 col_banner3">
                <img class="img-responsive lazy" data-original="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $bannerImg[1]->media_image ?>" alt="Quảng cáo" />
            </div>
            <?php
        }
        ?>
    </div>
</div>

<div class="clr"></div>
