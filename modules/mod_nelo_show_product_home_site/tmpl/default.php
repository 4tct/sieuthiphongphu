<?php
// No direct access
defined('_JEXEC') or die;

switch ($params->get('chooseTypeShow')){
    case 'l1':
        require 'content_type_1.php';
        break;
    case 'l2':
        require 'content_type_2.php';
        break;
    case 'l3':
        require 'content_type_3.php';
        break;
    case 'l4':
        require 'content_type_4.php';
        break;
    default :
        break;
}

//----------------------------------------------------
//------------------- SHOW BANNER --------------------
//----------------------------------------------------

if ($params->get('showbanner')) {
    switch ($params->get('frameBanner')) {
        case 'f1':
            require 'frame_banner_1.php';
            break;
        case 'f2':
            require 'frame_banner_2.php';
            break;
        case 'f3':
            require 'frame_banner_3.php';
            break;
        default :
            break;
    }
}
?>