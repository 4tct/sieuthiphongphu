<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$menusC = Modmenu_categoriesHelper::getmenu_categories($params);
require JModuleHelper::getLayoutPath('mod_menu_categories');