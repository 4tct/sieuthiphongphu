<?php

class Modmenu_categoriesHelper {

    public static function getmenu_categories($params) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__menu'))
                ->where('menutype = "' . $params->get('menu_id') . '"')
                ->where('published = 1')
                ->where('parent_id = 1')
                ->order('lft');
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getmenu_categories_child($menutype,$parent){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__menu'))
                ->where('menutype = "' . $menutype . '"')
                ->where('published = 1')
                ->where('parent_id = ' . $parent)
                ->order('lft');
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getBannerImageCategory($id_cat){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('banner_image')
                ->from($db->quoteName('#__openshop_categories'))
                ->where('id = "' . $id_cat . '"')
                ->where('published = 1');
        return $db->setQuery($query)->loadResult();
    }

}
