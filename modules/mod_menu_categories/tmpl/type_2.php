<?php
$view = $app->input->getCmd('view', '');

if (!OpenShopHelper::isMobile()) {
    ?>
    <div class="name_all_caterogy main_name_all_caterogy">
        <a>
            Tất cả danh mục
            <span class="float_right">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </a>
    </div>

    <?php
    $clsShow = 'hide_menu_cat';
    if ($view == 'frontpage') {
        $clsShow = 'show_menu_cat';
    }
    ?>
    <div class="h_s_m_c_t2">
        <div class="content-menu-caterogy <?php echo $clsShow ?>">
            <ul>
                <?php
                foreach ($menusC as $mc) {
                    $id = explode('&id=', $mc->link);
                    $banner_img = Modmenu_categoriesHelper::getBannerImageCategory($id[1]);
                    ?>
                    <li>
                        <a href="<?php echo JRoute::_($mc->link . '&Itemid=' . $mc->id) ?>">
                            <div class="textMenu">
                                <?php
                                $pms = json_decode($mc->params);
                                if ($pms->menu_image != '') {
                                    ?>
                                    <img src="<?php echo JUri::base() . $pms->menu_image ?>" class="img-responsive display_inline_block paddingR5" alt="<?php echo $mc->title ?>" width="32"/>
                                    <?php
                                }
                                ?>
                                <?php echo $mc->title ?>
                            </div>
                        </a>
                        <?php
                        $sub_menu = Modmenu_categoriesHelper::getmenu_categories_child($mc->menutype, $mc->id);
                        if (count($sub_menu)) {
                            echo '<i class="fa fa-angle-right paddingT7" aria-hidden="true"></i>';
                            if (isset($pms->show_menu_type) && $pms->show_menu_type == 1) {
                                ?>
                                <div class="sub_cat paddingL0">
                                    <div class="col-md-12 padding0">
                                        <div class="col-md-3 padding0">
                                            <div class="menu_sub_2_1">
                                                <ul>
                                                    <?php
                                                    foreach ($sub_menu as $sm) {
                                                        $id_sub = explode('&id=', $sm->link);
                                                        $banner_img_sub = Modmenu_categoriesHelper::getBannerImageCategory($id_sub[1]);
                                                        $sub_menu2 = Modmenu_categoriesHelper::getmenu_categories_child($sm->menutype, $sm->id);
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo JRoute::_($sm->link . '&Itemid=' . $sm->id) ?>">
                                                                <div class="textMenu">
                                                                    <?php echo $sm->title ?>
                                                                </div>
                                                            </a>

                                                            <?php
                                                            if (count($sub_menu2)) {
                                                                echo '<i class="fa fa-angle-right angle-right-sub-menu-2" aria-hidden="true"></i>';
                                                                echo '<div class="sub_menu_2_2">';
                                                                echo '<div class="col-md-4">';
                                                                foreach ($sub_menu2 as $sm2) {
                                                                    ?>
                                                                    <div class="col-md-12 paddingL0">
                                                                        <div class="titleSubMenu2">
                                                                            <a href="<?php echo JRoute::_($sm2->link . '&Itemid=' . $sm2->id); ?>">
                                                                                <i class="fa fa-angle-right display_none" aria-hidden="true"></i>
                                                                                <span><?php echo $sm2->title ?></span>
                                                                            </a>
                                                                        </div>

                                                                    </div>
                                                                    <?php
                                                                }
                                                                echo '</div>';
                                                                echo '<div class="col-md-8 heightImgBanner paddingR0">';
                                                                if (!empty($banner_img_sub)) {
                                                                    ?>
                                                                    <div class="imgMainBanner" style="background-image: url('<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $banner_img_sub ?>')"></div>
                                                                    <?php
                                                                }
                                                                echo '</div>';
                                                                echo '</div>';
                                                            }
                                                            ?>


                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-9 heightImgBanner paddingR0">
                                            <?php
                                            if (!empty($banner_img)) {
                                                ?>
                                                <div class="imgMainBanner" style="background-image: url('<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $banner_img ?>')"></div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="sub_cat">
                                    <div class="col-md-12 padding0">
                                        <div class="col-md-8">
                                            <?php
                                            foreach ($sub_menu as $sm) {
                                                $sub_menu_2 = Modmenu_categoriesHelper::getmenu_categories_child($sm->menutype, $sm->id);
                                                $cls = '';
                                                if (count($sub_menu_2)) {
                                                    $cls = 'border-see-allsubcat';
                                                }
                                                ?>
                                                <div class="col-md-4">
                                                    <div class="see-allsubcat <?php echo $cls ?>">
                                                        <a href="<?php echo JRoute::_($sm->link . '&Itemid=' . $sm->id) ?>">
                                                            <?php echo $sm->title; ?>
                                                        </a>
                                                    </div>

                                                    <?php
                                                    //Menu con
                                                    if (count($sub_menu_2)) {
                                                        echo '<div class="wap-see-subcat">';
                                                        foreach ($sub_menu_2 as $sm2) {
                                                            $sub_link2 = JRoute::_($sm2->link . '&Itemid=' . $sm2->id);
                                                            echo '<div class="see-subcat">';
                                                            echo '<i class="fa fa-angle-right icon_subcat display_none" aria-hidden="true"></i>';
                                                            echo '  <a href="' . $sub_link2 . '">';
                                                            echo $sm2->title;
                                                            echo '  </a>';
                                                            echo '</div>';
                                                        }
                                                        echo '</div>';
                                                    }
                                                    ?>

                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="col-md-4 show-banner-menu-cat">
                                            <?php
                                            if (!empty($banner_img)) {
                                                ?>
                                                <img src="<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $banner_img ?>" class="img-responsive" alt="Quảng cáo của <?php echo $mc->title ?>"/>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </li>   
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>

    <div class="menuScroll">
        <div class="width-body">
            <div class="col-md-3 padding0 scroll_name_all_caterogy">
                <div class="name_all_caterogy">
                    <a class="allCatScroll">
                        Tất cả danh mục
                        <span class="float_right">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </span>
                    </a>
                </div>
                <div class="h_s_m_c_t2">
                    <div class="content-scroll-menu-caterogy hide_menu_cat">
                        <ul>
                            <?php
                            foreach ($menusC as $mc) {
                                $id = explode('&id=', $mc->link);
                                $banner_img = Modmenu_categoriesHelper::getBannerImageCategory($id[1]);
                                ?>
                                <li>
                                    <a href="<?php echo JRoute::_($mc->link . '&Itemid=' . $mc->id) ?>">
                                        <div class="textMenu">
                                            <?php
                                            $pms = json_decode($mc->params);
                                            if ($pms->menu_image != '') {
                                                ?>
                                                <img src="<?php echo JUri::base() . $pms->menu_image ?>" class="img-responsive display_inline_block paddingR5" alt="<?php echo $mc->title ?>" width="32"/>
                                                <?php
                                            }
                                            ?>
                                            <?php echo $mc->title ?>
                                        </div>
                                    </a>
                                    <?php
                                    $sub_menu = Modmenu_categoriesHelper::getmenu_categories_child($mc->menutype, $mc->id);
                                    if (count($sub_menu)) {
                                        echo '<i class="fa fa-angle-right paddingT7" aria-hidden="true"></i>';
                                        if (isset($pms->show_menu_type) && $pms->show_menu_type == 1) {
                                            ?>
                                            <div class="sub_cat paddingL0">
                                                <div class="col-md-12 padding0">
                                                    <div class="col-md-3 padding0">
                                                        <div class="menu_sub_2_1">
                                                            <ul>
                                                                <?php
                                                                foreach ($sub_menu as $sm) {
                                                                    $id_sub = explode('&id=', $sm->link);
                                                                    $banner_img_sub = Modmenu_categoriesHelper::getBannerImageCategory($id_sub[1]);
                                                                    $sub_menu2 = Modmenu_categoriesHelper::getmenu_categories_child($sm->menutype, $sm->id);
                                                                    ?>
                                                                    <li>
                                                                        <a href="<?php echo JRoute::_($sm->link . '&Itemid=' . $sm->id) ?>">
                                                                            <div class="textMenu">
                                                                                <?php echo $sm->title ?>
                                                                            </div>
                                                                        </a>

                                                                        <?php
                                                                        if (count($sub_menu2)) {
                                                                            echo '<i class="fa fa-angle-right angle-right-sub-menu-2" aria-hidden="true"></i>';
                                                                            echo '<div class="sub_menu_2_2">';
                                                                            echo '<div class="col-md-4">';
                                                                            foreach ($sub_menu2 as $sm2) {
                                                                                ?>
                                                                                <div class="col-md-12 paddingL0">
                                                                                    <div class="titleSubMenu2">
                                                                                        <a href="<?php echo JRoute::_($sm2->link . '&Itemid=' . $sm2->id); ?>">
                                                                                            <i class="fa fa-angle-right display_none" aria-hidden="true"></i>
                                                                                            <span><?php echo $sm2->title ?></span>
                                                                                        </a>
                                                                                    </div>

                                                                                </div>
                                                                                <?php
                                                                            }
                                                                            echo '</div>';
                                                                            echo '<div class="col-md-8 heightImgBanner paddingR0">';
                                                                            if (!empty($banner_img_sub)) {
                                                                                ?>
                                                                                <div class="imgMainBanner" style="background-image: url('<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $banner_img_sub ?>')"></div>
                                                                                <?php
                                                                            }
                                                                            echo '</div>';
                                                                            echo '</div>';
                                                                        }
                                                                        ?>


                                                                    </li>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 heightImgBanner paddingR0">
                                                        <?php
                                                        if (!empty($banner_img)) {
                                                            ?>
                                                            <div class="imgMainBanner" style="background-image: url('<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $banner_img ?>')"></div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="sub_cat">
                                                <div class="col-md-12 padding0">
                                                    <div class="col-md-8">
                                                        <?php
                                                        foreach ($sub_menu as $sm) {
                                                            $sub_menu_2 = Modmenu_categoriesHelper::getmenu_categories_child($sm->menutype, $sm->id);
                                                            $cls = '';
                                                            if (count($sub_menu_2)) {
                                                                $cls = 'border-see-allsubcat';
                                                            }
                                                            ?>
                                                            <div class="col-md-4">
                                                                <div class="see-allsubcat <?php echo $cls ?>">
                                                                    <a href="<?php echo JRoute::_($sm->link . '&Itemid=' . $sm->id) ?>">
                                                                        <?php echo $sm->title; ?>
                                                                    </a>
                                                                </div>

                                                                <?php
                                                                //Menu con
                                                                if (count($sub_menu_2)) {
                                                                    echo '<div class="wap-see-subcat">';
                                                                    foreach ($sub_menu_2 as $sm2) {
                                                                        $sub_link2 = JRoute::_($sm2->link . '&Itemid=' . $sm2->id);
                                                                        echo '<div class="see-subcat">';
                                                                        echo '<i class="fa fa-angle-right icon_subcat display_none" aria-hidden="true"></i>';
                                                                        echo '  <a href="' . $sub_link2 . '">';
                                                                        echo $sm2->title;
                                                                        echo '  </a>';
                                                                        echo '</div>';
                                                                    }
                                                                    echo '</div>';
                                                                }
                                                                ?>

                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4 show-banner-menu-cat">
                                                        <?php
                                                        if (!empty($banner_img)) {
                                                            ?>
                                                            <img src="<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $banner_img ?>" class="img-responsive" alt="Quảng cáo của <?php echo $mc->title ?>"/>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </li>   
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <?php
                $modules = JModuleHelper::getModules('nelo-search');
                foreach ($modules as $module) {
                    echo JModuleHelper::renderModule($module);
                }
                ?>
            </div>
            <div class="col-md-3">
                <?php
                $modules = JModuleHelper::getModules('nelo-loginout');
                foreach ($modules as $module) {
                    echo JModuleHelper::renderModule($module);
                }

                $modules = JModuleHelper::getModules('nelo-cart');
                foreach ($modules as $module) {
                    echo JModuleHelper::renderModule($module);
                }
                ?>
            </div>
        </div>
    </div>
    <div class="bg_scrollmenu"></div>
    <?php
} else {       //mobile
    ?>
    <!--menu Mobile-->
    <div class="name_all_caterogy name_category_mobile">
        <a>
            Tất cả danh mục
            <span class="float_right">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </a>
    </div>

    <div class="menu-responsive-category">
        <ul>
            <li class="liBackMenuResponsive" onclick="hideMenuCatResponsive()">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
            </li>
            <?php
            $k_sub = 0;
            foreach ($menusC as $kc => $mc) {
                $link = JRoute::_($mc->link . '&Itemid=' . $mc->id);
                $sub_menu = Modmenu_categoriesHelper::getmenu_categories_child($mc->menutype, $mc->id);
                ?>
                <li data-showhide="showhideSubCat_<?php echo $kc ?>" data-status="hide" data-icon="icon_menu_<?php echo $kc ?>"> 
                    <a href="<?php echo $link ?>">
                        <?php echo $mc->title ?>
                    </a>
                    <?php
                    if (count($sub_menu)) {
                        ?>
                        <span class="float_right iconShowHideMenuCatRes icon_menu_<?php echo $kc ?>">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </span>
                        <?php
                    }
                    ?>
                </li>
                <?php
                if (count($sub_menu)) {
                    ?>
                    <div class="catSubMenuRes showhideSubCat_<?php echo $kc ?>">
                        <ul>
                            <?php
                            foreach ($sub_menu as $ksm => $sm) {
                                $link_sub = JRoute::_($sm->link . '&Itemid=' . $sm->id);
                                $sub_menu1 = Modmenu_categoriesHelper::getmenu_categories_child($sm->menutype, $sm->id);
                                ?>
                                <li data-showhide="showhideSubCat1_<?php echo $k_sub ?>" data-status="hide" data-icon="icon_menu_sub_<?php echo $k_sub ?>" data-heightParent="showhideSubCat_<?php echo $kc ?>">
                                    <a href="<?php echo $link_sub ?>">
                                        <?php
                                        echo $sm->title;
                                        ?>
                                    </a>
                                    <?php
                                    if (count($sub_menu1)) {
                                        ?>
                                        <span class="float_right iconShowHideMenuCatRes icon_menu_sub_<?php echo $k_sub ?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                        <?php
                                    }
                                    ?>
                                </li>
                                <?php
                                if (count($sub_menu1)) {
                                    ?>
                                    <div class="catSubMenuRes showhideSubCat1_<?php echo $k_sub ?>">
                                        <ul>
                                            <?php
                                            foreach ($sub_menu1 as $ksm1 => $sm1) {
                                                $link_sub = JRoute::_($sm1->link . '&Itemid=' . $sm1->id);
                                                ?>
                                                <li class="paddingL50">
                                                    <a href="<?php echo $link_sub ?>">
                                                        <?php echo $sm1->title ?>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                }

                                ++$k_sub;
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
    <div class="bg-menu-cat-responsive" onclick="hideMenuCatResponsive()"></div>
    <?php
}
?>

<input type="hidden" id="viewPage" value="<?php echo $view ?>" />

<script>
    jQuery('.main_name_all_caterogy').on('click', function () {
        if (jQuery('.content-menu-caterogy').hasClass('hide_menu_cat')) {
            jQuery('.content-menu-caterogy').addClass('show_menu_cat');
            jQuery('.content-menu-caterogy').removeClass('hide_menu_cat');
        } else {
            jQuery('.content-menu-caterogy').removeClass('show_menu_cat');
            jQuery('.content-menu-caterogy').addClass('hide_menu_cat');
        }
    });

    jQuery('.scroll_name_all_caterogy').on('click', function () {
        if (jQuery('.content-scroll-menu-caterogy').hasClass('hide_menu_cat')) {
            jQuery('.content-scroll-menu-caterogy').addClass('show_menu_cat');
            jQuery('.content-scroll-menu-caterogy').removeClass('hide_menu_cat');
            //hien nen
            jQuery('.bg_scrollmenu').css('display', 'block');
            jQuery('body').addClass('overflow_hidden');
        } else {
            jQuery('.content-scroll-menu-caterogy').removeClass('show_menu_cat');
            jQuery('.content-scroll-menu-caterogy').addClass('hide_menu_cat');
            //bo nen
            jQuery('.bg_scrollmenu').css('display', 'none');
            jQuery('body').removeClass('overflow_hidden');
        }
    });

    /*
     * Bo chon menu scroll
     */
    jQuery('.bg_scrollmenu').on('click', function () {
        jQuery('.content-scroll-menu-caterogy').removeClass('show_menu_cat');
        jQuery('.content-scroll-menu-caterogy').addClass('hide_menu_cat');
        //bo nen
        jQuery('.bg_scrollmenu').css('display', 'none');
        jQuery('body').removeClass('overflow_hidden');
    });

    jQuery(window).scroll(function () {
<?php
$option = JFactory::getApplication()->input->getCmd('option', '');
if ($option == 'com_openshop' && $view == 'frontpage') {
    ?>
            var showHeight = 700;
    <?php
} else {
    ?>
            var showHeight = 150;
    <?php
}
?>
        var scrollTop = jQuery(this).scrollTop();

        if (scrollTop > showHeight) {
            jQuery('.menuScroll').css('display', 'block');
        } else {
            jQuery('.menuScroll').css('display', 'none');
        }
    });

    /*
     * ------------------- RESPONSIVE-----------------
     */

    if (jQuery(window).width() <= 973) {
        if (jQuery('.content-menu-caterogy').hasClass('show_menu_cat')) {
            jQuery('.content-menu-caterogy').removeClass('show_menu_cat');
            jQuery('.content-menu-caterogy').addClass('hide_menu_cat');
        }
        jQuery('.h_s_m_c_t2').addClass('menu_category_resposive');
    } else {
        if (jQuery('.content-menu-caterogy').hasClass('hide_menu_cat')) {
            if (jQuery('#viewPage').val() !== 'frontpage') {
//                jQuery('.content-menu-caterogy').removeClass('hide_menu_cat');
//                jQuery('.content-menu-caterogy').addClass('show_menu_cat');
            }
        }
        jQuery('.h_s_m_c_t2').addClass('menu_category');
    }

    jQuery(window).resize(function () {
        var width = jQuery(this).width();
        if (width <= 973) {
            if (jQuery('.content-menu-caterogy').hasClass('show_menu_cat')) {
                jQuery('.content-menu-caterogy').removeClass('show_menu_cat');
                jQuery('.content-menu-caterogy').addClass('hide_menu_cat');
            }
            jQuery('.h_s_m_c_t2').addClass('menu_category_resposive');
            jQuery('.h_s_m_c_t2').removeClass('menu_category');
        } else {
            if (jQuery('.content-menu-caterogy').hasClass('hide_menu_cat')) {
                jQuery('.content-menu-caterogy').removeClass('hide_menu_cat');
                jQuery('.content-menu-caterogy').addClass('show_menu_cat');
            }
            jQuery('.h_s_m_c_t2').addClass('menu_category');
            jQuery('.h_s_m_c_t2').removeClass('menu_category_resposive');
        }
    });

    jQuery('.name_category_mobile').on('click', function () {
        if (jQuery('.menu-responsive-category').hasClass('show_menu_resposive_cat')) {
            jQuery('.menu-responsive-category').removeClass('show_menu_resposive_cat');
            jQuery('.bg-menu-cat-responsive').css('display', 'none');
        } else {
            jQuery('.menu-responsive-category').addClass('show_menu_resposive_cat');
            jQuery('.bg-menu-cat-responsive').css('display', 'block');
        }
    });

    function hideMenuCatResponsive() {
        jQuery('.menu-responsive-category').removeClass('show_menu_resposive_cat');
        jQuery('.bg-menu-cat-responsive').css('display', 'none');
    }

    jQuery(function () {
        jQuery('.menu-responsive-category li').on('click', function () {
            var status = jQuery(this).attr('data-status');
            var showhide = jQuery(this).attr('data-showhide');
            var icon = jQuery(this).attr('data-icon');

            if (status === 'hide') {        //show
                var h = jQuery('.' + showhide + ' ul').height();

                var heightParent = jQuery(this).attr('data-heightparent');
                if (typeof (heightParent) !== 'undefined') {
                    jQuery('.' + heightParent).css('height', 'auto');
                }

                jQuery('.' + showhide).animate({height: h + 'px'}, 300);
                //bg
                jQuery(this).css('font-weight', 'bold');
                //change staus
                jQuery(this).attr('data-status', 'show');
                //change icon
                jQuery('.' + icon).html('<i class="fa fa-minus" aria-hidden="true"></i>');

            } else {        //hide
                jQuery('.' + showhide).animate({height: '0px'}, 300);
                //bg
                jQuery(this).css('font-weight', '100');
                //change staus
                jQuery(this).attr('data-status', 'hide');
                //change icon
                jQuery('.' + icon).html('<i class="fa fa-plus" aria-hidden="true"></i>');
            }
        });
    });

</script>