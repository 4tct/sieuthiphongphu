<style>
    .menuc > li{
        list-style: none;
        padding:10px 20px;
        background: #444;
        border-bottom: 1px solid #2D2D2D;
        cursor: pointer;
    }
    .menuc li a{
        color: white;
        text-decoration: none;
    }
    .upper{
        text-transform: uppercase;
        font-weight: bold;
    }
    .menuc i{
        color: white;
        float: right;
    }
    .menuc > li:hover{
        background: white;
        border-left: 2px solid #6CC550;
    }
    .menuc > li:hover .sub_menuc{
        display: block;
    }
    .menuc > li:hover > a,.menuc > li:hover > i{
        color: #6CC550;
    }
    ul.sub_menuc{
        position: absolute;
        right: -220px;
        width: 220px;
        background: white;
        z-index: 1000000;
        margin: -30px 0 0 0;
        box-shadow: 1px 4px 8px -3px;
        display: none;
    }
    ul.sub_menuc li{
        padding: 10px 15px;
        list-style: none;
    }
    ul.sub_menuc li:hover > a{
        color: #6CC550;
    }
    ul.sub_menuc li a{
        color: #444444;
    }

</style>

<ul class="menuc">
    <?php
    foreach ($menusC as $c) {
        $p = json_decode($c->params, true);
        $linkP = JRoute::_($c->link . '&Itemid=' . $c->id);
        ?>
        <li onclick="location.href = '<?php echo $linkP ?>'" class="<?php echo $p['pageclass_sfx'] ?>">
            <a href="<?php echo $linkP ?>"><?php echo $c->title ?></a>
            <?php
            $mcs = Modmenu_categoriesHelper::getmenu_categories_child($params->get('menu_id'), $c->id);
            if (count($mcs)) {
                ?>
                <i class="fa fa-angle-right"></i>
                <ul class="sub_menuc">
                    <?php
                    foreach ($mcs as $mc) {
                        $linkMC = JRoute::_($mc->link . '&Itemid=' . $mc->id);
                        ?>
                        <li onclick="location.href = '<?php echo $linkMC ?>'">
                            <a href="<?php echo $linkMC ?>"><?php echo $mc->title ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            }
            ?>
        </li>
        <?php
    }
    ?>
</ul>
