<?php
class Modnelo_top_menuHelper
{
    public static function getnelo_top_menu($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*')
                ->from($db->quoteName('#__menu'))
                ->where('menutype = "'. $params->get('menu_id') .'"')
                ->where('parent_id = 1');
        return $db->setQuery($query)->loadObjectList();
    }
}
