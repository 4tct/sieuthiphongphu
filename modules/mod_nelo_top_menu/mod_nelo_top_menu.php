<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$menus = Modnelo_top_menuHelper::getnelo_top_menu($params);
require JModuleHelper::getLayoutPath('mod_nelo_top_menu');