<?php
// No direct access
defined('_JEXEC') or die;
$session = JFactory::getSession();
$user = $session->get('user');
?>

<style>
    .top{
        background: <?php echo $params->get('bgColor') ?>;
    }
    .menuTop{
        float: <?php echo $params->get('alignAuto') ?>;
    }
    .menuTopSmall{
        float: <?php echo $params->get('alignAuto') ?>;
    }
    .menuTop > ul > li{
        display: inline;
        border-right: 1px solid white;
        padding: 0px 10px;
    }
    .menuTop > ul > li > a{
        color: white;
        text-decoration: none;
        font-size: 12px;
        padding: 12px 0;
    }
    .menuTop ul li a:hover{
        color:#337ab7;
    }
    .iconMenuTop{
        display: none;
        color: white;
        cursor: pointer;
    }
    .menuTopSmall ul{
        position: fixed;
        width: 200px;
        top: 0;
        right: 0;
        background: white;
        padding: 10px;
        z-index: 1000;
        height: 100%;
    }
    .menuTopSmall ul li{
        list-style: none;
        padding: 5px 0;
    }
    .menuTopSmall ul li a{
        text-decoration: none;
        color: #6B6B6B;
    }
    .menuTopSmall ul li a:hover{
        color: #6CC550;
    }
    .bg-menu-top{
        position: fixed;
        top: 0;
        left: 0;
        background: #404040;
        width: 100%;
        height: 100%;
        z-index: 100;
        opacity: 0.5;
        display: none;
    }
    .showInfoCus{
        position: absolute;
        left: 0;
        top: 27px;
        background: white;
        display: none; 
        width: 160px;
        border: 1px solid #636363;
        z-index: 100;
    }
    ul.showInfoCus{
        list-style: none;
    }
    ul.showInfoCus > li{
        padding: 5px 10px;
    }
    ul.showInfoCus > li > a{
        color: black;
        text-decoration: none;
    }
    ._account:hover .showInfoCus{
        display: block;
    }
</style>

<?php
if ($params->get('selectMenuTop') == 'm1') {
    ?>
    <div class="menuTop">
        <div class="iconMenuTop" onclick="showMenuTop()">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <div class="bg-menu-top"></div>
        <ul>
            <?php
            foreach ($menus as $menu) {
                ?>
                <li>
                    <a href="<?php echo $menu->link . '&Itemid=' . $menu->id ?>"><?php echo $menu->title; ?></a>
                </li>
                <?php
            }

            if ($params->get('chooseShowLogInOut')) {
                if ($user->id) {
                    ?>
                    <li class="_account position_relative">
                        <a href="#" onclick="showInfoCusSite()"><?php echo $user->name ?></a>
                        <ul class="showInfoCus">
                            <li>
                                <a href="/thong-tin-ca-nhan.html">Thông tin cá nhân</a>
                            </li>
                            <li>
                                <a href="/lich-su-mua-hang.html">Lịch sử mua hàng</a>
                            </li>
                            <li>
                                <form action="<?php echo JRoute::_('index.php?option=com_users&task=user.logout'); ?>" method="post" id="formLogout">
                                    <a title="My History" href="#" onclick="formLogout()">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        Đăng xuất
                                    </a>
                                    <?php
                                    $return = substr($_SERVER['REQUEST_URI'], 1, strlen($_SERVER['REQUEST_URI']));
                                    ?>
                                    <input type="hidden" name="return" id="btl-return" value="<?php echo base64_encode($return); ?>">
                                    <?php echo JHtml::_('form.token'); ?>
                                </form>
                            </li>
                        </ul>
                    </li>
                    <?php
                } else {
                    ?>
                    <li>
                        <a href="#" onclick="showSignInUp()">Tài khoản</a>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
    <?php
} else if ($params->get('selectMenuTop') == 'm2') {
    require 'menu_top_type_2.php';
}
//form Sign In - Sign Up
require 'formSignInUp.php';
?>