<?php
$db = JFactory::getDbo();
$query = $db->getQuery(TRUE);
?>

<!--//LogIn-->
<form action="index.php?option=com_users&task=user.login" method="post" id="formLogin" class="form-horizontal">
    <div class="modal fade" id="modalFormLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Đăng nhập</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="username" id="username" value="" placeholder="Tài khoản" class="form-control"/>
                    <div class="error error-username"></div>
                    <br>
                    <input type="password" name="password" id="password" value="" placeholder="Mật khẩu" class="form-control"/>
                    <div class="error error-password"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="signUpForm()">Đăng ký</button>   
                    <button type="button" class="btn btn-primary" onclick="loginForm()">Đăng nhập</button>
                    <?php
                    $return = JRoute::_($_SERVER['REQUEST_URI']);
                    ?>
                    <input type="hidden" name="return" id="btl-return" value="<?php echo base64_encode($return) ?>">
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </div>
        </div>
    </div>
</form>


<!--//Sign Up-->
<form action="index.php?option=com_users&task=user.registerJ" method="post" id="formRegister" class="form-horizontal">
    <div class="modal fade" id="myModalSignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Đặt ký nhanh</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3">Tên tài khoản <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập tên tài khoản của bạn" name="username" id="usernameR"/>
                            <div class="error error-usernameR"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Mật khẩu <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="password" value="" class="form-control" placeholder="Nhập mật khẩu của bạn" name="password" id="passwordR"/>
                            <div class="error error-passwordR"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Nhập lại mật khẩu <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="password" value="" class="form-control" placeholder="Nhập mật khẩu của bạn" name="password_again" id="password_again"/>
                            <div class="error error-password_again"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Họ và tên <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập họ và tên của bạn" name="customer_name" id="customer_name"/>
                            <div class="error error-customer_name"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Số điện thoại <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập số điện thoại của bạn" name="customer_phone" id="customer_phone"/>
                            <div class="error error-customer_phone"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Email </label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập enail của bạn" name="customer_email" id="customer_email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Địa chỉ <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <input type="text" value="" class="form-control" placeholder="Nhập địa chỉ của bạn bao gồm số nhà, đường,..." name="customer_address" id="customer_address"/>
                            <div class="error error-customer_address"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Tỉnh/Thành Phố <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <select name="zone" id="customer_town" class="form-control" onchange="changeZone(this.value)">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_ZONE') ?></option>
                                <?php
                                $query->clear();
                                try {
                                    $query->select('id,zone_name')
                                            ->from($db->quoteName('#__openshop_zones'))
                                            ->where('published = 1');
                                } catch (Exception $ex) {
                                    //error
                                }
                                $town = $db->setQuery($query)->loadObjectList();

                                foreach ($town as $v_z) {
                                    $cls = '';
                                    echo '<option value="' . $v_z->id . '" ' . $cls . '>' . $v_z->zone_name . '</option>';
                                }
                                ?>
                            </select>
                            <div class="error error-customer_town"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3">Quận/Huyện <i style="color:red;">(*)</i></label>
                        <div class="col-md-9">
                            <select name="district" id="customer_district" class="form-control">
                                <option value="0"><?php echo JText::_('OPENSHOP_CHOOSE_DISTRICT') ?></option>
                                <?php
                                try {
                                    $query->clear();
                                    $query->select('id,district_name')
                                            ->from($db->quoteName('#__openshop_districts'))
                                            ->where('published = 1');
                                } catch (Exception $ex) {
                                    //error
                                }
                                $district = $db->setQuery($query)->loadObjectList();

                                foreach ($district as $v_d) {
                                    $cls = '';
                                    echo '<option value="' . $v_d->id . '" ' . $cls . '>' . $v_d->district_name . '</option>';
                                }
                                ?>
                            </select>
                            <div class="error error-customer_district"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3"></label>
                        <div class="col-md-9">
                            <i style="color:red;">(*)</i> Yêu cầu nhập
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="formRegister()">Đăng ký</button>
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </div>
        </div>
    </div>
</form>
