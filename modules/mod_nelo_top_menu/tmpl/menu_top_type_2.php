<style>
    ul.menu_top_account, ul.account_child_m_t_2, ul.menu_top_cart{
        list-style: none;
    }
    ul.menu_top_cart{
        float: left;
    }
    ul.menu_top_account{
        float: right;
    }
    ul.menu_top_account > li, ul.menu_top_cart > li{
        float: left;
    }
    ul.menu_top_account li.border_right_m_t_2, ul.menu_top_cart > li.border_right_m_t_2{
        border-right: 1px solid #e4e4e4;
        padding-right: 10px;
    }
    ul.menu_top_account li.border_left_m_t_2, ul.menu_top_cart > li.border_left_m_t_2{
        padding-left: 10px;
        padding-bottom: 10px;
    }
    .menutop_m2 ul li a{
        text-decoration: none;
        color: #41a1f3;
    }
    ul.menu_top_account > li:hover ul.account_child_m_t_2{
        opacity: 1;
        transform: translate(20px, 0px);
        z-index: 1000;
        transition: all 0.3s;
    }
    ul.account_child_m_t_2{
        position: absolute;
        background: white;
        top: 30px;
        right: 0;
        opacity: 0;
        transform: translate(20px, 20px);
        z-index: -1;
        min-width: 200px;
        box-shadow: 0px 0px 5px 0px #c5c5c5;
    }
    ul.account_child_m_t_2 > li{
        padding: 7px 25px;
    }
    ul.account_child_m_t_2 > li a{
        color: #777777;
    }
    ul.account_child_m_t_2 > li:hover a{
        color: #da1400;
    }
</style>

<div class="col-sm-5 col-md-5 menutop_m2">
    <ul class="menu_top_cart">
        <li class="border_left_m_t_2">
            Hotline: 1900 1090
        </li>
    </ul>
</div>
<div class="col-sm-7 col-md-7 menutop_m2">
    <ul class="menu_top_account">
        <?php
        if (!empty($user->id)) {
            ?>
            <li class="border_left_m_t_2">
                <i class="fa fa-user" aria-hidden="true"></i> 
                <span class="cursor_pointer">
                    <?php
                    echo $user->name;
                    ?>
                </span> 
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <!--menu child-->
                <ul class="account_child_m_t_2">
                    <li>
                        <a href="thong-tin-ca-nhan.html">
                            Thông tin cá nhân
                        </a>
                    </li>
                    <li>
                        <a href="gio-hang.html">
                            Giỏ hàng
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Sản phẩm yêu thích
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Lịch sử mua hàng
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        } else {
            ?>
            <li class="border_right_m_t_2">
                Chào mừng bạn đến, 
                <span class="hidden_test_top">
                    Bạn có thể <a href="#">Đăng nhập</a>
                    hoặc <a href="#">Tạo một tài khoản</a>
                </span>
            </li>
            <li class="border_left_m_t_2">
                <i class="fa fa-user" aria-hidden="true"></i> 
                <span class="cursor_pointer">Tài khoản</span> 
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <!--menu child-->
                <ul class="account_child_m_t_2">
                    <li>
                        <a href="dang-nhap.html">
                            Đăng nhập
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Giỏ hàng
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Sản phẩm yêu thích
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
    </ul>

</div>
<script src="<?php echo JUri::base() . 'components/com_openshop/assets/js/responsive/responsiveTopType2.js' ?>"></script>
