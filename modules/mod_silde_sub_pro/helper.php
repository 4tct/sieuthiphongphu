<?php

class Modsilde_sub_proHelper {

    public static function getProductSC($type, $limit) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);

        switch ($type) {
            case '1':
                $query->select('a.*,b.product_name')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.product_price_r > 0')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1');
                break;
            case '2':
                $query->select('a.*,b.product_name')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.product_hot > 0')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1');
                break;
            case '3':
                $query->select('a.*,b.product_name')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1')
                        ->order('a.buyer_virtual DESC');
                break;
            case '4':
                $query->select('a.*,b.product_name')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.product_featured = 1')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1')
                        ->order('rand()');
                break;
            default :
                $query->select('a.*,b.product_name')
                        ->from($db->quoteName('#__openshop_products', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . 'ON a.id = b.product_id')
                        ->where('a.delete_status = 0')
                        ->where('a.published = 1');
                break;
        }
        return $db->setQuery($query, 0, $limit)->loadObjectList();
    }

}
