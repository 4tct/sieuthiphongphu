<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .sub_pro_<?php echo $module->id ?>{
        float: <?php echo $params->get('floatLR') ?>;
    }
    .sub_pro_border_<?php echo $module->id ?>{
        border: 2px solid <?php echo $params->get('colorWapSubPro') ?>;
        border-top: none;
        border-radius: 0 0 5px 5px;
    }
    .sub_pro{
        margin-top: 20px;
    }
    .sub_pro .content_sub_pro_show .name_pro_3_t4{
        padding: 6px 0 6px 6px;
        font-size: 15px;
    }
    .sub_pro .content_sub_pro_show .name_pro_3_t4 a{
        color: #383838;
        text-decoration: none;
        font-size: 15px;
    }
    .sub_pro .content_sub_pro_show .name_pro_3_t4 a h5{
        text-align: justify;
        text-transform: capitalize;
    }
    .sub_pro .title_show_t4{
        color: white;
        text-transform: uppercase;
    }
    .sub_pro .title_show_t4 i{
        padding: 15px;
        color: white;
        text-transform: uppercase;
        font-weight: bold;
    }
    .sub_pro .title_show_t4 span{
        color: white;
        text-transform: uppercase;
        font-weight: bold;
        padding-left: 10px;
    }
    .sub_pro .img_pro_3_t4{
        float: left;
        margin-right: 8px;
        position: relative;
    }
    .sub_pro .name_pro_3_t4 a{
        font-size: 16px;
        color: #5f5f5f;
        text-decoration: none;
    }
    .sub_pro .price_pro_3_t4 .price_pro_sale_3_t4{
        font-size: 14px;
        text-decoration: line-through;
        color: #a5a5a5;
    }
    .sub_pro .price_pro_3_t4 .price_pro_3_t4{
        font-size: 18px;
        color: red;
        font-weight: bold;
    }

    .sub_pro .content_sub_pro_show .slider_3_t4_j{
        padding: 5px 8px 5px 5px;
    }
    .sub_pro .content_sub_pro_show .hit_pro_3_t4{
        padding-top: 10px;
    }
    .sub_pro .content_sub_pro_show .hit_pro_3_t4 .vb{
        display: inline-block;
        font-size: 14px;
        padding: 0 5px;
        color: #2f5271;
    }


    /*--------------*/
    .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: block;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
    .sub_pro div.title_cat_<?php echo $module->id ?>, .sub_pro .title_cat_<?php echo $module->id ?> li{
        background: <?php echo $params->get('colorWap4') ?>;
    }

    .btn_datmua_subcat{
        margin-top: 10px;
    }
    .btn_datmua_subcat a{
        padding: 10px 15px;
        background: #57b4c5;
        font-size: 14px;
        color: white;
        cursor: pointer;
        text-decoration: none;
    }
    .btn_datmua_subcat a:hover{
        background: #0a68b3;
    }

    .sub_pro .wapContetPro_<?php echo $module->id ?>{
        position: relative;
        border: 3px solid <?php echo $params->get('colorWap4') ?>;
        border-radius: 5px;
        background: white;
    }
    .sub_pro .slideLR{
        display: inline;
        float: right;
    }
    .sub_pro .slideLR div{
        display: inline-block;
    }
    .sub_pro .slideLR div i{
        padding: 15px 10px;
        cursor: pointer;
    }
    .sub_pro .wapsub_<?php echo $module->id ?>{
        padding-left: 0;
        padding-right: 0px;
        border: 3px solid <?php echo $params->get('colorWapSubPro') ?>;
        border-radius: 5px;
    }
    .sub_pro .title_show_t4_<?php echo $module->id ?> {
        background: <?php echo $params->get('colorWapSubPro') ?>;
    }
</style>

<?php
$clsSub = 'sub_pro_' . $module->id;
if (OpenShopHelper::isMobile()) {
    $clsSub = '';
}
echo $moduleclass_sfx = htmlspecialchars($params->get('header_class'));
?>

<div class="col-sm-12 sub_pro col-md-<?php echo $params->get('column') ?> padding0 <?php echo $clsSub ?>">
    <div class="content_sub_pro_show">
        <div class="wow fadeInLeft wapsub_<?php echo $module->id ?>">
            <div class="title_show_t4 title_show_t4_<?php echo $module->id ?>">
                <i class="fa fa-flag" aria-hidden="true"></i>
                <span><?php echo $module->title ?></span>

                <div class="slideLR">
                    <div class="slideL btnPrevSlide" data-placement="bottom" data-toggle="tooltip" title="Xem sản phẩm trước">
                        <i class="fa fa-caret-left" aria-hidden="true"></i>
                    </div>
                    <div class="slideL btnNextSlide" data-placement="bottom" data-toggle="tooltip" title="Xem sản phẩm sau">
                        <i class="fa fa-caret-right" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="sub_pro_show">
                <div class="swiper-container sub_pro_<?php echo $module->id ?> sub_pro_border_<?php echo $module->id ?>">
                    <div class="swiper-wrapper silde-boder">
                        <?php
                        $product = Modsilde_sub_proHelper::getProductSC($params->get('chooseTypeInProSub'), $params->get('numPro'));

                        foreach ($product as $ps) {
                            $link = JRoute::_(OpenShopRoute::getProductRoute($ps->id, OpenShopHelper::getProductCategory($ps->id)))
                            ?>
                            <div class="swiper-slide slider_3_t4_j">
                                <div class="wap_pro_3_t4">
                                    <div class="img_pro_3_t4">
                                        <!--<a href="<?php echo $link ?>">-->
                                        <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ps->id) ?>', '<?php echo $link ?>')">
                                            <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $ps->product_image ?>" alt="<?php echo $ps->product_name ?>" title="<?php echo $ps->product_name ?>" width="148"/>
                                        </a>

                                        <div class="ProSKU">
                                            <?php
                                            echo $ps->product_sku;
                                            ?>
                                        </div>

                                        <?php
                                        if ($ps->product_price_r > 0) {
                                            $percent = 100 - (int) (((int) $ps->product_price * 100) / $ps->product_price_r)
                                            ?>
                                            <div class="percentSale rightPercent1"> 
                                                <?php echo $percent ?>%
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="info_pro_3_t4">
                                        <div class="name_pro_3_t4">
                                            <!--<a href="<?php echo $link ?>">-->
                                            <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ps->id) ?>', '<?php echo $link ?>')">
                                                <h5 title="<?php echo $ps->product_name; ?>">
                                                    <?php
                                                    echo $ps->product_name;
                                                    ?>
                                                </h5>
                                            </a>
                                        </div>
                                        <div class="price_pro_3_t4">
                                            <?php
                                            $clsP = '';
                                            if ($ps->product_price_r > 0) {
                                                $clsP = 'style="padding-top:10px;"';
                                                ?>
                                                <div class="price_pro_sale_3_t4">
                                                    <?php echo number_format((int) $ps->product_price_r, 0, ',', '.') ?><sup>đ</sup>
                                                    <sub><?php echo OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) ?></sub>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="price_pro_3_t4" <?php echo $clsP ?>>
                                                <?php echo number_format((int) $ps->product_price, 0, ',', '.') ?><sup>đ</sup>
                                                <sub><?php echo OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) ?></sub>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn_datmua_subcat">
                                        <!--<a href="<?php echo $link ?>">-->
                                        <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($ps->id) ?>', '<?php echo $link ?>')">
                                            Đặt mua
                                        </a>
                                    </div>

                                    <div class="hit_pro_3_t4">
                                        <div class="vb" data-toggle="tooltip" title="Lượt xem">
                                            <?php echo $ps->hits; ?>
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </div>
                                        <div data-toggle="tooltip" title="Lượt mua" class="vb">
                                            <?php echo $ps->buyer_virtual; ?>
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var swiper = new Swiper('.sub_pro_<?php echo $module->id ?>', {
        pagination: '.swiper-pagination',
        slidesPerView: <?php echo $params->get('showProColumn') ?>,
        slidesPerColumn: <?php echo $params->get('showProRow') ?>,
        paginationClickable: true,
        spaceBetween: -1,
        autoplay: <?php echo (int) $params->get('autoplay') * 1000 ?>,
        effect: 'slide',
        nextButton: '.btnNextSlide',
        prevButton: '.btnPrevSlide',
        breakpoints: {
            1024: {
                slidesPerView: 2
            },
            768: {
                slidesPerView: 2
            },
            640: {
                slidesPerView: 1
            },
            320: {
                slidesPerView: 1
            }
        }
    });

</script>
