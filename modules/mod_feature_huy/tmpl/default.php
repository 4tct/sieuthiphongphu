<?php
// No direct access
defined('_JEXEC') or die;
?>
<style>
    .featureIj{
        padding: 0 40px;
    }
    .featureIj ul li{
        text-align: center;
        padding: 70px 0;
    }
    .icon-show{
        font-size: 50px;
    }
    .featureIj ul li span{
        color: grey;
    }
</style>

<?php
$t = 12 / substr($params->get('selectColumn'), 1, 1);
?>

<div class="featureIj">
    <ul>
        <?php
        for ($i = 1; $i <= substr($params->get('selectColumn'), 1, 1); $i++) {
            ?>
            <li class="col-ms-<?php echo $t ?> col-md-<?php echo $t ?> wow fadeInDown" data-sppb-wow-delay="<?php echo $i . '00ms' ?>" style="animation-delay: <?php echo $i . '00ms' ?>;">
                <div class="<?php echo $params->get('icon' . $i) ?> icon-show"></div>
                <h3><?php echo $params->get('title' . $i) ?></h3>
                <span>
                    <?php echo $params->get('content' . $i) ?>
                </span>
            </li>
            <?php
        }
        ?>
    </ul>
</div>

<script>
    var wow = new WOW(
            {
                animateClass: 'animated',
                offset: 100,
            }
    );
    wow.init();
</script>