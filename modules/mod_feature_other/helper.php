<?php
class Modfeature_otherHelper
{
    public static function getfeature_other($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('media_image')
                ->from($db->quoteName('#__openshop_mediadetails'))
                ->where('media_id = ' . $params->get('imgvideo'));
        return $db->setQuery($query)->loadObject();
    }
    
    public static function getLinkShow($params){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,link')
                ->from($db->quoteName('#__menu'))
                ->where('id = ' . $params->get('linkButton'));
        $row = $db->setQuery($query)->loadObject();
        
        return $row->link . '&ItemId=' . $row->id;
    }
}
