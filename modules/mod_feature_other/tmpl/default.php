<?php
// No direct access
defined('_JEXEC') or die;
?>
<style>
    .intro_<?php echo $params->get('classIntro') ?>{
        background: <?php echo $params->get('bgcolor') ?>;
    }
    .intro ul li{
        padding: 20px 0;
    }
    .button-feature{
        padding: 30px 0;
    }
    .button-feature a:hover{
        background: <?php echo $params->get('colorHoverButton') ?>;
    }
    .button-feature a{
        padding: 15px 50px;
        color: <?php echo $params->get('colorText') ?>;
        background: <?php echo $params->get('colorButton') ?>;
        border-radius: <?php echo $params->get('borderRButton') . 'px' ?>;
        cursor: pointer;
        text-decoration: none;
    }
</style>

<div class="col-md-12 intro_<?php echo $params->get('classIntro') ?>">
    <ul class="col-md-10 col-md-offset-1" style="padding: 30px 0;">
        <li class="col-md-6 wow slideInRight">
            <?php
            if ($params->get('selectPos') == 'c1') {
                ?>
                <div style="text-align: center">
                    <?php
                    $imgEx = array('png', 'jpg', 'jpeg', 'gif');
                    $extension = explode('.', $img->media_image);
                    if (in_array($extension[1], $imgEx)) {
                        ?>
                        <img class="lazy img-responsive" data-original="images/com_openshop/medias/<?php echo $img->media_image ?>" width="420px"/>
                        <?php
                    } else {
                        ?>
                        <video width="420" height="400" controls>
                            <source src="images/com_openshop/medias/<?php echo $img->media_image ?>" type="video/mp4">
                        </video>
                        <?php
                    }
                    ?>
                </div>
                <?php
            } else if ($params->get('selectPos') == 'c2') {
                ?>
                <h2><b><?php echo $params->get('title') ?></b></h2>
                <div style="text-align: justify;"><?php echo $params->get('content') ?> </div>
                <?php
                if ($params->get('showButton')) {
                    ?>
                    <div class="button-feature">
                        <a href="<?php echo $link ?>">
                            <?php echo $params->get('titleButton') ?>
                        </a>
                    </div>
                    <?php
                }
                ?>

                <?php
            }
            ?>
        </li>
        <li class="col-md-6 wow slideInLeft">
            <?php
            if ($params->get('selectPos') == 'c2') {
                ?>
                <div style="text-align: center">
                    <?php
                    $imgEx = array('png', 'jpg', 'jpeg', 'gif');
                    $extension = explode('.', $img->media_image);
                    if (in_array($extension[1], $imgEx)) {
                        ?>
                        <img class="lazy img-responsive" data-original="images/com_openshop/medias/<?php echo $img->media_image ?>" width="420px"/>
                        <?php
                    } else {
                        ?>
                        <video width="420" height="240" controls class="img-responsive">
                            <source src="images/com_openshop/medias/<?php echo $img->media_image ?>" type="video/mp4">
                        </video>
                        <?php
                    }
                    ?>
                </div>
                <?php
            } else if ($params->get('selectPos') == 'c1') {
                ?>
                <h2><b><?php echo $params->get('title') ?></b></h2>
                <div style="text-align: justify;"><?php echo $params->get('content') ?> </div>
                <?php
                if ($params->get('showButton')) {
                    ?>
                    <div class="button-feature">
                        <a href="<?php echo $link ?>">
                            <?php echo $params->get('titleButton') ?>
                        </a>
                    </div>
                    <?php
                }
                ?>
                <?php
            }
            ?>
        </li>
    </ul>
</div>

<script>
    var wow = new WOW({
       animateClass: 'animated',
       offset: 100
    });
    
    wow.init();
</script>