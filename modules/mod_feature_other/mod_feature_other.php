<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$img = Modfeature_otherHelper::getfeature_other($params);
$link = Modfeature_otherHelper::getLinkShow($params);
require JModuleHelper::getLayoutPath('mod_feature_other');