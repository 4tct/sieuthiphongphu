<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
//$hello = Modnelo_categories_siteHelper::getnelo_categories_site($params);
$cat = Modnelo_categories_siteHelper::getCategory($params);
$subcat = Modnelo_categories_siteHelper::getSubCategory($params);
$proCat = Modnelo_categories_siteHelper::getProductCat($params);
require JModuleHelper::getLayoutPath('mod_nelo_categories_site');