<?php
// No direct access
defined('_JEXEC') or die;
$session = JFactory::getSession();
$user = $session->get('user');
JFactory::getDocument()->addScript(JUri::base(true) . '/components/com_openshop/assets/js/jscolor.js');

$modules = JModuleHelper::getModules('nelo-category-site');
foreach ($modules as $module) {
	$moduleName = $module->module;
}

//get config
$config = OpenShopHelper::getOpenshopTemplate($user->id, 'config', $moduleName);
if(count($config)){
	$cf = json_decode($config->config, true);
	if(isset($cf[$params->get('cat_id')])){
		$config_catS = $cf[$params->get('cat_id')];
		$bgcolor = isset($config_catS['bgcolor']) ? '#' . $config_catS['bgcolor'] : $params->get('bg');
		$textcolor = isset($config_catS['textcolor']) ? '#' . $config_catS['textcolor'] : $params->get('color');	
		$crosscolor = isset($config_catS['crosscolor']) ? '#' . $config_catS['crosscolor'] : '#0043FF';
		
		//row - col-md
		$rows = isset($config_catS['rows']) ? $config_catS['rows'] : $params->get('rowsShow');
		$cols = isset($config_catS['cols']) ? $config_catS['cols'] : $params->get('colsShow');
	}
	else
	{
		$bgcolor = $params->get('bg');
		$textcolor = $params->get('color');
		$crosscolor = '#0043FF';
		
		//row - col-md
		$cols = $params->get('colsShow');
		$rows = $params->get('rowsShow');
	}
}
else
{
	$bgcolor = $params->get('bg');
	$textcolor = $params->get('color');
	$crosscolor = '#0043FF';
	
	//row - col-md
	$cols = $params->get('colsShow');
	$rows = $params->get('rowsShow');
}

?>

<style>
    .catS_<?php echo $cat->id ?> {
        margin: 0 auto;
    }
    .catS_<?php echo $cat->id ?> .bgCStie{
        background: <?php echo  $bgcolor ?>;
        padding: 0;
    }
	.catS_<?php echo $cat->id ?> .bgCStiePD{
		margin: 0 0 20px 0;
	}
    .catS_<?php echo $cat->id ?> .bgCStie h2{
        display: inline-block;
        margin: 15px;
    }
    .catS_<?php echo $cat->id ?> .bgCStie .btnForUser{
        display: inline;
        font-size: 20px;
		float:right;
        cursor: pointer;
		padding-top: 15px;
    }
    .catS_<?php echo $cat->id ?> .bgCStie .btnForUser span{
        padding: 0 5px;
    }
    .catS_<?php echo $cat->id ?> .boderCSite{
        border-top: 3px solid <?php echo $crosscolor ?>;
    }
    .catS_<?php echo $cat->id ?> .boderCSite h1{
        padding: 0 0 0 20px;
    }
    .catS_<?php echo $cat->id ?> .colShowP{
        padding: 0; 
        background: white;
    }
    .catS_<?php echo $cat->id ?> .colShowP .colShow{
        padding: 0; 
        border: 1px solid #EEE;
    }
    .catS_<?php echo $cat->id ?> .catName{
        text-align: center;
        font-size: 20px;
        font-weight: bold;
        text-transform: uppercase;
        background: #E0E0E0;
        padding: 20px;
        margin: 0 0 10px 0;
    }
    .catS_<?php echo $cat->id ?> .titleProCat{
        padding: 10px;
        text-align: center;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .catS_<?php echo $cat->id ?>.titleProCat .productPR{
        text-decoration: line-through;
        color: #CCC;
    }
    .catS_<?php echo $cat->id ?> .titleProCat .productP{
        font-weight: bold;
        font-size: 20px;
        color: <?php echo  $textcolor ?>;
    }
    .catS_<?php echo $cat->id ?> .colShowP .colShow:hover{
        opacity: 0.7;
        cursor: pointer;
    }
    .catS_<?php echo $cat->id ?> .colShowP .colShow:hover .titleProCat{
        color: <?php echo  $textcolor ?>;
    }
    .catS_<?php echo $cat->id ?> .colShowP a{
        text-decoration: none;
    }
    .catS_<?php echo $cat->id ?> .colShowP a .titleProCat{
        color: #777;
    }
    .catS_<?php echo $cat->id ?> .subCatName{
        margin: 0 10px;
    }
    .catS_<?php echo $cat->id ?> .subCatName .titleSubCat{
        margin: 5px;
        padding: 5px 10px;
        background: white;
        color: #777;
        border-radius: 5px;
        float: left;
        border: 1px solid #CCCCCC;
        cursor: pointer;
    }
    .catS_<?php echo $cat->id ?> .colCatName{
        background: <?php echo  $bgcolor ?>; 
        padding: 0;
    }
    .catS_<?php echo $cat->id ?> .subCatName a{
        text-decoration: none;
    }
    .catS_<?php echo $cat->id ?> .subCatName a.titleSubCat:hover{
        background: <?php echo  $textcolor ?>;
        color: white;
        border: 1px solid <?php echo  $textcolor ?>;
    }
    .catS_<?php echo $cat->id ?> .splitText{
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
	.catS_<?php echo $cat->id ?> .catName a{
		text-decoration: none;
		color: #555;
		cursor:pointer;
	}
	.catS_<?php echo $cat->id ?> .catName a:hover{
		color: <?php echo  $textcolor ?>;
	}
	.catS_<?php echo $cat->id ?> .loadBanner{
		width: 100%;
		background: #777;
	}
	.catS_<?php echo $cat->id ?> .loadBanner .sizeBanner{
		    text-align: center;
		font-size: 30px;
		font-weight: bold;
		color: white;
	}
	.tab-pane{
		padding: 15px 0;
	}
</style>

<?php
switch ($cols) {
    case '3':
        $colMD = 13;
        $colCat = 21;
        break;
    case '4':
        $colMD = 11;
        $colCat = 16;
        break;
    default :
        break;
}
?>
<div class="catS_<?php echo $cat->id ?>">
    <div class="bgCStie col-md-60-2r boderCSite">
        <h2 class="titleCategory_<?php echo $cat->id ?>">
			<?php 
				$cat_site = OpenShopHelper::getOpenshopTemplate($user->id,'cat_site');
				if(count($cat_site)){
					$ct = json_decode($cat_site->cat_site, true);
					if(isset($ct[$cat->id])){
						$nameCat = trim($ct[$cat->id]);
					}
					else{
						$nameCat = trim($cat->category_name);
					}
				}
				else
				{
					$nameCat = trim($cat->category_name);
				}
				echo $nameCat;
			?>
		</h2>
		<input class="noneD" type="text" value="<?php echo $nameCat ?>" id="valueCategory_<?php echo $cat->id ?>" style="margin: 10px 20px;font-size: 30px; height: 45px;"/>
		<?php
			if($user->id != '' && $user->id != 0){
				?>
				<div class="btnForUser">
					<span class="fa fa-edit" onclick="editCategorySite(<?php echo $cat->id ?>)"></span>
					<span class="fa fa-save" onclick="saveCategorySite(<?php echo $cat->id ?>)"></span>
					<span class="fa fa-cog" onclick="showConfigSite(<?php echo $cat->id ?>)"></span>
				</div>
				<?php
			} 
		?>
    </div>
    <div class="bgCStie col-md-60-2r bgCStiePD">
        <?php
        if ($params->get('positionCat') == 'l') {
            ?>
            <div class="colCatName col-md-<?php echo $colCat ?>-2r" >
                <img src="<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $cat->category_image ?>" class="img-responsive" width="100%" alt="<?php echo $cat->category_name ?>" title="<?php echo $cat->category_name ?>"/>
                <div class="catName">
                    <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($cat->id)); ?>" title="<?php echo $cat->category_name ?>"><?php echo $cat->category_name ?></a>
                </div>
                <div class="subCatName">
                    <?php
                    foreach ($subcat as $k => $sc) {
                        ?>
                        <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($sc->id)); ?>" class="titleSubCat tsc_<?php echo $k ?>"><?php echo $sc->category_name ?></a>
                        <?php
                    }
                    ?>
                </div>
				<div class="clr"></div>
				<div class="loadBanner">
					<!-- img banner -->
					<div class="sizeBanner"></div>
				</div>
            </div>
            <?php
        }
        ?>
        <?php
        $v = 0;
        for ($i = 1; $i <= $cols; $i++) {
            $check = 1;
            ?>
            <div class="colShowP col-md-<?php echo $colMD ?>-2r">
                <?php
                for ($j = 1; $j <= $rows; $j++) {
					$cartID = OpenShopHelper::getProductCategory($proCat[$v]->id);
                    $urlP = JRoute::_(OpenShopRoute::getProductRoute($proCat[$v]->id, $cartID));
                    ?>
                    <a href="<?php echo $urlP ?>" class="colShow col-md-60-2r">
                        <img src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $proCat[$v]->product_image ?>" class="img-responsive" width="100%" alt="<?php echo $proCat[$v]->product_name ?>" title="<?php echo $proCat[$v]->product_name ?>"/>
                        <div class="titleProCat" title="<?php echo $proCat[$v]->product_name ?>">
                            <?php
                            $name = substr($proCat[$v]->product_name, 0, 60);
                            if (strlen($proCat[$v]->product_name) > 60) {
                                $name .= '...';
                            }
                            echo $name;
                            ?>
                            <br/>
                            <span class="productP"><?php echo number_format($proCat[$v]->product_price, 0, ',', '.') ?><sup>đ</sup></span>
                            <?php
                            if ($proCat[$v]->product_price_r) {
                                echo '<span class="productPR">' . number_format($proCat[$v]->product_price_r, 0, ',', '.') . '<sup>đ</sup></span>';
                            }
                            ?>
                        </div>
                    </a>
                    <?php
                    ++$v;
                    $check = 0;
                }
                ?>
            </div>
            <?php
            if ($check) {
                ++$v;
            }
        }
        ?>
        <?php
        if ($params->get('positionCat') == 'r') {
            ?>
            <div class="colCatName col-md-<?php echo $colCat ?>-2r">
                <img src="<?php echo OPENSHOP_PATH_IMG_CATEGORIES_HTTP . $cat->category_image ?>" class="img-responsive" width="100%" alt="<?php echo $cat->category_name ?>" title="<?php echo $cat->category_name ?>"/>
                <div class="catName">
                    <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($cat->id)); ?>" title="<?php echo $cat->category_name ?>"><?php echo $cat->category_name ?></a>
                </div>
                <div class="subCatName">
                    <?php
                    foreach ($subcat as $k => $sc) {
                        ?>
                        <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($sc->id)); ?>" class="titleSubCat tsc_<?php echo $k ?>"><?php echo $sc->category_name ?></a>
                        <?php
                    }
                    ?>
                </div>
				<div class="clr"></div>
				<div class="loadBanner loadBanner_<?php echo $cat->id ?>">
					<!-- img banner -->
					<div class="sizeBanner"></div>
				</div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<!-- Config -->
<div class="modal fade" id="myModalConfig_<?php echo $cat->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cấu hình cho danh mục</h4>
      </div>
      <div class="modal-body form-horizontal">
		<form id="mcontent_<?php echo $cat->id ?>">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#color_<?php echo $cat->id ?>" aria-controls="color_<?php echo $cat->id ?>" role="tab" data-toggle="tab">Màu sắc</a></li>
				<li role="presentation" class=""><a href="#content_<?php echo $cat->id ?>" aria-controls="content_<?php echo $cat->id ?>" role="tab" data-toggle="tab">Nâng cao</a></li>
			</ul>
			<div class="tab-content">
			<!-- //color -->
				<div role="tabpanel" class="tab-pane active" id="color_<?php echo $cat->id ?>">
					<div class="form-group">
						<label class="col-md-4">Chọn màu nền</label>
						<div class="col-md-8">
							<input type="text" name="bgcolor_<?php echo $cat->id ?>" id="bgcolor_<?php echo $cat->id ?>" class="jscolor form-control" value="<?php echo $bgcolor ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Chọn màu khi rê chuột</label>
						<div class="col-md-8">
							<input type="text" name="textcolor_<?php echo $cat->id ?>" id="textcolor_<?php echo $cat->id ?>" class="jscolor form-control" value="<?php echo $textcolor ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Chọn màu gạch ngang trên</label>
						<div class="col-md-8">
							<input type="text" name="crosscolor_<?php echo $cat->id ?>" id="crosscolor_<?php echo $cat->id ?>" class="jscolor form-control" value="<?php echo $crosscolor ?>" />
						</div>
					</div>
				</div>
				
			<!-- content -->
				<div role="tabpanel" class="tab-pane" id="content_<?php echo $cat->id ?>">
					<div class="form-group">
						<label class="col-md-4">Số lượng dòng sản phẩm</label>
						<div class="col-md-8">
							<select class="form-control" name="rowShowPro_<?php echo $cat->id ?>" id="rowShowPro_<?php echo $cat->id ?>">
								<option value="">- Chọn -</option>
								<?php
								$actrow1 = '';
								$actrow2 = '';
								$actrow3 = '';
								switch($rows){
									case '1':
										$actrow1 = 'selected';
										break;
									case '2':
										$actrow2 = 'selected';
										break;
									case '3':
										$actrow3 = 'selected';
										break;
									default:
										break;
								}
								?>
								<option value="1" <?php echo $actrow1 ?>>1</option>
								<option value="2" <?php echo $actrow2 ?>>2</option>
								<option value="3" <?php echo $actrow3 ?>>3</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Số lượng cột sản phẩm</label>
						<div class="col-md-8">
							<select class="form-control" name="colShowPro_<?php echo $cat->id ?>" id="colShowPro_<?php echo $cat->id ?>">
								<?php
								$actcol3 = '';
								$actcol4 = '';
								switch($cols){
									case '3':
										$actcol3 = 'selected';
										break;
									case '4':
										$actcol4 = 'selected';
										break;
									default:
										break;
								}
								?>
								<option value="">- Chọn -</option>
								<option value="3" <?php echo $actcol3 ?>>3</option>
								<option value="4" <?php echo $actcol4 ?>>4</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="catSite" id="catSite_<?php echo $cat->id ?>" value="<?php echo OpenShopHelper::convertBase64Encode($cat->id) ?>" />
			<input type="hidden" name="moduleName" id="moduleName_<?php echo $cat->id ?>" value="<?php echo OpenShopHelper::convertBase64Encode($moduleName) ?>" />
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Thoát</button>
        <button type="button" class="btn btn-primary" onclick="saveConfigCatSite(<?php echo $cat->id ?>)"><i class="fa fa-save"></i> Lưu cấu hình</button>
      </div>
    </div>
  </div>
</div>

<script>
	
	//inital
	jQuery(window).bind('load',function(){
		editPX();
		loadBanner_<?php echo $cat->id ?>();
	});
	
	/*
	*	
	*/
    function editPX() {
        var c = 1;
        var wp = jQuery('.subCatName').width();
        for (var i = 0; i < 8; i++) {
            if (c === 1) { //kiểm tra cột 1
                c = 2;
                var w = jQuery('.tsc_' + i).width();
                if (100 - parseInt((w * 100) / wp) > 30) {
                    if ((w + jQuery('.tsc_' + (i+1)).width()) > (wp - 60)) {
                        var w_cur = wp - w - 60;
                        jQuery('.tsc_' + (i + 1)).css('width', w_cur + 'px');
                        jQuery('.tsc_' + (i + 1)).addClass('splitText');
                    }
                }
            } else if (c === 2) {
                c = 1;
            }
        }
    }
	
	function loadBanner_<?php echo $cat->id ?>(){
		var hw = jQuery('.catS_<?php echo $cat->id ?> .bgCStiePD').height();
		var hc = jQuery('.catS_<?php echo $cat->id ?> .colCatName').height();
		var h = hw - hc;
		if(h > 50){
			var w = jQuery('.catS_<?php echo $cat->id ?> .loadBanner').width();
			jQuery('.catS_<?php echo $cat->id ?> .loadBanner').css('height', h);
			jQuery('.catS_<?php echo $cat->id ?> .sizeBanner').html(w + ' x ' + h);
			jQuery('.catS_<?php echo $cat->id ?> .sizeBanner').css('padding-top', (parseInt(h/2) - 30) + 'px');
		}
	}
	
	
	function editCategorySite(i){
		jQuery('#valueCategory_' + i).addClass('inlineD');
		jQuery('#valueCategory_' + i).removeClass('noneD');
		
		jQuery('.titleCategory_' + i).addClass('noneD');
	}
	
	function saveCategorySite(i){
		jQuery('#valueCategory_' + i).removeClass('inlineD');
		jQuery('#valueCategory_' + i).addClass('noneD');
		
		jQuery('.titleCategory_' + i).removeClass('noneD');
		// jQuery('#valueCategory_' + i).val(jQuery('.titleCategory_' + i).text());
		jQuery.ajax({
			url: 'index.php?option=com_openshop&format=ajax&task=templates.saveCatSite',
			method: 'post',
			data: {
				i: i,
				v: jQuery('#valueCategory_' + i).val()
			},
			success:function(dt){
				jQuery('.titleCategory_' + i).text( jQuery('#valueCategory_' + i).val() );
			}
		});
	}
	
</script>