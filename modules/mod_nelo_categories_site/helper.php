<?php
class Modnelo_categories_siteHelper
{
    public static function getCategory($p){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.id,a.category_name,b.category_image')
                ->from($db->quoteName('#__openshop_categorydetails','a'))
                ->join('INNER', $db->quoteName('#__openshop_categories','b') . ' ON a.category_id = b.id')
                ->where('category_id = ' . $p->get('cat_id'));
        return $db->setQuery($query)->loadObject();
    }
    
    public static function getSubCategory($p){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.id,a.category_name,b.category_image')
                ->from($db->quoteName('#__openshop_categorydetails','a'))
                ->join('INNER', $db->quoteName('#__openshop_categories','b') . ' ON a.category_id = b.id')
                ->where('b.category_parent_id = ' . $p->get('cat_id'));
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getProductCat($p){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,a.product_image,b.product_name,a.product_price,a.product_price_r')
                ->from($db->quoteName('#__openshop_products', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'b') . ' ON a.id = b.product_id ')
                ->join('INNER', $db->quoteName('#__openshop_productcategories', 'c') . ' ON b.product_id = c.product_id ')
                ->where('c.category_id = ' . $p->get('cat_id'))
                ->order('created_date DESC');
        return $db->setQuery($query)->loadObjectList();
    }
}
