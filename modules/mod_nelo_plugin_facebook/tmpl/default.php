<?php
// No direct access
defined('_JEXEC') or die;
?>

<div class="pluginF" style="padding: 20px 0 0 0;">
    <iframe src="<?php echo $params->get('linkF') ?>"  
            width="100%" height="<?php echo $params->get('height') ?>" style="border:none;overflow:hidden"  
            scrolling="no" frameborder="0" allowTransparency="true">
    </iframe>
</div>