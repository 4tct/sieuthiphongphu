<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .footIj{
        padding: 20px 0 0 0;
    }
    .foot{
        padding: <?php echo $params->get('paddingFoot'). 'px' ?> 0;
        background: <?php echo $params->get('colorFoot') ?>;
        text-align: center;
        color: white;
    }
</style>

<div class="foot">
    <span>
        © <?php echo $params->get('contentFoot'); ?>
    </span>
</div>