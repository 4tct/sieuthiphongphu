<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .show_pro_<?php echo $module->id ?>{
        float: <?php echo $params->get('floatLR') ?>;
        padding-left: <?php echo $params->get('paddingL') . 'px' ?>;
        padding-right: <?php echo $params->get('paddingR') . 'px' ?>;
    }
    .show_pro_responsive_<?php echo $module->id ?>{
        clear: both;
        padding: 15px 0 0 0;
    }
    .padding_<?php echo $module->id ?>{
        padding: 5px;
    }
    .show_pro{
        margin-top: 20px;
    }
    .show_pro .content_show_pro_show .info_pro_t4{
        background: #f3f3f3;
    }
    .show_pro .content_show_pro_show .price_pro_t4{
        padding: 10px;
        font-size: 16px;
        color: #fd0000;
        font-weight: bold;
    }
    .show_pro .content_show_pro_show .act_pro ul{
        padding: 8px 0;
    }
    .show_pro .content_show_pro_show .act_pro ul li{
        display: inline;
    }
    .show_pro .content_show_pro_show .act_pro .cart_t4{
        padding: 8px 0;
        background: #57b4c5;
        color: white;
        cursor: pointer;
        text-align: center;
    }
    .show_pro .content_show_pro_show .act_pro .cart_t4:hover{
        background: #0a68b3;
    }
    .show_pro .content_show_pro_show .act_pro .buyer_t4{
        padding: 3px 8% 4px 8%;
        background: #e27b4c;
        color: white;
        cursor: pointer;
        border-left: 1px solid white;
    }
    .show_pro .content_show_pro_show .act_pro .buyer_t4:hover{
        background:#f90000;
    }
    .show_pro .content_show_pro_show .act_pro .view_t4{
        padding: 3px 0 4px 0;
        background: #e27b4c;
        color: white;
        cursor: pointer;
        text-align: center;
        border-left: 1px solid white;
    }
    .show_pro .content_show_pro_show .act_pro .view_t4:hover{
        background: #ffbd03;
    }
    .show_pro .content_show_pro_show .name_pro_t4 {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        padding: 10px 10px 0 10px;
        font-size: 15px;
    }
    .show_pro .content_show_pro_show .name_pro_t4 a{
        color: #383838;
        text-decoration: none;
    }
    .show_pro .content_show_pro_show .price_pro_t4 {
        padding: 10px;
        font-size: 16px;
        color: #fd0000;
        font-weight: bold;
    }
    .show_pro .content_show_pro_show .slider_3_t4_j{
        padding: 5px;
    }
    .show_pro .content_show_pro_show .hit_pro_3_t4{
        padding-top: 10px;
    }
    .show_pro .content_show_pro_show .hit_pro_3_t4 .vb{
        display: inline-block;
        font-size: 14px;
        padding: 0 10px;
        color: #2f5271;
    }
    /*--------------*/
    .show_pro .wap_pro_t3{
        display: inline-block;
        background: red;
    }

    .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: block;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
    .show_pro .content_show_pro_show .title_pro_main_t4 ul div{
        padding: 11px 0 10px 0;
        overflow: hidden;
    }
    .show_pro div.title_cat_<?php echo $module->id ?>, .show_pro .title_cat_<?php echo $module->id ?> li{
        background: <?php echo $params->get('colorWap') ?>;
    }
    .title_pro_main_t4 ul li{
        padding: 12px 0;
        display: inline;
        cursor: pointer;
        position: relative;
    }
    .title_pro_main_t4 ul li a{
        padding: 0px 12px;
    }
    .title_pro_main_t4 ul li a.border_right{
        border-right: 1px solid white;
    }
    .title_pro_main_t4 ul li.last_active_title_t4{
        border-radius: 0 0 10px 0;
    }
    .title_pro_main_t4 ul li.active_title_t4{
        padding: 12px;
        background: white;
        border-radius: 10px 10px 0 0;
        padding-left: 20px;
        padding-right: 20px;
        margin-left: -1px;
    }
    .show_pro .content_show_pro_show .title_pro_main_t4 ul li a{
        text-decoration: none;
        color: white;
    }
    .show_pro .title_pro_main_t4 ul li a span{
        position: relative;
        z-index: 20;
    }
    .title_pro_main_t4 ul li.active_title_t4 a span{
        color: #2f5271;
    }
    .show_pro .showProT4{
        padding: 8px;
    }

    .show_pro .active_title_t4:before{
        content: '';
        width: 20px;
        height: 20px;
        background: white;
        position: absolute;
        left: -20px;
        bottom: 0;
        z-index: 4;
    }
    .show_pro .active_title_t4:after{
        content: '';
        width: 20px;
        height: 20px;
        background: white;
        position: absolute;
        right: -20px;
        bottom: 0;
        z-index: 4;
    }
    .show_pro .wapContetPro_<?php echo $module->id ?>{
        position: relative;
        border: 3px solid <?php echo $params->get('colorWap') ?>;
        border-radius: 5px;
        background: white;
    }
    .show_pro .title_cat_<?php echo $module->id ?> .active_title_t4 a::after{
        content: '';
        width: 33px;
        height: 33px;
        background: <?php echo $params->get('colorWap') ?>;
        position: absolute;
        bottom: 0px;
        right: -33px;
        z-index: 10;
        border-radius: 20px;
    }
    .show_pro .title_cat_<?php echo $module->id ?> .active_title_t4 a::before{
        content: '';
        width: 33px;
        height: 33px;
        background: <?php echo $params->get('colorWap') ?>;
        position: absolute;
        bottom: 0px;
        left: -33px;
        z-index: 10;
        border-radius: 20px;
    }


    .show_pro .viewdetailcat{
        position: absolute;
        top: 0;
        right: 0;
    }

    .show_pro .loadingProCat{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: #868686;
        opacity: 0.5;
        z-index: 1000;
    }
    .show_pro .loadingProCat i{
        font-size: 140px;
        color: white;
        position: absolute;
        top: 35%;
        left: 40%;
    }
    .showbuyer, .showView{
        font-size: 12px;
    }
</style>

<?php
$clsS = 'show_pro_' . $module->id;
if (OpenShopHelper::isMobile()) {
    $clsS = 'show_pro_responsive_' . $module->id;
}
?>

<div class="col-sm-12 show_pro col-md-<?php echo $params->get('column') ?> <?php echo $clsS ?>">
    <div class="content_show_pro_show">
        <div class="wow fadeInRight" style="padding-right: 0;">
            <div class="wapContetPro_<?php echo $module->id ?>">
                <div class="title_pro_main_t4 title_pro_main_t4_<?php echo $module->id ?>">
                    <ul>
                        <div class="title_cat_<?php echo $module->id ?>">
                            <?php
                            $cls_cat = $module->id . '_100';
                            ?>
                            <li class="active_title_t4 addLoadingFa<?php echo $cls_cat ?> title_t4_<?php echo $module->id ?>" onclick="loadProductShowHomeSite('<?php echo $cls_cat ?>', '<?php echo OpenShopHelper::convertBase64Encode($category['id']) ?>', '<?php echo $module->id ?>')">
                                <a data-href="<?php echo $category['link_cat'] ?>" class="border_title_t4 border_right">
                                    <span class="catnameactive"><?php echo $category['name'] ?></span>
                                </a>
                            </li>
                            <?php
                            if (!OpenShopHelper::isMobile()) {
                                if (!empty($category['cat_sub'])) {

                                    foreach ($category['cat_sub'] as $k => $cs) {
                                        $next_act = '';
                                        $last_act = '';
                                        $cls_border = 'border_right';

                                        if ($k == (count($category['cat_sub']) - 1)) {
                                            $last_act = 'last_active_title_t4';
                                            $cls_border = '';
                                        }
                                        $link_cat = JRoute::_(OpenShopRoute::getCategoryRoute($cs->id));
                                        $cls_cat = $module->id . '_' . $k;
                                        ?>
                                        <li class="default_title_t4 addLoadingFa<?php echo $cls_cat ?> title_t4_<?php echo $module->id ?> <?php echo $last_act ?>" onclick="loadProductShowHomeSite('<?php echo $cls_cat ?>', '<?php echo OpenShopHelper::convertBase64Encode($cs->id) ?>', '<?php echo $module->id ?>')">
                                            <a data-href="<?php echo $link_cat ?>" class="border_title_t4 <?php echo $cls_border ?>">
                                                <span><?php echo $cs->category_name ?></span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <div class="viewdetailcat">
                                <li class="all_right_active_title_t4 load_all_right_active_title_t4_<?php echo $module->id ?>">
                                    <a href="<?php echo $category['link_cat'] ?>">
                                        Xem tất cả
                                    </a>
                                </li>
                            </div>
                        </div>
                    </ul>
                </div>
                <div class="showProT4 appendLoading_<?php echo $module->id ?>">
                    <div class="swiper-container slider-product-show-next-prev slide_product_<?php echo $module->id ?>">
                        <div class="swiper-wrapper silde-boder load_addLoadingFa<?php echo $module->id ?> widthBodyProduct_<?php echo $module->id ?>">
                            <?php
                            foreach ($category['products'] as $pm) {
                                $link = JRoute::_(OpenShopRoute::getProductRoute($pm->id, OpenShopHelper::getProductCategory($pm->id)));
                                ?>
                                <div class="swiper-slide padding_<?php echo $module->id ?>">
                                    <div class="marginImg imgProShow3">
                                        <!--<a href="<?php echo $link ?>">-->
                                        <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pm->id) ?>', '<?php echo $link ?>')" >
                                            <img data-src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pm->product_image ?>" class="img-responsive swiper-lazy" alt="<?php echo $pm->product_name; ?>" title="<?php echo $pm->product_name; ?>" data-placement="bottom" data-toggle="tooltip"/>
                                            <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>
                                        </a>

                                        <div class="ProSKU">
                                            <?php
                                            echo $pm->product_sku;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="info_pro_t4">
                                        <div class="name_pro_t4">
                                            <!--<a href="<?php echo $link ?>">-->
                                            <a onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pm->id) ?>', '<?php echo $link ?>')" >
                                                <h5 title="<?php echo $pm->product_name; ?>" data-placement="top" data-toggle="tooltip">
                                                    <?php
                                                    echo $pm->product_name;
                                                    ?>
                                                </h5>
                                            </a>
                                        </div>
                                        <div class="info_price_pro_t4">
                                            <?php
                                            if ($pm->product_price_r > 0) {
                                                ?>
                                                <div class="not_price_pro_t4">
                                                    <?php
                                                    echo number_format((int) $pm->product_price_r, 0, ',', '.');
                                                    ?>
                                                    <sup>đ</sup>
                                                    <sub><?php echo OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) ?></sub>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="price_pro_t4">
                                                <?php
                                                echo number_format((int) $pm->product_price, 0, ',', '.');
                                                ?>
                                                <sup>đ</sup>
                                                <sub><?php echo OpenShopHelper::getDonViProduct( isset($pm->donvi) ? $pm->donvi : 1 ) ?></sub>
                                            </div>
                                        </div>
                                        <div class="act_pro">
                                            <div class="col-md-12" style="padding:0">
                                                <div class="col-sm-12 col-md-6 cart_t4" onclick="getTestModalDetail('<?php echo OpenShopHelper::convertBase64Encode($pm->id) ?>', '<?php echo $link ?>')" >
                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                    <span style="font-size: 14px;font-weight: bold;">Đặt mua</span>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3 buyer_t4 borderL0" data-placement="top" data-toggle="tooltip" title="Lượt mua">
                                                    <div class="showbuyer">
                                                        <?php
                                                        echo $pm->buyer_virtual;
                                                        ?>
                                                    </div>
                                                    <div class="showView">
                                                        <i class="fa fa-users" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3 view_t4" data-placement="top" data-toggle="tooltip" title="Lượt xem">
                                                    <div class="showbuyer">
                                                        <?php
                                                        echo $pm->hits;
                                                        ?>
                                                    </div>
                                                    <div class="showView">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                    if ($pm->product_price_r > 0) {
                                        $percent = 100 - (int) (((int) $pm->product_price * 100) / $pm->product_price_r)
                                        ?>
                                        <div class="percentSale rightPercent"> 
                                            <?php echo $percent ?>%
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <!-- Navigation -->
                        <div class="jd-nav-button-next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div class="jd-nav-button-prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 padding0">
    <?php
    if ($params->get('showbanner')) {
        switch ($params->get('frameBanner')) {
            case 'f1':
                require 'frame_banner_1.php';
                break;
            case 'f2':
                require 'frame_banner_2.php';
                break;
            case 'f3':
                require 'frame_banner_3.php';
                break;
            default :
                break;
        }
    }
    ?>
</div>

<script>
    var swiper = new Swiper('.slide_product_<?php echo $module->id ?>', {
        slidesPerView: <?php echo $params->get('numPro') ?>,
        spaceBetween: 15,
        preloadImages: false,
        lazyLoading: true,
        nextButton: '.jd-nav-button-next',
        prevButton: '.jd-nav-button-prev',
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    jQuery('.slider-product-show-next-prev').mouseover(function () {
        $('.jd-nav-button-next').css('opacity', '1');
        $('.jd-nav-button-prev').css('opacity', '1');
    });
    jQuery('.slider-product-show-next-prev').mouseout(function () {
        $('.jd-nav-button-next').css('opacity', '0');
        $('.jd-nav-button-prev').css('opacity', '0');
    });
</script>