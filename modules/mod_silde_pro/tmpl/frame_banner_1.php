<div class="banner_custom">
    <?php
    $bannerImg = Modsilde_proHelper::getBannerImg($params->get('chooseBannerImg'));
    if (count($bannerImg)) {
        ?>
        <table border="0" cellpadding="1" cellspacing="1" style="width:100%;background: white;margin-top: 20px;">
            <tbody>
                <tr>
                    <td>
                        <p>
                            <img class="img-responsive lazy" alt="Quảng cáo" data-original="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $bannerImg[0]->media_image ?>" style="width: 360px;" /></p>
                        <br />
                        <p>
                            <img class="img-responsive lazy" alt="Quảng cáo" data-original="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $bannerImg[1]->media_image ?>" style="width: 360px;" /></p>
                    </td>
                    <td style="text-align:center;">
                        <img class="img-responsive lazy" alt="Quảng cáo" data-original="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $bannerImg[2]->media_image ?>" style="width: 440px;" /></td>
                    <td style="text-align:right;">
                        <img class="img-responsive lazy" alt="Quảng cáo" data-original="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $bannerImg[3]->media_image ?>" style="width: 358px;;" /></td>
                </tr>
            </tbody>
        </table>
        <?php
    }
    ?>

</div>