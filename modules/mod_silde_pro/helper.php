<?php

class Modsilde_proHelper {

    public static function getNameCategory($params, $limit = 4) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,a.category_name,b.category_image')
                ->from($db->quoteName('#__openshop_categorydetails', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_categories', 'b') . ' ON b.id = a.category_id')
                ->where('a.category_id = ' . $params->get('chooseCategory'));
        $name = $db->setQuery($query)->loadObject();

        $cat_sub = '';
        $catSub = $params->get('chooseCategorySub');
        if (!empty($catSub)) {
            $query->clear();
            $query->select('b.id,a.category_name')
                    ->from($db->quoteName('#__openshop_categorydetails', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_categories', 'b') . ' ON b.id = a.category_id')
                    ->where('b.id IN (' . implode(',', $params->get('chooseCategorySub')) . ')');
            $cat_sub = $db->setQuery($query)->loadObjectList();
        }
        //load product
        $limit = 10;
        $query->clear();
        $query->select('b.id,b.product_image,b.product_sku,b.product_price_r,b.product_price,c.product_name,b.product_price_r,b.hits,b.buyer_virtual')
                ->from($db->quoteName('#__openshop_productcategories', 'a'))
                ->join('INNER', $db->quoteName('#__openshop_products', 'b') . ' ON b.id = a.product_id')
                ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . ' ON b.id = c.product_id')
                ->where('a.category_id = ' . $params->get('chooseCategory'))
                ->where('published = 1')
                ->where('delete_status = 0')
                ->order('b.id DESC, b.product_hot DESC');
        $products = $db->setQuery($query, 0, $limit)->loadObjectList();

        if (!count($products)) {
            $query->clear();
            $query->select('DISTINCT b.*,c.product_name')
                    ->from($db->quoteName('#__openshop_productcategories', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_products', 'b') . ' ON b.id = a.product_id')
                    ->join('INNER', $db->quoteName('#__openshop_productdetails', 'c') . ' ON b.id = c.product_id')
                    ->where('a.category_id IN (' . implode(',', OpenShopHelper::getAllChildCategories($params->get('chooseCategory'))) . ')')
                    ->where('published = 1')
                    ->where('delete_status = 0')
                    ->order('b.id DESC, b.product_hot DESC');
            $products = $db->setQuery($query, 0, $limit)->loadObjectList();
        }

        $res = array();
        $res['link_cat'] = JRoute::_(OpenShopRoute::getCategoryRoute($name->id));
        $res['name'] = $name->category_name;
        $res['id'] = $name->id;
        $res['cat_sub'] = $cat_sub;
        $res['products'] = $products;
        $res['img_cat'] = $name->category_image;

        return $res;
    }
    
     public static function getBannerImg($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        try {
            $query->select('a.id,b.media_image')
                    ->from($db->quoteName('#__openshop_medias', 'a'))
                    ->join('INNER', $db->quoteName('#__openshop_mediadetails', 'b') . ' ON a.id = b.media_id')
                    ->where('a.published = 1')
                    ->where('a.id = ' . $id);
            return $db->setQuery($query)->loadObjectList();
        } catch (Exception $ex) {
            return '';
        }
    }

}
