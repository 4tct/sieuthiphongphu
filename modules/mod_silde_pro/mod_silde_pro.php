<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$category = Modsilde_proHelper::getNameCategory($params);
require JModuleHelper::getLayoutPath('mod_silde_pro');