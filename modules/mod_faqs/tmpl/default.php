<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .FAQs{
        width: 100%;
        background: <?php echo $params->get('colorFAQs') ?>;
        text-align: center;
        cursor: pointer;
        padding: 70px 0;
    }
    .FAQs a{
        text-decoration: none;
        color: white;
    }
    .FAQs a h1{
        font-size: 60px;
        font-weight: bold;
    }
</style>

<div class="FAQs" onclick="location.href = '<?php echo JRoute::_($link) ?>'">
    <a href="<?php echo JRoute::_($link) ?>">
        <h1>Câu hỏi thường gặp - F & Q</h1>
    </a>
</div>