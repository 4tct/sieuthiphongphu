<?php
class ModfaqsHelper
{
    public static function getfaqs($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,link')
                ->from($db->quoteName('#__menu'))
                ->where('id = ' . $params->get('link_FAQs'));
        $row = $db->setQuery($query)->loadObject();
        
        return $row->link . '&Itemid=' . $row->id;
    }
}
