<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

// Include the helper functions only once
require_once JPATH_ROOT.'/components/com_openshop/helpers/helper.php';
require_once dirname(__FILE__).'/helper.php';
require_once JPATH_ROOT . '/administrator/components/com_openshop/libraries/autoload.php';
$categories = modOpenShopCategoryHelper::getCategories();
$document = JFactory::getDocument();
$template = JFactory::getApplication()->getTemplate();

if (is_file(JPATH_SITE .  '/templates/'. $template .  '/css/'  . $module->module . '.css')) {
	$document->addStyleSheet(JURI::base().'templates/' . $template . '/css/' . $module->module . '.css');
} else {
	$document->addStyleSheet(JURI::base().'modules/' . $module->module . '/css/style.css');
}
$showChildren = $params->get('show_children');
$showNumberProducts = $params->get('show_number_products') && OpenShopHelper::getConfigValue('product_count');
if (JRequest::getVar('view') == 'category')
{
	$categoryId = JRequest::getVar('id');
}
else 
{
	$categoryId = 0;
}
if ($categoryId == 0)
{
	$parentCategoryId = 0;
	$childCategoryId = 0;
}
else
{
	$parentCategoryId = modOpenShopCategoryHelper::getParentCategoryId($categoryId);
	if ($parentCategoryId == $categoryId)
	{
		$childCategoryId = 0;
	}
	else 
	{
		$childCategoryId = $categoryId;
	}
}
require JModuleHelper::getLayoutPath('mod_openshop_category', $params->get('layout', 'default'));
