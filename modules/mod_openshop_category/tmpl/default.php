<?php
/**
 * @package	OpenShop for Joomla!
 * @version	2.0.1.6
 * @author	LMNX
 * @copyright	(C) 2016 LMNX
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="openshop-category<?php echo $params->get('moduleclass_sfx') ?>">
    <ul>
        <?php
        foreach ($categories as $category) {
            if ($showNumberProducts) {
                $numberProducts = ' (' . OpenShopHelper::getNumCategoryProducts($category->id, true) . ')';
            } else {
                $numberProducts = '';
            }
            ?>
            <li>
            <?php
            $active = $category->id == $parentCategoryId ? ' class="active"' : '';
            ?>
                <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($category->id)); ?>"<?php echo $active; ?>><?php echo $category->category_name . $numberProducts; ?></a>
                <?php
                if ($showChildren && $category->childCategories) {
                    ?>
                    <ul>
                    <?php
                    foreach ($category->childCategories as $childCategory) {
                        if ($showNumberProducts) {
                            $numberProducts = ' (' . OpenShopHelper::getNumCategoryProducts($childCategory->id, true) . ')';
                        } else {
                            $numberProducts = '';
                        }
                        ?>
                            <li>
                            <?php
                            $active = $childCategory->id == $childCategoryId ? 'class="active"' : '';
                            ?>
                                <a href="<?php echo JRoute::_(OpenShopRoute::getCategoryRoute($childCategory->id)); ?>" <?php echo $active; ?>> - <?php echo $childCategory->category_name . $numberProducts; ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                            <?php
                        }
                        ?>
            </li>
                    <?php
                }
                ?>
    </ul>
</div>