<?php
class Modnelo_bg_logoHelper
{
    public static function getBgLogo($params){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.media_image')
                ->from($db->quoteName('#__openshop_medias','a'))
                ->join('INNER', $db->quoteName('#__openshop_mediadetails','b') . ' ON a.id = b.media_id')
                ->where('a.id = ' . $params->get('backgroud'));
        return $db->setQuery($query)->loadObject();
    }
}
