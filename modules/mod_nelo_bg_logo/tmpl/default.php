<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .bg-logo{
        width: <?php echo $params->get('contentBG') . $params->get('selectP'); ?>;
        height: <?php echo $params->get('heightBG') . 'px'; ?>;
        margin: 0 auto;
        background: #b5b5b5;
    }
    .logoIj{
        float: left;
    }
</style>

<div class="bg-logo">
    <?php
    if ($params->get('showBG')) {
        ?>
        <img src="<?php echo JUri::root() . 'images/com_openshop/medias/' . $bg->media_image ?>" alt="" style="width: 100%;"/>
        <?php
    }
    ?>

    <div class="logoIj">
        <?php
        $modules = JModuleHelper::getModules('nelo-logo');
        foreach ($modules as $module) {
            echo JModuleHelper::renderModule($module);
        }
        ?>
    </div>

    <?php
    $modules = JModuleHelper::getModules('nelo-menu-logo');
    foreach ($modules as $module) {
        echo '<div class="menuInLogo">';
        echo JModuleHelper::renderModule($module);
        echo '</div>';
    }
    ?>
    <?php
    $modulesS = JModuleHelper::getModules('nelo-search');
    foreach ($modulesS as $module) {
        echo '<div class="search-box">';
        echo JModuleHelper::renderModule($module);
        echo '</div>';
    }
    ?>
    <?php
    $modulesL = JModuleHelper::getModules('nelo-loginout');
    foreach ($modulesL as $module) {
        echo '<div class="nelo-loginout">';
        echo JModuleHelper::renderModule($module);
        echo '</div>';
    }
    ?>
    <?php
    $modulesC = JModuleHelper::getModules('nelo-cart');
    foreach ($modulesC as $module) {
        echo '<div class="nelo-cart">';
        echo JModuleHelper::renderModule($module);
        echo '</div>';
    }
    ?>


    <?php
    $modules = JModuleHelper::getModules('nelo-process-top');
    foreach ($modules as $module) {
        echo '<div class="nelo_process_top">';
        echo JModuleHelper::renderModule($module);
        echo '</div>';
    }
    ?>

</div>
