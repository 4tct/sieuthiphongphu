<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$bg = Modnelo_bg_logoHelper::getBgLogo($params);
require JModuleHelper::getLayoutPath('mod_nelo_bg_logo');