<?php
class Modnelo_feature_highHelper
{
    public static function getLinkFeatureHigh($idLink)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,link')
                ->from($db->quoteName('#__menu'))
                ->where('id = ' . $idLink);
        $row = $db->setQuery($query)->loadObject();
        
        return $row->link . '&Itemid=' . $row->id;
    }
}
