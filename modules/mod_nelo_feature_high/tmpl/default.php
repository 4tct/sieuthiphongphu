<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .bg-opacity{
        text-align: center;
        padding: 30px;
        background: #696969;
        margin: 0 -15px;
        opacity: 0.3;
        cursor: pointer;
    }
    .title-high{
        position: absolute;
        bottom: 16px;
        text-align: center;
        width: 88%;
        color: white;
        cursor: pointer;
        font-size: 18px;
    }
    .highF a:hover .bg-opacity{
        opacity: 0.7;
    }
    .img-high{
        height: 280px;
        padding: 30px 5px 0 5px;
    }
    @media(max-width: 800px){
        .img-high{
            height: 200px;
        }
    }
</style>


<div class="col-md-10 col-md-offset-1">
    <?php
    $t = substr($params->get('selectColumn'), 1, 1);

    for ($i = 1; $i <= $t; $i++) {
        ?>
        <div class="col-sm-3 col-md-3 highF" style="background: <?php echo $params->get('colorBG' . $i) ?>;">
            <div class="img-high">
                <img class="lazy img-responsive" src="<?php echo $params->get('image' . $i) ?>" data-original="<?php echo $params->get('image' . $i) ?>" title="<?php echo $params->get('title' . $i) ?>" alt="<?php echo $params->get('title' . $i) ?>"/>
            </div>
            <a href="<?php echo JRoute::_(Modnelo_feature_highHelper::getLinkFeatureHigh($params->get('link' . $i))) ?>">
                <div class="bg-opacity"></div>
                <div class="title-high"><?php echo $params->get('title' . $i) ?></div>
            </a>
        </div>
        <?php
    }
    ?>
</div>