<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$productSpecials = Modnelo_special_home_siteHelper::getProductSpecial($params);
require JModuleHelper::getLayoutPath('mod_nelo_special_home_site');