<?php
class Modnelo_special_home_siteHelper
{
 
    public static function getProductSpecial($params){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('a.*,b.product_name')
                ->from($db->quoteName('#__openshop_products','a'))
                ->join('INNER', $db->quoteName('#__openshop_productdetails','b') . ' ON a.id = b.product_id')
                ->order('a.product_hot DESC, a.product_new DESC');
        return $db->setQuery($query,0,4)->loadObjectList();
    }
}
