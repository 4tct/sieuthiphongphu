<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .special_home_site{
        margin-top: 30px;
    }
    .show_special_product .title_special{
        background: #e40000;
        color: white;
        padding: 10px;
        font-weight: bold;
    }
    .show_special_product .content_special{
        background: white;
    }
    .show_special_product .content_special .product_special{
        padding: 20px;
        border: 1px solid white;
        border-bottom: 1px solid #ff7a7a;
    }
    .show_special_product .content_special .product_special:hover{
        border: 1px solid red;
    }
    .show_special_product .content_special .product_special:hover .name_product a{
        color: red;
    }
    .show_special_product .content_special .product_special img{
        max-height: 290px;
        width: 100%;
        cursor: pointer;
    }
    .show_special_product .content_special .product_special .name_product{
        padding: 10px 0 0 0;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        cursor: pointer;
    }
    .show_special_product .content_special .product_special hr{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .show_special_product .content_special .product_special .code_pro{
        text-align: center;
        padding: 0 0 10px 0;
    }
    .show_special_product .price_product_special{
        position: relative;
    }
    .show_special_product .price_product_special .price_buy{
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        color: red;
    }
    .show_special_product .price_product_special .price_not_buy{
        display: inline-block;
        position: absolute;
        right: 0;
        top: 0;
        text-decoration: line-through;
        color: #afafaf;
        font-size: 18px;
    }
    .show_special_product .price_product_special .price_only{
        text-align: center;
        display: block;
    }
    .show_special_product .product_special a{
        text-decoration: none;
        color: black;
    }
</style>

<div class="show_special_product">
    <div class="title_special">SẢN PHẨM NỔI BẬT</div>
    <div class="content_special">
        <?php
        foreach ($productSpecials as $pro) {
            $link = $link = JRoute::_(OpenShopRoute::getProductRoute($pro->id, OpenShopHelper::getProductCategory($pro->id)));;
            ?>
            <div class="product_special">
                <img onclick="location.href='<?php echo $link ?>'" class="lazy" src="<?php echo OPENSHOP_PATH_IMG_PRODUCT_HTTP . $pro->product_image ?>" class="img-responsive" title="<?php echo $pro->product_name ?>" alt="<?php echo $pro->product_name ?>"/>
                <div class="name_product" title="<?php echo $pro->product_name ?>"> 
                    <a href="<?php echo $link ?>">
                        <?php echo $pro->product_name ?>
                    </a>
                </div>
                <hr />
                <div class="code_pro">Mã SP: <strong> <?php echo $pro->product_sku ?></strong></div>
                <div class="price_product_special">
                    <?php
                    if ($pro->product_price_r != 0) {
                        ?>
                        <div class="price_buy">
                            <?php
                            echo number_format($pro->product_price, 0, ',', '.');
                            ?>
                            <sup>đ</sup>
                        </div>
                        <div class="price_not_buy">
                            <?php
                            echo number_format($pro->product_price_input, 0, ',', '.');
                            ?>
                            <sup>đ</sup>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="price_buy price_only">
                            <?php
                            echo number_format($pro->product_price, 0, ',', '.');
                            ?>
                            <sup>đ</sup>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
            <?php
        }
        ?>

    </div>
</div>