<?php
class Modnelo_info_aboutHelper
{
    public static function getnelo_info_about($params)
    {
        return 'Hello, World!';
    }
    
    public static function getLogos(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('b.*')
                ->from($db->quoteName('#__openshop_medias','a'))
                ->join('INNER', $db->quoteName('#__openshop_mediadetails','b') . 'ON a.id = b.media_id')
                ->where('a.id = 19');
        return $db->setQuery($query)->loadObjectList();
    }
    
    public static function getAllCategory($parent,$limit){
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);
        $query->select('a.id,b.category_name')
                ->from($db->quoteName('#__openshop_categories','a'))
                ->join('INNER', $db->quoteName('#__openshop_categorydetails','b') . 'ON a.id = b.category_id')
                ->where('a.published = 1')
                ->where('category_parent_id = ' . $parent);
        return $db->setQuery($query,0,$limit)->loadObjectList();
    }
    
}
