<?php
defined('_JEXEC') or die;
?>

<style>
    .info-about{
        width: <?php echo $params->get('contentIA') . $params->get('selectIA') ?>;
        margin: 0 auto;
        padding: 30px 0 0 0;
    }
    .info-about .titleIA{
        text-transform: uppercase;
        font-weight: bold;
    }
    .info-about .iacontent{
        padding: 5px 0;
    }
    .info-about .iacontent i{
        color: #6CC550;
        padding-right: 5px;
    }
    .info-about .iacontent a{
        text-decoration: none;
        color: #5D5D5D;
        cursor: pointer;
    }
    .info-about .iacontent a:hover{
        color: #6CC550;
    }
    .viewmap{
        text-decoration: underline;
        cursor: pointer;
    }
</style>

<div class="info-about">
    <div class="col-sm-6 col-md-3">
        <h3 class="titleIA">Liên hệ với chúng tôi</h3>
        <?php
        //contact
        $contacts = explode(';', OpenShopHelper::getConfigValue('contact_address'));
        foreach ($contacts as $contact) {
            $addr = explode('@@', $contact);
            ?>
            <div class="iacontent">
                <i class="fa fa-home"></i> <?php echo $addr[0] ?>
                <i class="viewmap" onclick="showViewMap('<?php echo $addr[1] ?>')"> Xem bản đồ</i>
            </div>
            <?php
        }

        //phone
        $phones = explode(',', OpenShopHelper::getConfigValue('contact_phone'));
        foreach ($phones as $phone) {
            ?>
            <div class="iacontent">
                <i class="fa fa-phone"></i> <?php echo $phone ?>
            </div>
            <?php
        }

        //mail
        $mails = explode(',', OpenShopHelper::getConfigValue('contact_email'));
        foreach ($mails as $mail) {
            ?>
            <div class="iacontent">
                <i class="fa fa-envelope-o"></i> <?php echo $mail ?>
            </div>
            <?php
        }
        //work hour
        $hours = explode(',', OpenShopHelper::getConfigValue('start_end_work'));
        ?>
        <div class="iacontent">
            <i class="fa fa-clock-o"></i> Giờ làm việc: Sáng: <?php echo $hours[0] ?> - Chiều: <?php echo $hours[1] ?> - Tối: <?php echo $hours[2] ?>
        </div>
        <?php
        ?>
    </div>
    <div class="col-sm-6 col-md-3">
        <h3 class="titleIA">Chính sách</h3>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Quy đinh chung</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Quy đinh mua hàng</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Quy đinh đặt hàng</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a href="/huong-dan-doi-tra.html">Quy đinh đổi trả</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Giải quyết khiếu nại</a>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <h3 class="titleIA">Tư vấn & Chăm sóc</h3>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Góp ý - Phản hồi</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Hướng dẫn mua hàng</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a href="/huong-dan-thanh-toan.html">Phương thức thanh toán</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Phí giao hàng</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Liên hệ mua hàng giá sỉ</a>
        </div>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a href="/do-size-san-pham.html">Đo size sản phẩm</a>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <h3 class="titleIA">Ứng dụng</h3>
        <div class="iacontent">
            <i class="fa fa-key"></i> 
            <a>Đang cập nhật...</a>
        </div>
        <div class="iacontent">
            <a href="http://www.thoitrangtichtac.com/" target="_blank">
                <img src="<?php echo OPENSHOP_PATH_IMG_OTHER_HTTP . 'logo4t.png' ?>" alt="Thời trang tích tắc" title="Thời trang tích tắc"/>
            </a>
        </div>
    </div>
</div>


<div class="modal fade modalViewMap" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Bản đồ</h4>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="450" frameborder="0" style="border:0" 
                        src="" allowfullscreen id="googlemaplink">
                </iframe>
            </div>
        </div>
    </div>
</div>

