<div class="paddingT20">
    <div class="col-md-12 infoFootT3">
        <div class="width-body">
            <div class="col-md-4 paddingL0">
                <div class="info_company">
                    <div class="img_logo">
                        <?php
                        $session = JFactory::getSession();
                        $logos = $session->get('logo_img');
                        if (!isset($logos) && empty($logos)) {
                            $logos = $logoDBs;
                        }

                        $modules = JModuleHelper::getModules('nelo-logo');
                        if (count($modules)) {
                            foreach ($logos as $logo) {
                                ?>
                                <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $logo->media_image ?>" alt="Logo" title="Logo" width="300px"/>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="address_company">
                        <p class="name_company">
                            <?php echo OpenShopHelper::getConfigValue('store_name') ?>
                        </p>
                        <p class="info_company">
                            Địa chỉ: <?php echo OpenShopHelper::getConfigValue('address') ?>
                        </p>
                        <p class="info_company">
                            Mã số thuế: <?php echo OpenShopHelper::getConfigValue('tax_code') ?>
                            do <?php echo OpenShopHelper::getConfigValue('tax_code_address') ?>
                            ngày <?php echo OpenShopHelper::getConfigValue('tax_code_date') ?>
                        </p>
                        <p class="info_company">
                            Số điện thoại: <?php echo OpenShopHelper::getConfigValue('telephone') ?>
                        </p>
                        <p class="info_company">
                            Email: <?php echo OpenShopHelper::getConfigValue('email') ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="col-md-3">
                    <div class="title_col_foot">
                        <span>Tài khoản</span>
                    </div>
                    <div class="content_col_foot">
                        <ul>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="#">
                                    <h4>Thông tin cá nhân</h4>
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="#">
                                    <h4>Giỏ hàng</h4>
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="#">
                                    <h4>Lịch sử mua hàng</h4>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="title_col_foot">
                        <span>Thông tin</span>
                    </div>
                    <div class="content_col_foot">
                        <ul>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="#">
                                    <h4>Sản phẩm mới</h4>
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="#">
                                    <h4>Sản phẩm giảm giá</h4>
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="#">
                                    <h4>Sản phẩm HOT</h4>
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="#">
                                    <h4>Bản đồ</h4>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="title_col_foot">
                        <span>Danh mục</span>
                        <div class="content_col_foot">
                            <ul>
                                <li>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <a href="#">
                                        <h4>Thời trang nam</h4>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <a href="#">
                                        <h4>Thời trang nữ</h4>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <a href="#">
                                        <h4>Thời trang cặp đôi</h4>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <a href="#">
                                        <h4>Giày dép</h4>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <a href="#">
                                        <h4>Mẹ và Bé</h4>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <a href="#">
                                        <h4>Túi xích</h4>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 paddingR0">
                    <div class="title_col_foot">
                        <span>Liên kết</span>
                        <div class="content_col_foot">
                            <ul>
                                <li>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <a href="#">Đang cập nhật ...</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clr"></div>

        <div class="width-body">
            <div class="allcat_inline">
                <ul>
                    <?php
                    foreach ($cats as $c) {
                        ?>
                        <li>
                            <a href="#">
                                <h4 class="display_inline_block">
                                    <?php
                                    echo $c->category_name;
                                    ?>
                                    :
                                </h4>
                            </a>

                            <ul class="display_inline_block catSub_inline">
                                <?php
                                $subcat = Modnelo_info_aboutHelper::getAllCategory($c->id, '6');
                                if (count($subcat)) {
                                    foreach ($subcat as $sc) {
                                        ?>
                                        <li class="display_inline_block">
                                            <a href="#">
                                                <h4 class="display_inline_block boderC">
                                                    <?php
                                                    echo $sc->category_name;
                                                    ?>
                                                </h4>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>

                                <li class="display_inline_block">
                                    <a href="#">
                                        <h4 class="display_inline_block">
                                            Xem tất cả
                                        </h4>
                                    </a>
                                </li>
                            </ul>

                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>

        <div class="clr"></div>

        <div class="width-body">
            <div class="copyright_foot">
                Copyright <i class="fa fa-copyright" aria-hidden="true"></i>. All Rights Reserved
            </div>
        </div>
    </div>
</div>