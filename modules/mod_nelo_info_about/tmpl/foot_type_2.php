<?php
defined('_JEXEC') or die;
?>

<style>
    .InfoAbout{
        padding: 30px 0 0 0;
    }
    .InfoAbout .bg_foot{
        width: 100%;
        background: red;
        height: 3px;
    }
    .InfoAbout .content_foot{
        background: #ccc2c2;
    }
    .content_foot .width_limit{
        width: 90%;
        padding-left: 5%;
        margin-top: 30px;
    }
    .InfoAbout .name_company{
        padding: 20px 0 0 0;
        font-weight: bold;
        font-size: 16px;
        text-transform: uppercase;
    }
    .InfoAbout ul.info_intro{
        list-style: none;
        padding: 20px 0 0 0;
    }
    .InfoAbout ul.info_intro li{
        padding: 3px 0;
    }
    .InfoAbout ul.info_intro li a{
        text-decoration: none;
        color: black;
    }
    .InfoAbout ul.info_intro li a:hover{
        color: red;
    }
    .InfoAbout .info_company{
        padding: 3px 0 0 0;
    }
</style>

<div class="bg_foot"></div>
<div class="content_foot">
    <div class="width_limit">
        <div class="col-md-4">
            <?php
            $session = JFactory::getSession();
            $logos = $session->get('logo_img');
            if(!isset($logos) && empty($logos)){
                $logos = $logoDBs;
            }
            foreach ($logos as $logo) {
                ?>
                <img class="img-responsive" src="<?php echo OPENSHOP_PATH_IMG_MEDIA_HTTP . $logo->media_image ?>" alt="Logo" title="Logo" width="150px"/>
                <?php
            }
            ?>
            <p class="name_company">
                <?php echo OpenShopHelper::getConfigValue('store_name') ?>
            </p>
            <p class="info_company">
                Địa chỉ: <?php echo OpenShopHelper::getConfigValue('address') ?>
            </p>
            <p class="info_company">
                Mã số thuế: <?php echo OpenShopHelper::getConfigValue('tax_code') ?>
                do <?php echo OpenShopHelper::getConfigValue('tax_code_address') ?>
                ngày <?php echo OpenShopHelper::getConfigValue('tax_code_date') ?>
            </p>
            <p class="info_company">
                Số điện thoại: <?php echo OpenShopHelper::getConfigValue('telephone') ?>
            </p>
            <p class="info_company">
                Email: <?php echo OpenShopHelper::getConfigValue('email') ?>
            </p>
        </div>
        <div class="col-md-8">
            <div class="col-md-4">
                <b class="title_company">GIỚI THIỆU</b>
                <ul class="info_intro">
                    <li>
                        <a href="/gioi-thieu-cong-ty.html">Về chúng tôi</a>
                    </li>
                    <li>
                        <a href="#">Bản đồ chỉ dẫn</a>
                    </li>
                    <li>
                        <a href="/lien-he.html">Liên hệ</a>
                    </li>
                    <li>
                        <a href="#">Thông báo</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <b class="title_company">TRỢ GIÚP</b>
                <ul class="info_intro">
                    <li>
                        <a href="#">Góp ý - phản hồi</a>
                    </li>
                    <li>
                        <a href="#">Hướng dẫn mua hàng</a>
                    </li>
                    <li>
                        <a href="/huong-dan-doi-tra.html">Hướng dẫn đổi trả</a>
                    </li>
                    <li>
                        <a href="/huong-dan-thanh-toan.html">Phương thức thanh toán</a>
                    </li>
                    <li>
                        <a href="/thong-tin-khach-hang.html">Thông tin khách hàng</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <b class="title_company">HỖ TRỢ KHÁCH HÀNG</b>
                <ul class="info_intro">
                    <li>
                        <a href="#">Đang cập nhật...</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>