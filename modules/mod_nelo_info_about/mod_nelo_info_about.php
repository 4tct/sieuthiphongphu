<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$hello = Modnelo_info_aboutHelper::getnelo_info_about($params);
$logoDBs = Modnelo_info_aboutHelper::getLogos();
$cats = Modnelo_info_aboutHelper::getAllCategory('0','10');
require JModuleHelper::getLayoutPath('mod_nelo_info_about');