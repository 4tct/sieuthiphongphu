<?php
// No direct access
defined('_JEXEC') or die;
?>
<div class="paddingT20">
    <div class="swiper-container slide_brands" style="background: white;">
        <div class="swiper-wrapper">
            <?php
            foreach ($brands as $b) {
                $link = ''; //JRoute::_(OpenShopRoute::getProductRoute($b->id, OpenShopHelper::getProductCategory($b->id)));
                ?>
                <div class="swiper-slide">
                    <img data-src="<?php echo JUri::base() .'media/com_openshop/brands/' . $b->brand_image ?>" class="img-responsive swiper-lazy" alt="<?php echo $b->brand_name; ?>" title="<?php echo $b->brand_name; ?>"/>
                    <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>
                </div>
                <?php
            }
            ?>
        </div>

        <!-- Navigation -->
        <div class="jd-nav-button-next">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </div>
        <div class="jd-nav-button-prev">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
        </div>
    </div>
</div>

<script>
    var silder = new Swiper('.slide_brands', {
        slidesPerView: 6,
        spaceBetween: 15,
        height: 3000,
        preloadImages: false,
        lazyLoading: true,
        nextButton: '.jd-nav-button-next',
        prevButton: '.jd-nav-button-prev',
        autoplay: 5000,
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });
    
    jQuery('.slide_brands').mouseover(function () {
        $('.jd-nav-button-next').css('opacity', '1');
        $('.jd-nav-button-prev').css('opacity', '1');
    });
    jQuery('.slide_brands').mouseout(function () {
        $('.jd-nav-button-next').css('opacity', '0');
        $('.jd-nav-button-prev').css('opacity', '0');
    });
</script>