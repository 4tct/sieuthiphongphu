<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$brands = Modsilde_one_rowHelper::getRows('brand');
require JModuleHelper::getLayoutPath('mod_silde_one_row');