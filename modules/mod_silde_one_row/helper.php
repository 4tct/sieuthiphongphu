<?php

class Modsilde_one_rowHelper {

    public static function getRows($cond) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(TRUE);

        switch ($cond) {
            case 'brand':
                $query->select('*')
                        ->from($db->quoteName('#__openshop_brands', 'a'))
                        ->join('INNER', $db->quoteName('#__openshop_branddetails', 'b') . 'ON a.id = b.brand_id');
                return $db->setQuery($query)->loadObjectList();
                break;
            default :
                break;
        }
    }
}
