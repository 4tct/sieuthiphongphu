<?php
// No direct access
defined('_JEXEC') or die;
?>

<link rel="stylesheet" type="text/css" href="<?php echo JUri::base() . 'modules/mod_nelo_menu_social/css/menusocial.css' ?>">
<style>
    .nav{
        overflow:hidden;
        margin:0 auto;
        width:100%;
    }
    .nav li{
        width: <?php echo 100 / 5 . '%' ?>;
        height:180px;
        display:inline-block;
        float:left;
        cursor:pointer;
        transition:all 0.4s;
        -webkit-transition:all 0.4s;
        -moz-transition:all 0.4s;
    }
</style>


<div class="menuSocaial">
    <ul class="nav">
        <li class="hm">
            <a href="index.php">
                <img class="icon" src="<?php echo JUri::base() . 'modules/mod_nelo_menu_social/images/home.png' ?>" alt="">
                <span>Home</span>
            </a>
        </li>
        <li class="fb">
            <a href="index.php">
                <img class="icon" src="<?php echo JUri::base() . 'modules/mod_nelo_menu_social/images/social-facebook.png' ?>" alt="">
                <span>Facebook</span>
            </a>
        </li>
        <li class="gp">
            <a href="index.php">
                <img class="icon" src="<?php echo JUri::base() . 'modules/mod_nelo_menu_social/images/google-plus.png' ?>" alt="">
                <span>Google-plus</span>
            </a>
        </li>
        <li class="tw">
            <a href="index.php">
                <img class="icon" src="<?php echo JUri::base() . 'modules/mod_nelo_menu_social/images/social-twitter.png' ?>" alt="">
                <span>Twitter</span>
            </a>
        </li>
        <li class="cl">
            <a href="index.php">
                <img class="icon" src="<?php echo JUri::base() . 'modules/mod_nelo_menu_social/images/phone.png' ?>" alt="">
                <span>Call</span>
            </a>
        </li>

    </ul>
</div>