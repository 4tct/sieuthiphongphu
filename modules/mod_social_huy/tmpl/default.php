<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .nelo-social{
        float: <?php echo $params->get('alignAuto') ?>;
    }
    .nelo-social ul{
        display: inline;
    }
    .socials li{
        display: inline;
        padding: 0 5px;
    }
    .socials li a{
        text-decoration: none;
    }
    .socials li a i{
        color: <?php echo $params->get('colorShow') ?>
    }
    .socials li a i:hover{
        color: <?php echo $params->get('colorHover') ?>
    }
</style>

<?php
if ($params->get('statusSocial') == "top") {
    ?>
    <div class="nelo-social">
        <ul class="socials">
            <?php
            $arr_social = array('facebook', 'twitter', 'pinterest', 'youtube', 'houzz');
            foreach ($arr_social as $value) {
                if ($socials->get('show_link_' . $value)) {
                    $t = $socials->get('link_' . $value);
                    $href = '';
                    if (!empty($t)) {
                        $href = 'href="' . $t . '"';
                    }
                    ?>
                    <li class="<?php echo $value ?>">
                        <a title="<?php echo ucwords($value) ?>" <?php echo $href; ?> target="_blank">
                            <i class="fa fa-<?php echo $value ?>"></i>
                        </a>
                    </li>
                    <?php
                }
            }
            ?>
        </ul> 
    </div>
    <?php
} else if ($params->get('statusSocial') == "menu") {
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo JUri::base() . 'modules/mod_social_huy/css/menusocial.css' ?>">
    <?php
    $arr_social = array('facebook', 'twitter', 'pinterest', 'youtube', 'houzz');
    $t = 1;
    foreach ($arr_social as $s) {
        if ($params->get('show_link_' . $s)) {
            ++$t;
        }
    }
    ?>
    <style>
        .nav{
            overflow:hidden;
            margin:0 auto;
            width:100%;
        }
        .nav li{
            width: <?php echo 100 / $t . '%' ?>;
            height:180px;
            display:inline-block;
            float:left;
            cursor:pointer;
            transition:all 0.4s;
            -webkit-transition:all 0.4s;
            -moz-transition:all 0.4s;
        }
    </style>

    <div class="menuSocaial">
        <ul class="nav">
            <li class="hm">
                <a href="index.php">
                    <img class="icon" src="<?php echo JUri::base() . 'modules/mod_social_huy/images/home.png' ?>" alt="">
                    <span>Home</span>
                </a>
            </li>
            <?php
            if ($params->get('show_link_facebook')) {
                ?>
                <li class="fb">
                    <a href="<?php echo $params->get('link_facebook'); ?>" target="_balnk">
                        <img class="icon" src="<?php echo JUri::base() . 'modules/mod_social_huy/images/social-facebook.png' ?>" alt="">
                        <span>Facebook</span>
                    </a>
                </li>
                <?php
            }
            ?>
            <?php
            if ($params->get('show_link_twitter')) {
                ?>
                <li class="tw">
                    <a href="<?php echo $params->get('link_twitter'); ?>" target="_balnk">
                        <img class="icon" src="<?php echo JUri::base() . 'modules/mod_social_huy/images/social-twitter.png' ?>" alt="">
                        <span>Twitter</span>
                    </a>
                </li>
                <?php
            }
            ?>
            <?php
            if ($params->get('show_link_pinterest')) {
                ?>
                <li class="pt">
                    <a href="<?php echo $params->get('link_pinterest'); ?>" target="_balnk">
                        <img class="icon" src="<?php echo JUri::base() . 'modules/mod_social_huy/images/pinterest.png' ?>" alt="">
                        <span>Pinterest</span>
                    </a>
                </li>
                <?php
            }
            ?>
            <?php
            if ($params->get('show_link_youtube')) {
                ?>
                <li class="yt">
                    <a href="<?php echo $params->get('link_youtube'); ?>" target="_balnk">
                        <img class="icon" src="<?php echo JUri::base() . 'modules/mod_social_huy/images/youtube.png' ?>" alt="">
                        <span>Youtube</span>
                    </a>
                </li>
                <?php
            }
            ?>
                <?php
            if ($params->get('show_link_houzz')) {
                ?>
                <li class="hz">
                    <a href="<?php echo $params->get('link_houzz'); ?>" target="_balnk">
                        <img class="icon" src="<?php echo JUri::base() . 'modules/mod_social_huy/images/houzz.png' ?>" alt="">
                        <span>Houzz</span>
                    </a>
                </li>
                <?php
            }
            ?>

        </ul>
    </div>
    <?php
}
?>
