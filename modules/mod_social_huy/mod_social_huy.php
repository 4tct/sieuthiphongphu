<?php
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$socials = Modsocial_huyHelper::getsocial_huy($params);
require JModuleHelper::getLayoutPath('mod_social_huy');