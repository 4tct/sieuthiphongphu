<?php
class Modnelo_searchHelper
{
    public static function getnelo_search($params)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('id,link')
                ->from($db->quoteName('#__menu'))
                ->where('id = ' . $params->get('menu_id'));
        $res = $db->setQuery($query)->loadObject();
        return $res->link . '&Itemid=' . $res->id;
    }
}
