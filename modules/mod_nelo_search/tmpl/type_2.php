<?php
// No direct access
defined('_JEXEC') or die;
?>

<style>
    .main-search #filter_search{
        padding: 7px;
        width: 450px;
        outline: 0;
        border: 2px solid red;
        border-left: 0px;
    }
    .main-search .slt_category_search{
        background: #e4e4e4;
        padding: 8px;
        margin-right: -5px;
        color: black;
        outline: 0;
        border: 2px solid red;
        border-right: 0px;
        width: 180px;
        display: none;
    }
    .main-search .btn-search{
        margin-left: -5px;
        padding: 7px;
        background: red;
        border: 2px solid red;
        color: white;
        cursor: pointer;
        outline: 0;
    }
    .main-search .btn-search i{
        padding: 0px 15px;
    }
</style>

<form id="main_search" action="<?php echo JRoute::_($link); ?>" method="post">
    <div class="main-search">
        <select class="slt_category_search" name="slt_category_search" style="display: none;">
            <option>Tất cả danh mục</option>
        </select>
        <input type="text" value="" name="filter_search" id="filter_search" placeholder="Nhập nội dung cần tìm..." />
        <button type="submit" class="btn-search">
            <i class="fa fa-search" aria-hidden="true"></i>
        </button>
    </div>
</form>
