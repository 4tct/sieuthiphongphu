<?php
// No direct access
defined('_JEXEC') or die;
//$app = JFactory::getApplication()->input;
?>

<style>
    .search-box{
        float: left;
        padding: 30px 0 0 10%;
    }
    .form-search {
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
        display: inline-block;
        position: relative;
        width: 485px;
        height: 42px;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(221, 221, 221);
        background: rgb(255, 255, 255);
    }
    .form-search .input-search {
        position: relative;
        float: left;
        width: 300px;
        height: 40px;
        padding-top: 5px;
    }
    .form-search .input-search #key_search {
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
        width: 100%;
        color: rgb(187, 187, 187);
        border-width: initial;
        border-style: none;
        border-color: initial;
        background: none;
    }
    input.input-text {
        width: auto;
        padding: 0px 10px;
    }
    input.input-text {
        width: auto;
        padding: 0px 10px;
    }

    select, .input-text, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"] {
        height: 30px;
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
        color: rgb(102, 102, 102);
        box-shadow: none;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(221, 221, 221);
        outline: none !important;
    }
    .form-search .cat-wrapper {
        float: left;
        position: relative;
        border-left: 1px solid rgb(221, 221, 221);
    }
    .form-search .cat-wrapper span {
        line-height: 40px;
        padding-left: 30px;
        white-space: nowrap;
        position: relative;
        margin-left: 4px;
        width: 110px;
        overflow: hidden;
    }
    .form-search .cat-wrapper select {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        opacity: 0;
    }
    .form-search .button-search-pro {
        font-size: 16px;
        position: absolute;
        right: 0px;
    }
    .button-search-pro {
        position: absolute;
        height: 41px;
        width: 55px;
        font-size: 116.67%;
        background-color: <?php echo OpenShopHelper::getConfigValue('t_background_color'); ?>;
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
        color: rgb(255, 255, 255);
        border-width: 1px;
        border-style: solid;
        border-color: <?php echo OpenShopHelper::getConfigValue('t_background_color'); ?>;
    }
    @media(max-width: 1200px){
        .search-box{
            padding: 30px 0 0 3%;
        }
    }
    @media(max-width: 1120px){
        #search_mini_form{
            width: 375px;
        }
        .form-search{
            width: 360px;
        }
        .form-search .input-search{
            width: 200px;
        }
    }
    
    .cat-wrapper .selector{
        width: 30px;
    }
    .cat-wrapper .selector .catall{
        width: 0px; 
        -webkit-user-select: none;
    }
    .sm-text-title{
        color: #999; cursor:pointer;
    }
</style>

<form id="search_mini_form" action="<?php echo JRoute::_($link); ?>" method="post">
    <div class="form-search">
        <div class="input-search"> 
            <input size="30" id="key_search" type="text" name="filter_search" class="input-text" value="<?php echo JRequest::getString('filter_search') ?>" placeholder="Nhập từ cần tìm">
        </div>
        <div class="cat-wrapper">
            <div class="selector" id="uniform-cat">
                <span class="catall">Tất cả</span>
                <select id="cat" name="cat" class="jqtransformdone">
                    <option value="">Tất cả</option>
                </select>
            </div>
        </div>
        <button type="submit" title="Search" class="button-search-pro form-button">
            <i class="fa fa-search"></i>
        </button>
    </div>

    <div id="search_autocomplete" class="search-autocomplete"></div>
    <a class="sm-text-title">
        Tìm kiếm nhiều:  áo thun hoạt hình, áo thun dễ thương, áo thun game thủ,...
    </a>
</form>

