toastr.options = {
    "positionClass": "toast-bottom-left",
    "closeButton": true,
}

function loadProductShowHomeSite(cls_cur, i, id_m) {
    var clses = cls_cur.split('_');
    jQuery('.appendLoading_' + clses[0]).append('<div class="loadingProCat lpc_' + clses[0] + '">'
            + '<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
            + '</div>');
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=product.loadProductShowHomeSite',
        method: 'post',
        data: {
            i: i,
            style: $('.widthBodyProduct_' + id_m + ' .swiper-slide').attr('style')
        },
        success: function (dt) {
            jQuery('.addLoadingFa' + cls_cur + ' i').remove();
            var d = jQuery.parseJSON(dt);
            if (d.status === 'success') {
                jQuery('.load_addLoadingFa' + clses[0]).html(d.html);

                //remove style
                jQuery('.load_addLoadingFa' + clses[0]).removeAttr('style');

                //link all
                var link_all = jQuery('.addLoadingFa' + cls_cur + ' a').attr('data-href');
                jQuery('.load_all_right_active_title_t4_' + clses[0] + ' a').attr('href', link_all);

                //active tab
                jQuery('.title_t4_' + clses[0]).removeClass('active_title_t4');
                jQuery('.title_t4_' + clses[0]).removeClass('default_title_t4');

                jQuery('.title_t4_' + clses[0]).addClass('default_title_t4');
                jQuery('.addLoadingFa' + cls_cur).addClass('active_title_t4');
            }

            //remove loading
            jQuery('.lpc_' + clses[0]).remove();
            //message
            toastr[d.status](d.message);
        }
    });
}


function setBuyProduct(i) {
    jQuery('#myDetailProduct').modal();
    jQuery('#myDetailProductLabel').html('<img src="images/loading/loading_3.gif" width="20px"/>');
}

/*
 * 
 */
function getTestModalDetail(i, url) {
    jQuery('#myTestModalDetail').modal('hide');
    jQuery('.showModalContentProductDetail').remove();
    jQuery('#myLoading').modal('show');
    history.pushState({ url: url }, "", url);

    jQuery.ajax({
        url: 'index.php?option=com_openshop&task=quote.getShowProductDetail&format=ajax',
        method: 'post',
        data: {
            i: i,
            title: document.title,
            token: Math.random(),
            urlHistory: jQuery('#urlHistory').val()
        },
        success: function (dt) {
            jQuery('#myLoading').modal('hide');
            jQuery('body').addClass('modal-open');
            jQuery('#showDetail').html(dt);
        }
    })

}

function checkGetURL() {
    jQuery.ajax({
        url: 'index.php?option=com_openshop&task=quote.getUrl&format=ajax',
        method: 'post',
        data: {
            catID: jQuery('#catID').val()
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            window.location.href = d.url;
        }
    });
}

function closeModal(t) {
    jQuery('#myTestModalDetail').modal('hide');
    jQuery('.widthFixed').css('display', 'none');
    if (typeof (t) !== 'undefined' && t !== '') {
        document.title = t;
    }
}

/*
 * Check Quantiry
 */
function checkQuantity(i, v) {
    if (v < 0 || v === '') {
        toastr['warning']('Số lượng không được nhỏ hơn 0');
        jQuery('.inputQ_' + i).val(0);
    } else if (v > 3) {
        toastr['warning']('Số lượng đặt vượt quá quy định.');
        jQuery('.inputQ_' + i).val(0);
    } else {
        console.log(v);
    }
}

/*
 * 
 */
function subQuantity(i_s, i) {
    var v = jQuery('.inputQ_' + i).val();
    var r = parseInt(v) - 1;
    if (r < 0) {
        toastr['warning']('Số lượng không được nhỏ hơn 0');
    } else {
        jQuery('.inputQ_' + i).val(r);
        changeSizeProduct(i_s, r);
    }
}

/*
 * 
 */
function addQuantity(i_s, i) {
    var v = jQuery('.inputQ_' + i).val();
    var r = parseInt(v) + 1;
    if (r > 3) {
        toastr['warning']('Số lượng đặt vượt quá quy định.');
    } else {
        jQuery('.inputQ_' + i).val(r);
        changeSizeProduct(i_s, r);
        jQuery('.inputQ_' + i).removeClass('animationValQuantity');
    }
}

/*
 * Phan trang dang load ajax
 */
function getProductPagination(i, col) {
    jQuery('.loadingLoadMore').html('<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>');
    jQuery.ajax({
        url: 'index.php?option=com_openshop&format=ajax&task=product.getProductPagination()',
        method: 'post',
        data: {
            i: i,
            col: col
        },
        success: function (dt) {
            var d = jQuery.parseJSON(dt);
            jQuery('.loadingLoadMore').html('');

            if (d.status === 'success') {
                var html = '';
                jQuery.each(d.products, function (i, v) {
                    html += '<div class="wow fadeInUp col-sm-4 b_t_2 b_t_t_2 ' + col + '">';
                    html += '   <div class="borderContentProduct">';
                    html += '       <div class="infoImgT2" onclick="getTestModalDetail(\'' + v.id_encode + '\', \'' + v.url + '\')" >';
                    html += '           <img style="display: inline;" src="' + v.link_image + '" class="img-responsive" title="' + v.product_name + '" alt="' + v.product_name + '"/>';
                    html += '           <div class="infoChildProT2">';
                    html += '               <div class="pricePro1">';
                    html += '                   ' + v.product_price + '<sup>đ</sup>';
                    if (v.product_price_r > 0) {
                        html += '               <span class="pricenotbuy">';
                        html += '                   ' + v.product_price_r + '<sup>đ</sup>';
                        html += '               </span>';
                    }
                    html += '               </div>';
                    html += '               <div>';
                    html += '                   Kích thước: ' + d.size[ v.id ];
                    html += '               </div>';
                    html += '           </div>';
                    html += '       </div>';
                    //                tên sản phẩm
                    html += '       <div class="namePro">';
                    html += '            <a onclick="getTestModalDetail(\'' + v.id_encode + '\', \'' + v.url + '\')" >';
                    html += '                ' + v.product_name;
                    html += '            </a>';
                    html += '       </div>';
                    html += '       <div class="codePro">';
                    html += '           Mã: <strong>' + v.sku + '</strong>';
                    html += '       </div>';
                    html += '       <div class="pricePro">';
                    html += '           ' + v.product_price + '<sup>đ</sup>';
                    if (v.product_price_r > 0) {
                        html += '       <span class="pricenotbuy">';
                        html += '           ' + v.product_price_r + '<sup>đ</sup>';
                        html += '       </span>';
                    }
                    html += '       </div>';
                    html += '       <div class="btn-buy">';
                    html += '           <a onclick="getTestModalDetail(\'' + v.id_encode + '\', \'' + v.url + '\')" >';
                    html += '               <i class="fa fa-shopping-cart" aria-hidden="true"></i>';
                    html += '               <span>Mua ngay</span>';
                    html += '           </a>';
                    html += '       </div>';
                    html += '   </div>';
                    html += '</div>';
                });

                jQuery('#contentLoadT2').append(html);
            } else {
                toastr[d.status](d.message);
            }
        }
    });
}

function updateQuantity(cls, i, c) {
    var quanCur = jQuery('.inputQ_' + cls).val();
    var check = 1;
    if (c === '+') {
        ++quanCur;
        if (quanCur > 5) {
            toastr['warning']('Số lượng đặt vượt quá giới hạn. Vui lòng liên hệ với chúng tôi!');
            check = 0;
        } else {
            jQuery('.inputQ_' + cls).val(quanCur);
        }
    } else if (c === '-') {
        --quanCur;
        if (quanCur < 1) {
            toastr['warning']('Số lượng không được nhỏ hơn 1!');
            check = 0;
        } else {
            jQuery('.inputQ_' + cls).val(quanCur);
        }
    } else {
        if (quanCur < 1 || quanCur > 5) {
            toastr['warning']('Số lượng đặt từ 1 đến 5. Nếu lớn hơn vui lòng liên hệ với chúng tôi');
            check = 0;
            jQuery('.inputQ_' + cls).val('1');
        } else {
            jQuery('.inputQ_' + cls).val(quanCur);
        }
    }

    if (check) {
        jQuery('.loading').css('display', 'block');
        jQuery.ajax({
            url: 'index.php?option=com_openshop&format=ajax&task=product.updateQuantityProduct',
            method: 'post',
            data: {
                i: i,
                q: quanCur
            },
            success: function (dt) {
                var d = jQuery.parseJSON(dt);
                toastr[d.status](d.message);
                jQuery('.loading').css('display', 'none');
            }
        });
    }
}

function closeModalCat(t,url) {
    jQuery('#myTestModalDetail').modal('hide');
    jQuery('.widthFixed').css('display', 'none');
    if (typeof (t) !== 'undefined' && t !== '') {
        document.title = t;
    }
    
    if(typeof (url) !== 'undefined' && url !== ''){
        history.pushState({}, "", url);
    }
}






